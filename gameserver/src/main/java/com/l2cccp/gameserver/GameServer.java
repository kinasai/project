package com.l2cccp.gameserver;

import java.io.File;
import java.net.InetAddress;
import java.net.ServerSocket;

import org.napile.primitive.Variables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.lang.StatsUtils;
import com.l2cccp.commons.listener.Listener;
import com.l2cccp.commons.listener.ListenerList;
import com.l2cccp.commons.net.nio.impl.SelectorStats;
import com.l2cccp.commons.net.nio.impl.SelectorThread;
import com.l2cccp.commons.versioning.Version;
import com.l2cccp.gameserver.cache.CrestCache;
import com.l2cccp.gameserver.dao.CharacterDAO;
import com.l2cccp.gameserver.dao.CharacterMinigameScoreDAO;
import com.l2cccp.gameserver.dao.CharacterVariablesDAO;
import com.l2cccp.gameserver.dao.ItemsDAO;
import com.l2cccp.gameserver.data.BoatHolder;
import com.l2cccp.gameserver.data.xml.Parsers;
import com.l2cccp.gameserver.data.xml.holder.EventHolder;
import com.l2cccp.gameserver.data.xml.holder.LeviathanEventHolder;
import com.l2cccp.gameserver.data.xml.holder.ResidenceHolder;
import com.l2cccp.gameserver.data.xml.holder.StaticObjectHolder;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.geodata.GeoEngine;
import com.l2cccp.gameserver.handler.admincommands.AdminCommandHandler;
import com.l2cccp.gameserver.handler.bbs.BbsHandlerHolder;
import com.l2cccp.gameserver.handler.bypass.BypassHolder;
import com.l2cccp.gameserver.handler.items.ItemHandler;
import com.l2cccp.gameserver.handler.onshiftaction.OnShiftActionHolder;
import com.l2cccp.gameserver.handler.usercommands.UserCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.idfactory.IdFactory;
import com.l2cccp.gameserver.instancemanager.AutoSpawnManager;
import com.l2cccp.gameserver.instancemanager.BloodAltarManager;
import com.l2cccp.gameserver.instancemanager.CastleManorManager;
import com.l2cccp.gameserver.instancemanager.CoupleManager;
import com.l2cccp.gameserver.instancemanager.CursedWeaponsManager;
import com.l2cccp.gameserver.instancemanager.HellboundManager;
import com.l2cccp.gameserver.instancemanager.PetitionManager;
import com.l2cccp.gameserver.instancemanager.PlayerMessageStack;
import com.l2cccp.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2cccp.gameserver.instancemanager.SoDManager;
import com.l2cccp.gameserver.instancemanager.SoIManager;
import com.l2cccp.gameserver.instancemanager.SpawnManager;
import com.l2cccp.gameserver.instancemanager.games.FishingChampionShipManager;
import com.l2cccp.gameserver.instancemanager.games.LotteryManager;
import com.l2cccp.gameserver.instancemanager.games.MiniGameScoreManager;
import com.l2cccp.gameserver.instancemanager.naia.NaiaCoreManager;
import com.l2cccp.gameserver.instancemanager.naia.NaiaTowerManager;
import com.l2cccp.gameserver.listener.GameListener;
import com.l2cccp.gameserver.listener.game.OnShutdownListener;
import com.l2cccp.gameserver.listener.game.OnStartListener;
import com.l2cccp.gameserver.mod.DailyTasks.DailyTaskEngine;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.entity.Hero;
import com.l2cccp.gameserver.model.entity.MonsterRace;
import com.l2cccp.gameserver.model.entity.SevenSigns;
import com.l2cccp.gameserver.model.entity.SevenSignsFestival.SevenSignsFestival;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.model.offlinebuffer.OfflineBuffers;
import com.l2cccp.gameserver.network.authcomm.AuthServerCommunication;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.GamePacketHandler;
import com.l2cccp.gameserver.network.telnet.TelnetServer;
import com.l2cccp.gameserver.scripts.QuestLoader;
import com.l2cccp.gameserver.scripts.Scripts;
import com.l2cccp.gameserver.tables.ClanTable;
import com.l2cccp.gameserver.tables.EnchantHPBonusTable;
import com.l2cccp.gameserver.tables.PetSkillsTable;
import com.l2cccp.gameserver.tables.SkillTreeTable;
import com.l2cccp.gameserver.taskmanager.ItemsAutoDestroy;
import com.l2cccp.gameserver.taskmanager.tasks.DeleteExpiredMailTask;
import com.l2cccp.gameserver.taskmanager.tasks.DeleteExpiredVarsTask;
import com.l2cccp.gameserver.taskmanager.tasks.OlympiadSaveTask;
import com.l2cccp.gameserver.taskmanager.tasks.RecNevitResetTask;
import com.l2cccp.gameserver.taskmanager.tasks.SoIStageUpdater;
import com.l2cccp.gameserver.utils.TradeHelper;
import com.l2cccp.gameserver.utils.velocity.VelocityUtils;

import Interface.KeyChecker;
import net.sf.ehcache.CacheManager;

public class GameServer
{
	private static final Logger _log = LoggerFactory.getLogger(GameServer.class);

	public class GameServerListenerList extends ListenerList<GameServer>
	{
		public void onStart()
		{
			for(Listener<GameServer> listener : getListeners())
				if(OnStartListener.class.isInstance(listener))
					((OnStartListener) listener).onStart();
		}

		public void onShutdown()
		{
			for(Listener<GameServer> listener : getListeners())
				if(OnShutdownListener.class.isInstance(listener))
					((OnShutdownListener) listener).onShutdown();
		}
	}

	public static GameServer _instance;

	private final SelectorThread<GameClient> _selectorThreads[];
	private final SelectorStats _selectorStats = new SelectorStats();
	private Version version;
	private TelnetServer statusServer;
	private final GameServerListenerList _listeners;

	private long _serverStartTimeMillis;

	public SelectorThread<GameClient>[] getSelectorThreads()
	{
		return _selectorThreads;
	}

	public SelectorStats getSelectorStats()
	{
		return _selectorStats;
	}

	public long getServerStartTime()
	{
		return _serverStartTimeMillis;
	}

	@SuppressWarnings("unchecked")
	public GameServer() throws Exception
	{
		_instance = this;
		_serverStartTimeMillis = System.currentTimeMillis();
		_listeners = new GameServerListenerList();

		new File("./log/").mkdir();
		KeyChecker.getInstance();

		version = new Version(GameServer.class);

		_log.info("=================================================");
		_log.info("Revision: ................ " + version.getVersionRevision());
		_log.info("Build date: .............. " + version.getBuildDate());
		_log.info("Build JDK: ............... " + version.getBuildJdk());
		_log.info("System JDK: .............. " + version.getJDK());
		_log.info("=================================================");

		Variables.RETURN_LONG_VALUE_IF_NOT_FOUND = -1;

		// Initialize config
		Config.load();
		VelocityUtils.init();
		// Check binding address
		checkFreePorts();
		// Initialize database
		Class.forName(Config.DATABASE_DRIVER).newInstance();
		DatabaseFactory.getInstance().getConnection().close();
		CharacterVariablesDAO.getInstance().deleteLastHeroVars("HeroPeriod");
		DailyTaskEngine.refresh();

		IdFactory _idFactory = IdFactory.getInstance();
		if(!_idFactory.isInitialized())
		{
			_log.error("Could not read object IDs from DB. Please Check Your Data.");
			throw new Exception("Could not initialize the ID factory");
		}

		CacheManager.getInstance();

		ThreadPoolManager.getInstance();

		Scripts.getInstance();

		GeoEngine.load();

		GameTimeController.getInstance();

		World.init();

		Parsers.parseAll();

		ItemsDAO.getInstance();

		CrestCache.getInstance();

		CharacterDAO.getInstance();

		ClanTable.getInstance();

		SkillTreeTable.getInstance();

		EnchantHPBonusTable.getInstance();

		PetSkillsTable.getInstance();

		SpawnManager.getInstance().spawnAll();

		BoatHolder.getInstance().spawnAll();

		StaticObjectHolder.getInstance().spawnAll();

		RaidBossSpawnManager.getInstance();

		Scripts.getInstance().init();

		QuestLoader.getInstance().init();

		LotteryManager.getInstance();

		PlayerMessageStack.getInstance();

		if(Config.AUTODESTROY_ITEM_AFTER > 0)
			ItemsAutoDestroy.getInstance();

		MonsterRace.getInstance();

		SevenSigns.getInstance();
		SevenSignsFestival.getInstance();
		SevenSigns.getInstance().updateFestivalScore();

		AutoSpawnManager.getInstance();

		SevenSigns.getInstance().spawnSevenSignsNPC();

		if(Config.ENABLE_OLYMPIAD)
		{
			Olympiad.load();
			Hero.getInstance();
		}

		PetitionManager.getInstance();

		CursedWeaponsManager.getInstance();

		if(!Config.ALLOW_WEDDING)
		{
			CoupleManager.getInstance();
			_log.info("CoupleManager initialized");
		}

		ItemHandler.getInstance();

		AdminCommandHandler.getInstance().log();
		UserCommandHandler.getInstance().log();
		VoicedCommandHandler.getInstance().log();
		BbsHandlerHolder.getInstance().log();
		BypassHolder.getInstance().log();
		OnShiftActionHolder.getInstance().log();

		// tasks
		if(Config.ENABLE_OLYMPIAD)
			new OlympiadSaveTask();
		if(Config.EX_JAPAN_MINIGAME)
			CharacterMinigameScoreDAO.getInstance().select();

		new DeleteExpiredVarsTask();
		new DeleteExpiredMailTask();
		new SoIStageUpdater();
		new RecNevitResetTask();

		ClanTable.getInstance().checkClans();

		_log.info("=[Events]=========================================");
		ResidenceHolder.getInstance().callInit();
		EventHolder.getInstance().callInit();
		_log.info("=[Leviathan Engine]===============================");
		LeviathanEventHolder.getInstance().initAll();
		_log.info("==================================================");

		CastleManorManager.getInstance();

		Runtime.getRuntime().addShutdownHook(Shutdown.getInstance());

		_log.info("IdFactory: Free ObjectID's remaining: " + IdFactory.getInstance().size());

		CoupleManager.getInstance();

		if(Config.ALT_FISH_CHAMPIONSHIP_ENABLED)
			FishingChampionShipManager.getInstance();

		HellboundManager.getInstance();

		NaiaTowerManager.getInstance();
		NaiaCoreManager.getInstance();

		SoDManager.getInstance();
		SoIManager.getInstance();
		BloodAltarManager.getInstance();

		MiniGameScoreManager.getInstance();
		//parsers();
		Shutdown.getInstance().schedule(Config.RESTART_AT_TIME, Shutdown.RESTART);

		GamePacketHandler gph = new GamePacketHandler();

		InetAddress serverAddr = Config.GAMESERVER_HOSTNAME.equalsIgnoreCase("*") ? null : InetAddress.getByName(Config.GAMESERVER_HOSTNAME);

		_selectorThreads = new SelectorThread[Config.PORTS_GAME.length];
		for(int i = 0; i < Config.PORTS_GAME.length; i++)
		{
			_selectorThreads[i] = new SelectorThread<GameClient>(Config.SELECTOR_CONFIG, _selectorStats, gph, gph, gph, null);
			_selectorThreads[i].openServerSocket(serverAddr, Config.PORTS_GAME[i]);
			_selectorThreads[i].start();
		}

		getListeners().onStart();

		if(Config.SERVICES_OFFLINE_TRADE_RESTORE_AFTER_RESTART)
		{
			_log.info("Restoring offline traders...");
			int count = TradeHelper.restoreOfflineTraders();
			_log.info("Restored " + count + " offline traders.");
		}

		if(Config.OFFLINE_BUFFER_STORE_ENABLED)
		{
			_log.info("Loading offline buffers...");
			OfflineBuffers.getInstance().restoreOfflineBuffers();
		}
		//NpcDataDropParser.main(null);
		_log.info("GameServer started.");

		AuthServerCommunication.getInstance().start();


		if (Config.WEB_SERVER_DELAY > 0) {
			ThreadPoolManager.getInstance().scheduleAtFixedRate(new com.l2cccp.gameserver.webserver.WebServer(), 5000L, Config.WEB_SERVER_DELAY);
		}
		if(Config.IS_TELNET_ENABLED)
			statusServer = new TelnetServer();
		else
			_log.info("Telnet server is currently disabled.");

		_log.info("=================================================");
		String memUsage = new StringBuilder().append(StatsUtils.getMemUsage()).toString();
		for(String line : memUsage.split("\n"))
			_log.info(line);
		_log.info("=================================================");

		if(Config.MEM_DEBUG)
			ThreadPoolManager.getInstance().scheduleAtFixedRate(new MemmoryDebug(), 1000L, 10000L);
	}

	public GameServerListenerList getListeners()
	{
		return _listeners;
	}

	public static GameServer getInstance()
	{
		return _instance;
	}

	public <T extends GameListener> boolean addListener(T listener)
	{
		return _listeners.add(listener);
	}

	public <T extends GameListener> boolean removeListener(T listener)
	{
		return _listeners.remove(listener);
	}

	public static void checkFreePorts()
	{
		boolean binded = false;
		while(!binded)
		{
			for(int PORT_GAME : Config.PORTS_GAME)
				try
				{
					ServerSocket ss;
					if(Config.GAMESERVER_HOSTNAME.equalsIgnoreCase("*"))
						ss = new ServerSocket(PORT_GAME);
					else
						ss = new ServerSocket(PORT_GAME, 50, InetAddress.getByName(Config.GAMESERVER_HOSTNAME));
					ss.close();
					binded = true;
				}
				catch(Exception e)
				{
					_log.warn("Port " + PORT_GAME + " is allready binded. Please free it and restart server.");
					binded = false;
					try
					{
						Thread.sleep(1000);
					}
					catch(InterruptedException e2)
					{}
				}
		}
	}

	public static void main(String[] args) throws Exception
	{
		new GameServer();
	}

	public Version getVersion()
	{
		return version;
	}

	public TelnetServer getStatusServer()
	{
		return statusServer;
	}

}