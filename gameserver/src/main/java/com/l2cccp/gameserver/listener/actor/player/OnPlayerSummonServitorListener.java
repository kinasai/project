package com.l2cccp.gameserver.listener.actor.player;

import com.l2cccp.gameserver.listener.PlayerListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;

/**
 * @author VISTALL
 * @date 15:37/05.08.2011
 */
public interface OnPlayerSummonServitorListener extends PlayerListener
{
	void onSummonServitor(Player player, Servitor servitor);
}
