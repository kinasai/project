package com.l2cccp.gameserver.listener.event;

import com.l2cccp.gameserver.listener.EventListener;
import com.l2cccp.gameserver.model.entity.events.Event;

/**
 * @author VISTALL
 * @date 7:18/10.06.2011
 */
public interface OnStartStopListener extends EventListener
{
	void onStart(Event event);

	void onStop(Event event);
}
