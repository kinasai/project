package com.l2cccp.gameserver.listener.actor;

import com.l2cccp.gameserver.listener.CharListener;
import com.l2cccp.gameserver.model.Creature;

/**
 * @author VISTALL
 * @date 20:50/13.03.2012
 */
public interface OnDeathFromUndyingListener extends CharListener
{
	void onDeathFromUndying(Creature actor, Creature killer);
}
