package com.l2cccp.gameserver.listener.game;

import com.l2cccp.gameserver.listener.GameListener;

public interface OnShutdownListener extends GameListener
{
	public void onShutdown();
}
