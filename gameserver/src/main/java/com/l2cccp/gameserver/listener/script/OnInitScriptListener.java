package com.l2cccp.gameserver.listener.script;

import com.l2cccp.gameserver.listener.ScriptListener;

/**
 * @author VISTALL
 * @date 1:06/19.08.2011
 */
public interface OnInitScriptListener extends ScriptListener
{
	void onInit();
}
