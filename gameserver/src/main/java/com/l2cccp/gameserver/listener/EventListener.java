package com.l2cccp.gameserver.listener;

import com.l2cccp.commons.listener.Listener;
import com.l2cccp.gameserver.model.entity.events.Event;

/**
 * @author VISTALL
 * @date 7:17/10.06.2011
 */
public interface EventListener extends Listener<Event>
{

}
