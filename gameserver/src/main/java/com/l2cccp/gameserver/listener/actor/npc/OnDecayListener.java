package com.l2cccp.gameserver.listener.actor.npc;

import com.l2cccp.gameserver.listener.NpcListener;
import com.l2cccp.gameserver.model.instances.NpcInstance;

public interface OnDecayListener extends NpcListener
{
	public void onDecay(NpcInstance actor);
}
