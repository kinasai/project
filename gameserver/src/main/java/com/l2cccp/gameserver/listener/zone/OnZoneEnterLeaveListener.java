package com.l2cccp.gameserver.listener.zone;

import com.l2cccp.commons.listener.Listener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Zone;

public interface OnZoneEnterLeaveListener extends Listener<Zone>
{
	public void onZoneEnter(Zone zone, Creature actor);

	public void onZoneLeave(Zone zone, Creature actor);
}
