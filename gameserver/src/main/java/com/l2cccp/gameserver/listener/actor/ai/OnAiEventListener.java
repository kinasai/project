package com.l2cccp.gameserver.listener.actor.ai;

import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.listener.AiListener;
import com.l2cccp.gameserver.model.Creature;

public interface OnAiEventListener extends AiListener
{
	public void onAiEvent(Creature actor, CtrlEvent evt, Object[] args);
}
