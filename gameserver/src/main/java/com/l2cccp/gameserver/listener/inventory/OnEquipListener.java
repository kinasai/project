package com.l2cccp.gameserver.listener.inventory;

import com.l2cccp.commons.listener.Listener;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.items.ItemInstance;

public interface OnEquipListener extends Listener<Playable>
{
	public void onEquip(int slot, ItemInstance item, Playable actor);

	public void onUnequip(int slot, ItemInstance item, Playable actor);
}
