package com.l2cccp.gameserver.listener.actor.player;

import com.l2cccp.gameserver.listener.PlayerListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public interface OnPlayerAttack extends PlayerListener
{
	public boolean canAttack(final Creature target, final Creature attacker, final Skill skill, final boolean force, final boolean nextAttackCheck);

	public SystemMsg checkForAttack(final Creature target, final Creature attacker, final Skill skill, final boolean force);
}
