package com.l2cccp.gameserver.listener.actor;

import com.l2cccp.gameserver.listener.CharListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.skills.SkillEntry;

public interface OnMagicUseListener extends CharListener
{
	public void onMagicUse(Creature actor, SkillEntry skill, Creature target, boolean alt);
}
