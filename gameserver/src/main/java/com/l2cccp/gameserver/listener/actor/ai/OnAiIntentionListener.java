package com.l2cccp.gameserver.listener.actor.ai;

import com.l2cccp.gameserver.ai.CtrlIntention;
import com.l2cccp.gameserver.listener.AiListener;
import com.l2cccp.gameserver.model.Creature;

public interface OnAiIntentionListener extends AiListener
{
	public void onAiIntention(Creature actor, CtrlIntention intention, Object arg0, Object arg1);
}
