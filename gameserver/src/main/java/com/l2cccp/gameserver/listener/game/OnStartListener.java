package com.l2cccp.gameserver.listener.game;

import com.l2cccp.gameserver.listener.GameListener;

public interface OnStartListener extends GameListener
{
	public void onStart();
}
