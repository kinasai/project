package com.l2cccp.gameserver.listener.actor.npc;

import com.l2cccp.gameserver.listener.NpcListener;
import com.l2cccp.gameserver.model.instances.NpcInstance;

public interface OnSpawnListener extends NpcListener
{
	public void onSpawn(NpcInstance actor);
}
