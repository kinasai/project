package com.l2cccp.gameserver.listener.zone.impl;

import com.l2cccp.gameserver.listener.zone.OnZoneEnterLeaveListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.entity.events.objects.TerritoryWardObject;
import com.l2cccp.gameserver.model.items.attachment.FlagItemAttachment;

/**
 * @author VISTALL
 * @date 12:48/18.09.2011
 */
public class DominionWardEnterLeaveListenerImpl implements OnZoneEnterLeaveListener
{
	public static final OnZoneEnterLeaveListener STATIC = new DominionWardEnterLeaveListenerImpl();

	@Override
	public void onZoneEnter(Zone zone, Creature actor)
	{
		if(!actor.isPlayer())
			return;

		Player player = actor.getPlayer();
		FlagItemAttachment flag = player.getActiveWeaponFlagAttachment();
		if(flag instanceof TerritoryWardObject)
		{
			flag.onLogout(player);

			player.sendDisarmMessage(((TerritoryWardObject) flag).getWardItemInstance());
		}
	}

	@Override
	public void onZoneLeave(Zone zone, Creature actor)
	{
		//
	}
}
