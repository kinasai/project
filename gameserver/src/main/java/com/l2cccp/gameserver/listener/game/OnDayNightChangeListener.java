package com.l2cccp.gameserver.listener.game;

import com.l2cccp.gameserver.listener.GameListener;

public interface OnDayNightChangeListener extends GameListener
{
	public void onDay();

	public void onNight();
}
