package com.l2cccp.gameserver.listener;

import com.l2cccp.commons.listener.Listener;
import com.l2cccp.gameserver.GameServer;

public interface GameListener extends Listener<GameServer>
{

}
