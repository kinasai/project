package com.l2cccp.gameserver.listener.actor.player;

import com.l2cccp.gameserver.listener.PlayerListener;
import com.l2cccp.gameserver.model.Player;

public interface OnPlayerPartyInviteListener extends PlayerListener
{
	public void onPartyInvite(Player player);
}
