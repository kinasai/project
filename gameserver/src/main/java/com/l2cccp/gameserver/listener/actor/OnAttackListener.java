package com.l2cccp.gameserver.listener.actor;

import com.l2cccp.gameserver.listener.CharListener;
import com.l2cccp.gameserver.model.Creature;

public interface OnAttackListener extends CharListener
{
	public void onAttack(Creature actor, Creature target);
}
