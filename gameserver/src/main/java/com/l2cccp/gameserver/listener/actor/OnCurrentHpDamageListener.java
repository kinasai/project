package com.l2cccp.gameserver.listener.actor;

import com.l2cccp.gameserver.listener.CharListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.skills.SkillEntry;

public interface OnCurrentHpDamageListener extends CharListener
{
	public void onCurrentHpDamage(Creature actor, double damage, Creature attacker, SkillEntry skill, boolean crit);
}
