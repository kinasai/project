package com.l2cccp.gameserver.listener.actor.player;

import com.l2cccp.gameserver.listener.PlayerListener;
import com.l2cccp.gameserver.model.Player;

/**
 * @author VISTALL
 * @date 9:37/15.04.2011
 */
public interface OnAnswerListener extends PlayerListener
{
	void sayYes(Player player);

	void sayNo(Player player);
}
