package com.l2cccp.gameserver.listener;

import com.l2cccp.commons.listener.Listener;
import com.l2cccp.gameserver.scripts.Scripts;

/**
 * @author VISTALL
 * @date 1:05/19.08.2011
 */
public interface ScriptListener extends Listener<Scripts>
{

}
