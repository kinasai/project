package com.l2cccp.gameserver.listener.actor;

import com.l2cccp.gameserver.listener.CharListener;
import com.l2cccp.gameserver.model.Creature;

public interface OnAttackHitListener extends CharListener
{
	public void onAttackHit(Creature actor, Creature attacker);
}
