package com.l2cccp.gameserver.listener.actor.player;

import com.l2cccp.gameserver.listener.PlayerListener;
import com.l2cccp.gameserver.model.Player;

public interface OnPlayerExitListener extends PlayerListener
{
	public void onPlayerExit(Player player);
}
