package com.l2cccp.gameserver.listener.reflection;

import com.l2cccp.commons.listener.Listener;
import com.l2cccp.gameserver.model.entity.Reflection;

public interface OnReflectionCollapseListener extends Listener<Reflection>
{
	public void onReflectionCollapse(Reflection reflection);
}
