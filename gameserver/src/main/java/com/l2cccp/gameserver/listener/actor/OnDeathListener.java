package com.l2cccp.gameserver.listener.actor;

import com.l2cccp.gameserver.listener.CharListener;
import com.l2cccp.gameserver.model.Creature;

public interface OnDeathListener extends CharListener
{
	public void onDeath(Creature actor, Creature killer);
}
