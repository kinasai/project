package com.l2cccp.gameserver.listener.actor.player;

import com.l2cccp.gameserver.listener.PlayerListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.ChatType;

/**
 * @author VISTALL
 * @date 20:45/15.09.2011
 */
public interface OnPlayerSayListener extends PlayerListener
{
	void onSay(Player activeChar, ChatType type, String target, String text);
}
