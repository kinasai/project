package com.l2cccp.gameserver.listener.actor;

import com.l2cccp.gameserver.listener.CharListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.skills.SkillEntry;

public interface OnMagicHitListener extends CharListener
{
	public void onMagicHit(Creature actor, SkillEntry skill, Creature caster);
}
