package com.l2cccp.gameserver.listener;

import com.l2cccp.commons.listener.Listener;
import com.l2cccp.gameserver.model.Creature;

public interface AiListener extends Listener<Creature>
{

}
