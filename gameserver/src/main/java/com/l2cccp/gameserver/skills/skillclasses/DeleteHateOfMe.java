package com.l2cccp.gameserver.skills.skillclasses;

import java.util.List;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ai.CtrlIntention;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.stats.Formulas;
import com.l2cccp.gameserver.templates.StatsSet;

public class DeleteHateOfMe extends Skill
{
	public DeleteHateOfMe(StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(SkillEntry skillEntry, Creature activeChar, List<Creature> targets)
	{
		for(Creature target : targets)
			if(target != null)
			{
				if(Config.SKILLS_CHANCE_SHOW && activeChar.isPlayer())
					activeChar.sendMessage(new CustomMessage("l2p.gameserver.skills.Formulas.Chance").addString(getName()).addNumber(getActivateRate()));

				if(target.isNpc() && Formulas.calcSkillSuccess(activeChar, target, skillEntry, getActivateRate()))
				{
					NpcInstance npc = (NpcInstance) target;
					npc.getAggroList().remove(activeChar, true);
					npc.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
				}
				getEffects(skillEntry, activeChar, target, true, false);
			}
	}
}