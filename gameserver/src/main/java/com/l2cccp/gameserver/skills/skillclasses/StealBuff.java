package com.l2cccp.gameserver.skills.skillclasses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.EffectType;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.skills.effects.EffectTemplate;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.templates.StatsSet;

/**
 * @author L2CCCP 
 */
public class StealBuff extends Skill
{
	private final int _count;

	public StealBuff(StatsSet set)
	{
		super(set);
		_count = set.getInteger("stealCount", 1);
	}

	@Override
	public boolean checkCondition(SkillEntry skillEntry, Creature activeChar, Creature target, boolean forceUse, boolean dontMove, boolean first)
	{
		if(target == null || !target.isPlayer())
		{
			activeChar.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
			return false;
		}

		return super.checkCondition(skillEntry, activeChar, target, forceUse, dontMove, first);
	}

	@Override
	public void useSkill(SkillEntry skillEntry, Creature activeChar, List<Creature> targets)
	{
		List<Effect> _triggerList = new ArrayList<Effect>();
		List<Effect> _musicList = new ArrayList<Effect>();
		List<Effect> _buffList = new ArrayList<Effect>();

		int calc = _count;

		if(Config.STEAL_FORMULA_DATA[0] != -1)
			calc = Rnd.get(Config.STEAL_FORMULA_DATA[0], _count);

		for(Creature _effected : targets)
		{
			double cancel_res = _effected.calcStat(Stats.CANCEL_RESIST, 0, null, null);

			if(Config.CANCEL_IMMUNITY && cancel_res >= 100) // Если резиста от кенсела 100% то идти дальше смысла нету.
				return;

			for(Effect effect : _effected.getEffectList().getAllEffects())
			{
				SkillEntry skill = effect.getSkill();
				if(skill.getTemplate().isTrigger())
					_triggerList.add(effect);
				else if(skill.getTemplate().isMusic())
					_musicList.add(effect);
				else
					_buffList.add(effect);
			}

			List<Effect> _effectList = new ArrayList<Effect>();
			Collections.reverse(_triggerList);
			Collections.reverse(_musicList);
			Collections.reverse(_buffList);
			_effectList.addAll(_triggerList); // В ХФ триггеры снимаются первыми!
			_effectList.addAll(_musicList);
			_effectList.addAll(_buffList);

			if(_effectList.isEmpty())
				return;

			int count = 0;
			int exit = 0;

			for(Effect effect : _effectList)
			{
				if(exit >= calc || exit >= _count || (count * Config.CANCEL_COUNT_LIMIT) >= _count) // Небольшей мод для разброса баффов!
					break;

				count++;
				if(canSteal(effect))
				{
					if(calcStealChance(_effected, activeChar, effect, cancel_res))
					{
						Effect stolenEffect = cloneEffect(activeChar, effect);
						if(stolenEffect != null)
							activeChar.getEffectList().addEffect(stolenEffect); // Добавляем состиленный эффект
						if(_effected.isPlayable())
							_effected.sendPacket(new SystemMessage(SystemMsg.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(effect.getSkill().getDisplayId(), effect.getSkill().getDisplayLevel()));
						effect.exit();
						exit++;
					}
				}
			}
		}
	}

	private boolean canSteal(Effect e)
	{
		final SkillEntry skill = e != null ? e.getSkill() : null;
		return e != null && e.isInUse() && e.isCancelable() && !skill.getTemplate().isToggle() && !skill.getTemplate().isPassive() && (!skill.getTemplate().isOffensive() || skill.getId() == 368) && e.getEffectType() != EffectType.Vitality && skill.getId() != 1540 && (skill.getId() != 1457 || skill.getId() != 6429 || skill.getId() != 112);
	}

	private boolean calcStealChance(Creature effected, Creature effector, Effect eff, double cancel_resist)
	{
		double chance = 0, eml, dml;
		long time;

		eml = eff.getSkill().getTemplate().getMagicLevel();
		dml = getMagicLevel() - (eml == 0 ? effected.getLevel() : eml);

		Effect ef = effected.getEffectList().getEffectByIndexAndType(eff.getSkill().getId(), eff.getEffectType());
		if(ef != null)
		{
			time = ef.getTimeLeft();
			cancel_resist = 1 - (cancel_resist * .01);
			chance = (2. * dml + 80 + time / 120000) * cancel_resist; // retail formula
		}

		int min = Config.STEAL_FORMULA_DATA[1];
		int max = Config.STEAL_FORMULA_DATA[2];

		if(chance < min)
			chance = min;
		else if(chance > max)
			chance = max;

		return Rnd.chance(chance);
	}

	private Effect cloneEffect(Creature cha, Effect eff)
	{
		SkillEntry skill = eff.getSkill();
		for(EffectTemplate et : skill.getTemplate().getEffectTemplates())
		{
			if((!skill.getTemplate().isBlockedByChar(cha, et) && !et._applyOnCaster) || skill.getId() == 368)
			{
				Effect effect = et.getEffect(new Env(cha, cha, skill));
				if(effect != null)
				{
					effect.setCount(eff.getCount());
					effect.setPeriod(eff.getCount() == 1 ? eff.getPeriod() - eff.getTime() : eff.getPeriod());
					return effect;
				}
			}
		}
		return null;
	}
}