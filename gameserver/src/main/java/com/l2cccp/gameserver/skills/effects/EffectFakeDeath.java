package com.l2cccp.gameserver.skills.effects;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.network.l2.s2c.ChangeWaitType;
import com.l2cccp.gameserver.network.l2.s2c.Revive;
import com.l2cccp.gameserver.stats.Env;

public final class EffectFakeDeath extends EffectManaDamOverTime
{
	public static final int FAKE_DEATH_OFF = 0;
	public static final int FAKE_DEATH_ON = 1;
	public static final int FAKE_DEATH_FAILED = 2;

	public EffectFakeDeath(Env env, EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(!getEffected().isPlayer())
			return false;

		return super.checkCondition();
	}

	@Override
	public void onStart()
	{
		super.onStart();

		final Player player = (Player) getEffected();
		player.abortAttack(true, false);
		if(player.isMoving)
			player.stopMove();
		СlearAttackers();
		if(Rnd.chance(getTemplate().chance()))
		{
			player.setFakeDeath(FAKE_DEATH_ON);
			player.getAI().notifyEvent(CtrlEvent.EVT_FAKE_DEATH, null, null);
		}
		else
			player.setFakeDeath(FAKE_DEATH_FAILED);
		player.broadcastPacket(new ChangeWaitType(player, ChangeWaitType.WT_START_FAKEDEATH));
		player.broadcastCharInfo();
	}

	@Override
	public void onExit()
	{
		super.onExit();
		// 5 секунд после FakeDeath на персонажа не агрятся мобы
		final Player player = (Player) getEffected();
		player.setNonAggroTime(System.currentTimeMillis() + 5000L);
		player.setFakeDeath(FAKE_DEATH_OFF);
		player.broadcastPacket(new ChangeWaitType(player, ChangeWaitType.WT_STOP_FAKEDEATH));
		player.broadcastPacket(new Revive(player));
		player.broadcastCharInfo();
	}

	// Clear all attackers! 
	public void СlearAttackers()
	{
		Player player = (Player) getEffected();

		for(Creature creature : World.getAroundNpc(player, 2000, 1000))
		{

			creature.abortAttack(true, false);
			if(creature.getCastingTarget() != null && creature.getCastingTarget().equals(player))
				creature.abortCast(true, false);

		}
	}
}