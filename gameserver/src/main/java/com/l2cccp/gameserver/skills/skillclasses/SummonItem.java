package com.l2cccp.gameserver.skills.skillclasses;

import java.util.List;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.utils.ItemFunctions;


public class SummonItem extends Skill
{
	private final int _itemId;
	private final int _minId;
	private final int _maxId;
	private final long _minCount;
	private final long _maxCount;

	public SummonItem(final StatsSet set)
	{
		super(set);

		_itemId = set.getInteger("SummonItemId", 0);
		_minId = set.getInteger("SummonMinId", 0);
		_maxId = set.getInteger("SummonMaxId", _minId);
		_minCount = set.getLong("SummonMinCount");
		_maxCount = set.getLong("SummonMaxCount", _minCount);
	}

	@Override
	public void useSkill(SkillEntry skillEntry, final Creature activeChar, final List<Creature> targets)
	{
		if(!activeChar.isPlayable())
			return;
		for(Creature target : targets)
			if(target != null)
			{
				int itemId = _minId > 0 ? Rnd.get(_minId, _maxId) : _itemId;
				long count = _minCount == _maxCount ? _minCount : Rnd.get(_minCount, _maxCount);

				ItemFunctions.addItem((Playable)activeChar, itemId, count);
				getEffects(skillEntry, activeChar, target, true, false);
			}
	}
}