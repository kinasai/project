package com.l2cccp.gameserver.skills.effects;

import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.Inventory;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExAutoSoulShot;
import com.l2cccp.gameserver.network.l2.s2c.ExUseSharedGroupItem;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.skills.TimeStamp;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.tables.SkillTable;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class EffectAutoConsume extends Effect
{
	private final int _item;
	private final int _percent;
	private final int _skill;
	private final int _level;
	private final int _type;

	public EffectAutoConsume(Env env, EffectTemplate template)
	{
		super(env, template);
		_item = template.getParam().getInteger("consume");
		_percent = template.getParam().getInteger("percent");
		String[] info = template.getParam().getString("skill").split("-");
		_skill = Integer.parseInt(info[0]);
		_level = Integer.parseInt(info[1]);
		_type = template.getParam().getInteger("type");
	}

	@Override
	public boolean onActionTime()
	{
		if(!_effected.isPlayer())
			return false;

		final Player player = _effected.getPlayer();
		final Inventory inv = player.getInventory();
		final ItemInstance item = inv.getItemByItemId(_item);
		if(item == null || item.getCount() < 1 || player.isInOlympiadMode())
		{
			player.sendPacket(SystemMsg.INCORRECT_ITEM_COUNT);
			player.removeAutoConsume(_item);
			player.sendPacket(new ExAutoSoulShot(_item, false), new SystemMessage(SystemMsg.THE_AUTOMATIC_USE_OF_S1_HAS_BEEN_DEACTIVATED).addItemName(_item));
			return false;
		}

		double current = 100D;
		if(_type == 0)
			current = player.getCurrentCpPercents();
		else if(_type == 1)
			current = player.getCurrentHpPercents();
		else if(_type == 2)
			current = player.getCurrentMpPercents();

		if(current <= _percent && !player.isOutOfControl())
		{
			final TimeStamp reuse = player.getSharedGroupReuse(item.getTemplate().getReuseGroup());
			if(reuse == null || !reuse.hasNotPassed())
			{
				final SkillEntry skill = SkillTable.getInstance().getSkillEntry(_skill, _level);
				if(skill.checkCondition(player, player, true, false, true))
					player.altUseSkill(skill, player);

				final long next_use = System.currentTimeMillis() + skill.getTemplate().getReuseDelay();
				final TimeStamp timeStamp = new TimeStamp(item.getItemId(), next_use, item.getTemplate().getReuseDelay());
				player.addSharedGroupReuse(item.getTemplate().getReuseGroup(), timeStamp);

				if(next_use > 0)
					player.sendPacket(new ExUseSharedGroupItem(item.getTemplate().getDisplayReuseGroup(), timeStamp));
			}
		}

		if(!player.getAutoConsume().contains(_item))
		{
			player.addAutoConsume(_item);
			player.sendPacket(new ExAutoSoulShot(_item, true));
			player.sendPacket(new SystemMessage(SystemMsg.THE_AUTOMATIC_USE_OF_S1_HAS_BEEN_ACTIVATED).addItemName(_item));
		}

		return true;
	}
}