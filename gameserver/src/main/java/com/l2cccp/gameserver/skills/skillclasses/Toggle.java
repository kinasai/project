package com.l2cccp.gameserver.skills.skillclasses;

import java.util.List;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.templates.StatsSet;


public class Toggle extends Skill
{
	public Toggle(StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(SkillEntry skillEntry, Creature activeChar, List<Creature> targets)
	{
		if(activeChar.getEffectList().getEffectsBySkillId(_id) != null)
		{
			activeChar.getEffectList().stopEffect(_id);
			activeChar.sendActionFailed();
			return;
		}

		getEffects(skillEntry, activeChar, activeChar, false, false);
	}
}
