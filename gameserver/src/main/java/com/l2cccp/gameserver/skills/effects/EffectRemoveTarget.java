package com.l2cccp.gameserver.skills.effects;

import com.l2cccp.gameserver.ai.CtrlIntention;
import com.l2cccp.gameserver.ai.DefaultAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.stats.Env;

/**
 * 
 * @author nonam3
 * @date 08/01/2011 17:37
 *
 */
public final class EffectRemoveTarget extends Effect
{
	private boolean _doStopTarget;

	public EffectRemoveTarget(Env env, EffectTemplate template)
	{
		super(env, template);
		_doStopTarget = template.getParam().getBool("doStopTarget", true);
	}

	@Override
	public void onStart()
	{
		final Creature target = getEffected();
		if (target.getTarget() == null) // атака/каст прерывается только если есть таргет
			return;

		if(target.getAI() instanceof DefaultAI)
			((DefaultAI) target.getAI()).setGlobalAggro(System.currentTimeMillis() + 3000L);

		target.setTarget(null);
		target.abortCast(true, true);
		if(_doStopTarget)
			target.stopMove();

		target.abortAttack(true, true);
		target.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE, getEffector());
	}

	@Override
	public boolean isHidden()
	{
		return true;
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}