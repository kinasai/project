package com.l2cccp.gameserver.skills.effects;

import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.stats.Env;

public class EffectNegateMusic extends Effect
{
	public EffectNegateMusic(Env env, EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onExit()
	{
		super.onExit();
	}

	@Override
	public boolean onActionTime()
	{
		for(Effect e : _effected.getEffectList().getAllEffects())
			if(e.getSkill().getTemplate().isMusic())
				e.exit();
		return false;
	}
}