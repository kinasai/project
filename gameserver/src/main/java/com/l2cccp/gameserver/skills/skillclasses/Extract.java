package com.l2cccp.gameserver.skills.skillclasses;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.l2cccp.commons.math.random.RndSelector;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Announcements;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.reward.RewardList;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.utils.ItemFunctions;

/**
 * @author VISTALL
 * @date 17:21/31.08.2011
 */
public class Extract extends Skill
{
	public static class ExtractItem
	{
		private int _itemId;
		private int[] _itemsId;
		private int[] _counts;
		private final boolean _announce;
		private final boolean _random;

		public ExtractItem(int itemId, int[] counts, boolean announce)
		{
			_random = false;
			_itemId = itemId;
			_counts = counts;
			_announce = announce;
		}

		public ExtractItem(int[] itemsId, int[] counts, boolean announce)
		{
			_random = true;
			_itemsId = itemsId;
			_counts = counts;
			_announce = announce;
		}
	}

	public static class ExtractGroup extends ArrayList<ExtractItem>
	{
		private static final long serialVersionUID = -2124531921046325587L;

		private final double _chance;

		public ExtractGroup(double chance)
		{
			_chance = chance;
		}
	}

	private final RndSelector<ExtractGroup> _selector;
	private final boolean _isFish;

	@SuppressWarnings("unchecked")
	public Extract(StatsSet set)
	{
		super(set);
		List<ExtractGroup> extractGroupList = (List<ExtractGroup>) set.get("extractlist");
		if(extractGroupList == null)
			extractGroupList = Collections.emptyList();

		_selector = new RndSelector<ExtractGroup>(extractGroupList.size());

		for(ExtractGroup g : extractGroupList)
			_selector.add(g, (int) (g._chance * 10000));

		_isFish = set.getBool("isFish", false);
	}

	@Override
	public void useSkill(SkillEntry skillEntry, Creature activeChar, List<Creature> targets)
	{
		for(Creature target : targets)
		{
			Player targetPlayer = target.getPlayer();
			if(targetPlayer == null)
				return;

			ExtractGroup extractGroup = _selector.chance(RewardList.MAX_CHANCE);
			if(extractGroup != null)
			{
				for(ExtractItem item : extractGroup)
				{
					if(item._random)
					{
						final int[] items = item._itemsId;
						final int count = Rnd.get(item._counts[0], item._counts[1]);
						ItemFunctions.addItem(targetPlayer, items[Rnd.get(0, items.length - 1)], _isFish ? (long) (count * Config.RATE_FISH_DROP_COUNT) : count);
						if(item._announce)
							Announcements.getInstance().announceToAll("Player " + targetPlayer.getName() + " get " + ItemHolder.getInstance().getTemplate(item._itemId).getName() + " from box!", "Extract");
					}
					else
					{
						final int count = Rnd.get(item._counts[0], item._counts[1]);
						ItemFunctions.addItem(targetPlayer, item._itemId, _isFish ? (long) (count * Config.RATE_FISH_DROP_COUNT) : count);
						if(item._announce)
							Announcements.getInstance().announceToAll("Player " + targetPlayer.getName() + " get " + ItemHolder.getInstance().getTemplate(item._itemId).getName() + " from box!", "Extract");
					}
				}
			}
			else
				targetPlayer.sendPacket(SystemMsg.THERE_WAS_NOTHING_FOUND_INSIDE);
		}
	}
}
