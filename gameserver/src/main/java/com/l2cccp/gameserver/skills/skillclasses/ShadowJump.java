package com.l2cccp.gameserver.skills.skillclasses;

import java.util.List;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.ai.CtrlIntention;
import com.l2cccp.gameserver.geodata.GeoEngine;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.FlyToLocation;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.network.l2.s2c.ValidateLocation;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.PositionUtils;

public class ShadowJump extends Skill
{

	public ShadowJump(StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(SkillEntry skillEntry, Creature activeChar, List<Creature> targets)
	{
		if(targets.size() == 0)
			return;

		Creature target = targets.get(0);
		if(Rnd.chance(target.calcStat(Stats.PSKILL_EVASION, 0, activeChar, skillEntry)))
		{
			if(activeChar.isPlayer())
				activeChar.sendPacket(new SystemMessage(SystemMsg.C1_DODGES_THE_ATTACK).addName(target));
			if(target.isPlayer())
				target.sendPacket(new SystemMessage(SystemMsg.C1_HAS_EVADED_C2S_ATTACK).addName(target).addName(activeChar));
			return;
		}
		int x, y, z;

		int px = target.getX();
		int py = target.getY();
		double ph = PositionUtils.convertHeadingToDegree(target.getHeading());

		ph += 180;

		if(ph > 360)
			ph -= 360;

		ph = (Math.PI * ph) / 180;

		x = (int) (px + (25 * Math.cos(ph)));
		y = (int) (py + (25 * Math.sin(ph)));
		z = target.getZ();

		Location loc = new Location(x, y, z);

		if(Config.ALLOW_GEODATA)
			loc = GeoEngine.moveCheck(activeChar.getX(), activeChar.getY(), activeChar.getZ(), x, y, activeChar.getReflection().getGeoIndex());

		if(target.isNpc())
		{
			NpcInstance npc = (NpcInstance) target;
			npc.abortAttack(true, false);
			npc.abortCast(true, false);
			npc.getAI().notifyEvent(CtrlEvent.EVT_THINK);
		}
		else
		{
			target.setTarget(null);
			target.abortAttack(true, true);
			target.abortCast(true, true);
		}

		activeChar.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
		activeChar.broadcastPacket(new FlyToLocation(activeChar, loc, FlyToLocation.FlyType.DUMMY));
		activeChar.abortAttack(true, false);
		activeChar.abortCast(true, false);
		activeChar.setXYZ(loc.x, loc.y, loc.z);
		activeChar.setHeading(target.getHeading());
		activeChar.broadcastPacket(new ValidateLocation(activeChar));
	}
}