package com.l2cccp.gameserver.skills.effects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.stats.Env;

/**
 * @author L2CCCP
 */
public class EffectCleanse extends Effect
{
	private final int _count;
	private final int _chance;

	public EffectCleanse(Env env, EffectTemplate template)
	{
		super(env, template);
		_count = template.getParam().getInteger("count", 10);
		_chance = template.getParam().getInteger("chance", 100);
	}

	@Override
	public void onStart()
	{
		List<Effect> _buffList = new ArrayList<Effect>();

		for(Effect effect : _effected.getEffectList().getAllEffects())
		{
			SkillEntry skill = effect.getSkill();
			if(effect.isOffensive() && effect.isCancelable() && cleanse(skill.getId()))
				_buffList.add(effect);
		}

		List<Effect> _effectList = new ArrayList<Effect>();
		Collections.reverse(_buffList);
		_effectList.addAll(_buffList);

		if(_effectList.isEmpty())
			return;

		int clear = 0;

		for(Effect effect : _effectList)
		{
			if(clear >= _count)
				break;

			if(Rnd.chance(_chance))
			{
				effect.exit();
				if(_effected.isPlayable())
					_effected.sendPacket(new SystemMessage(SystemMsg.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(effect.getSkill().getDisplayId(), effect.getSkill().getDisplayLevel()));
				clear++;
			}
		}
	}

	private boolean cleanse(int id)
	{
		switch(id)
		{
			case 2530:
			case 4215:
			case 4515:
			case 5660:
				return false;
			default:
				return true;
		}
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}

	@Override
	public void onExit()
	{
		super.onExit();
	}
}