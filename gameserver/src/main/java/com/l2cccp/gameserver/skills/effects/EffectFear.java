package com.l2cccp.gameserver.skills.effects;

import com.l2cccp.gameserver.ai.CtrlIntention;
import com.l2cccp.gameserver.geodata.GeoEngine;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.impl.SiegeEvent;
import com.l2cccp.gameserver.model.instances.SummonInstance;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.utils.PositionUtils;

public final class EffectFear extends Effect
{
	public static final double FEAR_RANGE = 900;

	public EffectFear(Env env, EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public boolean checkCondition()
	{
		if(_effected.isFearImmune())
		{
			getEffector().sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
			return false;
		}

		// Fear нельзя наложить на осадных саммонов
		Player player = _effected.getPlayer();
		if(player != null)
		{
			SiegeEvent<?, ?> siegeEvent = player.getEvent(SiegeEvent.class);
			if(_effected.isSummon() && siegeEvent != null && siegeEvent.containsSiegeSummon((SummonInstance) _effected))
			{
				getEffector().sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
				return false;
			}
		}

		if(_effected.isInZonePeace())
		{
			getEffector().sendPacket(SystemMsg.YOU_MAY_NOT_ATTACK_IN_A_PEACEFUL_ZONE);
			return false;
		}

		return super.checkCondition();
	}

	@Override
	public void onStart()
	{
		super.onStart();

		if(!_effected.startFear())
		{
			_effected.abortAttack(true, true);
			_effected.abortCast(true, true);
			_effected.stopMove();
		}

		onActionTime();
	}

	@Override
	public void onExit()
	{
		super.onExit();
		_effected.stopFear();
		if(!_effected.isDead() && !_effected.isSummon())
			_effected.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
	}

	@Override
	public boolean onActionTime()
	{
		if(_effected.isDead() && !_effected.isBlessedByNoblesse() && !_effected.isBlessed())
			return false;
		else if(!_effected.isDead())
		{
			final double angle = Math.toRadians(PositionUtils.calculateAngleFrom(_effector, _effected));
			final int oldX = _effected.getX();
			final int oldY = _effected.getY();
			final int x = oldX + (int) (FEAR_RANGE * Math.cos(angle));
			final int y = oldY + (int) (FEAR_RANGE * Math.sin(angle));
			_effected.setRunning();
			_effected.moveToLocation(GeoEngine.moveCheck(oldX, oldY, _effected.getZ(), x, y, _effected.getGeoIndex()), 0, false);
		}
		return true;
	}
}