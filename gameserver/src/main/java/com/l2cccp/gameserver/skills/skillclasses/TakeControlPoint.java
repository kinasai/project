package com.l2cccp.gameserver.skills.skillclasses;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.entity.events.impl.KingHillEvent;
import com.l2cccp.gameserver.model.entity.events.impl.TerritoryWarsEvent;
import com.l2cccp.gameserver.model.entity.events.objects.HillControlSpotObject;
import com.l2cccp.gameserver.model.entity.events.objects.TWControlSpotObject;
import com.l2cccp.gameserver.model.instances.HillControlSpotInstance;
import com.l2cccp.gameserver.model.instances.TWControlSpotInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.templates.StatsSet;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TakeControlPoint extends Skill
{
	protected final Logger _log = LoggerFactory.getLogger(getClass());
	private final String _event;

	public TakeControlPoint(StatsSet set)
	{
		super(set);
		_event = set.getString("event");
	}

	@Override
	public void useSkill(SkillEntry skillEntry, Creature activeChar, List<Creature> targets)
	{
		if(_event.equalsIgnoreCase("TerritoryWar"))
		{
			TerritoryWarsEvent event = activeChar.getEvent(TerritoryWarsEvent.class);
			if(event == null)
				return;

			for(Creature target : targets)
			{
				if(target instanceof TWControlSpotInstance)
				{
					TWControlSpotInstance npc = (TWControlSpotInstance) target;
					TWControlSpotObject object = npc.getControlSpotObject();
					object.changeOwnerTeam(activeChar.getTeam(), event);
				}
			}
		}
		else if(_event.equalsIgnoreCase("KingHill"))
		{
			KingHillEvent event = activeChar.getEvent(KingHillEvent.class);
			if(event == null)
				return;

			for(Creature target : targets)
			{
				if(target instanceof HillControlSpotInstance)
				{
					HillControlSpotInstance npc = (HillControlSpotInstance) target;
					HillControlSpotObject object = npc.getControlSpotObject();
					object.changeOwnerTeam(activeChar.getTeamId(), event);
				}
			}
		}
	}
}