package com.l2cccp.gameserver.skills.skillclasses;

import java.util.List;

import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.templates.StatsSet;

public class Aggression extends Skill
{
	private final boolean _unaggring;
	private final boolean _silent;

	public Aggression(StatsSet set)
	{
		super(set);
		_unaggring = set.getBool("unaggroing", false);
		_silent = set.getBool("silent", false);
	}

	@Override
	public void useSkill(SkillEntry skillEntry, Creature activeChar, List<Creature> targets)
	{
		int effect = _effectPoint;

		if(isSSPossible() && (activeChar.getChargedSoulShot() || activeChar.getChargedSpiritShot(false) > 0))
			effect *= 2;

		for(Creature target : targets)
			if(target != null)
			{
				if(!target.isAutoAttackable(activeChar))
					continue;
				if(target.isNpc())
					if(_unaggring)
					{
						if(target.isNpc() && activeChar.isPlayable())
							((NpcInstance) target).getAggroList().addDamageHate(activeChar, 0, -effect);
					}
					else
					{
						target.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, activeChar, effect);
						if(!_silent)
							target.getAI().notifyEvent(CtrlEvent.EVT_ATTACKED, activeChar, skillEntry, 0);
					}
				else if(target.isPlayable() && !target.isDebuffImmune())
					target.setTarget(activeChar);
				getEffects(skillEntry, activeChar, target, true, false);
			}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}