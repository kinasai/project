package com.l2cccp.gameserver.skills.effects;

import java.util.StringTokenizer;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.items.Restoration;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.utils.ItemFunctions;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class i_restoration_chance extends Effect
{
	private final Restoration[] extract;
	private static final String delim = "{}[]:";

	public i_restoration_chance(Env env, EffectTemplate template)
	{
		super(env, template);
		extract = add(template.getParam().getString("extract", ""));
	}

	private Restoration[] add(String string)
	{
		String[] data = string.split(";");
		final int size = data.length;
		Restoration[] array = new Restoration[size];
		for(int i = 0; i <= size - 1; i++)
		{
			StringTokenizer st = new StringTokenizer(data[i], delim);
			final int id = Integer.parseInt(st.nextToken());
			final long count = Integer.parseInt(st.nextToken());
			final double chance = Double.parseDouble(st.nextToken());
			final Restoration restoration = new Restoration(id, count);
			restoration.setChance(chance);
			array[i] = restoration;
		}

		return array;
	}

	@Override
	public void onStart()
	{
		super.onStart();

		for(Restoration item : extract)
		{
			if(Rnd.chance(item.getChance()))
				ItemFunctions.addItem(_effected.getPlayer(), item.getId(), item.getCount());
		}
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}