package com.l2cccp.gameserver.skills.skillclasses;

import java.util.List;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.templates.StatsSet;

public class VitalityHeal extends Skill
{
	public VitalityHeal(StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(SkillEntry skillEntry, Creature activeChar, List<Creature> targets)
	{
		int fullPoints = Config.VITALITY_MAX_POINTS;
		double percent = _power;

		for(Creature target : targets)
		{
			if(target.isPlayer())
			{
				Player player = target.getPlayer();
				double points = fullPoints / 100 * percent;
				player.getVitality().add((int) points);
			}

			getEffects(skillEntry, activeChar, target, true, false);
		}

		if(isSSPossible())
			activeChar.unChargeShots(isMagic());
	}
}