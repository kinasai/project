package com.l2cccp.gameserver.skills.effects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.EffectType;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;

/**
 * @author L2CCCP
 */
public class EffectCancel extends Effect
{
	private final String _type;
	private final int _min;
	private final int _max;
	private final int _chance;
	private final String[] _stacks;
	private int _count;

	public EffectCancel(Env env, EffectTemplate template)
	{
		super(env, template);
		_chance = template.getParam().getInteger("chance", 0);
		_min = template.getParam().getInteger("min", 10);
		_max = template.getParam().getInteger("max", 90);
		_count = template.getParam().getInteger("count", 5);
		final String st = template.getParam().getString("stacks", null);
		_stacks = st != null ? st.split(";") : null;
		_type = template.getParam().getString("type", "");
	}

	@Override
	public void onStart()
	{
		List<Effect> _triggerList = new ArrayList<Effect>();
		List<Effect> _musicList = new ArrayList<Effect>();
		List<Effect> _buffList = new ArrayList<Effect>();

		double cancel_res = _effected.calcStat(Stats.CANCEL_RESIST, 0, null, null);

		if(Config.CANCEL_IMMUNITY && cancel_res >= 100) // Если резиста от кенсела 100% то идти дальше смысла нету.
			return;

		for(Effect effect : _effected.getEffectList().getAllEffects())
		{
			SkillEntry skill = effect.getSkill();
			if(_type.equals("CANCEL"))
			{
				if(!effect.isOffensive() && !effect.getStackType().equals("Hourglass") && effect.isCancelable() && !skill.getTemplate().isPassive() && effect.getEffectType() != EffectType.Vitality)
				{
					if(skill.getTemplate().isTrigger()) // В ХФ триггеры снимаются первыми!
						_triggerList.add(effect);
					else if(skill.getTemplate().isMusic())
						_musicList.add(effect);
					else
						_buffList.add(effect);
				}
			}
			else if(_type.equals("BANE"))
			{
				if(!effect.isOffensive() && (ArrayUtils.contains(_stacks, effect.getStackType()) || ArrayUtils.contains(_stacks, effect.getStackType2())) && effect.isCancelable())
					_buffList.add(effect);
			}
		}

		List<Effect> _effectList = new ArrayList<Effect>();
		Collections.reverse(_triggerList);
		Collections.reverse(_musicList);
		Collections.reverse(_buffList);
		_effectList.addAll(_triggerList);
		_effectList.addAll(_musicList);
		_effectList.addAll(_buffList);

		if(_effectList.isEmpty())
			return;

		int count = 0;
		int exit = 0;

		final int first = Config.CANCEL_COUNT_SIZE[0];
		final int second = Math.max(1, Math.min(Config.CANCEL_COUNT_SIZE[1], _count));

		_count = Rnd.get(Math.min(first, second), Math.max(first, second));

		for(Effect effect : _effectList)
		{
			if(exit >= _count || (count * Config.CANCEL_COUNT_LIMIT) >= _count) // Небольшей мод для разброса баффов!
				break;

			count++;
			if(calcChance(_effected, _effector, effect, cancel_res))
			{
				if(_effected.isPlayable())
					_effected.sendPacket(new SystemMessage(SystemMsg.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(effect.getSkill().getDisplayId(), effect.getSkill().getDisplayLevel()));
				effect.exit();
				exit++;
			}
		}
	}

	private boolean calcChance(Creature effected, Creature effector, Effect eff, double cancel_resist)
	{
		double chance = 0, eml, dml;
		long time;

		SkillEntry skill = eff.getSkill();
		eml = skill.getTemplate().getMagicLevel();
		dml = getSkill().getTemplate().getMagicLevel() - (eml == 0 ? effected.getLevel() : eml);

		Effect ef = effected.getEffectList().getEffectByIndexAndType(skill.getId(), eff.getEffectType());
		if(ef != null)
		{
			time = ef.getTimeLeft();
			cancel_resist = 1 - (cancel_resist * .01);
			chance = (2. * dml + _chance + time / 120000) * cancel_resist; // retail formula
		}

		chance = Math.max(Math.min(chance, _max), _min);

		return Rnd.chance(chance);
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}

	@Override
	public void onExit()
	{
		super.onExit();
	}
}