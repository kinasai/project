package com.l2cccp.gameserver.skills.skillclasses;

import java.util.List;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.stats.Formulas;
import com.l2cccp.gameserver.templates.StatsSet;


public class ManaDam extends Skill
{
	public ManaDam(StatsSet set)
	{
		super(set);
	}

	@Override
	public void useSkill(SkillEntry skillEntry, Creature activeChar, List<Creature> targets)
	{
		int sps = 0;

		if (isSSPossible())
		{
			switch (activeChar.getChargedSpiritShot(false))
			{
			case ItemInstance.CHARGED_BLESSED_SPIRITSHOT:
				sps = 2;
				break;
			case ItemInstance.CHARGED_SPIRITSHOT:
				sps = 1;
				break;
			}
		}

		for(Creature target : targets)
			if(target != null)
			{
				if(target.isDead())
					continue;

				double damage = Formulas.calcMagicDam(activeChar, target, skillEntry, sps, true);
				if(damage >= 1)
					target.reduceCurrentMp(damage, activeChar);

				getEffects(skillEntry, activeChar, target, true, false);
			}

		if(isSSPossible() && isMagic())
			activeChar.unChargeShots(isMagic());
	}
}