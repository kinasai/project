package com.l2cccp.gameserver.skills.skillclasses;

import java.util.List;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ai.CtrlIntention;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.templates.StatsSet;


public class TameControl extends Skill
{
	public static interface TameControlTarget
	{
		void doCastBuffs();

		void free();
	}

	private final int _type;

	public TameControl(StatsSet set)
	{
		super(set);
		_type = set.getInteger("type", 0);
	}

	@Override
	public void useSkill(SkillEntry skillEntry, Creature activeChar, List<Creature> targets)
	{
		if(isSSPossible())
			activeChar.unChargeShots(isMagic());

		if(!activeChar.isPlayer())
			return;

		Player player = activeChar.getPlayer();
		switch(_type)
		{
			case 0:
				for(Creature target : targets)
					if(target instanceof TameControlTarget)
						((TameControlTarget) target).free();
				break;
			case 1:
				for(NpcInstance npc : player.getTamedBeasts())
					if(npc instanceof TameControlTarget)
						npc.getAI().setIntention(CtrlIntention.AI_INTENTION_FOLLOW, player, Config.FOLLOW_RANGE);
				break;
			case 3:
				for(NpcInstance npc : player.getTamedBeasts())
					if(npc instanceof TameControlTarget)
						((TameControlTarget) npc).doCastBuffs();
				break;
			case 4:
				for(NpcInstance npc : player.getTamedBeasts())
					if(npc instanceof TameControlTarget)
						((TameControlTarget) npc).free();
				break;
		}
	}
}