package com.l2cccp.gameserver.skills.effects;

import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.skills.skillclasses.NegateStats;
import com.l2cccp.gameserver.stats.Env;

public class EffectBlockStat extends Effect
{
	public EffectBlockStat(Env env, EffectTemplate template)
	{
		super(env, template);
	}

	@Override
	public void onStart()
	{
		super.onStart();
		_effected.addBlockStats(((NegateStats) _skill.getTemplate()).getNegateStats());
	}

	@Override
	public void onExit()
	{
		super.onExit();
		_effected.removeBlockStats(((NegateStats) _skill.getTemplate()).getNegateStats());
	}

	@Override
	public boolean onActionTime()
	{
		return false;
	}
}