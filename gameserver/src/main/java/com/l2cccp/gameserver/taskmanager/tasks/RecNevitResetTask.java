package com.l2cccp.gameserver.taskmanager.tasks;

import com.l2cccp.commons.time.cron.SchedulingPattern;
import com.l2cccp.gameserver.data.xml.holder.RecommendsHolder;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;

/**
 * @author VISTALL
 * @date 5:56/12.08.2011
 */
public class RecNevitResetTask extends AutomaticTask
{
	public static final SchedulingPattern PATTERN = new SchedulingPattern("30 6 * * *");

	@Override
	public void doTask() throws Exception
	{
		long t = System.currentTimeMillis();
		_log.info("Recommendation Global Task: launched.");
		for(Player player : GameObjectsStorage.getPlayers())
		{
			player.getRecommendSystem().restart();
			player.getNevitSystem().restartSystem();
		}
		_log.info("Recommendation Global Task: done in " + (System.currentTimeMillis() - t) + " ms.");
		RecommendsHolder.getInstance().recalc();
	}

	@Override
	public long reCalcTime(boolean start)
	{
		return PATTERN.next(System.currentTimeMillis());
	}
}
