package com.l2cccp.gameserver.mod.DailyTasks.Impl;

public class PvP {
	private long count;
	
	public PvP(final long count) {
		this.count = count;
	}
	
	public long getCount() {
		return count;
	}
}