package com.l2cccp.gameserver.mod.DailyTasks;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ScheduledFuture;

import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.dao.CharacterVariablesDAO;
import com.l2cccp.gameserver.data.xml.holder.DailyTaskHolder;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Craft;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Farm;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Level;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Monster;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Pk;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.PvP;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Reward;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Spoil;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Trigger;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.mail.Mail;
import com.l2cccp.gameserver.network.l2.s2c.ExNoticePostArrived;
import com.l2cccp.gameserver.utils.ItemFunctions;


public class DailyTaskEngine {
	private final static Logger LOGGER = LoggerFactory.getLogger(DailyTaskEngine.class);
	private static ScheduledFuture<?> _refreshTask;
    private static DailyTaskEngine INSTANCE;
    public static DailyTaskEngine getInstance() {
        if (INSTANCE == null) {
        	INSTANCE = new DailyTaskEngine();
        }
        return INSTANCE;
    }

	public static void getPlayerTask(final Player player) {
		if(!Config.DAILY_TASKS_ENABLE) {
			return;
		}
		if(player == null) {
			return;
		}
		
		if(isRequestComplete(player)) { //если игрок уже выполнял таск до сброса - ничего не делаем
			return;
		}
		
		if(isRequestAccepted(player)) { //если у игрока есть активный таск, отправляем напоминание в виде мигающего значка
            player.sendPacket(new ExNoticePostArrived(0));
			return;
		}
		
		final List<DailyTask> avalibleTasks = new ArrayList<>();
		for(final DailyTask task : DailyTaskHolder.getInstance().getTasks()) { //формируем список доступных тасков
			if(task.getTrigger() == Trigger.spoil && ((!player.getKnownSkillB(42) && !player.getKnownSkillB(444)) || (!player.getKnownSkillB(254) && !player.getKnownSkillB(302)))) {
				continue;
			}
			if(getAvalibleLevel(task.getLevels(), player.getLevel()) != null) {
				avalibleTasks.add(task);
			}
		}
		
		if(avalibleTasks.size() == 0 || avalibleTasks == null) { //если доступных тасков нет - шлем сообщение
			if(player.isLangRus()) {
				player.sendMessage("На текущем уровне нет доступных ежедневных заданий.");				
			} else {
				player.sendMessage("There are no daily tasks available at the current level.");
			}
			return;
		}
		
		final int[] triggers = new int[avalibleTasks.size()];
		int i = 0;
		for(final DailyTask task : avalibleTasks) { //формируем список триггеров доступных тасков
			triggers[i] = task.getTrigger().ordinal();
			if(i < avalibleTasks.size()) {
				i++;
			}
		}
		
		final DailyTask task = DailyTaskHolder.getInstance().getTask(Trigger.values()[triggers[Rnd.get(0, triggers.length - 1)]]); //получаем таск по триггеру
		
		if(task == null) {
			return;
		}
		
		final Level lvl = getAvalibleLevel(task.getLevels(), player.getLevel()); //получаем допустимую задачу для уровня игрока
		
		if(lvl != null) { //на всякий случай проверяем на нулл
        	double rateExp = player.getRate().getExp(); //формируем почтовое сообщение и создаем игроку активный таск
        	double rateSp = player.getRate().getSp();
			String senderNameRu = task.getSenderNameRu();
			String senderNameEn = task.getSenderNameEn();
			String topicNameRu = task.getTopicNameRu();
			String topicNameEn = task.getTopicNameEn();
			String bodyRu = task.getBodyRu();
			String bodyEn = task.getBodyEn();
			
			if(task.getTrigger() == Trigger.monster) {
				final List<Monster> mobs = lvl.getMonsterList();
				final Monster mob = Rnd.get(mobs);
				final List<Reward> rewards = lvl.getRewardList();
				long rewardExp = lvl.getRewardExp();
				long rewardSp = lvl.getRewardSp();
				if(lvl.isEnableRates()) {
					rewardExp = (long) (rewardExp * rateExp);
					rewardSp = (long) (rewardSp * rateSp);
				}
				final long count = Rnd.get(mob.getMinCount(), mob.getMaxCount());
				if(lvl.isPerUnit()) {
					rewardExp = rewardExp * count;
					rewardSp = rewardSp * count;
				}
				bodyRu = bodyRu.replaceAll("%monsterCount%", String.valueOf(count));
				bodyEn = bodyEn.replaceAll("%monsterCount%", String.valueOf(count));
				bodyRu = bodyRu.replaceAll("%monsterName%", NpcHolder.getInstance().getTemplate(mob.getId()).getName() + " " + NpcHolder.getInstance().getTemplate(mob.getId()).level + " уровня");
				bodyEn = bodyEn.replaceAll("%monsterName%", NpcHolder.getInstance().getTemplate(mob.getId()).getName() + " level " + NpcHolder.getInstance().getTemplate(mob.getId()).level);
				bodyRu = bodyRu.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyEn = bodyEn.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyRu = bodyRu.replaceAll("%sp%", String.valueOf(rewardSp));
				bodyEn = bodyEn.replaceAll("%sp%", String.valueOf(rewardSp));
				bodyRu = bodyRu.replaceAll("%party%", lvl.isPartyAll() ? " При убийстве мобов в пати, если у сопартийца имеется такое же задание, фраги сопартийца будут засчитаны Вам, а Ваши фраги ему." : "");
				bodyEn = bodyEn.replaceAll("%party%", lvl.isPartyAll() ? " When killing mobs in a party, if the party members have the same task, the kills party members will be credited to You and Your frags him." : "");
				String replace = "";
				for(Reward reward : rewards) {
					long rewardCount = reward.getCount();
					if(reward.isEnableRates()) {
						rewardCount = (long) (rewardCount * rateExp);
					}
					if(reward.isPerUnit()) {
						rewardCount = rewardCount * count;
					}
					replace += ", " + rewardCount + " " + ItemHolder.getInstance().getTemplate(reward.getId()).getName();
				}
				bodyRu = bodyRu.replaceAll("%rewardList%", replace);
				bodyEn = bodyEn.replaceAll("%rewardList%", replace);
				player.setVar("@daily_request_accepted", "monster_" + player.getLevel() + "_" + mob.getId() + "_" + count + "_" + count, -1);
	        } else if(task.getTrigger() == Trigger.pvp) {
				final List<PvP> pvps = lvl.getPvpList();
				final PvP pvp = Rnd.get(pvps);
				final List<Reward> rewards = lvl.getRewardList();
				long rewardExp = lvl.getRewardExp();
				long rewardSp = lvl.getRewardSp();
				if(lvl.isEnableRates()) {
					rewardExp = (long) (rewardExp * rateExp);
					rewardSp = (long) (rewardSp * rateSp);
				}
				final long count = pvp.getCount();
				if(lvl.isPerUnit()) {
					rewardExp = rewardExp * count;
					rewardSp = rewardSp * count;
				}
				bodyRu = bodyRu.replaceAll("%pvpCount%", String.valueOf(count));
				bodyEn = bodyEn.replaceAll("%pvpCount%", String.valueOf(count));
				bodyRu = bodyRu.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyEn = bodyEn.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyRu = bodyRu.replaceAll("%sp%", String.valueOf(rewardSp));
				bodyEn = bodyEn.replaceAll("%sp%", String.valueOf(rewardSp));
				String replace = "";
				for(Reward reward : rewards) {
					long rewardCount = reward.getCount();
					if(reward.isEnableRates()) {
						rewardCount = (long)(rewardCount * rateExp);
					}
					if(reward.isPerUnit()) {
						rewardCount = rewardCount * count;
					}
					replace += ", " + rewardCount + " " + ItemHolder.getInstance().getTemplate(reward.getId()).getName();
				}
				bodyRu = bodyRu.replaceAll("%rewardList%", replace);
				bodyEn = bodyEn.replaceAll("%rewardList%", replace);
				player.setVar("@daily_request_accepted", "pvp_" + player.getLevel() + "_" + count + "_" + count, -1);
			} else if(task.getTrigger() == Trigger.pk) {
				final List<Pk> pks = lvl.getPkList();
				final Pk pk = Rnd.get(pks);
				final List<Reward> rewards = lvl.getRewardList();
				long rewardExp = lvl.getRewardExp();
				long rewardSp = lvl.getRewardSp();
				if(lvl.isEnableRates()) {
					rewardExp = (long) (rewardExp * rateExp);
					rewardSp = (long) (rewardSp * rateSp);
				}
				final long count = pk.getCount();
				if(lvl.isPerUnit()) {
					rewardExp = rewardExp * count;
					rewardSp = rewardSp * count;
				}
				bodyRu = bodyRu.replaceAll("%pkCount%", String.valueOf(count));
				bodyEn = bodyEn.replaceAll("%pkCount%", String.valueOf(count));
				bodyRu = bodyRu.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyEn = bodyEn.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyRu = bodyRu.replaceAll("%sp%", String.valueOf(rewardSp));
				bodyEn = bodyEn.replaceAll("%sp%", String.valueOf(rewardSp));
				String replace = "";
				for(Reward reward : rewards) {
					long rewardCount = reward.getCount();
					if(reward.isEnableRates()) {
						rewardCount = (long)(rewardCount * rateExp);
					}
					if(reward.isPerUnit()) {
						rewardCount = rewardCount * count;
					}
					replace += ", " + rewardCount + " " + ItemHolder.getInstance().getTemplate(reward.getId()).getName();
				}
				bodyRu = bodyRu.replaceAll("%rewardList%", replace);
				bodyEn = bodyEn.replaceAll("%rewardList%", replace);
				player.setVar("@daily_request_accepted", "pk_" + player.getLevel() + "_" + count + "_" + count, -1);
			} else if(task.getTrigger() == Trigger.craft) {
				final List<Craft> crafts = lvl.getCraftList();
				final Craft craft = Rnd.get(crafts);
				final List<Reward> rewards = lvl.getRewardList();
				long rewardExp = lvl.getRewardExp();
				long rewardSp = lvl.getRewardSp();
				if(lvl.isEnableRates()) {
					rewardExp = (long) (rewardExp * rateExp);
					rewardSp = (long) (rewardSp * rateSp);
				}
				final long count = Rnd.get(craft.getMinCount(), craft.getMaxCount());
				if(lvl.isPerUnit()) {
					rewardExp = rewardExp * count;
					rewardSp = rewardSp * count;
				}
				bodyRu = bodyRu.replaceAll("%craftCount%", String.valueOf(count));
				bodyEn = bodyEn.replaceAll("%craftCount%", String.valueOf(count));
				bodyRu = bodyRu.replaceAll("%itemName%", ItemHolder.getInstance().getTemplate(craft.getId()).getName());
				bodyEn = bodyEn.replaceAll("%itemName%", ItemHolder.getInstance().getTemplate(craft.getId()).getName());
				bodyRu = bodyRu.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyEn = bodyEn.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyRu = bodyRu.replaceAll("%sp%", String.valueOf(rewardSp));
				bodyEn = bodyEn.replaceAll("%sp%", String.valueOf(rewardSp));
				String replace = "";
				for(Reward reward : rewards) {
					long rewardCount = reward.getCount();
					if(reward.isEnableRates()) {
						rewardCount = (long)(rewardCount * rateExp);
					}
					if(reward.isPerUnit()) {
						rewardCount = rewardCount * count;
					}
					replace += ", " + rewardCount + " " + ItemHolder.getInstance().getTemplate(reward.getId()).getName();
				}
				bodyRu = bodyRu.replaceAll("%rewardList%", replace);
				bodyEn = bodyEn.replaceAll("%rewardList%", replace);
				player.setVar("@daily_request_accepted", "craft_" + player.getLevel() + "_" + craft.getId() + "_" + count + "_" + count, -1);
			} else if(task.getTrigger() == Trigger.farm) {
				final List<Farm> farms = lvl.getFarmList();
				final Farm farm = Rnd.get(farms);
				final List<Reward> rewards = lvl.getRewardList();
				long rewardExp = lvl.getRewardExp();
				long rewardSp = lvl.getRewardSp();
				if(lvl.isEnableRates()) {
					rewardExp = (long) (rewardExp * rateExp);
					rewardSp = (long) (rewardSp * rateSp);
				}
				final long count = Rnd.get(farm.getMinCount(), farm.getMaxCount());
				if(lvl.isPerUnit()) {
					rewardExp = rewardExp * count;
					rewardSp = rewardSp * count;
				}
				bodyRu = bodyRu.replaceAll("%farmCount%", String.valueOf(count));
				bodyEn = bodyEn.replaceAll("%farmCount%", String.valueOf(count));
				bodyRu = bodyRu.replaceAll("%itemName%", ItemHolder.getInstance().getTemplate(farm.getId()).getName());
				bodyEn = bodyEn.replaceAll("%itemName%", ItemHolder.getInstance().getTemplate(farm.getId()).getName());
				bodyRu = bodyRu.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyEn = bodyEn.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyRu = bodyRu.replaceAll("%sp%", String.valueOf(rewardSp));
				bodyEn = bodyEn.replaceAll("%sp%", String.valueOf(rewardSp));
				String replace = "";
				for(Reward reward : rewards) {
					long rewardCount = reward.getCount();
					if(reward.isEnableRates()) {
						rewardCount = (long)(rewardCount * rateExp);
					}
					if(reward.isPerUnit()) {
						rewardCount = rewardCount * count;
					}
					replace += ", " + rewardCount + " " + ItemHolder.getInstance().getTemplate(reward.getId()).getName();
				}
				bodyRu = bodyRu.replaceAll("%rewardList%", replace);
				bodyEn = bodyEn.replaceAll("%rewardList%", replace);
				player.setVar("@daily_request_accepted", "farm_" + player.getLevel() + "_" + farm.getId() + "_" + count + "_" + count, -1);
			} else if(task.getTrigger() == Trigger.spoil) {
				final List<Spoil> spoils = lvl.getSpoilList();
				final Spoil spoil = Rnd.get(spoils);
				final List<Reward> rewards = lvl.getRewardList();
				long rewardExp = lvl.getRewardExp();
				long rewardSp = lvl.getRewardSp();
				if(lvl.isEnableRates()) {
					rewardExp = (long) (rewardExp * rateExp);
					rewardSp = (long) (rewardSp * rateSp);
				}
				final long count = Rnd.get(spoil.getMinCount(), spoil.getMaxCount());
				if(lvl.isPerUnit()) {
					rewardExp = rewardExp * count;
					rewardSp = rewardSp * count;
				}
				bodyRu = bodyRu.replaceAll("%spoilCount%", String.valueOf(count));
				bodyEn = bodyEn.replaceAll("%spoilCount%", String.valueOf(count));
				bodyRu = bodyRu.replaceAll("%itemName%", ItemHolder.getInstance().getTemplate(spoil.getId()).getName());
				bodyEn = bodyEn.replaceAll("%itemName%", ItemHolder.getInstance().getTemplate(spoil.getId()).getName());
				bodyRu = bodyRu.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyEn = bodyEn.replaceAll("%exp%", String.valueOf(rewardExp));
				bodyRu = bodyRu.replaceAll("%sp%", String.valueOf(rewardSp));
				bodyEn = bodyEn.replaceAll("%sp%", String.valueOf(rewardSp));
				String replace = "";
				for(Reward reward : rewards) {
					long rewardCount = reward.getCount();
					if(reward.isEnableRates()) {
						rewardCount = (long)(rewardCount * rateExp);
					}
					if(reward.isPerUnit()) {
						rewardCount = rewardCount * count;
					}
					replace += ", " + rewardCount + " " + ItemHolder.getInstance().getTemplate(reward.getId()).getName();
				}
				bodyRu = bodyRu.replaceAll("%rewardList%", replace);
				bodyEn = bodyEn.replaceAll("%rewardList%", replace);
				player.setVar("@daily_request_accepted", "spoil_" + player.getLevel() + "_" + spoil.getId() + "_" + count + "_" + count, -1);
			}
			
            boolean langRu = player.isLangRus();
            final Mail mail = new Mail();
            String senderName = langRu ? senderNameRu : senderNameEn;
            String topic = langRu ? topicNameRu : topicNameEn;
            String body = langRu ? bodyRu : bodyEn;
            mail.setSenderId(1);
			mail.setSenderName(senderName);
			mail.setReceiverId(player.getObjectId());
			mail.setReceiverName(player.getName());
			mail.setTopic(topic);
			mail.setBody(body);
			mail.setType(Mail.SenderType.NEWS_INFORMER);
			mail.setPrice(0);
			mail.setUnread(true);
			mail.setExpireTime(720 * 3600 + (int) (System.currentTimeMillis() / 1000L));
			mail.save();
		} else {
			if(player.isLangRus()) {
				player.sendMessage("На текущем уровне нет доступных ежедневных заданий.");				
			} else {
				player.sendMessage("There are no daily tasks available at the current level.");
			}
		}
	}

	public static Level getAvalibleLevel(final List<Level> levels, final int level) {
		for(final Level lvl : levels) {
			if(lvl.getMin() <= level && lvl.getMax() >= level) {
				return lvl;
			}
		}
		return null;
	}

    public static boolean isRequestComplete(final Player player) { //проверка на выполнение таска
        return player.getVarB("@daily_request_complete", false);
    }

    public static boolean isRequestAccepted(final Player player) { //проверка на наличие активного таска
        return player.getVarB("@daily_request_accepted", false);
    }
	
	public boolean npcKilled(final Player player) {
        try {
            String request = player.getVar("@daily_request_accepted");
            player.unsetVar("@daily_request_accepted");
            String request_arr[] = request.split("_");
            long count = Long.parseLong(request_arr[3]);
            if(count <= 1) { // если мы убили моба и нам оставался 1 моб, то реквест выполнен
                count--;
                player.setVar("@daily_request_accepted", request_arr[0] + "_" + request_arr[1] + "_" + request_arr[2] + "_" + count + "_" + request_arr[4], -1);
                return true;
            }
            count--;
            // Находим нужный темплейт
            DailyTask task = DailyTaskHolder.getInstance().getTask(Trigger.valueOf(request_arr[0]));
            // Выводим сообещние об успехе выполнения
            player.sendMessage((player.isLangRus() ? task.getSystemStringOnKillRu() : task.getSystemStringOnKillEn()).replaceAll("%decreaseCount%", String.valueOf(count)));
			player.sendPacket(new ExShowScreenMessage(((player.isLangRus() ? task.getSystemStringOnKillRu() : task.getSystemStringOnKillEn()).replaceAll("%decreaseCount%", String.valueOf(count))), 3000, ExShowScreenMessage.ScreenMessageAlign.TOP_CENTER, true));
            player.setVar("@daily_request_accepted", request_arr[0] + "_" + request_arr[1] + "_" + request_arr[2] + "_" + count + "_" + request_arr[4], -1);
            return false;
        } catch(Exception e) {
            LOGGER.error("", e);
            return false;
        }
    }

    public boolean playerKilled(Player player) {
        try {
            String request = player.getVar("@daily_request_accepted");
            player.unsetVar("@daily_request_accepted");
            String request_arr[] = request.split("_");
            long count = Long.parseLong(request_arr[2]);
            if(count <= 1) { // если мы убили бродягу и нам оставался еще 1, то реквест выполнен
                count--;
                player.setVar("@daily_request_accepted", request_arr[0] + "_" + request_arr[1] + "_" + count + "_" + request_arr[3], -1);
                return true;
            }
            count--;
            // Находим нужный темплейт
            DailyTask task = DailyTaskHolder.getInstance().getTask(Trigger.valueOf(request_arr[0]));
            // Выводим сообещние об успехе выполнения
            player.sendMessage((player.isLangRus() ? task.getSystemStringOnKillRu() : task.getSystemStringOnKillEn()).replaceAll("%decreaseCount%", String.valueOf(count)));
            player.setVar("@daily_request_accepted", request_arr[0] + "_" + request_arr[1] + "_" + count + "_" + request_arr[3], -1);
            return false;
        } catch(Exception e) {
            LOGGER.error("", e);
            return false;
        }
    }

    public boolean itemCrafted(Player player) {
        try {
            String request = player.getVar("@daily_request_accepted");
            player.unsetVar("@daily_request_accepted");
            String request_arr[] = request.split("_");
            long count = Long.parseLong(request_arr[3]);
            if(count <= 1) { // если мы скрафтили итем и нам оставался 1 итем, то реквест выполнен
                count--;
                player.setVar("@daily_request_accepted", request_arr[0] + "_" + request_arr[1] + "_" + request_arr[2] + "_" + String.valueOf(count) + "_" + request_arr[4], -1);
                return true;
            }
            count--;
            // Находим нужный темплейт
            DailyTask task = DailyTaskHolder.getInstance().getTask(Trigger.valueOf(request_arr[0]));
            // Выводим сообещние об успехе выполнения
            player.sendMessage((player.isLangRus() ? task.getSystemStringOnKillRu() : task.getSystemStringOnKillEn()).replaceAll("%decreaseCount%", String.valueOf(count)));
            player.setVar("@daily_request_accepted", request_arr[0] + "_" + request_arr[1] + "_" + request_arr[2] + "_" + String.valueOf(count) + "_" + request_arr[4], -1);
            return false;
        } catch(Exception e) {
        	LOGGER.error("", e);
            return false;
        }
    }

    public boolean itemFarmed(Player player, long pickupCount) {
        try {
            String request = player.getVar("@daily_request_accepted");
            player.unsetVar("@daily_request_accepted");
            String request_arr[] = request.split("_");
            long currentCount = Long.parseLong(request_arr[3]);
            long count = currentCount - pickupCount;
            if(count < 1) { // если мы получили итем и нам оставался 1 итем, то реквест выполнен
                player.setVar("@daily_request_accepted", request_arr[0] + "_" + request_arr[1] + "_" + request_arr[2] + "_" + String.valueOf(count) + "_" + request_arr[4], -1);
                return true;
            }
            // Находим нужный темплейт
            DailyTask task = DailyTaskHolder.getInstance().getTask(Trigger.valueOf(request_arr[0]));
            // Выводим сообещние об успехе выполнения
            player.sendMessage((player.isLangRus() ? task.getSystemStringOnKillRu() : task.getSystemStringOnKillEn()).replaceAll("%decreaseCount%", String.valueOf(count)));
            player.setVar("@daily_request_accepted", request_arr[0] + "_" + request_arr[1] + "_" + request_arr[2] + "_" + String.valueOf(count) + "_" + request_arr[4], -1);
            return false;
        } catch(Exception e) {
        	LOGGER.error("", e);
            return false;
        }
    }
	
	public void requestComplete(final Player player) {
        try {
            player.setVar("@daily_request_complete", "true", -1);
            String request = player.getVar("@daily_request_accepted");
            String request_arr[] = request.split("_");
            int level = Integer.parseInt(request_arr[1]);
            player.unsetVar("@daily_request_accepted");
            // Находим нужный таск
            DailyTask task = DailyTaskHolder.getInstance().getTask(Trigger.valueOf((request_arr[0])));
            final Level lvl = getAvalibleLevel(task.getLevels(), level);

            // Выводим сообещние об успехе выполнения
            player.sendMessage(player.isLangRus() ? task.getSystemStringOnCompleteRu() : task.getSystemStringOnCompleteEn());
			player.sendPacket(new ExShowScreenMessage(player.isLangRus() ? task.getSystemStringOnCompleteRu() : task.getSystemStringOnCompleteEn(), 3000, ExShowScreenMessage.ScreenMessageAlign.TOP_CENTER, true));
            // Начисляем награду
            int units = task.getTrigger() == Trigger.pvp || task.getTrigger() == Trigger.pk ? Integer.parseInt(request_arr[3]) : Integer.parseInt(request_arr[4]);
        	double rateExp = player.getRate().getExp();
        	double rateSp = player.getRate().getSp();
        	long rewardExp = lvl.getRewardExp();
        	long rewardSp = lvl.getRewardSp();
        	if(lvl.isEnableRates()) {
        		rewardExp = (long)(rewardExp * rateExp);
        		rewardSp = (long)(rewardSp * rateSp);
        	}
        	if(lvl.isPerUnit()) {
        		rewardExp = rewardExp * units;
        		rewardSp = rewardSp * units;
        	}
        	player.addExpAndSpDT(rewardExp, rewardSp);
        	for(final Reward item : lvl.getRewardList()) {
        		int id = item.getId();
        		long count = item.getCount();
        		if(item.isEnableRates()) {
        			count = (long)(count * rateExp);
        		}
        		if(item.isPerUnit()) {
        			count = count * units;
        		}
            	ItemFunctions.addItem(player, id, count, true);
            }
        } catch(Exception e) {
            LOGGER.error("", e);
        }
    }
	
	public void scheduleRefresh() { //определяем время выполнения задачи на сброс тасков
        final Calendar calendar = Calendar.getInstance();
        final int[] days = { 1, 2, 3, 4, 5, 6, 7 };
        final int day = days.length;
        final int hour = 23;
        final int minute = 59;

        for(int i = 0; i < day; ++i) {
            int dayId = days[i];
            if (calendar.get(7) == dayId) {
                calendar.set(11, hour);
                calendar.set(12, minute);
                long time = (long)((int)(calendar.getTimeInMillis() - System.currentTimeMillis()));
                if (calendar.getTimeInMillis() < System.currentTimeMillis()) {
                    return;
                }
                
                if (_refreshTask != null) {
                	_refreshTask.cancel(false);
                	_refreshTask = null;
                }

                _refreshTask = ThreadPoolManager.getInstance().schedule(new RefreshTask(), time);
        		LOGGER.info("DailyTaskEngine: Refresh in " + hour + ":" + minute + ".");
            }
        }
    }
    
    private static class RefreshTask extends RunnableImpl { //сброс ВЫПОЛНЕННЫХ тасков у всех игроков
        @Override
		public void runImpl() {
			LOGGER.info("DailyTaskEngine: refreshing...");
        	Connection con = null;
            PreparedStatement statement = null;
            try {
                con = DatabaseFactory.getInstance().getConnection();

                statement = con.prepareStatement("DELETE FROM character_variables WHERE name=?");
    			statement.setString(1, "@daily_request_complete");
                statement.execute();
            } catch(final Exception e) {
            	LOGGER.warn("DailyTaskEngine: delete requests fail " + e, e);
            } finally {
                DbUtils.closeQuietly(con, statement);
            }
            LOGGER.info("DailyTaskEngine: the refreshing is completed.");

            for(final Player player : GameObjectsStorage.getPlayers()) {
            	player.unsetVar("@daily_request_complete");
            }
        }
    }
    
    public static void refresh() {
    	CharacterVariablesDAO.getInstance().deleteLastHeroVars("@daily_request_accepted");
    	CharacterVariablesDAO.getInstance().deleteLastHeroVars("@daily_request_complete");
    }
}