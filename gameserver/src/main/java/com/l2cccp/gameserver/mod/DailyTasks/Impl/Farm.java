package com.l2cccp.gameserver.mod.DailyTasks.Impl;

public class Farm {
	private int id;
	private long minCount;
	private long maxCount;
	
	public Farm(final int id, final long minCount, final long maxCount) {
		this.id = id;
		this.minCount = minCount;
		this.maxCount = maxCount;
	}
	
	public int getId() {
		return id;
	}
	
	public long getMinCount() {
		return minCount;
	}
	
	public long getMaxCount() {
		return maxCount;
	}
}
