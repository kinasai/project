package com.l2cccp.gameserver.mod.DailyTasks.Impl;

import java.util.List;

public class Level {
	private int min;
	private int max;
	private long rewardExp;
	private long rewardSp;
	private boolean enableRates;
	private boolean isPerUnit;
	private boolean isPartyAll;
	private List<Reward> rewardList;
	private List<Monster> monsterList;
	private List<Craft> craftList;
	private List<Farm> farmList;
	private List<Spoil> spoilList;
	private List<PvP> pvpList;
	private List<Pk> pkList;
	
	public int getMin() {
		return min;
	}
	
	public void setMin(final int min) {
		this.min = min;
	}
	
	public int getMax() {
		return max;
	}
	
	public void setMax(final int max) {
		this.max = max;
	}
	
	public long getRewardExp() {
		return rewardExp;
	}
	
	public void setRewardExp(final long rewardExp) {
		this.rewardExp = rewardExp;
	}
	
	public long getRewardSp() {
		return rewardSp;
	}
	
	public void setRewardSp(final long rewardSp) {
		this.rewardSp = rewardSp;
	}
	
	public boolean isEnableRates() {
		return enableRates;
	}
	
	public void setEnableRates(final boolean enableRates) {
		this.enableRates = enableRates;
	}
	
	public boolean isPerUnit() {
		return isPerUnit;
	}
	
	public void setPerUnit(final boolean isPerUnit) {
		this.isPerUnit = isPerUnit;
	}
	
	public List<Reward> getRewardList() {
		return rewardList;
	}
	
	public void setRewardList(final List<Reward> rewardList) {
		this.rewardList = rewardList;
	}
	
	public List<Monster> getMonsterList() {
		return monsterList;
	}
	
	public void setMonsterList(final List<Monster> monsterList) {
		this.monsterList = monsterList;
	}
	
	public List<Craft> getCraftList() {
		return craftList;
	}
	
	public void setCraftList(final List<Craft> craftList) {
		this.craftList = craftList;
	}
	
	public List<PvP> getPvpList() {
		return pvpList;
	}
	
	public void setPvpList(final List<PvP> pvpList) {
		this.pvpList = pvpList;
	}
	
	public List<Pk> getPkList() {
		return pkList;
	}
	
	public void setPkList(final List<Pk> pkList) {
		this.pkList = pkList;
	}

	public boolean isPartyAll() {
		return isPartyAll;
	}

	public void setPartyAll(final boolean isPartyAll) {
		this.isPartyAll = isPartyAll;
	}

	public List<Farm> getFarmList() {
		return farmList;
	}

	public void setFarmList(final List<Farm> farmList) {
		this.farmList = farmList;
	}

	public List<Spoil> getSpoilList() {
		return spoilList;
	}

	public void setSpoilList(final List<Spoil> spoilList) {
		this.spoilList = spoilList;
	}
}
