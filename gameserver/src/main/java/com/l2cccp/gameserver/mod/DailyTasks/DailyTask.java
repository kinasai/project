package com.l2cccp.gameserver.mod.DailyTasks;

import java.util.List;

import com.l2cccp.gameserver.mod.DailyTasks.Impl.Level;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Trigger;

public class DailyTask {
	private Trigger trigger;
	private String senderNameRu;
	private String senderNameEn;
	private String topicNameRu;
	private String topicNameEn;
	private String bodyRu;
	private String bodyEn;
	private String systemStringOnKillRu;
	private String systemStringOnKillEn;
	private String systemStringOnCompleteRu;
	private String systemStringOnCompleteEn;
	private List<Level> levels;
	
	public Trigger getTrigger() {
		return trigger;
	}
	
	public void setTrigger(final Trigger trigger) {
		this.trigger = trigger;
	}
	
	public String getSenderNameRu() {
		return senderNameRu;
	}
	
	public void setSenderNameRu(final String senderNameRu) {
		this.senderNameRu = senderNameRu;
	}
	
	public String getSenderNameEn() {
		return senderNameEn;
	}
	
	public void setSenderNameEn(final String senderNameEn) {
		this.senderNameEn = senderNameEn;
	}
	
	public String getTopicNameRu() {
		return topicNameRu;
	}
	
	public void setTopicNameRu(final String topicNameRu) {
		this.topicNameRu = topicNameRu;
	}
	
	public String getTopicNameEn() {
		return topicNameEn;
	}
	
	public void setTopicNameEn(final String topicNameEn) {
		this.topicNameEn = topicNameEn;
	}
	
	public String getBodyRu() {
		return bodyRu;
	}
	
	public void setBodyRu(final String bodyRu) {
		this.bodyRu = bodyRu;
	}
	
	public String getBodyEn() {
		return bodyEn;
	}
	
	public void setBodyEn(final String bodyEn) {
		this.bodyEn = bodyEn;
	}
	
	public String getSystemStringOnKillRu() {
		return systemStringOnKillRu;
	}
	
	public void setSystemStringOnKillRu(final String systemStringOnKillRu) {
		this.systemStringOnKillRu = systemStringOnKillRu;
	}
	
	public String getSystemStringOnKillEn() {
		return systemStringOnKillEn;
	}
	
	public void setSystemStringOnKillEn(final String systemStringOnKillEn) {
		this.systemStringOnKillEn = systemStringOnKillEn;
	}
	
	public String getSystemStringOnCompleteRu() {
		return systemStringOnCompleteRu;
	}
	
	public void setSystemStringOnCompleteRu(final String systemStringOnCompleteRu) {
		this.systemStringOnCompleteRu = systemStringOnCompleteRu;
	}
	
	public String getSystemStringOnCompleteEn() {
		return systemStringOnCompleteEn;
	}
	
	public void setSystemStringOnCompleteEn(final String systemStringOnCompleteEn) {
		this.systemStringOnCompleteEn = systemStringOnCompleteEn;
	}
	
	public List<Level> getLevels() {
		return levels;
	}
	
	public void setLevels(final List<Level> levels) {
		this.levels = levels;
	}
}
