package com.l2cccp.gameserver.mod.DailyTasks.Impl;

public class Reward {
	private int id;
	private long count;
	private boolean isEnableRates;
	private boolean isPerUnit;
	
	public Reward(final int id, final long count, final boolean isEnableRates, final boolean isPerUnit) {
		this.id = id;
		this.count = count;
		this.isEnableRates = isEnableRates;
		this.isPerUnit = isPerUnit;
	}
	
	public int getId() {
		return id;
	}
	
	public long getCount() {
		return count;
	}

	public boolean isEnableRates() {
		return isEnableRates;
	}

	public boolean isPerUnit() {
		return isPerUnit;
	}
}
