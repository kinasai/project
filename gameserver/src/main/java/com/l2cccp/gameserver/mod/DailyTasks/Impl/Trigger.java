package com.l2cccp.gameserver.mod.DailyTasks.Impl;

public enum Trigger {
	monster,
	pvp,
	pk,
	craft,
	farm,
	spoil;
}
