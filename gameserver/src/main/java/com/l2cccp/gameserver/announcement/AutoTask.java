package com.l2cccp.gameserver.announcement;

import com.l2cccp.commons.threading.RunnableImpl;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class AutoTask extends RunnableImpl
{
	private final Announcement announcement;
	private int limit = 0;

	public AutoTask(final Announcement announcement)
	{
		this.announcement = announcement;
		limit = announcement.getLimit();
	}

	@Override
	protected void runImpl() throws Exception
	{
		boolean send = false;
		if(limit == -1)
			send = true;
		else if(limit >= 1)
		{
			limit--;
			send = true;
		}

		if(send)
			Broadcast.getInstance().send(announcement);
		else
			announcement.stopTask();
	}
}