package com.l2cccp.gameserver.announcement;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;

import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.tables.GmListTable;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Announcement
{
	// Settings!
	private final int _id;
	private final ChatType _chat;
	private final boolean _auto;
	private final long _initial;
	private final long _interval;
	private final int _limit;

	private int _min;
	private int _max;

	// Messages
	private final Map<Language, String> _msg = new HashMap<Language, String>();
	private final Map<Language, String> _author = new HashMap<Language, String>();

	// Scheduled task
	private Future<?> _task;

	public Announcement(final int id, final ChatType chat, final boolean auto, final long initial, final long interval, final int limit)
	{
		_id = id;
		_chat = chat;
		_auto = auto;
		_initial = initial;
		_interval = interval;
		_limit = limit;
	}

	public int getId()
	{
		return _id;
	}

	public ChatType getChatType()
	{
		return _chat;
	}

	public boolean isAuto()
	{
		return _auto;
	}

	public long getInitial()
	{
		return _initial;
	}

	public long getInterval()
	{
		return _interval;
	}

	public int getLimit()
	{
		return _limit;
	}

	public void setLevels(final int min, final int max)
	{
		_min = min;
		_max = max;
	}

	public int getMinLevel()
	{
		return _min;
	}

	public int getMaxLevel()
	{
		return _max;
	}

	public void putMsg(final Language lang, final String message)
	{
		_msg.put(lang, message);
	}

	public String getMsg(final Language lang)
	{
		return _msg.get(lang);
	}

	public void putAuthor(final Language lang, final String message)
	{
		_author.put(lang, message);
	}

	public String getAuthor(final Language lang)
	{
		return _author.get(lang);
	}

	public void startTask()
	{
		if(isAuto())
			_task = ThreadPoolManager.getInstance().scheduleAtFixedRate(new AutoTask(this), getInitial(), getInterval());
	}

	public void stopTask()
	{
		if(_task != null)
		{
			_task.cancel(false);
			_task = null;
			GmListTable.broadcastMessageToGMs("Announcement by id " + getId() + " is stopped!");
		}
	}
}