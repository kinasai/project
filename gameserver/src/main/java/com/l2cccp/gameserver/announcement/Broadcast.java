package com.l2cccp.gameserver.announcement;

import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Broadcast
{
	private static Broadcast _instance = new Broadcast();

	public static Broadcast getInstance()
	{
		return _instance;
	}

	public void send(Announcement announcement)
	{
		// Не совсем то, но лучше создать 2 переменные чем создавать каждый раз копию для рассылки!
		final String en_author = announcement.getAuthor(Language.ENGLISH);
		final String ru_author = announcement.getAuthor(Language.RUSSIAN);
		final Say2 en = new Say2(0, announcement.getChatType(), en_author, announcement.getMsg(Language.ENGLISH), null);
		final Say2 ru = new Say2(0, announcement.getChatType(), ru_author, announcement.getMsg(Language.RUSSIAN), null);

		for(Player player : GameObjectsStorage.getPlayers())
		{
			if(!player.isInOfflineMode() && player.isOnline() && player.getNetConnection() != null)
			{
				final int level = player.getLevel();
				if(level >= announcement.getMinLevel() && level <= announcement.getMaxLevel())
				{
					final boolean eng = player.getLanguage().equals(Language.ENGLISH);
					player.sendPacket(eng ? en : ru);
				}
			}
		}
	}
}