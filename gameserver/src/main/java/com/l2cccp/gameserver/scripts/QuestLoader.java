package com.l2cccp.gameserver.scripts;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.model.quest.Quest;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 * Заготовочное хранилище!
 */
public class QuestLoader
{
	private static QuestLoader _instance;
	private final Logger _log = LoggerFactory.getLogger(QuestLoader.class);
	private List<Quest> quests;

	private QuestLoader()
	{
		quests = new ArrayList<Quest>();
	}

	public static QuestLoader getInstance()
	{
		if(_instance == null)
			_instance = new QuestLoader();

		return _instance;
	}

	public void add(final Quest quest)
	{
		quests.add(quest);
	}

	public void init()
	{
		for(final Quest quest : quests)
		{
			quest.init();
			//_log.info("Load quest: " + quest.getName(Language.ENGLISH));
		}

		_log.info("QuestLoader: Initialization " + quests.size() + " quests!");
		quests.clear();
	}
}