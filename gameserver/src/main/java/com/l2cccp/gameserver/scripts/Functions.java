package com.l2cccp.gameserver.scripts;

import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import com.l2cccp.commons.lang.reference.HardReference;
import com.l2cccp.commons.lang.reference.HardReferences;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.data.xml.holder.StringHolder;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.mail.Mail;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.NpcString;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExNoticePostArrived;
import com.l2cccp.gameserver.network.l2.s2c.NpcSay;
import com.l2cccp.gameserver.utils.ChatUtils;
import com.l2cccp.gameserver.utils.HtmlUtils;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;

@Deprecated
public class Functions
{
	public HardReference<Player> self = HardReferences.emptyRef();
	public static void show(String text, Player self, NpcInstance npc, Object... arg)
	{
		if(text == null || self == null)
			return;

		HtmlMessage msg = npc == null ? new HtmlMessage(5) : new HtmlMessage(npc);

		// приводим нашу html-ку в нужный вид
		if(text.endsWith(".htm"))
			msg.setFile(text);
		else
			msg.setHtml(HtmlUtils.bbParse(text));

		if(arg != null && arg.length % 2 == 0)
			for(int i = 0; i < arg.length; i = +2)
				msg.replace(String.valueOf(arg[i]), String.valueOf(arg[i + 1]));

		self.sendPacket(msg);
	}

	public static void showFromStringHolder(String message, Player self)
	{
		show(StringHolder.getInstance().getString(self, message), self, null);
	}

	// Белый чат
	public static void npcSayInRange(NpcInstance npc, String text, int range)
	{
		npcSayInRange(npc, range, NpcString.NONE, text);
	}


	// Белый чат
	public static void npcSayInRange(NpcInstance npc, int range, NpcString npcString, String... params)
	{
		ChatUtils.say(npc, range, npcString, params);
	}

	// private message
	public static void npcSayToPlayer(NpcInstance npc, Player player, String text)
	{
		npcSayToPlayer(npc, player, NpcString.NONE, text);
	}

	// private message
	public static void npcSayToPlayer(NpcInstance npc, Player player, NpcString npcString, String... params)
	{
		player.sendPacket(new NpcSay(npc, ChatType.NPC_ALL, npcString, params));
	}

	// Shout (желтый) чат
	public static void npcShout(NpcInstance npc, String text)
	{
		npcShout(npc, NpcString.NONE, text);
	}

	// Shout (желтый) чат
	public static void npcShout(NpcInstance npc, NpcString npcString, String... params)
	{
		ChatUtils.shout(npc, npcString, params);
	}

	//TODO [VISTALL] use NpcUtils
	public static NpcInstance spawn(Location loc, int npcId)
	{
		return spawn(loc, npcId, ReflectionManager.DEFAULT);
	}

	@Deprecated
	public static NpcInstance spawn(Location loc, int npcId, Reflection reflection)
	{
		return NpcUtils.spawnSingle(npcId, loc, reflection, 0);
	}

	public static void sendSystemMail(Player receiver, String title, String body, Map<Integer, Long> items)
	{
		if(receiver == null || !receiver.isOnline())
			return;
		if(title == null)
			return;
		if(items.keySet().size() > 8)
			return;

		Mail mail = new Mail();
		mail.setSenderId(1);
		mail.setSenderName("Admin");
		mail.setReceiverId(receiver.getObjectId());
		mail.setReceiverName(receiver.getName());
		mail.setTopic(title);
		mail.setBody(body);
		for(Map.Entry<Integer, Long> itm : items.entrySet())
		{
			ItemInstance item = ItemFunctions.createItem(itm.getKey());
			item.setLocation(ItemInstance.ItemLocation.MAIL);
			item.setCount(itm.getValue());
			item.save();
			mail.addAttachment(item);
		}
		mail.setType(Mail.SenderType.NEWS_INFORMER);
		mail.setUnread(true);
		mail.setExpireTime(720 * 3600 + (int) (System.currentTimeMillis() / 1000L));
		mail.save();

		receiver.sendPacket(ExNoticePostArrived.STATIC_TRUE);
		receiver.sendPacket(SystemMsg.THE_MAIL_HAS_ARRIVED);
	}

	public static void unRide(Player player) {
		if(player.isMounted()) {
			player.setMount(0, 0, 0, -1);
		}
	}

	public static void unSummonPet(Player player, boolean onlyPets) {
		Servitor pet = player.getServitor();
		if(pet == null) {
			return;
		}
		if(pet.isPet() && onlyPets) {
			pet.unSummon(true, true);
		} else if(!onlyPets) {
			pet.unSummon(true, true);
		}
	}

	public static long getItemCount(Playable playable, int itemId) {
		return ItemFunctions.getItemCount(playable, itemId);
	}

	public static boolean removeItem(Playable playable, int itemId, long count) {
		return ItemFunctions.deleteItem(playable, itemId, count, true);
	}

	public static void addItem(Playable playable, int itemId, long count) {
		addItem(playable, itemId, count, true);
	}

	public static void addItem(Playable playable, int itemId, long count, boolean mess) {
		ItemFunctions.addItem(playable, itemId, count, mess);
	}

	public static void show(CustomMessage message, Player self) {
		show(message.toString(self), self, null);
	}

	public static boolean isPvPEventStarted() {
		if((Boolean) callScripts("events.TvT.TvT", "isRunned", new Object[] {})) {
			return true;
		}
		if((Boolean) callScripts("events.lastHero.LastHero", "isRunned", new Object[] {})) {
			return true;
		}
		if((Boolean) callScripts("events.CtF.CtF", "isRunned", new Object[] {})) {
			return true;
		}
		return false;
	}

	public static Object callScripts(String className, String methodName, Object[] args) {
		return callScripts(className, methodName, args, null);
	}

	public static Object callScripts(String className, String methodName, Object[] args, Map<String, Object> variables) {
		return callScripts(null, className, methodName, args, variables);
	}

	public static Object callScripts(Player player, String className, String methodName, Object[] args, Map<String, Object> variables) {
		return Scripts.getInstance().callScripts(player, className, methodName, args, variables);
	}

	public static ScheduledFuture<?> executeTask(final Player caller, final String className, final String methodName, final Object[] args, final Map<String, Object> variables, long delay) {
		return ThreadPoolManager.getInstance().schedule(new RunnableImpl() {
			@Override
			public void runImpl() throws Exception {
				callScripts(caller, className, methodName, args, variables);
			}
		}, delay);
	}

	public static ScheduledFuture<?> executeTask(String className, String methodName, Object[] args, Map<String, Object> variables, long delay) {
		return executeTask(null, className, methodName, args, variables, delay);
	}

	public static ScheduledFuture<?> executeTask(Player player, String className, String methodName, Object[] args, long delay) {
		return executeTask(player, className, methodName, args, null, delay);
	}

	public static ScheduledFuture<?> executeTask(String className, String methodName, Object[] args, long delay) {
		return executeTask(className, methodName, args, null, delay);
	}

	public Player getSelf() {
		return self.get();
	}
}