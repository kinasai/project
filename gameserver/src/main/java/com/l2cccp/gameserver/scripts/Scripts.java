package com.l2cccp.gameserver.scripts;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.reflect.MethodUtils;

import com.l2cccp.commons.compiler.Compiler;
import com.l2cccp.commons.compiler.MemoryClassLoader;
import com.l2cccp.commons.listener.Listener;
import com.l2cccp.commons.listener.ListenerList;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.handler.bypass.BypassHolder;
import com.l2cccp.gameserver.listener.ScriptListener;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.quest.Quest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Scripts
{
	public class ScriptListenerImpl extends ListenerList<Scripts>
	{
		public void init()
		{
			for(Listener<Scripts> listener : getListeners())
				if(OnInitScriptListener.class.isInstance(listener))
					((OnInitScriptListener) listener).onInit();
		}
	}

	private static final Logger _log = LoggerFactory.getLogger(Scripts.class);
	private static final String SCRIPTS_JAR_FILE = "./lib/scripts.jar";

	private static final Scripts _instance = new Scripts();

	public static Scripts getInstance()
	{
		return _instance;
	}

	private final Compiler compiler = new Compiler();
	private final Map<String, Class<?>> _classes = new TreeMap<String, Class<?>>();
	private final ScriptListenerImpl _listeners = new ScriptListenerImpl();

	private Scripts()
	{
		load();
	}

	/**
	 * Вызывается при загрузке сервера. Загрузает все скрипты в data/scripts. Не инициирует объекты и обработчики.
	 *
	 * @return true, если загрузка прошла успешно
	 */
	private void load()
	{
		_log.info("Scripts: Loading...");

		List<Class<?>> classes = new ArrayList<Class<?>>();
		boolean result = false;

		if(new File(SCRIPTS_JAR_FILE).exists())
			result = loadJarFile(classes);

		if(!result)
			result = load(classes);

		if(!result)
		{
			_log.error("Scripts: Failed loading scripts!");
			Runtime.getRuntime().exit(0);
			return;
		}

		_log.info("Scripts: Loaded " + classes.size() + " classes.");

		Class<?> clazz;
		for(int i = 0; i < classes.size(); i++)
		{
			clazz = classes.get(i);
			if(Config.DONTLOADQUEST)
				if(ClassUtils.isAssignable(clazz, Quest.class))
					continue;
			_classes.put(clazz.getName(), clazz);
		}
	}

	private boolean loadJarFile(List<Class<?>> classes)
	{
		boolean result = false;
		JarInputStream stream = null;
		try
		{
			stream = new JarInputStream(new FileInputStream(SCRIPTS_JAR_FILE));
			JarEntry entry = null;
			while((entry = stream.getNextJarEntry()) != null)
			{
				if(entry.getName().contains(ClassUtils.INNER_CLASS_SEPARATOR) || !entry.getName().endsWith(".class"))
					continue;
				final String name = entry.getName().replace(".class", "").replace("/", ".");
				Class<?> clazz = ClassLoader.getSystemClassLoader().loadClass(name);
				if(Modifier.isAbstract(clazz.getModifiers()))
					continue;
				classes.add(clazz);
			}
			result = true;
		}
		catch(Exception e)
		{
			_log.error("Fail to load scripts.jar!", e);
			classes.clear();
		}
		finally
		{
			IOUtils.closeQuietly(stream);
		}
		return result;
	}

	/**
	 * Вызывается при загрузке сервера. Инициализирует объекты и обработчики.
	 */
	public void init()
	{
		for(Class<?> clazz : _classes.values())
			init(clazz);

		_listeners.init();
	}

	/**
	 * Загрузить все классы в data/scripts/target
	 *
	 * @param target путь до класса, или каталога со скриптами
	 * @return список загруженых скриптов
	 */
	private boolean load(List<Class<?>> classes)
	{
		Collection<File> scriptFiles = Collections.emptyList();

		File file = new File(Config.DATAPACK_ROOT, "data/scripts/".replace(".", "/") + ".java");
		if(file.isFile())
		{
			scriptFiles = new ArrayList<File>(1);
			scriptFiles.add(file);
		}
		else
		{
			file = new File(Config.DATAPACK_ROOT, "data/scripts/");
			if(file.isDirectory())
				scriptFiles = FileUtils.listFiles(file, FileFilterUtils.suffixFileFilter(".java"), FileFilterUtils.directoryFileFilter());
		}

		if(scriptFiles.isEmpty())
			return false;

		Class<?> clazz;
		boolean success;

		if(success = compiler.compile(scriptFiles))
		{
			MemoryClassLoader classLoader = compiler.getClassLoader();
			for(String name : classLoader.getLoadedClasses())
			{
				if(name.contains(ClassUtils.INNER_CLASS_SEPARATOR))
					continue;

				try
				{
					clazz = classLoader.loadClass(name);
					if(Modifier.isAbstract(clazz.getModifiers()))
						continue;
					classes.add(clazz);
				}
				catch(ClassNotFoundException e)
				{
					success = false;
					_log.error("Scripts: Can't load script class: " + name, e);
				}
			}
			classLoader.clear();
		}

		return success;
	}

	public List<Class<?>> load(File target)
	{
		Collection<File> scriptFiles = Collections.emptyList();

		if(target.isFile())
		{
			scriptFiles = new ArrayList<File>(1);
			scriptFiles.add(target);
		}
		else if(target.isDirectory())
		{
			scriptFiles = FileUtils.listFiles(target, FileFilterUtils.suffixFileFilter(".java"), FileFilterUtils.directoryFileFilter());
		}

		if(scriptFiles.isEmpty())
			return Collections.emptyList();

		List<Class<?>> classes = new ArrayList<Class<?>>();
		Compiler compiler = new Compiler();

		if(compiler.compile(scriptFiles))
		{
			MemoryClassLoader classLoader = compiler.getClassLoader();
			for(String name : classLoader.getLoadedClasses())
			{
				//Вложенные класс
				if(name.contains(ClassUtils.INNER_CLASS_SEPARATOR))
					continue;

				try
				{
					Class<?> clazz = classLoader.loadClass(name);
					if(Modifier.isAbstract(clazz.getModifiers()))
						continue;
					classes.add(clazz);
				}
				catch(ClassNotFoundException e)
				{
					_log.error("Scripts: Can't load script class: " + name, e);
					classes.clear();
					break;
				}
			}
		}

		return classes;
	}

	private Object init(Class<?> clazz)
	{
		Object o = null;

		try
		{
			if(ClassUtils.isAssignable(clazz, ScriptListener.class))
			{
				o = clazz.newInstance();

				_listeners.add((ScriptListener) o);
			}

			for(Method method : clazz.getMethods())
				if(method.isAnnotationPresent(Bypass.class))
				{
					Bypass an = method.getAnnotation(Bypass.class);
					if(o == null)
						o = clazz.newInstance();
					Class[] par = method.getParameterTypes();
					if(par.length == 0 || par[0] != Player.class || par[1] != NpcInstance.class || par[2] != String[].class)
					{
						_log.error("Wrong parameters for bypass method: " + method.getName() + ", class: " + clazz.getSimpleName());
						continue;
					}

					BypassHolder.getInstance().registerBypass(an.value(), o, method);
				}
		}
		catch(Exception e)
		{
			_log.error("", e);
		}
		return o;
	}

	public Map<String, Class<?>> getClasses()
	{
		return _classes;
	}

	public Object callScripts(Player caller, String className, String methodName, Object[] args)
	{
		return callScripts(caller, className, methodName, args, null);
	}

	public Object callScripts(Player caller, String className, String methodName, Object[] args, Map<String, Object> variables)
	{
		Object o;
		Class<?> clazz;

		clazz = _classes.get(className);
		if(clazz == null)
		{
			_log.error("Script class " + className + " not found!");
			return null;
		}

		try
		{
			o = clazz.newInstance();
		}
		catch(Exception e)
		{
			_log.error("Scripts: Failed creating instance of " + clazz.getName(), e);
			return null;
		}

		if(variables != null && !variables.isEmpty())
			for(Map.Entry<String, Object> param : variables.entrySet())
				try
				{
					FieldUtils.writeField(o, param.getKey(), param.getValue());
				}
				catch(Exception e)
				{
					_log.error("Scripts: Failed setting fields for " + clazz.getName(), e);
				}

		if(caller != null)
			try
			{
				Field field = null;
				if((field = FieldUtils.getField(clazz, "self")) != null)
					FieldUtils.writeField(field, o, caller.getRef());
			}
			catch(Exception e)
			{
				_log.error("Scripts: Failed setting field for " + clazz.getName(), e);
			}

		Object ret = null;
		try
		{
			Class<?>[] parameterTypes = new Class<?>[args.length];
			for(int i = 0; i < args.length; i++)
				parameterTypes[i] = args[i] != null ? args[i].getClass() : null;

			ret = MethodUtils.invokeMethod(o, methodName, args, parameterTypes);
		}
		catch(NoSuchMethodException nsme)
		{
			_log.error("Scripts: No such method " + clazz.getName() + "." + methodName + "()!");
		}
		catch(InvocationTargetException ite)
		{
			_log.error("Scripts: Error while calling " + clazz.getName() + "." + methodName + "()", ite.getTargetException());
		}
		catch(Exception e)
		{
			_log.error("Scripts: Failed calling " + clazz.getName() + "." + methodName + "()", e);
		}

		return ret;
	}
}