package com.l2cccp.gameserver.stats.calc;

import com.l2cccp.gameserver.data.xml.holder.PlayerBalancerHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.balancing.player.PlayerLimits;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.stats.funcs.Func;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class AccuracyLimit extends Func
{
	private static final AccuracyLimit _instance = new AccuracyLimit();

	public static AccuracyLimit init()
	{
		return _instance;
	}

	private AccuracyLimit()
	{
		super(Stats.ACCURACY_COMBAT, 0x100, null);
	}

	@Override
	public void calc(final Env env)
	{
		final Creature character = env.character;
		double max = 200;

		if(character != null)
		{
			final Player player = character.getPlayer();
			if(player != null)
			{
				final int id = player.getClassId().getId();
				final PlayerLimits limit = PlayerBalancerHolder.getInstance().getLimits(id);
				max = limit.getAccuracy();
			}
		}

		env.value = Math.min(max, env.value);
	}
}