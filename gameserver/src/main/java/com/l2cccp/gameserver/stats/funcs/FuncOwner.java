package com.l2cccp.gameserver.stats.funcs;

public interface FuncOwner
{
	public boolean isFuncEnabled();

	public boolean overrideLimits();
}