package com.l2cccp.gameserver.stats.conditions;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.stats.Env;

public class ConditionPlayerIsHero extends Condition
{
	private final boolean _value;

	public ConditionPlayerIsHero(boolean value)
	{
		_value = value;
	}

	@Override
	protected boolean testImpl(Env env)
	{
		if(!env.character.isPlayer())
			return false;

		Player player = env.character.getPlayer();
		return player.isHero() == _value || player.FakeHeroEquip() == _value;
	}
}
