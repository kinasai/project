package com.l2cccp.gameserver.stats.conditions;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.stats.Env;

public class ConditionClanCastle extends Condition
{
	private final boolean _castle;

	public ConditionClanCastle(boolean castle)
	{
		_castle = castle;
	}

	@Override
	protected boolean testImpl(Env env)
	{
		if(!env.character.isPlayer())
			return false;

		Player player = (Player) env.character;
		final Clan clan = player.getClan();
		if(clan != null)
			return clan.getCastle() != 0 && _castle;
		else
			return !_castle;
	}
}