package com.l2cccp.gameserver.stats.conditions;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.stats.Env;

public class ConditionPlayerChargesMax extends Condition
{
	private final int _maxCharges;

	public ConditionPlayerChargesMax(int maxCharges)
	{
		_maxCharges = maxCharges;
	}

	@Override
	protected boolean testImpl(Env env)
	{
		if (env.character == null || !env.character.isPlayer())
			return false;

		if (((Player)env.character).getIncreasedForce() >= _maxCharges)
		{
			env.character.sendPacket(SystemMsg.YOUR_FORCE_HAS_REACHED_MAXIMUM_CAPACITY);
			return false;
		}
		return true;
	}
}