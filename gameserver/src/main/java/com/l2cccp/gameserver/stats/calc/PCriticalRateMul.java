package com.l2cccp.gameserver.stats.calc;

import com.l2cccp.gameserver.data.xml.holder.PlayerBalancerHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.model.balancing.player.BalancerType;
import com.l2cccp.gameserver.model.base.BaseStats;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.stats.funcs.Func;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PCriticalRateMul extends Func
{
	private static final PCriticalRateMul _instance = new PCriticalRateMul();

	public static PCriticalRateMul init()
	{
		return _instance;
	}

	private PCriticalRateMul()
	{
		super(Stats.CRITICAL_BASE, 0x10, null);
	}

	@Override
	public void calc(final Env env)
	{
		final Creature character = env.character;

		double bonus = 1.0D;
		if(character != null)
		{
			final BaseStats stats = BaseStats.DEX;
			if(!(character instanceof Servitor))
				env.value *= stats.calcBonus(character);

			final Player player = character.getPlayer();
			if(player != null)
			{
				final int id = player.getClassId().getId();
				final boolean olympiad = player.isInOlympiadMode();
				bonus = PlayerBalancerHolder.getInstance().getBalancer(BalancerType.physical_critical_attack, id, olympiad);
			}

			env.value *= 0.01 * character.calcStat(Stats.CRITICAL_RATE, env.target, env.skill) * bonus;
		}
	}
}