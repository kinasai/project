package com.l2cccp.gameserver.stats.calc;

import com.l2cccp.gameserver.data.xml.holder.PlayerBalancerHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.balancing.player.BalancerType;
import com.l2cccp.gameserver.model.base.BaseStats;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.stats.funcs.Func;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class MaxMpMul extends Func
{
	private static final MaxMpMul _instance = new MaxMpMul();

	public static MaxMpMul init()
	{
		return _instance;
	}

	private MaxMpMul()
	{
		super(Stats.MAX_MP, 0x20, null);
	}

	@Override
	public void calc(final Env env)
	{
		final BaseStats stats = BaseStats.MEN;
		final Creature character = env.character;
		final double men_bonus = stats.calcBonus(character);

		double bonus = 1.0D;
		if(character != null)
		{
			final Player player = character.getPlayer();
			if(player != null)
			{
				final int id = player.getClassId().getId();
				final boolean olympiad = player.isInOlympiadMode();
				bonus = PlayerBalancerHolder.getInstance().getBalancer(BalancerType.max_mp, id, olympiad);
			}
		}

		env.value *= men_bonus * bonus;
	}
}