package com.l2cccp.gameserver.stats.conditions;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.entity.events.impl.CastleSiegeEvent;
import com.l2cccp.gameserver.model.entity.events.impl.DominionSiegeEvent;
import com.l2cccp.gameserver.model.entity.events.impl.SiegeEvent;
import com.l2cccp.gameserver.stats.Env;

/**
 * @author VISTALL
 * @date 18:37/22.05.2011
 */
public class ConditionPlayerSummonSiegeGolem extends Condition
{
	public ConditionPlayerSummonSiegeGolem()
	{
		//
	}

	@Override
	protected boolean testImpl(Env env)
	{
		Player player = env.character.getPlayer();
		if(player == null)
			return false;
		Zone zone = player.getZone(Zone.ZoneType.RESIDENCE);
		if(zone != null)
			return false;
		zone = player.getZone(Zone.ZoneType.SIEGE);
		if(zone == null)
			return false;
		SiegeEvent event = player.getEvent(SiegeEvent.class);
		if(event == null)
			return false;
		if(event instanceof CastleSiegeEvent)
		{
			if(zone.getParams().getInteger("residence") != event.getId())
				return false;
			if(event.getSiegeClan(CastleSiegeEvent.ATTACKERS, player.getClan()) == null)
				return false;
		}
		else
		{
			boolean asClan = event.getSiegeClan(DominionSiegeEvent.DEFENDERS, player.getClan()) != null;
			boolean asPlayer = event.getObjects(DominionSiegeEvent.DEFENDER_PLAYERS).contains(player.getObjectId());
			if(!asClan && !asPlayer)
				return false;
		}

		return true;
	}
}
