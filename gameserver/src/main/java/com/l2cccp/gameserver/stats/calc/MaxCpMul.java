package com.l2cccp.gameserver.stats.calc;

import com.l2cccp.gameserver.data.xml.holder.PlayerBalancerHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.balancing.player.BalancerType;
import com.l2cccp.gameserver.model.base.BaseStats;
import com.l2cccp.gameserver.model.entity.SevenSigns;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.stats.funcs.Func;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class MaxCpMul extends Func
{
	private static final MaxCpMul _instance = new MaxCpMul();

	public static MaxCpMul init()
	{
		return _instance;
	}

	private MaxCpMul()
	{
		super(Stats.MAX_CP, 0x20, null);
	}

	@Override
	public void calc(final Env env)
	{
		final BaseStats stats = BaseStats.CON;
		final Creature character = env.character;
		final double con_bonus = stats.calcBonus(character);

		double bonus = 1.0D;
		if(character != null)
		{
			final Player player = character.getPlayer();
			if(player != null)
			{
				final int id = player.getClassId().getId();
				final boolean olympiad = player.isInOlympiadMode();
				bonus = PlayerBalancerHolder.getInstance().getBalancer(BalancerType.max_cp, id, olympiad);
			}
		}

		double signs_mod = 1;
		final int seal = SevenSigns.getInstance().getSealOwner(SevenSigns.SEAL_STRIFE);

		if(seal != SevenSigns.CABAL_NULL)
		{
			final int cabal = SevenSigns.getInstance().getPlayerCabal((Player) env.character);
			if(cabal == seal)
				signs_mod = 1.1;
			else
				signs_mod = 0.9;
		}

		env.value *= con_bonus * signs_mod * bonus;
	}
}