package com.l2cccp.gameserver.stats;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.BaseStats;
import com.l2cccp.gameserver.model.base.ClassType2;
import com.l2cccp.gameserver.model.base.Element;
import com.l2cccp.gameserver.model.base.Race;
import com.l2cccp.gameserver.model.items.Inventory;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.stats.calc.AccuracyAdd;
import com.l2cccp.gameserver.stats.calc.AccuracyLimit;
import com.l2cccp.gameserver.stats.calc.CritLimit;
import com.l2cccp.gameserver.stats.calc.EvasionAdd;
import com.l2cccp.gameserver.stats.calc.EvasionLimit;
import com.l2cccp.gameserver.stats.calc.MAtkLimit;
import com.l2cccp.gameserver.stats.calc.MAtkMul;
import com.l2cccp.gameserver.stats.calc.MAtkSpdLimit;
import com.l2cccp.gameserver.stats.calc.MAtkSpeedMul;
import com.l2cccp.gameserver.stats.calc.MCritLimit;
import com.l2cccp.gameserver.stats.calc.MCriticalRateMul;
import com.l2cccp.gameserver.stats.calc.MDefLimit;
import com.l2cccp.gameserver.stats.calc.MDefMul;
import com.l2cccp.gameserver.stats.calc.MaxCpLimit;
import com.l2cccp.gameserver.stats.calc.MaxCpMul;
import com.l2cccp.gameserver.stats.calc.MaxHpLimit;
import com.l2cccp.gameserver.stats.calc.MaxHpMul;
import com.l2cccp.gameserver.stats.calc.MaxMpLimit;
import com.l2cccp.gameserver.stats.calc.MaxMpMul;
import com.l2cccp.gameserver.stats.calc.MoveSpeedMul;
import com.l2cccp.gameserver.stats.calc.PAtkLimit;
import com.l2cccp.gameserver.stats.calc.PAtkMul;
import com.l2cccp.gameserver.stats.calc.PAtkSpeedMul;
import com.l2cccp.gameserver.stats.calc.PCriticalRateMul;
import com.l2cccp.gameserver.stats.calc.PDefLimit;
import com.l2cccp.gameserver.stats.calc.PDefMul;
import com.l2cccp.gameserver.stats.calc.RunSpdLimit;
import com.l2cccp.gameserver.stats.conditions.ConditionPlayerState;
import com.l2cccp.gameserver.stats.conditions.ConditionPlayerState.CheckPlayerState;
import com.l2cccp.gameserver.stats.funcs.Func;
import com.l2cccp.gameserver.templates.item.WeaponTemplate;
import com.l2cccp.gameserver.templates.item.WeaponTemplate.WeaponType;

/**
 * Коллекция предопределенных классов функций.
 *
 */
public class StatFunctions
{
	private static class FuncMultRegenResting extends Func
	{
		static final FuncMultRegenResting[] func = new FuncMultRegenResting[Stats.NUM_STATS];

		static Func getFunc(Stats stat)
		{
			int pos = stat.ordinal();
			if(func[pos] == null)
				func[pos] = new FuncMultRegenResting(stat);
			return func[pos];
		}

		private FuncMultRegenResting(Stats stat)
		{
			super(stat, 0x30, null);
			setCondition(new ConditionPlayerState(CheckPlayerState.RESTING, true));
		}

		@Override
		public void calc(Env env)
		{
			if(env.character.isPlayer() && env.character.getLevel() <= 40 && ((Player) env.character).getClassId().getLevel() < 3 && stat == Stats.REGENERATE_HP_RATE)
				env.value *= 6.; // TODO: переделать красивее
			else
				env.value *= 1.5;
		}
	}

	private static class FuncMultRegenStanding extends Func
	{
		static final FuncMultRegenStanding[] func = new FuncMultRegenStanding[Stats.NUM_STATS];

		static Func getFunc(Stats stat)
		{
			int pos = stat.ordinal();
			if(func[pos] == null)
				func[pos] = new FuncMultRegenStanding(stat);
			return func[pos];
		}

		private FuncMultRegenStanding(Stats stat)
		{
			super(stat, 0x30, null);
			setCondition(new ConditionPlayerState(CheckPlayerState.STANDING, true));
		}

		@Override
		public void calc(Env env)
		{
			env.value *= 1.1;
		}
	}

	private static class FuncMultRegenRunning extends Func
	{
		static final FuncMultRegenRunning[] func = new FuncMultRegenRunning[Stats.NUM_STATS];

		static Func getFunc(Stats stat)
		{
			int pos = stat.ordinal();
			if(func[pos] == null)
				func[pos] = new FuncMultRegenRunning(stat);
			return func[pos];
		}

		private FuncMultRegenRunning(Stats stat)
		{
			super(stat, 0x30, null);
			setCondition(new ConditionPlayerState(CheckPlayerState.RUNNING, true));
		}

		@Override
		public void calc(Env env)
		{
			env.value *= 0.7;
		}
	}

	private static class FuncAttackRange extends Func
	{
		static final FuncAttackRange func = new FuncAttackRange();

		private FuncAttackRange()
		{
			super(Stats.POWER_ATTACK_RANGE, 0x20, null);
		}

		@Override
		public void calc(Env env)
		{
			WeaponTemplate weapon = env.character.getActiveWeaponItem();
			if(weapon != null)
				env.value += weapon.getAttackRange();
		}
	}

	private static class FuncHennaSTR extends Func
	{
		static final FuncHennaSTR func = new FuncHennaSTR();

		private FuncHennaSTR()
		{
			super(Stats.STAT_STR, 0x10, null);
		}

		@Override
		public void calc(Env env)
		{
			Player pc = (Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatSTR());
		}
	}

	private static class FuncHennaDEX extends Func
	{
		static final FuncHennaDEX func = new FuncHennaDEX();

		private FuncHennaDEX()
		{
			super(Stats.STAT_DEX, 0x10, null);
		}

		@Override
		public void calc(Env env)
		{
			Player pc = (Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatDEX());
		}
	}

	private static class FuncHennaINT extends Func
	{
		static final FuncHennaINT func = new FuncHennaINT();

		private FuncHennaINT()
		{
			super(Stats.STAT_INT, 0x10, null);
		}

		@Override
		public void calc(Env env)
		{
			Player pc = (Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatINT());
		}
	}

	private static class FuncHennaMEN extends Func
	{
		static final FuncHennaMEN func = new FuncHennaMEN();

		private FuncHennaMEN()
		{
			super(Stats.STAT_MEN, 0x10, null);
		}

		@Override
		public void calc(Env env)
		{
			Player pc = (Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatMEN());
		}
	}

	private static class FuncHennaCON extends Func
	{
		static final FuncHennaCON func = new FuncHennaCON();

		private FuncHennaCON()
		{
			super(Stats.STAT_CON, 0x10, null);
		}

		@Override
		public void calc(Env env)
		{
			Player pc = (Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatCON());
		}
	}

	private static class FuncHennaWIT extends Func
	{
		static final FuncHennaWIT func = new FuncHennaWIT();

		private FuncHennaWIT()
		{
			super(Stats.STAT_WIT, 0x10, null);
		}

		@Override
		public void calc(Env env)
		{
			Player pc = (Player) env.character;
			if(pc != null)
				env.value = Math.max(1, env.value + pc.getHennaStatWIT());
		}
	}

	private static class FuncPDamageResists extends Func
	{
		static final FuncPDamageResists func = new FuncPDamageResists();

		private FuncPDamageResists()
		{
			super(Stats.PHYSICAL_DAMAGE, 0x30, null);
		}

		@Override
		public void calc(Env env)
		{
			if(env.target.isRaid() && env.character.getLevel() - env.target.getLevel() > Config.RAID_MAX_LEVEL_DIFF)
			{
				env.value = 1;
				return;
			}

			// TODO переделать на ту же систему, что у эффектов
			WeaponTemplate weapon = env.character.getActiveWeaponItem();
			if(weapon == null)
				env.value *= 0.01 * env.target.calcStat(Stats.FIST_WPN_VULNERABILITY, env.character, env.skill);
			else if(weapon.getItemType().getDefence() != null)
				env.value *= 0.01 * env.target.calcStat(weapon.getItemType().getDefence(), env.character, env.skill);

			env.value = Formulas.calcDamageResists(env.skill, env.character, env.target, env.value);
		}
	}

	private static class FuncMDamageResists extends Func
	{
		static final FuncMDamageResists func = new FuncMDamageResists();

		private FuncMDamageResists()
		{
			super(Stats.MAGIC_DAMAGE, 0x30, null);
		}

		@Override
		public void calc(Env env)
		{
			if(env.target.isRaid() && Math.abs(env.character.getLevel() - env.target.getLevel()) > Config.RAID_MAX_LEVEL_DIFF)
			{
				env.value = 1;
				return;
			}

			env.value = Formulas.calcDamageResists(env.skill, env.character, env.target, env.value);
		}
	}

	private static class FuncInventory extends Func
	{
		static final FuncInventory func = new FuncInventory();

		private FuncInventory()
		{
			super(Stats.INVENTORY_LIMIT, 0x01, null);
		}

		@Override
		public void calc(Env env)
		{
			Player player = (Player) env.character;
			if(player.isGM())
				env.value = Config.INVENTORY_MAXIMUM_GM;
			else if(player.getTemplate()._race == Race.dwarf)
				env.value = Config.INVENTORY_MAXIMUM_DWARF;
			else
				env.value = Config.INVENTORY_MAXIMUM_NO_DWARF;

			env.value += player.getExpandInventory();
			env.value = Math.min(env.value, Config.SERVICES_EXPAND_INVENTORY_MAX);
		}
	}

	private static class FuncWarehouse extends Func
	{
		static final FuncWarehouse func = new FuncWarehouse();

		private FuncWarehouse()
		{
			super(Stats.STORAGE_LIMIT, 0x01, null);
		}

		@Override
		public void calc(Env env)
		{
			Player player = (Player) env.character;
			if(player.getTemplate()._race == Race.dwarf)
				env.value = Config.WAREHOUSE_SLOTS_DWARF;
			else
				env.value = Config.WAREHOUSE_SLOTS_NO_DWARF;
			env.value += player.getExpandWarehouse();
		}
	}

	private static class FuncTradeLimit extends Func
	{
		static final FuncTradeLimit func = new FuncTradeLimit();

		private FuncTradeLimit()
		{
			super(Stats.TRADE_LIMIT, 0x01, null);
		}

		@Override
		public void calc(Env env)
		{
			Player _cha = (Player) env.character;
			if(_cha.getRace() == Race.dwarf)
				env.value = Config.MAX_PVTSTORE_SLOTS_DWARF;
			else
				env.value = Config.MAX_PVTSTORE_SLOTS_OTHER;
		}
	}

	private static class FuncSDefInit extends Func
	{
		static final Func func = new FuncSDefInit();

		private FuncSDefInit()
		{
			super(Stats.SHIELD_RATE, 0x01, null);
		}

		@Override
		public void calc(Env env)
		{
			Creature cha = env.character;
			env.value = cha.getTemplate().baseShldRate;
		}
	}

	private static class FuncSDefAll extends Func
	{
		static final FuncSDefAll func = new FuncSDefAll();

		private FuncSDefAll()
		{
			super(Stats.SHIELD_RATE, 0x20, null);
		}

		@Override
		public void calc(Env env)
		{
			if(env.value == 0)
				return;

			Creature target = env.target;
			if(target != null)
			{
				WeaponTemplate weapon = target.getActiveWeaponItem();
				if(weapon != null)
					switch(weapon.getItemType())
					{
						case BOW:
						case CROSSBOW:
							env.value += 30.;
							break;
						case DAGGER:
						case DUALDAGGER:
							env.value += 12.;
							break;
						default:
							break;
					}
			}
		}
	}

	private static class FuncSDefPlayers extends Func
	{
		static final FuncSDefPlayers func = new FuncSDefPlayers();

		private FuncSDefPlayers()
		{
			super(Stats.SHIELD_RATE, 0x20, null);
		}

		@Override
		public void calc(Env env)
		{
			if(env.value == 0)
				return;

			Creature cha = env.character;
			ItemInstance shld = ((Player) cha).getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
			if(shld == null || shld.getItemType() != WeaponType.NONE)
				return;
			env.value *= BaseStats.DEX.calcBonus(env.character);
		}
	}

	private static class FuncCAtkLimit extends Func
	{
		static final Func func = new FuncCAtkLimit();

		private FuncCAtkLimit()
		{
			super(Stats.CRITICAL_DAMAGE, 0x100, null);
		}

		@Override
		public void calc(Env env)
		{
			env.value = Math.min(Config.LIM_CRIT_DAM / 2., env.value);
		}
	}

	private static class FuncAttributeAttackInit extends Func
	{
		static final Func[] func = new FuncAttributeAttackInit[Element.VALUES.length];

		static
		{
			for(int i = 0; i < Element.VALUES.length; i++)
				func[i] = new FuncAttributeAttackInit(Element.VALUES[i]);
		}

		static Func getFunc(Element element)
		{
			return func[element.getId()];
		}

		private Element element;

		private FuncAttributeAttackInit(Element element)
		{
			super(element.getAttack(), 0x01, null);
			this.element = element;
		}

		@Override
		public void calc(Env env)
		{
			env.value += env.character.getTemplate().baseAttributeAttack[element.getId()];
		}
	}

	private static class FuncAttributeDefenceInit extends Func
	{
		static final Func[] func = new FuncAttributeDefenceInit[Element.VALUES.length];

		static
		{
			for(int i = 0; i < Element.VALUES.length; i++)
				func[i] = new FuncAttributeDefenceInit(Element.VALUES[i]);
		}

		static Func getFunc(Element element)
		{
			return func[element.getId()];
		}

		private Element element;

		private FuncAttributeDefenceInit(Element element)
		{
			super(element.getDefence(), 0x01, null);
			this.element = element;
		}

		@Override
		public void calc(Env env)
		{
			env.value += env.character.getTemplate().baseAttributeDefence[element.getId()];
		}
	}

	private static class FuncAttributeAttackSet extends Func
	{
		static final Func[] func = new FuncAttributeAttackSet[Element.VALUES.length];

		static
		{
			for(int i = 0; i < Element.VALUES.length; i++)
				func[i] = new FuncAttributeAttackSet(Element.VALUES[i].getAttack());
		}

		static Func getFunc(Element element)
		{
			return func[element.getId()];
		}

		private FuncAttributeAttackSet(Stats stat)
		{
			super(stat, 0x10, null);
		}

		@Override
		public void calc(Env env)
		{
			if(env.character.getPlayer().getClassId().getType2() == ClassType2.Summoner)
				env.value = env.character.getPlayer().calcStat(stat, 0., env.target, env.skill);
		}
	}

	private static class FuncAttributeDefenceSet extends Func
	{
		static final Func[] func = new FuncAttributeDefenceSet[Element.VALUES.length];

		static
		{
			for(int i = 0; i < Element.VALUES.length; i++)
				func[i] = new FuncAttributeDefenceSet(Element.VALUES[i].getDefence());
		}

		static Func getFunc(Element element)
		{
			return func[element.getId()];
		}

		private FuncAttributeDefenceSet(Stats stat)
		{
			super(stat, 0x10, null);
		}

		@Override
		public void calc(Env env)
		{
			if(env.character.getPlayer().getClassId().getType2() == ClassType2.Summoner)
				env.value = env.character.getPlayer().calcStat(stat, 0., env.target, env.skill);
		}
	}

	public static void addPredefinedFuncs(Creature cha)
	{
		if(cha.isPlayer())
		{
			cha.addStatFunc(FuncMultRegenResting.getFunc(Stats.REGENERATE_CP_RATE));
			cha.addStatFunc(FuncMultRegenStanding.getFunc(Stats.REGENERATE_CP_RATE));
			cha.addStatFunc(FuncMultRegenRunning.getFunc(Stats.REGENERATE_CP_RATE));
			cha.addStatFunc(FuncMultRegenResting.getFunc(Stats.REGENERATE_HP_RATE));
			cha.addStatFunc(FuncMultRegenStanding.getFunc(Stats.REGENERATE_HP_RATE));
			cha.addStatFunc(FuncMultRegenRunning.getFunc(Stats.REGENERATE_HP_RATE));
			cha.addStatFunc(FuncMultRegenResting.getFunc(Stats.REGENERATE_MP_RATE));
			cha.addStatFunc(FuncMultRegenStanding.getFunc(Stats.REGENERATE_MP_RATE));
			cha.addStatFunc(FuncMultRegenRunning.getFunc(Stats.REGENERATE_MP_RATE));

			cha.addStatFunc(MaxCpMul.init());
			cha.addStatFunc(MaxHpMul.init());
			cha.addStatFunc(MaxMpMul.init());

			cha.addStatFunc(FuncAttackRange.func);

			cha.addStatFunc(MoveSpeedMul.init());

			cha.addStatFunc(FuncHennaSTR.func);
			cha.addStatFunc(FuncHennaDEX.func);
			cha.addStatFunc(FuncHennaINT.func);
			cha.addStatFunc(FuncHennaMEN.func);
			cha.addStatFunc(FuncHennaCON.func);
			cha.addStatFunc(FuncHennaWIT.func);

			cha.addStatFunc(FuncInventory.func);
			cha.addStatFunc(FuncWarehouse.func);
			cha.addStatFunc(FuncTradeLimit.func);

			cha.addStatFunc(FuncSDefPlayers.func);

			cha.addStatFunc(MaxHpLimit.init());
			cha.addStatFunc(MaxMpLimit.init());
			cha.addStatFunc(MaxCpLimit.init());
			cha.addStatFunc(RunSpdLimit.init());
			cha.addStatFunc(PDefLimit.init());
			cha.addStatFunc(MDefLimit.init());
			cha.addStatFunc(PAtkLimit.init());
			cha.addStatFunc(MAtkLimit.init());
		}

		if(cha.isNpc())
		{
			cha.addStatFunc(MaxHpMul.init());
			cha.addStatFunc(MaxMpMul.init());
		}

		if(!cha.isSummon() && !cha.isDoor())
		{
			cha.addStatFunc(PAtkMul.init());
			cha.addStatFunc(MAtkMul.init());
			cha.addStatFunc(PDefMul.init());
			cha.addStatFunc(MDefMul.init());
		}

		if(cha.isSummon())
		{
			cha.addStatFunc(FuncAttributeAttackSet.getFunc(Element.FIRE));
			cha.addStatFunc(FuncAttributeAttackSet.getFunc(Element.WATER));
			cha.addStatFunc(FuncAttributeAttackSet.getFunc(Element.EARTH));
			cha.addStatFunc(FuncAttributeAttackSet.getFunc(Element.WIND));
			cha.addStatFunc(FuncAttributeAttackSet.getFunc(Element.HOLY));
			cha.addStatFunc(FuncAttributeAttackSet.getFunc(Element.UNHOLY));

			cha.addStatFunc(FuncAttributeDefenceSet.getFunc(Element.FIRE));
			cha.addStatFunc(FuncAttributeDefenceSet.getFunc(Element.WATER));
			cha.addStatFunc(FuncAttributeDefenceSet.getFunc(Element.EARTH));
			cha.addStatFunc(FuncAttributeDefenceSet.getFunc(Element.WIND));
			cha.addStatFunc(FuncAttributeDefenceSet.getFunc(Element.HOLY));
			cha.addStatFunc(FuncAttributeDefenceSet.getFunc(Element.UNHOLY));
		}

		if(!cha.isPet())
		{
			cha.addStatFunc(AccuracyAdd.init());
			cha.addStatFunc(EvasionAdd.init());
		}

		if(!cha.isServitor())
		{
			cha.addStatFunc(PAtkSpeedMul.init());
			cha.addStatFunc(MAtkSpeedMul.init());
			cha.addStatFunc(FuncSDefInit.func);
			cha.addStatFunc(FuncSDefAll.func);
		}

		cha.addStatFunc(MAtkSpdLimit.init());
		cha.addStatFunc(FuncCAtkLimit.func);
		cha.addStatFunc(EvasionLimit.init());
		cha.addStatFunc(AccuracyLimit.init());
		cha.addStatFunc(CritLimit.init());
		cha.addStatFunc(MCritLimit.init());

		cha.addStatFunc(MCriticalRateMul.init());
		cha.addStatFunc(PCriticalRateMul.init());
		cha.addStatFunc(FuncPDamageResists.func);
		cha.addStatFunc(FuncMDamageResists.func);

		cha.addStatFunc(FuncAttributeAttackInit.getFunc(Element.FIRE));
		cha.addStatFunc(FuncAttributeAttackInit.getFunc(Element.WATER));
		cha.addStatFunc(FuncAttributeAttackInit.getFunc(Element.EARTH));
		cha.addStatFunc(FuncAttributeAttackInit.getFunc(Element.WIND));
		cha.addStatFunc(FuncAttributeAttackInit.getFunc(Element.HOLY));
		cha.addStatFunc(FuncAttributeAttackInit.getFunc(Element.UNHOLY));

		cha.addStatFunc(FuncAttributeDefenceInit.getFunc(Element.FIRE));
		cha.addStatFunc(FuncAttributeDefenceInit.getFunc(Element.WATER));
		cha.addStatFunc(FuncAttributeDefenceInit.getFunc(Element.EARTH));
		cha.addStatFunc(FuncAttributeDefenceInit.getFunc(Element.WIND));
		cha.addStatFunc(FuncAttributeDefenceInit.getFunc(Element.HOLY));
		cha.addStatFunc(FuncAttributeDefenceInit.getFunc(Element.UNHOLY));
	}
}
