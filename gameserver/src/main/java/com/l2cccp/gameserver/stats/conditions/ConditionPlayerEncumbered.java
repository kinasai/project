package com.l2cccp.gameserver.stats.conditions;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.stats.Env;

public class ConditionPlayerEncumbered extends Condition
{
	private final int _maxWeightPercent;
	private final int _maxLoadPercent;

	public ConditionPlayerEncumbered(int remainingWeightPercent, int remainingLoadPercent)
	{
		_maxWeightPercent = 100 - remainingWeightPercent;
		_maxLoadPercent = 100 - remainingLoadPercent;
	}

	@Override
	protected boolean testImpl(Env env)
	{
		if (env.character == null || !env.character.isPlayer())
			return false;

		if (((Player)env.character).getWeightPercents() >= _maxWeightPercent || ((Player)env.character).getUsedInventoryPercents() >= _maxLoadPercent)
		{
			env.character.sendPacket(SystemMsg.YOUR_INVENTORY_IS_FULL);
			return false;
		}

		return true;
	}
}