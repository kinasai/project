package com.l2cccp.gameserver.stats.conditions;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.pledge.Rank;
import com.l2cccp.gameserver.stats.Env;

public class ConditionPlayerRank extends Condition
{
	private final Rank _rank;

	public ConditionPlayerRank(String rank)
	{
		_rank = Rank.valueOf(rank.toUpperCase());
	}

	@Override
	protected boolean testImpl(Env env)
	{
		if(!env.character.isPlayer())
			return false;

		return ((Player) env.character).getPledgeClass().ordinal() >= _rank.ordinal();
	}
}