package com.l2cccp.gameserver.stats.calc;

import com.l2cccp.gameserver.data.xml.holder.PlayerBalancerHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.balancing.player.PlayerLimits;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.stats.funcs.Func;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PDefLimit extends Func
{
	private static final PDefLimit _instance = new PDefLimit();

	public static PDefLimit init()
	{
		return _instance;
	}

	private PDefLimit()
	{
		super(Stats.POWER_DEFENCE, 0x100, null);
	}

	@Override
	public void calc(final Env env)
	{
		final Creature character = env.character;
		double max = 150000;

		if(character != null)
		{
			final Player player = character.getPlayer();
			if(player != null)
			{
				final int id = player.getClassId().getId();
				final PlayerLimits limit = PlayerBalancerHolder.getInstance().getLimits(id);
				max = limit.getPhysicalDefence();
			}
		}

		env.value = Math.min(max, env.value);
	}
}