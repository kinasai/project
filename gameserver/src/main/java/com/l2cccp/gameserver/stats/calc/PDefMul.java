package com.l2cccp.gameserver.stats.calc;

import com.l2cccp.gameserver.data.xml.holder.PlayerBalancerHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.balancing.player.BalancerType;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.stats.funcs.Func;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PDefMul extends Func
{
	private static final PDefMul _instance = new PDefMul();

	public static PDefMul init()
	{
		return _instance;
	}

	private PDefMul()
	{
		super(Stats.POWER_DEFENCE, 0x20, null);
	}

	@Override
	public void calc(final Env env)
	{
		final Creature character = env.character;

		double level_bonus = 1.0D;
		double bonus = 1.0D;
		if(character != null)
		{
			level_bonus = character.getLevelMod();
			final Player player = character.getPlayer();
			if(player != null)
			{
				final int id = player.getClassId().getId();
				final boolean olympiad = player.isInOlympiadMode();
				bonus = PlayerBalancerHolder.getInstance().getBalancer(BalancerType.physical_defence, id, olympiad);
			}
		}

		env.value *= level_bonus * bonus;
	}
}