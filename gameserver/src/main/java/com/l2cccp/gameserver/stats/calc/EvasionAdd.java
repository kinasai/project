package com.l2cccp.gameserver.stats.calc;

import com.l2cccp.gameserver.data.xml.holder.PlayerBalancerHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.balancing.player.BalancerType;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.stats.funcs.Func;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class EvasionAdd extends Func
{
	private static final EvasionAdd _instance = new EvasionAdd();

	private static final int[] EVASION_LEVEL_BONUS = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, //  1-10
			11,
			12,
			13,
			14,
			15,
			16,
			17,
			18,
			18,
			20, // 11-20
			21,
			22,
			23,
			24,
			25,
			26,
			27,
			28,
			29,
			30, // 21-30
			31,
			32,
			33,
			34,
			35,
			36,
			37,
			38,
			39,
			40, // 31-40
			41,
			42,
			43,
			44,
			45,
			46,
			47,
			48,
			49,
			50, // 41-50
			51,
			52,
			53,
			54,
			55,
			56,
			57,
			58,
			59,
			60, // 51-60
			61,
			62,
			63,
			64,
			65,
			66,
			67,
			68,
			69,
			71, // 61-70
			73,
			75,
			77,
			79,
			81,
			83,
			85,
			89,
			91,
			94, // 71-80
			96,
			98,
			100,
			103,
			105,
			107,
			109,
			112,
			114,
			116 // 81-90
	};

	public static EvasionAdd init()
	{
		return _instance;
	}

	private EvasionAdd()
	{
		super(Stats.EVASION_RATE, 0x10, null);
	}

	@Override
	public void calc(final Env env)
	{
		final Creature character = env.character;

		double bonus = 0.0D;
		if(character != null)
		{
			final Player player = character.getPlayer();
			if(player != null)
			{
				final int id = player.getClassId().getId();
				final boolean olympiad = player.isInOlympiadMode();
				bonus = PlayerBalancerHolder.getInstance().getBalancer(BalancerType.evasion, id, olympiad);
			}

			final int dex_bonus = character.getDEX();
			final int level = character.getLevel();

			//[Square(DEX)]*6 + lvl + weapon hitbonus;
			env.value += (Math.sqrt(dex_bonus) * 6 + EVASION_LEVEL_BONUS[level]) + bonus;
		}
	}
}