package com.l2cccp.gameserver.stats.funcs;

import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;

public class FuncSet extends Func
{
	public FuncSet(Stats stat, int order, Object owner, double value)
	{
		super(stat, order, owner, value);
	}

	@Override
	public void calc(Env env)
	{
		env.value = value;
	}
}
