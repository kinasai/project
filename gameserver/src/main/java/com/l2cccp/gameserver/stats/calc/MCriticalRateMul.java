package com.l2cccp.gameserver.stats.calc;

import com.l2cccp.gameserver.data.xml.holder.PlayerBalancerHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.balancing.player.BalancerType;
import com.l2cccp.gameserver.model.base.BaseStats;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.stats.funcs.Func;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class MCriticalRateMul extends Func
{
	private static final MCriticalRateMul _instance = new MCriticalRateMul();

	public static MCriticalRateMul init()
	{
		return _instance;
	}

	private MCriticalRateMul()
	{
		super(Stats.MCRITICAL_RATE, 0x10, null);
	}

	@Override
	public void calc(final Env env)
	{
		final BaseStats stats = BaseStats.WIT;
		final Creature character = env.character;
		final double wit_bonus = stats.calcBonus(character);

		double bonus = 1.0D;
		if(character != null)
		{
			final Player player = character.getPlayer();
			if(player != null)
			{
				final int id = player.getClassId().getId();
				final boolean olympiad = player.isInOlympiadMode();
				bonus = PlayerBalancerHolder.getInstance().getBalancer(BalancerType.magic_critical_attack, id, olympiad);
			}
		}

		env.value *= wit_bonus * bonus;
	}
}