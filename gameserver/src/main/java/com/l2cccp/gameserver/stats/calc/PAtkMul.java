package com.l2cccp.gameserver.stats.calc;

import com.l2cccp.gameserver.data.xml.holder.PlayerBalancerHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.balancing.player.BalancerType;
import com.l2cccp.gameserver.model.base.BaseStats;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.stats.funcs.Func;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PAtkMul extends Func
{
	private static final PAtkMul _instance = new PAtkMul();

	public static PAtkMul init()
	{
		return _instance;
	}

	private PAtkMul()
	{
		super(Stats.POWER_ATTACK, 0x20, null);
	}

	@Override
	public void calc(final Env env)
	{
		final BaseStats stats = BaseStats.STR;
		final Creature character = env.character;
		double bonus = stats.calcBonus(character);
		if(character != null)
		{
			bonus *= character.getLevelMod();

			final Player player = character.getPlayer();
			if(player != null)
			{
				final int id = player.getClassId().getId();
				final boolean olympiad = player.isInOlympiadMode();
				bonus *= PlayerBalancerHolder.getInstance().getBalancer(BalancerType.physical_attack, id, olympiad);
			}
		}

		env.value *= bonus;
	}
}