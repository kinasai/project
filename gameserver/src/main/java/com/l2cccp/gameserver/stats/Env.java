package com.l2cccp.gameserver.stats;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.skills.SkillEntry;

/**
 *
 * An Env object is just a class to pass parameters to a calculator such as L2Player,
 * L2ItemInstance, Initial value.
 *
 */
public final class Env
{
	public Creature character;
	public Creature target;
	public ItemInstance item;
	public SkillEntry skill;
	public double value;

	public Env()
	{}

	public Env(Creature cha, Creature tar, SkillEntry sk)
	{
		character = cha;
		target = tar;
		skill = sk;
	}
}
