package com.l2cccp.gameserver.stats.conditions;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.Race;
import com.l2cccp.gameserver.stats.Env;

public class ConditionPlayerRace extends Condition
{
	private final Race _race;

	public ConditionPlayerRace(String race)
	{
		_race = Race.valueOf(race.toLowerCase());
	}

	@Override
	protected boolean testImpl(Env env)
	{
		if(!env.character.isPlayer())
			return false;
		return ((Player) env.character).getRace() == _race;
	}
}