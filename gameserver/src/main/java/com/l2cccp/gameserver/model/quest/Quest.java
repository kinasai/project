package com.l2cccp.gameserver.model.quest;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.commons.logging.LogUtils;
import com.l2cccp.commons.util.TroveUtils;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.CharacterQuestDAO;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
import com.l2cccp.gameserver.data.xml.holder.QuestHolder;
import com.l2cccp.gameserver.instancemanager.QuestManager;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.entity.olympiad.OlympiadGame;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExQuestNpcLogList;
import com.l2cccp.gameserver.scripts.QuestLoader;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.HtmlUtils;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.set.hash.TIntHashSet;

public class Quest implements OnInitScriptListener
{
	private static final Logger _log = LoggerFactory.getLogger(Quest.class);

	public static final String SOUND_ITEMGET = "ItemSound.quest_itemget";
	public static final String SOUND_ACCEPT = "ItemSound.quest_accept";
	public static final String SOUND_MIDDLE = "ItemSound.quest_middle";
	public static final String SOUND_FINISH = "ItemSound.quest_finish";
	public static final String SOUND_GIVEUP = "ItemSound.quest_giveup";
	public static final String SOUND_TUTORIAL = "ItemSound.quest_tutorial";
	public static final String SOUND_JACKPOT = "ItemSound.quest_jackpot";
	public static final String SOUND_HORROR2 = "SkillSound5.horror_02";
	public static final String SOUND_BEFORE_BATTLE = "Itemsound.quest_before_battle";
	public static final String SOUND_FANFARE_MIDDLE = "ItemSound.quest_fanfare_middle";
	public static final String SOUND_FANFARE2 = "ItemSound.quest_fanfare_2";
	public static final String SOUND_BROKEN_KEY = "ItemSound2.broken_key";
	public static final String SOUND_ENCHANT_SUCESS = "ItemSound3.sys_enchant_sucess";
	public static final String SOUND_ENCHANT_FAILED = "ItemSound3.sys_enchant_failed";
	public static final String SOUND_ED_CHIMES05 = "AmdSound.ed_chimes_05";
	public static final String SOUND_ARMOR_WOOD_3 = "ItemSound.armor_wood_3";
	public static final String SOUND_ITEM_DROP_EQUIP_ARMOR_CLOTH = "ItemSound.item_drop_equip_armor_cloth";

	public static final String NO_QUEST_DIALOG = "no-quest";

	public static final int ADENA_ID = 57;

	public static final int PARTY_NONE = 0;
	public static final int PARTY_ONE = 1;
	public static final int PARTY_ALL = 2;
	public static final int PARTY_ANY = 3; // Специальный тип аналогичный PARTY_ONE, но вызывается если в пати есть хотя бы кто-то с квестом. Используется в квесте 350 (прокачка СА)

	//карта с приостановленными квестовыми таймерами для каждого игрока
	private Map<Integer, Map<String, QuestTimer>> _pausedQuestTimers = new ConcurrentHashMap<Integer, Map<String, QuestTimer>>();

	private TIntHashSet _questItems = new TIntHashSet();
	private TIntObjectHashMap<List<QuestNpcLogInfo>> _npcLogList = TroveUtils.emptyIntObjectMap();

	private QuestTemplate _template;
	protected final int _questId;

	public int satrtNpc;

	public final static int CREATED = 1;
	public final static int STARTED = 2;
	public final static int COMPLETED = 3;
	public final static int DELAYED = 4;

	/**
	 * Deprecated.
	 */
	public Quest(boolean party) //FIXME: удалить!!!!
	{
		this();
	}

	/**
	 * 0 - по ластхиту, 1 - случайно по пати, 2 - всей пати.
	 */
	public Quest(int party) //FIXME: удалить!!!!
	{
		this();
	}

	public Quest()
	{
		_questId = Integer.parseInt(getClass().getSimpleName().split("_")[1]);
		QuestLoader.getInstance().add(this);
	}

	public void init()
	{
		_template = QuestHolder.getInstance().get(getId());
		if(_template == null || _template.getId() != getId())
		{
			_log.warn("Incorrect quest template [" + getId() + "] " + getName());
			throw new NullPointerException();
		}

		for(Entry<QuestEventType, List<Integer>> s : _template.getEvents().entrySet())
		{
			final QuestEventType type = s.getKey();
			for(int id : s.getValue())
				addEventId(id, type);
		}

		for(final int id : _template.getItems())
		{
			final ItemTemplate item = ItemHolder.getInstance().getTemplate(id);

			if(item == null)
				_log.warn("Item [" + id + "] is missing [" + getId() + "] " + getName());
			if(_questItems.contains(id))
				_log.warn("Item [" + item.getItemId() + "] " + item.getName() + " multiple times in quest drop in [" + getId() + "] " + getName());
			else if(id <= 0)
				_log.warn("Item [" + item.getItemId() + "] " + item.getName() + " is incorrect [" + getId() + "] " + getName());
			else
				_questItems.add(id);
		}
	}

	public List<QuestNpcLogInfo> getNpcLogList(int cond)
	{
		return _npcLogList.get(cond);
	}

	@Deprecated
	public void addAttackId(int... attackIds)
	{}

	/**
	 * Add this quest to the list of quests that the passed mob will respond to
	 * for the specified Event type.<BR>
	 * <BR>
	 *
	 * @param npcId : id of the NPC to register
	 * @param eventType : type of event being registered
	 * @return int : npcId
	 */
	public NpcTemplate addEventId(int npcId, QuestEventType eventType)
	{
		try
		{
			NpcTemplate t = NpcHolder.getInstance().getTemplate(npcId);

			if(t != null)
				t.addQuestEvent(eventType, this);
			else
				_log.error("Can't find npc ID: " + npcId + " for quest [" + getId() + "] " + getName());
			return t;
		}
		catch(Exception e)
		{
			_log.error("", e);
			return null;
		}
	}

	@Deprecated
	public void addKillId(int... killIds)
	{}

	/**
	 * Добавляет нпц масив для слушателя при их убийстве, и обновлении пакетом {@link com.l2cccp.gameserver.network.l2.s2c.ExQuestNpcLogList}
	 * @param cond
	 * @param varName
	 * @param killIds
	 */
	public void addKillNpcWithLog(int cond, String varName, int max, int... killIds)
	{
		if(killIds.length == 0)
			throw new IllegalArgumentException("Npc list cant be empty!");

		//	addKillId(killIds);
		if(_npcLogList.isEmpty())
			_npcLogList = new TIntObjectHashMap<List<QuestNpcLogInfo>>(5);

		List<QuestNpcLogInfo> vars = _npcLogList.get(cond);
		if(vars == null)
			_npcLogList.put(cond, (vars = new ArrayList<QuestNpcLogInfo>(5)));

		vars.add(new QuestNpcLogInfo(killIds, varName, max));
	}

	public boolean updateKill(NpcInstance npc, QuestState st)
	{
		Player player = st.getPlayer();
		if(player == null)
			return false;
		List<QuestNpcLogInfo> vars = getNpcLogList(st.getCond());
		if(vars == null)
			return false;
		boolean done = true;
		boolean find = false;
		for(QuestNpcLogInfo info : vars)
		{
			int count = st.getInt(info.getVarName());
			if(!find && ArrayUtils.contains(info.getNpcIds(), npc.getNpcId()))
			{
				find = true;
				if(count < info.getMaxCount())
				{
					st.set(info.getVarName(), ++count);
					player.sendPacket(new ExQuestNpcLogList(st));
				}
			}

			if(count != info.getMaxCount())
				done = false;
		}

		return done;
	}

	@Deprecated
	public void addKillId(Collection<Integer> killIds)
	{}

	@Deprecated
	public void addStartNpc(int... npcIds)
	{}

	@Deprecated
	public void addFirstTalkId(int... npcIds)
	{}

	@Deprecated
	public void addTalkId(int... talkIds)
	{}

	@Deprecated
	public void addTalkId(Collection<Integer> talkIds)
	{}

	/**
	 * Возвращает название квеста (Берется с npcstring-*.dat)
	 * state 1 = ""
	 * state 2 = "In Progress"
	 * state 3 = "Done"
	 */
	public String getDescr(Player player)
	{
		if(!isVisible())
			return null;

		QuestState qs = player.getQuestState(this);
		int state = 2;
		if(qs == null || qs.isCreated() && qs.isNowAvailable())
			state = 1;
		else if(qs.isCompleted() || !qs.isNowAvailable())
			state = 3;

		int fStringId = getId();
		if(fStringId >= 10000)
			fStringId -= 5000;
		fStringId = fStringId * 100 + state;
		return HtmlUtils.htmlNpcString(fStringId);
	}

	/**
	 * Return name of the quest on Eng
	 * @return String
	 */
	public String getName() // Метод под старый метод, по дефолту берем ангю язык для логов!
	{
		return getName(Language.ENGLISH);
	}

	/**
	 * Return name of the quest
	 * @return String
	 */
	public String getName(final Language lang)
	{
		return _template.getName(lang);
	}

	/**
	 * Return ID of the quest
	 * @return int
	 */
	public int getId()
	{
		return _questId;
	}

	/**
	 * Return party state of quest
	 * @return String
	 */
	public int getParty()
	{
		return _template.getType().getId();
	}

	/**
	 * Add a new QuestState to the database and return it.
	 * @param player
	 * @param state
	 * @return QuestState : QuestState created
	 */
	public QuestState newQuestState(Player player, int state)
	{
		QuestState qs = new QuestState(this, player, state);
		CharacterQuestDAO.getInstance().replace(qs);
		return qs;
	}

	public QuestState newQuestStateAndNotSave(Player player, int state)
	{
		return new QuestState(this, player, state);
	}

	public void notifyAttack(NpcInstance npc, QuestState qs)
	{
		String res = null;
		try
		{
			res = onAttack(npc, qs);
		}
		catch(Exception e)
		{
			showError(qs.getPlayer(), e);
			return;
		}
		showResult(npc, qs.getPlayer(), res);
	}

	public void notifyDeath(Creature killer, Creature victim, QuestState qs)
	{
		String res = null;
		try
		{
			res = onDeath(killer, victim, qs);
		}
		catch(Exception e)
		{
			showError(qs.getPlayer(), e);
			return;
		}
		showResult(null, qs.getPlayer(), res);
	}

	public void notifyEvent(String event, QuestState qs, NpcInstance npc)
	{
		String res = null;
		try
		{
			res = onEvent(event, qs, npc);
		}
		catch(Exception e)
		{
			showError(qs.getPlayer(), e);
			return;
		}
		showResult(npc, qs.getPlayer(), res);
	}

	public void notifyKill(NpcInstance npc, QuestState qs)
	{
		String res = null;
		try
		{
			res = onKill(npc, qs);
		}
		catch(Exception e)
		{
			showError(qs.getPlayer(), e);
			return;
		}
		showResult(npc, qs.getPlayer(), res);
	}

	public void notifyKill(Player target, QuestState qs)
	{
		String res = null;
		try
		{
			res = onKill(target, qs);
		}
		catch(Exception e)
		{
			showError(qs.getPlayer(), e);
			return;
		}
		showResult(null, qs.getPlayer(), res);
	}

	/**
	 * Override the default NPC dialogs when a quest defines this for the given NPC
	 */
	public final boolean notifyFirstTalk(NpcInstance npc, Player player)
	{
		String res = null;
		try
		{
			res = onFirstTalk(npc, player);
		}
		catch(Exception e)
		{
			showError(player, e);
			return true;
		}
		// if the quest returns text to display, display it. Otherwise, use the default npc text.
		return showResult(npc, player, res, true);
	}

	public boolean notifyTalk(NpcInstance npc, QuestState qs)
	{
		String res = null;
		try
		{
			res = onTalk(npc, qs);
		}
		catch(Exception e)
		{
			showError(qs.getPlayer(), e);
			return true;
		}
		return showResult(npc, qs.getPlayer(), res);
	}

	public boolean notifySkillUse(NpcInstance npc, Skill skill, QuestState qs)
	{
		String res = null;
		try
		{
			res = onSkillUse(npc, skill, qs);
		}
		catch(Exception e)
		{
			showError(qs.getPlayer(), e);
			return true;
		}
		return showResult(npc, qs.getPlayer(), res);
	}

	public void notifyCreate(QuestState qs)
	{
		try
		{
			onCreate(qs);
		}
		catch(Exception e)
		{
			showError(qs.getPlayer(), e);
		}
	}

	public void onCreate(QuestState qs)
	{}

	public String onAttack(NpcInstance npc, QuestState qs)
	{
		return null;
	}

	public String onDeath(Creature killer, Creature victim, QuestState qs)
	{
		return null;
	}

	public String onEvent(String event, QuestState qs, NpcInstance npc)
	{
		return null;
	}

	public String onKill(NpcInstance npc, QuestState qs)
	{
		return null;
	}

	public String onKill(Player killed, QuestState st)
	{
		return null;
	}

	public String onFirstTalk(NpcInstance npc, Player player)
	{
		return null;
	}

	public String onTalk(NpcInstance npc, QuestState qs)
	{
		return null;
	}

	public String onSkillUse(NpcInstance npc, Skill skill, QuestState qs)
	{
		return null;
	}

	public void onOlympiadEnd(OlympiadGame og, QuestState qs)
	{}

	public void onAbort(QuestState qs)
	{}

	public boolean canAbortByPacket()
	{
		return true;
	}

	/**
	 * Show message error to player who has an access level greater than 0
	 * @param player : L2Player
	 * @param t : Throwable
	 */
	private void showError(Player player, Throwable t)
	{
		_log.error("", t);
		if(player != null && player.isGM())
		{
			String res = "<html><body><title>Script error</title>" + LogUtils.dumpStack(t).replace("\n", "<br>") + "</body></html>";
			showResult(null, player, res);
		}
	}

	protected void showHtmlFile(Player player, String fileName, boolean showQuestInfo)
	{
		showHtmlFile(player, fileName, showQuestInfo, ArrayUtils.EMPTY_OBJECT_ARRAY);
	}

	protected void showHtmlFile(Player player, String fileName, boolean showQuestInfo, Object... arg)
	{
		if(player == null)
			return;

		GameObject target = player.getTarget();
		HtmlMessage npcReply = new HtmlMessage(target == null ? 5 : target.getObjectId());
		if(showQuestInfo)
			npcReply.setQuestId(getId());
		npcReply.setFile("quests/" + getClass().getSimpleName() + "/" + fileName);

		if(arg.length % 2 == 0)
			for(int i = 0; i < arg.length; i += 2)
				npcReply.replace(String.valueOf(arg[i]), String.valueOf(arg[i + 1]));

		player.sendPacket(npcReply);
	}

	protected void showSimpleHtmFile(Player player, String fileName)
	{
		if(player == null)
			return;

		HtmlMessage npcReply = new HtmlMessage(5);
		npcReply.setFile(fileName);
		player.sendPacket(npcReply);
	}

	/**
	 * Show a message to player.<BR><BR>
	 * <U><I>Concept : </I></U><BR>
	 * 3 cases are managed according to the value of the parameter "res" :<BR>
	 * <LI><U>"res" ends with string ".html" :</U> an HTML is opened in order to be shown in a dialog box</LI>
	 * <LI><U>"res" starts with tag "html" :</U> the message hold in "res" is shown in a dialog box</LI>
	 * <LI><U>"res" is null :</U> do not show any message</LI>
	 * <LI><U>"res" is empty string :</U> show default message</LI>
	 * <LI><U>otherwise :</U> the message hold in "res" is shown in chat box</LI>
	 * @param npc
	 * @param player
	 * @param res : String pointing out the message to show at the player
	 */
	private boolean showResult(NpcInstance npc, Player player, String res)
	{
		return showResult(npc, player, res, false);
	}

	private boolean showResult(NpcInstance npc, Player player, String res, boolean isFirstTalk)
	{
		boolean showQuestInfo = showQuestInfo(player);
		if(isFirstTalk)
			showQuestInfo = false;
		if(res == null) // do not show message
			return true;
		if(res.isEmpty()) // show default npc message
			return false;
		if(res.startsWith("no_quest") || res.equalsIgnoreCase("noquest") || res.equalsIgnoreCase(NO_QUEST_DIALOG))
			showSimpleHtmFile(player, "no-quest.htm");
		else if(res.equalsIgnoreCase("completed"))
			showSimpleHtmFile(player, "completed-quest.htm");
		else if(res.endsWith(".htm"))
			showHtmlFile(player, res, showQuestInfo);
		else
		{
			HtmlMessage npcReply = new HtmlMessage(npc == null ? 5 : npc.getObjectId());
			npcReply.setHtml(res);
			if(showQuestInfo)
				npcReply.setQuestId(getId());
			player.sendPacket(npcReply);
		}
		return true;
	}

	// Проверяем, показывать ли информацию о квесте в диалоге.
	private boolean showQuestInfo(Player player)
	{
		QuestState qs = player.getQuestState(this);
		if(qs != null && qs.getState() != CREATED)
			return false;
		if(!isVisible())
			return false;

		return true;
	}

	// Останавливаем и сохраняем таймеры (при выходе из игры)
	void pauseQuestTimers(QuestState qs)
	{
		if(qs.getTimers().isEmpty())
			return;

		for(QuestTimer timer : qs.getTimers().values())
		{
			timer.setQuestState(null);
			timer.pause();
		}

		_pausedQuestTimers.put(qs.getPlayer().getObjectId(), qs.getTimers());
	}

	// Восстанавливаем таймеры (при входе в игру)
	void resumeQuestTimers(QuestState qs)
	{
		Map<String, QuestTimer> timers = _pausedQuestTimers.remove(qs.getPlayer().getObjectId());
		if(timers == null)
			return;

		qs.getTimers().putAll(timers);

		for(QuestTimer timer : qs.getTimers().values())
		{
			timer.setQuestState(qs);
			timer.start();
		}
	}

	protected String str(long i)
	{
		return String.valueOf(i);
	}

	// =========================================================
	//  QUEST SPAWNS
	// =========================================================

	public NpcInstance addSpawn(int npcId, int x, int y, int z, int heading, int randomOffset, int despawnDelay)
	{
		return addSpawn(npcId, new Location(x, y, z, heading), randomOffset, despawnDelay);
	}

	public NpcInstance addSpawn(int npcId, Location loc, int randomOffset, int despawnDelay)
	{
		return NpcUtils.spawnSingle(npcId, randomOffset > 50 ? Location.findPointToStay(loc, 0, randomOffset, ReflectionManager.DEFAULT.getGeoIndex()) : loc, despawnDelay);
	}

	/**
	 * Добавляет спаун с числовым значением разброса - от 50 до randomOffset.
	 * Если randomOffset указан мене 50, то координаты не меняются.
	 */
	public static NpcInstance addSpawnToInstance(int npcId, int x, int y, int z, int heading, int randomOffset, int refId)
	{
		return addSpawnToInstance(npcId, new Location(x, y, z, heading), randomOffset, refId);
	}

	public static NpcInstance addSpawnToInstance(int npcId, Location loc, int randomOffset, int refId)
	{
		try
		{
			NpcTemplate template = NpcHolder.getInstance().getTemplate(npcId);
			if(template != null)
			{
				NpcInstance npc = NpcHolder.getInstance().getTemplate(npcId).getNewInstance();
				npc.setReflection(refId);
				npc.setSpawnedLoc(randomOffset > 50 ? Location.findPointToStay(loc, 50, randomOffset, npc.getGeoIndex()) : loc);
				npc.spawnMe(npc.getSpawnedLoc());
				return npc;
			}
		}
		catch(Exception e1)
		{
			_log.warn("Could not spawn Npc " + npcId);
		}
		return null;
	}

	public static String getStateName(int state)
	{
		switch(state)
		{
			case CREATED:
				return "Start";
			case STARTED:
				return "Started";
			case COMPLETED:
				return "Completed";
			case DELAYED:
				return "Delayed";
		}
		return "Start";
	}

	public static int getStateId(String state)
	{
		if(state.equalsIgnoreCase("Start"))
			return CREATED;
		else if(state.equalsIgnoreCase("Started"))
			return STARTED;
		else if(state.equalsIgnoreCase("Completed"))
			return COMPLETED;
		else if(state.equalsIgnoreCase("Delayed"))
			return DELAYED;
		return CREATED;
	}

	@Deprecated
	public void addQuestItem(int... ids)
	{}

	public int[] getItems()
	{
		return _questItems.toArray();
	}

	public boolean isQuestItem(int id)
	{
		return _questItems.contains(id);
	}

	public boolean isVisible()
	{
		return true;
	}

	public boolean isUnderLimit()
	{
		return false;
	}

	public double getRateExp()
	{
		return _template.getExp();
	}

	public double getRateSp()
	{
		return _template.getSp();
	}

	public double getRateDrop()
	{
		return _template.getDrop();
	}

	public double getRateReward()
	{
		return _template.getReward();
	}

	public MultiValueSet<String> getParam()
	{
		return _template.getParam();
	}

	@Override
	public void onInit()
	{
		if(!Config.DONTLOADQUEST)
			QuestManager.addQuest(this);
	}
}