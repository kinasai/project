package com.l2cccp.gameserver.model.entity.events;

import com.l2cccp.commons.lang.reference.HardReference;
import com.l2cccp.commons.lang.reference.HardReferences;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.scripts.Functions;
import com.l2cccp.gameserver.utils.Location;

public abstract class GameEvent {
	public HardReference<Player> self = HardReferences.emptyRef();
	public HardReference<NpcInstance> npc = HardReferences.emptyRef();
	public static final int STATE_INACTIVE = 0;
	public static final int STATE_ACTIVE = 1;
	public static final int STATE_RUNNING = 2;

	public int getState() {
		return 0;
	}

	public abstract String getName();
	public abstract String minLvl();
	public abstract String maxLvl();

	public long getNextTime() {
		return 0;
	}

	public boolean isRunning() {
		return getState() == 2;
	}

	public boolean canRegister(Player player, boolean first) {
		return (getState() == 1) && (!isParticipant(player)) && (player.getEvents() == null);
	}

	public abstract boolean isParticipant(Player paramPlayer);
	public abstract boolean register(Player paramPlayer);
	public abstract void unreg(Player paramPlayer);
	public abstract void remove(Player paramPlayer);
	public abstract void start();
	public abstract void finish();
	public abstract void abort();

	public boolean canAttack(Creature attacker, Creature target) {
		return true;
	}

	public boolean checkPvP(Creature attacker, Creature target) {
		return getState() != 2;
	}

	public boolean canUseItem(Player actor, ItemInstance item) {
		return true;
	}

	public boolean canUseSkill(Creature caster, Creature target, Skill skill) {
		return true;
	}

	public boolean canTeleportOnDie(Player player) {
		return getState() != 2;
	}

	public boolean canLostExpOnDie() {
		return getState() != 2;
	}

	public int getCountPlayers() {
		return 0;
	}

	public StringBuffer getInformation(Player player) {
		return null;
	}

	public boolean talkWithNpc(Player player, NpcInstance npc) {
		return false;
	}

	public static void unRide(Player player) {
		Functions.unRide(player);
	}

	public static void unSummonPet(Player player, boolean onlyPets) {
		Functions.unSummonPet(player, onlyPets);
	}
	
	public Player getSelf() {
		return self.get();
	}

	public NpcInstance getNpc() {
		return npc.get();
	}

	public static long getItemCount(Playable playable, int itemId) {
		return Functions.getItemCount(playable, itemId);
	}

	public static boolean removeItem(Playable playable, int itemId, long count) {
		return Functions.removeItem(playable, itemId, count);
	}

	public static void addItem(Playable playable, int itemId, long count) {
		Functions.addItem(playable, itemId, count);
	}

	public static void addItem(Playable playable, int itemId, long count, boolean mess) {
		Functions.addItem(playable, itemId, count, mess);
	}

	public static void show(CustomMessage message, Player self) {
		Functions.show(message, self);
	}

	public static NpcInstance spawn(Location loc, int npcId) {
		return Functions.spawn(loc, npcId);
	}

	public static NpcInstance spawn(Location loc, int npcId, Reflection reflection) {
		return Functions.spawn(loc, npcId, reflection);
	}
}