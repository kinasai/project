package com.l2cccp.gameserver.model.bbs.teleport;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TeleportPoint
{
	private final int id;
	private final int subsection;

	private Map<PriceType, Price> prices = new EnumMap<PriceType, Price>(PriceType.class);
	private Map<Language, String> names = new HashMap<Language, String>();

	// Settings
	private int min;
	private int max;
	private boolean premium;
	private boolean pk;
	private boolean noblesse;

	// Cords
	private int x;
	private int y;
	private int z;

	public TeleportPoint(final int id, final int subsection)
	{
		this.id = id;
		this.subsection = subsection;
	}

	public void initLevels(final int min, final int max)
	{
		this.min = min;
		this.max = max;
	}

	public void initStates(final boolean premium, final boolean pk, final boolean noblesse)
	{
		this.premium = premium;
		this.pk = pk;
		this.noblesse = noblesse;
	}

	public void initCost(final PriceType type, final Price price)
	{
		prices.put(type, price);
	}

	public void initName(final Language lang, final String string)
	{
		names.put(lang, string);
	}

	public void initCoords(int x, int y, int z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getId()
	{
		return id;
	}

	public int getSubsection()
	{
		return subsection;
	}

	public String getName(final Language lang)
	{
		return names.get(lang);
	}

	public Price getPrice(final PriceType type)
	{
		return prices.get(type);
	}

	public boolean checkLevel(final int level)
	{
		return level >= min && level <= max;
	}

	public boolean canPkUse()
	{
		return !pk;
	}

	public boolean isPremiumOnly()
	{
		return premium;
	}

	public boolean isNoblesseOnly()
	{
		return noblesse;
	}

	public int getMinLevel()
	{
		return min;
	}

	public int getMaxLevel()
	{
		return max;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public int getZ()
	{
		return z;
	}
}