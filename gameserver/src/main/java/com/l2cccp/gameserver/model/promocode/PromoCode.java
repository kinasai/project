package com.l2cccp.gameserver.model.promocode;

import java.util.List;

public class PromoCode
{
	private final int _limit;
	private final long _fromDate;
	private final long _toDate;
	private final List<PromoCodeReward> _rewards;
	private final boolean _limitByUser;

	public PromoCode(int limit, long from, long to, List<PromoCodeReward> rewards, boolean limitByUser)
	{
		_limit = limit;
		_fromDate = from;
		_toDate = to;
		_rewards = rewards;
		_limitByUser = limitByUser;
	}

	public long getFromDate()
	{
		return _fromDate;
	}

	public long getToDate()
	{
		return _toDate;
	}

	public List<PromoCodeReward> getRewards()
	{
		return _rewards;
	}

	public int getLimit()
	{
		return _limit;
	}

	public boolean isLimitByUser()
	{
		return _limitByUser;
	}
}
