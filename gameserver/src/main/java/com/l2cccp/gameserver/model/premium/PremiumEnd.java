package com.l2cccp.gameserver.model.premium;

import com.l2cccp.gameserver.dao.PremiumDAO;
import com.l2cccp.gameserver.model.Party;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.Bonus;
import com.l2cccp.gameserver.network.l2.s2c.ExBR_PremiumState;

public class PremiumEnd
{
	private static PremiumEnd _instance = new PremiumEnd();

	public static PremiumEnd getInstance()
	{
		return _instance;
	}

	/**
	 * Откат рейтов после истечения па!
	 * @param player
	 */
	public void resetBonuses(final Player player, final PremiumAccount premium)
	{
		final Bonus bonus = player.getBonus();
		bonus.setRateXp(premium.getExp(), false);
		bonus.setRateSp(premium.getSp(), false);
		bonus.setDropSiege(premium.getEpaulette(), false);
		bonus.setDropAdena(premium.getAdena(), false);
		bonus.setDropItems(premium.getItems(), false);
		bonus.setDropSpoil(premium.getSpoil(), false);
		bonus.setWeight(premium.getWeight(), false);
		bonus.setCraftChance(premium.getCraftChance(), false);
		bonus.setMasterWorkChance(premium.getMasterWorkChance(), false);

		if(premium.canShout())
			bonus.setShoutChat(false);
		if(premium.canHeroVoice())
			bonus.setHeroChat(false);

		final Party party = player.getParty();
		if(party != null)
			party.recalculatePartyData();

		premium.removeGifts(player);
		bonus.remove(premium);
		PremiumDAO.getInstance().delete(player, premium.getId());
		player.sendPacket(new ExBR_PremiumState(player, bonus.checkPremium()));
	}
}