package com.l2cccp.gameserver.model.bbs.buffer;

import java.util.LinkedHashMap;
import java.util.Map;

public class Scheme
{
	private String name;
	private Map<Integer, Integer> buffs = new LinkedHashMap<Integer, Integer>();

	public Scheme(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void addBuff(int id, int level)
	{
		buffs.put(id, level);
	}

	public Map<Integer, Integer> getBuffs()
	{
		return buffs;
	}
}