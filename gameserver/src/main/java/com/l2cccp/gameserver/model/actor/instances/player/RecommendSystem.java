package com.l2cccp.gameserver.model.actor.instances.player;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.data.xml.holder.RecommendsHolder;
import com.l2cccp.gameserver.model.GameObjectTasks.RecommendBonusTask;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.s2c.ExVoteSystemInfo;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RecommendSystem
{
	private Player _player;
	private int _recommendsHave;
	private int _recommendsLeft = 20;
	private int _recommendsLeftToday;
	private int _bonusTime = 3600;
	private ScheduledFuture<?> _bonusTask;
	private boolean _isActive;

	public RecommendSystem(Player player)
	{
		_player = player;
	}

	public void setStats(int rec_have, int rec_left, int rec_left_today, int bonus_time)
	{
		_recommendsHave = rec_have;
		_recommendsLeft = rec_left;
		_recommendsLeftToday = rec_left_today;
		_bonusTime = bonus_time;
	}

	public int getRecommendsHave()
	{
		return _recommendsHave;
	}

	public void setRecommendsHave(int value)
	{
		if(value > 255)
			_recommendsHave = 255;
		else if(value < 0)
			_recommendsHave = 0;
		else
			_recommendsHave = value;
	}

	public void addRecommendsHave(int val)
	{
		setRecommendsHave(_recommendsHave + val);
		_player.broadcastUserInfo(true);
		sendInfo();
	}

	public int getRecommendsLeft()
	{
		return _recommendsLeft;
	}

	public void setRecommendLeft(int value)
	{
		if(value > 255)
			_recommendsLeft = 255;
		else if(value < 0)
			_recommendsLeft = 0;
		else
			_recommendsLeft = value;
	}

	public int addRecommendsLeft()
	{
		int recoms = 1;
		if(_recommendsLeftToday < 20)
			recoms = 10;
		setRecommendLeft(_recommendsLeft + recoms);
		_recommendsLeftToday += recoms;
		_player.sendUserInfo(true);
		return recoms;
	}

	public void setRecommendLeftToday(final int value)
	{
		_recommendsLeftToday = value;
		_player.setVar("recLeftToday", String.valueOf(_recommendsLeftToday), -1);
	}

	public void giveRecommend(Player target)
	{
		final RecommendSystem system = target.getRecommendSystem();
		if(system == null)
			return;

		if(system.getRecommendsHave() < 255)
			system.addRecommendsHave(1);

		if(getRecommendsLeft() > 0)
			setRecommendLeft(_recommendsLeft - 1);

		_player.sendUserInfo(true);
		sendInfo();
	}

	public int getBonusTime()
	{
		if(_bonusTask != null)
			return (int) Math.max(0, _bonusTask.getDelay(TimeUnit.SECONDS));
		return _bonusTime;
	}

	public void setBonusTime(int val)
	{
		_bonusTime = val;
	}

	public int getBonusVal()
	{
		if(getBonusTime() > 0 || _player.isHourglassEffected())
			return RecommendBonus.getBonus(_player);
		return 0;
	}

	public double getBonusMod()
	{
		if(getBonusTime() > 0 || _player.isHourglassEffected())
			return RecommendBonus.getMultiplier(_player);
		return 1;
	}

	public void startBonusSystem()
	{
		check();
		sendInfo();
	}

	public boolean isActive()
	{
		return _isActive;
	}

	public void setActive(boolean val)
	{
		if(_isActive == val)
			return;

		_isActive = val;
		if(val)
			startBonusTask();
		else
			stopBonusTask(true);

		sendInfo();
	}

	public void startBonusTask()
	{
		if(_bonusTask == null && getBonusTime() > 0 && !_player.isHourglassEffected() && _isActive)
			_bonusTask = ThreadPoolManager.getInstance().schedule(new RecommendBonusTask(_player), getBonusTime() * 1000);
	}

	public void stopBonusTask(boolean saveTime)
	{
		if(_bonusTask != null)
		{
			if(saveTime)
				_bonusTime = (int) Math.max(0, _bonusTask.getDelay(TimeUnit.SECONDS));
			_bonusTask.cancel(false);
			_bonusTask = null;
		}
	}

	private void check()
	{
		final long time = RecommendsHolder.getInstance().getTime();
		long count = Math.round((System.currentTimeMillis() / 1000 - _player.getLogoutTime()) / 86400);
		if(count == 0 && _player.getLogoutTime() < time / 1000 && System.currentTimeMillis() > time)
			count++;

		for(int i = 1; i < count; i++)
			setRecommendsHave(_recommendsHave - 20);

		if(count > 0)
			restart();
	}

	public void restart()
	{
		_bonusTime = 3600;
		setRecommendLeft(20);
		setRecommendLeftToday(0);
		setRecommendsHave(_recommendsHave - 20);
		stopBonusTask(false);
		startBonusTask();
		_player.sendUserInfo(true);
		sendInfo();
	}

	public void sendInfo()
	{
		_player.sendPacket(new ExVoteSystemInfo(_player));
	}
}