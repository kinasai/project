package com.l2cccp.gameserver.model.pledge;

public class ClanUtil
{

	public static boolean isNotOne(Clan one, Clan two)
	{
		if(one != null && two != null && one.getClanId() == two.getClanId())
			return false;
		else if(one != null && two != null && one.getAlliance() != null && two.getAlliance() != null && one.getAlliance().getAllyId() == two.getAlliance().getAllyId())
			return false;
		else
			return true;
	}
}
