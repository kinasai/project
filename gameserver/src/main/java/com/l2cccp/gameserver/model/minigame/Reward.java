package com.l2cccp.gameserver.model.minigame;

public final class Reward
{
	private int _id;
	private String _name;
	private String _icon;
	private long _count;
	private int _enchant;

	public int getId()
	{
		return _id;
	}

	public String getName()
	{
		return _name;
	}

	public String getIcon()
	{
		return _icon;
	}

	public long getCount()
	{
		return _count;
	}

	public int getEnchantLevel()
	{
		return _enchant;
	}

	public void setId(int _id)
	{
		this._id = _id;
	}

	public void setName(String _name)
	{
		this._name = _name;
	}

	public void setIcon(String _icon)
	{
		this._icon = _icon;
	}

	public void setCount(long _count)
	{
		this._count = _count;
	}

	public void setEnchantLevel(int _enchant)
	{
		this._enchant = _enchant;
	}

	public String toString()
	{
		if(_enchant > 0)
			return "+" + _enchant + " " + _name;
		else if(_count > 1)
			return _count + " " + _name;
		else
			return _name;
	}
}
