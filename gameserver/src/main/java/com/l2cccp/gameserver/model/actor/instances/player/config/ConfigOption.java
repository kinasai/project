package com.l2cccp.gameserver.model.actor.instances.player.config;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.CharacterConfigurationDAO;
import com.l2cccp.gameserver.model.Player;

/**
 * 
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ConfigOption
{
	// Сессионное хранение
	private static Map<Integer, PlayerConfig> list = new HashMap<Integer, PlayerConfig>();

	/**
	 * Создание конфига
	 * @param obj
	 * @return
	 */
	public static PlayerConfig create(int obj)
	{
		PlayerConfig config = new PlayerConfig(obj);
		config.setTranslit("Off");
		config.setAutoLoot(Config.AUTO_LOOT ? 1 : 0);
		config.setAutoLootHerb(Config.AUTO_LOOT_HERBS ? true : false);
		config.setBuffDistance(1500);
		config.setExpOff(false);
		config.setHideTraders(false);
		config.setClassicShift(false);
		config.setRain(false);
		config.setImages(false);
		config.setHideVisual(false);
		config.setNoCarrier(Config.SERVICES_ENABLE_NO_CARRIER ? Config.SERVICES_NO_CARRIER_MIN_TIME : 0);
		add(config);

		CharacterConfigurationDAO.getInstance().saveConfig(config);
		return config;
	}

	public static PlayerConfig get(int obj)
	{
		if(list.containsKey(obj))
			return list.get(obj);
		else
			return CharacterConfigurationDAO.getInstance().loadConfig(obj);
	}

	public static void applyConfig(Player player)
	{
		PlayerConfig config = player.getConfig();
		player.setDisableFogAndRain(config.showRain());
		player.setBuffAnimRange(config.getBuffDistance());
		player.setNotShowTraders(config.isHideTraders());
		player.setForceVisualFlag(config.isHideVisual());
	}

	public static void add(PlayerConfig config)
	{
		list.put(config.getObj(), config);
	}
}