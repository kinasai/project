package com.l2cccp.gameserver.model.bbs.teleport;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Price
{
	private final int item;
	private final long count;

	public Price(final int item, final long count)
	{
		this.item = item;
		this.count = count;
	}

	public int getItem()
	{
		return item;
	}

	public long getCount()
	{
		return count;
	}
}