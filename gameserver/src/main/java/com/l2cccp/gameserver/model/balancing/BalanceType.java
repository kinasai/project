package com.l2cccp.gameserver.model.balancing;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum BalanceType
{
	ALL,
	ID,
	EQUIP,
	STACK;
}