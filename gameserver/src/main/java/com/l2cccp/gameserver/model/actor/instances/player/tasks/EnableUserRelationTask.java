package com.l2cccp.gameserver.model.actor.instances.player.tasks;

import com.l2cccp.commons.lang.reference.HardReference;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.impl.SiegeEvent;

/**
 * @author VISTALL
 * @date 17:29/03.10.2011
 */
public class EnableUserRelationTask extends RunnableImpl
{
	private HardReference<Player> _playerRef;
	private SiegeEvent<?, ?> _siegeEvent;

	public EnableUserRelationTask(Player player, SiegeEvent<?, ?> siegeEvent)
	{
		_siegeEvent = siegeEvent;
		_playerRef = player.getRef();
	}

	@Override
	public void runImpl() throws Exception
	{
		Player player = _playerRef.get();
		if(player == null)
			return;

		_siegeEvent.removeBlockFame(player);

		player.stopEnableUserRelationTask();
		player.broadcastUserInfo(true);
	}
}
