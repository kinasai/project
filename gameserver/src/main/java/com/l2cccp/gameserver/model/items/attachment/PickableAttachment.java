package com.l2cccp.gameserver.model.items.attachment;

import com.l2cccp.gameserver.model.Player;

/**
 * @author VISTALL
 * @date 0:50/04.06.2011
 */
public interface PickableAttachment extends ItemAttachment
{
	boolean canPickUp(Player player);

	void pickUp(Player player);
}
