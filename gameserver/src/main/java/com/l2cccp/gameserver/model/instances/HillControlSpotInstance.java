package com.l2cccp.gameserver.model.instances;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.impl.KingHillEvent;
import com.l2cccp.gameserver.model.entity.events.objects.HillControlSpotObject;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class HillControlSpotInstance extends NpcInstance
{
	private static final SkillEntry TAKE_CONTROL_POINT_SKILL = SkillTable.getInstance().getSkillEntry(40001, 1);

	private final HillControlSpotObject _twControlSpotObject;

	public HillControlSpotInstance(int objectId, NpcTemplate template, HillControlSpotObject object)
	{
		super(objectId, template);
		_twControlSpotObject = object;
	}

	@Override
	protected void onDecay()
	{
		decayMe();
	}

	@Override
	public boolean isAttackable(Creature attacker)
	{
		return false;
	}

	@Override
	public boolean isAutoAttackable(Creature attacker)
	{
		return false;
	}

	@Override
	public Clan getClan()
	{
		return null;
	}

	@Override
	public void showChatWindow(Player player, final int val, Object... arg)
	{
		final KingHillEvent event = player.getEvent(KingHillEvent.class);
		if(event == null)
			return;

		final int team = event.owner;

		if(team == 0)
			super.showChatWindow(player, "events/territory_wars/hill_control_spot_no_owner.htm");
		else
		{
			final int index = team - 1;
			super.showChatWindow(player, "events/territory_wars/hill_control_spot" + (team == player.getTeamId() ? "_my" : "") + ".htm", "%owner%", event.teams[index], "%color%", event.colors[index]);
		}
	}

	@Override
	public final void onBypassFeedback(Player player, String command)
	{
		final KingHillEvent event = player.getEvent(KingHillEvent.class);
		if(event == null)
			return;

		if(!canBypassCheck(player, this) || event.owner == player.getTeamId())
			return;

		if(command.startsWith("takecontrolpoint"))
			player.doCast(TAKE_CONTROL_POINT_SKILL, this, true);
		else
			super.onBypassFeedback(player, command);
	}

	public static boolean canBypassCheck(Player player, NpcInstance npc)
	{
		if(npc == null || player.isDead() || !npc.isInRangeZ(player, 300))
		{
			player.sendActionFailed();
			return false;
		}
		return true;
	}

	public HillControlSpotObject getControlSpotObject()
	{
		return _twControlSpotObject;
	}
}