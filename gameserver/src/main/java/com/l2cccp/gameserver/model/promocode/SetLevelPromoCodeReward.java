package com.l2cccp.gameserver.model.promocode;

import org.dom4j.Element;

import com.l2cccp.gameserver.data.xml.holder.ExperienceHolder;
import com.l2cccp.gameserver.model.Player;

public class SetLevelPromoCodeReward extends PromoCodeReward
{
	public int _level;

	public SetLevelPromoCodeReward(Element element)
	{
		_level = Integer.parseInt(element.attributeValue("val"));
	}

	@Override
	public void giveReward(Player player)
	{
		if(player.getLevel() == _level)
			return;

		long expAdd = ExperienceHolder.getInstance().getLevels()[_level] - player.getExp();

		player.addExpAndSp(expAdd, 0);
	}
}
