package com.l2cccp.gameserver.model.top;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum Tops
{
	L2TOPRU("L2TOP.RU"),
	MMOTOPRU("MMOTOP.RU"),
	MMTOPSU("MMTOP.SU");

	private final String _link;

	private Tops(String link)
	{
		_link = link;
	}

	public String getLink()
	{
		return _link;
	}

	public static boolean isTop(final String link)
	{
		for(Tops top : values())
		{
			if(top.getLink().equals(link))
				return true;
		}

		return false;
	}
}