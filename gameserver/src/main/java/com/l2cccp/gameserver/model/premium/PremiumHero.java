package com.l2cccp.gameserver.model.premium;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum PremiumHero
{
	EQUIP(true, false, false),
	SKILLS(false, true, false),
	CHAT(false, false, true),
	EQUIP_SKILLS(true, true, false),
	EQUIP_CHAT(true, false, true),
	SKILLS_CHAT(false, true, true),
	FULL(true, true, true);

	private static final PremiumHero[] VALUES = values();

	public static PremiumHero find(String name)
	{
		for(PremiumHero key : VALUES)
		{
			if(key.name().equalsIgnoreCase(name))
				return key;
		}

		return null;
	}

	private final boolean _equip;
	private final boolean _skills;
	private final boolean _chat;

	PremiumHero(final boolean equip, final boolean skills, final boolean chat)
	{
		_equip = equip;
		_skills = skills;
		_chat = chat;
	}

	public boolean getEquip()
	{
		return _equip;
	}

	public boolean getSkills()
	{
		return _skills;
	}

	public boolean getChat()
	{
		return _chat;
	}
}