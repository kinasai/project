package com.l2cccp.gameserver.model.entity.events.actions;

import java.util.List;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
import com.l2cccp.gameserver.model.entity.events.Event;
import com.l2cccp.gameserver.model.entity.events.EventAction;
import com.l2cccp.gameserver.model.entity.events.impl.FunEvent;
import com.l2cccp.gameserver.model.reward.RewardGroup;
import com.l2cccp.gameserver.model.reward.RewardList;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

/**
 * @author VISTALL
 * @date 18:12/29.04.2012
 */
public class GlobalRewardListAction implements EventAction
{
	private boolean _add;
	private String _name;

	public GlobalRewardListAction(boolean add, String name)
	{
		_add = add;
		_name = name;
	}

	@Override
	public void call(Event event)
	{
		List<Object> list = event.getObjects(_name);
		for(NpcTemplate npc : NpcHolder.getInstance().getAll())
		{
			if(npc != null && !npc.getRewards().isEmpty())
			{
				if(event instanceof FunEvent)
				{
					FunEvent fun = (FunEvent) event;
					MultiValueSet<String> param = fun.getParameters();
					if(param != null)
					{
						boolean isRaid = param.getBool("raid", false);
						int[] levels = param.getIntegerArray("levels", null);
						if(isRaid && !npc.isRaid)
							continue;
						else if(!isRaid && npc.isRaid)
							continue;
						else if(levels != null && (npc.level < levels[0] || npc.level > levels[1]))
							continue;
					}
				}

				loop: for(RewardList rl : npc.getRewards())
				{
					for(RewardGroup rg : rl)
					{
						if(!rg.isAdena())
						{
							for(Object o : list)
							{
								if(o instanceof RewardList)
								{
									RewardList reward = (RewardList) o;
									if(_add)
										npc.addRewardList(reward);
									else
										npc.removeRewardList(reward);
								}
							}
							break loop;
						}
					}
				}
			}
		}
	}
}
