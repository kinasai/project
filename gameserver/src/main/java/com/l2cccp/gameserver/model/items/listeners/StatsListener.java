package com.l2cccp.gameserver.model.items.listeners;

import com.l2cccp.gameserver.listener.inventory.OnEquipListener;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.stats.funcs.Func;

public final class StatsListener implements OnEquipListener
{
	private static final StatsListener _instance = new StatsListener();

	public static StatsListener getInstance()
	{
		return _instance;
	}

	@Override
	public void onUnequip(int slot, ItemInstance item, Playable actor)
	{
		actor.removeStatsByOwner(item);
		actor.updateStats();
	}

	@Override
	public void onEquip(int slot, ItemInstance item, Playable actor)
	{
		Func[] funcs = item.getStatFuncs();
		actor.addStatFuncs(funcs);
		actor.updateStats();
	}
}