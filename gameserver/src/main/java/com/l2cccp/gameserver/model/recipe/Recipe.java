package com.l2cccp.gameserver.model.recipe;

public class Recipe
{
	private RecipeComponent[] recipes;
	private int id;
	private int level;
	private int recipeId;
	private String name;
	private int successRate;
	private int mpCost;
	private int itemId;
	private int count;
	private boolean isdwarvencraft;
	private int foundation;

	private long exp, sp;

	public Recipe(final int id, final String name)
	{
		this.id = id;
		this.name = name;
	}

	public Recipe(int id, int level, int recipeId, int successRate, int mpCost, int itemId, int foundation, int count, long exp, long sp, boolean isdwarvencraft)
	{
		this.id = id;
		this.level = level;
		this.recipeId = recipeId;
		this.successRate = successRate;
		this.mpCost = mpCost;
		this.itemId = itemId;
		this.foundation = foundation;
		this.count = count;
		this.exp = exp;
		this.sp = sp;
		this.isdwarvencraft = isdwarvencraft;
	}

	public void addRecipe(RecipeComponent recipe)
	{
		if(recipes == null)
			recipes = new RecipeComponent[] { recipe };
		else
		{
			int len = recipes.length;
			RecipeComponent[] tmp = new RecipeComponent[len + 1];
			System.arraycopy(recipes, 0, tmp, 0, len);
			tmp[len] = recipe;
			recipes = tmp;
		}
	}

	public int getId()
	{
		return id;
	}

	public int getLevel()
	{
		return level;
	}

	public int getRecipeId()
	{
		return recipeId;
	}

	public String getRecipeName()
	{
		return name;
	}

	public int getSuccessRate()
	{
		return successRate;
	}

	public int getMpCost()
	{
		return mpCost;
	}

	public int getItemId()
	{
		return itemId;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public void setRecipeId(int recipeId)
	{
		this.recipeId = recipeId;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public void setSuccessRate(int successRate)
	{
		this.successRate = successRate;
	}

	public void setMpCost(int mpCost)
	{
		this.mpCost = mpCost;
	}

	public void setItemId(int itemId)
	{
		this.itemId = itemId;
	}

	public void setCount(int count)
	{
		this.count = count;
	}

	public void setExp(long exp)
	{
		this.exp = exp;
	}

	public void setSp(long sp)
	{
		this.sp = sp;
	}

	public int getCount()
	{
		return count;
	}

	public RecipeComponent[] getRecipes()
	{
		return recipes;
	}

	public long getExp()
	{
		return exp;
	}

	public long getSp()
	{
		return sp;
	}

	public boolean isDwarvenCraft()
	{
		return isdwarvencraft;
	}

	public void setDwarvenCraft(boolean isdwarvencraft)
	{
		this.isdwarvencraft = isdwarvencraft;
	}

	public int getFoundation()
	{
		return foundation;
	}

	public void setFoundation(int foundation)
	{
		this.foundation = foundation;
	}
}