package com.l2cccp.gameserver.model.premium;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PremiumVariant
{
	private final int _id;
	private long _time;
	private int _price_id;
	private long _price_count;

	public PremiumVariant(final int id)
	{
		_id = id;
	}

	public int getId()
	{
		return _id;
	}

	public void setTime(final long time)
	{
		_time = time;
	}

	public long getTime()
	{
		return _time;
	}

	public void setPrice(final int id, final long count)
	{
		_price_id = id;
		_price_count = count;
	}

	public int getPriceId()
	{
		return _price_id;
	}

	public long getPriceCount()
	{
		return _price_count;
	}

	@Override
	public String toString()
	{
		return "Variant " + _id + ", Price: Count[" + _price_count + "] Id[" + _price_id + "], Time: " + _time;
	}
}