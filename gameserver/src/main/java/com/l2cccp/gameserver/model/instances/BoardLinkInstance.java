package com.l2cccp.gameserver.model.instances;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BoardLinkInstance extends NpcInstance
{
	private final String link;

	public BoardLinkInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
		link = template.getAIParams().getString("link", "_bbshome");
	}

	@Override
	public void showChatWindow(Player player, int val, Object... replace)
	{
		Util.communityNextPage(player, link);
	}
}