package com.l2cccp.gameserver.model.actor.instances.player;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;

import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.data.xml.holder.PremiumHolder;
import com.l2cccp.gameserver.model.premium.PremiumAccount;
import com.l2cccp.gameserver.model.premium.PremiumTask;

/**
 * Класс с бонусными рейтами для игрока
 */
public class Bonus
{
	private double _exp = 1.;
	private double _sp = 1.;
	private double _siege = 1.;
	private double _adena = 1.;
	private double _items = 1.;
	private double _spoil = 1.;
	private double _weight = 1.;
	private int _craft;
	private int _master_work;
	private Map<PremiumAccount, Long> _premiums = new HashMap<PremiumAccount, Long>();
	private Map<PremiumAccount, Future<?>> _tasks = new HashMap<PremiumAccount, Future<?>>();

	private boolean _shout;
	private boolean _hero;

	public double getRateXp()
	{
		return Math.max(1., _exp);
	}

	public void setRateXp(final double exp, final boolean inc)
	{
		if(inc)
			_exp += exp;
		else
			_exp -= exp;
	}

	public double getRateSp()
	{
		return Math.max(1., _sp);
	}

	public void setRateSp(final double sp, final boolean inc)
	{
		if(inc)
			_sp += sp;
		else
			_sp -= sp;
	}

	public double getDropAdena()
	{
		return Math.max(1., _adena);
	}

	public void setDropAdena(final double adena, final boolean inc)
	{
		if(inc)
			_adena += adena;
		else
			_adena -= adena;
	}

	public double getDropItems()
	{
		return Math.max(1., _items);
	}

	public void setDropItems(final double items, final boolean inc)
	{
		if(inc)
			_items += items;
		else
			_items -= items;
	}

	public double getDropSpoil()
	{
		return Math.max(1., _spoil);
	}

	public void setDropSpoil(final double spoil, final boolean inc)
	{
		if(inc)
			_spoil += spoil;
		else
			_spoil -= spoil;
	}

	public double getDropSiege()
	{
		return Math.max(1., _siege);
	}

	public void setDropSiege(final double siege, final boolean inc)
	{
		if(inc)
			_siege += siege;
		else
			_siege -= siege;
	}

	public double getWeight()
	{
		return Math.max(1., _weight);
	}

	public void setWeight(final double weight, final boolean inc)
	{
		if(inc)
			_weight += weight;
		else
			_weight -= weight;
	}

	public double getCraftChance()
	{
		return _craft;
	}

	public void setCraftChance(final int craft, final boolean inc)
	{
		if(inc)
			_craft += craft;
		else
			_craft -= craft;
	}

	public double getMasterWorkChance()
	{
		return _master_work;
	}

	public void setMasterWorkChance(final int master_work, final boolean inc)
	{
		if(inc)
			_master_work += master_work;
		else
			_master_work -= master_work;
	}

	public void setShoutChat(final boolean shout)
	{
		_shout = shout;
	}

	public void setHeroChat(final boolean hero)
	{
		_hero = hero;
	}

	public boolean canShout()
	{
		return _shout;
	}

	public boolean canHeroVoice()
	{
		return _hero;
	}

	public void addPremium(final PremiumAccount premium, final long expie)
	{
		if(!_premiums.containsKey(premium)) // Если такой прем уже есть то просто продлим ему время
		{
			setRateXp(premium.getExp(), true);
			setRateSp(premium.getSp(), true);
			setDropSiege(premium.getEpaulette(), true);
			setDropAdena(premium.getAdena(), true);
			setDropItems(premium.getItems(), true);
			setDropSpoil(premium.getSpoil(), true);
			setWeight(premium.getWeight(), true);
			setCraftChance(premium.getCraftChance(), true);
			setMasterWorkChance(premium.getMasterWorkChance(), true);

			if(premium.canShout())
				setShoutChat(true);
			if(premium.canHeroVoice())
				setHeroChat(true);
		}

		_premiums.put(premium, expie);
	}

	public void addTask(final PremiumAccount premium, final PremiumTask task)
	{
		final Future<?> future = _tasks.get(premium);
		if(future != null)
			future.cancel(false);

		final long execute = _premiums.get(premium) - System.currentTimeMillis();
		final ScheduledFuture<?> schedule = ThreadPoolManager.getInstance().schedule(task, execute);
		_tasks.put(premium, schedule);
	}

	public void remove(final PremiumAccount premium)
	{
		_premiums.remove(premium);
	}

	public long getExpire(final PremiumAccount premium)
	{
		return _premiums.containsKey(premium) ? _premiums.get(premium) : System.currentTimeMillis();
	}

	public boolean isActive(final int id)
	{
		return getExpire(PremiumHolder.getInstance().getPremium(id)) > System.currentTimeMillis();
	}

	/**
	 * Проверяем есть ли у нас хотябы один прем
	 */
	public boolean checkPremium()
	{
		for(final PremiumAccount premium : PremiumHolder.getInstance().getAllPremiums())
		{
			if(_premiums.containsKey(premium))
			{
				final long expire = _premiums.get(premium);
				if(expire > System.currentTimeMillis())
					return true;
			}
		}

		return false;
	}
}