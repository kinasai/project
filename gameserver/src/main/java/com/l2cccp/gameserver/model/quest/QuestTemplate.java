package com.l2cccp.gameserver.model.quest;

import java.util.List;
import java.util.Map;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class QuestTemplate
{
	private final int _id;
	private final QuestType _type;
	private Map<Language, String> _name;
	private Map<QuestEventType, List<Integer>> _events;
	private MultiValueSet<String> _params;
	private List<Integer> _items;
	private double _drop;
	private double _reward;
	private double _exp;
	private double _sp;

	public QuestTemplate(final int id, final QuestType type)
	{
		_id = id;
		_type = type;
	}

	public int getId()
	{
		return _id;
	}

	public QuestType getType()
	{
		return _type;
	}

	public String getName(final Language lang)
	{
		return _name.get(lang);
	}

	public void setNames(final Map<Language, String> name)
	{
		_name = name;
	}

	public Map<QuestEventType, List<Integer>> getEvents()
	{
		return _events;
	}

	public void setEvents(final Map<QuestEventType, List<Integer>> events)
	{
		_events = events;
	}

	public List<Integer> getItems()
	{
		return _items;
	}

	public void setItems(final List<Integer> items)
	{
		_items = items;
	}

	public void setParams(final MultiValueSet<String> params)
	{
		_params = params;
	}

	public MultiValueSet<String> getParam()
	{
		return _params;
	}

	public double getDrop()
	{
		return _drop;
	}

	public double getReward()
	{
		return _reward;
	}

	public double getExp()
	{
		return _exp;
	}

	public double getSp()
	{
		return _sp;
	}

	public void setDrop(final double drop)
	{
		_drop = drop;
	}

	public void setReward(final double reward)
	{
		_reward = reward;
	}

	public void setExp(final double exp)
	{
		_exp = exp;
	}

	public void setSp(final double sp)
	{
		_sp = sp;
	}
}