package com.l2cccp.gameserver.model.entity.events.util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum EventState
{
	NONE,
	REGISTRATION,
	TELEPORT_PLAYERS,
	FIRST_PHASE,
	SECOND_PHASE,
	NO_PLAYERS,
	FINISH
}