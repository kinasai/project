package com.l2cccp.gameserver.model;

import java.util.Collection;

import com.l2cccp.gameserver.dao.ItemsDAO;
import com.l2cccp.gameserver.model.items.Inventory;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.items.ItemInstance.ItemLocation;

/**
 * Used to Store data sent to Client for Character
 * Selection screen.
 *
 * @version $Revision: 1.2.2.2.2.4 $ $Date: 2005/03/27 15:29:33 $
 */
public class CharSelectInfo
{
	private String _name;
	private int _objectId = 0;
	private int _charId = 0x00030b7a;
	private long _exp = 0;
	private int _sp = 0;
	private int _clanId = 0;
	private int _race = 0;
	private int _classId = 0;
	private int _baseClassId = 0;
	private int _deleteTimer = 0;
	private long _lastAccess = 0L;
	private int _face = 0;
	private int _hairStyle = 0;
	private int _hairColor = 0;
	private int _sex = 0;
	private int _level = 1;
	private int _karma = 0, _pk = 0, _pvp = 0;
	private int _maxHp = 0;
	private double _currentHp = 0;
	private int _maxMp = 0;
	private double _currentMp = 0;
	private ItemInstance[] _paperdoll;
	//private CharSelectInfoPaperdollItem[] _paperdoll;
	private int _accesslevel = 0;
	private int _x = 0, _y = 0, _z = 0;
	private int _vitalityPoints = 20000;
	private int vChest = -1, vLegs = -1, vGloves = -1, vBoots = -1, vCloak = -1;

	private boolean _passwordChecked;
	private boolean _passwordEnable;
	private String _password;

	public CharSelectInfo(int objectId, String name) {
		setObjectId(objectId);
		setName(name);
		Collection<ItemInstance> items = ItemsDAO.getInstance().getItemsByOwnerIdAndLoc(objectId, ItemLocation.PAPERDOLL);
		_paperdoll = new ItemInstance[Inventory.PAPERDOLL_MAX];
		//_paperdoll = new CharSelectInfoPaperdollItem[Inventory.PAPERDOLL_MAX];
		for(ItemInstance item : items) {
			if(item.getEquipSlot() < Inventory.PAPERDOLL_MAX) {//FIXME [G1ta0] временный фикс отображения одетых вещей при входе на персонажа в NO CARRIER
				_paperdoll[item.getEquipSlot()] = item;
				//_paperdoll[item.getEquipSlot()] = new CharSelectInfoPaperdollItem(item.getItemId(), item.getVisualItemId(), item.getAugmentations(), item.getEnchantLevel(), item.getCustomFlags());
			}
		}
	}

	public int getObjectId()
	{
		return _objectId;
	}

	public void setObjectId(int objectId)
	{
		_objectId = objectId;
	}

	public int getCharId()
	{
		return _charId;
	}

	public void setCharId(int charId)
	{
		_charId = charId;
	}

	public int getClanId()
	{
		return _clanId;
	}

	public void setClanId(int clanId)
	{
		_clanId = clanId;
	}

	public int getClassId()
	{
		return _classId;
	}

	public int getBaseClassId()
	{
		return _baseClassId;
	}

	public void setBaseClassId(int baseClassId)
	{
		_baseClassId = baseClassId;
	}

	public void setClassId(int classId)
	{
		_classId = classId;
	}

	public double getCurrentHp()
	{
		return _currentHp;
	}

	public void setCurrentHp(double currentHp)
	{
		_currentHp = currentHp;
	}

	public double getCurrentMp()
	{
		return _currentMp;
	}

	public void setCurrentMp(double currentMp)
	{
		_currentMp = currentMp;
	}

	public int getDeleteTimer()
	{
		return _deleteTimer;
	}

	public void setDeleteTimer(int deleteTimer)
	{
		_deleteTimer = deleteTimer;
	}

	public long getLastAccess()
	{
		return _lastAccess;
	}

	public void setLastAccess(long lastAccess)
	{
		_lastAccess = lastAccess;
	}

	public long getExp()
	{
		return _exp;
	}

	public void setExp(long exp)
	{
		_exp = exp;
	}

	public int getFace()
	{
		return _face;
	}

	public void setFace(int face)
	{
		_face = face;
	}

	public int getHairColor()
	{
		return _hairColor;
	}

	public void setHairColor(int hairColor)
	{
		_hairColor = hairColor;
	}

	public int getHairStyle()
	{
		return _hairStyle;
	}

	public void setHairStyle(int hairStyle)
	{
		_hairStyle = hairStyle;
	}

	public int[] getPaperdollAugmentationId(int slot)
	{
		ItemInstance item = _paperdoll[slot];
		//CharSelectInfoPaperdollItem item = _paperdoll[slot];
		if(item != null)
			return item.getAugmentations();
		return ItemInstance.EMPTY_AUGMENTATIONS;
	}

	public int getPaperdollItemId(int slot) {
		ItemInstance item = _paperdoll[slot];
		if(slot == Inventory.PAPERDOLL_CHEST) {
			if(getVChest() > 0) {
				return getVChest();
			}
			if(getVChest() == -2) {
				return 0;
			}
		}
		if(slot == Inventory.PAPERDOLL_LEGS) {
			if(getVLegs() > 0) {
				return getVLegs();
			}
			if(getVLegs() == -2) {
				return 0;
			}
		}
		if(slot == Inventory.PAPERDOLL_GLOVES) {
			if(getVGloves() > 0) {
				return getVGloves();
			}
			if(getVGloves() == -2) {
				return 0;
			}
		}
		if(slot == Inventory.PAPERDOLL_FEET) {
			if(getVBoots() > 0) {
				return getVBoots();
			}
			if(getVBoots() == -2) {
				return 0;
			}
		}
		if(slot == Inventory.PAPERDOLL_RHAND || slot == Inventory.PAPERDOLL_LRHAND || slot == Inventory.PAPERDOLL_LHAND) {
			if(item != null) {
				int visualItemId = item.getVisualItemId();
				if(visualItemId != 0) {
					return visualItemId;
				} else {
					return item.getItemId();
				}
			}
		}
		if(slot == Inventory.PAPERDOLL_BACK) {
			if(getVCloak() > 0) {
				return getVCloak();
			}
			if(getVCloak() == -2) {
				return 0;
			}
		}
		if(item != null) {
			return item.getItemId();
		}
		return 0;
		/*CharSelectInfoPaperdollItem item = _paperdoll[slot];
		if(item != null)
		{
			if(slot == Inventory.PAPERDOLL_LEGS || slot == Inventory.PAPERDOLL_HEAD || slot == Inventory.PAPERDOLL_FEET || slot == Inventory.PAPERDOLL_GLOVES)
			{
				CharSelectInfoPaperdollItem paperdollItem = _paperdoll[Inventory.PAPERDOLL_CHEST];
				if(paperdollItem != null && (paperdollItem.getCustomFlags() & ItemInstance.FLAG_COSTUME) == ItemInstance.FLAG_COSTUME)
					return 0;
			}

			int visualItemId = item.getVisualItemId();
			if(visualItemId != 0)
				return visualItemId;
			else
				return item.getItemId();
		}*/
	}

	public int getPaperdollEnchantEffect(int slot)
	{
		ItemInstance item = _paperdoll[slot];
		//CharSelectInfoPaperdollItem item = _paperdoll[slot];
		if(item != null)
			return item.getEnchantLevel();
		return 0;
	}

	public int getLevel()
	{
		return _level;
	}

	public void setLevel(int level)
	{
		_level = level;
	}

	public int getMaxHp()
	{
		return _maxHp;
	}

	public void setMaxHp(int maxHp)
	{
		_maxHp = maxHp;
	}

	public int getMaxMp()
	{
		return _maxMp;
	}

	public void setMaxMp(int maxMp)
	{
		_maxMp = maxMp;
	}

	public String getName()
	{
		return _name;
	}

	public void setName(String name)
	{
		_name = name;
	}

	public int getRace()
	{
		return _race;
	}

	public void setRace(int race)
	{
		_race = race;
	}

	public int getSex()
	{
		return _sex;
	}

	public void setSex(int sex)
	{
		_sex = sex;
	}

	public int getSp()
	{
		return _sp;
	}

	public void setSp(int sp)
	{
		_sp = sp;
	}

	public int getKarma()
	{
		return _karma;
	}

	public void setKarma(int karma)
	{
		_karma = karma;
	}

	public int getAccessLevel()
	{
		return _accesslevel;
	}

	public void setAccessLevel(int accesslevel)
	{
		_accesslevel = accesslevel;
	}

	public int getX()
	{
		return _x;
	}

	public void setX(int x)
	{
		_x = x;
	}

	public int getY()
	{
		return _y;
	}

	public void setY(int y)
	{
		_y = y;
	}

	public int getZ()
	{
		return _z;
	}

	public void setZ(int z)
	{
		_z = z;
	}

	public int getPk()
	{
		return _pk;
	}

	public void setPk(int pk)
	{
		_pk = pk;
	}

	public int getPvP()
	{
		return _pvp;
	}

	public void setPvP(int pvp)
	{
		_pvp = pvp;
	}

	public int getVitalityPoints()
	{
		return _vitalityPoints;
	}

	public void setVitalityPoints(int points)
	{
		_vitalityPoints = points;
	}

	public boolean isPasswordEnable()
	{
		return _passwordEnable;
	}

	public void setPasswordEnable(boolean passwordEnable)
	{
		_passwordEnable = passwordEnable;
	}

	public String getPassword()
	{
		return _password;
	}

	public void setPassword(String password)
	{
		_password = password;
	}

	public boolean isPasswordChecked()
	{
		return _passwordChecked;
	}

	public void setPasswordChecked(boolean passwordChecked)
	{
		_passwordChecked = passwordChecked;
	}

	public void setVChest(int vChest) {
		this.vChest = vChest;
	}

	public void setVLegs(int vLegs) {
		this.vLegs = vLegs;
	}

	public void setVGloves(int vGloves) {
		this.vGloves = vGloves;
	}

	public void setVBoots(int vBoots) {
		this.vBoots = vBoots;
	}

	public void setVCloak(int vCloak) {
		this.vCloak = vCloak;
	}
	
	public int getVChest() {
		return vChest;
	}

	public int getVLegs() {
		return vLegs;
	}

	public int getVGloves() {
		return vGloves;
	}

	public int getVBoots() {
		return vBoots;
	}

	public int getVCloak() {
		return vCloak;
	}
}