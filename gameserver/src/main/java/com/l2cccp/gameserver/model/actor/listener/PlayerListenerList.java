package com.l2cccp.gameserver.model.actor.listener;

import com.l2cccp.commons.listener.Listener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerAttack;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerEnterListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerExitListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerPartyInviteListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerPartyLeaveListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerSayListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerSummonServitorListener;
import com.l2cccp.gameserver.listener.actor.player.OnTeleportListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;

/**
 * @author G1ta0
 */
public class PlayerListenerList extends CharListenerList
{
	public PlayerListenerList(Player actor)
	{
		super(actor);
	}

	@Override
	public Player getActor()
	{
		return (Player) actor;
	}

	public void onEnter()
	{
		if(!global.getListeners().isEmpty())
			for(Listener<Creature> listener : global.getListeners())
				if(OnPlayerEnterListener.class.isInstance(listener))
					((OnPlayerEnterListener) listener).onPlayerEnter(getActor());

		if(!getListeners().isEmpty())
			for(Listener<Creature> listener : getListeners())
				if(OnPlayerEnterListener.class.isInstance(listener))
					((OnPlayerEnterListener) listener).onPlayerEnter(getActor());
	}

	public void onExit()
	{
		if(!global.getListeners().isEmpty())
			for(Listener<Creature> listener : global.getListeners())
				if(OnPlayerExitListener.class.isInstance(listener))
					((OnPlayerExitListener) listener).onPlayerExit(getActor());

		if(!getListeners().isEmpty())
			for(Listener<Creature> listener : getListeners())
				if(OnPlayerExitListener.class.isInstance(listener))
					((OnPlayerExitListener) listener).onPlayerExit(getActor());
	}

	public void onTeleport(int x, int y, int z, Reflection reflection)
	{
		if(!global.getListeners().isEmpty())
			for(Listener<Creature> listener : global.getListeners())
				if(OnTeleportListener.class.isInstance(listener))
					((OnTeleportListener) listener).onTeleport(getActor(), x, y, z, reflection);

		if(!getListeners().isEmpty())
			for(Listener<Creature> listener : getListeners())
				if(OnTeleportListener.class.isInstance(listener))
					((OnTeleportListener) listener).onTeleport(getActor(), x, y, z, reflection);
	}

	public void onPartyInvite()
	{
		if(!global.getListeners().isEmpty())
			for(Listener<Creature> listener : global.getListeners())
				if(OnPlayerPartyInviteListener.class.isInstance(listener))
					((OnPlayerPartyInviteListener) listener).onPartyInvite(getActor());

		if(!getListeners().isEmpty())
			for(Listener<Creature> listener : getListeners())
				if(OnPlayerPartyInviteListener.class.isInstance(listener))
					((OnPlayerPartyInviteListener) listener).onPartyInvite(getActor());
	}

	public void onPartyLeave()
	{
		if(!global.getListeners().isEmpty())
			for(Listener<Creature> listener : global.getListeners())
				if(OnPlayerPartyLeaveListener.class.isInstance(listener))
					((OnPlayerPartyLeaveListener) listener).onPartyLeave(getActor());

		if(!getListeners().isEmpty())
			for(Listener<Creature> listener : getListeners())
				if(OnPlayerPartyLeaveListener.class.isInstance(listener))
					((OnPlayerPartyLeaveListener) listener).onPartyLeave(getActor());
	}

	public void onSummonServitor(Servitor servitor)
	{
		if(!global.getListeners().isEmpty())
			for(Listener<Creature> listener : global.getListeners())
				if(OnPlayerSummonServitorListener.class.isInstance(listener))
					((OnPlayerSummonServitorListener) listener).onSummonServitor(getActor(), servitor);

		if(!getListeners().isEmpty())
			for(Listener<Creature> listener : getListeners())
				if(OnPlayerSummonServitorListener.class.isInstance(listener))
					((OnPlayerSummonServitorListener) listener).onSummonServitor(getActor(), servitor);
	}

	public void onSay(ChatType type, String target, String text)
	{
		if(!global.getListeners().isEmpty())
			for(Listener<Creature> listener : global.getListeners())
				if(OnPlayerSayListener.class.isInstance(listener))
					((OnPlayerSayListener) listener).onSay(getActor(), type, target, text);

		if(!getListeners().isEmpty())
			for(Listener<Creature> listener : getListeners())
				if(OnPlayerSayListener.class.isInstance(listener))
					((OnPlayerSayListener) listener).onSay(getActor(), type, target, text);
	}

	public boolean canAttack(final Creature target, final Creature attacker, final Skill skill, final boolean force, final boolean nextAttackCheck)
	{
		if(!global.getListeners().isEmpty())
		{
			for(Listener<Creature> listener : global.getListeners())
			{
				if(OnPlayerAttack.class.isInstance(listener))
					return ((OnPlayerAttack) listener).canAttack(target, attacker, skill, force, nextAttackCheck);
			}
		}

		if(!getListeners().isEmpty())
		{
			for(Listener<Creature> listener : getListeners())
			{
				if(OnPlayerAttack.class.isInstance(listener))
					return ((OnPlayerAttack) listener).canAttack(target, attacker, skill, force, nextAttackCheck);
			}
		}

		return false;
	}

	public SystemMsg checkForAttack(final Creature target, final Creature attacker, final Skill skill, final boolean force)
	{
		if(!global.getListeners().isEmpty())
		{
			for(Listener<Creature> listener : global.getListeners())
			{
				if(OnPlayerAttack.class.isInstance(listener))
					return ((OnPlayerAttack) listener).checkForAttack(target, attacker, skill, force);
			}
		}

		if(!getListeners().isEmpty())
		{
			for(Listener<Creature> listener : getListeners())
			{
				if(OnPlayerAttack.class.isInstance(listener))
					return ((OnPlayerAttack) listener).checkForAttack(target, attacker, skill, force);
			}
		}

		return null;
	}
}
