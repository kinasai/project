package com.l2cccp.gameserver.model.premium;

import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.model.Player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PremiumTask extends RunnableImpl
{
	private final Player player;
	private final PremiumAccount premium;

	public PremiumTask(final Player player, final PremiumAccount premium)
	{
		this.player = player;
		this.premium = premium;
	}

	@Override
	protected void runImpl() throws Exception
	{
		PremiumEnd.getInstance().resetBonuses(player, premium);
	}
}