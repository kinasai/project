package com.l2cccp.gameserver.model.farm;

import org.apache.commons.lang3.ArrayUtils;

import com.l2cccp.gameserver.data.xml.holder.FarmDataHolder;
import com.l2cccp.gameserver.handler.items.IItemHandler;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone.ZoneType;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FarmHandler implements IItemHandler
{
	@Override
	public boolean useItem(Playable playable, ItemInstance item, boolean ctrl)
	{
		final Player player = playable.getPlayer();
		if(player.isInZone(ZoneType.FARM))
		{
			if(player.getInventory().destroyItem(item, 1))
			{
				final FarmFacility farm = FarmDataHolder.getInstance().get(item.getItemId());
				farm.use(player);
				return true;
			}
		}
		else
			player.sendMessage("Здесь нельзя посадить это семя!");

		return false;
	}

	@Override
	public boolean dropItem(Player player, ItemInstance item, long count, Location loc)
	{
		return false;
	}

	@Override
	public boolean pickupItem(Playable playable, ItemInstance item)
	{
		return false;
	}

	@Override
	public int[] getItemIds()
	{
		return ArrayUtils.EMPTY_INT_ARRAY;
	}
}