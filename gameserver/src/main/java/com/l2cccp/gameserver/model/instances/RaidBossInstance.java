package com.l2cccp.gameserver.model.instances;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang3.ArrayUtils;

import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.data.xml.holder.ExperienceHolder;
import com.l2cccp.gameserver.instancemanager.QuestManager;
import com.l2cccp.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2cccp.gameserver.model.AggroList.HateInfo;
import com.l2cccp.gameserver.model.CommandChannel;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Party;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.PlayerGroup;
import com.l2cccp.gameserver.model.entity.Hero;
import com.l2cccp.gameserver.model.entity.HeroDiary;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.model.quest.Quest;
import com.l2cccp.gameserver.model.quest.QuestState;
import com.l2cccp.gameserver.network.l2.components.NpcString;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

public class RaidBossInstance extends MonsterWithLongRespawnInstance
{
	private static final long LOCK_RESET_INTERVAL = 300000L;

	private AtomicBoolean _channelLocked = new AtomicBoolean();
	private ScheduledFuture<?> _channelLockCheckTask = null;
	private CommandChannel _lockedCommandChannel = null;
	private long _lastAttackTimeStamp = 0;

	private ScheduledFuture<?> _minionMaintainTask;

	private static final int MINION_UNSPAWN_INTERVAL = 5000; //time to unspawn minions when boss is dead, msec

	public RaidBossInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	protected int getMinionUnspawnInterval()
	{
		return MINION_UNSPAWN_INTERVAL;
	}

	protected int getKilledInterval(NpcInstance minion)
	{
		return 120000; //2 minutes to respawn
	}

	@Override
	public void notifyMinionDied(NpcInstance minion)
	{
		_minionMaintainTask = ThreadPoolManager.getInstance().schedule(new MaintainKilledMinion(minion), getKilledInterval(minion));
		super.notifyMinionDied(minion);
	}

	private class MaintainKilledMinion extends RunnableImpl
	{
		private final NpcInstance minion;

		public MaintainKilledMinion(NpcInstance minion)
		{
			this.minion = minion;
		}

		@Override
		public void runImpl() throws Exception
		{
			if(!isDead() && !minion.isVisible())
			{
				minion.refreshID();
				spawnMinion(minion);
			}
		}
	}

	protected int getMinChannelSizeForLock()
	{
		return 18;
	}

	protected void onChannelLock(String leaderName)
	{
		broadcastPacket(new ExShowScreenMessage(NpcString.S1_COMMAND_CHANNEL_HAS_LOOTING_RIGHTS, 4000, ScreenMessageAlign.TOP_CENTER, true, 1, -1, false, leaderName));
	}

	protected void onChannelUnlock()
	{
		broadcastPacket(new ExShowScreenMessage(NpcString.LOOTING_RULES_ARE_NO_LONGER_ACTIVE, 4000, ScreenMessageAlign.TOP_CENTER, true, 1, -1, false));
	}

	private final class ChannelLockCheckTask extends RunnableImpl
	{
		@Override
		public final void runImpl() throws Exception
		{
			final long nextCheckInterval = LOCK_RESET_INTERVAL - (System.currentTimeMillis() - _lastAttackTimeStamp);
			if(nextCheckInterval < 1000L || _lockedCommandChannel == null || _lockedCommandChannel.getChannelLeader() == null)
			{
				_channelLockCheckTask = null;
				_lockedCommandChannel = null;
				_channelLocked.set(false);
				onChannelUnlock();
				return;
			}
			_channelLockCheckTask = ThreadPoolManager.getInstance().schedule(this, nextCheckInterval);
		}
	}

	@Override
	protected void onReduceCurrentHp(double damage, Creature attacker, SkillEntry skill, boolean awake, boolean standUp, boolean directHp)
	{
		_lastAttackTimeStamp = System.currentTimeMillis();

		if(getMinChannelSizeForLock() > 0)
		{
			final Player activePlayer = attacker.getPlayer();
			if(activePlayer != null)
			{
				final PlayerGroup pg = activePlayer.getPlayerGroup();
				if(pg instanceof CommandChannel)
				{
					final CommandChannel cc = (CommandChannel) pg;
					final Player leader = cc.getChannelLeader();
					if(!_channelLocked.get() && leader != null && cc.getMemberCount() >= getMinChannelSizeForLock())
					{
						if(_channelLocked.compareAndSet(false, true))
						{
							_lockedCommandChannel = cc;
							_channelLockCheckTask = ThreadPoolManager.getInstance().schedule(new ChannelLockCheckTask(), LOCK_RESET_INTERVAL);
							onChannelLock(leader.getName());
						}
					}
				}
			}
		}

		super.onReduceCurrentHp(damage, attacker, skill, awake, standUp, directHp);
	}

	@Override
	protected Player lockDropTo(Player topDamager)
	{
		final CommandChannel cc = _lockedCommandChannel;
		if(cc != null)
		{
			final Player leader = cc.getChannelLeader();
			if(leader != null)
				return leader;
		}

		return topDamager;
	}

	@Override
	public double getRewardRate(Player player)
	{
		return isRaid() ? player.getRate().getEpic() : player.getRate().getRaid(); // ПА не увеличивает дроп с рейдов
	}

	@Override
	protected void onDeath(Creature killer)
	{
		if(_minionMaintainTask != null)
		{
			_minionMaintainTask.cancel(false);
			_minionMaintainTask = null;
		}

		if(_channelLockCheckTask != null)
		{
			_channelLockCheckTask.cancel(false);
			_channelLockCheckTask = null;
		}

		final int points = getTemplate().rewardRp;
		if(points > 0)
			calcRaidPointsReward(points);

		if(this instanceof ReflectionBossInstance)
		{
			super.onDeath(killer);
			return;
		}

		if(killer.isPlayable())
		{
			boolean skipRecord = ArrayUtils.contains(Config.HERO_DIARY_EXCLUDED_BOSSES, getNpcId());

			Player player = killer.getPlayer();
			if(!skipRecord)
			{
				if(player.isInParty())
				{
					for(Player member : player.getParty().getPartyMembers())
					{
						member.updateRaidKills();
						if(member.isNoble())
							Hero.getInstance().addHeroDiary(member.getObjectId(), HeroDiary.ACTION_RAID_KILLED, getNpcId());
					}
					player.getParty().broadCast(SystemMsg.CONGRATULATIONS_RAID_WAS_SUCCESSFUL);
				}
				else
				{
					if(player.isNoble())
						Hero.getInstance().addHeroDiary(player.getObjectId(), HeroDiary.ACTION_RAID_KILLED, getNpcId());
					player.sendPacket(SystemMsg.CONGRATULATIONS_RAID_WAS_SUCCESSFUL);
					player.updateRaidKills();
				}
			}

			Quest q = QuestManager.getQuest(508);
			if(q != null)
			{
				final Clan cl = player.getClan();
				if(cl != null && cl.getLeader().isOnline() && cl.getLeader().getPlayer().getQuestState(q) != null)
				{
					QuestState st = cl.getLeader().getPlayer().getQuestState(q);
					st.getQuest().onKill(this, st);
				}
			}
		}

		if(hasMinions() && getMinionList().hasAliveMinions())
			ThreadPoolManager.getInstance().schedule(new RunnableImpl(){
				@Override
				public void runImpl() throws Exception
				{
					if(isDead())
						getMinionList().unspawnMinions();
				}
			}, getMinionUnspawnInterval());

		super.onDeath(killer);
	}

	@Override
	protected void onDecay()
	{
		if(_channelLockCheckTask != null)
		{
			_channelLockCheckTask.cancel(false);
			_channelLockCheckTask = null;
		}
		_lockedCommandChannel = null;
		_channelLocked.set(false);

		super.onDecay();
	}

	//FIXME [G1ta0] разобрать этот хлам
	@SuppressWarnings("unchecked")
	private void calcRaidPointsReward(int totalPoints)
	{
		// Object groupkey (L2Party/L2CommandChannel/L2Player) | [List<L2Player> group, Long GroupDdamage]
		Map<Object, Object[]> participants = new HashMap<Object, Object[]>();
		double totalHp = getMaxHp();

		// Разбиваем игроков по группам. По возможности используем наибольшую из доступных групп: Command Channel → Party → StandAlone (сам плюс пет :)
		for(HateInfo ai : getAggroList().getPlayableMap().values())
		{
			Player player = ai.attacker.getPlayer();
			Object key = player.getParty() != null ? player.getParty().getCommandChannel() != null ? player.getParty().getCommandChannel() : player.getParty() : player.getPlayer();
			Object[] info = participants.get(key);
			if(info == null)
			{
				info = new Object[] { new HashSet<Player>(), new Long(0) };
				participants.put(key, info);
			}

			// если это пати или командный канал то берем оттуда весь список участвующих, даже тех кто не в аггролисте
			// дубликаты не страшны - это хашсет
			if(key instanceof CommandChannel)
			{
				for(Player p : ((CommandChannel) key))
					if(p.isInRangeZ(this, Config.ALT_PARTY_DISTRIBUTION_RANGE))
						((Set<Player>) info[0]).add(p);
			}
			else if(key instanceof Party)
			{
				for(Player p : ((Party) key).getPartyMembers())
					if(p.isInRangeZ(this, Config.ALT_PARTY_DISTRIBUTION_RANGE))
						((Set<Player>) info[0]).add(p);
			}
			else
				((Set<Player>) info[0]).add(player);

			info[1] = ((Long) info[1]).longValue() + ai.damage;
		}

		for(Object[] groupInfo : participants.values())
		{
			Set<Player> players = (HashSet<Player>) groupInfo[0];
			// это та часть, которую игрок заслужил дамагом группы, но на нее может быть наложен штраф от уровня игрока
			int perPlayer = (int) Math.round(totalPoints * ((Long) groupInfo[1]).longValue() / (totalHp * players.size()));
			for(Player player : players)
			{
				int playerReward = perPlayer;
				// применяем штраф если нужен
				playerReward = (int) Math.round(playerReward * ExperienceHolder.getInstance().penaltyModifier(calculateLevelDiffForDrop(player.getLevel()), 9));
				if(playerReward == 0)
					continue;
				player.sendPacket(new SystemMessage(SystemMsg.YOU_HAVE_EARNED_S1_RAID_POINTS).addNumber(playerReward));
				RaidBossSpawnManager.getInstance().addPoints(player.getObjectId(), getNpcId(), playerReward);
			}
		}

		RaidBossSpawnManager.getInstance().updatePointsDb();
		RaidBossSpawnManager.getInstance().calculateRanking();
	}

	@Override
	public boolean isFearImmune()
	{
		return true;
	}

	@Override
	public boolean isParalyzeImmune()
	{
		return true;
	}

	@Override
	public boolean isLethalImmune()
	{
		return true;
	}

	@Override
	public boolean canChampion()
	{
		return false;
	}
}