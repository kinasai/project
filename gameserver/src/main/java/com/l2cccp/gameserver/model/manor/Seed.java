package com.l2cccp.gameserver.model.manor;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Seed
{
	private int _id;
	private final int _level;
	private final int _crop;
	private final int _mature;
	private int _type1;
	private int _type2;
	private int _manorId; // id of manor (castle id) where seed can be farmed
	private boolean _isAlternative;
	private long _limitSeeds;
	private long _limitCrops;

	public Seed(int level, int crop, int mature)
	{
		_level = level;
		_crop = crop;
		_mature = mature;
	}

	public void setData(int id, int t1, int t2, int manorId, boolean isAlt, long lim1, long lim2)
	{
		_id = id;
		_type1 = t1;
		_type2 = t2;
		_manorId = manorId;
		_isAlternative = isAlt;
		_limitSeeds = lim1;
		_limitCrops = lim2;
	}

	public int getManorId()
	{
		return _manorId;
	}

	public int getId()
	{
		return _id;
	}

	public int getCrop()
	{
		return _crop;
	}

	public int getMature()
	{
		return _mature;
	}

	public int getReward(int type)
	{
		return type == 1 ? _type1 : _type2;
	}

	public int getLevel()
	{
		return _level;
	}

	public boolean isAlternative()
	{
		return _isAlternative;
	}

	public long getSeedLimit()
	{
		return _limitSeeds;
	}

	public long getCropLimit()
	{
		return _limitCrops;
	}
}
