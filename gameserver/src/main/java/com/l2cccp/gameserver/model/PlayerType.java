package com.l2cccp.gameserver.model;

public enum PlayerType
{
	PLAYER,
	SUMMON,
	BOT,
	TRADER
}