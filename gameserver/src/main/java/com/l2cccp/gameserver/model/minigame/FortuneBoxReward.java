package com.l2cccp.gameserver.model.minigame;

public class FortuneBoxReward
{
	//reward
	private final int _id;
	private final long[] _count;
	private final double _chance;
	private final double[] _enchant;
	private final boolean _announce;

	public FortuneBoxReward(int reward_id, long[] reward_count, double reward_chance, double[] reward_enchant, boolean announce)
	{
		_id = reward_id;
		_count = reward_count;
		_chance = reward_chance;
		_enchant = reward_enchant;
		_announce = announce;
	}

	public int getId()
	{
		return _id;
	}

	public long[] getCount()
	{
		return _count;
	}

	public double getChance()
	{
		return _chance;
	}

	public double[] getEnchantData()
	{
		return _enchant;
	}

	public boolean doAnnounce()
	{
		return _announce;
	}
}
