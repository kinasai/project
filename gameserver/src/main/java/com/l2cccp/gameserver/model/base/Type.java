package com.l2cccp.gameserver.model.base;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum Type
{
	FIGHTER,
	MAGE;
	private final boolean mage;

	private Type()
	{
		mage = this.name().equals("MAGE");
	}

	public boolean isMage()
	{
		return mage;
	}
}