package com.l2cccp.gameserver.model.rate;

import com.l2cccp.gameserver.templates.StatsSet;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RateSetting
{
	private final StatsSet set;
	private final int start;
	private final int next;
	private final boolean announce;

	public RateSetting(final int start, final int next, final boolean announce, final StatsSet set)
	{
		this.set = set;
		this.start = start;
		this.next = next;
		this.announce = announce;
	}

	public StatsSet get()
	{
		return set;
	}

	public int getNext()
	{
		return next;
	}

	public int getStart()
	{
		return start;
	}

	public boolean doAnnounce()
	{
		return announce;
	}
}