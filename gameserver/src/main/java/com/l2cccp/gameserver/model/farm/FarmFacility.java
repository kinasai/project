package com.l2cccp.gameserver.model.farm;

import java.util.concurrent.atomic.AtomicInteger;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
import com.l2cccp.gameserver.idfactory.IdFactory;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.FarmInstance;
import com.l2cccp.gameserver.model.items.Restoration;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FarmFacility
{
	private final String name;
	private final String icon;
	private final FarmType type;
	private final int npc;
	private final int lifetime;

	// Fertilize
	private int f_min;
	private int f_max;

	// Reward
	private int r_id;
	private long r_min;
	private long r_max;

	public FarmFacility(final String name, final String icon, final FarmType type, final int npc, final int lifetime)
	{
		this.name = name;
		this.icon = icon;
		this.type = type;
		this.npc = npc;
		this.lifetime = lifetime;
	}

	public void setFertilize(final String[] data)
	{
		this.f_min = Integer.parseInt(data[0]);
		this.f_max = Integer.parseInt(data[1]);
	}

	public void setReward(final String[] data)
	{
		this.r_id = Integer.parseInt(data[0]);
		this.r_min = Integer.parseInt(data[1]);
		this.r_max = Integer.parseInt(data[2]);
	}

	public String getName()
	{
		return name;
	}

	public String getIcon()
	{
		return icon;
	}

	public FarmType getType()
	{
		return type;
	}

	public int getLifeTime()
	{
		return lifetime;
	}

	public AtomicInteger getFertilize()
	{
		return new AtomicInteger(Rnd.get(f_min, f_max));
	}

	public Restoration getReward()
	{
		return new Restoration(r_id, Rnd.get(r_min, r_max));
	}

	public void use(final Player player)
	{
		final FarmInstance farm = new FarmInstance(IdFactory.getInstance().getNextId(), NpcHolder.getInstance().getTemplate(npc));
		farm.setOwner(player.getObjectId(), this);
		farm.setTitle(player.getName());
		farm.spawnMe(player.getLoc());
		player.sendMessage("Вы использовали семя " + name);
	}
}