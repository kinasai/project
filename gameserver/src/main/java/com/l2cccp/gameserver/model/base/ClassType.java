package com.l2cccp.gameserver.model.base;

public enum ClassType
{
	Fighter,
	Mystic,
	Priest
}