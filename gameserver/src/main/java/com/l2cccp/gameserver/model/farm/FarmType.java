package com.l2cccp.gameserver.model.farm;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum FarmType
{
	PLANT,
	BEAST;
}