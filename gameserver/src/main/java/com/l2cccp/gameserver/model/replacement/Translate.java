package com.l2cccp.gameserver.model.replacement;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Translate
{
	private final String _from;
	private final String _to;

	public Translate(String from, String to)
	{
		_from = from;
		_to = to;
	}

	public String getFrom()
	{
		return _from;
	}

	public String getTo()
	{
		return _to;
	}
}