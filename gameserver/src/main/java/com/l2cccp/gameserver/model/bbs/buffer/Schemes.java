package com.l2cccp.gameserver.model.bbs.buffer;

import java.util.ArrayList;
import java.util.List;

public class Schemes
{
	private final int id;
	private final String name;
	private final int priceId;
	private int priceCount;
	private List<Integer> allowedClass;
	private ArrayList<Buff> buffIds = new ArrayList<Buff>();

	public Schemes(int id, String name, int priceId, int priceCount)
	{
		this.id = id;
		this.name = name;
		this.priceId = priceId;
		this.priceCount = priceCount;
	}

	public int getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public int getPriceId()
	{
		return priceId;
	}

	public int getPriceCount()
	{
		return priceCount;
	}

	public void addBuff(Buff buff)
	{
		buffIds.add(buff);
	}

	public ArrayList<Buff> getBuffIds()
	{
		return buffIds;
	}

	public void addAllowedClass(List<Integer> list)
	{
		allowedClass = list;
	}

	public List<Integer> getAllowedClass()
	{
		return allowedClass;
	}
}