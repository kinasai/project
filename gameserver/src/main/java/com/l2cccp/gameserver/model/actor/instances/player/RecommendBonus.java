package com.l2cccp.gameserver.model.actor.instances.player;

import com.l2cccp.gameserver.data.xml.holder.RecommendsHolder;
import com.l2cccp.gameserver.model.Player;

public final class RecommendBonus
{
	public static int getBonus(Player activeChar)
	{
		if(activeChar != null && activeChar.isOnline())
		{
			if(activeChar.getRecommendSystem().getRecommendsHave() == 0)
				return 0;

			int _lvl = (int) Math.ceil(activeChar.getLevel() / 10);
			int _exp = (int) Math.ceil((Math.min(100, activeChar.getRecommendSystem().getRecommendsHave()) - 1) / 10);

			return RecommendsHolder.getInstance().getBonus(_lvl, _exp);
		}

		return 0;
	}

	public static double getMultiplier(Player activeChar)
	{
		double _multiplier = 1;

		int bonus = getBonus(activeChar);
		if(bonus > 0)
			_multiplier = (1 + (bonus / 100));

		if(_multiplier < 1)
			_multiplier = 1;

		return _multiplier;
	}
}