package com.l2cccp.gameserver.model.items.listeners;

import com.l2cccp.gameserver.data.xml.holder.OptionDataHolder;
import com.l2cccp.gameserver.listener.inventory.OnEquipListener;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.s2c.SkillCoolTime;
import com.l2cccp.gameserver.network.l2.s2c.SkillList;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.skills.SkillEntryType;
import com.l2cccp.gameserver.templates.OptionDataTemplate;

public final class ItemAugmentationListener implements OnEquipListener
{
	private static final ItemAugmentationListener _instance = new ItemAugmentationListener();

	public static ItemAugmentationListener getInstance()
	{
		return _instance;
	}

	@Override
	public void onEquip(int slot, ItemInstance item, Playable actor)
	{
		if(!item.isEquipable())
			return;
		if(!item.isAugmented())
			return;

		Player player = actor.getPlayer();

		// При несоотвествии грейда аугмент не применяется
		if(player.getExpertisePenalty(item) > 0)
			return;

		int stats[] = item.getAugmentations();

		boolean sendList = false;
		boolean sendReuseList = false;
		for(int i : stats)
		{
			OptionDataTemplate template = OptionDataHolder.getInstance().getTemplate(i);
			if(template == null)
				continue;

			player.addStatFuncs(template.getStatFuncs(template));

			for(SkillEntry skill : template.getSkills())
			{
				sendList = true;
				player.addSkill(skill.copyTo(SkillEntryType.EQUIP));

				if(player.isSkillDisabled(skill))
					sendReuseList = true;
			}

			player.addTriggers(template);
		}

		if(sendList)
			player.sendPacket(new SkillList(player));

		if(sendReuseList)
			player.sendPacket(new SkillCoolTime(player));

		player.updateStats();
	}

	@Override
	public void onUnequip(int slot, ItemInstance item, Playable actor)
	{
		if(!item.isEquipable())
			return;
		if(!item.isAugmented())
			return;

		Player player = actor.getPlayer();

		int stats[] = item.getAugmentations();

		boolean sendList = false;
		for(int i : stats)
		{
			OptionDataTemplate template = OptionDataHolder.getInstance().getTemplate(i);
			if(template == null)
				continue;

			player.removeStatsByOwner(template);

			for(SkillEntry skill : template.getSkills())
			{
				sendList = true;
				player.removeSkill(skill.getId(), false);
			}

			player.removeTriggers(template);
		}

		if(sendList)
			player.sendPacket(new SkillList(player));

		player.updateStats();
	}
}