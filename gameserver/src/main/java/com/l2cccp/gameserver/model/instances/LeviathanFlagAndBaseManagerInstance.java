package com.l2cccp.gameserver.model.instances;

import com.l2cccp.gameserver.leviathan.type.FlagAndBase;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class LeviathanFlagAndBaseManagerInstance extends LeviathanNpcInstance
{
	public LeviathanFlagAndBaseManagerInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void showChatWindow(Player player, int val, Object... arg)
	{
		final HtmlMessage packet = new HtmlMessage(this);
		init(packet, player.getLanguage());
		player.sendPacket(packet);
	}

	private void init(final HtmlMessage packet, final Language language)
	{
		final FlagAndBase event = getEvent();
		switch(event.getState())
		{
			case PREPARE:
			{
				packet.setFile("leviathan/" + folder + "/index.htm");
				packet.replace("%time%", TimeUtils.formatTime((int) ((event.getTimeStart() - System.currentTimeMillis()) / 1000L), language));
				break;
			}
			case REGISTER:
			{
				packet.setFile("leviathan/" + folder + "/register.htm");
				packet.replace("%registered%", Util.formatAdena(event.getMembers().size()));
				packet.replace("%map%", event.getMap().getName(language));
				packet.replace("%end%", TimeUtils.toSimpleTime(event.getTimeStop()));
				break;
			}
			case REGISTER_CLOSED:
			case RUN:
			{
				packet.setFile("leviathan/" + folder + "/info.htm");
				packet.replace("%blue_size%", Util.formatAdena(event.getTeam(TeamType.BLUE.ordinal()).size()));
				packet.replace("%red_size%", Util.formatAdena(event.getTeam(TeamType.RED.ordinal()).size()));
				packet.replace("%blue_flags%", Util.formatAdena(event.getBase(TeamType.BLUE).getFlags()));
				packet.replace("%red_flags%", Util.formatAdena(event.getBase(TeamType.RED).getFlags()));
				break;
			}
		}

		packet.replace("%name%", event.getTemplate().getName(language));
	}

	@Override
	public FlagAndBase getEvent()
	{
		return (FlagAndBase) event;
	}
}
