package com.l2cccp.gameserver.model.bbs.buffer;

import java.util.List;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Buff
{
	private final int _id;
	private final int _level;
	private List<Integer> _class;

	public Buff(int id, int level)
	{
		_id = id;
		_level = level;
	}

	public int getId()
	{
		return _id;
	}

	public int getLevel()
	{
		return _level;
	}

	public void addAllowedList(List<Integer> list)
	{
		_class = list;
	}

	public List<Integer> getAllowedClass()
	{
		return _class;
	}
}