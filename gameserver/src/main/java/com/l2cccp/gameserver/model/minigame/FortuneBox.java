package com.l2cccp.gameserver.model.minigame;

import java.util.List;

public class FortuneBox
{

	//data
	private final int _id;
	private final String _name;

	private final int _max_rewards;

	//price
	private final int _price_id;
	private final long _price_count;
	private final List<FortuneBoxReward> _rewards;

	public FortuneBox(int id, String name, int max_rewards, int price_id, long price_count, List<FortuneBoxReward> rewards)
	{
		_id = id;
		_name = name;
		_max_rewards = max_rewards;
		_price_id = price_id;
		_price_count = price_count;
		_rewards = rewards;
	}

	public int getId()
	{
		return _id;
	}

	public String getName()
	{
		return _name;
	}

	public int getPriceId()
	{
		return _price_id;
	}

	public long getPriceCount()
	{
		return _price_count;
	}

	public int getMaxRewards()
	{
		return _max_rewards;
	}

	public List<FortuneBoxReward> getRewards()
	{
		return _rewards;
	}
}
