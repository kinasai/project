package com.l2cccp.gameserver.model.balancing;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PlayerBalance
{
	private final double _base;
	private final double _olympiad;

	public PlayerBalance(final double base, final double olympiad)
	{
		_base = base;
		_olympiad = olympiad;
	}

	public double getBalancer(final boolean olymp)
	{
		return olymp ? _olympiad : _base;
	}
}