package com.l2cccp.gameserver.model.entity.events.objects;

import java.util.EnumMap;
import java.util.Map;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
import com.l2cccp.gameserver.idfactory.IdFactory;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.events.Event;
import com.l2cccp.gameserver.model.entity.events.impl.TerritoryWarsEvent;
import com.l2cccp.gameserver.model.instances.TWControlSpotInstance;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TWControlSpotObject implements SpawnableObject
{
	private final Map<TeamType, Integer> npcs;
	private final Location loc;
	private TeamType ownerTeam;

	private TWControlSpotInstance _npc;

	public TWControlSpotObject(Map<TeamType, Integer> npcs, Location loc)
	{
		this.npcs = new EnumMap<TeamType, Integer>(npcs);
		this.loc = loc;
		ownerTeam = TeamType.NONE;
	}

	@Override
	public void spawnObject(Event event)
	{
		NpcTemplate tpl = NpcHolder.getInstance().getTemplate(npcs.get(ownerTeam));
		_npc = new TWControlSpotInstance(IdFactory.getInstance().getNextId(), tpl, this);
		_npc.setHeading(loc.h < 0 ? Rnd.get(0xFFFF) : loc.h);
		_npc.setSpawnedLoc(loc);

		TerritoryWarsEvent war = (TerritoryWarsEvent) event;
		_npc.setReflection(war.getReflection());
		_npc.setCurrentHpMp(_npc.getMaxHp(), _npc.getMaxMp(), true);
		_npc.spawnMe(_npc.getSpawnedLoc());
		_npc.addEvent(war);

		_npc.sendPacket(new MagicSkillUse(_npc, 6783, 1, 0, 0));
		war.putControl(this);
	}

	@Override
	public void despawnObject(Event event)
	{
		TerritoryWarsEvent war = (TerritoryWarsEvent) event;
		war.removeControl(this);

		for(Player $player : World.getAroundPlayers(_npc))
		{
			$player.getAI().notifyEvent(CtrlEvent.EVT_FORGET_OBJECT, _npc);
		}

		_npc.removeEvent(war);
		_npc.deleteMe();
	}

	@Override
	public void refreshObject(Event event)
	{}

	public TeamType getOwnerTeam()
	{
		return ownerTeam;
	}

	public void changeOwnerTeam(TeamType ownerTeam, Event event)
	{
		this.ownerTeam = ownerTeam;

		despawnObject(event);
		spawnObject(event);
	}

	public void changeTeam(TeamType ownerTeam)
	{
		this.ownerTeam = ownerTeam;
	}

	@Override
	public void respawnObject(Event event)
	{}
}