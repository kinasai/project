package com.l2cccp.gameserver.model.battle;

import com.l2cccp.gameserver.Config;

public enum BattleType
{
	PvP(Config.PvP_SYSTEM_ENABLE, Config.PvP_SYSTEM_KILLER_LEVEL_FOR_REWARD, Config.PvP_SYSTEM_TARGET_LEVEL_FOR_REWARD, Config.PvP_SYSTEM_ALLOW_REWARD, Config.PvP_SYSTEM_ITEM_INFO, Config.PvP_SYSTEM_ADD_EXP_SP, Config.PvP_SYSTEM_EXP_SP, Config.PvP_SYSTEM_ENABLE_BLOCK_TIME, Config.PvP_SYSTEM_BLOCK_TIME_AFTER_KILL, Config.PvP_SYSTEM_ANNOUNCE, Config.PvP_SYSTEM_ANNOUNCE_RADIUS, Config.PvP_SYSTEM_IN_ZONE, Config.PvP_SYSTEM_ZONE, "PvP_Time"),
	PK(Config.PK_SYSTEM_ENABLE, Config.PK_SYSTEM_KILLER_LEVEL_FOR_REWARD, Config.PK_SYSTEM_TARGET_LEVEL_FOR_REWARD, Config.PK_SYSTEM_ALLOW_REWARD, Config.PK_SYSTEM_ITEM_INFO, Config.PK_SYSTEM_ADD_EXP_SP, Config.PK_SYSTEM_EXP_SP, Config.PK_SYSTEM_ENABLE_BLOCK_TIME, Config.PK_SYSTEM_BLOCK_TIME_AFTER_KILL, Config.PK_SYSTEM_ANNOUNCE, Config.PK_SYSTEM_ANNOUNCE_RADIUS, Config.PK_SYSTEM_IN_ZONE, Config.PK_SYSTEM_ZONE, "PK_Time");

	private final boolean _enable;
	private final int[] _killer;
	private final int[] _target;
	private final boolean _item;
	private final int[] _items;
	private final boolean _exp_sp;
	private final int[] _exp_sp_data;
	private final boolean _time;
	private final int _times;
	private final boolean _announce;
	private final int _radius;
	private final boolean _in_zone;
	private final String _zone;
	private final String _var;

	private BattleType(boolean enable, int[] killer, int[] target, boolean item, int[] items, boolean exp_sp, int[] exp_sp_data, boolean time, int times, boolean announce, int radius, boolean in_zone, String zone, String var)
	{
		_enable = enable;
		_killer = killer;
		_target = target;
		_item = item;
		_items = items;
		_exp_sp = exp_sp;
		_exp_sp_data = exp_sp_data;
		_time = time;
		_times = times;
		_announce = announce;
		_radius = radius;
		_in_zone = in_zone;
		_zone = zone;
		_var = var;
	}

	public boolean isEnable()
	{
		return _enable;
	}

	public int[] getKiller()
	{
		return _killer;
	}

	public int[] getTarget()
	{
		return _target;
	}

	public boolean giveItem()
	{
		return _item;
	}

	public int[] getItems()
	{
		return _items;
	}

	public boolean giveExpSp()
	{
		return _exp_sp;
	}

	public int[] getExpSp()
	{
		return _exp_sp_data;
	}

	public boolean isTimeBlock()
	{
		return _time;
	}

	public int getTime()
	{
		return _times;
	}

	public boolean isAnnouncement()
	{
		return _announce;
	}

	public int getRadius()
	{
		return _radius;
	}

	public boolean needZone()
	{
		return _in_zone;
	}

	public String getZone()
	{
		return _zone;
	}

	public String getVar()
	{
		return _var;
	}
}
