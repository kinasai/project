package com.l2cccp.gameserver.model.offlinebuffer;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.OfflineBuffersDAO;
import com.l2cccp.gameserver.dao.OfflineBuffsDAO;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.skills.SkillEntry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OfflineBuffers
{
	protected static final OfflineBuffers _instance = new OfflineBuffers();

	public static OfflineBuffers getInstance()
	{
		return _instance;
	}

	private static final Logger _log = LoggerFactory.getLogger(OfflineBuffers.class);

	public void restoreOfflineBuffers()
	{
		Map<Integer, BufferData> map = new HashMap<Integer, BufferData>();
		OfflineBuffersDAO.getInstance().restore(map);
		Player player = null;

		try
		{
			for(Entry<Integer, BufferData> list : map.entrySet())
			{
				player = Player.restore(list.getKey());
				if(player == null)
					continue;

				player.setOfflineMode(true);
				player.setIsOnline(true);

				player.spawnMe();

				if(player.getClan() != null && player.getClan().getAnyMember(player.getObjectId()) != null)
					player.getClan().getAnyMember(player.getObjectId()).setPlayerInstance(player, false);

				BufferData buffer = list.getValue();
				buffer = OfflineBuffsDAO.getInstance().restore(player, buffer);

				OfflineBufferManager.getInstance().getBuffStores().put(player.getObjectId(), buffer);

				player.sitDown(null);

				player.setVisibleTitle(buffer.getSaleTitle());
				player.setVisibleTitleColor(Config.OFFLINE_BUFFER_STORE_TITLE_COLOR);
				player.setVisibleNameColor(Config.OFFLINE_BUFFER_STORE_NAME_COLOR);

				player.setPrivateStoreType(Player.STORE_PRIVATE_BUFF);

				player.broadcastUserInfo(true);

			}
		}
		catch(Exception e)
		{
			_log.warn("Error loading buffer: " + player, e);
			if(player != null)
			{
				player.deleteMe();
			}
		}

		_log.info("Loaded: " + map.size() + " offline buffer(s)");
	}

	public synchronized void onLogin(Player trader)
	{
		OfflineBufferManager.getInstance().getBuffStores().remove(trader.getObjectId());
		OfflineBuffersDAO.getInstance().delete(trader.getObjectId());
		OfflineBuffsDAO.getInstance().delete(trader.getObjectId());
	}

	public synchronized void onLogout(Player trader)
	{
		final BufferData buffer = OfflineBufferManager.getInstance().getBuffStores().get(trader.getObjectId());
		if(buffer == null)
			return;
		OfflineBuffersDAO.getInstance().replace(trader.getObjectId(), buffer);
		String buffs = joinAllSkillsToString(buffer.getBuffs().values());
		OfflineBuffsDAO.getInstance().replace(trader.getObjectId(), buffs);
	}

	public final String joinAllSkillsToString(Collection<SkillEntry> collection)
	{
		if(collection.isEmpty())
			return "";

		String result = "";
		for(SkillEntry val : collection)
		{
			result += val.getId() + ",";
		}

		return result.substring(0, result.length() - 1);
	}
}
