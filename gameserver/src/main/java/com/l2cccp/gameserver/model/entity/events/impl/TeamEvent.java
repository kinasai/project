package com.l2cccp.gameserver.model.entity.events.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.ArrayUtils;

import com.l2cccp.commons.collections.JoinedIterator;
import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.commons.time.cron.SchedulingPattern;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.data.xml.holder.EventMapsHolder;
import com.l2cccp.gameserver.data.xml.holder.InstantZoneHolder;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.Zone.ZoneType;
import com.l2cccp.gameserver.model.base.RestartType;
import com.l2cccp.gameserver.model.base.SpecialEffectState;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.entity.events.EventType;
import com.l2cccp.gameserver.model.entity.events.maps.EventMaps;
import com.l2cccp.gameserver.model.entity.events.objects.DuelSnapshotObject;
import com.l2cccp.gameserver.model.entity.events.util.EventState;
import com.l2cccp.gameserver.model.entity.events.util.RegisterAsk;
import com.l2cccp.gameserver.model.entity.events.util.Utils;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.IBroadcastPacket;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.network.l2.s2c.PlaySound;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.AbnormalEffect;
import com.l2cccp.gameserver.templates.InstantZone;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;
import com.l2cccp.gameserver.utils.ReflectionUtils;
import com.l2cccp.gameserver.utils.TimeUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public abstract class TeamEvent extends SingleMatchEvent implements Iterable<DuelSnapshotObject>
{
	public static final String CHEST = "chest";
	public static final String MAKEMAP = "makemap";

	protected EventState state;
	public TeamType winner;
	protected final Map<String, Integer> _scores = new ConcurrentHashMap<String, Integer>();

	protected final SchedulingPattern pattern;
	protected Reflection reflection;
	protected Zone zone;
	protected EventMaps map;
	protected long startTime;
	protected boolean block_party;
	protected int minLevel;
	protected int maxLevel;
	protected final boolean battlepvp;

	protected boolean levelMul;
	protected final int[] rewardItems;
	protected final long[] rewardCounts;
	protected final String[] rewardPosition;

	protected final int[] blockItems;
	public final int[] blockSkills;

	public final int maxChest;
	private int chestSpawns = 0;

	protected TeamEvent(MultiValueSet<String> set)
	{
		super(set);
		pattern = new SchedulingPattern(set.getString("pattern"));

		block_party = set.getBool("block_party", false);
		minLevel = set.getInteger("min_level", 1);
		maxLevel = set.getInteger("max_level", 99);

		maxChest = set.getInteger("max_chest", 48);

		battlepvp = set.getBool("increase_pvp", false);

		String positions = set.getString("reward_position", null);
		rewardPosition = positions != null ? positions.split(";") : null;
		rewardItems = set.getIntegerArray("reward_items", null);
		rewardCounts = set.getLongArray("reward_counts", null);
		levelMul = set.getBool("reward_level_mul", false);

		blockItems = set.getIntegerArray("block_items", null);
		blockSkills = set.getIntegerArray("block_skills", null);
		startTime = 0;
	}

	@Override
	public void startEvent()
	{
		winner = null;

		teamCheck(true);

		if(!checkTeamSize())
		{
			setState(EventState.NO_PLAYERS);
			announceToValidPlayer("event.сancelled");
			stopEvent();
			return;
		}

		setState(EventState.FIRST_PHASE);
		updatePlayers(true, false);
		sendPackets(PlaySound.B04_S01, SystemMsg.LET_THE_DUEL_BEGIN);
		super.startEvent();
	}

	@Override
	public void stopEvent()
	{
		if(zone != null)
		{
			zone.setActive(false);
			zone = null;
		}

		for(final DuelSnapshotObject object : this)
			removeObject(object.getTeam(), object);

		_scores.clear();

		_log.info("Stop event " + getName() + " - " + getId());

		super.stopEvent();
	}

	@Override
	public void reCalcNextTime(boolean onInit)
	{
		clearActions();

		if(pattern != null)
			startTime = pattern.next(System.currentTimeMillis() + 60000L);

		if(!onInit)
			printInfo();

		registerActions();
	}

	@Override
	public void action(final String name, final boolean start)
	{
		if(name.startsWith(CHEST))
		{
			if(chestSpawns < maxChest && state != EventState.SECOND_PHASE)
				spawnChest(name.split(":"));
		}
		else if(name.equals(MAKEMAP))
		{
			if(reflection != null)
			{
				setReflection(null);
				getReflection();
			}

			makeMap();
		}
		else
			super.action(name, start);
	}

	// Метод выхода персонажа с ивента
	protected void quit(DuelSnapshotObject obj)
	{
		Player player = obj.getPlayer();
		if(obj.getTeamId() != 0)
		{
			player.setTeamId(0);
			player.setVisibleTitle(null);
			player.setVisibleTitleColor(0);
			player.setVisibleNameColor(0);
		}

		player.setTeam(TeamType.NONE);

		player.stopAttackStanceTask();

		// Удаляем таргеты с цели!
		for(Player $player : World.getAroundPlayers(player))
		{
			$player.getAI().notifyEvent(CtrlEvent.EVT_FORGET_OBJECT, player);
			if(player.getServitor() != null)
				$player.getAI().notifyEvent(CtrlEvent.EVT_FORGET_OBJECT, player.getServitor());
		}

		// Удаляем таргет у цели!
		GameObject target = player.getTarget();
		if(target != null)
			player.getAI().notifyEvent(CtrlEvent.EVT_FORGET_OBJECT, target);

		if(block_party)
			player.blockParty(false);

		player.doRevive(100);

		// Хилим цель!
		player.setCurrentMp(player.getMaxMp());
		player.setCurrentCp(player.getMaxCp());
		player.setCurrentHp(player.getMaxHp(), true);

		player.removeEvent(this);

		player.sendChanges();

		obj.restore();
		player.setInvul(SpecialEffectState.FALSE);
		player.setInvisible(SpecialEffectState.FALSE);
		obj.teleportBack(500L);
	}

	protected void teamCheck(boolean second)
	{
		for(DuelSnapshotObject object : this)
		{
			if(object == null)
				continue;

			Player player;
			if((player = object.getPlayer()) == null)
				continue;

			if(!checkPlayer(player, second))
			{
				if(player.isTeleporting()) // Если игрок все еще в стадии телепорта - его невозможно повторно телепортировать, переносим на арену, ток без отражения
				{
					player.setReflection(ReflectionManager.DEFAULT);
					_log.debug(getName() + ": player teleporting error: ", player);
				}
				else
					object.teleportBack(500L);

				player.sendPacket(new HtmlMessage(0).setFile("events/custom_event_cancel.htm"));
				player.removeEvent(this);
				removeObject(object.getTeam(), object);

				if(!canWalkInWaitTime())
					unparalyze(player);
			}
		}
	}

	protected void setState(EventState s)
	{
		state = s;
	}

	protected void updatePlayers(boolean start, boolean teleport)
	{
		for(DuelSnapshotObject snapshot : this)
		{
			if(snapshot.getPlayer() == null)
				continue;

			if(state == EventState.NO_PLAYERS)
			{
				snapshot.getPlayer().removeEvent(this);
				getObjects(snapshot.getTeam()).remove(snapshot);
				continue;
			}

			if(teleport && state == EventState.FINISH)
				snapshot.teleportBack(500L);
			else
			{
				Player player = snapshot.getPlayer();
				if(start)
				{
					player.setTeam(snapshot.getTeam());

					if(!canWalkInWaitTime())
						unparalyze(player);

					if(block_party)
					{
						player.blockParty(true);

						if(player.isInParty())
							player.leaveParty();
					}

					player.setCurrentMp(player.getMaxMp());
					player.setCurrentCp(player.getMaxCp());
					player.setCurrentHp(player.getMaxHp(), true);
				}
				else
					quit(snapshot);

				actionUpdate(start, player);
			}
		}
	}

	protected void paralyze(Player player)
	{
		if(!player.isRooted())
		{
			player.startRooted();
			player.startAbnormalEffect(AbnormalEffect.ROOT);
		}

		Servitor pet = player.getServitor();
		if(pet != null)
		{
			if(!pet.isRooted())
			{
				pet.startRooted();
				pet.startAbnormalEffect(AbnormalEffect.ROOT);
			}
		}
	}

	protected void unparalyze(Player player)
	{
		if(player.isRooted())
		{
			player.stopRooted();
			player.stopAbnormalEffect(AbnormalEffect.ROOT);
		}

		Servitor pet = player.getServitor();
		if(pet != null)
		{
			if(pet.isRooted())
			{
				pet.stopRooted();
				pet.stopAbnormalEffect(AbnormalEffect.ROOT);
			}
		}
	}

	protected void announceToValidPlayer(String str)
	{
		boolean sendAsk = state == EventState.REGISTRATION;
		for(Player player : GameObjectsStorage.getPlayers())
		{
			if(player != null)
			{
				if(player.isGM())
					sendGMInfo(player);

				if(player.isGM() || checkLevel(player))
					player.sendPacket(new Say2(0, ChatType.CRITICAL_ANNOUNCE, "", new CustomMessage(str).addString(getName()).addNumber(minLevel).addNumber(maxLevel).addString(map.getName()).toString(player), null));

				if(sendAsk && (player.isGM() && player.isDebug() || !player.isGM()) && !player.isDead() && checkLevel(player) && player.getReflection().isDefault() && !player.isInOlympiadMode() && !player.isInObserverMode() && player.getEvent(SingleMatchEvent.class) == null)
				{
					String msg = new CustomMessage("event.registration.ask").addString(getName()).addNumber(minLevel).addNumber(maxLevel).addString(map.getName()).toString(player);
					ConfirmDlg ask = new ConfirmDlg(SystemMsg.S1, 60000);
					ask.addString(msg);
					player.ask(ask, new RegisterAsk(this));
				}
			}
		}
	}

	private void sendGMInfo(Player player)
	{
		if(player.isDebug())
			player.sendPacket(new Say2(0, ChatType.PETITION_GM, "EVENT", getName() + "[" + getId() + "] -> " + state + ", R: " + map.getInstance(), null));
	}

	public void sendPacket(final IBroadcastPacket packet)
	{
		sendPackets(packet);
	}

	public void sendPackets(final IBroadcastPacket... packet)
	{
		for(final DuelSnapshotObject d : this)
		{
			if(d.getPlayer() != null)
				d.getPlayer().sendPacket(packet);
		}
	}

	public void sendPacket(final IBroadcastPacket packet, final TeamType... ar)
	{
		for(final DuelSnapshotObject d : this)
		{
			if(d.getPlayer() != null)
				d.getPlayer().sendPacket(packet);
		}
	}

	@Override
	public void checkRestartLocs(Player player, Map<RestartType, Boolean> r)
	{
		r.clear();
	}

	/**
	 * Отправка инфо о следующем старте ивента.
	 */
	@Override
	public void printInfo()
	{
		if(startTime == 0)
			info(getName() + " " + minLevel + "-" + maxLevel + " time - undefined");
		else
			info(getName() + " " + minLevel + "-" + maxLevel + " time - " + TimeUtils.toSimpleFormat(startTime));
	}

	private void makeMap()
	{
		List<EventMaps> maps = EventMapsHolder.getInstance().getMapsForEvent(getId());
		int id = maps.size() > 1 ? Rnd.get(0, maps.size() - 1) : 0;
		map = maps.get(id);
		zone = ReflectionUtils.getZone(map.getZoneTemplate().getName());
		zone.setReflection(getReflection());
		zone.setActive(true);
		_log.info("Make Map " + getName() + " - " + getId() + ", Zone: " + zone.getName() + ":" + zone.getReflection().getId() + ", Active: " + zone.isActive());
	}

	protected void spawnChest(String[] chest)
	{
		final int id = Integer.parseInt(chest[1]);
		final int count = Integer.parseInt(chest[2]);
		final int despawn = Integer.parseInt(chest[3]) * 1000;
		for(Location location : getChestSpawns(count))
		{
			NpcUtils.spawnSingle(id, location, getReflection(), despawn);
			chestSpawns++;
		}
	}

	public Location[] getChestSpawns(int count)
	{
		Location[] locs = new Location[count];
		List<Location> exclude = new ArrayList<Location>(map.getKeyLocations().length - count);

		int i = 0;
		while(i < count)
		{
			Location loc = Rnd.get(map.getKeyLocations());

			boolean excluded = false;
			for(Location exLocation : exclude)
			{
				if(exLocation.equals(loc))
				{
					excluded = true;
					break;
				}
			}

			if(!excluded)
			{
				locs[i] = loc;
				i++;
				exclude.add(loc);
			}
		}

		return locs;
	}

	protected void actionUpdate(boolean start, Player player)
	{}

	protected void announceKill(Player killer, Player actor, List<DuelSnapshotObject> objs)
	{
		int score = 0;
		final String _obj = killer.getName();
		if(_scores.get(_obj) != null)
			score = _scores.get(_obj);

		score++;

		_scores.put(_obj, score);

		CustomMessage text = Utils.killMsg(killer.getName(), actor.getName(), score);
		for(DuelSnapshotObject d : objs)
		{
			if(d.getPlayer() != null)
				d.getPlayer().sendPacket(new ExShowScreenMessage(text.toString(d.getPlayer()), 3000, ScreenMessageAlign.MIDDLE_CENTER, true));
		}
	}

	/**
	 * Геттеры
	 */
	@Override
	public Reflection getReflection()
	{
		if(reflection == null)
		{
			InstantZone instant = InstantZoneHolder.getInstance().getInstantZone(getId());
			reflection = new Reflection();
			reflection.init(instant);
			setReflection(reflection);
		}

		return reflection;
	}

	protected void setReflection(final Reflection ref)
	{
		if(ref == null)
		{
			if(!getReflection().isDefault() && !getReflection().isCollapseStarted())
			{
				getReflection().collapse();
				reflection = null;
			}
		}
		else
			reflection = ref;
	}

	@Override
	public EventType getType()
	{
		return EventType.PVP_EVENT;
	}

	@Override
	public long startTimeMillis()
	{
		return startTime;
	}

	public int getMinLevel()
	{
		return minLevel;
	}

	public int getMaxLevel()
	{
		return maxLevel;
	}

	public TeamType getWinner()
	{
		return winner;
	}

	protected boolean canWalkInWaitTime()
	{
		return false;
	}

	@Override
	public boolean isInProgress()
	{
		return state != EventState.REGISTRATION && state != EventState.NONE;
	}

	public boolean isRegistrationOver()
	{
		return state != EventState.REGISTRATION;
	}

	protected Location getTeleportLoc(TeamType team)
	{
		Location[] loc = map.getTeamSpawns().get(team.ordinalWithoutNone());
		return loc[Rnd.get(0, loc.length - 1)];
	}

	@Override
	public boolean canResurrect(Creature active, Creature target, boolean force, boolean quiet)
	{
		active.sendPacket(SystemMsg.INVALID_TARGET);
		return false;
	}

	@Override
	public Iterator<DuelSnapshotObject> iterator()
	{
		List<DuelSnapshotObject> blue = getObjects(TeamType.BLUE);
		List<DuelSnapshotObject> red = getObjects(TeamType.RED);
		return new JoinedIterator<DuelSnapshotObject>(blue.iterator(), red.iterator());
	}

	@Override
	public SystemMsg checkForAttack(Creature target, Creature attacker, Skill skill, boolean force)
	{
		if(!canAttack(target, attacker, skill, force, false))
			return SystemMsg.INVALID_TARGET;

		return null;
	}

	@Override
	public boolean canAttack(Creature target, Creature attacker, Skill skill, boolean force, boolean nextAttackCheck)
	{
		if(skill != null && blockSkills != null)
		{
			for(final int id : blockSkills)
			{
				if(id == skill.getId())
					return false;
			}
		}

		if(!isInProgress() || target.getTeam() == TeamType.NONE || attacker.getTeam() == TeamType.NONE || target.getTeam() == attacker.getTeam())
			return false;

		return true;
	}

	@Override
	public IBroadcastPacket canUseItem(Player player, ItemInstance item)
	{
		if(item != null && blockItems != null)
		{
			if(ArrayUtils.contains(blockItems, item.getItemId()))
				return new SystemMessage(SystemMsg.S1_CANNOT_BE_USED_DUE_TO_UNSUITABLE_TERMS).addItemName(item.getItemId());
		}

		return super.canUseItem(player, item);
	}

	public boolean checkLevel(Player player)
	{
		return player.getLevel() >= minLevel && player.getLevel() <= maxLevel;
	}

	public boolean registerPlayer(Player player)
	{
		if(isRegistrationOver())
		{
			player.sendMessage(new CustomMessage("event.registration.off").addString(getName()));
			return false;
		}

		for(DuelSnapshotObject d : this)
		{
			if(d.getPlayer() == player)
			{
				player.sendMessage(new CustomMessage("event.registration.allready").addString(getName()));
				return false;
			}
		}

		if(!checkPlayer(player, false))
		{
			player.sendMessage(new CustomMessage("event.registration.failed").addString(getName()));
			return false;
		}

		return true;
	}

	protected boolean checkTeamSize()
	{
		List<Object> red = getObjects(TeamType.RED);
		List<Object> blue = getObjects(TeamType.BLUE);
		if(red == null || blue == null) {
			return false;
		}
		if((blue.size() + red.size()) < map.getMin()) {
			return false;
		}		
		return true;
	}

	protected boolean checkPlayer(Player player, boolean second)
	{
		if(player.isInOfflineMode())
		{
			player.sendMessage("Вы не можете принимать участие в событии находясь в режиме торговли!");
			return false;
		}
		else if(!checkLevel(player))
		{
			player.sendMessage("Ваш уровень не подходит для участия в данном событии!");
			return false;
		}
		else if(player.isDead())
		{
			player.sendMessage("Вы мертвы, принимать участие в данном состоянии запрещено!");
			return false;
		}
		else if(player.isDead())
		{
			player.sendMessage("Запрещено принимать участие в событии, находясь верхом на ездовом питомце, на корабле или в режиме наблюдения!");
			return false;
		}
		if(player.isInDuel())
		{
			player.sendMessage("Запрещено принимать участие в событии находясь в дуели!");
			return false;
		}
		else if(player.getOlympiadGame() != null || Olympiad.isRegistered(player))
		{
			player.sendMessage("Запрещено принимать участие в событии проводя Олимпийские сражения!");
			return false;
		}
		else if(player.isInParty() && player.getParty().isInDimensionalRift())
		{
			player.sendMessage("Запрещено принимать участие в событии находясь в группе!");
			return false;
		}
		else if(player.isTeleporting())
		{
			player.sendMessage("Запрещено принимать участие в событии в режиме телепорта!");
			return false;
		}
		else if(player.getEvent(SiegeEvent.class) != null)
		{
			player.sendMessage("Запрещено принимать участие в событии принимая участие в осадных событиях!");
			return false;
		}
		else if(player.isTerritoryFlagEquipped())
		{
			player.sendMessage("Запрещено принимать участие в событии с осадным флагом!");
			return false;
		}
		else if(!second)
		{
			final SingleMatchEvent evt = player.getEvent(SingleMatchEvent.class);
			if(evt != null && evt != this)
			{
				player.sendMessage("Запрещено принимать участие в событии при активном участии в другом событии!");
				return false;
			}
			else if(player.getReflection() != ReflectionManager.DEFAULT)
			{
				player.sendMessage("Запрещено принимать участие в событии находясь в отражении мира!");
				return false;
			}
			else if(player.isInZone(ZoneType.epic))
			{
				player.sendMessage("Запрещено принимать участие в событии в эпической зоне!");
				return false;
			}
			else
				return true;
		}
		else
			return true;
	}

	protected void dispelBuffs(Player player)
	{
		for(Effect e : player.getEffectList().getAllEffects())
		{
			if(!e.getSkill().getTemplate().isOffensive() && !e.getSkill().getTemplate().isNewbie() && e.isCancelable() && !e.getSkill().getTemplate().isPreservedOnDeath())
			{
				player.sendPacket(new SystemMessage(SystemMsg.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(e.getSkill().getId(), e.getSkill().getLevel()));
				e.exit();
			}
		}

		final Servitor servitor = player.getServitor();
		if(servitor != null)
		{
			for(Effect e : servitor.getEffectList().getAllEffects())
			{
				if(!e.getSkill().getTemplate().isOffensive() && !e.getSkill().getTemplate().isNewbie() && e.isCancelable() && !e.getSkill().getTemplate().isPreservedOnDeath())
					e.exit();
			}
		}
	}
}