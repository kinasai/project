package com.l2cccp.gameserver.model.entity.events.objects;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
import com.l2cccp.gameserver.idfactory.IdFactory;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.entity.events.Event;
import com.l2cccp.gameserver.model.entity.events.impl.KingHillEvent;
import com.l2cccp.gameserver.model.instances.HillControlSpotInstance;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class HillControlSpotObject implements SpawnableObject
{
	private final int id;
	private final Location loc;

	private HillControlSpotInstance _npc;

	public HillControlSpotObject(final int id, final Location loc)
	{
		this.id = id;
		this.loc = loc;
	}

	@Override
	public void spawnObject(final Event event)
	{
		NpcTemplate tpl = NpcHolder.getInstance().getTemplate(id);
		_npc = new HillControlSpotInstance(IdFactory.getInstance().getNextId(), tpl, this);
		_npc.setHeading(loc.h < 0 ? Rnd.get(0xFFFF) : loc.h);
		_npc.setSpawnedLoc(loc);

		KingHillEvent war = (KingHillEvent) event;
		_npc.setReflection(war.getReflection());
		_npc.setCurrentHpMp(_npc.getMaxHp(), _npc.getMaxMp(), true);
		_npc.spawnMe(_npc.getSpawnedLoc());
		_npc.addEvent(war);
	}

	@Override
	public void despawnObject(final Event event)
	{
		for(Player $player : World.getAroundPlayers(_npc))
		{
			$player.getAI().notifyEvent(CtrlEvent.EVT_FORGET_OBJECT, _npc);
		}

		_npc.removeEvent(event);
		_npc.deleteMe();
	}

	public void changeOwnerTeam(final int owner, Event event)
	{
		KingHillEvent king = (KingHillEvent) event;
		king.changeOwnerTeam(owner);
		despawnObject(king);
		spawnObject(king);
	}

	@Override
	public void refreshObject(final Event event)
	{}

	@Override
	public void respawnObject(final Event event)
	{}
}