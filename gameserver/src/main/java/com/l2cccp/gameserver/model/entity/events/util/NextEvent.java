package com.l2cccp.gameserver.model.entity.events.util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class NextEvent
{
	private final long _time;
	private final String _name;

	public NextEvent(final long time, final String name)
	{
		_time = time;
		_name = name;
	}

	public long getTime()
	{
		return _time;
	}

	public String getName()
	{
		return _name;
	}
}