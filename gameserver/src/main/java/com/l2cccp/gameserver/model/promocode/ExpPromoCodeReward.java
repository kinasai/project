package com.l2cccp.gameserver.model.promocode;

import org.dom4j.Element;

import com.l2cccp.gameserver.model.Player;

public class ExpPromoCodeReward extends PromoCodeReward
{
	public long _value;

	public ExpPromoCodeReward(Element element)
	{
		_value = Long.parseLong(element.attributeValue("val"));
	}

	@Override
	public void giveReward(Player player)
	{
		player.addExpAndSp(_value, 0);
	}
}
