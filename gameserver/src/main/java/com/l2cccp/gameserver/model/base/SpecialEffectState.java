package com.l2cccp.gameserver.model.base;

/**
 * Описание состояния спец. эффектов: неуязвимости, невидимости, бессмертности.
 *
 * @author G1ta0
 */
public enum SpecialEffectState
{
	FALSE,
	TRUE,
	GM
}
