package com.l2cccp.gameserver.model.farm;

import com.l2cccp.gameserver.model.instances.FarmInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FarmRemoveMe implements Runnable
{
	private final FarmInstance npc;

	public FarmRemoveMe(final FarmInstance npc)
	{
		this.npc = npc;
	}

	@Override
	public void run()
	{
		if(npc != null)
		{
			npc.announce();
			npc.deleteMe();
		}
	}
}