package com.l2cccp.gameserver.model.instances;

import com.l2cccp.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

public class MonsterWithLongRespawnInstance extends MonsterInstance
{
	public MonsterWithLongRespawnInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	protected void onDecay()
	{
		super.onDecay();
		RaidBossSpawnManager.getInstance().onBossDespawned(this);
	}

	@Override
	protected void onSpawn()
	{
		super.onSpawn();
		RaidBossSpawnManager.getInstance().onBossSpawned(this);
	}
}