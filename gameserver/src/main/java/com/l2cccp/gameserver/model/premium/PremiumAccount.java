package com.l2cccp.gameserver.model.premium;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.utils.ItemFunctions;

public class PremiumAccount
{
	private final int _id;
	private final String _name;
	private final String _icon;
	private final List<PremiumGift> _gifts;
	private final Map<Integer, PremiumVariant> _variants;

	// Rates
	private double _exp = 1.;
	private double _sp = 1.;
	private double _epaulette = 1.;
	private double _adena = 1.;
	private double _spoil = 1.;
	private double _items = 1.;
	private double _weight = 1.;
	private int _craft = 0;
	private int _masterwork = 0;

	//Chat
	private boolean _shout;
	private boolean _hero;

	//Hero
	private PremiumHero _hero_type;

	//Noble
	private boolean _noblesse;

	public PremiumAccount(final int id, final String name, final String icon, final List<PremiumGift> gifts)
	{
		_id = id;
		_name = name;
		_icon = icon;
		_gifts = gifts;
		_variants = new HashMap<Integer, PremiumVariant>();
	}

	public int getId()
	{
		return _id;
	}

	public String getName()
	{
		return _name;
	}

	public String getIcon()
	{
		return _icon;
	}

	public double getExp()
	{
		return _exp;
	}

	public double getSp()
	{
		return _sp;
	}

	public double getEpaulette()
	{
		return _epaulette;
	}

	public double getAdena()
	{
		return _adena;
	}

	public double getSpoil()
	{
		return _spoil;
	}

	public double getItems()
	{
		return _items;
	}

	public double getWeight()
	{
		return _weight;
	}

	public int getCraftChance()
	{
		return _craft;
	}

	public int getMasterWorkChance()
	{
		return _masterwork;
	}

	public void putVariant(final PremiumVariant variant)
	{
		_variants.put(variant.getId(), variant);
	}

	public PremiumVariant getVariant(final Integer id)
	{
		return _variants.get(id);
	}

	public Map<Integer, PremiumVariant> getVariants()
	{
		return _variants;
	}

	public void setRate(final PremiumKeys key, final String value)
	{
		switch(key)
		{
			case ADENA:
				_adena = Double.parseDouble(value);
				break;
			case CRAFT:
				_craft = Integer.parseInt(value);
				break;
			case DROP:
				_items = Double.parseDouble(value);
				break;
			case EXP:
				_exp = Double.parseDouble(value);
				break;
			case MASTERWORK_CRAFT:
				_masterwork = Integer.parseInt(value);
				break;
			case SIEGE:
				_epaulette = Double.parseDouble(value);
				break;
			case SP:
				_sp = Double.parseDouble(value);
				break;
			case SPOIL:
				_spoil = Double.parseDouble(value);
				break;
			case WEIGHT_LIMIT:
				_weight = Double.parseDouble(value);
				break;
		}
	}

	public boolean canShout()
	{
		return _shout;
	}

	public boolean canHeroVoice()
	{
		return _hero;
	}

	public void setChat(final PremiumChat chat, final String value)
	{
		switch(chat)
		{
			case SHOUT:
				_shout = Boolean.parseBoolean(value);
				break;
			case HERO:
				_hero = Boolean.parseBoolean(value);
				break;
		}
	}

	public PremiumHero getHero()
	{
		return _hero_type;
	}

	public void setHero(final PremiumHero hero)
	{
		_hero_type = hero;
	}

	public boolean getNoble()
	{
		return _noblesse;
	}

	public void setNoble(final boolean noble)
	{
		_noblesse = noble;
	}

	public void giveGifts(final Player player)
	{
		boolean give = false;
		for(final PremiumGift gift : _gifts)
		{
			if(gift.isRemovable() && ItemFunctions.getItemCount(player, gift.getId()) > 0)
				continue;

			if(player.getWeightPercents() >= 80 || player.getUsedInventoryPercents() >= 90)
			{
				player.sendPacket(new SystemMessage(SystemMsg.THE_PREMIUM_ITEM_CANNOT_BE_RECEIVED_BECAUSE_THE_INVENTORY_WEIGHTQUANTITY_LIMIT_HAS_BEEN_EXCEEDED));
				return;
			}

			ItemFunctions.addItem(player, gift.getId(), gift.getCount());
			give = true;
		}

		if(give)
			player.sendPacket(new SystemMessage(SystemMsg.THE_PREMIUM_ITEM_FOR_THIS_ACCOUNT_WAS_PROVIDED));
	}

	public void removeGifts(final Player player)
	{
		boolean removed = false;

		for(final PremiumGift gift : _gifts)
		{
			if(gift.isRemovable() && ItemFunctions.deleteItem(player, gift.getId(), gift.getCount()))
				removed = true;
		}

		if(removed)
			player.sendPacket(new SystemMessage(SystemMsg.THE_REMIUM_ACCOUNT_HAS_BEEN_TERMINATED));
	}
}