package com.l2cccp.gameserver.model.promocode;

import org.dom4j.Element;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.s2c.InventoryUpdate;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.templates.item.ItemTemplate;

public class ItemPromoCodeReward extends PromoCodeReward
{
	public int _itemId;
	public long _itemCount;
	private int _enchant;
	private int _durability;

	public ItemPromoCodeReward(Element element)
	{
		_itemId = Integer.parseInt(element.attributeValue("id"));
		_itemCount = Long.parseLong(element.attributeValue("count", "1"));
		_enchant = Integer.parseInt(element.attributeValue("enchant", "0"));
		_durability = Integer.parseInt(element.attributeValue("durability", "0"));
	}

	@Override
	public void giveReward(Player player)
	{
		ItemTemplate t = ItemHolder.getInstance().getTemplate(_itemId);
		if(t.isStackable())
		{
			player.getInventory().addItem(_itemId, _itemCount);
		}
		else
		{
			InventoryUpdate inventoryUpdate = new InventoryUpdate();
			for(long i = 0; i < _itemCount; i++)
			{
				ItemInstance itemInstance = player.getInventory().addItem(_itemId, 1);

				if(_enchant > 0)
				{
					itemInstance.setEnchantLevel(_enchant);
					itemInstance.setJdbcState(JdbcEntityState.UPDATED);
				}

				if(_durability > 0)
				{
					itemInstance.setCustomFlags(ItemInstance.FLAG_TEMPORAL);
					itemInstance.setLifeTime((int) (System.currentTimeMillis() / 1000L) + _durability * 60);
					itemInstance.setJdbcState(JdbcEntityState.UPDATED);
				}

				itemInstance.update();
				inventoryUpdate.addModifiedItem(itemInstance);
			}
			player.sendPacket(inventoryUpdate);
		}

		player.sendPacket(SystemMessage.obtainItems(_itemId, _itemCount, _enchant));
	}
}
