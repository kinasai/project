package com.l2cccp.gameserver.model.balancing;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class DropConstructor
{
	private static DropConstructor _instance = new DropConstructor();

	public static DropConstructor getInstance()
	{
		return _instance;
	}

	public List<DropBalance> construct(String data)
	{
		String[] array = data.split(";");
		List<DropBalance> list = new ArrayList<DropBalance>();

		for(String item : array)
		{
			StringTokenizer st = new StringTokenizer(item, "{}():");
			final BalanceType type = BalanceType.valueOf(st.nextToken());
			final int id = type == BalanceType.ID ? Integer.parseInt(st.nextToken()) : -1;
			final double count = Double.parseDouble(st.nextToken());
			final double chance = Double.parseDouble(st.nextToken());
			DropBalance balance = new DropBalance(type, id, count, chance);
			list.add(balance);
		}

		return list;
	}
}