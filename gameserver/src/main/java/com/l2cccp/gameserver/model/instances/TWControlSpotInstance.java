package com.l2cccp.gameserver.model.instances;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.events.objects.TWControlSpotObject;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TWControlSpotInstance extends NpcInstance
{
	private static final SkillEntry TAKE_CONTROL_POINT_SKILL = SkillTable.getInstance().getSkillEntry(40000, 1);

	private final TWControlSpotObject _twControlSpotObject;

	public TWControlSpotInstance(int objectId, NpcTemplate template, TWControlSpotObject object)
	{
		super(objectId, template);
		_twControlSpotObject = object;
	}

	@Override
	protected void onDecay()
	{
		decayMe();
	}

	@Override
	public boolean isAttackable(Creature attacker)
	{
		return false;
	}

	@Override
	public boolean isAutoAttackable(Creature attacker)
	{
		return false;
	}

	@Override
	public Clan getClan()
	{
		return null;
	}

	@Override
	public void showChatWindow(Player player, final int val, Object... arg)
	{
		if(getControlSpotObject().getOwnerTeam() == player.getTeam())
			return;
		else if(player.getTeam() == TeamType.NONE)
		{
			System.out.println("Spot window none team for player -> " + player.getName());
			return;
		}

		String filename = "events/territory_wars/control_spot_" + getControlSpotObject().getOwnerTeam().ordinal() + ".htm";
		super.showChatWindow(player, filename);
	}

	@Override
	public final void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this) || getControlSpotObject().getOwnerTeam() == player.getTeam())
			return;
		else if(player.getTeam() == TeamType.NONE)
		{
			System.out.println("Feedback none team for player -> " + player.getName());
			return;
		}

		if(command.startsWith("takecontrolpoint"))
			player.doCast(TAKE_CONTROL_POINT_SKILL, this, false);
		else
			super.onBypassFeedback(player, command);
	}

	public TWControlSpotObject getControlSpotObject()
	{
		return _twControlSpotObject;
	}
}