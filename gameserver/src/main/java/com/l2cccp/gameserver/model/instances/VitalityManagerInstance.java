package com.l2cccp.gameserver.model.instances;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.gameserver.data.client.holder.NpcStringLineHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.network.l2.s2c.NpcSay;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.templates.client.NpcStringLine;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.TimeUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class VitalityManagerInstance extends NpcInstance
{
	public VitalityManagerInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equals("gift"))
		{
			if(canGiveEventData(player))
			{
				castBuffForQuestReward(player, 1519058945);
				castBuffForQuestReward(player, 1519124481);
				showHtm(player, "br_vi_stevu002.htm");
			}
			else
				showHtm(player, "br_vi_stevu003.htm");
		}
		else if(command.equals("player"))
		{
			if(player.getLevel() <= 75)
				showHtm(player, "br_vi_stevu005.htm");
			else if(!player.isMageClass())
			{
				castBuffForQuestReward(player, 283246593);
				castBuffForQuestReward(player, 283312129);
				castBuffForQuestReward(player, 369426433);
				castBuffForQuestReward(player, 283377665);
				castBuffForQuestReward(player, 283443201);
				castBuffForQuestReward(player, 283508737);
				castBuffForQuestReward(player, 369098753);
				showHtm(player, "br_vi_stevu006.htm");
			}
			else
			{
				castBuffForQuestReward(player, 283246593);
				castBuffForQuestReward(player, 283312129);
				castBuffForQuestReward(player, 369426433);
				castBuffForQuestReward(player, 283639809);
				castBuffForQuestReward(player, 283705345);
				castBuffForQuestReward(player, 283770881);
				castBuffForQuestReward(player, 283836417);
				showHtm(player, "br_vi_stevu007.htm");
			}
		}
		else if(command.equals("summon"))
		{
			final Servitor servitor = player.getServitor();
			if(player.getLevel() <= 75)
				showHtm(player, "br_vi_stevu011.htm");
			else if(servitor != null && servitor.isSummon() && servitor.isPet())
			{
				castBuffForQuestReward(servitor, 283246593);
				castBuffForQuestReward(servitor, 283312129);
				castBuffForQuestReward(servitor, 369426433);
				castBuffForQuestReward(servitor, 283377665);
				castBuffForQuestReward(servitor, 283443201);
				castBuffForQuestReward(servitor, 283508737);
				castBuffForQuestReward(servitor, 283639809);
				castBuffForQuestReward(servitor, 283705345);
				castBuffForQuestReward(servitor, 283770881);
				castBuffForQuestReward(servitor, 283836417);
				castBuffForQuestReward(servitor, 369098753);
				showHtm(player, "br_vi_stevu009.htm");
			}
			else
				showHtm(player, "br_vi_stevu010.htm");
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public void showChatWindow(Player player, int val, Object... arg)
	{
		showHtm(player, "br_vi_stevu001.htm");
		player.sendActionFailed();
	}

	private void showHtm(final Player player, final String path)
	{
		final HtmlMessage html = new HtmlMessage(this);
		html.setFile("events/The Gift of Vitality/" + path);
		player.sendPacket(html);
	}

	private void castBuffForQuestReward(final Playable target, final int index)
	{
		final int id = index >> 16;
		final int level = index & 0xFFFF;
		final SkillEntry skill = SkillTable.getInstance().getSkillEntry(id, level);

		final List<Creature> targets = new ArrayList<Creature>();
		targets.add(target);
		skill.useSkill(this, targets);
		broadcastPacketToOthers(new MagicSkillUse(this, target, id, level, 500, 0));
	}

	private boolean canGiveEventData(final Player player)
	{
		final long time = player.getVarLong("br_vi_stevu", 0);
		final long now = System.currentTimeMillis();
		int hours = 24, minutes = 0, seconds = 0;
		if(time > now)
		{
			final long recalc = (time - now) / 1000;
			hours = (int) (recalc / 3600);
			minutes = (int) (recalc - (hours * 3600)) / 60;
			seconds = (int) (recalc - (hours * 3600)) % 60;
		}
		else
		{
			final long expiration = now + TimeUtils.addHours(24); // It can be acquired once per day by speaking to Jack Sage in any town.
			player.setVar("br_vi_stevu", expiration, expiration);
		}

		final NpcStringLine string = NpcStringLineHolder.getInstance().get(player.getLanguage(), 1900138);
		final String msg = String.format(string.getValue(), hours, minutes, seconds);
		player.sendPacket(new NpcSay(this, ChatType.NPC_ALL, msg));
		return time < 1;
	}
}