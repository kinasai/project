package com.l2cccp.gameserver.model.premium;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum PremiumChat
{
	SHOUT,
	HERO;

	private static final PremiumChat[] VALUES = values();

	public static PremiumChat find(String name)
	{
		for(PremiumChat key : VALUES)
		{
			if(key.name().equalsIgnoreCase(name))
				return key;
		}

		return null;
	}
}
