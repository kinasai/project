package com.l2cccp.gameserver.model.promocode;

import org.dom4j.Element;

import com.l2cccp.gameserver.model.Player;

public class SpPromoCodeReward extends PromoCodeReward
{
	public int _value;

	public SpPromoCodeReward(Element element)
	{
		_value = Integer.parseInt(element.attributeValue("val"));
	}

	@Override
	public void giveReward(Player player)
	{
		player.addExpAndSp(0, _value);
	}
}
