package com.l2cccp.gameserver.model.botreport;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum ReportType
{
	IMAGES("command/bot_report/images.htm"),
	COLOR("command/bot_report/color.htm");

	private final String _html;

	private ReportType(final String html)
	{
		_html = html;
	}

	public String getHtml()
	{
		return _html;
	}
}