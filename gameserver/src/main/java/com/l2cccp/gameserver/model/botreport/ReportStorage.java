package com.l2cccp.gameserver.model.botreport;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.utils.TimeUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ReportStorage
{
	private final Map<Integer, Long> _storage = new HashMap<Integer, Long>();
	private final static ReportStorage _instance = new ReportStorage();

	public static ReportStorage getInstance()
	{
		return _instance;
	}

	public boolean report(final int obj)
	{
		if(_storage.containsKey(obj))
		{
			final long time = _storage.get(obj);
			if(time > System.currentTimeMillis())
				return false;
			else
			{
				_storage.remove(obj);
				return true;
			}
		}
		else
			return true;
	}

	public void put(final int obj)
	{
		final long time = TimeUtils.considerMinutes(Config.COMMAND_BOT_REPORT_REUSE);
		_storage.put(obj, time);
	}
}