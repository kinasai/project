package com.l2cccp.gameserver.model.chat.chatfilter.matcher;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.chat.chatfilter.ChatFilterMatcher;
import com.l2cccp.gameserver.network.l2.components.ChatType;

/**
 * Проверка на минимальный уровень игрока.
 *
 * @author G1ta0
 */
public class MatchMinLevel implements ChatFilterMatcher
{
	private final int _level;

	public MatchMinLevel(int level)
	{
		_level = level;
	}

	@Override
	public boolean isMatch(Player player, ChatType type, String msg, Player recipient)
	{
		return player.getLevel() < _level;
	}

}
