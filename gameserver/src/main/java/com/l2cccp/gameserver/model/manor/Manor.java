package com.l2cccp.gameserver.model.manor;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.data.xml.holder.ResidenceHolder;
import com.l2cccp.gameserver.data.xml.holder.SeedsHolder;
import com.l2cccp.gameserver.instancemanager.CastleManorManager;
import com.l2cccp.gameserver.model.entity.residence.Castle;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.templates.manor.CropProcure;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Manor
{
	private static Manor _instance;

	public static Manor getInstance()
	{
		if(_instance == null)
			_instance = new Manor();
		return _instance;
	}

	public List<Integer> getAllCrops()
	{
		List<Integer> crops = new ArrayList<Integer>();
		for(Seed seed : SeedsHolder.getInstance().getSeeds())
		{
			if(!crops.contains(seed.getCrop()) && seed.getCrop() != 0 && !crops.contains(seed.getCrop()))
				crops.add(seed.getCrop());
		}

		return crops;
	}

	public int getSeedBasicPrice(int seedId)
	{
		ItemTemplate seedItem = ItemHolder.getInstance().getTemplate(seedId);
		if(seedItem != null)
			return seedItem.getReferencePrice();
		else
			return 0;
	}

	public int getSeedBasicPriceByCrop(int cropId)
	{
		for(Seed seed : SeedsHolder.getInstance().getSeeds())
		{
			if(seed.getCrop() == cropId)
				return getSeedBasicPrice(seed.getId());
		}

		return 0;
	}

	public int getCropBasicPrice(int cropId)
	{
		ItemTemplate cropItem = ItemHolder.getInstance().getTemplate(cropId);
		if(cropItem != null)
			return cropItem.getReferencePrice();
		else
			return 0;
	}

	public int getMatureCrop(int cropId)
	{
		for(Seed seed : SeedsHolder.getInstance().getSeeds())
		{
			if(seed.getCrop() == cropId)
				return seed.getMature();
		}

		return 0;
	}

	/**
	 * Returns price which lord pays to buy one seed
	 * @param seedId
	 * @return seed price
	 */
	public long getSeedBuyPrice(int seedId)
	{
		long buyPrice = getSeedBasicPrice(seedId) / 10;
		return buyPrice >= 0 ? buyPrice : 1;
	}

	public int getSeedMinLevel(int seedId)
	{
		Seed seed = SeedsHolder.getInstance().getSeed(seedId);
		if(seed != null)
			return seed.getLevel() - 5;
		else
			return -1;
	}

	public int getSeedMaxLevel(int seedId)
	{
		Seed seed = SeedsHolder.getInstance().getSeed(seedId);
		if(seed != null)
			return seed.getLevel() + 5;
		else
			return -1;
	}

	public int getSeedLevelByCrop(int cropId)
	{
		for(Seed seed : SeedsHolder.getInstance().getSeeds())
		{
			if(seed.getCrop() == cropId)
				return seed.getLevel();
		}

		return 0;
	}

	public int getSeedLevel(int seedId)
	{
		Seed seed = SeedsHolder.getInstance().getSeed(seedId);
		if(seed != null)
			return seed.getLevel();
		else
			return -1;
	}

	public boolean isAlternative(int seedId)
	{
		for(Seed seed : SeedsHolder.getInstance().getSeeds())
		{
			if(seed.getId() == seedId)
				return seed.isAlternative();
		}

		return false;
	}

	public int getCropType(int seedId)
	{
		Seed seed = SeedsHolder.getInstance().getSeed(seedId);
		if(seed != null)
			return seed.getCrop();
		else
			return -1;
	}

	public synchronized int getRewardItem(int cropId, int type)
	{
		for(Seed seed : SeedsHolder.getInstance().getSeeds())
		{
			if(seed.getCrop() == cropId)
				return seed.getReward(type);
		}
		// there can be several
		// seeds with same crop, but
		// reward should be the same for
		// all

		return -1;
	}

	public synchronized long getRewardAmountPerCrop(int castle, int cropId, int type)
	{
		final CropProcure cs = ResidenceHolder.getInstance().getResidence(Castle.class, castle).getCropProcure(CastleManorManager.PERIOD_CURRENT).get(cropId);
		for(Seed seed : SeedsHolder.getInstance().getSeeds())
		{
			if(seed.getCrop() == cropId)
				return cs.getPrice() / getCropBasicPrice(seed.getReward(type));
		}

		return -1;
	}

	public synchronized int getRewardItemBySeed(int seedId, int type)
	{
		Seed seed = SeedsHolder.getInstance().getSeed(seedId);
		if(seed != null)
			return seed.getReward(type);
		else
			return 0;
	}

	/**
	 * Return all crops which can be purchased by given castle
	 *
	 * @param castleId
	 * @return
	 */
	public List<Seed> getCropsForCastle(int castleId)
	{
		List<Seed> crops = new ArrayList<Seed>(20);
		for(Seed seed : SeedsHolder.getInstance().getSeeds())
		{
			if(seed.getManorId() == castleId)
				crops.add(seed);
		}

		return crops;
	}

	/**
	 * Return list of seed ids, which belongs to castle with given id
	 * @param castleId - id of the castle
	 * @return seedIds - list of seed ids
	 */
	public List<Seed> getSeedsForCastle(int castleId)
	{
		List<Seed> seeds = new ArrayList<Seed>(20);
		for(Seed seed : SeedsHolder.getInstance().getSeeds())
		{
			if(seed.getManorId() == castleId)
				seeds.add(seed);
		}

		return seeds;
	}

	/**
	 * Returns castle id where seed can be sowned<br>
	 * @param seedId
	 * @return castleId
	 */
	public int getCastleIdForSeed(int seedId)
	{
		Seed seed = SeedsHolder.getInstance().getSeed(seedId);
		if(seed != null)
			return seed.getManorId();
		else
			return 0;
	}

	public long getSeedSaleLimit(int seedId)
	{
		Seed seed = SeedsHolder.getInstance().getSeed(seedId);
		if(seed != null)
			return seed.getSeedLimit();
		else
			return 0;
	}

	public long getCropPuchaseLimit(int cropId)
	{
		for(Seed seed : SeedsHolder.getInstance().getSeeds())
		{
			if(seed.getCrop() == cropId)
				return seed.getCropLimit();
		}

		return 0;
	}
}