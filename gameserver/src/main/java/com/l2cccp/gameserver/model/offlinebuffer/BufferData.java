package com.l2cccp.gameserver.model.offlinebuffer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.skills.SkillEntry;

public class BufferData
{
	private Player _player;
	private String _title;
	private long _price;
	private Map<Integer, SkillEntry> _buffs = new HashMap<Integer, SkillEntry>();

	public BufferData()
	{}

	public BufferData(Player player, String title, long price, List<SkillEntry> buffs)
	{
		_player = player;
		_title = title;
		_price = price;
		if(buffs != null)
		{
			for(SkillEntry buff : buffs)
			{
				_buffs.put(buff.getId(), buff);
			}
		}
	}

	public Player getOwner()
	{
		return _player;
	}

	public String getSaleTitle()
	{
		return _title;
	}

	public long getBuffPrice()
	{
		return _price;
	}

	public Map<Integer, SkillEntry> getBuffs()
	{
		return _buffs;
	}

	public void setTitle(String title)
	{
		_title = title;
	}

	public void setPrice(long price)
	{
		_price = price;
	}

	public void setPlayer(Player player)
	{
		_player = player;
	}
}
