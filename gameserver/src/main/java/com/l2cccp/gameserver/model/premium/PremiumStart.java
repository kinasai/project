package com.l2cccp.gameserver.model.premium;

import com.l2cccp.gameserver.data.xml.holder.PremiumHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.Bonus;
import com.l2cccp.gameserver.model.entity.Hero;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExBR_PremiumState;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.network.l2.s2c.SkillList;
import com.l2cccp.gameserver.network.l2.s2c.SocialAction;
import com.l2cccp.gameserver.utils.TimeUtils;

public class PremiumStart
{
	private static PremiumStart _instance = new PremiumStart();

	public static PremiumStart getInstance()
	{
		return _instance;
	}

	public void start(final Player player, final int id, final long expire)
	{
		GameClient client = player.getNetConnection();
		if(client == null)
			return;

		final Bonus bonus = player.getBonus();
		final PremiumAccount premium = PremiumHolder.getInstance().getPremium(id);
		if(premium != null)
		{
			bonus.addPremium(premium, expire);
			final PremiumTask task = new PremiumTask(player, premium);
			bonus.addTask(premium, task);

			if(premium.getNoble() && !player.isNoble())
			{
				Olympiad.addNoble(player);
				player.setNoble(true);
				player.updatePledgeClass();
				player.updateNobleSkills();
				player.sendPacket(new SkillList(player));
				player.broadcastUserInfo(true);
			}

			if(premium.getHero() != null && !player.isHero() && !player.isFakeHero())
			{
				player.setVar("hasFakeHero", 1, expire);

				PremiumHero settings = premium.getHero();
				if(settings.getChat())
					player.setVar("hasFakeHeroChat", 1, expire);
				if(settings.getEquip())
					player.setVar("hasFakeHeroItems", 1, expire);
				if(settings.getSkills())
				{
					player.setVar("hasFakeHeroSkills", 1, expire);
					Hero.addSkills(player);
				}

				player.sendChanges();
				player.broadcastCharInfo();
				player.broadcastPacket(new SocialAction(player.getObjectId(), SocialAction.GIVE_HERO));
			}

			premium.giveGifts(player);

			String msg = new CustomMessage("premium.expires").addString(TimeUtils.formatTime((int) ((expire - System.currentTimeMillis()) / 1000L), player.getLanguage())).toString(player);
			player.sendPacket(new ExShowScreenMessage(msg, 10000, ScreenMessageAlign.TOP_CENTER, false, 1, -1, false));
			player.sendMessage(msg);

			if(player.getParty() != null)
				player.getParty().recalculatePartyData();
			player.sendPacket(new ExBR_PremiumState(player, true));
		}
	}
}