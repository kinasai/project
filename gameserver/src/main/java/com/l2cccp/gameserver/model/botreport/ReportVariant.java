package com.l2cccp.gameserver.model.botreport;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ReportVariant
{
	private final int _id;
	private final String _value;
	private final boolean _correct;

	public ReportVariant(final int id, final String value, final boolean correct)
	{
		_id = id;
		_value = value;
		_correct = correct;
	}

	public int getId()
	{
		return _id;
	}

	public String getValue()
	{
		return _value;
	}

	public boolean isCorrect()
	{
		return _correct;
	}
}