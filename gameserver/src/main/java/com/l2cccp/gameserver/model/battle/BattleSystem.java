package com.l2cccp.gameserver.model.battle;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.pledge.ClanUtil;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.utils.DeclensionKey;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

public class BattleSystem
{
	public static BattleSystem _instance = new BattleSystem();

	public static BattleSystem getInstance()
	{
		return _instance;
	}

	public void start(Player one, Player two, BattleType battle)
	{
		if(!isValid(one, two, battle))
			return;

		// Выдача опыта и экспы
		if(battle.giveExpSp())
			one.addExpAndSp(battle.getExpSp()[0], battle.getExpSp()[1]);

		// Выдача итема.
		if(battle.giveItem() && Rnd.chance(battle.getItems()[2] * one.getRateItems()))
		{
			int id = battle.getItems()[0];

			if(id == ItemTemplate.ITEM_ID_FAME)
				one.setFame(one.getFame() + battle.getItems()[1], "BattleSystem");
			else if(id == ItemTemplate.ITEM_ID_CLAN_REPUTATION_SCORE)
			{
				if(one.getClan() != null)
					one.getClan().incReputation(battle.getItems()[1], false, "BattleSystem");
			}
			else if(id == ItemTemplate.ITEM_ID_PC_BANG_POINTS)
				one.addPcBangPoints(battle.getItems()[1], false);
			else
				one.getInventory().addItem(id, battle.getItems()[1]);

			one.sendMessage(new CustomMessage("common.pk.pvp.kill.reward").addString(two.getName()).addString(Util.getItemName(battle.getItems()[0])).addString(battle.getItems()[1] + " " + Util.declension(one.getLanguage(), battle.getItems()[1], DeclensionKey.PIECE)).toString(one));

			if(battle.isTimeBlock())
			{
				long MINUTES = TimeUtils.addMinutes(battle.getTime());
				long TIME = System.currentTimeMillis() + MINUTES;
				one.setVar(battle.getVar(), TIME, TIME);
			}
		}

		String tag = battle.name().toLowerCase();

		if(battle.isAnnouncement() && one != null && !one.isGM())
		{
			for(Creature player : one.getAroundCharacters(battle.getRadius(), 500))
			{
				if(player.isPlayer())
					player.sendPacket(new ExShowScreenMessage(new CustomMessage("common." + tag + ".kill").addString(one.getName()).addString(two.getName()).toString(player.getPlayer()), 3000, ScreenMessageAlign.TOP_CENTER, true));
			}

			one.sendPacket(new ExShowScreenMessage(new CustomMessage("common." + tag + ".youkill").addString(two.getName()).toString(one), 3000, ScreenMessageAlign.TOP_CENTER, true));
		}
	}

	private boolean isValid(Player one, Player two, BattleType battle)
	{
		if(one == null || two == null)
			return false;

		GameClient client_one = one.getNetConnection();
		GameClient client_two = two.getNetConnection();

		if(client_one == null || client_two == null)
			return false;
		else if(client_one.getIpAddr().equals(client_two.getIpAddr()))
			return false;
		else if(client_one.getHWID() != null && client_two.getHWID() != null && client_one.getHWID().equals(client_two.getHWID()))
			return false;
		else if(!ClanUtil.isNotOne(one.getClan(), two.getClan()))
			return false;
		else if(!check(one.getLevel(), battle.getKiller()) || !check(two.getLevel(), battle.getTarget()))
			return false;
		else if(battle.needZone() && (!one.isInZone(battle.getZone()) || !two.isInZone(battle.getZone())))
			return false;
		else if(battle.isTimeBlock())
			return !one.getVarB(battle.getVar());
		else
			return true;
	}

	private boolean check(int level, int[] is)
	{
		return level >= is[0] && level <= is[1];
	}
}
