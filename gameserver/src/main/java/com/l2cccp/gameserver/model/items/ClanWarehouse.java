package com.l2cccp.gameserver.model.items;

import com.l2cccp.gameserver.model.items.ItemInstance.ItemLocation;
import com.l2cccp.gameserver.model.pledge.Clan;

public final class ClanWarehouse extends Warehouse
{
	public ClanWarehouse(Clan clan)
	{
		super(clan.getClanId());
	}

	@Override
	public ItemLocation getItemLocation()
	{
		return ItemLocation.CLANWH;
	}
}