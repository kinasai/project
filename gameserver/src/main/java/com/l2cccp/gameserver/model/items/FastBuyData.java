package com.l2cccp.gameserver.model.items;

import java.util.Map;

public class FastBuyData
{
	private final int _id;
	private final String _name;
	private final int _priceId;
	private final long _priceCount;
	private final Map<Integer, Long> _list;

	public FastBuyData(int id, String name, int priceId, long priceCount, Map<Integer, Long> map)
	{
		_id = id;
		_name = name;
		_priceId = priceId;
		_priceCount = priceCount;
		_list = map;
	}

	public int getId()
	{
		return _id;
	}

	public String getName()
	{
		return _name;
	}

	public int getPriceId()
	{
		return _priceId;
	}

	public long getPriceCount()
	{
		return _priceCount;
	}

	public Map<Integer, Long> getList()
	{
		return _list;
	}
}