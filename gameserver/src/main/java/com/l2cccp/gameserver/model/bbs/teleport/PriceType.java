package com.l2cccp.gameserver.model.bbs.teleport;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum PriceType
{
	SIMPLE,
	NOBLESSE,
	PREMIUM
}