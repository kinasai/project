package com.l2cccp.gameserver.model.items;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Restoration
{
	private final int _id;
	private final long _count;
	private double _chance;

	public Restoration(int id, long count)
	{
		_id = id;
		_count = count;
	}

	public void setChance(double chance)
	{
		_chance = chance;
	}

	public int getId()
	{
		return _id;
	}

	public long getCount()
	{
		return _count;
	}

	public double getChance()
	{
		return _chance;
	}
}