package com.l2cccp.gameserver.model.balancing.player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum BalancerType
{
	magic_attack,
	physical_attack,
	magic_attack_speed,
	physical_attack_speed,
	magic_critical_attack,
	physical_critical_attack,
	magic_defence,
	physical_defence,
	max_cp,
	max_hp,
	max_mp,
	speed,
	accuracy,
	evasion
}