package com.l2cccp.gameserver.model.dress;

import com.l2cccp.gameserver.model.Player;

public class DressData
{
	public static boolean check(Player player)
	{
		return (player.isForceVisualFlag() && !player.isInOlympiadMode());
	}
}
