package com.l2cccp.gameserver.model.botreport;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ReportTemplate
{
	private final int _id;
	private final ReportType _type;
	private final double _chance;
	private final Map<Language, String> _msg;
	private final List<ReportVariant> _variants;

	public ReportTemplate(final int id, final ReportType type, final double chance)
	{
		_id = id;
		_type = type;
		_chance = chance;
		_msg = new HashMap<Language, String>();
		_variants = new ArrayList<ReportVariant>();
	}

	public int getId()
	{
		return _id;
	}

	public ReportType getType()
	{
		return _type;
	}

	public double getChance()
	{
		return _chance;
	}

	public void addMsg(final Language lang, final String msg)
	{
		_msg.put(lang, msg);
	}

	public String getMsg(final Language lang)
	{
		return _msg.get(lang);
	}

	public void addVariant(final ReportVariant var)
	{
		_variants.add(var);
	}

	public List<ReportVariant> getVariants()
	{
		return _variants;
	}
}