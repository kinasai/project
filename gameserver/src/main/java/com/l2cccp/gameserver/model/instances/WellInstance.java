package com.l2cccp.gameserver.model.instances;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.PcInventory;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.TimeUtils;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class WellInstance extends NpcInstance
{
	private TIntObjectHashMap<Long> data = new TIntObjectHashMap<Long>();

	public WellInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equals("draw"))
		{
			if(check(player))
			{
				player.getInventory().addItem(23013, 1);
				player.sendMessage("Вы набрали ведро воды!");
				data.put(player.getObjectId(), (System.currentTimeMillis() + TimeUtils.addSecond(Rnd.get(7, 10))));
			}
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public void showChatWindow(Player player, int val, Object... arg)
	{
		HtmlMessage html = new HtmlMessage(this);
		html.setFile("farm/well.htm");
		player.sendPacket(html);
		player.sendActionFailed();
	}

	private boolean check(final Player player)
	{
		final PcInventory inv = player.getInventory();
		if(inv.getCountOf(23006) < 1)
		{
			player.sendMessage("Для начала, возьмите пустое ведро у менеджера фермы!");
			return false;
		}
		else if(inv.getCountOf(23013) >= 30)
		{
			player.sendMessage("Вы набрали максимальное количество воды!");
			return false;
		}

		final int obj = player.getObjectId();
		if(!data.contains(obj))
			return true;

		final long time = data.get(obj);
		final int calc = (int) (time - System.currentTimeMillis());
		if(calc > 0)
		{
			player.sendMessage("Не так быстро, подождите " + (calc / 1000) + " сек.");
			return false;
		}
		else
			return true;
	}
}