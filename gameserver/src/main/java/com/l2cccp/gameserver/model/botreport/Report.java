package com.l2cccp.gameserver.model.botreport;

import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.skills.AbnormalEffect;
import com.l2cccp.gameserver.skills.EffectType;
import com.l2cccp.gameserver.utils.DeclensionKey;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Report extends RunnableImpl
{
	private final Player player;
	private final ReportTemplate report;
	private int time;
	private boolean punish = true;

	public Report(Player player, ReportTemplate report)
	{
		this.player = player;
		this.report = report;
		this.time = Config.COMMAND_BOT_REPORT_ANSWER_TIME;
		ThreadPoolManager.getInstance().schedule(this, 1000L);
		if(player.isImmobilized())
		{
			Effect effect = player.getEffectList().getEffectByType(EffectType.Immobilize);
			if(effect != null)
				effect.exit();
			player.stopImmobilized();
		}

		player.startImmobilized();
		player.startAbnormalEffect(AbnormalEffect.INVULNERABLE);
	}

	@Override
	public void runImpl()
	{
		if(player == null)
			return;

		if(time > 0)
		{
			if((time % 10 == 0) || time < 10)
				player.sendMessage(new CustomMessage("report.bot.jail.msg").addNumber(time).addString(Util.declension(player.getLanguage(), time, DeclensionKey.SECONDS)));
			time--;
			ThreadPoolManager.getInstance().schedule(this, 1000L);
		}
		else if(punish)
			ReportPunishment.getInstance().jail(player);
	}

	public ReportTemplate getReport()
	{
		return report;
	}

	public int getTime()
	{
		return time;
	}

	public void stop()
	{
		punish = false;
		time = 0;
		player.stopImmobilized();
		player.stopAbnormalEffect(AbnormalEffect.INVULNERABLE);
	}
}