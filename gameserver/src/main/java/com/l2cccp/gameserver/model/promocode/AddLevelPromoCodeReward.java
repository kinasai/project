package com.l2cccp.gameserver.model.promocode;

import org.dom4j.Element;

import com.l2cccp.gameserver.data.xml.holder.ExperienceHolder;
import com.l2cccp.gameserver.model.Player;

public class AddLevelPromoCodeReward extends PromoCodeReward
{
	public int _level;

	public AddLevelPromoCodeReward(Element element)
	{
		_level = Integer.parseInt(element.attributeValue("val"));
	}

	@Override
	public void giveReward(Player player)
	{
		long expAdd = ExperienceHolder.getInstance().getLevels()[Math.max(ExperienceHolder.getInstance().getMaxLevel(), _level + player.getLevel())] - player.getExp();

		if(expAdd > 0)
			player.addExpAndSp(expAdd, 0);
	}
}
