package com.l2cccp.gameserver.model.entity.olympiad;

import com.l2cccp.gameserver.ai.CtrlIntention;
import com.l2cccp.gameserver.listener.actor.OnCurrentHpDamageListener;
import com.l2cccp.gameserver.listener.actor.OnDeathFromUndyingListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.skills.SkillEntry;

/**
 * @author VISTALL
 * @date 21:08/13.03.2012
 */
public class OlympiadPlayerListeners implements OnCurrentHpDamageListener, OnDeathFromUndyingListener
{
	@Override
	public void onCurrentHpDamage(Creature actor, double damage, Creature attacker, SkillEntry skill, boolean crit)
	{
		if (!actor.isPlayer())
			return;

		// считаем дамаг от простых ударов и атакующих скиллов
		if (actor == attacker || (skill != null && !skill.getTemplate().isOffensive()))
			return;

		final Player player = (Player)actor;
		if (!player.isInOlympiadMode())
			return;

		final OlympiadGame game = player.getOlympiadGame();
		if (game == null)
			return;

		game.addDamage(player, Math.min(actor.getCurrentHp(), damage));
	}

	@Override
	public void onDeathFromUndying(Creature actor, Creature killer)
	{
		if (!actor.isPlayer())
			return;

		final Player player = (Player)actor;
		if (!player.isInOlympiadMode())
			return;

		final OlympiadGame game = player.getOlympiadGame();
		if (game == null)
			return;

		if(game.getType() != CompType.TEAM || game.doDie(player)) // Все умерли
		{
			game.setWinner(player.getOlympiadSide() == 1 ? 2 : 1);
			game.endGame(20, false);
		}
		else
		{
			player.startFrozen();
			player.stopAttackStanceTask();
			player.sendChanges();
			Servitor servitor = player.getServitor();
			if (servitor != null)
			{
				servitor.startFrozen();
				servitor.stopAttackStanceTask();
				servitor.sendChanges();
			}
		}

		killer.getAI().setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
		killer.sendActionFailed();
	}
}
