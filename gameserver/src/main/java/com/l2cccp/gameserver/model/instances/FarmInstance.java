package com.l2cccp.gameserver.model.instances;

import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.farm.FarmFacility;
import com.l2cccp.gameserver.model.farm.FarmRemoveMe;
import com.l2cccp.gameserver.model.farm.FarmType;
import com.l2cccp.gameserver.model.items.Restoration;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FarmInstance extends NpcInstance
{
	private int owner;
	private AtomicInteger fertilize;
	private FarmFacility farm;
	private Future<?> task;

	public FarmInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;
		else if(player.getObjectId() != owner)
		{
			player.sendMessage("Это не ваше владение, действие запрещено!");
			return;
		}

		if(command.equals("fertilize"))
		{
			if(Util.getPay(player, 23013, 1, true))
			{
				if(fertilize.decrementAndGet() <= 0)
				{
					final Restoration reward = farm.getReward();
					player.getInventory().addItem(reward.getId(), reward.getCount());
					player.sendMessage("Вы успешно собрали урожай!");
					deleteMe();

					if(task != null)
					{
						task.cancel(false);
						task = null;
					}
				}
				else
					showChatWindow(player, 0, null, true);
			}
			else
				showChatWindow(player, 0, null, true);
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public void showChatWindow(Player player, int val, Object... arg)
	{
		HtmlMessage html = new HtmlMessage(this);
		html.setFile("farm/index.htm");
		html.replace("%item%", farm.getName());
		html.replace("%fertilize%", String.valueOf(fertilize.get()));
		html.replace("%icon%", farm.getIcon());

		player.sendPacket(html);
		player.sendActionFailed();
	}

	public void setOwner(final int owner, final FarmFacility farm)
	{
		this.owner = owner;
		this.farm = farm;
		this.fertilize = farm.getFertilize();
		this.task = ThreadPoolManager.getInstance().schedule(new FarmRemoveMe(this), TimeUtils.addMinutes(farm.getLifeTime()));
	}

	public void announce()
	{
		final Player player = GameObjectsStorage.getPlayer(owner);
		if(player != null)
			player.sendMessage(farm.getName() + (farm.getType() == FarmType.BEAST ? " погибла от обезвоживания!" : " завяла из за засухи!"));
	}
}