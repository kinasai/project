package com.l2cccp.gameserver.model.entity.events.maps;

import java.util.Map;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.gameserver.templates.ZoneTemplate;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class EventMaps
{
	private final String _name;
	private final int[] _events;
	private final int _teams;
	private final int _min;
	private final int _max;
	private final int _instance;
	private final Map<Integer, Location[]> _teamSpawns;
	private final ZoneTemplate _zone;
	private final Location[] _keyLocations;

	public EventMaps(MultiValueSet<String> params, Map<Integer, Location[]> teamSpawns, ZoneTemplate territory, Location[] keyLocations)
	{
		_name = params.getString("name");
		_events = params.getIntegerArray("events", null);
		_min = params.getInteger("min", 1);
		_max = params.getInteger("max", 1000);
		_instance = params.getInteger("instances", -1);

		_teams = params.getInteger("teams", -1);
		_teamSpawns = teamSpawns;
		_zone = territory;
		_keyLocations = keyLocations;
	}

	public String getName()
	{
		return _name;
	}

	public int[] getEvents()
	{
		return _events;
	}

	public int getTeamCount()
	{
		return _teams;
	}

	public int getMin()
	{
		return _min;
	}

	public int getMax()
	{
		return _max;
	}

	public Map<Integer, Location[]> getTeamSpawns()
	{
		return _teamSpawns;
	}

	public Location[] getPlayerSpawns()
	{
		return _teamSpawns.get(-1);
	}

	public ZoneTemplate getZoneTemplate()
	{
		return _zone;
	}

	public Location[] getKeyLocations()
	{
		return _keyLocations;
	}

	public int getInstance()
	{
		return _instance;
	}
}