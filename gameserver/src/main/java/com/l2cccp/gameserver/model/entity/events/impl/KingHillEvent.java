package com.l2cccp.gameserver.model.entity.events.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.l2cccp.commons.collections.JoinedIterator;
import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.listener.PlayerListener;
import com.l2cccp.gameserver.listener.actor.OnDeathListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerExitListener;
import com.l2cccp.gameserver.listener.actor.player.OnTeleportListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.entity.events.objects.DuelSnapshotObject;
import com.l2cccp.gameserver.model.entity.events.util.EventState;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.NpcString;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class KingHillEvent extends TeamEvent
{
	public static final String REGISTRATION = "registration";
	public static final String FIRST_PHASE = "first_phase";

	public static final String TW_MANAGER = "tw_manager";

	private static final HtmlMessage cancelHtml = new HtmlMessage(0).setFile("events/territory_wars/tw_cancel.htm");
	private static final HtmlMessage unregSuccessHtml = new HtmlMessage(0).setFile("events/territory_wars/tw_unreg_succ.htm");

	private final Location[] teleportLocations;
	private final PlayerListener playerListener = new PlayerListenerImpl();
	public int owner;

	private final Map<Integer, List<Player>> registeredPlayers = new HashMap<Integer, List<Player>>();
	private final Map<Integer, List<DuelSnapshotObject>> players = new HashMap<Integer, List<DuelSnapshotObject>>();
	public final String[] teams;
	private final int[] color;
	public final String[] colors;

	public KingHillEvent(final MultiValueSet<String> set)
	{
		super(set);
		setState(EventState.NONE);

		teams = set.getString("teams").split(",");
		color = new int[teams.length];
		colors = set.getString("colors").split(",");

		teleportLocations = new Location[teams.length];
		for(int i = 1; i <= teams.length; i++)
		{
			final int ar = i - 1;
			color[ar] = Util.parseColor(colors[ar]);
			registeredPlayers.put(i, new ArrayList<Player>());
			players.put(i, new ArrayList<DuelSnapshotObject>());
		}
	}

	@Override
	public void stopEvent()
	{
		clearActions();
		updatePlayers(false, false);

		if(state != EventState.NO_PLAYERS)
		{
			if(owner == 0)
			{
				sendPacket(SystemMsg.THE_DUEL_HAS_ENDED_IN_A_TIE);
				sendPacket(new ExShowScreenMessage(NpcString.NONE, 10000, ScreenMessageAlign.TOP_CENTER, false, 1, -1, false, "Ничья"));
			}
			else
			{
				final String message = "Выиграла команда " + teams[owner - 1] + ".";
				final ExShowScreenMessage sysMessageEnd = new ExShowScreenMessage(NpcString.NONE, 10000, ExShowScreenMessage.ScreenMessageAlign.TOP_CENTER, false, 1, -1, false, message);
				sendPackets(sysMessageEnd);

				if(rewardItems != null)
				{
					for(DuelSnapshotObject d : getPlayers(owner))
					{
						for(int i = 0; i < rewardItems.length; i++)
						{
							if(d.getPlayer() != null)
								ItemFunctions.addItem(d.getPlayer(), rewardItems[i], levelMul ? rewardCounts[i] * d.getPlayer().getLevel() : rewardCounts[i]);
						}
					}
				}
			}
		}

		setState(EventState.NONE);
		updatePlayers(false, true);

		for(int i = 1; i <= teams.length; i++)
			players.get(i).clear();

		reCalcNextTime(false);

		if(zone != null)
		{
			zone.setActive(false);
			zone = null;
		}

		super.stopEvent();
	}

	@Override
	protected boolean checkTeamSize()
	{
		for(int i = 1; i <= teams.length; i++)
		{
			if(players.get(i).size() < map.getMin())
				return false;
		}

		return true;
	}

	@Override
	public void teleportPlayers(final String name)
	{
		setState(EventState.TELEPORT_PLAYERS);

		for(final Entry<Integer, List<Player>> entry : registeredPlayers.entrySet())
		{
			final Integer team = entry.getKey();
			for(final Player player : entry.getValue())
			{
				players.get(team).add(new DuelSnapshotObject(player, null, true, team));
			}
		}

		for(int i = 1; i <= teams.length; i++)
			registeredPlayers.get(i).clear();

		owner = 0;
		teamCheck(true);

		if(!checkTeamSize())
		{
			setState(EventState.NO_PLAYERS);
			announceToValidPlayer("event.сancelled");
			stopEvent();
			return;
		}

		zone.setActive(true);
		initTeleport();

		final boolean dispel = getReflection().getInstancedZone().isDispelBuffs();
		for(final DuelSnapshotObject object : this)
		{
			final Player player = object.getPlayer();

			player.setTeamId(object.getTeamId());
			player.setVisibleTitle(teams[object.getTeamId() - 1]);
			player.setVisibleTitleColor(color[object.getTeamId() - 1]);
			player.abortAttack(true, true);
			player.abortCast(true, true);
			player.stopMove();

			if(dispel)
				dispelBuffs(player);

			player.broadcastCharInfo();
			player._stablePoint = object.getLoc();
			player.teleToLocation(getTeleportLoc(object.getTeamId()), getReflection());
		}
	}

	private void initTeleport()
	{
		int count = 0;
		for(int i = 1; i <= teams.length; i++)
			count += players.get(i).size();

		for(int i = 1; i <= teams.length; i++)
			teleportLocations[i - 1] = map.getTeamSpawns().get(i)[count <= 50 ? 1 : 0];
	}

	@Override
	protected void teamCheck(boolean second)
	{
		for(int i = 1; i <= teams.length; i++)
		{
			final List<DuelSnapshotObject> list = players.get(i);
			for(final DuelSnapshotObject object : list)
			{
				final Player player = object.getPlayer();
				if(!checkPlayer(player, second))
				{
					player.sendPacket(cancelHtml);
					list.remove(object);
				}
			}
		}
	}

	@Override
	public boolean registerPlayer(final Player player)
	{
		if(isRegistrationOver())
		{
			player.sendMessage(new CustomMessage("event.registration.off").addString(getName()));
			return false;
		}

		for(final Integer team : registeredPlayers.keySet())
		{
			for(Player pl : registeredPlayers.get(team))
			{
				if(pl == player)
				{
					player.sendMessage(new CustomMessage("event.registration.allready").addString(getName()));
					return false;
				}
			}
		}

		if(!checkPlayer(player, false))
		{
			player.sendMessage(new CustomMessage("event.registration.failed").addString(getName()));
			return false;
		}

		int set = 1;
		int last = Integer.MAX_VALUE;
		for(final Integer team : registeredPlayers.keySet())
		{
			final int size = registeredPlayers.get(team).size();
			if(size < last)
			{
				last = size;
				set = team;
			}
		}

		registeredPlayers.get(set).add(player);

		player.addEvent(this);
		player.sendMessage(new CustomMessage("event.registration").addString(getName()));
		return true;
	}

	public boolean unregisterPlayer(final Player player)
	{
		if(isRegistrationOver())
		{
			player.sendMessage(new CustomMessage("event.registration.off").addString(getName()));
			return false;
		}

		if(registeredPlayers.get(player.getTeamId()).contains(player))
		{
			registeredPlayers.get(player.getTeamId()).remove(player);
			player.sendPacket(unregSuccessHtml);
			return true;
		}

		return false;
	}

	@Override
	public void announce(final int a)
	{}

	@Override
	public void onAddEvent(final GameObject o)
	{
		if(o.isPlayer())
			o.getPlayer().addListener(playerListener);
	}

	@Override
	public void onRemoveEvent(final GameObject o)
	{
		if(o.isPlayer())
			o.getPlayer().removeListener(playerListener);
	}

	@Override
	public void action(final String name, final boolean start)
	{
		if(name.equalsIgnoreCase(REGISTRATION))
			registration();
		else
			super.action(name, start);
	}

	public void registration()
	{
		setState(EventState.REGISTRATION);
		//		makeMap();
		announceToValidPlayer("event.registration.open");
	}

	@Override
	public void startEvent()
	{
		teamCheck(true);

		if(!checkTeamSize())
		{
			setState(EventState.NO_PLAYERS);
			announceToValidPlayer("event.сancelled");
			stopEvent();
			return;
		}

		setState(EventState.FIRST_PHASE);
		updatePlayers(true, false);
	}

	@Override
	public Iterator<DuelSnapshotObject> iterator() // Дикий изжоп переделать
	{
		JoinedIterator<DuelSnapshotObject> iterators = null;
		if(players.size() == 5)
		{
			final List<DuelSnapshotObject> Alpha = players.get(1);
			final List<DuelSnapshotObject> Beta = players.get(2);
			final List<DuelSnapshotObject> Delta = players.get(3);
			final List<DuelSnapshotObject> Gamma = players.get(4);
			final List<DuelSnapshotObject> Omega = players.get(5);
			iterators = new JoinedIterator<DuelSnapshotObject>(Alpha.iterator(), Beta.iterator(), Delta.iterator(), Gamma.iterator(), Omega.iterator());
		}
		else if(players.size() == 4)
		{
			final List<DuelSnapshotObject> Alpha = players.get(1);
			final List<DuelSnapshotObject> Beta = players.get(2);
			final List<DuelSnapshotObject> Delta = players.get(3);
			final List<DuelSnapshotObject> Gamma = players.get(4);
			iterators = new JoinedIterator<DuelSnapshotObject>(Alpha.iterator(), Beta.iterator(), Delta.iterator(), Gamma.iterator());
		}
		else if(players.size() == 3)
		{
			final List<DuelSnapshotObject> Alpha = players.get(1);
			final List<DuelSnapshotObject> Beta = players.get(2);
			final List<DuelSnapshotObject> Delta = players.get(3);
			iterators = new JoinedIterator<DuelSnapshotObject>(Alpha.iterator(), Beta.iterator(), Delta.iterator());
		}
		else if(players.size() == 2)
		{
			final List<DuelSnapshotObject> Alpha = players.get(1);
			final List<DuelSnapshotObject> Beta = players.get(2);
			iterators = new JoinedIterator<DuelSnapshotObject>(Alpha.iterator(), Beta.iterator());
		}

		return iterators;
	}

	private class PlayerListenerImpl implements OnPlayerExitListener, OnTeleportListener, OnDeathListener
	{
		@Override
		public void onPlayerExit(final Player player)
		{
			player.setReflection(ReflectionManager.DEFAULT);
			exitPlayer(player, true);
		}

		@Override
		public void onTeleport(final Player player, final int x, final int y, final int z, final Reflection r)
		{
			if(state != EventState.REGISTRATION && r != getReflection())
				exitPlayer(player, false);
		}

		private void exitPlayer(final Player player, final boolean exit)
		{
			for(int i = 1; i <= teams.length; i++)
			{
				final List<DuelSnapshotObject> objects = players.get(i);

				for(final DuelSnapshotObject d : objects)
				{
					if(d.getPlayer() == player)
					{
						objects.remove(d);
						quit(d);
						break;
					}
				}
			}

			player.removeListener(playerListener);
		}

		@Override
		public void onDeath(Creature actor, Creature killer)
		{
			final Player player = actor.getPlayer();
			final Player _killer = killer.getPlayer();
			if(player == null || _killer == null)
				return;

			announceKill(_killer, player, getPlayers(_killer.getTeamId()));
			_scores.remove(player.getName());
			player.doRevive(100);
			player.setCurrentMp(player.getMaxMp());
			player.setCurrentCp(player.getMaxCp());
			player.setCurrentHp(player.getMaxHp(), true);
			player.teleToLocation(getTeleportLoc(player.getTeamId()));

			if(battlepvp && zone.getType() == Zone.ZoneType.battle_zone)
				_killer.setPvpKills(_killer.getPvpKills() + 1);
		}
	}

	public List<DuelSnapshotObject> getPlayers(int team)
	{
		return players.get(team);
	}

	private Location getTeleportLoc(int team)
	{
		return teleportLocations[team - 1];
	}

	protected void updatePlayers(boolean start, boolean teleport)
	{
		for(DuelSnapshotObject snapshot : this)
		{
			if(snapshot.getPlayer() == null)
				continue;

			if(state == EventState.NO_PLAYERS)
			{
				snapshot.getPlayer().removeEvent(this);
				getObjects(snapshot.getTeamId()).remove(snapshot);
				continue;
			}

			if(teleport && state == EventState.FINISH)
				snapshot.teleportBack(500L);
			else
			{
				Player player = snapshot.getPlayer();
				if(start)
				{
					player.setTeamId(snapshot.getTeamId());
					player.setVisibleTitle(teams[snapshot.getTeamId() - 1]);

					if(!canWalkInWaitTime())
						unparalyze(player);

					if(block_party)
					{
						player.blockParty(true);

						if(player.isInParty())
							player.leaveParty();
					}

					player.setCurrentMp(player.getMaxMp());
					player.setCurrentCp(player.getMaxCp());
					player.setCurrentHp(player.getMaxHp(), true);
				}
				else
					quit(snapshot);

				actionUpdate(start, player);
			}
		}
	}

	public void changeOwnerTeam(int team)
	{
		owner = team;
	}

	@Override
	public SystemMsg checkForAttack(Creature target, Creature attacker, Skill skill, boolean force)
	{
		if(!canAttack(target, attacker, skill, force, false))
			return SystemMsg.INVALID_TARGET;

		return null;
	}

	@Override
	public boolean canAttack(Creature target, Creature attacker, Skill skill, boolean force, boolean nextAttackCheck)
	{
		if(skill != null && blockSkills != null)
		{
			for(final int id : blockSkills)
			{
				if(id == skill.getId())
					return false;
			}
		}

		if(!isInProgress() || target.getTeamId() == 0 || attacker.getTeamId() == 0 || target.getTeamId() == attacker.getTeamId())
			return false;

		return true;
	}
}