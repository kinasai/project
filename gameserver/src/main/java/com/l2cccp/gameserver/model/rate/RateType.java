package com.l2cccp.gameserver.model.rate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum RateType
{
	EXP,
	SP,
	ADENA,
	ITEMS,
	SPOIL,
	COMMON,
	RAID,
	EPIC,
	SIEGE,
	FAME
}