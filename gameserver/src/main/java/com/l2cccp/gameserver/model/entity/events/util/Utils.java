package com.l2cccp.gameserver.model.entity.events.util;

import com.l2cccp.gameserver.data.xml.holder.EventHolder;
import com.l2cccp.gameserver.model.entity.events.Event;
import com.l2cccp.gameserver.model.entity.events.impl.TeamEvent;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Utils
{
	public static CustomMessage killMsg(String player, String target, int count)
	{
		CustomMessage msg = new CustomMessage("common.event.kill.null");

		switch(count)
		{
			case 0:
				break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
				msg = new CustomMessage("common.event.kill." + count + "").addString(player);
				break;
			default:
				msg = new CustomMessage("common.event.kill.default").addString(player).addString(target);
				break;
		}

		return msg;
	}

	public static NextEvent getNextEvent()
	{
		long time = Long.MAX_VALUE;
		NextEvent _next = null;

		for(Event event : EventHolder.getInstance().getEvents().values())
		{
			if(event instanceof TeamEvent)
			{
				final TeamEvent team = (TeamEvent) event;
				final long next = team.startTimeMillis();
				if(time > next)
				{
					time = next;
					if(_next != null)
						_next = null;
					_next = new NextEvent(next, team.getName());
				}
			}
		}

		return _next;
	}
}