package com.l2cccp.gameserver.model.entity.events.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.atomic.AtomicInteger;

import com.l2cccp.commons.collections.JoinedIterator;
import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.commons.geometry.Circle;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.listener.PlayerListener;
import com.l2cccp.gameserver.listener.actor.OnDeathListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerExitListener;
import com.l2cccp.gameserver.listener.actor.player.OnTeleportListener;
import com.l2cccp.gameserver.listener.zone.OnZoneEnterLeaveListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Territory;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.base.RestartType;
import com.l2cccp.gameserver.model.base.SpecialEffectState;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.entity.events.objects.DuelSnapshotObject;
import com.l2cccp.gameserver.model.entity.events.objects.TWControlSpotObject;
import com.l2cccp.gameserver.model.entity.events.util.EventState;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.AbnormalEffect;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.templates.ZoneTemplate;
import com.l2cccp.gameserver.utils.DeclensionKey;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP & Java-Man
 * @site http://l2cccp.com/
 */
public class TerritoryWarsEvent extends TeamEvent
{
	public static final String REGISTRATION = "registration";
	public static final String FIRST_PHASE = "first_phase";
	public static final String SECOND_PHASE = "second_phase";

	public static final String TW_MANAGER = "tw_manager";
	public static final String TW_TELEPORTER = "tw_teleporter";
	public static final String CONTROL_SPOTS = "control_spots";
	public static final String TW_RAIDBOSS = "tw_raidboss";

	private static final HtmlMessage cancelHtml = new HtmlMessage(0).setFile("events/territory_wars/tw_cancel.htm");
	private static final HtmlMessage unregSuccessHtml = new HtmlMessage(0).setFile("events/territory_wars/tw_unreg_succ.htm");

	private Location[] teleportLocations = new Location[2];
	private final PlayerListener playerListener = new PlayerListenerImpl();

	private final List<Player> registered = new ArrayList<Player>();
	private final Map<TeamType, List<DuelSnapshotObject>> players = new EnumMap<TeamType, List<DuelSnapshotObject>>(TeamType.class);
	private AtomicInteger[] commandPoints;
	private final List<TWControlSpotObject> controlSpots;
	private final int[] points;
	private ScheduledFuture<?> pointManagerTask;
	private Zone peace = null;

	public TerritoryWarsEvent(final MultiValueSet<String> set)
	{
		super(set);
		setState(EventState.NONE);
		points = set.getIntegerArray("points");

		controlSpots = new ArrayList<TWControlSpotObject>();
		for(TeamType team : TeamType.VALUES)
		{
			if(team == TeamType.NONE)
				continue;

			players.put(team, new ArrayList<DuelSnapshotObject>());
		}
	}

	@Override
	public void stopEvent()
	{
		clearActions();
		updatePlayers(false, false);
		setState(EventState.NONE);

		updatePlayers(false, true);
		spawnAction(TW_TELEPORTER, false);
		spawnAction(TW_RAIDBOSS, false);

		players.get(TeamType.BLUE).clear();
		players.get(TeamType.RED).clear();

		reCalcNextTime(false);

		if(zone != null)
		{
			zone.setActive(false);
			zone = null;
		}

		if(peace != null)
		{
			peace.setActive(false);
			peace = null;
		}

		super.stopEvent();
	}

	@Override
	protected boolean checkTeamSize()
	{
		List<DuelSnapshotObject> red = players.get(TeamType.RED);
		List<DuelSnapshotObject> blue = players.get(TeamType.BLUE);
		if(red == null || blue == null)
			return false;
		else if((blue.size() < map.getMin() || red.size() < map.getMin()))
			return false;
		else
			return true;
	}

	@Override
	public void teleportPlayers(final String name)
	{
		setState(EventState.TELEPORT_PLAYERS);

		final List<Player> list = new ArrayList<Player>(registered);
		Collections.shuffle(list);
		int pos = 1;
		for(final Player player : list)
		{
			final TeamType team = pos % 2 == 0 ? TeamType.BLUE : TeamType.RED;
			players.get(team).add(new DuelSnapshotObject(player, team, true));
			player.setTeam(team);
		}

		registered.clear();

		winner = TeamType.NONE;
		teamCheck(true);

		if(!checkTeamSize())
		{
			winner = null;
			setState(EventState.NO_PLAYERS);
			announceToValidPlayer("event.сancelled");
			stopEvent();
			return;
		}

		int point = 0;
		final int size = players.size();

		if(size >= 50)
			point = points[3];
		else if(size >= 30)
			point = points[2];
		else if(size >= 20)
			point = points[1];
		else
			point = points[0];

		commandPoints = new AtomicInteger[] { new AtomicInteger(point), new AtomicInteger(point) };
		zone.setActive(true);
		for(TeamType team : TeamType.VALUES)
		{
			if(team == TeamType.NONE)
				continue;

			Circle c = new Circle(getTeleportLoc(team), 300);
			c.setZmax(World.MAP_MAX_Z);
			c.setZmin(World.MAP_MIN_Z);

			StatsSet set = new StatsSet();
			set.set("name", "TW Peace Start");
			set.set("type", Zone.ZoneType.peace_zone); // Ссанина не активируеться по верх боевой зоны
			set.set("territory", new Territory().add(c));

			peace = new Zone(new ZoneTemplate(set));
			peace.setReflection(getReflection());
			peace.addListener(new ZoneListener());
			peace.setActive(true);
		}

		final boolean dispel = getReflection().getInstancedZone().isDispelBuffs();
		for(final Entry<TeamType, List<DuelSnapshotObject>> objects : players.entrySet())
		{
			final TeamType team = objects.getKey();
			for(final DuelSnapshotObject object : objects.getValue())
			{
				final Player player = object.getPlayer();

				player.abortAttack(true, true);
				player.abortCast(true, true);
				player.stopMove();

				if(dispel)
					dispelBuffs(player);

				player._stablePoint = object.getLoc();
				player.teleToLocation(getTeleportLoc(team), getReflection());
			}
		}
	}

	@Override
	protected void teamCheck(boolean second)
	{
		for(final TeamType team : TeamType.VALUES)
		{
			if(team == TeamType.NONE)
				continue;

			final List<DuelSnapshotObject> list = players.get(team);
			for(final DuelSnapshotObject object : new ArrayList<DuelSnapshotObject>(list))
			{
				final Player player = object.getPlayer();
				if(!checkPlayer(player, second))
				{
					player.sendPacket(cancelHtml);
					list.remove(object);
				}
			}
		}
	}

	@Override
	public boolean registerPlayer(final Player player)
	{
		if(isRegistrationOver())
		{
			player.sendMessage(new CustomMessage("event.registration.off").addString(getName()));
			return false;
		}

		for(Player pl : registered)
		{
			if(pl == player)
			{
				player.sendMessage(new CustomMessage("event.registration.allready").addString(getName()));
				return false;
			}
		}

		if(!checkPlayer(player, false))
		{
			player.sendMessage(new CustomMessage("event.registration.failed").addString(getName()));
			return false;
		}

		registered.add(player);

		player.addEvent(this);
		player.sendMessage(new CustomMessage("event.registration").addString(getName()));
		return true;
	}

	public boolean unregisterPlayer(final Player player)
	{
		if(isRegistrationOver())
		{
			player.sendMessage(new CustomMessage("event.registration.off").addString(getName()));
			return false;
		}

		if(registered.contains(player))
			registered.remove(player);

		player.sendPacket(unregSuccessHtml);

		return true;
	}

	@Override
	public void announce(final int a)
	{}

	@Override
	public void onAddEvent(final GameObject o)
	{
		if(o.isPlayer())
			o.getPlayer().addListener(playerListener);
	}

	@Override
	public void onRemoveEvent(final GameObject o)
	{
		if(o.isPlayer())
			o.getPlayer().removeListener(playerListener);
	}

	@Override
	public void action(final String name, final boolean start)
	{
		if(name.equalsIgnoreCase(REGISTRATION))
			registration();
		else
			super.action(name, start);
	}

	public void registration()
	{
		setState(EventState.REGISTRATION);
		announceToValidPlayer("event.registration.open");
	}

	@Override
	public void startEvent()
	{
		controlSpots.clear();
		teamCheck(true);

		if(!checkTeamSize())
		{
			setState(EventState.NO_PLAYERS);
			announceToValidPlayer("event.сancelled");
			stopEvent();
			return;
		}

		pointManagerTask = ThreadPoolManager.getInstance().scheduleAtFixedRate(new ControlSpotPointManager(), 3000L, 3000L);

		setState(EventState.FIRST_PHASE);

		sendPointMsg();
		updatePlayers(true, false);
	}

	private class ZoneListener implements OnZoneEnterLeaveListener
	{
		@Override
		public void onZoneEnter(Zone zone, Creature actor)
		{
			if(!actor.isPlayer() || actor.getTeam() == TeamType.NONE)
				return;

			Player player = actor.getPlayer();
			player.setInvul(SpecialEffectState.TRUE);
			player.startAbnormalEffect(AbnormalEffect.INVULNERABLE);
		}

		@Override
		public void onZoneLeave(Zone zone, Creature actor)
		{
			if(!actor.isPlayer() || actor.getTeam() == TeamType.NONE)
				return;

			Player player = actor.getPlayer();
			player.setInvul(SpecialEffectState.FALSE);
			player.stopAbnormalEffect(AbnormalEffect.INVULNERABLE);
		}
	}

	private void startSecondPhase()
	{
		if(state == EventState.SECOND_PHASE)
			return;

		if(pointManagerTask != null)
		{
			pointManagerTask.cancel(true);
			pointManagerTask = null;
		}
		setState(EventState.SECOND_PHASE);

		for(final TWControlSpotObject controlSpot : controlSpots)
			controlSpot.changeTeam(TeamType.NONE);

		spawnAction(CONTROL_SPOTS, false);
		spawnAction(TW_TELEPORTER, true);
		spawnAction(TW_RAIDBOSS, true);
	}

	@Override
	public void checkRestartLocs(Player player, Map<RestartType, Boolean> r)
	{
		r.clear();
	}

	private void checkForWinner()
	{
		if(state == EventState.REGISTRATION)
			return;

		if(state == EventState.FIRST_PHASE)
		{
			for(TeamType team : TeamType.values())
			{
				if(team == TeamType.NONE)
					continue;

				final AtomicInteger points = commandPoints[team.ordinalWithoutNone()];
				if(points.get() <= 0)
				{
					winner = team.revert();

					final String message = winner == TeamType.RED ? "Выиграла команда красных." : "Выиграла команда синих.";
					sendPackets(new SystemMessage(winner == TeamType.RED ? SystemMsg.THE_RED_TEAM_IS_VICTORIOUS : SystemMsg.THE_BLUE_TEAM_IS_VICTORIOUS));

					if(rewardItems != null)
					{
						for(DuelSnapshotObject d : players.get(winner))
						{
							for(int i = 0; i < rewardItems.length; i++)
							{
								Player player = d.getPlayer();
								if(player != null)
								{
									player.sendPacket(new ExShowScreenMessage(message, 1000, ScreenMessageAlign.TOP_CENTER, true));
									ItemFunctions.addItem(player, rewardItems[i], levelMul ? rewardCounts[i] * d.getPlayer().getLevel() : rewardCounts[i]);
								}
							}
						}
					}

					for(DuelSnapshotObject object : players.get(winner.revert()))
						quit(object);

					startSecondPhase();
					return;
				}
			}
		}
		else if(state == EventState.SECOND_PHASE)
		{
			int size = 0;
			for(final TeamType team : TeamType.VALUES)
			{
				final List<DuelSnapshotObject> objects = players.get(team);
				size += objects.size();
			}

			if(size < 1)
				stopEvent();
		}
	}

	public void addPoints(final TeamType team, final int pointsCount)
	{
		if(commandPoints == null)
			return;

		final AtomicInteger points = commandPoints[team.ordinalWithoutNone()];
		if(pointsCount == 1)
			points.incrementAndGet();
		else if(pointsCount == -1)
			points.decrementAndGet();
		else
			points.addAndGet(pointsCount);

		sendPointMsg();
		checkForWinner();
	}

	private void sendPointMsg()
	{
		String red = String.valueOf(commandPoints[1]);
		long rp = Long.parseLong(red);
		String blue = String.valueOf(commandPoints[0]);
		long bp = Long.parseLong(blue);

		for(TeamType t : TeamType.VALUES)
		{
			for(DuelSnapshotObject object : players.get(t))
			{
				Player player = object.getPlayer();
				final Language lang = player.getLanguage();
				final String msg = "Red: " + Util.formatAdena(rp) + " " + Util.declension(lang, rp, DeclensionKey.POINT) + " - Blue: " + Util.formatAdena(bp) + " " + Util.declension(lang, bp, DeclensionKey.POINT);
				player.sendPacket(new ExShowScreenMessage(msg, 9000000, ScreenMessageAlign.TOP_CENTER, true));
			}
		}
	}

	@Override
	public Iterator<DuelSnapshotObject> iterator()
	{
		final List<DuelSnapshotObject> blue = players.get(TeamType.BLUE);
		final List<DuelSnapshotObject> red = players.get(TeamType.RED);
		return new JoinedIterator<DuelSnapshotObject>(blue.iterator(), red.iterator());
	}

	private class PlayerListenerImpl implements OnPlayerExitListener, OnTeleportListener, OnDeathListener
	{
		@Override
		public void onPlayerExit(final Player player)
		{
			player.setReflection(ReflectionManager.DEFAULT);
			exitPlayer(player, true);
		}

		@Override
		public void onTeleport(final Player player, final int x, final int y, final int z, final Reflection r)
		{
			if(state != EventState.REGISTRATION && r != getReflection())
				exitPlayer(player, false);
		}

		private void exitPlayer(final Player player, final boolean exit)
		{
			for(final TeamType team : TeamType.VALUES)
			{
				final List<DuelSnapshotObject> objects = players.get(team);

				for(final DuelSnapshotObject d : objects)
				{
					if(d.getPlayer() == player)
					{
						objects.remove(d);
						quit(d);
						break;
					}
				}
			}

			player.removeListener(playerListener);
			checkForWinner();
		}

		@Override
		public void onDeath(Creature actor, Creature killer)
		{
			final Player player = actor.getPlayer();
			if(player == null)
				return;

			final TeamType team = player.getTeam();
			if(team == TeamType.NONE)
				return;

			_scores.remove(player.getName());
			player.doRevive(100);
			player.setCurrentMp(player.getMaxMp());
			player.setCurrentCp(player.getMaxCp());
			player.setCurrentHp(player.getMaxHp(), true);
			player.teleToLocation(getTeleportLoc(team));

			final Player _killer = killer.getPlayer();
			if(_killer != null)
			{
				addPoints(team, -1);
				announceKill(_killer, player, getPlayers(_killer.getTeam()));
				if(battlepvp && zone.getType() == Zone.ZoneType.battle_zone)
					_killer.setPvpKills(_killer.getPvpKills() + 1);
			}
		}
	}

	public List<DuelSnapshotObject> getPlayers(TeamType team)
	{
		return players.get(team);
	}

	private class ControlSpotPointManager extends RunnableImpl
	{
		@Override
		public void runImpl()
		{
			int red = 0;
			for(final TWControlSpotObject controlSpot : controlSpots)
			{
				TeamType owner = controlSpot.getOwnerTeam();
				if(owner == null || owner == TeamType.NONE)
				{
					red = -1;
					break;
				}
				else if(owner == TeamType.RED)
					red++;
			}

			if(red != -1)
			{
				if(red > controlSpots.size() / 2)
					addPoints(TeamType.BLUE, -1);
				else
					addPoints(TeamType.RED, -1);
			}
		}
	}

	public void putControl(TWControlSpotObject object)
	{
		controlSpots.add(object);
	}

	public void removeControl(TWControlSpotObject object)
	{
		controlSpots.remove(object);
	}

	@Override
	protected Location getTeleportLoc(TeamType team)
	{
		return teleportLocations[team.ordinalWithoutNone()];
	}

	@Override
	public boolean canResurrect(Creature active, Creature target, boolean force, boolean quiet)
	{
		if(state == EventState.SECOND_PHASE && active.getTeam() == target.getTeam())
			return true;
		else
		{
			active.sendPacket(SystemMsg.INVALID_TARGET);
			return false;
		}
	}

	//	@Override
	//	private void makeMap()
	//	{
	//		super.makeMap();
	//		teleportLocations[0] = map.getTeamSpawns().get(1)[0];
	//		teleportLocations[1] = map.getTeamSpawns().get(2)[0];
	//	}
}