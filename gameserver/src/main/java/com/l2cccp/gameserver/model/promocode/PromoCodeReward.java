package com.l2cccp.gameserver.model.promocode;

import com.l2cccp.gameserver.model.Player;

public abstract class PromoCodeReward
{
	public boolean validate()
	{
		return true;
	}

	public abstract void giveReward(Player player);
}
