package com.l2cccp.gameserver.model.entity.events.actions;

import com.l2cccp.gameserver.model.entity.events.Event;
import com.l2cccp.gameserver.model.entity.events.EventAction;

/**
 * @author VISTALL
 * @date 11:12/11.03.2011
 */
public class AnnounceAction implements EventAction
{
	private int _id;

	public AnnounceAction(int id)
	{
		_id = id;
	}

	@Override
	public void call(Event event)
	{
		event.announce(_id);
	}
}
