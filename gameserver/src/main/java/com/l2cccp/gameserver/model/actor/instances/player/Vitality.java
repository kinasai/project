package com.l2cccp.gameserver.model.actor.instances.player;

import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.tasks.VitalityUpdate;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.stats.Stats;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Vitality
{
	private static final Logger _log = LoggerFactory.getLogger(Vitality.class);
	private double _points;
	private final Player _player;
	private ScheduledFuture<?> _task;

	public Vitality(final Player player)
	{
		_points = Config.VITALITY_MAX_POINTS;
		_player = player;
	}

	public void set(final int points)
	{
		_points = Math.min(Config.VITALITY_MAX_POINTS, points);
		if(_task == null)
			_task = ThreadPoolManager.getInstance().scheduleAtFixedRate(new VitalityUpdate(_player), 60000, 60000);;
	}

	public void add(final int points)
	{
		if(_player.getNevitSystem().isBlessingActive() && points < 0)
			return;

		if(Config.VITALITY_DEBUG)
			_log.info("Vitality[" + _player.getName() + "] setPoints: vitPoints " + points);

		update(points);
	}

	public int getPoints()
	{
		return (int) _points;
	}

	public int getLevel()
	{
		int level = 0;
		for(final int point : Config.VITALITY_LEVELS_POINT)
		{
			if(_points <= point)
				break;

			level++;
		}

		return level;
	}

	public double getRate()
	{
		if(_player.getNevitSystem().isBlessingActive())
			return Config.VITALITY_LEVELS_RATE[4];

		return Config.VITALITY_LEVELS_RATE[(int) _player.calcStat(Stats.CHANGE_VP, getLevel(), null, null)];
	}

	public synchronized void update(final double points)
	{
		if(Config.VITALITY_MAX_PLAYER_LEVEL > _player.getLevel())
		{
			final boolean check = Config.VITALITY_LUCKY_SKILL;
			if(!check || (check && _player.getSkillLevel(159) > 0))
				return;
		}

		int prevLvl = getLevel();
		int prevPoints = (int) _points;

		changePoints(points);
		sendMsg(prevPoints, prevLvl);
	}

	public void updateExp(final long exp, final int level, boolean add, final boolean msg)
	{
		if(!add)
		{
			final int recovery = (int) _player.calcStat(Stats.RECOVERY_VP, -1, null, null);
			if(recovery == 1 || _player.getNevitSystem().isBlessingActive())
				add = true;
			else if(recovery == 0)
				return;
		}

		final double points = (exp / _player.getRateExp()) / (double) (level * level) * 100 / 9;

		if(Config.VITALITY_DEBUG)
			_log.info("Vitality[" + _player.getName() + "] updatePoints exp/lvl: " + exp + "[Recalc by rate: " + (exp / _player.getRateExp()) + "]/" + level + " vitPoints " + points);

		update(add ? points : -points);

		if(msg)
			_player.sendPacket(SystemMsg.YOU_HAVE_GAINED_VITALITY_POINTS);
	}

	private void changePoints(final double points)
	{
		final double point = _points + points;
		final double old = _points;
		_points = Math.min(Config.VITALITY_MAX_POINTS, Math.max(0, point));

		if(Config.VITALITY_DEBUG)
			_log.info("Vitality[" + _player.getName() + "] change points " + old + " --> " + _points);
	}

	private void sendMsg(final int points, final int level)
	{
		if(points != _points)
		{
			if(_points == 0)
				_player.sendPacket(SystemMsg.YOUR_VITALITY_IS_FULLY_EXHAUSTED);
			else if(_points == Config.VITALITY_MAX_POINTS)
				_player.sendPacket(SystemMsg.YOUR_VITALITY_IS_AT_MAXIMUM);
		}

		final int lvl = getLevel();
		if(level != lvl)
		{
			if(level > lvl)
				_player.sendPacket(SystemMsg.YOUR_VITALITY_HAS_DECREASED);
			else
				_player.sendPacket(SystemMsg.YOUR_VITALITY_HAS_INCREASED);

			_player.sendUserInfo(true);
		}
	}

	public void addMax()
	{
		add(Config.VITALITY_MAX_POINTS);
	}

	public void stopTask()
	{
		if(_task != null)
		{
			if(Config.VITALITY_DEBUG)
				_log.info("Vitality[" + _player.getName() + "] stop update task!");
			_task.cancel(true);
			_task = null;
		}
	}
}