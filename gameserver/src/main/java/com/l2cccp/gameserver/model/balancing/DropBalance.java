package com.l2cccp.gameserver.model.balancing;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class DropBalance
{
	private final BalanceType _type;
	private final int _id;
	private final double _count;
	private final double _chance;

	public DropBalance(final BalanceType type, final int id, final double count, final double chance)
	{
		_type = type;
		_id = _type == BalanceType.ID ? id : -1;
		_count = count;
		_chance = chance;
	}

	public BalanceType getType()
	{
		return _type;
	}

	public int getId()
	{
		return _id;
	}

	public double getCount()
	{
		return _count;
	}

	public double getChance()
	{
		return _chance;
	}
}