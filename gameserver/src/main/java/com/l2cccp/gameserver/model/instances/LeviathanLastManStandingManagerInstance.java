package com.l2cccp.gameserver.model.instances;

import com.l2cccp.gameserver.leviathan.type.LastManStanding;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class LeviathanLastManStandingManagerInstance extends LeviathanNpcInstance
{
	public LeviathanLastManStandingManagerInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void showChatWindow(Player player, int val, Object... arg)
	{
		final HtmlMessage packet = new HtmlMessage(this);
		init(packet, player.getLanguage());
		player.sendPacket(packet);
	}

	private void init(final HtmlMessage packet, final Language language)
	{
		final LastManStanding event = getEvent();
		switch(event.getState())
		{
			case PREPARE:
			{
				packet.setFile("leviathan/" + folder + "/index.htm");
				packet.replace("%time%", TimeUtils.formatTime((int) ((event.getTimeStart() - System.currentTimeMillis()) / 1000L), language));
				break;
			}
			case REGISTER:
			{
				packet.setFile("leviathan/" + folder + "/register.htm");
				packet.replace("%registered%", Util.formatAdena(event.getMembers().size()));
				packet.replace("%map%", event.getMap().getName(language));
				packet.replace("%end%", TimeUtils.toSimpleTime(event.getTimeStop()));
				break;
			}
			case REGISTER_CLOSED:
			case RUN:
			{
				packet.setFile("leviathan/" + folder + "/info.htm");
				packet.replace("%size%", Util.formatAdena(event.getTeam(TeamType.BLUE.ordinal()).size()));
				break;
			}
		}

		packet.replace("%name%", event.getTemplate().getName(language));
	}

	@Override
	public LastManStanding getEvent()
	{
		return (LastManStanding) event;
	}
}
