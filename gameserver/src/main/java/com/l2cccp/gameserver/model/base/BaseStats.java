package com.l2cccp.gameserver.model.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.data.xml.holder.BaseStatsHolder;
import com.l2cccp.gameserver.model.Creature;

public enum BaseStats
{
	STR
	{
		@Override
		public final double calcBonus(Creature actor)
		{
			return actor == null ? 1. : BaseStatsHolder.getInstance().getSTR(actor.getSTR());
		}

		@Override
		public final double calcChanceMod(Creature actor)
		{
			return Math.min(2. - Math.sqrt(calcBonus(actor)), 1.); // не более 1
		}
	},
	INT
	{
		@Override
		public final double calcBonus(Creature actor)
		{
			return actor == null ? 1. : BaseStatsHolder.getInstance().getINT(actor.getINT());
		}
	},
	DEX
	{
		@Override
		public final double calcBonus(Creature actor)
		{
			return actor == null ? 1. : BaseStatsHolder.getInstance().getDEX(actor.getDEX());
		}
	},
	WIT
	{
		@Override
		public final double calcBonus(Creature actor)
		{
			return actor == null ? 1. : BaseStatsHolder.getInstance().getWIT(actor.getWIT());
		}
	},
	CON
	{
		@Override
		public final double calcBonus(Creature actor)
		{
			return actor == null ? 1. : BaseStatsHolder.getInstance().getCON(actor.getCON());
		}
	},
	MEN
	{
		@Override
		public final double calcBonus(Creature actor)
		{
			return actor == null ? 1. : BaseStatsHolder.getInstance().getMEN(actor.getMEN());
		}
	},
	NONE;

	public static final BaseStats[] VALUES = values();

	protected static final Logger _log = LoggerFactory.getLogger(BaseStats.class);

	public double calcBonus(Creature actor)
	{
		return 1.;
	}

	public double calcChanceMod(Creature actor)
	{
		return 2. - Math.sqrt(calcBonus(actor));
	}
}