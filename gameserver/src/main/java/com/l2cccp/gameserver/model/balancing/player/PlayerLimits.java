package com.l2cccp.gameserver.model.balancing.player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PlayerLimits
{
	private int magic_attack;
	private int physical_attack;
	private int magic_attack_speed;
	private int physical_attack_speed;
	private int magic_critical_attack;
	private int physical_critical_attack;
	private int magic_defence;
	private int physical_defence;
	private int max_cp;
	private int max_hp;
	private int max_mp;
	private int speed;
	private int accuracy;
	private int evasion;

	public void setLimit(final BalancerType type, final int value)
	{
		switch(type)
		{
			case accuracy:
				accuracy = value;
				break;
			case evasion:
				evasion = value;
				break;
			case magic_attack:
				magic_attack = value;
				break;
			case magic_attack_speed:
				magic_attack_speed = value;
				break;
			case magic_critical_attack:
				magic_critical_attack = value;
				break;
			case magic_defence:
				magic_defence = value;
				break;
			case max_cp:
				max_cp = value;
				break;
			case max_hp:
				max_hp = value;
				break;
			case max_mp:
				max_mp = value;
				break;
			case physical_attack:
				physical_attack = value;
				break;
			case physical_attack_speed:
				physical_attack_speed = value;
				break;
			case physical_critical_attack:
				physical_critical_attack = value;
				break;
			case physical_defence:
				physical_defence = value;
				break;
			case speed:
				speed = value;
				break;
		}
	}

	public int getAccuracy()
	{
		return accuracy;
	}

	public int getEvasion()
	{
		return evasion;
	}

	public int getMagicAttack()
	{
		return magic_attack;
	}

	public int getMagicAttackSpeed()
	{
		return magic_attack_speed;
	}

	public int getMagicCriticalAttack()
	{
		return magic_critical_attack;
	}

	public int getMagicDefence()
	{
		return magic_defence;
	}

	public int getMaxCP()
	{
		return max_cp;
	}

	public int getMaxHP()
	{
		return max_hp;
	}

	public int getMaxMP()
	{
		return max_mp;
	}

	public int getPhysicalAttack()
	{
		return physical_attack;
	}

	public int getPhysicalAttackSpeed()
	{
		return physical_attack_speed;
	}

	public int getPhysicalCriticalAttack()
	{
		return physical_critical_attack;
	}

	public int getPhysicalDefence()
	{
		return physical_defence;
	}

	public int getSpeed()
	{
		return speed;
	}
}