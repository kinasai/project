package com.l2cccp.gameserver.model.items;

import java.util.ArrayList;
import java.util.List;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TradeList
{
	private List<TradeItem> tradeList = new ArrayList<TradeItem>();
	private int _id;
	private int _npcId;

	public TradeList(int id)
	{
		_id = id;
	}

	public int getListId()
	{
		return _id;
	}

	public void setNpcId(int id)
	{
		_npcId = id;
	}

	public int getNpcId()
	{
		return _npcId;
	}

	public void addItem(TradeItem ti)
	{
		tradeList.add(ti);
	}

	public synchronized List<TradeItem> getItems()
	{
		List<TradeItem> result = new ArrayList<TradeItem>();
		long currentTime = System.currentTimeMillis() / 60000L;
		for(TradeItem ti : tradeList)
		{
			// А не пора ли обновить количество лимитированных предметов в трейд листе?
			if(ti.isCountLimited())
			{
				if(ti.getCurrentValue() < ti.getCount() && ti.getLastRechargeTime() + ti.getRechargeTime() <= currentTime)
				{
					ti.setLastRechargeTime(ti.getLastRechargeTime() + ti.getRechargeTime());
					ti.setCurrentValue(ti.getCount());
				}

				if(ti.getCurrentValue() == 0)
					continue;
			}

			result.add(ti);
		}

		return result;
	}

	public TradeItem getItemByItemId(int itemId)
	{
		for(TradeItem ti : tradeList)
		{
			if(ti.getItemId() == itemId)
				return ti;
		}
		return null;
	}

	public synchronized void updateItems(List<TradeItem> buyList)
	{
		for(TradeItem ti : buyList)
		{
			TradeItem ic = getItemByItemId(ti.getItemId());

			if(ic.isCountLimited())
				ic.setCurrentValue(Math.max(ic.getCurrentValue() - ti.getCount(), 0));
		}
	}
}