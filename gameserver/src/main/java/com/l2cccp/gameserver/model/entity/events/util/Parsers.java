package com.l2cccp.gameserver.model.entity.events.util;

import java.util.StringTokenizer;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Parsers
{
	public static long[][] parseReward(String list)
	{
		if(list == null || list.isEmpty())
			return null;

		StringTokenizer st = new StringTokenizer(list, ",");
		long[][] rewards = new long[st.countTokens()][2];
		int index = 0;
		while(st.hasMoreTokens())
		{
			String[] reward = st.nextToken().split(":");
			rewards[index] = new long[] { Integer.parseInt(reward[0]), Long.parseLong(reward[1]) };
			index++;
		}

		return rewards;
	}
}
