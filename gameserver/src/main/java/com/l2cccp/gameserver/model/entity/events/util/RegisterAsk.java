package com.l2cccp.gameserver.model.entity.events.util;

import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.impl.TeamEvent;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RegisterAsk implements OnAnswerListener
{
	private TeamEvent _event;

	public RegisterAsk(TeamEvent event)
	{
		_event = event;
	}

	@Override
	public void sayYes(Player player)
	{
		_event.registerPlayer(player);
	}

	@Override
	public void sayNo(Player player)
	{}
}