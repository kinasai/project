package com.l2cccp.gameserver.model.balancing;

import java.util.ArrayList;
import java.util.List;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public abstract class Balancing
{
	private static List<Balancing> _balancers = new ArrayList<Balancing>();

	protected Balancing()
	{
		_balancers.add(this);
	}

	public static void clearAll()
	{
		for(Balancing blancer : _balancers)
			blancer.clear();
	}

	public abstract void add(final int id, final PlayerBalance balance);

	public abstract double getBalancing(final int id, final boolean olymp);

	public abstract void clear();
}