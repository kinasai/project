package com.l2cccp.gameserver.model.offlinebuffer;

import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentHashMap;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.ResidenceHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.Skill.SkillTargetType;
import com.l2cccp.gameserver.model.Skill.SkillType;
import com.l2cccp.gameserver.model.Zone.ZoneType;
import com.l2cccp.gameserver.model.base.ClassId;
import com.l2cccp.gameserver.model.dress.DressData;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.model.entity.residence.ClanHall;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.CharInfo;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.PositionUtils;
import com.l2cccp.gameserver.utils.TradeHelper;
import com.l2cccp.gameserver.utils.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OfflineBufferManager
{
	protected static final Logger _log = LoggerFactory.getLogger(OfflineBufferManager.class);

	private static final int MAX_INTERACT_DISTANCE = 100;

	private final Map<Integer, BufferData> _buffStores = new ConcurrentHashMap<Integer, BufferData>();

	protected OfflineBufferManager()
	{}

	public Map<Integer, BufferData> getBuffStores()
	{
		return _buffStores;
	}

	public void processBypass(Player player, String command)
	{
		final StringTokenizer st = new StringTokenizer(command, " ");
		st.nextToken();
		String link = st.nextToken();

		if("setstore".equals(link))
		{
			try
			{
				final int price = Integer.parseInt(st.nextToken());
				String title = st.nextToken();
				while(st.hasMoreTokens())
				{
					title += " " + st.nextToken();
				}
				title = title.trim();

				if(_buffStores.containsKey(player.getObjectId()))
					return;

				if(player.getPrivateStoreType() != Player.STORE_PRIVATE_NONE)
				{
					player.sendMessage("You already have a store");
					return;
				}

				if(!Config.OFFLINE_BUFFER_STORE_ALLOWED_CLASS_LIST.contains(player.getClassId().getId()))
				{
					player.sendMessage("Your profession is not allowed to set an Buff Store");
					return;
				}

				if(!TradeHelper.checksIfCanOpenStore(player, Player.STORE_PRIVATE_BUFF))
					return;

				if(title.isEmpty() || title.length() >= 29)
				{
					player.sendMessage("You must put a title for this store and it must have less than 29 characters");
					return;
				}

				if(price < Config.OFFLINE_BUFFER_STORE_PRICE_DATA[0] || price > Config.OFFLINE_BUFFER_STORE_PRICE_DATA[1])
				{
					player.sendMessage("The price for each buff must be between " + Util.formatAdena(Config.OFFLINE_BUFFER_STORE_PRICE_DATA[0]) + " and " + Util.formatAdena(Config.OFFLINE_BUFFER_STORE_PRICE_DATA[1]));
					return;
				}

				final ClanHall ch = ResidenceHolder.getInstance().getResidenceByObject(ClanHall.class, player);
				if(!player.isGM() && !player.isInZone(ZoneType.OFFBUFF) && ch == null)
				{
					player.sendMessage("You can't put a buff store here. Look for special designated zones or clan halls");
					return;
				}

				if(player.isAlikeDead() || player.isInOlympiadMode() || player.isMounted() || player.isCastingNow() || player.getOlympiadObserveGame() != null || player.getOlympiadGame() != null || Olympiad.isRegisteredInComp(player))
				{
					player.sendMessage("You don't meet the required conditions to put a buff store right now");
					return;
				}

				final BufferData buffer = new BufferData(player, title, price, null);

				for(SkillEntry skill : player.getAllSkills())
				{
					if(!skill.getTemplate().isActive())
						continue;

					if(skill.getSkillType() != SkillType.BUFF)
						continue;

					if(skill.getTemplate().isHeroic())
						continue;

					if(skill.getTemplate().getTargetType() == SkillTargetType.TARGET_SELF)
						continue;

					if(skill.getTemplate().getTargetType() == SkillTargetType.TARGET_PET)
						continue;

					if(player.getClassId().equalsOrChildOf(ClassId.doomcryer) && skill.getTemplate().getTargetType() == SkillTargetType.TARGET_CLAN)
						continue;

					if(player.getClassId().equalsOrChildOf(ClassId.dominator) && (skill.getTemplate().getTargetType() == SkillTargetType.TARGET_PARTY || skill.getTemplate().getTargetType() == SkillTargetType.TARGET_ONE))
						continue;

					if(Config.OFFLINE_BUFFER_STORE_FORBIDDEN_SKILL_LIST.contains(skill.getId()))
						continue;

					buffer.getBuffs().put(skill.getId(), skill);
				}

				if(buffer.getBuffs().isEmpty())
				{
					player.sendMessage("You don't have any available buff to put on sale in the store");
					return;
				}

				_buffStores.put(player.getObjectId(), buffer);

				player.sitDown(null);

				player.setVisibleTitle(title);
				player.setVisibleTitleColor(Config.OFFLINE_BUFFER_STORE_TITLE_COLOR);
				player.setVisibleNameColor(Config.OFFLINE_BUFFER_STORE_NAME_COLOR);

				player.setPrivateStoreType(Player.STORE_PRIVATE_BUFF);

				updateInWorld(player);
				player.broadcastUserInfo(true);

				player.sendMessage("Your Buff Store was set succesfully");
			}
			catch(NumberFormatException e)
			{
				player.sendMessage("The price for each buff must be between 1 and 10kk");

				final HtmlMessage html = new HtmlMessage(5);
				html.setFile("command/buffstore/create.htm");
				player.sendPacket(html);
				_log.error("NumberFormatException ", e);
			}
			catch(Exception e)
			{
				final HtmlMessage html = new HtmlMessage(5);
				html.setFile("command/buffstore/create.htm");
				player.sendPacket(html);
				_log.error("Exception ", e);
			}
			return;
		}

		if("stopstore".equals(link))
		{
			if(player.getPrivateStoreType() != Player.STORE_PRIVATE_BUFF)
			{
				player.sendMessage("You dont have any store set right now");
				return;
			}

			_buffStores.remove(player.getObjectId());

			player.setPrivateStoreType(Player.STORE_PRIVATE_NONE);
			player.standUp();

			player.setVisibleTitle(null);
			player.setVisibleTitleColor(0);
			player.setVisibleNameColor(0);
			
			updateInWorld(player);
			player.broadcastUserInfo(true);

			player.sendMessage("Your Buff Store was removed succesfuly");

			return;
		}

		if("bufflist".equals(link))
		{
			try
			{
				final int playerId = Integer.parseInt(st.nextToken());
				final boolean isPlayer = (st.hasMoreTokens() ? st.nextToken().equalsIgnoreCase("player") : true);
				final int page = (st.hasMoreTokens() ? Integer.parseInt(st.nextToken()) : 0);

				final BufferData buffer = _buffStores.get(playerId);
				if(buffer == null)
					return;

				if(PositionUtils.calculateDistance(player, buffer.getOwner(), true) > MAX_INTERACT_DISTANCE)
					return;

				if(!isPlayer && player.getServitor() == null)
				{
					player.sendMessage("You don't have any active summon right now");

					showStoreWindow(player, buffer, !isPlayer, page);
					return;
				}

				showStoreWindow(player, buffer, isPlayer, page);
			}
			catch(Exception e)
			{}
			return;
		}

		if("purchasebuff".equals(link))
		{
			try
			{
				final int playerId = Integer.parseInt(st.nextToken());
				final boolean isPlayer = (st.hasMoreTokens() ? st.nextToken().equalsIgnoreCase("player") : true);
				final int buffId = Integer.parseInt(st.nextToken());
				final int page = (st.hasMoreTokens() ? Integer.parseInt(st.nextToken()) : 0);

				final BufferData buffer = _buffStores.get(playerId);
				if(buffer == null)
					return;

				Player owner = buffer.getOwner();

				if(owner.getPrivateStoreType() != Player.STORE_PRIVATE_BUFF)
				{
					player.sendMessage("This player can't sell buff!");
					return;
				}

				if(!buffer.getBuffs().containsKey(buffId))
					return;

				if(PositionUtils.calculateDistance(player, owner, true) > MAX_INTERACT_DISTANCE)
					return;

				if(!isPlayer && player.getServitor() == null)
				{
					player.sendMessage("You don't have any active summon right now");
					showStoreWindow(player, buffer, !isPlayer, page);
					return;
				}

				if(player.getPvpFlag() > 0 || player.isInCombat() || player.getKarma() > 0 || player.isAlikeDead() || player.getVar("jailed") != null || player.isInOlympiadMode() || player.isCursedWeaponEquipped() || player.isInStoreMode() || player.isInTrade() || player.getEnchantScroll() != null || player.isFishing())
				{
					player.sendMessage("You don't meet the required conditions to use the buffer right now");
					return;
				}

				final double buffMpCost = (Config.OFFLINE_BUFFER_STORE_MP_ENABLED ? buffer.getBuffs().get(buffId).getTemplate().getMpConsume() * Config.OFFLINE_BUFFER_STORE_MP_CONSUME_MULTIPLIER : 0);

				if(buffMpCost > 0 && owner.getCurrentMp() < buffMpCost)
				{
					boolean stop = true;

					if(Config.OFFLINE_BUFFER_STORE_MP_RESTORE_ENABLED)
					{
						int item = (int) Config.OFFLINE_BUFFER_STORE_MP_RESTORE_PRICE_DATA[0];
						long count = Config.OFFLINE_BUFFER_STORE_MP_RESTORE_PRICE_DATA[1];
						if(Util.getPay(owner, item, count, false))
						{
							stop = false;
							owner.setCurrentMp(owner.getMaxMp());
							owner.broadcastPacket(new MagicSkillUse(owner, owner, 5678, 1, 0, 0));
						}
					}

					if(stop)
					{
						player.sendMessage("This store doesn't have enough mp to give sell you this buff");
						showStoreWindow(player, buffer, isPlayer, page);
						return;
					}
				}

				long buffPrice = (player.getClan() != null && owner.getClan() != null && player.getClanId() == owner.getClanId()) ? 0 : buffer.getBuffPrice();

				if(buffPrice > 0 && player.getAdena() < buffPrice)
				{
					player.sendMessage("You don't have enough adena to purchase a buff");
					return;
				}

				if(buffPrice > 0 && !player.reduceAdena(buffPrice, true))
				{
					player.sendMessage("You don't have enough adena to purchase a buff");
					return;
				}

				if(buffPrice > 0)
				{
					if(Config.OFFLINE_BUFFER_STORE_FEE != 0)
					{
						long fee = (long) ((buffPrice * Config.OFFLINE_BUFFER_STORE_FEE) / 100);
						if(fee < 0)
							owner.sendMessage("From your payment withdrawn " + Util.formatAdena(fee) + " fee.");
						buffPrice = buffPrice + fee;
					}
					owner.addAdena(buffPrice, true);
				}

				if(buffMpCost > 0)
					owner.reduceCurrentMp(buffMpCost, null);

				SkillEntry skill = buffer.getBuffs().get(buffId);
				int baseMaxLvl = SkillTable.getInstance().getBaseLevel(skill.getId());
				int lvl = skill.getLevel() > baseMaxLvl ? baseMaxLvl : skill.getLevel();
				if(isPlayer)
				{
					owner.broadcastPacket(new MagicSkillUse(owner, player, buffId, lvl, skill.getTemplate().getHitTime() / 2, 0));

					if(Config.OFFLINE_BUFFER_STORE_TIMER > 0)
						skill.getEffects(player, player, false, false, Config.OFFLINE_BUFFER_STORE_TIMER * 60000, 0, 5);
					else
						skill.getEffects(player, player, false, false);
				}
				else
				{
					owner.broadcastPacket(new MagicSkillUse(owner, player.getServitor(), buffId, lvl, skill.getTemplate().getHitTime() / 2, 0));
					if(Config.OFFLINE_BUFFER_STORE_TIMER > 0)
						skill.getEffects(player.getServitor(), player.getServitor(), false, false, Config.OFFLINE_BUFFER_STORE_TIMER * 60000, 0, 5);
					else
						skill.getEffects(player.getServitor(), player.getServitor(), false, false);
				}

				player.sendMessage("You have bought " + buffer.getBuffs().get(buffId).getName() + " from " + owner.getName());

				showStoreWindow(player, buffer, isPlayer, page);
			}
			catch(Exception e)
			{

			}
			return;
		}

	}

	private void updateInWorld(Player activeChar)
	{
		activeChar.sendUserInfo(true);
		activeChar.sendUserInfo(true);
		for(Player player : World.getAroundPlayers(activeChar))
		{
			if(player.isInvisible() && !activeChar.getPlayerAccess().IsGM)
				continue;

			activeChar.sendPacket(new CharInfo(player, DressData.check(activeChar)));
		}
	}

	private void showStoreWindow(Player player, BufferData buffer, boolean isForPlayer, int page)
	{
		final HtmlMessage html = new HtmlMessage(5);
		html.setFile("command/buffstore/buffer.htm");

		final int MAX_ENTRANCES_PER_ROW = 6;
		final double entrancesSize = buffer.getBuffs().size();
		final int maxPage = (int) Math.ceil(entrancesSize / MAX_ENTRANCES_PER_ROW) - 1;
		final int currentPage = Math.min(maxPage, page);

		final Iterator<SkillEntry> it = buffer.getBuffs().values().iterator();
		SkillEntry buff;
		int i = 0;
		int baseMaxLvl;
		int enchantLvl;
		int enchantType;
		boolean changeColor = false;

		String template = HtmCache.getInstance().getHtml("command/buffstore/template.htm", player);
		String block = "";
		String list = "";

		while(it.hasNext())
		{
			if(i < currentPage * MAX_ENTRANCES_PER_ROW)
			{
				it.next();
				i++;
				continue;
			}

			if(i >= (currentPage * MAX_ENTRANCES_PER_ROW + MAX_ENTRANCES_PER_ROW))
				break;

			buff = it.next();
			baseMaxLvl = SkillTable.getInstance().getBaseLevel(buff.getId());

			block = template;
			block = block.replace("{bypass}", "bypass -h offbuff purchasebuff " + buffer.getOwner().getObjectId() + " " + (isForPlayer ? "player" : "summon") + " " + buff.getId() + " " + currentPage);
			block = block.replace("{name}", buff.getName());
			block = block.replace("{icon}", buff.getTemplate().getIcon());
			block = block.replace("{color}", changeColor ? "171612" : "23221e");

			if(buff.getLevel() > baseMaxLvl)
			{
				enchantType = (buff.getLevel() - baseMaxLvl) / buff.getTemplate().getEnchantLevelCount();
				enchantLvl = (buff.getLevel() - baseMaxLvl) % buff.getTemplate().getEnchantLevelCount();
				enchantLvl = (enchantLvl == 0 ? buff.getTemplate().getEnchantLevelCount() : enchantLvl);

				block = block.replace("{level}", String.valueOf(baseMaxLvl));
				block = block.replace("{enchant}", "+" + enchantLvl + " " + (enchantType >= 3 ? "Power" : (enchantType >= 2 ? "Cost" : "Time")));
			}
			else
			{
				block = block.replace("{level}", String.valueOf(buff.getLevel()));
				block = block.replace("{enchant}", "None");
			}

			list += block;
			i++;
			changeColor = !changeColor;
		}

		String previousPageButton = "";
		String nextPageButton = "";

		if(currentPage > 0)
			previousPageButton = "bypass -h offbuff bufflist " + buffer.getOwner().getObjectId() + " " + (isForPlayer ? "player" : "summon") + " " + (currentPage - 1);

		if(currentPage < maxPage)
			nextPageButton = "bypass -h offbuff bufflist " + buffer.getOwner().getObjectId() + " " + (isForPlayer ? "player" : "summon") + " " + (currentPage + 1);

		html.replace("%bufferId%", String.valueOf(buffer.getOwner().getObjectId()));
		html.replace("%bufferClass%", Util.className(player, buffer.getOwner().getClassId().getId()));
		html.replace("%bufferLvl%", String.valueOf((buffer.getOwner().getLevel() >= 76 && buffer.getOwner().getLevel() < 80 ? 76 : (buffer.getOwner().getLevel() >= 84 ? 84 : Math.round(buffer.getOwner().getLevel() / 10) * 10))));
		html.replace("%bufferName%", buffer.getOwner().getName());

		int curr = (int) buffer.getOwner().getCurrentMp();
		int max = (int) buffer.getOwner().getMaxMp();

		html.replace("%Mp%", String.valueOf(curr));
		html.replace("%MaxMp%", String.valueOf(max));
		html.replace("%mpstat%", String.valueOf(Util.calcLineSize(curr, max, 89)));

		int currhp = (int) buffer.getOwner().getCurrentHp();
		int maxhp = (int) buffer.getOwner().getMaxHp();

		html.replace("%Hp%", String.valueOf(currhp));
		html.replace("%MaxHp%", String.valueOf(maxhp));
		html.replace("%hpstat%", String.valueOf(Util.calcLineSize(currhp, maxhp, 89)));
		html.replace("%buffPrice%", Util.formatPay(player, buffer.getBuffPrice(), 57));
		html.replace("%target%", (isForPlayer ? "Player" : "Summon"));
		html.replace("%page%", String.valueOf(currentPage));
		html.replace("%previousPageButton%", previousPageButton);
		html.replace("%nextPageButton%", nextPageButton);
		html.replace("%pageCount%", (currentPage + 1) + "/" + (maxPage + 1));
		html.replace("%buffs%", list);

		player.sendPacket(html);
	}

	private static final OfflineBufferManager _instance = new OfflineBufferManager();

	public static OfflineBufferManager getInstance()
	{
		return _instance;
	}
}
