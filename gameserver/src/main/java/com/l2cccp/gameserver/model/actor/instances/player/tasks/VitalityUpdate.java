package com.l2cccp.gameserver.model.actor.instances.player.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.lang.reference.HardReference;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.Vitality;
import com.l2cccp.gameserver.network.l2.s2c.ExVitalityPointInfo;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class VitalityUpdate implements Runnable
{
	private static final Logger _log = LoggerFactory.getLogger(VitalityUpdate.class);
	private int _last = 0;
	private final HardReference<Player> _player;

	public VitalityUpdate(final Player player)
	{
		_player = player.getRef();
	}

	public void run()
	{
		Player player = _player.get();

		if(Config.VITALITY_DEBUG)
			_log.info("Vitality[" + player.getName() + "] update task run");

		Vitality vitality = player.getVitality();
		if(player.isInZonePeace())
			vitality.update((Config.VITALITY_MAX_POINTS / Config.VITALITY_RECOVERY_TIME) / Config.VITALITY_RECOVERY_RATE);

		final int point = vitality.getPoints();
		if(_last != point)
		{
			_last = point;
			player.sendPacket(new ExVitalityPointInfo(_last));
		}
	}
}