package com.l2cccp.gameserver.model.autoAtt;

import com.l2cccp.gameserver.model.base.Element;
import com.l2cccp.gameserver.model.items.ItemInstance;

public class AutoAtt {
	public ItemInstance itemToEnchant;
	public ItemInstance stone;
	public Element element;
	public int maxValue;
	public long stoneCount;
	public boolean isCrystal;
	public boolean possible;
}