package com.l2cccp.gameserver.model.entity.events.objects;

import com.l2cccp.gameserver.model.entity.events.Event;

/**
 * @author VISTALL
 * @date 11:38/30.06.2011
 */
public interface InitableObject
{
	void initObject(Event e);
}
