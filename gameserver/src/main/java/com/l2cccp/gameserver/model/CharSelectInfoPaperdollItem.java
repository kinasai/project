package com.l2cccp.gameserver.model;

/**
* @author VISTALL
* @date 20:21/03.02.2012
*/
public class CharSelectInfoPaperdollItem
{
	private int _itemId;
	private int _visual_itemId;
	private int[] _augmentations;
	private int _enchantLevel;
	private int _flag;

	public CharSelectInfoPaperdollItem(int itemId, int visual_itemId, int[] augmentations, int enchantLevel, int flag)
	{
		_itemId = itemId;
		_visual_itemId = visual_itemId;
		_augmentations = augmentations;
		_enchantLevel = enchantLevel;
		_flag = flag;
	}

	public int getItemId()
	{
		return _itemId;
	}

	public int getVisualItemId()
	{
		return _visual_itemId;
	}

	public int[] getAugmentations()
	{
		return _augmentations;
	}

	public int getEnchantLevel()
	{
		return _enchantLevel;
	}

	public int getCustomFlags()
	{
		return _flag;
	}
}
