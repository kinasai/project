package com.l2cccp.gameserver.model.instances;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.impl.TerritoryWarsEvent;
import com.l2cccp.gameserver.model.entity.events.objects.DuelSnapshotObject;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TWManagerInstance extends NpcInstance
{
	public TWManagerInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		final TerritoryWarsEvent event = getEvent(TerritoryWarsEvent.class);

		if(event == null)
			return;

		if(command.startsWith("register"))
			event.registerPlayer(player);
		else if(command.startsWith("unregister"))
			event.unregisterPlayer(player);
		else if(command.equals("teleport"))
		{
			if(player.getTeam() == event.getWinner())
				player.teleToLocation(new Location(85640, 16024, -1256));
		}
		else if(command.equals("teleportback"))
		{
			for(DuelSnapshotObject object : event.getPlayers(player.getTeam()))
			{
				if(object.getPlayer().getObjectId() == player.getObjectId())
					object.teleportBack(5000L);
			}
		}
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public void showChatWindow(Player player, int val, Object... arg)
	{
		final String fileName;

		switch(getNpcId())
		{
			case 40001:
				fileName = "events/territory_wars/tw_teleporter.htm";
				break;
			default:
				fileName = "events/territory_wars/tw_manager.htm";
				break;
		}

		super.showChatWindow(player, fileName);
	}
}