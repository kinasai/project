package com.l2cccp.gameserver.model.balancing;

import java.util.List;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class NpcBalance
{
	private final int _id;
	private List<DropBalance> _drop;
	private double _adena = 1.;
	private List<DropBalance> _spoil;

	public NpcBalance(final int id)
	{
		_id = id;
	}

	public int getId()
	{
		return _id;
	}

	public void setDrop(final List<DropBalance> drop)
	{
		_drop = drop;
	}

	public List<DropBalance> getDrop()
	{
		return _drop;
	}

	public void setAdena(final double adena)
	{
		_adena = adena;
	}

	public double getAdena()
	{
		return _adena;
	}

	public void setSpoil(final List<DropBalance> spoil)
	{
		_spoil = spoil;
	}

	public List<DropBalance> getSpoil()
	{
		return _spoil;
	}
}