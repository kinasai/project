package com.l2cccp.gameserver.model.entity.events.impl;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerEnterListener;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.listener.PlayerListenerList;
import com.l2cccp.gameserver.model.entity.events.Event;
import com.l2cccp.gameserver.model.entity.events.EventType;
import com.l2cccp.gameserver.model.entity.events.actions.StartStopAction;
import com.l2cccp.gameserver.network.l2.s2c.ExBR_BroadcastEventState;
import com.l2cccp.gameserver.utils.TimeUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OfficialEvent extends Event
{
	protected final Calendar _startPattern, _stopPattern;
	protected final int _broadcast;
	private final MultiValueSet<String> _parameters;
	private final Listener _listener;

	private boolean _isInProgress;
	protected long _startTime;

	public OfficialEvent(MultiValueSet<String> set)
	{
		super(set);

		_startPattern = getCalendar(set.getString("start-pattern"));
		_stopPattern = getCalendar(set.getString("stop-pattern"));
		_broadcast = set.getInteger("broadcast");
		_listener = new Listener();
		_parameters = set;
	}

	private static Calendar getCalendar(String d)
	{
		final Calendar calendar = Calendar.getInstance();

		try
		{
			final Date date = TimeUtils.SIMPLE_FORMAT.parse(d);
			calendar.setTimeInMillis(date.getTime());
		}
		catch(ParseException e)
		{
			throw new Error(e);
		}

		return calendar;
	}

	@Override
	public void startEvent()
	{
		setProgress(true);
		for(final Player player : GameObjectsStorage.getPlayers())
			broadcast(player);

		PlayerListenerList.addGlobal(_listener);
		super.startEvent();
	}

	@Override
	public void stopEvent()
	{
		setProgress(false);
		reCalcNextTime(false);
		for(final Player player : GameObjectsStorage.getPlayers())
			broadcast(player);

		PlayerListenerList.removeGlobal(_listener);
		super.stopEvent();
	}

	@Override
	public void printInfo()
	{
		final long startSiegeMillis = startTimeMillis();

		if(startSiegeMillis == 0)
			info(getName() + " time - undefined");
		else
			info(getName() + " time - " + TimeUtils.toSimpleFormat(startSiegeMillis) + ", stop - " + TimeUtils.toSimpleFormat(_stopPattern.getTimeInMillis()));
	}

	@Override
	public void reCalcNextTime(boolean onInit)
	{
		_onTimeActions.clear();

		if(_stopPattern.getTimeInMillis() > System.currentTimeMillis())
		{
			_startTime = _startPattern.getTimeInMillis();
			final long dff = _stopPattern.getTimeInMillis() - _startPattern.getTimeInMillis();

			addOnTimeAction(0, new StartStopAction(StartStopAction.EVENT, true));
			addOnTimeAction((int) (dff / 1000L), new StartStopAction(StartStopAction.EVENT, false));

			registerActions();
		}
	}

	@Override
	public EventType getType()
	{
		return EventType.OFFICIAL_EVENT;
	}

	@Override
	protected long startTimeMillis()
	{
		return _startTime;
	}

	private void setProgress(final boolean progress)
	{
		_isInProgress = progress;
	}

	@Override
	public boolean isInProgress()
	{
		return _isInProgress;
	}

	public MultiValueSet<String> getParameters()
	{
		return _parameters;
	}

	private class Listener implements OnPlayerEnterListener
	{
		@Override
		public void onPlayerEnter(Player player)
		{
			if(isInProgress())
				broadcast(player);
		}
	}

	private void broadcast(final Player player)
	{
		player.sendPacket(new ExBR_BroadcastEventState(_broadcast, isInProgress() ? 1 : 0));
	}
}