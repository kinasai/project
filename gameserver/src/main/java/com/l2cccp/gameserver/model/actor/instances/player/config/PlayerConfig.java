package com.l2cccp.gameserver.model.actor.instances.player.config;

/**
 * 
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PlayerConfig
{
	private final int _obj;
	private int _nocarrier; // 0-90
	private int _buffdist; // -1:Off, 0:Self, 1-100
	private String _translit; // On, Off, LT
	private int _autoloot; // 0:Off, 1:On, 2:No Arrows
	private boolean _autolootherbs; // On, Off
	private boolean _expoff; // On, Off
	private boolean _hidetraders; // On, Off
	private boolean _hidevisual; // On, Off
	private boolean _classicshift; // On, Off
	private boolean _rain; // On, Off
	private boolean _imgindrop; // On, Off

	public PlayerConfig(final int obj)
	{
		_obj = obj;
	}

	public int getObj()
	{
		return _obj;
	}

	public String getTranslit()
	{
		return _translit;
	}

	public void setTranslit(final String value)
	{
		_translit = value;
	}

	public int getAutoLoot()
	{
		return _autoloot;
	}

	public void setAutoLoot(final int value)
	{
		_autoloot = value;
	}

	public int getBuffDistance()
	{
		return _buffdist;
	}

	public void setBuffDistance(final int value)
	{
		_buffdist = value;
	}

	public int getNoCarrier()
	{
		return _nocarrier;
	}

	public void setNoCarrier(final int value)
	{
		_nocarrier = value;
	}

	public boolean isAutoLootHerb()
	{
		return _autolootherbs;
	}

	public void setAutoLootHerb(final boolean value)
	{
		_autolootherbs = value;
	}

	public boolean isExpOff()
	{
		return _expoff;
	}

	public void setExpOff(final boolean value)
	{
		_expoff = value;
	}

	public boolean isHideTraders()
	{
		return _hidetraders;
	}

	public void setHideTraders(final boolean value)
	{
		_hidetraders = value;
	}

	public boolean isClassicShift()
	{
		return _classicshift;
	}

	public void setClassicShift(final boolean value)
	{
		_classicshift = value;
	}

	public boolean showRain()
	{
		return _rain;
	}

	public void setRain(final boolean value)
	{
		_rain = value;
	}

	public boolean showImages()
	{
		return _imgindrop;
	}

	public void setImages(final boolean value)
	{
		_imgindrop = value;
	}

	public boolean isHideVisual()
	{
		return _hidevisual;
	}

	public void setHideVisual(final boolean value)
	{
		_hidevisual = value;
	}
}