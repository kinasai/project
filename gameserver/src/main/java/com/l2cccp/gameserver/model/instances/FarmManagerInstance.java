package com.l2cccp.gameserver.model.instances;

import com.l2cccp.gameserver.data.xml.holder.MultiSellHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.PcInventory;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FarmManagerInstance extends NpcInstance
{
	private final static int bucket = 23006;

	public FarmManagerInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equals("bucket"))
		{
			final PcInventory inv = player.getInventory();
			if(inv.getCountOf(bucket) == 0)
			{
				player.getInventory().addItem(bucket, 1);
				player.sendMessage("Вы получили пустое ведро!");
			}
			else
				showChatWindow(player, 0, null, true);
		}
		else if(command.equals("exchange"))
			MultiSellHolder.getInstance().SeparateAndSend(getNpcId(), player, getObjectId(), 0);
		else
			super.onBypassFeedback(player, command);
	}

	@Override
	public void showChatWindow(Player player, int val, Object... arg)
	{
		HtmlMessage html = new HtmlMessage(this);
		html.setFile("farm/manager.htm");
		player.sendPacket(html);
		player.sendActionFailed();
	}
}