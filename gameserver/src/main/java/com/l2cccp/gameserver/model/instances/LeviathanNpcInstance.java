package com.l2cccp.gameserver.model.instances;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.data.xml.holder.LeviathanEventHolder;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public abstract class LeviathanNpcInstance extends NpcInstance
{
	protected static final Logger _log = LoggerFactory.getLogger(AdventurerInstance.class);
	protected final AbstractLeviathan event;
	protected final String folder;

	public LeviathanNpcInstance(final int objectId, final NpcTemplate template)
	{
		super(objectId, template);
		final int id = getParameter("event", -1);
		folder = getParameter("folder", "event");
		event = LeviathanEventHolder.getInstance().getEvent(id);
	}

	@Override
	public void onBypassFeedback(final Player player, final String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equalsIgnoreCase("register"))
			event.register(player);
		else
			super.onBypassFeedback(player, command);
	}

	public abstract AbstractLeviathan getEvent();
}
