package com.l2cccp.gameserver.model.promocode;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dom4j.Element;

import com.l2cccp.gameserver.dao.PremiumDAO;
import com.l2cccp.gameserver.data.xml.holder.PremiumHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.premium.PremiumAccount;
import com.l2cccp.gameserver.model.premium.PremiumStart;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.Log;
import com.l2cccp.gameserver.utils.TimeUtils;

public class PremiumPromoCodeReward extends PromoCodeReward
{
	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");
	public int _days;
	public int _id;

	public PremiumPromoCodeReward(Element element)
	{
		_days = Integer.parseInt(element.attributeValue("days"));
		_id = Integer.parseInt(element.attributeValue("id"));
	}

	@Override
	public void giveReward(Player player)
	{
		final PremiumAccount premium = PremiumHolder.getInstance().getPremium(_id);
		final long time = TimeUtils.addDay(_days);
		final long expire = player.getBonus().getExpire(premium) + time;

		PremiumStart.getInstance().start(player, premium.getId(), expire);
		PremiumDAO.getInstance().insert(player, premium.getId(), expire);

		String end = TIME_FORMAT.format(new Date(expire));

		player.broadcastPacket(new MagicSkillUse(player, player, 6463, 1, 0, 0));
		Log.service("Player " + player + " get Premium-Account from promocodes (id:" + premium.getId() + ", name: " + premium.getName() + ") at " + TimeUtils.formatTime((int) (time / 1000L), Language.ENGLISH) + ", End: " + end + ".", this.getClass().getName());
	}
}
