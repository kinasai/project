package com.l2cccp.gameserver.model.entity.rifts;

import java.util.List;

import com.l2cccp.gameserver.model.SimpleSpawner;
import com.l2cccp.gameserver.model.Territory;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class DimensionalRiftRoom
{
	private final Territory _territory;
	private final Location _teleportCoords;
	private final boolean _isBossRoom;
	private List<SimpleSpawner> _roomSpawns;

	public DimensionalRiftRoom(Territory territory, Location tele, boolean isBossRoom)
	{
		_territory = territory;
		_teleportCoords = tele;
		_isBossRoom = isBossRoom;
	}

	public Location getTeleportCoords()
	{
		return _teleportCoords;
	}

	public boolean checkIfInZone(Location loc)
	{
		return checkIfInZone(loc.x, loc.y, loc.z);
	}

	public boolean checkIfInZone(int x, int y, int z)
	{
		return _territory.isInside(x, y, z);
	}

	public boolean isBossRoom()
	{
		return _isBossRoom;
	}

	public List<SimpleSpawner> getSpawns()
	{
		return _roomSpawns;
	}

	public void setSpawns(List<SimpleSpawner> spawns)
	{
		_roomSpawns = spawns;
	}
}