package com.l2cccp.gameserver.model.actor.instances.player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.data.xml.holder.ExperienceHolder;
import com.l2cccp.gameserver.data.xml.holder.RateHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.rate.RateSetting;
import com.l2cccp.gameserver.model.rate.RateType;
import com.l2cccp.gameserver.templates.StatsSet;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Rate
{
	private static final Logger _log = LoggerFactory.getLogger(Rate.class);
	private final Player owner;
	private int next;
	private int start;
	private double exp;
	private double sp;
	private double adena;
	private double items;
	private double spoil;
	private double common;
	private double raid;
	private double epic;
	private double siege;
	private double fame;
	private RateSetting rate;

	public Rate(final Player owner, final int start)
	{
		this.owner = owner;
		this.start = start;
	}

	public void update()
	{
		final int my = owner.getLevel();
		final boolean base = owner.getActiveClass().isBase();
		final int base_max = ExperienceHolder.getInstance().getMaxLevel();
		final int sub_max = ExperienceHolder.getInstance().getMaxSubLevel();
		if(rate != null && (base ? base_max : sub_max) == my && next >= base_max) // Если мы уже указали рейт и задали последний рейт то обновлять уже нету смысла.
			return;

		if(my == start || rate == null)
		{
			rate = RateHolder.getInstance().get(start);
			next = rate.getNext();
			updateRate(rate.doAnnounce(), rate.get());
			return;
		}
		else if(my == next)
		{
			rate = RateHolder.getInstance().get(next);
			start = rate.getStart();
			next = rate.getNext();
			updateRate(rate.doAnnounce(), rate.get());
			return;
		}

		while(my > next || my < start) // Корректировка рейтов
		{
			rate = RateHolder.getInstance().find(my);
			if(rate != null)
			{
				start = rate.getStart();
				next = rate.getNext();
				if(my < next)
					updateRate(rate.doAnnounce(), rate.get());
			}
			else
				_log.error("Can't find rate for character " + owner.getName() + ", current level " + my + ", rate setting [" + start + ":" + next + "]");
		}

		//		_log.info("Update rate for player " + owner.getName() + ", started rate " + start);
	}

	private void updateRate(final boolean announce, final StatsSet rate)
	{
		setExp(rate.getDouble(RateType.EXP.name(), 1.0D));
		setSp(rate.getDouble(RateType.SP.name(), 1.0D));
		setAdena(rate.getDouble(RateType.ADENA.name(), 1.0D));
		setItems(rate.getDouble(RateType.ITEMS.name(), 1.0D));
		setSpoil(rate.getDouble(RateType.SPOIL.name(), 1.0D));
		setCommon(rate.getDouble(RateType.COMMON.name(), 1.0D));
		setRaid(rate.getDouble(RateType.RAID.name(), 1.0D));
		setEpic(rate.getDouble(RateType.EPIC.name(), 1.0D));
		setSiege(rate.getDouble(RateType.SIEGE.name(), 1.0D));
		setFame(rate.getDouble(RateType.FAME.name(), 1.0D));

		if(announce)
		{
			//			owner.sendMessage("Rate has been changed!!! Next rate is changed on " + next + " level!");
			//			for(RateType type : RateType.values())
			//			{
			//				final String key = type.name();
			//				owner.sendMessage(key + " rate: " + rate.getDouble(key));
			//			}
		}
	}

	public Player getOwner()
	{
		return owner;
	}

	public int getStart()
	{
		return start;
	}

	public int getNext()
	{
		return next;
	}

	public double getExp()
	{
		return exp;
	}

	public void setExp(final double exp)
	{
		this.exp = exp;
	}

	public double getSp()
	{
		return sp;
	}

	public void setSp(final double sp)
	{
		this.sp = sp;
	}

	public double getAdena()
	{
		return adena;
	}

	public void setAdena(final double adena)
	{
		this.adena = adena;
	}

	public double getItems()
	{
		return items;
	}

	public void setItems(final double items)
	{
		this.items = items;
	}

	public double getSpoil()
	{
		return spoil;
	}

	public void setSpoil(final double spoil)
	{
		this.spoil = spoil;
	}

	public double getCommon()
	{
		return common;
	}

	public void setCommon(final double common)
	{
		this.common = common;
	}

	public double getRaid()
	{
		return raid;
	}

	public void setRaid(final double raid)
	{
		this.raid = raid;
	}

	public double getEpic()
	{
		return epic;
	}

	public void setEpic(final double epic)
	{
		this.epic = epic;
	}

	public double getSiege()
	{
		return siege;
	}

	public void setSiege(final double siege)
	{
		this.siege = siege;
	}

	public double getFame()
	{
		return fame;
	}

	public void setFame(final double fame)
	{
		this.fame = fame;
	}
}