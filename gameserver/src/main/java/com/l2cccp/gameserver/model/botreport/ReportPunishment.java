package com.l2cccp.gameserver.model.botreport;

import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.utils.AdminFunctions;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ReportPunishment
{
	private final static ReportPunishment _instance = new ReportPunishment();

	public static ReportPunishment getInstance()
	{
		return _instance;
	}

	public void jail(Player player)
	{
		final int period = 60000;
		player.setVar("jailedFrom", player.getX() + ";" + player.getY() + ";" + player.getZ() + ";" + player.getReflectionId(), -1);
		player.setVar("jailed", period, -1);
		player.startUnjailTask(player, period);
		player.teleToLocation(Location.findPointToStay(player, AdminFunctions.JAIL_SPAWN, 50, 200), ReflectionManager.JAIL);
		player.sitDown(null);
		player.block();
		player.sendMessage("You moved to jail, reason: using third-party software.");
	}
}
