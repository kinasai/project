package com.l2cccp.gameserver.model.farm;

import java.util.concurrent.Future;

import com.l2cccp.commons.geometry.Circle;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.listener.zone.OnZoneEnterLeaveListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Territory;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.items.PcInventory;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.templates.ZoneTemplate;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FarmZone
{
	private final static int full = 23013;
	private final static int empty = 23006;
	private static FarmZone _instance;
	private Zone well;
	private Future<?> _timer;

	public static FarmZone getInstance()
	{
		if(_instance == null)
			_instance = new FarmZone();

		return _instance;
	}

	public void init()
	{
		Circle c = new Circle(new Location(17525, 170193, -3504), 125);
		c.setZmax(World.MAP_MAX_Z);
		c.setZmin(World.MAP_MIN_Z);

		StatsSet set = new StatsSet();
		set.set("name", "Well");
		set.set("type", Zone.ZoneType.peace_zone); // Ссанина не активируеться по верх боевой зоны
		set.set("territory", new Territory().add(c));

		well = new Zone(new ZoneTemplate(set));
		well.setReflection(ReflectionManager.DEFAULT);
		well.addListener(new ZoneListener());
		well.setActive(true);
	}

	private class Executor extends RunnableImpl
	{
		@Override
		protected void runImpl() throws Exception
		{
			for(final Player player : well.getInsidePlayers())
			{
				final PcInventory inv = player.getInventory();
				if(inv.getCountOf(full) >= 30)
				{
					player.sendMessage("Вы набрали максимальное количество воды!");
					continue;
				}
				else if(inv.getCountOf(full) < 1 && inv.getCountOf(empty) < 1)
				{
					player.sendMessage("Для начала, возьмите пустое ведро у менеджера фермы!");
					continue;
				}

				if(Util.getPay(player, empty, 1, false))
					player.sendMessage("Вы наполнили пустое ведро водой!");
				else
					player.sendMessage("Вы набрали ведро воды!");

				inv.addItem(full, 1);
			}
		}
	}

	private class ZoneListener implements OnZoneEnterLeaveListener
	{
		@Override
		public void onZoneEnter(Zone zone, Creature actor)
		{
			if(!actor.isPlayer())
				return;

			if(_timer == null)
			{
				final long val = TimeUtils.addSecond(10);
				_timer = ThreadPoolManager.getInstance().scheduleAtFixedRate(new Executor(), val, val);
			}
		}

		@Override
		public void onZoneLeave(Zone zone, Creature actor)
		{
			if(!actor.isPlayer())
				return;

			if(zone.getInsidePlayables().size() == 0 && _timer != null)
			{
				_timer.cancel(false);
				_timer = null;
			}
		}
	}
}