package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ItemAuctionDAO
{
	private static final Logger _log = LoggerFactory.getLogger(ItemAuctionDAO.class);
	private static final ItemAuctionDAO _instance = new ItemAuctionDAO();

	private static final String SELECT_SQL_QUERY = "SELECT auctionId FROM item_auction ORDER BY auctionId DESC LIMIT 0, 1";
	private static final String DELETE_SQL_QUERY = "DELETE FROM item_auction WHERE auctionId=?";
	private static final String DELETE_BID_SQL_QUERY = "DELETE FROM item_auction_bid WHERE auctionId=?";

	public static ItemAuctionDAO getInstance()
	{
		return _instance;
	}

	public AtomicInteger next()
	{
		AtomicInteger _next = new AtomicInteger();
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(SELECT_SQL_QUERY);
			rset = statement.executeQuery();
			if(rset.next())
				_next.set(rset.getInt(1));
		}
		catch(SQLException e)
		{
			_log.error("ItemAuctionDAO.next(): Failed loading auctions.", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return _next;
	}

	public void delete(int id)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(DELETE_SQL_QUERY);
			statement.setInt(1, id);
			statement.execute();
			statement.close();

			statement = con.prepareStatement(DELETE_BID_SQL_QUERY);
			statement.setInt(1, id);
			statement.execute();
			statement.close();
		}
		catch(SQLException e)
		{
			_log.error("ItemAuctionDAO.delete(int): Failed deleting auction: " + id, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}