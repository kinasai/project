package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.bbs.buffer.Scheme;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CharacterBuffsDAO
{
	private static final Logger _log = LoggerFactory.getLogger(CharacterBuffsDAO.class);

	public static final String SELECT_SQL_QUERY = "SELECT name, buffs FROM bbs_buffer WHERE player_id=?";
	public static final String INSERT_SQL_QUERY = "INSERT INTO bbs_buffer (name, player_id, buffs) VALUES (?,?,?)";
	public static final String DELETE_SQL_QUERY = "DELETE FROM bbs_buffer WHERE player_id=? AND name=?";

	private static final CharacterBuffsDAO _instance = new CharacterBuffsDAO();

	public static CharacterBuffsDAO getInstance()
	{
		return _instance;
	}

	public void load(Player player)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(SELECT_SQL_QUERY);
			statement.setInt(1, player.getObjectId());
			rset = statement.executeQuery();

			while(rset.next())
			{
				String buffs = rset.getString("buffs");
				String name = rset.getString("name");
				if(!buffs.isEmpty())
				{
					Scheme scheme = new Scheme(name);
					for(String str : buffs.split(";"))
					{
						String[] arrayOfString2 = str.split(",");
						int id = Integer.parseInt(arrayOfString2[0]);
						int lvl = Integer.parseInt(arrayOfString2[1]);
						scheme.addBuff(id, lvl);
					}

					if(scheme.getBuffs().size() > 1)
						player.setScheme(name, scheme);
				}
				else
				{
					player.sendMessage("Schema " + name + " has been removed as it doesn't have buff!");
					delete(player, name);
				}
			}
		}
		catch(Exception e)
		{
			_log.error("CharacterBuffsDAO:load(Player): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void insert(Player player, String buffs, String name)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(INSERT_SQL_QUERY);
			statement.setString(1, name);
			statement.setInt(2, player.getObjectId());
			statement.setString(3, buffs);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.warn("CharacterBuffsDAO:insert(Player,String,String): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void delete(Player player, String name)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(DELETE_SQL_QUERY);
			statement.setInt(1, player.getObjectId());
			statement.setString(2, name);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.warn("CharacterBuffsDAO:delete(Player,String): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

}
