package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.skills.EffectType;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.skills.effects.EffectTemplate;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.SqlBatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author VISTALL
 * @date 13:01/02.02.2011
 */
public class CharacterEffectDAO
{
	private static final Logger _log = LoggerFactory.getLogger(CharacterEffectDAO.class);
	private static final CharacterEffectDAO _instance = new CharacterEffectDAO();

	CharacterEffectDAO()
	{
		//
	}

	public static CharacterEffectDAO getInstance()
	{
		return _instance;
	}

	public void select(Player player)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT `skill_id`,`skill_level`,`effect_count`,`effect_cur_time`,`duration` FROM `character_effects` WHERE `object_id`=? AND `class_index`=? ORDER BY `order` ASC");
			statement.setInt(1, player.getObjectId());
			statement.setInt(2, player.getActiveClassId());
			rset = statement.executeQuery();
			int size = rset.getFetchSize();
			while(rset.next())
			{
				int skillId = rset.getInt("skill_id");
				int skillLvl = rset.getInt("skill_level");
				int effectCount = rset.getInt("effect_count");
				long effectCurTime = rset.getLong("effect_cur_time");
				long duration = rset.getLong("duration");

				SkillEntry skill = SkillTable.getInstance().getSkillEntry(skillId, skillLvl);
				if(skill == null)
					continue;

				for(EffectTemplate et : skill.getTemplate().getEffectTemplates())
				{
					if(et == null)
						continue;
					Env env = new Env(player, player, skill);
					Effect effect = et.getEffect(env);
					if(effect == null || effect.isOneTime())
						continue;

					effect.setCount(effectCount);
					effect.setPeriod(effectCount == 1 ? duration - effectCurTime : duration);

					player.getEffectList().addEffect(effect);
					effect.fixStartTime(size--);
				}
			}

			DbUtils.closeQuietly(statement, rset);

			statement = con.prepareStatement("DELETE FROM character_effects WHERE object_id = ? AND class_index=?");
			statement.setInt(1, player.getObjectId());
			statement.setInt(2, player.getActiveClassId());
			statement.execute();
			DbUtils.close(statement);
		}
		catch(final Exception e)
		{
			_log.error("CharacterEffectDAO.select(Player): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con);
		}
	}

	public void delete(int objectId, int classIndex)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM character_effects WHERE object_id = ? AND class_index=?");
			statement.setInt(1, objectId);
			statement.setInt(2, classIndex);
			statement.execute();
		}
		catch(final Exception e)
		{
			_log.error("Could not delete effects active effects data!" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void insert(Player player, final boolean sub)
	{
		List<Effect> effects = player.getEffectList().getAllEffects();
		if(effects.isEmpty())
			return;

		Connection con = null;
		Statement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();

			int order = 0;
			SqlBatch b = new SqlBatch("INSERT IGNORE INTO `character_effects` (`object_id`,`skill_id`,`skill_level`,`effect_count`,`effect_cur_time`,`duration`,`order`,`class_index`) VALUES");

			StringBuilder sb;
			for(Effect effect : effects)
			{
				if(effect.isInUse())
				{
					final SkillEntry skill = effect.getSkill();
					final Skill template = skill.getTemplate();
					if(!template.isToggle())
					{
						final EffectType type = effect.getEffectType();
						if(type != EffectType.HealOverTime && type != EffectType.CombatPointHealOverTime)
						{
							final boolean save = ((sub && template.isStayOnSubclassChange()) || !sub) && effect.isSaveable();
							if(save)
							{
								sb = new StringBuilder("(");
								sb.append(player.getObjectId()).append(",");
								sb.append(skill.getId()).append(",");
								sb.append(skill.getLevel()).append(",");
								sb.append(effect.getCount()).append(",");
								sb.append(effect.getTime()).append(",");
								sb.append(effect.getPeriod()).append(",");
								sb.append(order).append(",");
								sb.append(player.getActiveClassId()).append(")");
								b.write(sb.toString());
							}

							while((effect = effect.getNext()) != null && save)
							{
								sb = new StringBuilder("(");
								sb.append(player.getObjectId()).append(",");
								sb.append(skill.getId()).append(",");
								sb.append(skill.getLevel()).append(",");
								sb.append(effect.getCount()).append(",");
								sb.append(effect.getTime()).append(",");
								sb.append(effect.getPeriod()).append(",");
								sb.append(order).append(",");
								sb.append(player.getActiveClassId()).append(")");
								b.write(sb.toString());
							}
							order++;
						}
					}
				}
			}

			if(!b.isEmpty())
				statement.executeUpdate(b.close());
		}
		catch(final Exception e)
		{
			_log.error("Could not store active effects data!", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
