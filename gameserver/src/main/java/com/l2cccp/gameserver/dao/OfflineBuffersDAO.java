package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.model.offlinebuffer.BufferData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OfflineBuffersDAO
{
	private static final Logger _log = LoggerFactory.getLogger(OfflineBuffersDAO.class);
	private static final OfflineBuffersDAO _instance = new OfflineBuffersDAO();

	public static OfflineBuffersDAO getInstance()
	{
		return _instance;
	}

	private static final String SELECT_SQL_QUERY = "SELECT * FROM offline_buffers WHERE charId > 0";
	private static final String REPLACE_SQL_QUERY = "REPLACE INTO offline_buffers VALUES (?,?,?)";
	private static final String DELETE_SQL_QUERY = "DELETE FROM offline_buffers WHERE charId=?";

	public void restore(Map<Integer, BufferData> map)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(SELECT_SQL_QUERY);
			rset = statement.executeQuery();

			while(rset.next())
			{
				BufferData buffer = new BufferData();
				buffer.setTitle(rset.getString("title"));
				buffer.setPrice(rset.getInt("price"));
				map.put(rset.getInt("charId"), buffer);
			}
		}
		catch(Exception e)
		{
			_log.error("OfflineBuffersDAO.restore(map):" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void replace(int charId, BufferData buffer)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(REPLACE_SQL_QUERY);
			statement.setInt(1, charId);
			statement.setLong(2, buffer.getBuffPrice());
			statement.setString(3, buffer.getSaleTitle());
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("OfflineBuffersDAO.replace(int, buffer):" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void delete(int charId)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(DELETE_SQL_QUERY);
			statement.setInt(1, charId);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("OfflineBuffersDAO.delete(int):" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
