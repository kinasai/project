package com.l2cccp.gameserver.dao;

import gnu.trove.map.hash.TObjectIntHashMap;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;

public class PromoCodeLimitDAO
{
	public static final PromoCodeLimitDAO INSTANCE = new PromoCodeLimitDAO();

	private static final Logger _log = LoggerFactory.getLogger(PromoCodeLimitDAO.class);

	public TObjectIntHashMap<String> select()
	{
		TObjectIntHashMap<String> map = new TObjectIntHashMap<String>();
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM promocode_limits");
			rset = statement.executeQuery();
			while(rset.next())
			{
				map.put(rset.getString("code"), rset.getInt("count"));
			}
		}
		catch(Exception e)
		{
			_log.error(e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return map;
	}

	public void insert(String code, int count)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("REPLACE INTO promocode_limits (code, count) VALUES(?,?)");
			statement.setString(1, code);
			statement.setInt(2, count);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error(e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
