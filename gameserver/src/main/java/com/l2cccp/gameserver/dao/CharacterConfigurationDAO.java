package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.model.actor.instances.player.config.PlayerConfig;
import com.l2cccp.gameserver.model.actor.instances.player.config.ConfigOption;

/**
 * 
 * @author L2CCCP
 * {@link} http://l2cccp.com/
 */
public class CharacterConfigurationDAO
{
	private static final Logger _log = LoggerFactory.getLogger(CharacterConfigurationDAO.class);
	private static final CharacterConfigurationDAO _instance = new CharacterConfigurationDAO();

	private static final String SET = "REPLACE INTO character_configs (obj_id, nocarrier, buffdist, translit, autoloot, autolootherbs, expoff, hidetraders, classicshift, rain, imgindrop, hidevisual) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String GET = "SELECT * FROM character_configs WHERE obj_id = ? LIMIT 1";
	private static final String DELETE = "DELETE FROM character_configs WHERE obj_id = ? LIMIT 1";

	public static CharacterConfigurationDAO getInstance()
	{
		return _instance;
	}

	public void saveConfig(PlayerConfig config)
	{
		if(config == null) // Выбивает нпе при создании чара! Но после входа в игру создаем конфиги!
			return;

		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(SET);
			statement.setInt(1, config.getObj());
			statement.setInt(2, config.getNoCarrier());
			statement.setInt(3, config.getBuffDistance());
			statement.setString(4, config.getTranslit());
			statement.setInt(5, config.getAutoLoot());
			statement.setBoolean(6, config.isAutoLootHerb());
			statement.setBoolean(7, config.isExpOff());
			statement.setBoolean(8, config.isHideTraders());
			statement.setBoolean(9, config.isClassicShift());
			statement.setBoolean(10, config.showRain());
			statement.setBoolean(11, config.showImages());
			statement.setBoolean(12, config.isHideVisual());
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("CharacterConfigurationDAO.saveConfig(PlayerConfig): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void deleteConfig(int objectId)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(DELETE);
			statement.setInt(1, objectId);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("CharacterConfigurationDAO.deleteConfig(int): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public PlayerConfig loadConfig(int obj)
	{
		PlayerConfig config = null;
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(GET);
			statement.setInt(1, obj);
			rs = statement.executeQuery();
			while(rs.next())
			{
				config = new PlayerConfig(rs.getInt("obj_Id"));
				config.setNoCarrier(rs.getInt("nocarrier"));
				config.setBuffDistance(rs.getInt("buffdist"));
				config.setTranslit(rs.getString("translit"));
				config.setAutoLoot(rs.getInt("autoloot"));
				config.setAutoLootHerb(rs.getBoolean("autolootherbs"));
				config.setExpOff(rs.getBoolean("expoff"));
				config.setHideTraders(rs.getBoolean("hidetraders"));
				config.setClassicShift(rs.getBoolean("classicshift"));
				config.setRain(rs.getBoolean("rain"));
				config.setImages(rs.getBoolean("imgindrop"));
				config.setHideVisual(rs.getBoolean("hidevisual"));
				ConfigOption.add(config);
			}
		}
		catch(Exception e)
		{
			_log.error("CharacterConfigurationDAO.loadConfig(int): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rs);
		}

		if(config == null)
			config = ConfigOption.create(obj);

		return config;
	}
}