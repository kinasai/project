package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.handler.bbs.Academicians;
import com.l2cccp.gameserver.handler.bbs.AcademiciansStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AcademiciansDAO
{
	private static final Logger _log = LoggerFactory.getLogger(AcademiciansDAO.class);
	private static final AcademiciansDAO _instance = new AcademiciansDAO();

	private static final String SELECT_SQL_QUERY = "SELECT * FROM academicians";
	private static final String INSERT_SQL_QUERY = "INSERT INTO academicians (objId, clanId, end_time) VALUES (?,?,?)";
	private static final String DELETE_SQL_QUERY = "DELETE FROM academicians WHERE objId=?";


	public static AcademiciansDAO getInstance()
	{
		return _instance;
	}

	public void load()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(SELECT_SQL_QUERY);
			rset = statement.executeQuery();
			while(rset.next())
			{
				int objId = rset.getInt("objId");
				int clanId = rset.getInt("clanId");
				long end_time = rset.getLong("end_time");
				new Academicians(end_time, objId, clanId);
			}
		}
		catch(Exception e)
		{
			_log.error("AcademiciansDAO.load():" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void insert(Academicians academic)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(INSERT_SQL_QUERY);
			statement.setInt(1, academic.getObjId());
			statement.setInt(2, academic.getClanId());
			statement.setLong(3, academic.getTime());
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("AcademiciansDAO.insert(Academicians):" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void delete(Academicians academic)
	{

		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(DELETE_SQL_QUERY);
			statement.setInt(1, academic.getObjId());
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("AcademiciansDAO.delete(Academicians):" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
