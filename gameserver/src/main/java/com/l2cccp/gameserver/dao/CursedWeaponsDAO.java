package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.data.xml.holder.CursedWeaponsHolder;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.instancemanager.CursedWeaponsManager;
import com.l2cccp.gameserver.model.CursedWeapon;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CursedWeaponsDAO
{
	private static final Logger _log = LoggerFactory.getLogger(CursedWeaponsDAO.class);
	private static final CursedWeaponsDAO _instance = new CursedWeaponsDAO();

	private static final String SELECT_SQL_QUERY = "SELECT * FROM cursed_weapons";
	private static final String DELETE_SQL_QUERY = "DELETE FROM cursed_weapons WHERE item_id = ?";
	private static final String DELETE_SKILLS_SQL_QUERY = "DELETE FROM character_skills WHERE skill_id = ?";
	private static final String DELETE_SKILLS2_SQL_QUERY = "DELETE FROM character_skills WHERE char_obj_id=? AND skill_id=?";
	private static final String SELECT_OWNER_SQL_QUERY = "SELECT owner_id FROM items WHERE item_id = ?";
	private static final String DELETE_CURSED_SQL_QUERY = "DELETE FROM items WHERE owner_id = ? AND item_id = ?";
	private static final String UPDATE_CHARACTER_SQL_QUERY = "UPDATE characters SET karma = ?, pkkills = ? WHERE obj_id = ?";
	private static final String REPLACE_CURSED_SQL_QUERY = "REPLACE INTO cursed_weapons (item_id, player_id, player_karma, player_pkkills, nb_kills, x, y, z, end_time) VALUES (?,?,?,?,?,?,?,?,?)";

	public static CursedWeaponsDAO getInstance()
	{
		return _instance;
	}

	public void restore()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(SELECT_SQL_QUERY);
			rset = statement.executeQuery();

			while(rset.next())
			{
				int itemId = rset.getInt("item_id");
				CursedWeapon cw = CursedWeaponsHolder.getInstance().get(itemId);
				if(cw != null)
				{
					cw.setPlayerId(rset.getInt("player_id"));
					cw.setPlayerKarma(rset.getInt("player_karma"));
					cw.setPlayerPkKills(rset.getInt("player_pkkills"));
					cw.setNbKills(rset.getInt("nb_kills"));
					cw.setLoc(new Location(rset.getInt("x"), rset.getInt("y"), rset.getInt("z")));
					cw.setEndTime(rset.getLong("end_time") * 1000L);

					if(!cw.reActivate())
						CursedWeaponsManager.getInstance().endOfLife(cw);
				}
				else
				{
					remove(itemId);
					_log.warn("CursedWeaponsDAO.restore(): Unknown cursed weapon " + itemId + ", deleted");
				}
			}
		}
		catch(Exception e)
		{
			_log.warn("CursedWeaponsDAO.restore(): Could not restore cursed_weapons data: " + e);
			_log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void remove(int id)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement(DELETE_SQL_QUERY);
			statement.setInt(1, id);
			statement.executeUpdate();

			CursedWeapon cursed = CursedWeaponsHolder.getInstance().get(id);

			if(cursed != null)
				cursed.initWeapon();
		}
		catch(SQLException e)
		{
			_log.error("CursedWeaponsDAO.remove(int): Failed to remove data: " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void check()
	{
		Connection con = null;
		PreparedStatement statement1 = null, statement2 = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement1 = con.prepareStatement(DELETE_SKILLS_SQL_QUERY);
			statement2 = con.prepareStatement(SELECT_OWNER_SQL_QUERY);

			for(CursedWeapon cursed : CursedWeaponsHolder.getInstance().getCursed())
			{
				int itemId = cursed.getItemId();
				// Do an item check to be sure that the cursed weapon and/or skill isn't hold by someone
				int skillId = cursed.getSkillId();
				boolean foundedInItems = false;

				// Delete all cursed weapons skills (we don`t care about same skill on multiply weapons, when player back, skill will appears again)
				statement1.setInt(1, skillId);
				statement1.executeUpdate();

				statement2.setInt(1, itemId);
				rset = statement2.executeQuery();

				while(rset.next())
				{
					int playerId = rset.getInt("owner_id"); // A player has the cursed weapon in his inventory ...

					if(!foundedInItems)
					{
						if(playerId != cursed.getPlayerId() || cursed.getPlayerId() == 0)
						{
							clean(playerId, itemId, cursed);
							_log.info("CursedWeaponsDAO.check(): Player " + playerId + " owns the cursed weapon " + itemId + " but he shouldn't.");
						}
						else
							foundedInItems = true;
					}
					else
					{
						clean(playerId, itemId, cursed);
						_log.info("CursedWeaponsDAO.check(): Player " + playerId + " owns the cursed weapon " + itemId + " but he shouldn't.");
					}
				}

				if(!foundedInItems && cursed.getPlayerId() != 0)
				{
					CursedWeaponsDAO.getInstance().remove(cursed.getItemId());

					_log.info("CursedWeaponsDAO.check(): Unownered weapon, removing from table...");
				}
			}
		}
		catch(Exception e)
		{
			_log.warn("CursedWeaponsDAO.check(): Could not check cursed_weapons data: " + e);
			return;
		}
		finally
		{
			DbUtils.closeQuietly(statement1);
			DbUtils.closeQuietly(con, statement2, rset);
		}
	}

	public void save(CursedWeapon cursed)
	{
		Connection con = null;
		PreparedStatement statement = null;
		synchronized (cursed)//FIXME [G1ta0] зачем синхронизация если она только на сохранении
		{
			try
			{
				con = DatabaseFactory.getInstance().getConnection();

				// Delete previous datas
				statement = con.prepareStatement(DELETE_SQL_QUERY);
				statement.setInt(1, cursed.getItemId());
				statement.executeUpdate();

				if(cursed.isActive())
				{
					DbUtils.close(statement);
					statement = con.prepareStatement(REPLACE_CURSED_SQL_QUERY);
					statement.setInt(1, cursed.getItemId());
					statement.setInt(2, cursed.getPlayerId());
					statement.setInt(3, cursed.getPlayerKarma());
					statement.setInt(4, cursed.getPlayerPkKills());
					statement.setInt(5, cursed.getNbKills());
					statement.setInt(6, cursed.getLoc().x);
					statement.setInt(7, cursed.getLoc().y);
					statement.setInt(8, cursed.getLoc().z);
					statement.setLong(9, cursed.getEndTime() / 1000);
					statement.executeUpdate();
				}
			}
			catch(SQLException e)
			{
				_log.error("CursedWeaponsDAO.save(): Failed to save data: " + e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement);
			}
		}
	}

	private void clean(int playerId, int itemId, CursedWeapon cursed)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();

			// Delete the item
			statement = con.prepareStatement(DELETE_CURSED_SQL_QUERY);
			statement.setInt(1, playerId);
			statement.setInt(2, itemId);
			statement.executeUpdate();
			DbUtils.close(statement);

			statement = con.prepareStatement(UPDATE_CHARACTER_SQL_QUERY);
			statement.setInt(1, cursed.getPlayerKarma());
			statement.setInt(2, cursed.getPlayerPkKills());
			statement.setInt(3, playerId);
			if(statement.executeUpdate() != 1)
				_log.warn("CursedWeaponsDAO.clean(): Error while updating karma & pkkills for userId " + cursed.getPlayerId());

			CursedWeaponsDAO.getInstance().remove(itemId); // clean up the cursedweapons table.
		}
		catch(SQLException e)
		{
			_log.error("CursedWeaponsDAO.clean():", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void remove(CursedWeapon cursed)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();

			// Delete the item
			statement = con.prepareStatement(DELETE_CURSED_SQL_QUERY);
			statement.setInt(1, cursed.getPlayerId());
			statement.setInt(2, cursed.getItemId());
			statement.executeUpdate();
			DbUtils.close(statement);

			// Delete the skill
			statement = con.prepareStatement(DELETE_SKILLS2_SQL_QUERY);
			statement.setInt(1, cursed.getPlayerId());
			statement.setInt(2, cursed.getSkillId());
			statement.executeUpdate();
			DbUtils.close(statement);

			// Restore the karma
			statement = con.prepareStatement(UPDATE_CHARACTER_SQL_QUERY);
			statement.setInt(1, cursed.getPlayerKarma());
			statement.setInt(2, cursed.getPlayerPkKills());
			statement.setInt(3, cursed.getPlayerId());
			statement.executeUpdate();
		}
		catch(SQLException e)
		{
			_log.warn("CursedWeaponsDAO.remove(): Could not delete : " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}