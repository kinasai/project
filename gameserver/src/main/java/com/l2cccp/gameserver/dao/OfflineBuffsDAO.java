package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.offlinebuffer.BufferData;
import com.l2cccp.gameserver.skills.SkillEntry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OfflineBuffsDAO
{
	private static final Logger _log = LoggerFactory.getLogger(OfflineBuffsDAO.class);
	private static final OfflineBuffsDAO _instance = new OfflineBuffsDAO();

	public static OfflineBuffsDAO getInstance()
	{
		return _instance;
	}

	private static final String SELECT_SQL_QUERY = "SELECT * FROM offline_buffs WHERE charId = ?";
	private static final String REPLACE_SQL_QUERY = "REPLACE INTO offline_buffs VALUES (?,?)";
	private static final String DELETE_SQL_QUERY = "DELETE FROM offline_buffs WHERE charId=?";

	public BufferData restore(Player player, BufferData buffer)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		buffer.setPlayer(player);

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(SELECT_SQL_QUERY);
			statement.setInt(1, player.getObjectId());
			ResultSet skills = statement.executeQuery();
			try
			{
				if(skills.next())
				{
					final String[] skillIds = skills.getString("skillIds").split(",");
					for(String skillId : skillIds)
					{
						final SkillEntry skill = player.getKnownSkill(Integer.parseInt(skillId));
						if(skill == null)
							continue;

						buffer.getBuffs().put(skill.getId(), skill);
					}
				}
			}
			catch(Exception e)
			{
				_log.error("OfflineBuffsDAO.restore():" + e, e);
			}
		}
		catch(Exception e)
		{
			_log.error("OfflineBuffsDAO.restore():" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return buffer;
	}

	public void delete(int charId)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(DELETE_SQL_QUERY);
			statement.setInt(1, charId);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("OfflineBuffersDAO.delete(int):" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void replace(int charId, String buffs)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(REPLACE_SQL_QUERY);
			statement.setInt(1, charId);
			statement.setString(2, buffs);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("OfflineBuffersDAO.insert(int):" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
