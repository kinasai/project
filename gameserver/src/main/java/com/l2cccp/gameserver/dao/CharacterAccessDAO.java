package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.model.CharSelectInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author VISTALL
 * @date 2:47/09.09.2011
 */
public class CharacterAccessDAO
{
	private static final Logger _log = LoggerFactory.getLogger(CharacterAccessDAO.class);

	private static final CharacterAccessDAO _instance = new CharacterAccessDAO();

	public static CharacterAccessDAO getInstance()
	{
		return _instance;
	}

	public void select(CharSelectInfo info)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM character_access WHERE object_id=?");
			statement.setInt(1, info.getObjectId());
			rset = statement.executeQuery();
			if(rset.next())
			{
				info.setPasswordEnable(rset.getInt("password_enable") == 1);
				info.setPassword(rset.getString("password"));
			}
		}
		catch(Exception e)
		{
			_log.error("CharacterAccessDAO.select(CharSelectInfoPackage): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void update(int objectId, String password)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();

			statement = con.prepareStatement("SELECT * FROM character_access WHERE object_id=?");
			statement.setInt(1, objectId);
			rset = statement.executeQuery();
			boolean is = rset.next();

			statement = con.prepareStatement(!is ? "INSERT INTO character_access (password, object_id, password_enable) VALUES(?,?,0)" : "UPDATE character_access SET password=? WHERE object_id=?");
			statement.setString(1, password);
			statement.setInt(2, objectId);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("CharacterAccessDAO.update(int,String): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void enable(int objectId, int enable)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("UPDATE character_access SET password_enable=? WHERE object_id=?");
			statement.setInt(1, enable);
			statement.setInt(2, objectId);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("CharacterAccessDAO.enable(int): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
