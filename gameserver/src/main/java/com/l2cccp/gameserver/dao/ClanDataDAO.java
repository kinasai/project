package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.model.entity.residence.Castle;
import com.l2cccp.gameserver.model.entity.residence.ClanHall;
import com.l2cccp.gameserver.model.entity.residence.Fortress;
import com.l2cccp.gameserver.model.entity.residence.Residence;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.tables.ClanTable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author VISTALL
 * @date 19:23/15.04.2011
 */
public class ClanDataDAO
{
	private static final Logger _log = LoggerFactory.getLogger(ClanDataDAO.class);
	private static final ClanDataDAO _instance = new ClanDataDAO();

	public static final String SELECT_CASTLE_OWNER = "SELECT clan_id FROM clan_data WHERE hasCastle = ? LIMIT 1";
	public static final String SELECT_FORTRESS_OWNER = "SELECT clan_id FROM clan_data WHERE hasFortress = ? LIMIT 1";
	public static final String SELECT_CLANHALL_OWNER = "SELECT clan_id FROM clan_data WHERE hasHideout = ? LIMIT 1";
	public static final String UPDATE_CLAN_DESCRIPTION = "UPDATE clan_description SET description=? WHERE clan_id=?";
	public static final String INSERT_CLAN_DESCRIPTION = "INSERT INTO clan_description (clan_id, description) VALUES (?, ?)";

	public static ClanDataDAO getInstance()
	{
		return _instance;
	}

	public Clan getOwner(Castle c)
	{
		return getOwner(c, SELECT_CASTLE_OWNER);
	}

	public Clan getOwner(Fortress f)
	{
		return getOwner(f, SELECT_FORTRESS_OWNER);
	}

	public Clan getOwner(ClanHall c)
	{
		return getOwner(c, SELECT_CLANHALL_OWNER);
	}

	private Clan getOwner(Residence residence, String sql)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(sql);
			statement.setInt(1, residence.getId());
			rset = statement.executeQuery();
			if(rset.next())
				return ClanTable.getInstance().getClan(rset.getInt("clan_id"));
		}
		catch(Exception e)
		{
			_log.error("ClanDataDAO.getOwner(Residence, String)", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return null;
	}

	public void updateDescription(int id, String description)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(UPDATE_CLAN_DESCRIPTION);
			statement.setString(1, description);
			statement.setInt(2, id);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("ClanDataDAO.updateDescription(int, String)", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void insertDescription(int id, String description)
	{

		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(INSERT_CLAN_DESCRIPTION);
			statement.setInt(1, id);
			statement.setString(2, description);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("ClanDataDAO.updateDescription(int, String)", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
