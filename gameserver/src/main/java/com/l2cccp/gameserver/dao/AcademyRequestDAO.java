package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.handler.bbs.AcademyRequest;
import com.l2cccp.gameserver.handler.bbs.AcademyStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AcademyRequestDAO
{
	private static final Logger _log = LoggerFactory.getLogger(AcademyRequestDAO.class);
	private static final AcademyRequestDAO _instance = new AcademyRequestDAO();

	private static final String LOAD_SQL_QUERY = "SELECT * FROM academy_request";
	private static final String UPDATE_SQL_QUERY = "UPDATE academy_request SET seats=? WHERE clanId=?";
	private static final String INSERT_SQL_QUERY = "INSERT INTO academy_request (time, clanId, seats, price, item) VALUES (?,?,?,?,?)";
	private static final String DELETE_SQL_QUERY = "DELETE FROM academy_request WHERE clanId=?";

	public static AcademyRequestDAO getInstance()
	{
		return _instance;
	}

	public void load()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(LOAD_SQL_QUERY);
			rset = statement.executeQuery();
			while(rset.next())
			{
				int time = rset.getInt("time");
				int clanId = rset.getInt("clanId");
				int seats = rset.getInt("seats");
				int item = rset.getInt("item");
				long price = rset.getLong("price");
				new AcademyRequest(time, clanId, seats, price, item);
			}
		}
		catch(Exception e)
		{
			_log.error("AcademyRequestDAO.load():" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void update(AcademyRequest request)
	{
		AcademyStorage.getInstance().updateList();
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(UPDATE_SQL_QUERY);
			statement.setInt(1, request.getSeats());
			statement.setInt(2, request.getClanId());
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("AcademyRequestDAO.update(AcademyRequest):" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void insert(AcademyRequest request)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(INSERT_SQL_QUERY);
			statement.setInt(1, request.getTime());
			statement.setInt(2, request.getClanId());
			statement.setInt(3, request.getSeats());
			statement.setLong(4, request.getPrice());
			statement.setLong(5, request.getItem());
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("AcademyRequestDAO.insert(AcademyRequest):" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void delete(int id)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(DELETE_SQL_QUERY);
			statement.setInt(1, id);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error("AcademyRequestDAO.delete(id):" + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
