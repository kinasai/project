package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PromoCodeDAO
{
	public static final PromoCodeDAO INSTANCE = new PromoCodeDAO();

	private static final Logger _log = LoggerFactory.getLogger(PromoCodeDAO.class);

	public boolean isActivated(String accountName, String code)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT code FROM promocodes WHERE account_name=? AND code=?");
			statement.setString(1, accountName);
			statement.setString(2, code);
			rset = statement.executeQuery();
			return rset.next();
		}
		catch(Exception e)
		{
			_log.error(e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return false;
	}

	public void insert(String accountName, String code)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("INSERT INTO promocodes (account_name,code) VALUES(?,?)");
			statement.setString(1, accountName);
			statement.setString(2, code);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.error(e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
