package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.database.DatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PromoCodeUserDAO
{
	public static final PromoCodeUserDAO INSTANCE = new PromoCodeUserDAO();

	private static final Logger _log = LoggerFactory.getLogger(PromoCodeUserDAO.class);

	public boolean isContains(String name, String code)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT code FROM promocode_users WHERE account_name=? AND code=?");
			statement.setString(1, name);
			statement.setString(2, code);
			rset = statement.executeQuery();
			return rset.next();
		}
		catch(Exception e)
		{
			_log.error(e.getMessage(), e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return false;
	}
}
