package com.l2cccp.gameserver.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.premium.PremiumStart;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PremiumDAO
{
	private static final Logger _log = LoggerFactory.getLogger(PremiumDAO.class);
	private static final PremiumDAO _instance = new PremiumDAO();

	private static final String SELECT_SQL_QUERY = "SELECT id, expire FROM premiums WHERE owner=?";
	private static final String DELETE_SQL_QUERY = "DELETE FROM premiums WHERE owner=? AND id=?";
	private static final String INSERT_SQL_QUERY = "REPLACE INTO premiums(owner, id, expire) VALUES (?,?,?)";

	public static PremiumDAO getInstance()
	{
		return _instance;
	}

	public void select(final Player player)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(SELECT_SQL_QUERY);
			if(Config.PREMIUM_ACCOUNT_TYPE == 1)
				statement.setInt(1, player.getObjectId());
			else
				statement.setString(1, player.getAccountName());
			rset = statement.executeQuery();
			while(rset.next())
			{
				final int id = rset.getInt("id");
				final long expire = rset.getLong("expire");
				if(expire >= System.currentTimeMillis())
					PremiumStart.getInstance().start(player, id, expire);
			}
		}
		catch(Exception e)
		{
			_log.info("PremiumDAO.select(String): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void delete(final Player player, final int id)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(DELETE_SQL_QUERY);
			if(Config.PREMIUM_ACCOUNT_TYPE == 1)
				statement.setInt(1, player.getObjectId());
			else
				statement.setString(1, player.getAccountName());
			statement.setInt(2, id);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.info("PremiumDAO.delete(Player,int): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	public void insert(final Player player, final int id, final long endTime)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(INSERT_SQL_QUERY);
			if(Config.PREMIUM_ACCOUNT_TYPE == 1)
				statement.setInt(1, player.getObjectId());
			else
				statement.setString(1, player.getAccountName());
			statement.setInt(2, id);
			statement.setLong(3, endTime);
			statement.execute();
		}
		catch(Exception e)
		{
			_log.info("PremiumDAO.insert(Player, int, long): " + e, e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}
}
