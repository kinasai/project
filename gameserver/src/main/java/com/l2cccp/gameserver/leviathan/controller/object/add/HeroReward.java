package com.l2cccp.gameserver.leviathan.controller.object.add;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.HeroRewardInfo;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.enums.Reward;
import com.l2cccp.gameserver.leviathan.enums.RewardType;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.utils.TimeUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class HeroReward extends Controller
{
	private final long time;
	private final boolean chat, items, skills;
	private final double chance;
	private final RewardType type;
	private final int position;

	public HeroReward(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		position = _params.getInteger("position", 0);
		time = TimeUtils.addHours(_params.getInteger("hours"));
		chat = _params.getBool("chat", false);
		items = _params.getBool("items", false);
		skills = _params.getBool("skills", false);
		chance = _params.getDouble("chance", 100D);
		type = RewardType.valueOf(_params.getString("type"));
	}

	@Override
	public void run()
	{
		final LeviathanTemplate template = _event.getTemplate();
		final HeroRewardInfo reward = new HeroRewardInfo(time, chat, items, skills, chance, Reward.HERO);
		if(position > 0)
			reward.setPosition(position);
		template.addReward(type, reward);
	}
}