package com.l2cccp.gameserver.leviathan.interfaces;

import com.l2cccp.gameserver.model.Player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public interface IReward
{
	int getPosition();

	void give(final Player player);
}