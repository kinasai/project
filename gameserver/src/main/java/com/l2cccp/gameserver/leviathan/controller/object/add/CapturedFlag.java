package com.l2cccp.gameserver.leviathan.controller.object.add;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.maps.Point;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.CapturedFlagData;
import com.l2cccp.gameserver.leviathan.type.FlagAndBase;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CapturedFlag extends Controller
{
	private final int id;
	private final TeamType team;

	public CapturedFlag(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		id = _params.getInteger("id");
		team = TeamType.valueOf(_params.getString("team", "NONE"));
	}

	@Override
	public void run()
	{
		final FlagAndBase event = (FlagAndBase) _event;
		for(final Point point : event.getMap().getPoints(100 + team.ordinal()))
		{
			final Location location = new Location(point.getX(), point.getY(), point.getZ());
			final CapturedFlagData flag = new CapturedFlagData(id, event, location, team);
			event.addFlag(flag);
		}
	}
}