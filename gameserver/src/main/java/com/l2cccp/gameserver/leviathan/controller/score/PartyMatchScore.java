package com.l2cccp.gameserver.leviathan.controller.score;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.leviathan.type.TeamVsTeam;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.network.l2.s2c.ExPVPMatchRecord;
import com.l2cccp.gameserver.network.l2.s2c.ExPVPMatchRecord.Member;
import com.l2cccp.gameserver.network.l2.s2c.ExPVPMatchUserDie;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PartyMatchScore extends Controller
{
	private final boolean winner;
	private final int type;

	public PartyMatchScore(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		winner = _params.getBool("winner", false);
		type = _params.getInteger("type", 1);
	}

	@Override
	public void run()
	{
		final TeamVsTeam event = (TeamVsTeam) _event;
		final int red = event.getKills(TeamType.RED), blue = event.getKills(TeamType.BLUE);
		final ExPVPMatchRecord packet = new ExPVPMatchRecord(type, winner ? event.getWinnerTeam() : TeamType.NONE, blue, red, getList(TeamType.BLUE), getList(TeamType.RED));
		final ExPVPMatchUserDie packet2 = type == ExPVPMatchRecord.UPDATE ? new ExPVPMatchUserDie(blue, red) : null;
		for(final Participant participant : event)
		{
			participant.sendPacket(packet);
			if(packet2 != null)
				participant.sendPacket(packet2);
		}
	}

	private List<Member> getList(final TeamType team)
	{
		final TeamVsTeam event = (TeamVsTeam) _event;
		final List<Member> list = new ArrayList<Member>();
		for(final Participant participant : event.getTeam(team.ordinal()))
		{
			final Player player = participant.get();
			if(player != null)
				list.add(new Member(player.getName(), participant.getKills(), participant.getDeath()));
		}

		return list;
	}
}
