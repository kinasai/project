package com.l2cccp.gameserver.leviathan.listeners.inventory;

import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.objects.CapturedFlagData;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.leviathan.type.FlagAndBase;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OnCapturedFlagEquip extends AbstractEquipListener
{
	private final String _equip;
	private final String _uneguip;
	private final ChatType _chat;

	public OnCapturedFlagEquip(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		_equip = _params.getString("equip");
		_uneguip = _params.getString("uneguip");
		_chat = ChatType.valueOf(_params.getString("chat"));
	}

	@Override
	public void onEquip(final int slot, final ItemInstance item, final Playable actor)
	{
		final FlagAndBase event = (FlagAndBase) _event;
		final TeamType team = actor.getTeam();
		final List<CapturedFlagData> flags = event.getFlags(team);
		if(flags == null || flags.isEmpty())
			return;

		final LeviathanTemplate template = event.getTemplate();
		for(final Participant object : event.getTeam(team.revert().ordinal()))
		{
			final Player player = object.get();
			final CustomMessage message = new CustomMessage(_equip);
			final Language language = player.getLanguage();
			player.sendPacket(new Say2(0, _chat, template.getName(language), message.toString(language), null));
		}
	}

	@Override
	public void onUnequip(final int slot, final ItemInstance item, final Playable actor)
	{
		final FlagAndBase event = (FlagAndBase) _event;
		final TeamType team = actor.getTeam();

		final List<CapturedFlagData> flags = event.getFlags(team);
		if(flags == null || flags.isEmpty())
			return;

		final LeviathanTemplate template = event.getTemplate();
		for(final Participant object : event.getTeam(team.revert().ordinal()))
		{
			final Player player = object.get();
			final CustomMessage message = new CustomMessage(_uneguip);
			final Language language = player.getLanguage();
			player.sendPacket(new Say2(0, _chat, template.getName(language), message.toString(language), null));
		}
	}
}