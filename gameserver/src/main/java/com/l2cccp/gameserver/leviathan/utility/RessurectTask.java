package com.l2cccp.gameserver.leviathan.utility;

import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.maps.Point;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RessurectTask extends RunnableImpl
{
	private final Player player;
	private final AbstractLeviathan event;

	public RessurectTask(final AbstractLeviathan event, final Player player)
	{
		this.event = event;
		this.player = player;
	}

	@Override
	public void runImpl()
	{
		final Point point = event.getPoint(player.getTeam().ordinal());
		final Location position = new Location(point.getX(), point.getY(), point.getZ());
		player.teleToLocation(position);
		player.doRevive();
	}
}