package com.l2cccp.gameserver.leviathan.objects;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.StringUtils;

import com.l2cccp.commons.geometry.Circle;
import com.l2cccp.commons.logging.LoggerObject;
import com.l2cccp.gameserver.leviathan.component.maps.Point;
import com.l2cccp.gameserver.leviathan.listeners.zone.OnCapturedBaseEnter;
import com.l2cccp.gameserver.leviathan.type.FlagAndBase;
import com.l2cccp.gameserver.model.Territory;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.templates.ZoneTemplate;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CapturedBaseData extends LoggerObject
{
	private final int id;
	private final int point;
	private final AtomicInteger flags;
	private final TeamType team;
	private final FlagAndBase event;
	private final int radius;
	private final String listener;
	private Zone zone;
	private NpcInstance base;

	public CapturedBaseData(final FlagAndBase event, final int id, final int point, final TeamType team, final String listener, final int radius)
	{
		this.id = id;
		this.point = point;
		this.event = event;
		this.team = team;
		this.radius = radius;
		this.flags = new AtomicInteger(0);
		this.listener = listener;
	}

	public void spawn()
	{
		final Point point = event.getMap().getPoints(200 + team.ordinal()).get(0);
		final Location location = new Location(point.getX(), point.getY(), point.getZ());
		base = NpcUtils.spawnSingle(id, location, event.getReflection());
		final Circle circle = new Circle(location, radius);
		circle.setZmax(World.MAP_MAX_Z);
		circle.setZmin(World.MAP_MIN_Z);

		StatsSet set = new StatsSet();
		set.set("name", StringUtils.EMPTY);
		set.set("type", Zone.ZoneType.dummy);
		set.set("territory", new Territory().add(circle));

		zone = new Zone(new ZoneTemplate(set));
		zone.setReflection(event.getReflection());
		final OnCapturedBaseEnter listen = (OnCapturedBaseEnter) event.getTemplate().getZoneListener(listener);
		listen.setBase(this);
		zone.addListener(listen);
		zone.setType(Zone.ZoneType.dummy);
		zone.setActive(true);
	}

	public void despawn()
	{
		flags.set(0);
		base.deleteMe();
		base = null;

		if(zone == null)
			return;

		zone.setActive(false);
		zone = null;
	}

	public FlagAndBase getEvent()
	{
		return event;
	}

	public boolean delivered()
	{
		return flags.incrementAndGet() >= point;
	}

	public TeamType getTeam()
	{
		return team;
	}

	public long getFlags()
	{
		return flags.get();
	}
}