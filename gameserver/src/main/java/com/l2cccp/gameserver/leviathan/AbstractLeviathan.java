package com.l2cccp.gameserver.leviathan;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;

import org.napile.primitive.maps.IntObjectMap;
import org.napile.primitive.maps.impl.CHashIntObjectMap;

import com.l2cccp.commons.logging.LoggerObject;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.leviathan.action.CallAction;
import com.l2cccp.gameserver.leviathan.component.maps.EventMap;
import com.l2cccp.gameserver.leviathan.component.maps.Point;
import com.l2cccp.gameserver.leviathan.enums.ActionState;
import com.l2cccp.gameserver.leviathan.enums.EventState;
import com.l2cccp.gameserver.leviathan.interfaces.ICondition;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.leviathan.utility.ActionExecutor;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.TimeUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public abstract class AbstractLeviathan extends LoggerObject implements Iterable<Participant>
{
	private final LeviathanTemplate template;
	private EventMap map;
	protected EventState state;
	private Map<Integer, Participant> members;
	private Reflection reflection = ReflectionManager.DEFAULT;
	private List<Zone> zones;
	private List<Future<?>> actions = null;
	private IntObjectMap<Future<?>> revives = null;
	protected Calendar _calendar = Calendar.getInstance();
	protected long stoptTime;

	public AbstractLeviathan(LeviathanTemplate template)
	{
		this.template = template;
		this.zones = new ArrayList<Zone>();
		this.members = new HashMap<Integer, Participant>();
	}

	public void init()
	{
		final List<CallAction> actions = getTemplate().getActions(ActionState.INIT);
		if(actions != null) // Perhaps no INIT element
		{
			for(final CallAction action : actions)
			{
				final ActionExecutor runnable = new ActionExecutor(action);
				ThreadPoolManager.getInstance().execute(runnable);
			}
		}
	}

	public void stop()
	{
		members.clear();
		final List<CallAction> actions = getTemplate().getActions(ActionState.STOP);
		for(final CallAction action : actions)
		{
			final ActionExecutor runnable = new ActionExecutor(action);
			ThreadPoolManager.getInstance().execute(runnable);
		}
	}

	public LeviathanTemplate getTemplate()
	{
		return template;
	}

	public boolean checkLevel(final Player player)
	{
		return template.checkLevel(player);
	}

	public void setMap(final EventMap map)
	{
		this.map = map;
	}

	public EventMap getMap()
	{
		return map;
	}

	public List<Zone> getZones()
	{
		return zones;
	}

	public void setZones(final List<Zone> zones)
	{
		this.zones = zones;
	}

	public void setState(final EventState state)
	{
		this.state = state;
	}

	public EventState getState()
	{
		return state;
	}

	public void setReflection(final Reflection reflection)
	{
		this.reflection = reflection;
	}

	public Reflection getReflection()
	{
		return reflection;
	}

	public void register(final Player player)
	{
		if(state != EventState.REGISTER)
		{
			player.sendMessage(new CustomMessage(state.getMessage()));
			return;
		}

		for(final ICondition condition : getTemplate().getConditions())
		{
			if(!condition.check(player, null))
			{
				final Language language = player.getLanguage();
				final String htm = HtmCache.getInstance().getHtml(condition.getFailedHtml(), player);
				if(htm != null)
				{
					HtmlMessage html = new HtmlMessage(0);
					html.setHtml(htm);
					html.replace("%id%", String.valueOf(getTemplate().getEventId()));
					html.replace("%event%", String.valueOf(getTemplate().getName(language)));
					condition.replace(html);
					player.sendPacket(html);
				}

				player.sendMessage(condition.getFailedMessage(language));
				return;
			}
		}

		Participant participant = new Participant(player);
		addParticipant(participant);
	}

	public void addParticipant(final Participant object)
	{
		members.put(object.getObjectId(), object);
	}

	public Map<Integer, Participant> getMembers()
	{
		return members;
	}

	public void shuffleRegistered()
	{
		List<Participant> participants = new ArrayList<Participant>(members.values());
		Collections.shuffle(participants);
		members.clear();
		for(final Participant participant : participants)
			members.put(participant.getObjectId(), participant);
	}

	public long getTimeStart()
	{
		return _calendar.getTimeInMillis();
	}

	public long getTimeStop()
	{
		return stoptTime;
	}

	protected void logInfo()
	{
		final long start = getTimeStart();
		final long stop = getTimeStop();
		final String name = getName();
		if(start > 0)
		{
			if(stop > 0)
				info(name + " start time - " + TimeUtils.toSimpleFormat(start) + ", stop time - " + TimeUtils.toSimpleFormat(stop));
			else
				info(name + " start time - " + TimeUtils.toSimpleFormat(start) + ", stop time - Not recognized");
		}
		else
			info(name + " start time - Not recognized");
	}

	public String getName()
	{
		return getTemplate().getName(Language.ENGLISH);
	}

	/**
	 * Actions & Task
	 */
	public void registerActions(final boolean init)
	{
		final long start = getTimeStart();
		if(start == 0)
		{
			info(getName() + " try register controller but start time is not recognized");
			return;
		}

		final long now = System.currentTimeMillis();
		final List<CallAction> list = getTemplate().getActions(ActionState.ON);
		if(actions == null)
			actions = new ArrayList<Future<?>>(list.size());

		long last = Integer.MIN_VALUE;
		for(final CallAction action : list)
		{
			final long delay = action.delay();
			final long calc = start + delay;
			stoptTime = start + (last = Math.max(last, delay));
			final ActionExecutor runnable = new ActionExecutor(action);
			if(calc > now)
			{
				final ScheduledFuture<?> task = ThreadPoolManager.getInstance().schedule(runnable, calc - now);
				actions.add(task);
				continue;
			}

			ThreadPoolManager.getInstance().execute(runnable);
		}
	}

	public void addRevive(final int object, final Future<?> task)
	{
		if(revives == null)
			revives = new CHashIntObjectMap<Future<?>>();

		revives.put(object, task);
	}

	public void stopRevive(final int object)
	{
		final Future<?> revive = revives.get(object);
		if(revive != null)
		{
			revive.cancel(false);
			revives.remove(object);
		}
	}

	public void stopActions()
	{
		if(revives != null)
		{
			for(final Future<?> revive : revives.values())
				revive.cancel(false);

			revives.clear();
		}

		if(actions != null)
		{
			for(final Future<?> action : actions)
				action.cancel(false);

			actions.clear();
		}
	}

	/**
	 * Abstract method
	 */
	public abstract void reCalcStart(final boolean init);

	public abstract void setTeam(final Participant participant);

	public abstract void start();

	public abstract void finish();

	public abstract TeamType getWinnerTeam();

	public abstract List<Participant> getTeam(final int team);

	public abstract Point getPoint(final int team);

	public abstract void kill(final Participant killer, final Participant victim);

	public abstract void die(final Participant killer, final Participant victim);

	public abstract boolean checkWinner();

	public abstract void remove(final Participant victim);
}