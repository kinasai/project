package com.l2cccp.gameserver.leviathan.utility;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.model.Player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RegisterAsk implements OnAnswerListener
{
	private AbstractLeviathan _event;

	public RegisterAsk(final AbstractLeviathan event)
	{
		_event = event;
	}

	@Override
	public void sayYes(Player player)
	{
		_event.register(player);
	}

	@Override
	public void sayNo(Player player)
	{}
}