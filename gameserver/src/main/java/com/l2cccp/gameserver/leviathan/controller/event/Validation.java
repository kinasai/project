package com.l2cccp.gameserver.leviathan.controller.event;

import org.dom4j.Element;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Validation extends Controller
{
	private final String _success, _failure;
	private final ChatType _chat;

	public Validation(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		_success = _params.getString("success");
		_failure = _params.getString("failure");
		_chat = ChatType.valueOf(_params.getString("chat"));
	}

	@Override
	public void run()
	{
		final LeviathanTemplate template = _event.getTemplate();
		final int size = _event.getMembers().size();
		final boolean run = size >= _event.getMap().getMin();
		if(!run)
		{
			if(Config.LEVIATHAN_DEBUG)
				System.out.println("Registered size -> " + size);
			_event.stop();
		}
		else
			_event.shuffleRegistered();

		final String msg = run ? _success : _failure;
		for(final Player player : GameObjectsStorage.getPlayers())
		{
			if(_event.checkLevel(player))
			{
				final CustomMessage message = new CustomMessage(msg);
				final Language language = player.getLanguage();
				player.sendPacket(new Say2(0, _chat, template.getName(language), message.toString(language), null));
			}
		}
	}
}
