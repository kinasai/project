package com.l2cccp.gameserver.leviathan.controller.object.spawn;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public abstract class AbstractSpawnController extends Controller
{
	protected final boolean reflection;

	public AbstractSpawnController(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		reflection = _params.getBool("reflection", false);
	}
}