package com.l2cccp.gameserver.leviathan.controller.object.spawn;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.objects.CapturedBaseData;
import com.l2cccp.gameserver.leviathan.type.FlagAndBase;
import com.l2cccp.gameserver.model.base.TeamType;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CapturedBase extends AbstractSpawnController
{
	private final TeamType team;

	public CapturedBase(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		team = TeamType.valueOf(_params.getString("team"));
	}

	@Override
	public void run()
	{
		final FlagAndBase event = (FlagAndBase) _event;
		final CapturedBaseData base = event.getBase(team);
		if(base == null)
		{
			warn("Can't find base " + team.name());
			return;
		}

		base.spawn();
	}
}