package com.l2cccp.gameserver.leviathan.controller.event;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.enums.EventState;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class StateChanger extends Controller
{
	private final EventState state;

	public StateChanger(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		state = EventState.valueOf(_params.getString("state", "PREPARE"));
	}

	@Override
	public void run()
	{
		_event.setState(state);
	}
}