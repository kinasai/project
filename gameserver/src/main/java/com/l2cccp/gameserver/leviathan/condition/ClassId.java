package com.l2cccp.gameserver.leviathan.condition;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.dom4j.Element;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ClassId extends AbstractCondition
{
	private final Set<Integer> ids;

	public ClassId(final Element element)
	{
		super(element);
		this.ids = new HashSet<Integer>();
		final StringTokenizer tokens = new StringTokenizer(element.attributeValue("set"), ",");
		while(tokens.hasMoreElements())
			ids.add(Integer.parseInt(tokens.nextToken()));
	}

	@Override
	public boolean check(Player player, Creature target)
	{
		if(player == null)
			return false;

		return ids.contains(player.getActiveClassId());
	}

	@Override
	public String getFailedMessage(Language language)
	{
		return new CustomMessage("leviathan.condition.classid").toString(language);
	}

	@Override
	public void replace(final HtmlMessage html)
	{
		// do nothing
	}
}