package com.l2cccp.gameserver.leviathan.condition;

import org.dom4j.Element;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CheckClan extends AbstractCondition
{
	private final boolean inClan;

	private final int level;

	public CheckClan(final Element element)
	{
		super(element);
		this.inClan = Boolean.parseBoolean(element.attributeValue("clan", "true"));
		this.level = Integer.parseInt(element.attributeValue("level", "1"));
	}

	@Override
	public boolean check(final Player player, final Creature target)
	{
		if(player == null)
			return false;
		final Clan clan = player.getClan();

		return inClan ? clan != null && clan.getLevel() >= level : clan == null;
	}

	@Override
	public String getFailedMessage(final Language language)
	{
		if(inClan)
			return new CustomMessage("leviathan.condition.clan").addNumber(level).toString(language);
		else
			return new CustomMessage("leviathan.condition.noclan").toString(language);
	}

	@Override
	public void replace(final HtmlMessage html)
	{
		html.replace("%level%", String.valueOf(level));
	}
}