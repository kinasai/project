package com.l2cccp.gameserver.leviathan.controller.object.add;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.FameRewardInfo;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.enums.Reward;
import com.l2cccp.gameserver.leviathan.enums.RewardType;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FameReward extends Controller
{
	private final int count;
	private final double chance;
	private final RewardType type;
	private final int position;

	public FameReward(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		position = _params.getInteger("position", 0);
		count = _params.getInteger("count");
		chance = _params.getDouble("chance", 100D);
		type = RewardType.valueOf(_params.getString("type"));
	}

	@Override
	public void run()
	{
		final LeviathanTemplate template = _event.getTemplate();
		final FameRewardInfo reward = new FameRewardInfo(count, chance, Reward.FAME);
		if(position > 0)
			reward.setPosition(position);
		template.addReward(type, reward);
	}
}