package com.l2cccp.gameserver.leviathan.controller.object.add;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.NpcInfo;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.model.base.TeamType;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Npc extends Controller
{
	private final String group, title, route;
	private final int id, x, y, z, h, despawn;
	private final TeamType team;

	public Npc(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		group = _params.getString("group");
		id = _params.getInteger("id");
		x = _params.getInteger("x");
		y = _params.getInteger("y");
		z = _params.getInteger("z");
		h = _params.getInteger("h", 0);
		despawn = _params.getInteger("despawn", 0);
		title = _params.getString("title", null);
		route = _params.getString("route", null);
		team = TeamType.valueOf(_params.getString("team", "NONE"));
	}

	@Override
	public void run()
	{
		final LeviathanTemplate template = _event.getTemplate();
		final NpcInfo npc = new NpcInfo(id, x, y, z, h, despawn, title, route, team);
		template.addNpc(group, npc);
	}
}