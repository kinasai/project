package com.l2cccp.gameserver.leviathan.condition;

import org.dom4j.Element;

import com.l2cccp.commons.logging.LoggerObject;
import com.l2cccp.gameserver.leviathan.interfaces.ICondition;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public abstract class AbstractCondition extends LoggerObject implements ICondition
{
	private final String html;

	public AbstractCondition(final Element element)
	{
		this.html = element.attributeValue("html");
	}

	@Override
	public String getFailedHtml()
	{
		return html;
	}
}