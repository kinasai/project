package com.l2cccp.gameserver.leviathan.controller.event;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TeamManager extends Controller
{
	public TeamManager(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event.getMembers().values())
			_event.setTeam(participant);
	}
}