package com.l2cccp.gameserver.leviathan.component.maps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2cccp.gameserver.utils.Language;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class EventMap
{
	private final Map<Language, String> name;
	private final int min;
	private final int max;
	private final int instances;
	private List<String> zones;
	private TIntObjectHashMap<List<Point>> points;

	public EventMap(final int min, final int max, final int instances)
	{
		this.name = new HashMap<Language, String>();
		this.min = min;
		this.max = max;
		this.instances = instances;
		this.zones = new ArrayList<String>();
		this.points = new TIntObjectHashMap<List<Point>>();
	}

	public void addPoint(final int team, final Point point)
	{
		List<Point> list = points.get(team);
		if(list == null)
			points.put(team, list = new ArrayList<Point>());

		list.add(point);
	}

	public List<Point> getPoints(final int team)
	{
		return points.get(team);
	}

	public void addZone(final String zone)
	{
		zones.add(zone);
	}

	public List<String> getZones()
	{
		return zones;
	}

	public void addName(final Language language, final String string)
	{
		name.put(language, string);
	}

	public String getName(final Language language)
	{
		return name.get(language);
	}

	public int getMin()
	{
		return min;
	}

	public int getMax()
	{
		return max;
	}

	public int getInstances()
	{
		return instances;
	}
}