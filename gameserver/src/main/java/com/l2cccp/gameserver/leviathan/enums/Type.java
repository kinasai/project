package com.l2cccp.gameserver.leviathan.enums;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.leviathan.type.FlagAndBase;
import com.l2cccp.gameserver.leviathan.type.LastManStanding;
import com.l2cccp.gameserver.leviathan.type.TeamVsTeam;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum Type
{
	LAST_MAN_STANDING
	{
		@Override
		public AbstractLeviathan create(final LeviathanTemplate template)
		{
			return new LastManStanding(template);
		}
	},
	FLAG_AND_BASE
	{
		@Override
		public AbstractLeviathan create(final LeviathanTemplate template)
		{
			return new FlagAndBase(template);
		}
	},
	TEAM_VS_TEAM
	{
		@Override
		public AbstractLeviathan create(final LeviathanTemplate template)
		{
			return new TeamVsTeam(template);
		}
	};

	public abstract AbstractLeviathan create(final LeviathanTemplate template);
}