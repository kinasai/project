package com.l2cccp.gameserver.leviathan.controller.player;

import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.EffectList;
import com.l2cccp.gameserver.model.Player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RestoreEffects extends Controller
{
	public RestoreEffects(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event)
		{
			final Player player = participant.get();
			final EffectList list = player.getEffectList();
			if(list.getAllEffects().size() > 0)
				list.stopAllEffects();

			final List<Effect> effects = participant.getEffects();
			for(final Effect effect : effects)
				list.addEffect(effect);

			player.updateEffectIcons();
		}
	}
}