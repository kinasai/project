package com.l2cccp.gameserver.leviathan.controller.announcements;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class SimpleInfo extends Controller
{
	private final String _msg;
	private final ChatType _chat;

	public SimpleInfo(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		_msg = _params.getString("msg");
		_chat = ChatType.valueOf(_params.getString("chat"));
	}

	@Override
	public void run()
	{
		final LeviathanTemplate template = _event.getTemplate();

		for(final Player player : GameObjectsStorage.getPlayers())
		{
			if(_event.checkLevel(player))
			{
				final Language language = player.getLanguage();
				final CustomMessage message = new CustomMessage(_msg).addNumber(template.getMinLevel()).addNumber(template.getMaxLevel()).addString(_event.getMap().getName(language));
				player.sendPacket(new Say2(0, _chat, template.getName(language), message.toString(language), null));
			}
		}
	}
}