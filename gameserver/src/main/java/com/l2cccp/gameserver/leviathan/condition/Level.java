package com.l2cccp.gameserver.leviathan.condition;

import org.dom4j.Element;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Level extends AbstractCondition
{
	private final byte min, max;

	public Level(final Element element)
	{
		super(element);
		this.min = Byte.parseByte(element.attributeValue("min"));
		this.max = Byte.parseByte(element.attributeValue("max"));
	}

	@Override
	public boolean check(Player player, Creature target)
	{
		if(player == null)
			return false;

		return player.getLevel() >= min && player.getLevel() <= max;
	}

	@Override
	public String getFailedMessage(Language language)
	{
		return new CustomMessage("leviathan.condition.level").addNumber(min).addNumber(max).toString(language);
	}

	@Override
	public void replace(final HtmlMessage html)
	{
		html.replace("%min%", String.valueOf(min));
		html.replace("%max%", String.valueOf(max));
	}
}