package com.l2cccp.gameserver.leviathan.component;

import com.l2cccp.gameserver.leviathan.enums.Reward;
import com.l2cccp.gameserver.leviathan.interfaces.IReward;
import com.l2cccp.gameserver.model.Player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ItemRewardInfo extends AbstractComponent implements IReward
{
	private final int id;
	private final long min, max;
	private final Reward type;
	private final double chance;
	private int position;

	public ItemRewardInfo(final int id, final long min, final long max, final double chance, final Reward type)
	{
		this.id = id;
		this.min = min;
		this.max = max;
		this.chance = chance;
		this.type = type;
	}

	public int getItemId()
	{
		return id;
	}

	public long getMin()
	{
		return min;
	}

	public long getMax()
	{
		return max;
	}

	public double getChance()
	{
		return chance;
	}

	@Override
	public void give(final Player player)
	{
		type.give(player, this);
	}

	public void setPosition(final int position)
	{
		this.position = position;
	}

	@Override
	public int getPosition()
	{
		return position;
	}
}