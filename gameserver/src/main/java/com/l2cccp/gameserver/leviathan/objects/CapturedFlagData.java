package com.l2cccp.gameserver.leviathan.objects;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.gameserver.leviathan.type.FlagAndBase;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.items.ItemInstance.ItemLocation;
import com.l2cccp.gameserver.model.items.attachment.FlagItemAttachment;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CapturedFlagData implements FlagItemAttachment
{
	private static final Logger _log = LoggerFactory.getLogger(CapturedFlagData.class);
	private final int id;
	private final FlagAndBase event;
	private final TeamType team;
	private final Location location;
	private ItemInstance flag;

	public CapturedFlagData(final int id, final FlagAndBase event, final Location location, final TeamType team)
	{
		this.id = id;
		this.event = event;
		this.team = team;
		this.location = location;
	}

	public void spawn()
	{
		if(flag != null)
		{
			_log.info("CapturedFlag: try spawn twice: " + event.getName());
			return;
		}

		flag = ItemFunctions.createItem(id);
		flag.setAttachment(this);
		flag.dropMe(null, location);
		flag.setReflection(event.getReflection());
		flag.setDropTime(0);
	}

	public void despawn()
	{
		if(flag == null)
			return;

		final Player owner = GameObjectsStorage.getPlayer(flag.getOwnerId());
		if(owner != null)
		{
			owner.getInventory().destroyItem(flag);
			owner.sendDisarmMessage(flag);
		}

		flag.setAttachment(null);
		flag.setJdbcState(JdbcEntityState.UPDATED);
		flag.delete();
		flag.deleteMe();
		flag = null;
	}

	@Override
	public boolean canPickUp(final Player player)
	{
		if(player.getActiveWeaponFlagAttachment() != null || player.isMounted())
			return false;
		else if(player.getTeam() == TeamType.NONE || player.getTeam() == team)
			return false;
		else
			return true;
	}

	@Override
	public void pickUp(final Player player)
	{
		player.getInventory().equipItem(flag);
	}

	@Override
	public void setItem(final ItemInstance item)
	{
		// do nothing
	}

	@Override
	public void onLogout(final Player player)
	{
		onDeath(player, null);
	}

	@Override
	public void onDeath(final Player owner, final Creature killer)
	{
		owner.getInventory().removeItem(flag);

		flag.setLocation(ItemLocation.VOID);
		flag.setJdbcState(JdbcEntityState.UPDATED);
		flag.update();

		owner.sendPacket(new SystemMessage(SystemMsg.YOU_HAVE_DROPPED_S1).addItemName(flag.getItemId()));

		flag.setCount(1);
		flag.dropMe(owner, location);
		flag.setDropTime(0);
	}

	@Override
	public boolean canAttack(final Player player)
	{
		player.sendPacket(SystemMsg.THAT_WEAPON_CANNOT_PERFORM_ANY_ATTACKS);
		return false;
	}

	@Override
	public boolean canCast(final Player player, final SkillEntry skill)
	{
		final SkillEntry[] skills = player.getActiveWeaponItem().getAttachedSkills();
		if(ArrayUtils.contains(skills, skill))
			return true;

		player.sendPacket(SystemMsg.THAT_WEAPON_CANNOT_USE_ANY_OTHER_SKILL_EXCEPT_THE_WEAPONS_SKILL);
		return false;
	}

	public TeamType getTeam()
	{
		return team;
	}
}