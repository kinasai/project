package com.l2cccp.gameserver.leviathan.controller.event;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.leviathan.utility.RegisterAsk;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.PlayerType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class SendAsk extends Controller
{
	private final String _msg;
	private final int _time;

	public SendAsk(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		_msg = _params.getString("msg");
		_time = _params.getInteger("time");
	}

	@Override
	public void run()
	{
		final LeviathanTemplate template = _event.getTemplate();

		for(final Player player : GameObjectsStorage.getPlayers())
		{
			if(_event.checkLevel(player) && player.getPlayableType() == PlayerType.PLAYER)
			{
				final Language language = player.getLanguage();
				final CustomMessage message = new CustomMessage(_msg).addString(template.getName(language)).addNumber(template.getMinLevel()).addNumber(template.getMaxLevel()).addString(_event.getMap().getName(language));
				ConfirmDlg ask = new ConfirmDlg(SystemMsg.S1, _time);
				ask.addString(message.toString(language));
				player.ask(ask, new RegisterAsk(_event));
			}
		}
	}
}