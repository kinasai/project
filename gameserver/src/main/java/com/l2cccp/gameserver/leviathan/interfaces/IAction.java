package com.l2cccp.gameserver.leviathan.interfaces;

import com.l2cccp.gameserver.leviathan.enums.ActionState;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public interface IAction
{
	ActionState state();

	long delay();

	void call();
}