package com.l2cccp.gameserver.leviathan.action;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.enums.ActionState;
import com.l2cccp.gameserver.leviathan.interfaces.IAction;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CallAction implements IAction
{
	protected final List<String> _objects;
	protected final AbstractLeviathan _event;
	private final long _delay;
	private final ActionState _state;

	public CallAction(final AbstractLeviathan event, final ActionState state, final Element element)
	{
		_event = event;
		_state = state;
		final double delay = Double.parseDouble(element.attributeValue("delay", "0"));
		_delay = (long) (delay * 1000D);
		_objects = new ArrayList<String>();
		read(element);
	}

	private void read(final Element element)
	{
		for(Iterator<Element> parameterIterator = element.elementIterator("call"); parameterIterator.hasNext();)
		{
			Element parameterElement = parameterIterator.next();
			_objects.add(parameterElement.attributeValue("object"));
		}
	}

	@Override
	public ActionState state()
	{
		return _state;
	}

	@Override
	public long delay()
	{
		return _delay;
	}

	@Override
	public void call()
	{
		if(Config.LEVIATHAN_DEBUG)
			System.out.println("Action state: " + _state.name() + ", delay: " + (_delay / 1000L) + " sec.");
		for(final String object : _objects)
		{
			if(Config.LEVIATHAN_DEBUG)
				System.out.println("\tCall object -> " + object);
			_event.getTemplate().getController(object).run();
		}
	}
}