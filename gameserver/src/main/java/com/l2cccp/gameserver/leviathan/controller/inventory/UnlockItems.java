package com.l2cccp.gameserver.leviathan.controller.inventory;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class UnlockItems extends Controller
{
	public UnlockItems(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event)
		{
			final Player player = participant.get();
			player.getInventory().unlock();
		}
	}
}