package com.l2cccp.gameserver.leviathan.controller.event;

import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.enums.RewardType;
import com.l2cccp.gameserver.leviathan.interfaces.IReward;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class GiveReward extends Controller
{
	private final String _winner, _losser, _draw;
	private final ChatType _chat;

	public GiveReward(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		_winner = _params.getString("winner");
		_losser = _params.getString("losser", null);
		_draw = _params.getString("draw", null);
		_chat = ChatType.valueOf(_params.getString("chat"));
	}

	@Override
	public void run()
	{
		final LeviathanTemplate template = _event.getTemplate();
		final TeamType winner = _event.getWinnerTeam();
		for(final Participant participant : _event)
		{
			String msg = null;
			List<IReward> rewards = null;
			if(winner == TeamType.NONE)
			{
				msg = _draw;
				rewards = template.getRewards(RewardType.LOSSER);
			}
			else if(winner == participant.getTeam())
			{
				msg = _winner;
				rewards = template.getRewards(RewardType.WINNER);
			}
			else
			{
				msg = _losser;
				rewards = template.getRewards(RewardType.LOSSER);
			}

			final Player player = participant.get();
			if(player != null)
			{
				final CustomMessage message = new CustomMessage(msg);
				final Language language = player.getLanguage();
				player.sendPacket(new Say2(0, _chat, template.getName(language), message.toString(language), null));

				if(rewards != null)
				{
					for(final IReward reward : rewards)
						reward.give(participant.get());
				}
			}
		}
	}
}
