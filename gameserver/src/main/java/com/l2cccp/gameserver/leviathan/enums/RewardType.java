package com.l2cccp.gameserver.leviathan.enums;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum RewardType
{
	WINNER,
	LOSSER,
	POSITION,
	KILL
}