package com.l2cccp.gameserver.leviathan.component;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class NpcInfo extends AbstractComponent
{
	private final int id, x, y, z, h;
	private final long despawn;
	private final String title, route;
	private final TeamType team;
	private NpcInstance npc;

	public NpcInfo(final int id, final int x, final int y, final int z, final int h, final long despawn, final String title, final String route, final TeamType team)
	{
		this.id = id;
		this.x = x;
		this.y = y;
		this.z = z;
		this.h = h;
		this.despawn = despawn;
		this.title = title;
		this.route = route;
		this.team = team;
	}

	public void spawn(final AbstractLeviathan _event, final Reflection reflection)
	{
		final Location loc = new Location(x, y, z, h);
		final NpcInstance npc = NpcUtils.spawnSingle(id, loc, reflection, despawn, title, route);
		npc.setTeam(team);
		this.npc = npc;
	}

	public void despawn()
	{
		if(npc != null)
			npc.deleteMe();
	}
}