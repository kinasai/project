package com.l2cccp.gameserver.leviathan.listeners;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.enums.EventState;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerAttack;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OnTeamEventAttack extends AbstractListener implements OnPlayerAttack
{
	public OnTeamEventAttack(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public boolean canAttack(final Creature target, final Creature attacker, final Skill skill, final boolean force, final boolean nextAttackCheck)
	{
		if(event.getState() != EventState.RUN || target.getTeam() == attacker.getTeam())
			return false;

		return true;
	}

	@Override
	public SystemMsg checkForAttack(final Creature target, final Creature attacker, final Skill skill, final boolean force)
	{
		if(!canAttack(target, attacker, skill, force, false))
			return SystemMsg.INVALID_TARGET;

		return null;
	}
}
