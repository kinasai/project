package com.l2cccp.gameserver.leviathan.enums;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum ActionState
{
	INIT,
	ON,
	STOP
}