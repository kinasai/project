package com.l2cccp.gameserver.leviathan.controller.player;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.skills.effects.EffectTemplate;
import com.l2cccp.gameserver.stats.Env;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class StoreageEffects extends Controller
{
	public StoreageEffects(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event)
		{
			final Player player = participant.get();

			for(final Effect effect : player.getEffectList().getAllEffects())
			{
				final SkillEntry skill = effect.getSkill();
				for(final EffectTemplate template : skill.getTemplate().getEffectTemplates())
				{
					final Effect clone = template.getEffect(new Env(player, player, skill));
					if(clone != null)
					{
						final int count = effect.getCount();
						clone.setCount(count);
						clone.setPeriod(count == 1 ? effect.getPeriod() - effect.getTime() : effect.getPeriod());
						participant.saveEffect(clone);
					}
				}
			}
		}
	}
}