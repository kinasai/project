package com.l2cccp.gameserver.leviathan.controller.event.reflection;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.model.entity.Reflection;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CollapseteReflection extends Controller
{
	public CollapseteReflection(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		final Reflection reflection = _event.getReflection();
		if(reflection != null && !reflection.isDefault() && !reflection.isCollapseStarted())
			reflection.collapse();

		_event.setReflection(null);
	}
}