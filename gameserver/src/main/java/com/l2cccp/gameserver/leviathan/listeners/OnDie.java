package com.l2cccp.gameserver.leviathan.listeners;

import java.util.concurrent.Future;

import org.dom4j.Element;

import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.listeners.inventory.AbstractEquipListener;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.leviathan.utility.RessurectTask;
import com.l2cccp.gameserver.listener.actor.OnDeathListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.utils.TimeUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OnDie extends AbstractListener implements OnDeathListener
{
	private final static int DEFAULT = -1;
	private final int ressurect;
	private final boolean remove;

	public OnDie(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		ressurect = _params.getInteger("ressurect", DEFAULT);
		remove = _params.getBool("remove", false);
	}

	@Override
	public void onDeath(Creature one, Creature two)
	{
		if(!one.isPlayable() || !two.isPlayer())
			return;

		final Participant victim = event.getMembers().get(one.getObjectId());
		final Participant killer = event.getMembers().get(two.getObjectId());
		victim.incDeath();
		event.die(killer, victim);
		if(ressurect != DEFAULT)
		{
			final RessurectTask task = new RessurectTask(event, victim.get());
			if(ressurect > 0)
			{
				final long milis = TimeUtils.addSecond(ressurect);
				if(event.getTimeStop() > System.currentTimeMillis() + milis)
				{
					victim.sendPacket(new SystemMessage(SystemMsg.RESURRECTION_WILL_TAKE_PLACE_IN_THE_WAITING_ROOM_AFTER_S1_SECONDS).addNumber(ressurect));
					final Future<?> revive = ThreadPoolManager.getInstance().schedule(task, milis);
					event.addRevive(victim.getObjectId(), revive);
				}
			}
			else
				ThreadPoolManager.getInstance().execute(task);
		}

		if(remove)
		{
			for(final AbstractListener listener : event.getTemplate().getListeners())
				victim.removeListener(listener);
			for(final AbstractEquipListener listener : event.getTemplate().getEquipListeners())
				victim.removeListener(listener);

			victim.restore();
			event.remove(victim);
		}

		call();
	}
}