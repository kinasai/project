package com.l2cccp.gameserver.leviathan.listeners.inventory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.listener.inventory.OnEquipListener;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public abstract class AbstractEquipListener implements OnEquipListener
{
	protected final List<String> _actions;
	protected final MultiValueSet<String> _params;
	protected final AbstractLeviathan _event;

	public AbstractEquipListener(final AbstractLeviathan event, final Element element)
	{
		_event = event;
		_params = new MultiValueSet<String>();
		_actions = new ArrayList<String>();
		read(element);
	}

	private void read(final Element element)
	{
		for(Iterator<Element> iterator = element.elementIterator(); iterator.hasNext();)
		{
			Element param = iterator.next();
			if(param.getName().equals("param"))
				_params.set(param.attributeValue("key"), param.attributeValue("val"));
			else if(param.getName().equals("call"))
				_actions.add(param.attributeValue("object"));
		}
	}

	protected void call()
	{
		for(final String object : _actions)
		{
			final Controller action = _event.getTemplate().getController(object);
			action.run();
		}
	}
}
