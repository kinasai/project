package com.l2cccp.gameserver.leviathan.controller.object.add;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.CapturedBaseData;
import com.l2cccp.gameserver.leviathan.type.FlagAndBase;
import com.l2cccp.gameserver.model.base.TeamType;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CapturedBase extends Controller
{
	private final int id, point, radius;
	private final String listener;
	private final TeamType team;

	public CapturedBase(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		id = _params.getInteger("id");
		point = _params.getInteger("point");
		radius = _params.getInteger("radius");
		listener = _params.getString("listener");
		team = TeamType.valueOf(_params.getString("team", "NONE"));
	}

	@Override
	public void run()
	{
		final FlagAndBase event = (FlagAndBase) _event;
		final CapturedBaseData base = new CapturedBaseData(event, id, point, team, listener, radius);
		event.addBase(base);
	}
}