package com.l2cccp.gameserver.leviathan.template;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2cccp.gameserver.leviathan.action.CallAction;
import com.l2cccp.gameserver.leviathan.component.NpcInfo;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.enums.ActionState;
import com.l2cccp.gameserver.leviathan.enums.RewardType;
import com.l2cccp.gameserver.leviathan.enums.Type;
import com.l2cccp.gameserver.leviathan.interfaces.ICondition;
import com.l2cccp.gameserver.leviathan.interfaces.IReward;
import com.l2cccp.gameserver.leviathan.listeners.AbstractListener;
import com.l2cccp.gameserver.leviathan.listeners.inventory.AbstractEquipListener;
import com.l2cccp.gameserver.leviathan.listeners.zone.AbstractZoneListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class LeviathanTemplate
{
	private final int id;
	private final Type type; // Event type
	private final byte[] levels = new byte[2]; // min - 0, max -1
	private final String cron;
	private final Map<Language, String> name; // Multilang names
	private final Map<ActionState, List<CallAction>> actions;
	private final Map<String, Controller> controllers;
	private final List<AbstractListener> listeners;
	private final List<AbstractEquipListener> equip_listeners;
	private final Map<String, AbstractZoneListener> zone_listeners;
	private final Map<RewardType, List<IReward>> rewards;
	private final Map<String, List<NpcInfo>> npcs;

	private List<ICondition> conditions = Collections.emptyList(); // Conditions

	public LeviathanTemplate(final int id, final Type type, final String cron)
	{
		this.id = id;
		this.type = type;
		this.cron = cron;
		this.name = new HashMap<Language, String>();
		this.actions = new HashMap<ActionState, List<CallAction>>();
		this.controllers = new HashMap<String, Controller>();
		this.listeners = new ArrayList<AbstractListener>();
		this.equip_listeners = new ArrayList<AbstractEquipListener>();
		this.zone_listeners = new HashMap<String, AbstractZoneListener>();
		this.rewards = new HashMap<RewardType, List<IReward>>();
		this.npcs = new HashMap<String, List<NpcInfo>>();
	}

	public void addName(final Language language, final String string)
	{
		name.put(language, string);
	}

	public void setLevels(final byte min, final byte max)
	{
		levels[0] = min;
		levels[1] = max;
	}

	public void addController(final String object, final Controller controller)
	{
		controllers.put(object, controller);
	}

	public void addListener(final AbstractListener listener)
	{
		listeners.add(listener);
	}

	public void addEquipListener(final AbstractEquipListener listener)
	{
		equip_listeners.add(listener);
	}

	public void addZoneListener(final String zone, final AbstractZoneListener listener)
	{
		zone_listeners.put(zone, listener);
	}

	public void addAction(final ActionState state, final CallAction action)
	{
		List<CallAction> list = actions.get(state);
		if(list == null)
			actions.put(state, list = new ArrayList<CallAction>());

		list.add(action);
	}

	public void addReward(final RewardType team, final IReward reward)
	{
		List<IReward> rew = rewards.get(team);
		if(rew == null)
			rewards.put(team, rew = new ArrayList<IReward>());

		rew.add(reward);
	}

	public void addNpc(final String group, final NpcInfo npc)
	{
		List<NpcInfo> list = npcs.get(group);
		if(list == null)
			npcs.put(group, list = new ArrayList<NpcInfo>());

		list.add(npc);
	}

	public void addCondition(final ICondition condition)
	{
		if(conditions.isEmpty())
			conditions = new ArrayList<ICondition>();

		conditions.add(condition);
	}

	public boolean checkLevel(final Player player)
	{
		final int level = player.getLevel();
		return level >= getMinLevel() && level <= getMaxLevel();
	}

	public Map<String, List<NpcInfo>> getNpcs()
	{
		return npcs;
	}

	public List<NpcInfo> getNpcs(final String group)
	{
		return npcs.get(group);
	}

	public Controller getController(final String object)
	{
		return controllers.get(object);
	}

	public List<CallAction> getActions(final ActionState state)
	{
		return actions.get(state);
	}

	public List<IReward> getRewards(final RewardType team)
	{
		return rewards.get(team);
	}

	public List<AbstractListener> getListeners()
	{
		return listeners;
	}

	public List<AbstractEquipListener> getEquipListeners()
	{
		return equip_listeners;
	}

	public AbstractZoneListener getZoneListener(final String zone)
	{
		return zone_listeners.get(zone);
	}

	public List<ICondition> getConditions()
	{
		return conditions;
	}

	public int getEventId()
	{
		return id;
	}

	public int getMinLevel()
	{
		return levels[0];
	}

	public int getMaxLevel()
	{
		return levels[1];
	}

	public String getCron()
	{
		return cron;
	}

	public Type getType()
	{
		return type;
	}

	public String getName(final Language language)
	{
		return name.get(language);
	}
}