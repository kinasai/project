package com.l2cccp.gameserver.leviathan.controller.object.add;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.ItemRewardInfo;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.enums.Reward;
import com.l2cccp.gameserver.leviathan.enums.RewardType;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ItemReward extends Controller
{
	private final int id;
	private final long min, max;
	private final double chance;
	private final RewardType type;
	private final int position;
	private final boolean mulltiplication;

	public ItemReward(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		id = _params.getInteger("id");
		position = _params.getInteger("position", 0);
		min = _params.getLong("min");
		max = _params.getLong("max", min);
		chance = _params.getDouble("chance", 100D);
		mulltiplication = _params.getBool("mulltiplication", false);
		type = RewardType.valueOf(_params.getString("type"));
	}

	@Override
	public void run()
	{
		final int mul = mulltiplication ? _event.getMembers().size() : 1;
		final LeviathanTemplate template = _event.getTemplate();
		final ItemRewardInfo reward = new ItemRewardInfo(id, min * mul, max * mul, chance, Reward.ITEM);
		if(position > 0)
			reward.setPosition(position);
		template.addReward(type, reward);
	}
}