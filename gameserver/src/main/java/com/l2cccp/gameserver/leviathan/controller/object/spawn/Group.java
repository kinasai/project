package com.l2cccp.gameserver.leviathan.controller.object.spawn;

import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.NpcInfo;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Group extends AbstractSpawnController
{
	private final String group;

	public Group(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		group = _params.getString("group");
	}

	@Override
	public void run()
	{
		List<NpcInfo> npcs = _event.getTemplate().getNpcs(group);
		if(npcs == null)
		{
			warn("Can't find spawn group " + group);
			return;
		}

		for(final NpcInfo npc : npcs)
			npc.spawn(_event, reflection ? _event.getReflection() : ReflectionManager.DEFAULT);
	}
}