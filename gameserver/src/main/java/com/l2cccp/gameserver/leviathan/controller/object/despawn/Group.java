package com.l2cccp.gameserver.leviathan.controller.object.despawn;

import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.NpcInfo;
import com.l2cccp.gameserver.leviathan.controller.Controller;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Group extends Controller
{
	private final String group;

	public Group(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		group = _params.getString("group");
	}

	@Override
	public void run()
	{
		List<NpcInfo> npcs = _event.getTemplate().getNpcs(group);
		if(npcs == null)
		{
			warn("Can't find spawn group " + group);
			return;
		}

		for(final NpcInfo npc : npcs)
			npc.despawn();
	}
}