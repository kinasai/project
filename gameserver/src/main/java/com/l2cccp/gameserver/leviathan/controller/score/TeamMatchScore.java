package com.l2cccp.gameserver.leviathan.controller.score;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.leviathan.type.TeamVsTeam;
import com.l2cccp.gameserver.network.l2.s2c.ExPVPMatchCCMyRecord;
import com.l2cccp.gameserver.network.l2.s2c.ExPVPMatchCCRetire;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TeamMatchScore extends Controller
{
	private final boolean hide;

	public TeamMatchScore(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		hide = _params.getBool("hide", false);
	}

	@Override
	public void run()
	{
		final TeamVsTeam event = (TeamVsTeam) _event;
		for(final Participant participant : event)
		{
			if(hide)
				participant.sendPacket(ExPVPMatchCCRetire.STATIC);
			else
				participant.sendPacket(new ExPVPMatchCCMyRecord(event.getKills(participant.getTeam())));
		}
	}
}
