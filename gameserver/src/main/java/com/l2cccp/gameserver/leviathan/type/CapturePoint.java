package com.l2cccp.gameserver.leviathan.type;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.l2cccp.commons.collections.JoinedIterator;
import com.l2cccp.commons.time.cron.SchedulingPattern;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.maps.Point;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.model.base.TeamType;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CapturePoint extends AbstractLeviathan
{
	protected final SchedulingPattern pattern;
	private final List<Participant> red_list, blue_list;
	private final AtomicInteger red_point, blue_point;
	private final AtomicInteger last_add;
	private TeamType winner;

	public CapturePoint(LeviathanTemplate template)
	{
		super(template);
		pattern = new SchedulingPattern(template.getCron());
		red_list = new ArrayList<Participant>();
		blue_list = new ArrayList<Participant>();
		red_point = new AtomicInteger(0);
		blue_point = new AtomicInteger(0);
		last_add = new AtomicInteger(0);
	}

	@Override
	public Iterator<Participant> iterator()
	{
		return new JoinedIterator<Participant>(blue_list.iterator(), red_list.iterator());
	}

	@Override
	public void start()
	{
		// do nothing
	}

	@Override
	public void stop()
	{
		red_list.clear();
		blue_list.clear();
		red_point.set(0);
		blue_point.set(0);
		super.stop();
	}

	@Override
	public void setTeam(final Participant participant)
	{
		if(last_add.getAndIncrement() % 2 == 0)
		{
			red_list.add(participant);
			participant.setTeam(TeamType.RED);
		}
		else
		{
			blue_list.add(participant);
			participant.setTeam(TeamType.BLUE);
		}
	}

	@Override
	public void reCalcStart(final boolean init)
	{
		stopActions();

		if(pattern != null)
		{
			final long end = getTimeStop();
			_calendar.setTimeInMillis(pattern.next(end > 0 ? end : System.currentTimeMillis()));
		}

		registerActions(init);

		if(!init)
			logInfo();
	}

	@SuppressWarnings("incomplete-switch")
	private void updatePoints(final TeamType team)
	{
		switch(team)
		{
			case BLUE:
			{
				blue_point.incrementAndGet();
				break;
			}
			case RED:
			{
				red_point.incrementAndGet();
				break;
			}
		}
	}

	@Override
	public void finish()
	{
		final int blue = blue_point.get();
		final int red = red_point.get();
		if(blue > red)
			setWinner(TeamType.BLUE);
		else if(red > blue)
			setWinner(TeamType.RED);
		else
			setWinner(TeamType.NONE);
	}

	private void setWinner(final TeamType winner)
	{
		this.winner = winner;
	}

	@Override
	public TeamType getWinnerTeam()
	{
		return winner;
	}

	@Override
	public void die(final Participant killer, final Participant victim)
	{
		// do nothing
	}

	@Override
	public void kill(final Participant killer, final Participant target)
	{
		final TeamType team = killer.getTeam();
		updatePoints(team);
	}

	public int getKills(final TeamType team)
	{
		switch(team)
		{
			case BLUE:
				return blue_point.get();
			case RED:
				return red_point.get();
			default:
				return 0;
		}
	}

	public List<Participant> getTeam(final int team)
	{
		switch(TeamType.getById(team))
		{
			case BLUE:
				return blue_list;
			case RED:
				return red_list;
			default:
				return null;
		}
	}

	@Override
	public Point getPoint(final int team)
	{
		final List<Point> points = getMap().getPoints(team);
		final Point point = points.get(Rnd.get(0, points.size() - 1));

		return point;
	}

	@Override
	public boolean checkWinner()
	{
		final int blue = blue_list.size(), red = red_list.size();
		if(blue > 0 && red > 0)
			return false;

		else if(blue < 1)
			setWinner(TeamType.RED);
		else if(red < 1)
			setWinner(TeamType.BLUE);

		return true;
	}

	@Override
	public void remove(Participant victim)
	{
		final TeamType team = victim.getTeam();
		victim.restore();
		if(team == TeamType.BLUE)
			blue_list.remove(victim);
		else
			red_list.remove(victim);
	}
}