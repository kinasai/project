package com.l2cccp.gameserver.leviathan.component;

import com.l2cccp.gameserver.leviathan.enums.Reward;
import com.l2cccp.gameserver.leviathan.interfaces.IReward;
import com.l2cccp.gameserver.model.Player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FameRewardInfo extends AbstractComponent implements IReward
{
	private final int count;
	private final Reward type;
	private final double chance;
	private int position;

	public FameRewardInfo(final int count, final double chance, final Reward type)
	{
		this.count = count;
		this.chance = chance;
		this.type = type;
	}

	public int getCount()
	{
		return count;
	}

	public double getChance()
	{
		return chance;
	}

	@Override
	public void give(final Player player)
	{
		type.give(player, this);
	}

	public void setPosition(final int position)
	{
		this.position = position;
	}

	@Override
	public int getPosition()
	{
		return position;
	}
}