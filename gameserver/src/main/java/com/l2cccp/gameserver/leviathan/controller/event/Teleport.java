package com.l2cccp.gameserver.leviathan.controller.event;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.maps.Point;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.AbnormalEffect;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Teleport extends Controller
{
	private final boolean paralyze;
	private final boolean dispel;

	public Teleport(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		paralyze = _params.getBool("paralyze", false);
		dispel = _params.getBool("dispel", false);
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event)
		{
			final Player player = participant.getReference().get();
			if(player != null)
			{
				if(paralyze)
					paralyze(player);

				if(dispel)
					dispelBuffs(player);

				final Point point = _event.getPoint(player.getTeam().ordinal());
				final Location position = new Location(point.getX(), point.getY(), point.getZ());
				player.teleToLocation(position, _event.getReflection());
			}
		}
	}

	private void paralyze(final Player player)
	{
		if(!player.isRooted())
		{
			player.startRooted();
			player.startAbnormalEffect(AbnormalEffect.ROOT);
		}

		Servitor pet = player.getServitor();
		if(pet != null)
		{
			if(!pet.isRooted())
			{
				pet.startRooted();
				pet.startAbnormalEffect(AbnormalEffect.ROOT);
			}
		}
	}

	private void dispelBuffs(final Player player)
	{
		for(final Effect e : player.getEffectList().getAllEffects())
		{
			if(!e.getSkill().getTemplate().isOffensive() && !e.getSkill().getTemplate().isNewbie() && e.isCancelable() && !e.getSkill().getTemplate().isPreservedOnDeath())
			{
				player.sendPacket(new SystemMessage(SystemMsg.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(e.getSkill().getId(), e.getSkill().getLevel()));
				e.exit();
			}
		}

		final Servitor servitor = player.getServitor();
		if(servitor != null)
		{
			for(final Effect e : servitor.getEffectList().getAllEffects())
			{
				if(!e.getSkill().getTemplate().isOffensive() && !e.getSkill().getTemplate().isNewbie() && e.isCancelable() && !e.getSkill().getTemplate().isPreservedOnDeath())
					e.exit();
			}
		}
	}
}