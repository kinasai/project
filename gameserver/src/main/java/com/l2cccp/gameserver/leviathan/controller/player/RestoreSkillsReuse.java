package com.l2cccp.gameserver.leviathan.controller.player;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.s2c.SkillCoolTime;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.skills.TimeStamp;
import com.l2cccp.gameserver.tables.SkillTable;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RestoreSkillsReuse extends Controller
{
	public RestoreSkillsReuse(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event)
		{
			final Player player = participant.get();
			for(final TimeStamp timeStamp : participant.getSkillReuses().values())
			{
				if(timeStamp.hasNotPassed())
				{
					final SkillEntry skill = SkillTable.getInstance().getSkillEntry(timeStamp.getId(), timeStamp.getLevel());
					final long reuse = timeStamp.getReuseCurrent();
					player.disableSkill(skill, reuse);
				}
			}

			player.sendPacket(new SkillCoolTime(player));
		}
	}
}