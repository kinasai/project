package com.l2cccp.gameserver.leviathan.listeners.team;

import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.listeners.AbstractListener;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerExitListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OnExit extends AbstractListener implements OnPlayerExitListener
{
	private final boolean announce;

	public OnExit(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		announce = _params.getBool("announce", false);
	}

	@Override
	public void onPlayerExit(final Player player)
	{
		final List<Participant> participants = event.getTeam(player.getTeam().ordinal());
		if(participants != null)
		{
			final Participant object = event.getMembers().get(player.getObjectId());
			object.restore();
			participants.remove(object);

			if(announce)
			{
				final String name = player.getName();
				for(final Participant participant : participants)
				{
					final Player member = participant.get();
					if(member != null)
						member.sendMessage(new CustomMessage("leviathan.event.deserter").addString(name));
				}
			}
		}

		call();
	}
}