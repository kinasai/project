package com.l2cccp.gameserver.leviathan.controller.player;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.s2c.SkillList;
import com.l2cccp.gameserver.skills.SkillEntry;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RestoreSkills extends Controller
{
	public RestoreSkills(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event)
		{
			final Player player = participant.get();
			for(final SkillEntry skill : participant.getSkills())
				player.addSkill(skill);

			player.sendPacket(new SkillList(player));
		}
	}
}