package com.l2cccp.gameserver.leviathan.controller.announcements;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.TimeUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TimeInfo extends Controller
{
	private final String _msg;
	private final int _seconds;
	private final ChatType _chat;

	public TimeInfo(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		_msg = _params.getString("msg");
		_seconds = _params.getInteger("seconds");
		_chat = ChatType.valueOf(_params.getString("chat"));
	}

	@Override
	public void run()
	{
		final LeviathanTemplate template = _event.getTemplate();

		for(final Player player : GameObjectsStorage.getPlayers())
		{
			if(_event.checkLevel(player))
			{
				final Language language = player.getLanguage();
				final CustomMessage message = new CustomMessage(_msg).addString(TimeUtils.formatTime(_seconds, language));
				player.sendPacket(new Say2(0, _chat, template.getName(language), message.toString(language), null));
			}
		}
	}
}