package com.l2cccp.gameserver.leviathan.controller.object.spawn;

import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.NpcInfo;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class AllNpcs extends AbstractSpawnController
{
	public AllNpcs(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		final Map<String, List<NpcInfo>> npcs = _event.getTemplate().getNpcs();
		for(final List<NpcInfo> list : npcs.values())
		{
			for(final NpcInfo npc : list)
				npc.spawn(_event, reflection ? _event.getReflection() : ReflectionManager.DEFAULT);
		}
	}
}