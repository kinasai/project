package com.l2cccp.gameserver.leviathan.listeners.zone;

import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.objects.CapturedBaseData;
import com.l2cccp.gameserver.leviathan.objects.CapturedFlagData;
import com.l2cccp.gameserver.leviathan.type.FlagAndBase;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.items.attachment.FlagItemAttachment;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OnCapturedBaseEnter extends AbstractZoneListener
{
	private CapturedBaseData base;

	public OnCapturedBaseEnter(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void onZoneEnter(final Zone zone, final Creature actor)
	{
		if(!actor.isPlayer() || actor.getTeam() != base.getTeam())
			return;

		final Player player = actor.getPlayer();
		final FlagItemAttachment weapon = player.getActiveWeaponFlagAttachment();
		if(weapon == null)
			return;

		final FlagAndBase event = (FlagAndBase) _event;
		final List<CapturedFlagData> flags = event.getFlags(base.getTeam().revert());
		if(flags == null || flags.isEmpty() || !flags.contains(weapon))
			return;

		for(final CapturedFlagData flag : flags)
		{
			if(weapon == flag)
			{
				if(base.delivered())
				{
					event.setWinner(base.getTeam());
					call();
				}
				else
					weapon.onDeath(player, null);
				break;
			}
		}
	}

	@Override
	public void onZoneLeave(final Zone zone, final Creature actor)
	{
		// do nothing
	}

	public void setBase(final CapturedBaseData base)
	{
		this.base = base;
	}
}