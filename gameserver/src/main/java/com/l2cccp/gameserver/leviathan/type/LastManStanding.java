package com.l2cccp.gameserver.leviathan.type;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.l2cccp.commons.time.cron.SchedulingPattern;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.maps.Point;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.model.base.TeamType;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class LastManStanding extends AbstractLeviathan
{
	protected final SchedulingPattern pattern;
	private final List<Participant> list;
	private TeamType winner;

	public LastManStanding(LeviathanTemplate template)
	{
		super(template);
		pattern = new SchedulingPattern(template.getCron());
		list = new ArrayList<Participant>();
	}

	@Override
	public Iterator<Participant> iterator()
	{
		return list.iterator();
	}

	@Override
	public void start()
	{
		// do nothing
	}

	@Override
	public void stop()
	{
		list.clear();
		super.stop();
	}

	@Override
	public void setTeam(final Participant participant)
	{
		list.add(participant);
		participant.setTeam(TeamType.BLUE);
	}

	@Override
	public void reCalcStart(final boolean init)
	{
		stopActions();

		if(pattern != null)
		{
			final long end = getTimeStop();
			_calendar.setTimeInMillis(pattern.next(end > 0 ? end : System.currentTimeMillis()));
		}

		registerActions(init);
		if(!init)
			logInfo();
	}

	@Override
	public void finish()
	{
		setWinner(TeamType.NONE);
	}

	private void setWinner(final TeamType winner)
	{
		this.winner = winner;
	}

	@Override
	public TeamType getWinnerTeam()
	{
		return winner;
	}

	@Override
	public void die(final Participant killer, final Participant victim)
	{
		// do nothing
	}

	@Override
	public void kill(final Participant killer, final Participant target)
	{
		// do nothing
	}

	@Override
	public Point getPoint(final int team)
	{
		final List<Point> points = getMap().getPoints(team);
		final Point point = points.get(Rnd.get(0, points.size() - 1));

		return point;
	}

	@Override
	public boolean checkWinner()
	{
		final boolean size = list.size() < 2;
		if(size)
			setWinner(TeamType.BLUE);

		return size;
	}

	@Override
	public List<Participant> getTeam(int team)
	{
		return list;
	}

	@Override
	public void remove(Participant victim)
	{
		list.remove(victim);
	}
}