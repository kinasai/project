package com.l2cccp.gameserver.leviathan.controller.player;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.s2c.SkillCoolTime;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.skills.TimeStamp;
import com.l2cccp.gameserver.tables.SkillTable;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class StoreageSkillsReuse extends Controller
{
	public StoreageSkillsReuse(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event)
		{
			final Player player = participant.get();

			for(TimeStamp timeStamp : player.getSkillReuses())
			{
				if(timeStamp.hasNotPassed())
				{
					final SkillEntry skill = SkillTable.getInstance().getSkillEntry(timeStamp.getId(), timeStamp.getLevel());
					final long endTime = timeStamp.getEndTime();
					final long rDelayOrg = timeStamp.getReuseBasic();
					participant.setReuses(skill.hashCode(), new TimeStamp(skill, endTime, rDelayOrg));
				}
			}

			player.resetReuse();
			player.updateStats();
			player.sendPacket(new SkillCoolTime(player));
		}
	}
}