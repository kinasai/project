package com.l2cccp.gameserver.leviathan.type;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.l2cccp.commons.collections.JoinedIterator;
import com.l2cccp.commons.time.cron.SchedulingPattern;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.maps.Point;
import com.l2cccp.gameserver.leviathan.objects.CapturedBaseData;
import com.l2cccp.gameserver.leviathan.objects.CapturedFlagData;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.model.base.TeamType;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FlagAndBase extends AbstractLeviathan
{
	protected final SchedulingPattern pattern;
	private final List<Participant> red_list, blue_list;
	private final Map<TeamType, List<CapturedFlagData>> flags;
	private final Map<TeamType, CapturedBaseData> bases;
	private final AtomicInteger last_add;
	private TeamType winner;

	public FlagAndBase(LeviathanTemplate template)
	{
		super(template);
		pattern = new SchedulingPattern(template.getCron());
		red_list = new ArrayList<Participant>();
		blue_list = new ArrayList<Participant>();
		last_add = new AtomicInteger(0);
		flags = new HashMap<TeamType, List<CapturedFlagData>>();
		bases = new HashMap<TeamType, CapturedBaseData>();
	}

	@Override
	public Iterator<Participant> iterator()
	{
		return new JoinedIterator<Participant>(blue_list.iterator(), red_list.iterator());
	}

	@Override
	public void start()
	{
		// do nothing
	}

	@Override
	public void stop()
	{
		red_list.clear();
		blue_list.clear();
		super.stop();
	}

	public void addFlag(final CapturedFlagData flag)
	{
		final TeamType team = flag.getTeam();
		List<CapturedFlagData> list = flags.get(team);
		if(list == null)
			flags.put(team, list = new ArrayList<CapturedFlagData>());

		list.add(flag);
	}

	public List<CapturedFlagData> getFlags(final TeamType team)
	{
		return flags.get(team);
	}

	public void addBase(final CapturedBaseData base)
	{
		bases.put(base.getTeam(), base);
	}

	public CapturedBaseData getBase(final TeamType team)
	{
		return bases.get(team);
	}

	@Override
	public void setTeam(final Participant participant)
	{
		if(last_add.getAndIncrement() % 2 == 0)
		{
			red_list.add(participant);
			participant.setTeam(TeamType.RED);
		}
		else
		{
			blue_list.add(participant);
			participant.setTeam(TeamType.BLUE);
		}
	}

	@Override
	public void reCalcStart(final boolean init)
	{
		stopActions();

		if(pattern != null)
		{
			final long end = getTimeStop();
			_calendar.setTimeInMillis(pattern.next(end > 0 ? end : System.currentTimeMillis()));
		}

		registerActions(init);

		if(!init)
			logInfo();
	}

	@Override
	public void finish()
	{
		setWinner(TeamType.NONE);
	}

	public void setWinner(final TeamType winner)
	{
		this.winner = winner;
	}

	@Override
	public TeamType getWinnerTeam()
	{
		return winner;
	}

	@Override
	public void die(final Participant killer, final Participant victim)
	{
		// do nothing
	}

	@Override
	public void kill(final Participant killer, final Participant target)
	{
		// do nothing
	}

	public List<Participant> getTeam(final int team)
	{
		switch(TeamType.getById(team))
		{
			case BLUE:
				return blue_list;
			case RED:
				return red_list;
			default:
				return null;
		}
	}

	@Override
	public Point getPoint(final int team)
	{
		final List<Point> points = getMap().getPoints(team);
		final Point point = points.get(Rnd.get(0, points.size() - 1));

		return point;
	}

	@Override
	public boolean checkWinner()
	{
		final int blue = blue_list.size(), red = red_list.size();
		if(blue > 0 && red > 0)
			return false;

		else if(blue < 1)
			setWinner(TeamType.RED);
		else if(red < 1)
			setWinner(TeamType.BLUE);

		return true;
	}

	@Override
	public void remove(Participant victim)
	{
		final TeamType team = victim.getTeam();
		victim.restore();
		if(team == TeamType.BLUE)
			blue_list.remove(victim);
		else
			red_list.remove(victim);
	}
}