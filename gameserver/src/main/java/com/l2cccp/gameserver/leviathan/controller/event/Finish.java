package com.l2cccp.gameserver.leviathan.controller.event;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Finish extends Controller
{
	public Finish(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		_event.finish();
	}
}