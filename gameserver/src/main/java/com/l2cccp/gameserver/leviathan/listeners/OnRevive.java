package com.l2cccp.gameserver.leviathan.listeners;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.listener.actor.OnReviveListener;
import com.l2cccp.gameserver.model.Creature;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OnRevive extends AbstractListener implements OnReviveListener
{
	private final boolean heal;
	private final double hp, mp, cp;

	public OnRevive(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		heal = _params.getBool("heal", false);
		hp = _params.getDouble("hp", 100D);
		mp = _params.getDouble("mp", 100D);
		cp = _params.getDouble("cp", 100D);
	}

	@Override
	public void onRevive(final Creature actor)
	{
		if(heal)
		{
			actor.setCurrentCp((actor.getMaxCp() * cp) / 100);
			actor.setCurrentMp((actor.getMaxMp() * mp) / 100);
			actor.setCurrentHp((actor.getMaxHp() * hp) / 100, true);
		}

		event.stopRevive(actor.getObjectId());
		call();
	}
}