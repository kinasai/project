package com.l2cccp.gameserver.leviathan.listeners.reward;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.enums.RewardType;
import com.l2cccp.gameserver.leviathan.interfaces.IReward;
import com.l2cccp.gameserver.leviathan.listeners.OnDie;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Creature;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OnDiePositionReward extends OnDie
{
	public OnDiePositionReward(AbstractLeviathan event, Element element)
	{
		super(event, element);
	}

	@Override
	public void onDeath(Creature one, Creature two)
	{
		final Participant victim = event.getMembers().get(one.getObjectId());
		final int position = event.getTeam(victim.getTeam().ordinal()).size();
		for(final IReward reward : event.getTemplate().getRewards(RewardType.POSITION))
		{
			if(reward.getPosition() == position)
				reward.give(victim.get());
		}

		super.onDeath(one, two);
	}
}
