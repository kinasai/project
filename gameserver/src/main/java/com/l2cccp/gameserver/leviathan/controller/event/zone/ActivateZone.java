package com.l2cccp.gameserver.leviathan.controller.event.zone;

import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.utils.ReflectionUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ActivateZone extends Controller
{
	public ActivateZone(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		final List<Zone> zones = new ArrayList<Zone>();
		for(final String name : _event.getMap().getZones())
		{
			final Zone zone = ReflectionUtils.getZone(name);
			zone.setReflection(_event.getReflection());
			zone.setActive(true);
			zones.add(zone);
		}

		_event.setZones(zones);
	}
}