package com.l2cccp.gameserver.leviathan.controller.object.despawn;

import java.util.List;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.CapturedFlagData;
import com.l2cccp.gameserver.leviathan.type.FlagAndBase;
import com.l2cccp.gameserver.model.base.TeamType;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CapturedFlag extends Controller
{
	private final TeamType team;

	public CapturedFlag(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		team = TeamType.valueOf(_params.getString("team"));
	}

	@Override
	public void run()
	{
		final FlagAndBase event = (FlagAndBase) _event;
		final List<CapturedFlagData> flags = event.getFlags(team);
		if(flags == null || flags.isEmpty())
		{
			warn("Can't find flag " + team.name());
			return;
		}

		for(final CapturedFlagData flag : flags)
			flag.despawn();
	}
}