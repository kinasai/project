package com.l2cccp.gameserver.leviathan.controller.inventory;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.LockType;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class LockItems extends Controller
{
	private final int[] items;
	private final LockType type;

	public LockItems(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		items = _params.getIntegerArray("items");
		type = LockType.valueOf(_params.getString("type", "INCLUDE"));
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event)
		{
			final Player player = participant.get();
			player.getInventory().lockItems(type, items);
		}
	}
}