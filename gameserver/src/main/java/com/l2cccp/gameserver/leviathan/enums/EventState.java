package com.l2cccp.gameserver.leviathan.enums;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum EventState
{
	PREPARE("leviathan.event.registration.not.started"),
	REGISTER(null),
	REGISTER_CLOSED("leviathan.event.registration.closed"),
	RUN("leviathan.event.registration.stop");

	private final String message;

	public final String getMessage()
	{
		return message;
	}

	private EventState(final String message)
	{
		this.message = message;
	}
}