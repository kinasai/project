package com.l2cccp.gameserver.leviathan.controller.event.zone;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.model.Zone;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class DectivateZone extends Controller
{
	public DectivateZone(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		for(final Zone zone : _event.getZones())
			zone.setActive(false);

		_event.getZones().clear();
	}
}