package com.l2cccp.gameserver.leviathan.controller.event;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PartyManager extends Controller
{
	public PartyManager(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		_event.setTeam(null);
	}
}