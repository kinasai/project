package com.l2cccp.gameserver.leviathan.utility;

import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.leviathan.action.CallAction;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ActionExecutor extends RunnableImpl
{
	private final CallAction _action;

	public ActionExecutor(final CallAction action)
	{
		_action = action;
	}

	@Override
	public void runImpl() throws Exception
	{
		_action.call();
	}
}