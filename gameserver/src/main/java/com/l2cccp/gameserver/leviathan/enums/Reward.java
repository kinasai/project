package com.l2cccp.gameserver.leviathan.enums;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.leviathan.component.AbstractComponent;
import com.l2cccp.gameserver.leviathan.component.FameRewardInfo;
import com.l2cccp.gameserver.leviathan.component.HeroRewardInfo;
import com.l2cccp.gameserver.leviathan.component.ItemRewardInfo;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.Hero;
import com.l2cccp.gameserver.utils.ItemFunctions;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum Reward
{
	FAME
	{
		@Override
		public void give(final Player player, final AbstractComponent reward)
		{
			final FameRewardInfo fame = (FameRewardInfo) reward;
			player.setFame(player.getFame() + fame.getCount(), "Leviathan Reward");
		}
	},
	HERO
	{
		@Override
		public void give(final Player player, AbstractComponent reward)
		{
			final HeroRewardInfo hero = (HeroRewardInfo) reward;
			if(Rnd.chance(hero.getChance()))
			{
				final long time = hero.getTime();
				player.setVar("hasFakeHero", 1, time);
				if(hero.canEquipItems())
					player.setVar("hasFakeHeroItems", 1, time);
				if(hero.canHeroChat())
					player.setVar("hasFakeHeroChat", 1, time);
				if(hero.giveSkills())
				{
					player.setVar("hasFakeHeroSkills", 1, time);
					Hero.addSkills(player);
				}
			}
		}
	},
	ITEM
	{
		@Override
		public void give(final Player player, final AbstractComponent reward)
		{
			final ItemRewardInfo item = (ItemRewardInfo) reward;
			final long count = calculate(item);
			if(count < 1)
				return;

			ItemFunctions.addItem(player, item.getItemId(), count, true);
		}
	};

	public abstract void give(final Player player, final AbstractComponent reward);

	private final static long calculate(final ItemRewardInfo reward)
	{
		long count = 0;
		final boolean success = Rnd.chance(reward.getChance());
		if(success)
			count = Rnd.get(reward.getMin(), reward.getMax());

		return count;
	}
}