package com.l2cccp.gameserver.leviathan.component.maps;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Point
{
	private final int team, x, y, z;

	public Point(final int team, final int x, final int y, final int z)
	{
		this.team = team;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public int getTeam()
	{
		return team;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}

	public int getZ()
	{
		return z;
	}
}
