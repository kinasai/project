package com.l2cccp.gameserver.leviathan.controller;

import com.l2cccp.commons.logging.LoggerObject;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public abstract class AbstractController extends LoggerObject
{
	public abstract void run();
}