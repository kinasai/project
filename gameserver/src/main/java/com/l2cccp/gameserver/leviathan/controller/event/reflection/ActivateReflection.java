package com.l2cccp.gameserver.leviathan.controller.event.reflection;

import org.dom4j.Element;

import com.l2cccp.gameserver.data.xml.holder.InstantZoneHolder;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.maps.EventMap;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.templates.InstantZone;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ActivateReflection extends Controller
{
	public ActivateReflection(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		final EventMap map = _event.getMap();
		final InstantZone instant = InstantZoneHolder.getInstance().getInstantZone(map.getInstances());
		final Reflection reflection = new Reflection();
		reflection.init(instant);
		_event.setReflection(reflection);
	}
}