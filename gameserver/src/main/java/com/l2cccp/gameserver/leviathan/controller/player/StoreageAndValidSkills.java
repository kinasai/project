package com.l2cccp.gameserver.leviathan.controller.player;

import org.apache.commons.lang3.ArrayUtils;
import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.skills.SkillEntry;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class StoreageAndValidSkills extends Controller
{
	private final int[] forbidden;

	public StoreageAndValidSkills(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		forbidden = _params.getIntegerArray("forbidden");
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event)
		{
			final Player player = participant.get();
			for(final SkillEntry skill : player.getAllSkills())
			{
				if(ArrayUtils.contains(forbidden, skill.getId()))
				{
					participant.saveSkill(skill);
					player.removeSkill(skill);
				}
			}
		}
	}
}