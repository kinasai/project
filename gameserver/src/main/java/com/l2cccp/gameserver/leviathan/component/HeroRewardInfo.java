package com.l2cccp.gameserver.leviathan.component;

import com.l2cccp.gameserver.leviathan.enums.Reward;
import com.l2cccp.gameserver.leviathan.interfaces.IReward;
import com.l2cccp.gameserver.model.Player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class HeroRewardInfo extends AbstractComponent implements IReward
{
	private final long time;
	private final boolean chat, items, skills;
	private final Reward type;
	private final double chance;
	private int position;

	public HeroRewardInfo(final long time, final boolean chat, final boolean items, final boolean skills, final double chance, final Reward type)
	{
		this.time = time;
		this.chat = chat;
		this.items = items;
		this.skills = skills;
		this.chance = chance;
		this.type = type;
	}

	public long getTime()
	{
		return time;
	}

	public boolean canHeroChat()
	{
		return chat;
	}

	public boolean canEquipItems()
	{
		return items;
	}

	public boolean giveSkills()
	{
		return skills;
	}

	public double getChance()
	{
		return chance;
	}

	@Override
	public void give(final Player player)
	{
		type.give(player, this);
	}

	public void setPosition(final int position)
	{
		this.position = position;
	}

	@Override
	public int getPosition()
	{
		return position;
	}
}