package com.l2cccp.gameserver.leviathan.objects;

import java.util.concurrent.atomic.AtomicInteger;

import com.l2cccp.gameserver.leviathan.listeners.AbstractListener;
import com.l2cccp.gameserver.leviathan.listeners.inventory.AbstractEquipListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.items.PcInventory;
import com.l2cccp.gameserver.network.l2.components.IBroadcastPacket;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Participant extends CloneData
{
	private final AtomicInteger kills, death;
	private TeamType team = TeamType.NONE;

	public Participant(final Player player)
	{
		super(player);
		kills = new AtomicInteger(0);
		death = new AtomicInteger(0);
	}

	public TeamType getTeam()
	{
		return team;
	}

	public void setTeam(final TeamType team)
	{
		this.team = team;
		final Player player = get();
		if(player != null)
			player.setTeam(team);
	}

	public void sendPacket(final IBroadcastPacket packet)
	{
		final Player player = get();
		if(player != null)
			player.sendPacket(packet);
	}

	/**
	 * Kills & Death
	 */
	public void incKills()
	{
		kills.incrementAndGet();
	}

	public int getKills()
	{
		return kills.get();
	}

	public void incDeath()
	{
		death.incrementAndGet();
	}

	public int getDeath()
	{
		return death.get();
	}

	/**
	 * Listeners
	 */
	public void addListener(final AbstractListener listener)
	{
		final Player player = getReference().get();
		if(player != null)
			player.addListener(listener);
	}

	public void removeListener(final AbstractListener listener)
	{
		final Player player = getReference().get();
		if(player != null)
			player.removeListener(listener);
	}

	public void addListener(final AbstractEquipListener listener)
	{
		final Player player = getReference().get();
		if(player != null)
		{
			final PcInventory inv = player.getInventory();
			inv.addListener(listener);
		}
	}

	public void removeListener(final AbstractEquipListener listener)
	{
		final Player player = getReference().get();
		if(player != null)
		{
			final PcInventory inv = player.getInventory();
			inv.removeListener(listener);
		}
	}
}