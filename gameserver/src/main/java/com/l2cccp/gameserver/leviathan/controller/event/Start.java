package com.l2cccp.gameserver.leviathan.controller.event;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.skills.AbnormalEffect;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Start extends Controller
{
	private final boolean unparalyze;

	public Start(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
		unparalyze = _params.getBool("unparalyze", false);
	}

	@Override
	public void run()
	{
		if(unparalyze)
		{
			for(final Participant participant : _event)
			{

				final Player player = participant.getReference().get();
				if(player != null)
					unparalyze(player);
			}
		}

		_event.start();
	}

	private void unparalyze(final Player player)
	{
		if(player.isRooted())
		{
			player.stopRooted();
			player.stopAbnormalEffect(AbnormalEffect.ROOT);
		}

		Servitor pet = player.getServitor();
		if(pet != null)
		{
			if(pet.isRooted())
			{
				pet.stopRooted();
				pet.stopAbnormalEffect(AbnormalEffect.ROOT);
			}
		}
	}
}