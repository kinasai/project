package com.l2cccp.gameserver.leviathan.condition;

import org.dom4j.Element;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ItemCheck extends AbstractCondition
{
	private final int id;
	private final long count;

	public ItemCheck(final Element element)
	{
		super(element);
		this.id = Integer.parseInt(element.attributeValue("id"));
		this.count = Long.parseLong(element.attributeValue("count"));
	}

	@Override
	public boolean check(Player player, Creature target)
	{
		if(player == null)
			return false;

		return player.getInventory().getCountOf(id) >= count;
	}

	@Override
	public String getFailedMessage(Language language)
	{
		return new CustomMessage("leviathan.condition.item.check").addString(Util.formatPay(language, count, id)).toString(language);
	}

	@Override
	public void replace(final HtmlMessage html)
	{
		html.replace("%id%", String.valueOf(id));
		html.replace("%count%", String.valueOf(count));
	}
}