package com.l2cccp.gameserver.leviathan.controller.event.zone;

import java.util.List;

import org.dom4j.Element;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.data.xml.holder.LeviathanMapsHolder;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.component.maps.EventMap;
import com.l2cccp.gameserver.leviathan.controller.Controller;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class GenerateMap extends Controller
{
	public GenerateMap(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		final List<EventMap> maps = LeviathanMapsHolder.getInstance().getMaps(_event.getTemplate().getEventId());
		final EventMap map = maps.get(Rnd.get(0, maps.size() - 1));
		_event.setMap(map);
	}
}