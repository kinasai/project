package com.l2cccp.gameserver.leviathan.objects;

import java.util.ArrayList;
import java.util.List;

import org.napile.primitive.maps.IntObjectMap;
import org.napile.primitive.maps.impl.CHashIntObjectMap;

import com.l2cccp.commons.lang.reference.HardReference;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.EffectList;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.skills.TimeStamp;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CloneData
{
	protected HardReference<Player> reference;
	private final int obj;
	private Location location;
	private double cp, hp, mp;
	private List<SkillEntry> skills;
	private IntObjectMap<TimeStamp> reuses;
	private List<Effect> effects;

	public CloneData(final Player player)
	{
		this.reference = player.getRef();
		this.obj = player.getObjectId();
		this.skills = new ArrayList<SkillEntry>();
		this.reuses = new CHashIntObjectMap<TimeStamp>();
		this.effects = new ArrayList<Effect>();
	}

	public int getObjectId()
	{
		return obj;
	}

	public HardReference<Player> getReference()
	{
		return reference;
	}

	public Player get()
	{
		Player player = reference.get();
		if(player == null)
		{
			player = GameObjectsStorage.getPlayer(obj);
			if(player != null)
				reference = player.getRef();
		}

		return player;
	}

	public void store()
	{
		final Player player = get();

		if(player != null)
		{
			location = player.getLoc();
			player._stablePoint = location;
			cp = player.getCurrentCp();
			hp = player.getCurrentHp();
			mp = player.getCurrentMp();
			player.isInEvent(true);
		}
	}

	public void restore()
	{
		final Player player = get();
		player.isInEvent(false);
		final EffectList list = player.getEffectList();
		if(list.getAllEffects().size() > 0)
			list.stopAllEffects();

		for(Effect effect : effects)
			list.addEffect(effect);

		player.updateEffectIcons();

		if(hp < 1)
		{
			player.setCurrentHp(hp, false);
			player.onDeath(null);
		}
		else
		{
			if(player.isDead())
				player.doRevive();
			player.setCurrentHp(hp, true);
		}

		player.setCurrentCp(cp);
		player.setCurrentMp(mp);

		player.setTeam(TeamType.NONE);
		player.updateStats();
		player.teleToLocation(location, ReflectionManager.DEFAULT);
		player.broadcastUserInfo(true);
		player.sendChanges();
	}

	public List<SkillEntry> getSkills()
	{
		return skills;
	}

	public void saveSkill(final SkillEntry skill)
	{
		skills.add(skill);
	}

	public List<Effect> getEffects()
	{
		return effects;
	}

	public void saveEffect(final Effect effect)
	{
		effects.add(effect);
	}

	public IntObjectMap<TimeStamp> getSkillReuses()
	{
		return reuses;
	}

	public void setReuses(final int hash, final TimeStamp stamp)
	{
		reuses.put(hash, stamp);
	}

	public void info()
	{
		final Player player = get();
		player.sendMessage("Location: " + location != null ? location.toString() : "Is NULL");
		player.sendMessage("HP: " + hp + ", CP: " + cp + ", mp: " + mp);
	}
}