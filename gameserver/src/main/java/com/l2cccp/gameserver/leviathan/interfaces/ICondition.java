package com.l2cccp.gameserver.leviathan.interfaces;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public interface ICondition
{
	boolean check(final Player player, final Creature target);

	String getFailedHtml();

	String getFailedMessage(final Language language);

	void replace(final HtmlMessage html);

	//String getInfo(final Language language);
}