package com.l2cccp.gameserver.leviathan.listeners;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.listener.actor.OnKillListener;
import com.l2cccp.gameserver.model.Creature;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OnPlayerKill extends AbstractListener implements OnKillListener
{
	public OnPlayerKill(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void onKill(final Creature one, Creature two)
	{
		if(!one.isPlayable() || !two.isPlayer())
			return;

		final Participant killer = event.getMembers().get(one.getObjectId());
		final Participant victim = event.getMembers().get(two.getObjectId());
		killer.incKills();
		event.kill(killer, victim);
		call();
	}

	@Override
	public boolean ignorePetOrSummon()
	{
		return true;
	}
}