package com.l2cccp.gameserver.leviathan.controller.player;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.listeners.AbstractListener;
import com.l2cccp.gameserver.leviathan.listeners.inventory.AbstractEquipListener;
import com.l2cccp.gameserver.leviathan.objects.Participant;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class AddListeners extends Controller
{
	public AddListeners(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void run()
	{
		for(final Participant participant : _event)
		{
			for(final AbstractListener listener : _event.getTemplate().getListeners())
				participant.addListener(listener);
			for(final AbstractEquipListener listener : _event.getTemplate().getEquipListeners())
				participant.addListener(listener);
		}
	}
}