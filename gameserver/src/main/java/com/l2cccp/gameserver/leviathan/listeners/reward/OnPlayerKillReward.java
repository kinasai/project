package com.l2cccp.gameserver.leviathan.listeners.reward;

import org.dom4j.Element;

import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.enums.RewardType;
import com.l2cccp.gameserver.leviathan.interfaces.IReward;
import com.l2cccp.gameserver.leviathan.listeners.OnPlayerKill;
import com.l2cccp.gameserver.leviathan.objects.Participant;
import com.l2cccp.gameserver.model.Creature;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OnPlayerKillReward extends OnPlayerKill
{
	public OnPlayerKillReward(final AbstractLeviathan event, final Element element)
	{
		super(event, element);
	}

	@Override
	public void onKill(final Creature one, Creature two)
	{
		if(!one.isPlayable() || !two.isPlayer())
			return;

		final Participant killer = event.getMembers().get(one.getObjectId());
		for(final IReward reward : event.getTemplate().getRewards(RewardType.KILL))
			reward.give(killer.get());

		super.onKill(one, two);
	}
}