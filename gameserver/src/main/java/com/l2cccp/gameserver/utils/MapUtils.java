package com.l2cccp.gameserver.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class MapUtils
{
	/**
	 * Сортировка мапы по значениям
	 */
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(final Map<K, V> map)
	{
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>(){
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2)
			{
				return (o1.getValue()).compareTo(o2.getValue());
			}
		});

		Map<K, V> result = new LinkedHashMap<K, V>();
		for(Map.Entry<K, V> entry : list)
			result.put(entry.getKey(), entry.getValue());

		return result;
	}

	/**
	 * Сортировка мапы по ключу
	 */
	public static <K extends Comparable<? super K>, V> Map<K, V> sortByKey(final Map<K, V> map)
	{
		List<K> list = new ArrayList<K>(map.keySet());
		Collections.sort(list);

		Map<K, V> result = new LinkedHashMap<K, V>();
		for(K entry : list)
			result.put(entry, map.get(entry));

		return result;
	}
}