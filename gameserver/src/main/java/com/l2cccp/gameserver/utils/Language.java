package com.l2cccp.gameserver.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author VISTALL
 * @date 13:50/29.03.2011
 */
public enum Language
{
	ENGLISH(1, "en"),
	RUSSIAN(8, "ru");

	private static final Logger _log = LoggerFactory.getLogger(Language.class);
	public static final Language[] VALUES = Language.values();

	private int _clientIndex;
	private String _shortName;

	Language(int clientIndex, String shortName)
	{
		_clientIndex = clientIndex;
		_shortName = shortName;
	}

	public String getShortName()
	{
		return _shortName;
	}

	public static Language valueOf(int index)
	{
		for(Language l : VALUES)
			if(l._clientIndex == index)
				return l;

		_log.info("Unknown lang by id -> " + index);

		return ENGLISH;
	}

	public static Language findByShortName(String shortName)
	{
		for(Language l : VALUES)
			if(l.getShortName().equals(shortName))
				return l;
		return null;
	}
}
