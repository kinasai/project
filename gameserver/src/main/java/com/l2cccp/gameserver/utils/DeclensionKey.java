package com.l2cccp.gameserver.utils;

/**
 * Ключи для склонения.
 */
public enum DeclensionKey
{
	DAYS,
	HOUR,
	MINUTES,
	SECONDS,
	PIECE,
	POINT
}
