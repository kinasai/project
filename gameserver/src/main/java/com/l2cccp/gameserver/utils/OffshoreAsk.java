package com.l2cccp.gameserver.utils;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.PrivateStoreManageListSell;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OffshoreAsk implements OnAnswerListener
{
	private boolean _packages;

	public OffshoreAsk(boolean packages)
	{
		_packages = packages;
	}

	@Override
	public void sayYes(Player player)
	{
		if(player.isInPeaceZone())
		{
			final int[] cord = Config.PVTSTORE_OFFSHORE_CORD;
			player.teleToLocation(cord[0], cord[1], cord[2]);
		}
		else
			player.sendMessage(new CustomMessage("trade.offshore.peace"));
	}

	@Override
	public void sayNo(Player player)
	{
		player.sendPacket(new PrivateStoreManageListSell(player, _packages));
	}
}
