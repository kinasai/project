package com.l2cccp.gameserver.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author VISTALL
 * @date 16:18/14.02.2011
 */
public class TimeUtils
{
	public static final SimpleDateFormat SIMPLE_FORMAT = new SimpleDateFormat("HH:mm dd.MM.yyyy");

	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

	public static String time()
	{
		return TIME_FORMAT.format(new Date(System.currentTimeMillis()));
	}

	public static String date()
	{
		return DATE_FORMAT.format(new Date(System.currentTimeMillis()));
	}

	public static String toSimpleFormat(Calendar cal)
	{
		return SIMPLE_FORMAT.format(cal.getTime());
	}

	public static String toSimpleFormat(long cal)
	{
		return SIMPLE_FORMAT.format(cal);
	}

	public static String toSimpleTime(long cal)
	{
		return TIME_FORMAT.format(cal);
	}

	public static String toSimpleDate(long cal)
	{
		return DATE_FORMAT.format(cal);
	}

	public static long parse(String time) throws ParseException
	{
		return SIMPLE_FORMAT.parse(time).getTime();
	}

	public static long addDay(int count)
	{
		long DAY = count * 60 * 60 * 24 * 1000L;
		return DAY;
	}

	public static long addHours(int count)
	{
		long HOUR = count * 60 * 60 * 1000L;
		return HOUR;
	}

	public static long addMinutes(int count)
	{
		long MINUTE = count * 60 * 1000L;
		return MINUTE;
	}

	public static long addSecond(int count)
	{
		long SECONDS = count * 1000L;
		return SECONDS;
	}

	public static long considerMinutes(final int count)
	{
		final long TIME = addMinutes(count) + System.currentTimeMillis();
		return TIME;
	}

	public static String formatTime(final int time, final Language lang)
	{
		return formatTime(time, lang, true);
	}

	public static String formatTime(final int time, final Language lang, final boolean cut)
	{
		int days = 0, hours = 0, minutes = 0, seconds = 0;

		days = time / (24 * 3600);
		hours = (time - days * 24 * 3600) / 3600;
		minutes = (time - days * 24 * 3600 - hours * 3600) / 60;
		seconds = (time - days * 24 * 3600 - hours * 3600) % 60;

		String result;
		if(days >= 1)
		{
			if(hours < 1 || cut)
				result = days + " " + Util.declension(lang, days, DeclensionKey.DAYS);
			else
				result = days + " " + Util.declension(lang, days, DeclensionKey.DAYS) + " " + hours + " " + Util.declension(lang, hours, DeclensionKey.HOUR);
		}
		else if(hours >= 1)
		{
			if(minutes < 1 || cut)
				result = hours + " " + Util.declension(lang, hours, DeclensionKey.HOUR);
			else
				result = hours + " " + Util.declension(lang, hours, DeclensionKey.HOUR) + " " + minutes + " " + Util.declension(lang, minutes, DeclensionKey.MINUTES);
		}
		else if(minutes >= 1)
		{
			if(seconds < 1 || cut)
				result = minutes + " " + Util.declension(lang, minutes, DeclensionKey.MINUTES);
			else
				result = minutes + " " + Util.declension(lang, minutes, DeclensionKey.MINUTES) + " " + seconds + " " + Util.declension(lang, seconds, DeclensionKey.SECONDS);
		}
		else
			result = seconds + " " + Util.declension(lang, seconds, DeclensionKey.SECONDS);

		return result;
	}
}
