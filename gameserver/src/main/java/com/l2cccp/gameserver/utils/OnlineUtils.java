package com.l2cccp.gameserver.utils;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;

public class OnlineUtils
{
	private static int online = 0, offline = 0, olympiad = 0;
	private static long last_update = 0;

	private static void calc()
	{
		online = 0;
		offline = 0;
		olympiad = 0;

		last_update = TimeUtils.addMinutes(1) + System.currentTimeMillis();
		for(Player pl : GameObjectsStorage.getPlayers())
		{
			if(pl.isInOfflineMode())
				offline++;
			else
				online++;;
		}

		online = online + (online * Config.BBS_ONLINE_CHEAT) / 100;
		olympiad = Olympiad._nonClassBasedRegisters.size() + Olympiad._classBasedRegisters.size() + Olympiad._teamBasedRegisters.size();
	}

	public static int get(boolean on)
	{
		if(last_update < System.currentTimeMillis())
			calc();

		return on ? online : offline;
	}

	public static String getformat(boolean on)
	{
		if(last_update < System.currentTimeMillis())
			calc();

		return Util.formatAdena(on ? online : offline);
	}
	public static String online(boolean on)
	{
		if(last_update < System.currentTimeMillis())
			calc();

		return Util.formatAdena(on ? online+offline : online+offline);
	}
	public static String olympiad(boolean on)
	{
		if(last_update < System.currentTimeMillis())
			calc();

		return Util.formatAdena(on ? olympiad : online);
	}
}