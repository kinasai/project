package com.l2cccp.gameserver.templates.player;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ClassData
{
	private final TIntObjectHashMap<HealthData> _data = new TIntObjectHashMap<HealthData>();
	private final TIntObjectHashMap<HealthData> _regen = new TIntObjectHashMap<HealthData>();
	private final int _classId;

	public ClassData(int classId)
	{
		_classId = classId;
	}

	public void addHealthData(int level, double hp, double mp, double cp)
	{
		_data.put(level, new HealthData(hp, mp, cp));
	}

	public void addRegenData(int level, double hp, double mp, double cp)
	{
		_regen.put(level, new HealthData(hp, mp, cp));
	}

	public HealthData getHealthData(int level)
	{
		return _data.get(level);
	}

	public HealthData getRegenData(int level)
	{
		return _regen.get(level);
	}

	public int getClassId()
	{
		return _classId;
	}
}