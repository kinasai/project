package com.l2cccp.gameserver.templates.client;

import com.l2cccp.gameserver.utils.Language;

public class ItemNameLine
{
	private int _itemId;
	private String _name;
	private String _augmentName;

	public ItemNameLine(Language lang, int itemId, String name, String augment)
	{
		_itemId = itemId;
		_name = name;
		_augmentName = augment;
	}

	public int getItemId()
	{
		return _itemId;
	}

	public String getName()
	{
		return _name;
	}

	public String getAugmentName()
	{
		return _augmentName;
	}
}