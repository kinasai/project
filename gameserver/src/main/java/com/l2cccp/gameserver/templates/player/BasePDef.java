package com.l2cccp.gameserver.templates.player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BasePDef
{
	private final int _chest;
	private final int _legs;
	private final int _helmet;
	private final int _boots;
	private final int _gloves;
	private final int _underwear;
	private final int _cloak;

	public BasePDef(final int chest, final int legs, final int helmet, final int boots, final int gloves, final int underwear, final int cloak)
	{
		_chest = chest;
		_legs = legs;
		_helmet = helmet;
		_boots = boots;
		_gloves = gloves;
		_underwear = underwear;
		_cloak = cloak;
	}

	public int getChest()
	{
		return _chest;
	}

	public int getLegs()
	{
		return _legs;
	}

	public int getHelmet()
	{
		return _helmet;
	}

	public int getBoots()
	{
		return _boots;
	}

	public int getGloves()
	{
		return _gloves;
	}

	public int getUnderwear()
	{
		return _underwear;
	}

	public int getCloak()
	{
		return _cloak;
	}
}