package com.l2cccp.gameserver.templates.moveroute;

import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.NpcString;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author VISTALL
 * @date 22:12/24.10.2011
 */
public class MoveNode extends Location
{
	private static final long serialVersionUID = 8291528118019681063L;

	private final NpcString _npcString;
	private final ChatType _chatType;
	private final long _delay;
	private final int _socialId;
	private final int _scatter;

	public MoveNode(int x, int y, int z, int scatter, NpcString npcString, int socialId, long delay, ChatType chatType)
	{
		super(x, y, z);
		_npcString = npcString;
		_socialId = socialId;
		_delay = delay;
		_scatter = scatter;
		_chatType = chatType;
	}

	public NpcString getNpcString()
	{
		return _npcString;
	}

	public long getDelay()
	{
		return _delay;
	}

	public int getSocialId()
	{
		return _socialId;
	}

	public ChatType getChatType()
	{
		return _chatType;
	}

	public int getScatter()
	{
		return _scatter;
	}
}
