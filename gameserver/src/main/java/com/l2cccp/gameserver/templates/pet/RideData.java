package com.l2cccp.gameserver.templates.pet;

import com.l2cccp.gameserver.templates.StatsSet;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RideData
{
	private final int _attack_speed_on_ride;
	private final int _consume_meal_in_battle_on_ride;
	private final int _consume_meal_in_normal_on_ride;
	private final float _mattack_on_ride;
	private final float _pattack_on_ride;
	private final int[] _speed_on_ride;

	public RideData(StatsSet set)
	{
		_attack_speed_on_ride = set.getInteger("attack_speed_on_ride");
		_consume_meal_in_battle_on_ride = set.getInteger("consume_meal_in_battle_on_ride");
		_consume_meal_in_normal_on_ride = set.getInteger("consume_meal_in_normal_on_ride");
		_mattack_on_ride = set.getFloat("mattack_on_ride");
		_pattack_on_ride = set.getFloat("pattack_on_ride");
		_speed_on_ride = set.getIntegerArray("speed_on_ride");
	}

	public int[] getSpeed()
	{
		return _speed_on_ride;
	}
}