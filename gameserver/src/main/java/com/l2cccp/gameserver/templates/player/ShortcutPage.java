package com.l2cccp.gameserver.templates.player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum ShortcutPage
{
	NORMAL_0(0),
	NORMAL_1(1),
	NORMAL_2(2),
	NORMAL_3(3),
	NORMAL_4(4),
	NORMAL_5(5),
	NORMAL_6(6),
	NORMAL_7(7),
	NORMAL_8(8),
	NORMAL_9(9),
	FLY_TRANSFORM(10),
	AIRSHIP(11);

	private final int _id;

	ShortcutPage(final int id)
	{
		_id = id;
	}

	public static ShortcutPage byId(final int id)
	{
		for(ShortcutPage type : values())
			if(id == type.getId())
				return type;

		return null;
	}

	public int getId()
	{
		return _id;
	}
}