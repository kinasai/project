package com.l2cccp.gameserver.templates.spawn;

import com.l2cccp.gameserver.utils.Location;

/**
 * @author VISTALL
 * @date 4:08/19.05.2011
 */
public interface SpawnRange
{
	Location getRandomLoc(int geoIndex);
}
