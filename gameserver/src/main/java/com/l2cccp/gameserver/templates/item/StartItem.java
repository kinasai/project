package com.l2cccp.gameserver.templates.item;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class StartItem
{
	private final int _id;
	private final long _count;
	private final boolean _equip;

	public StartItem(final int id, final long count, final boolean equip)
	{
		_id = id;
		_count = count;
		_equip = equip;
	}

	public int getItemId()
	{
		return _id;
	}

	public long getCount()
	{
		return _count;
	}

	public boolean mustEquip()
	{
		return _equip;
	}
}