package com.l2cccp.gameserver.templates.pet;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum PetType
{
	PET,
	VITAMINE,
	BABY,
	IMPROVED;
}