package com.l2cccp.gameserver.templates.item;

public interface ItemType
{
	public long mask();
}