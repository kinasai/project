package com.l2cccp.gameserver.templates.mapregion;

import com.l2cccp.gameserver.model.Territory;

public interface RegionData
{
	public Territory getTerritory();
}
