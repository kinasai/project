package com.l2cccp.gameserver.templates.player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum ShortcutType
{
	ITEM(1),
	SKILL(2),
	ACTION(3);

	private final int _id;

	ShortcutType(final int id)
	{
		_id = id;
	}

	public static ShortcutType byId(final int id)
	{
		for(ShortcutType type : values())
			if(id == type.getId())
				return type;

		return null;
	}

	public int getId()
	{
		return _id;
	}
}