package com.l2cccp.gameserver.templates.pet;

import java.util.Set;

import com.l2cccp.gameserver.templates.StatsSet;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PetTemplate
{
	private final int id;
	private final int index;
	private final String name;
	private final int control;
	private final PetType type;
	private final PetArmor armor;

	private int level;
	private long exp;
	private float exp_type;
	private int consume_meal_in_battle;
	private int consume_meal_in_normal;
	private Set<Integer> food;
	private float hungry_limit;
	private int max_meal;
	private float org_hp;
	private float org_hp_regen;
	private float org_mp;
	private float org_mp_regen;
	private float org_pattack;
	private float org_mattack;
	private float org_pdefend;
	private float org_mdefend;
	private int soulshot_count;
	private int spiritshot_count;
	private int attack_speed_on_ride;
	private int meal_in_battle_on_ride;
	private int meal_in_normal_on_ride;
	private int mattack_on_ride;
	private int pattack_on_ride;
	private int speed_on_ride_g;
	private int speed_on_ride_s;
	private int speed_on_ride_f;
	private int ride_state;
	private RideData ride;

	public PetTemplate(final int id, final int index, final String name, final int control, final PetType type, final PetArmor armor, StatsSet set)
	{
		this.id = id;
		this.index = index;
		this.name = name;
		this.control = control;
		this.type = type;
		this.armor = armor;
	}

	public int getLevel()
	{
		return level;
	}

	public void setLevel(int level)
	{
		this.level = level;
	}

	public long getExp()
	{
		return exp;
	}

	public void setExp(long exp)
	{
		this.exp = exp;
	}

	public float getExp_type()
	{
		return exp_type;
	}

	public void setExp_type(float exp_type)
	{
		this.exp_type = exp_type;
	}

	public int getConsume_meal_in_battle()
	{
		return consume_meal_in_battle;
	}

	public void setConsume_meal_in_battle(int consume_meal_in_battle)
	{
		this.consume_meal_in_battle = consume_meal_in_battle;
	}

	public int getConsume_meal_in_normal()
	{
		return consume_meal_in_normal;
	}

	public void setConsume_meal_in_normal(int consume_meal_in_normal)
	{
		this.consume_meal_in_normal = consume_meal_in_normal;
	}

	public Set<Integer> getFood()
	{
		return food;
	}

	public void setFood(Set<Integer> food)
	{
		this.food = food;
	}

	public float getHungry_limit()
	{
		return hungry_limit;
	}

	public void setHungry_limit(float hungry_limit)
	{
		this.hungry_limit = hungry_limit;
	}

	public int getMaxMeal()
	{
		return max_meal;
	}

	public void setMax_meal(int max_meal)
	{
		this.max_meal = max_meal;
	}

	public float getOrg_hp()
	{
		return org_hp;
	}

	public void setOrg_hp(float org_hp)
	{
		this.org_hp = org_hp;
	}

	public float getOrg_hp_regen()
	{
		return org_hp_regen;
	}

	public void setOrg_hp_regen(float org_hp_regen)
	{
		this.org_hp_regen = org_hp_regen;
	}

	public float getOrg_mp()
	{
		return org_mp;
	}

	public void setOrg_mp(float org_mp)
	{
		this.org_mp = org_mp;
	}

	public float getOrg_mp_regen()
	{
		return org_mp_regen;
	}

	public void setOrg_mp_regen(float org_mp_regen)
	{
		this.org_mp_regen = org_mp_regen;
	}

	public float getOrg_pattack()
	{
		return org_pattack;
	}

	public void setOrg_pattack(float org_pattack)
	{
		this.org_pattack = org_pattack;
	}

	public float getOrg_mattack()
	{
		return org_mattack;
	}

	public void setOrg_mattack(float org_mattack)
	{
		this.org_mattack = org_mattack;
	}

	public float getOrg_pdefend()
	{
		return org_pdefend;
	}

	public void setOrg_pdefend(float org_pdefend)
	{
		this.org_pdefend = org_pdefend;
	}

	public float getOrg_mdefend()
	{
		return org_mdefend;
	}

	public void setOrg_mdefend(float org_mdefend)
	{
		this.org_mdefend = org_mdefend;
	}

	public int getSoulshot_count()
	{
		return soulshot_count;
	}

	public void setSoulshot_count(int soulshot_count)
	{
		this.soulshot_count = soulshot_count;
	}

	public int getSpiritshot_count()
	{
		return spiritshot_count;
	}

	public void setSpiritshot_count(int spiritshot_count)
	{
		this.spiritshot_count = spiritshot_count;
	}

	public int getAttack_speed_on_ride()
	{
		return attack_speed_on_ride;
	}

	public void setAttack_speed_on_ride(int attack_speed_on_ride)
	{
		this.attack_speed_on_ride = attack_speed_on_ride;
	}

	public int getMeal_in_battle_on_ride()
	{
		return meal_in_battle_on_ride;
	}

	public void setMeal_in_battle_on_ride(int meal_in_battle_on_ride)
	{
		this.meal_in_battle_on_ride = meal_in_battle_on_ride;
	}

	public int getMeal_in_normal_on_ride()
	{
		return meal_in_normal_on_ride;
	}

	public void setMeal_in_normal_on_ride(int meal_in_normal_on_ride)
	{
		this.meal_in_normal_on_ride = meal_in_normal_on_ride;
	}

	public int getMattack_on_ride()
	{
		return mattack_on_ride;
	}

	public void setMattack_on_ride(int mattack_on_ride)
	{
		this.mattack_on_ride = mattack_on_ride;
	}

	public int getPattack_on_ride()
	{
		return pattack_on_ride;
	}

	public void setPattack_on_ride(int pattack_on_ride)
	{
		this.pattack_on_ride = pattack_on_ride;
	}

	public int getSpeed_on_ride_g()
	{
		return speed_on_ride_g;
	}

	public void setSpeed_on_ride_g(int speed_on_ride_g)
	{
		this.speed_on_ride_g = speed_on_ride_g;
	}

	public int getSpeed_on_ride_s()
	{
		return speed_on_ride_s;
	}

	public void setSpeed_on_ride_s(int speed_on_ride_s)
	{
		this.speed_on_ride_s = speed_on_ride_s;
	}

	public int getSpeed_on_ride_f()
	{
		return speed_on_ride_f;
	}

	public void setSpeed_on_ride_f(int speed_on_ride_f)
	{
		this.speed_on_ride_f = speed_on_ride_f;
	}

	public int getRide_state()
	{
		return ride_state;
	}

	public void setRide_state(int ride_state)
	{
		this.ride_state = ride_state;
	}

	public RideData getRide()
	{
		return ride;
	}

	public void setRide(RideData ride)
	{
		this.ride = ride;
	}

	public int getId()
	{
		return id;
	}

	public int getIndex()
	{
		return index;
	}

	public String getName()
	{
		return name;
	}

	public int getControl()
	{
		return control;
	}

	public PetType getType()
	{
		return type;
	}

	public PetArmor getArmor()
	{
		return armor;
	}
}