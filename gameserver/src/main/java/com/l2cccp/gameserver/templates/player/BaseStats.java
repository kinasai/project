package com.l2cccp.gameserver.templates.player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BaseStats
{
	private final byte _STR;
	private final byte _CON;
	private final byte _DEX;
	private final byte _INT;
	private final byte _WIT;
	private final byte _MEN;

	public BaseStats(final byte STR, final byte CON, final byte DEX, final byte INT, final byte WIT, final byte MEN)
	{
		_STR = STR;
		_CON = CON;
		_DEX = DEX;
		_INT = INT;
		_WIT = WIT;
		_MEN = MEN;
	}

	public byte getSTR()
	{
		return _STR;
	}

	public byte getCON()
	{
		return _CON;
	}

	public byte getDEX()
	{
		return _DEX;
	}

	public byte getINT()
	{
		return _INT;
	}

	public byte getWIT()
	{
		return _WIT;
	}

	public byte getMEN()
	{
		return _MEN;
	}
}