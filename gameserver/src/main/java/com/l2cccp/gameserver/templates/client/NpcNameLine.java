package com.l2cccp.gameserver.templates.client;

import com.l2cccp.gameserver.utils.Language;

public class NpcNameLine
{
	private final int _npcId;
	private final String _name;
	private final String _title;

	public NpcNameLine(Language lang, int npcId, String name, String title)
	{
		_npcId = npcId;
		_name = name;
		_title = title;
	}

	public int getNpcId()
	{
		return _npcId;
	}

	public String getName()
	{
		return _name;
	}

	public String getTitle()
	{
		return _title;
	}
}