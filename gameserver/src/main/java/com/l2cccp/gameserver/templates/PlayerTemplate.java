package com.l2cccp.gameserver.templates;

import java.util.List;

import com.l2cccp.gameserver.model.actor.instances.player.ShortCut;
import com.l2cccp.gameserver.model.base.ClassId;
import com.l2cccp.gameserver.model.base.Race;
import com.l2cccp.gameserver.model.base.Type;
import com.l2cccp.gameserver.templates.item.StartItem;
import com.l2cccp.gameserver.templates.player.BaseMDef;
import com.l2cccp.gameserver.templates.player.BasePDef;
import com.l2cccp.gameserver.templates.player.BaseStats;
import com.l2cccp.gameserver.utils.Location;

public class PlayerTemplate extends CharTemplate
{
	public final ClassId classId;
	public Race _race;
	public String className;
	public Location spawnLoc;
	private final List<ShortCut> _shortcuts;
	public final boolean isMale;
	private final List<StartItem> _items;
	private final BasePDef _pDef;
	private final BaseMDef _mDef;
	private final Type _type;

	public PlayerTemplate(final int id, final Race race, Type type, final BaseStats stats, final BasePDef pDef, final BaseMDef mDef, final StatsSet set, final boolean male, final List<StartItem> items, List<ShortCut> shortcuts)
	{
		super(set);
		classId = ClassId.VALUES[id];
		_race = race;
		_type = type;
		className = set.getString("name");
		isMale = male;
		_items = items;
		_shortcuts = shortcuts;
		_pDef = pDef;
		_mDef = mDef;
	}

	public List<StartItem> getItems()
	{
		return _items;
	}

	public BasePDef getBasePDef()
	{
		return _pDef;
	}

	public BaseMDef getBaseMDef()
	{
		return _mDef;
	}

	public Type getType()
	{
		return _type;
	}

	public List<ShortCut> getShortCuts()
	{
		return _shortcuts;
	}
}