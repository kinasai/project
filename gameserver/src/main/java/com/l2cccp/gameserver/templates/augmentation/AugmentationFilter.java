package com.l2cccp.gameserver.templates.augmentation;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public enum AugmentationFilter
{
	NONE,
	ACTIVE_SKILL,
	PASSIVE_SKILL,
	CHANCE_SKILL,
	STATS
}
