package com.l2cccp.gameserver.templates.player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BaseMDef
{
	private final int _r_earring;
	private final int _l_earring;
	private final int _necklace;
	private final int _r_ring;
	private final int _l_ring;

	public BaseMDef(final int r_earring, final int l_earring, final int necklace, final int r_ring, final int l_ring)
	{
		_r_earring = r_earring;
		_l_earring = l_earring;
		_necklace = necklace;
		_r_ring = r_ring;
		_l_ring = l_ring;
	}

	public int getREarring()
	{
		return _r_earring;
	}

	public int getLEarring()
	{
		return _l_earring;
	}

	public int getNecklace()
	{
		return _necklace;
	}

	public int getRRing()
	{
		return _r_ring;
	}

	public int getLRing()
	{
		return _l_ring;
	}
}