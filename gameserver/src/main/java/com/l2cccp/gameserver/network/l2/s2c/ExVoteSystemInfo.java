package com.l2cccp.gameserver.network.l2.s2c;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.RecommendSystem;

public class ExVoteSystemInfo extends L2GameServerPacket
{
	private int _recomLeft, _recomHave, _bonusTime, _bonusVal, _bonusType;

	public ExVoteSystemInfo(Player player)
	{
		final RecommendSystem system = player.getRecommendSystem();
		_recomLeft = system.getRecommendsLeft();
		_recomHave = system.getRecommendsHave();
		_bonusTime = system.getBonusTime();
		_bonusVal = system.getBonusVal();
		_bonusType = !system.isActive() || player.isHourglassEffected() ? 1 : 0;
	}

	@Override
	protected void writeImpl()
	{
		writeEx(0xC9);
		writeD(_recomLeft); //полученые реки
		writeD(_recomHave); //отданые реки
		writeD(_bonusTime); //таймер скок секунд осталось
		writeD(_bonusVal); // процент бонуса
		writeD(_bonusType); //если ноль то таймера нету 1 - пишет чтоли "Работает"
	}
}