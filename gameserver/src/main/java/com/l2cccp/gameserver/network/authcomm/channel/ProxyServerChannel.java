package com.l2cccp.gameserver.network.authcomm.channel;

import com.l2cccp.commons.net.utils.Net;
import com.l2cccp.commons.net.utils.NetInfo;

/**
 * @author VISTALL
 * @since 29.04.14
 */
public class ProxyServerChannel extends AbstractServerChannel
{
	public ProxyServerChannel(int id, String ip, int port)
	{
		super(id);
		add(new NetInfo(Net.valueOf("*.*.*.*"), ip, port));
	}
}
