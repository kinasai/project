package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.data.xml.holder.EventHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.EventType;
import com.l2cccp.gameserver.model.entity.events.impl.DominionSiegeRunnerEvent;
import com.l2cccp.gameserver.network.l2.s2c.ExReplyDominionInfo;
import com.l2cccp.gameserver.network.l2.s2c.ExShowOwnthingPos;

public class RequestExDominionInfo extends L2GameClientPacket
{
	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		activeChar.sendPacket(new ExReplyDominionInfo());

		DominionSiegeRunnerEvent runnerEvent = EventHolder.getInstance().getEvent(EventType.MAIN_EVENT, 1);
		if(runnerEvent.isInProgress())
			activeChar.sendPacket(new ExShowOwnthingPos());
	}
}