package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.network.l2.s2c.UnicodeFormat;

public class RequestAbnormalState extends L2GameClientPacket {

	@Override
	protected void readImpl() throws Exception { /* */ }

	@Override
	protected void runImpl() throws Exception {
		getClient().close(new UnicodeFormat());
	}

}
