package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.commons.collections.CollectionUtils;
import com.l2cccp.gameserver.dao.CharacterTPBookmarkDAO;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.TpBookMark;
import com.l2cccp.gameserver.network.l2.s2c.ExGetBookMarkInfo;

/**
 * dSdS
 */
public class RequestModifyBookMarkSlot extends L2GameClientPacket
{
	private String _name, _acronym;
	private int _icon, _slot;

	@Override
	protected void readImpl()
	{
		_slot = readD();
		_name = readS(32);
		_icon = readD();
		_acronym = readS(4);
	}

	@Override
	protected void runImpl()
	{
		final Player player = getClient().getActiveChar();
		if(player == null)
			return;

		final TpBookMark mark = CollectionUtils.safeGet(player.getTpBookMarks(), _slot - 1);
		if(mark != null)
		{
			mark.setName(_name);
			mark.setIcon(_icon);
			mark.setAcronym(_acronym);

			CharacterTPBookmarkDAO.getInstance().update(player, mark);
			player.sendPacket(new ExGetBookMarkInfo(player));
		}
	}
}