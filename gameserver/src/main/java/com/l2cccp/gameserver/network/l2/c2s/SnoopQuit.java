package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.commons.listener.Listener;
import com.l2cccp.gameserver.listener.actor.player.impl.SnoopPlayerSayListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;

public class SnoopQuit extends L2GameClientPacket
{
	private int _targetObjectId;

	@Override
	protected void readImpl()
	{
		_targetObjectId = readD();
	}

	@Override
	protected void runImpl()
	{
		Player player = getClient().getActiveChar();
		if(player == null)
			return;

		Player target = GameObjectsStorage.getPlayer(_targetObjectId);
		if(target == null)
			return;

		for(Listener<Creature> $listener : target.getListeners().getListeners())
		{
			if($listener instanceof SnoopPlayerSayListener)
			{
				SnoopPlayerSayListener listener = (SnoopPlayerSayListener) $listener;

				if(listener.getOwner() == player)
				{
					target.removeListener($listener);
					player.getVars().remove(Player.SNOOP_TARGET);
					break;
				}
			}
		}
	}
}