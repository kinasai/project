package com.l2cccp.gameserver.network.telnet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.network.telnet.commands.TelnetBan;
import com.l2cccp.gameserver.network.telnet.commands.TelnetConfig;
import com.l2cccp.gameserver.network.telnet.commands.TelnetDebug;
import com.l2cccp.gameserver.network.telnet.commands.TelnetPerfomance;
import com.l2cccp.gameserver.network.telnet.commands.TelnetSay;
import com.l2cccp.gameserver.network.telnet.commands.TelnetServer;
import com.l2cccp.gameserver.network.telnet.commands.TelnetStatus;
import com.l2cccp.gameserver.network.telnet.commands.TelnetWorld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TelnetServerHandler extends SimpleChannelUpstreamHandler implements TelnetCommandHolder
{
	private static final Logger _log = LoggerFactory.getLogger(TelnetServerHandler.class);

	//The following regex splits a line into its parts, separated by spaces, unless there are quotes, in which case the quotes take precedence.  
	private static final Pattern COMMAND_ARGS_PATTERN = Pattern.compile("\"([^\"]*)\"|([^\\s]+)");
	
	private Set<TelnetCommand> _commands = new LinkedHashSet<TelnetCommand>();

	public TelnetServerHandler()
	{
		_commands.add(new TelnetCommand("help", "h"){
			@Override
			public String getUsage()
			{
				return "help [command]";
			}

			@Override
			public String handle(String[] args)
			{
				if(args.length == 0)
				{
					StringBuilder sb = new StringBuilder();
					sb.append("Available commands:\r\n");
					for(TelnetCommand cmd : _commands)
					{
						sb.append(cmd.getCommand()).append("\r\n");
					}

					return sb.toString();
				}
				else
				{
					TelnetCommand cmd = TelnetServerHandler.this.getCommand(args[0]);
					if(cmd == null)
						return "Unknown command.\r\n";

					return "usage:\r\n" + cmd.getUsage() + "\r\n";
				}
			}
		});

		addHandler(new TelnetBan());
		addHandler(new TelnetConfig());
		addHandler(new TelnetDebug());
		addHandler(new TelnetPerfomance());
		addHandler(new TelnetSay());
		addHandler(new TelnetServer());
		addHandler(new TelnetStatus());
		addHandler(new TelnetWorld());
	}

	public void addHandler(TelnetCommandHolder handler)
	{
		for(TelnetCommand cmd : handler.getCommands())
			_commands.add(cmd);
	}

	@Override
	public Set<TelnetCommand> getCommands()
	{
		return _commands;
	}

	private TelnetCommand getCommand(String command)
	{
		for(TelnetCommand cmd : _commands)
			if(cmd.equals(command))
				return cmd;

		return null;
	}

	private String tryHandleCommand(String command, String[] args)
	{
		TelnetCommand cmd = getCommand(command);

		if(cmd == null)
			return "Unknown command.\r\n";

		String response = cmd.handle(args);
		if(response == null)
			response = "usage:\r\n" + cmd.getUsage() + "\r\n";

		return response;
	}

	@Override
	public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception
	{
		// Send greeting for a new connection.
		e.getChannel().write("Welcome to L2CCCP GameServer telnet console.\r\n");
		e.getChannel().write("It is " + new Date() + " now.\r\n");
		if(!Config.TELNET_PASSWORD.isEmpty())
		{
			// Ask password
			e.getChannel().write("Password:");
			ctx.setAttachment(Boolean.FALSE);
		}
		else
		{
			e.getChannel().write("\r\nType 'help' to see all available commands.\r\n");
			ctx.setAttachment(Boolean.TRUE);
		}
	}

	@Override
	public void messageReceived(ChannelHandlerContext ctx, MessageEvent e)
	{
		// Cast to a String first.
		// We know it is a String because we put some codec in TelnetPipelineFactory.
		String request = (String) e.getMessage();

		// Generate and write a response.
		String response = null;
		boolean close = false;

		if(Boolean.FALSE.equals(ctx.getAttachment()))
			if(Config.TELNET_PASSWORD.equals(request))
			{
				ctx.setAttachment(Boolean.TRUE);
				request = "";
			}
			else
			{
				response = "\r\nWrong password!\r\n";
				close = true;
			}

		if(Boolean.TRUE.equals(ctx.getAttachment()))
			if(request.isEmpty())
				response = "\r\nType 'help' to see all available commands.\r\n";
			else if(request.toLowerCase().equals("exit"))
			{
				response = "\r\nHave a good day!\r\n";
				close = true;
			}
			else
			{
				Matcher m = COMMAND_ARGS_PATTERN.matcher(request);

				m.find();
				String command = m.group();

				List<String> args = new ArrayList<String>();
				String arg;
				while(m.find())
				{
					arg = m.group(1);
					if(arg == null)
						arg = m.group(0);
					args.add(arg);
				}

				response = tryHandleCommand(command, args.toArray(new String[args.size()]));
			}

		// We do not need to write a ChannelBuffer here.
		// We know the encoder inserted at TelnetPipelineFactory will do the conversion.
		ChannelFuture future = e.getChannel().write(response);

		// Close the connection after sending 'Have a good day!'
		// if the client has sent 'exit'.
		if(close)
			future.addListener(ChannelFutureListener.CLOSE);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e)
	{
		if(e.getCause() instanceof IOException)
			e.getChannel().close();
		else
			_log.error("", e.getCause());
	}
}
