package com.l2cccp.gameserver.network.telnet.commands;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.LinkedHashSet;
import java.util.Set;

import net.sf.ehcache.Cache;
import net.sf.ehcache.statistics.LiveCacheStatistics;

import org.apache.commons.io.FileUtils;
import com.l2cccp.commons.dao.JdbcEntityStats;
import com.l2cccp.commons.lang.StatsUtils;
import com.l2cccp.commons.net.nio.impl.SelectorStats;
import com.l2cccp.commons.threading.RunnableStatsManager;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.GameServer;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.dao.ItemsDAO;
import com.l2cccp.gameserver.dao.MailDAO;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.geodata.PathFindBuffers;
import com.l2cccp.gameserver.network.telnet.TelnetCommand;
import com.l2cccp.gameserver.network.telnet.TelnetCommandHolder;
import com.l2cccp.gameserver.taskmanager.AiTaskManager;
import com.l2cccp.gameserver.taskmanager.EffectTaskManager;
import com.l2cccp.gameserver.utils.GameStats;

public class TelnetPerfomance implements TelnetCommandHolder
{
	private Set<TelnetCommand> _commands = new LinkedHashSet<TelnetCommand>();

	public TelnetPerfomance()
	{
		_commands.add(new TelnetCommand("pool", "p"){
			@Override
			public String getUsage()
			{
				return "pool [dump]";
			}

			@Override
			public String handle(String[] args)
			{
				StringBuilder sb = new StringBuilder();

				if(args.length == 0 || args[0].isEmpty())
				{
					sb.append(ThreadPoolManager.getInstance().getStats());
				}
				else if(args[0].equals("dump") || args[0].equals("d"))
					try
					{
						new File("stats").mkdir();
						FileUtils.writeStringToFile(new File("stats/RunnableStats-" + new SimpleDateFormat("MMddHHmmss").format(System.currentTimeMillis()) + ".txt"), RunnableStatsManager.getInstance().getStats().toString());
						sb.append("Runnable stats saved.\r\n");
					}
					catch(IOException e)
					{
						sb.append("Exception: " + e.getMessage() + "!\r\n");
					}
				else
					return null;

				return sb.toString();
			}

		});

		_commands.add(new TelnetCommand("mem", "m"){
			@Override
			public String getUsage()
			{
				return "mem";
			}

			@Override
			public String handle(String[] args)
			{
				StringBuilder sb = new StringBuilder();
				sb.append(StatsUtils.getMemUsage());

				return sb.toString();
			}
		});

		_commands.add(new TelnetCommand("threads", "t"){
			@Override
			public String getUsage()
			{
				return "threads [dump]";
			}

			@Override
			public String handle(String[] args)
			{
				StringBuilder sb = new StringBuilder();

				if(args.length == 0 || args[0].isEmpty())
				{
					sb.append(StatsUtils.getThreadStats());
				}
				else if(args[0].equals("dump") || args[0].equals("d"))
					try
					{
						new File("stats").mkdir();
						FileUtils.writeStringToFile(new File("stats/ThreadsDump-" + new SimpleDateFormat("MMddHHmmss").format(System.currentTimeMillis()) + ".txt"), StatsUtils.getThreadStats(true, true, true).toString());
						sb.append("Threads stats saved.\r\n");
					}
					catch(IOException e)
					{
						sb.append("Exception: " + e.getMessage() + "!\r\n");
					}
				else
					return null;

				return sb.toString();
			}
		});

		_commands.add(new TelnetCommand("gc"){
			@Override
			public String getUsage()
			{
				return "gc";
			}

			@Override
			public String handle(String[] args)
			{
				StringBuilder sb = new StringBuilder();
				sb.append(StatsUtils.getGCStats());

				return sb.toString();
			}
		});

		_commands.add(new TelnetCommand("net", "ns"){
			@Override
			public String getUsage()
			{
				return "net";
			}

			@Override
			public String handle(String[] args)
			{
				StringBuilder sb = new StringBuilder();

				SelectorStats sts = GameServer.getInstance().getSelectorStats();
				sb.append("selectorThreadCount: .... ").append(GameServer.getInstance().getSelectorThreads().length).append("\r\n");
				sb.append("=================================================\r\n");
				sb.append("getTotalConnections: .... ").append(sts.getTotalConnections()).append("\r\n");
				sb.append("getCurrentConnections: .. ").append(sts.getCurrentConnections()).append("\r\n");
				sb.append("getMaximumConnections: .. ").append(sts.getMaximumConnections()).append("\r\n");
				sb.append("getIncomingBytesTotal: .. ").append(sts.getIncomingBytesTotal()).append("\r\n");
				sb.append("getOutgoingBytesTotal: .. ").append(sts.getOutgoingBytesTotal()).append("\r\n");
				sb.append("getIncomingPacketsTotal:  ").append(sts.getIncomingPacketsTotal()).append("\r\n");
				sb.append("getOutgoingPacketsTotal:  ").append(sts.getOutgoingPacketsTotal()).append("\r\n");
				sb.append("getMaxBytesPerRead: ..... ").append(sts.getMaxBytesPerRead()).append("\r\n");
				sb.append("getMaxBytesPerWrite: .... ").append(sts.getMaxBytesPerWrite()).append("\r\n");
				sb.append("=================================================\r\n");

				return sb.toString();
			}

		});

		_commands.add(new TelnetCommand("pathfind", "pfs"){

			@Override
			public String getUsage()
			{
				return "pathfind";
			}

			@Override
			public String handle(String[] args)
			{
				StringBuilder sb = new StringBuilder();

				sb.append(PathFindBuffers.getStats());

				return sb.toString();
			}

		});

		_commands.add(new TelnetCommand("dbstats", "ds"){

			@Override
			public String getUsage()
			{
				return "dbstats";
			}

			@Override
			public String handle(String[] args)
			{
				StringBuilder sb = new StringBuilder();

				sb.append("Basic database usage\r\n");
				sb.append("=================================================\r\n");
				sb.append("Connections").append("\r\n");
				try
				{
					sb.append("     Busy: ........................ ").append(DatabaseFactory.getInstance().getBusyConnectionCount()).append("\r\n");
					sb.append("     Idle: ........................ ").append(DatabaseFactory.getInstance().getIdleConnectionCount()).append("\r\n");
				}
				catch(SQLException e)
				{
					return "Error: " + e.getMessage() + "\r\n";
				}

				sb.append("Players").append("\r\n");
				sb.append("     Update: ...................... ").append(GameStats.getUpdatePlayerBase()).append("\r\n");

				double cacheHitCount, cacheMissCount, cacheHitRatio;
				Cache cache;
				LiveCacheStatistics cacheStats;
				JdbcEntityStats entityStats;

				cache = ItemsDAO.getInstance().getCache();
				cacheStats = cache.getLiveCacheStatistics();
				entityStats = ItemsDAO.getInstance().getStats();

				cacheHitCount = cacheStats.getCacheHitCount();
				cacheMissCount = cacheStats.getCacheMissCount();
				cacheHitRatio = cacheHitCount / (cacheHitCount + cacheMissCount);

				sb.append("Items").append("\r\n");
				sb.append("     getLoadCount: ................ ").append(entityStats.getLoadCount()).append("\r\n");
				sb.append("     getInsertCount: .............. ").append(entityStats.getInsertCount()).append("\r\n");
				sb.append("     getUpdateCount: .............. ").append(entityStats.getUpdateCount()).append("\r\n");
				sb.append("     getDeleteCount: .............. ").append(entityStats.getDeleteCount()).append("\r\n");
				sb.append("Cache").append("\r\n");
				sb.append("     getPutCount: ................. ").append(cacheStats.getPutCount()).append("\r\n");
				sb.append("     getUpdateCount: .............. ").append(cacheStats.getUpdateCount()).append("\r\n");
				sb.append("     getRemovedCount: ............. ").append(cacheStats.getRemovedCount()).append("\r\n");
				sb.append("     getEvictedCount: ............. ").append(cacheStats.getEvictedCount()).append("\r\n");
				sb.append("     getExpiredCount: ............. ").append(cacheStats.getExpiredCount()).append("\r\n");
				sb.append("     getSize: ..................... ").append(cacheStats.getSize()).append("\r\n");
				sb.append("     getInMemorySize: ............. ").append(cacheStats.getInMemorySize()).append("\r\n");
				sb.append("     getOnDiskSize: ............... ").append(cacheStats.getOnDiskSize()).append("\r\n");
				sb.append("     cacheHitRatio: ............... ").append(String.format("%2.2f", cacheHitRatio)).append("\r\n");
				sb.append("=================================================\r\n");

				cache = MailDAO.getInstance().getCache();
				cacheStats = cache.getLiveCacheStatistics();
				entityStats = MailDAO.getInstance().getStats();

				cacheHitCount = cacheStats.getCacheHitCount();
				cacheMissCount = cacheStats.getCacheMissCount();
				cacheHitRatio = cacheHitCount / (cacheHitCount + cacheMissCount);

				sb.append("Mail").append("\r\n");
				sb.append("     getLoadCount: ................ ").append(entityStats.getLoadCount()).append("\r\n");
				sb.append("     getInsertCount: .............. ").append(entityStats.getInsertCount()).append("\r\n");
				sb.append("     getUpdateCount: .............. ").append(entityStats.getUpdateCount()).append("\r\n");
				sb.append("     getDeleteCount: .............. ").append(entityStats.getDeleteCount()).append("\r\n");
				sb.append("Cache").append("\r\n");
				sb.append("     getPutCount: ................. ").append(cacheStats.getPutCount()).append("\r\n");
				sb.append("     getUpdateCount: .............. ").append(cacheStats.getUpdateCount()).append("\r\n");
				sb.append("     getRemovedCount: ............. ").append(cacheStats.getRemovedCount()).append("\r\n");
				sb.append("     getEvictedCount: ............. ").append(cacheStats.getEvictedCount()).append("\r\n");
				sb.append("     getExpiredCount: ............. ").append(cacheStats.getExpiredCount()).append("\r\n");
				sb.append("     getSize: ..................... ").append(cacheStats.getSize()).append("\r\n");
				sb.append("     getInMemorySize: ............. ").append(cacheStats.getInMemorySize()).append("\r\n");
				sb.append("     getOnDiskSize: ............... ").append(cacheStats.getOnDiskSize()).append("\r\n");
				sb.append("     cacheHitRatio: ............... ").append(String.format("%2.2f", cacheHitRatio)).append("\r\n");
				sb.append("=================================================\r\n");

				return sb.toString();
			}

		});

		_commands.add(new TelnetCommand("aistats", "as"){

			@Override
			public String getUsage()
			{
				return "aistats";
			}

			@Override
			public String handle(String[] args)
			{
				StringBuilder sb = new StringBuilder();

				for(int i = 0; i < Config.AI_TASK_MANAGER_COUNT; i++)
				{
					sb.append("AiTaskManager #").append(i + 1).append("\r\n");
					sb.append("=================================================\r\n");
					sb.append(AiTaskManager.getInstance().getStats(i));
					sb.append("=================================================\r\n");
				}

				return sb.toString();
			}

		});
		_commands.add(new TelnetCommand("effectstats", "es"){

			@Override
			public String getUsage()
			{
				return "effectstats";
			}

			@Override
			public String handle(String[] args)
			{
				StringBuilder sb = new StringBuilder();

				for(int i = 0; i < Config.EFFECT_TASK_MANAGER_COUNT; i++)
				{
					sb.append("EffectTaskManager #").append(i + 1).append("\r\n");
					sb.append("=================================================\r\n");
					sb.append(EffectTaskManager.getInstance().getStats(i));
					sb.append("=================================================\r\n");
				}

				return sb.toString();
			}

		});
	}

	@Override
	public Set<TelnetCommand> getCommands()
	{
		return _commands;
	}
}