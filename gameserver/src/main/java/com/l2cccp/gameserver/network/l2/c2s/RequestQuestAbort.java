package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.instancemanager.QuestManager;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.quest.Quest;
import com.l2cccp.gameserver.model.quest.QuestState;
import com.l2cccp.gameserver.network.l2.s2c.QuestList;

public class RequestQuestAbort extends L2GameClientPacket
{
	private int _questID;

	@Override
	protected void readImpl()
	{
		_questID = readD();
	}

	@Override
	protected void runImpl()
	{
		Player activeChar = getClient().getActiveChar();
		Quest quest = QuestManager.getQuest(_questID);
		if(activeChar == null || quest == null)
			return;

		if(!quest.canAbortByPacket())
		{
			// обновляем клиент, ибо он сам удаляет
			activeChar.sendPacket(new QuestList(activeChar));
			return;
		}

		QuestState qs = activeChar.getQuestState(_questID);
		if(qs != null && !qs.isCompleted())
			qs.abortQuest();
	}
}