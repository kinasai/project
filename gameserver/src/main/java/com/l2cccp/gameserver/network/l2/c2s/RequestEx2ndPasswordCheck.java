package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.network.l2.s2c.Ex2ndPasswordCheck;

/**
 * @author VISTALL
 */
public class RequestEx2ndPasswordCheck extends L2GameClientPacket
{
	@Override
	protected void readImpl()
	{
		//
	}

	@Override
	protected void runImpl()
	{
		if(Config.SECOND_AUTH_ENABLED)
			sendPacket(new Ex2ndPasswordCheck(Ex2ndPasswordCheck.PASSWORD_OK));
	}
}
