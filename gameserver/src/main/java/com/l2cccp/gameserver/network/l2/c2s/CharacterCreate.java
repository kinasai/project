package com.l2cccp.gameserver.network.l2.c2s;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.CharacterDAO;
import com.l2cccp.gameserver.data.client.holder.NpcNameLineHolder;
import com.l2cccp.gameserver.data.xml.holder.ExperienceHolder;
import com.l2cccp.gameserver.data.xml.holder.SkillAcquireHolder;
import com.l2cccp.gameserver.instancemanager.QuestManager;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.SkillLearn;
import com.l2cccp.gameserver.model.actor.instances.player.ShortCut;
import com.l2cccp.gameserver.model.base.AcquireType;
import com.l2cccp.gameserver.model.base.ClassId;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.items.PcInventory;
import com.l2cccp.gameserver.model.quest.Quest;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.s2c.CharacterCreateFail;
import com.l2cccp.gameserver.network.l2.s2c.CharacterCreateSuccess;
import com.l2cccp.gameserver.network.l2.s2c.CharacterSelectionInfo;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.templates.PlayerTemplate;
import com.l2cccp.gameserver.templates.item.StartItem;
import com.l2cccp.gameserver.utils.Util;

public class CharacterCreate extends L2GameClientPacket
{
	private static final Logger _log = LoggerFactory.getLogger(CharacterCreate.class);
	// cSdddddddddddd
	private String _name;
	private int _sex;
	private int _classId;
	private int _hairStyle;
	private int _hairColor;
	private int _face;

	@Override
	protected void readImpl()
	{
		_name = readS();
		readD(); // race
		_sex = readD();
		_classId = readD();
		readD(); // int
		readD(); // str
		readD(); // con
		readD(); // men
		readD(); // dex
		readD(); // wit
		_hairStyle = readD();
		_hairColor = readD();
		_face = readD();
	}

	@Override
	protected void runImpl()
	{
		for(ClassId cid : ClassId.VALUES)
			if(cid.getId() == _classId && cid.getLevel() != 1)
				return;
		if(CharacterDAO.getInstance().accountCharNumber(getClient().getLogin()) >= Config.CREATE_MAX_CHAR)
		{
			sendPacket(CharacterCreateFail.REASON_TOO_MANY_CHARACTERS);
			return;
		}

		if(!Util.isMatchingRegexp(_name, Config.CNAME_TEMPLATE) || NpcNameLineHolder.getInstance().isBlackListContainsName(_name))
		{
			sendPacket(CharacterCreateFail.REASON_16_ENG_CHARS);
			return;
		}
		else if(CharacterDAO.getInstance().getObjectIdByName(_name) > 0)
		{
			sendPacket(CharacterCreateFail.REASON_NAME_ALREADY_EXISTS);
			return;
		}
		else if(_face > 2 || _face < 0)
		{
			_log.warn("Character Creation Failure: Character face " + _face + " is invalid. Possible client hack. " + getClient());
			sendPacket(CharacterCreateFail.REASON_CREATION_FAILED);
			return;
		}
		else if(_hairStyle < 0 || _sex == 0 && _hairStyle > 4 || _sex != 0 && _hairStyle > 6)
		{
			_log.warn("Character Creation Failure: Character hair style " + _hairStyle + " is invalid. Possible client hack. " + getClient());
			sendPacket(CharacterCreateFail.REASON_CREATION_FAILED);
			return;
		}
		else if(_hairColor > 3 || _hairColor < 0)
		{
			_log.warn("Character Creation Failure: Character hair color " + _hairColor + " is invalid. Possible client hack. " + getClient());
			sendPacket(CharacterCreateFail.REASON_CREATION_FAILED);
			return;
		}

		Player newChar = Player.create(_classId, _sex, getClient().getLogin(), _name, _hairStyle, _hairColor, _face);
		if(newChar == null)
			return;

		sendPacket(CharacterCreateSuccess.STATIC);

		initNewChar(getClient(), newChar);
	}

	private void initNewChar(GameClient client, Player newChar)
	{
		PlayerTemplate template = newChar.getTemplate();

		Player.restoreCharSubClasses(newChar);

		int level = Config.CREATE_START_LVL;
		int sp = Config.CREATE_START_SP;
		if(level > 0 || sp > 0)
			newChar.addExpAndSp(ExperienceHolder.getInstance().getLevels()[level] - newChar.getExp(), sp);

		newChar.setLoc(template.spawnLoc);

		if(Config.CREATE_CHAR_TITLE)
			newChar.setTitle(Config.CREATE_ADD_CHAR_TITLE);
		else
			newChar.setTitle("");

		final PcInventory inventory = newChar.getInventory();
		for(StartItem start : template.getItems())
		{
			ItemInstance item = inventory.addItem(start.getItemId(), start.getCount());

			if(start.mustEquip() && item.isEquipable())
				inventory.equipItem(item);
		}

		for(SkillLearn skill : SkillAcquireHolder.getInstance().getAvailableSkills(newChar, AcquireType.NORMAL)){
		newChar.addSkill(SkillTable.getInstance().getSkillEntry(skill.getId(), skill.getLevel()), true);
		}
		if (newChar.getSkillLevel(Integer.valueOf(1001)) > 0)
			newChar.registerShortCut(new ShortCut(1, 0, 2, 1001, 1, 1));
		if (newChar.getSkillLevel(Integer.valueOf(1177)) > 0)
			newChar.registerShortCut(new ShortCut(1, 0, 2, 1177, 1, 1));
		if (newChar.getSkillLevel(Integer.valueOf(1216)) > 0) {
			newChar.registerShortCut(new ShortCut(2, 0, 2, 1216, 1, 1));
		}

		for(ShortCut shortcut : template.getShortCuts())
		{
			if(shortcut.getType() == ShortCut.TYPE_ITEM)
			{
				final int obj = inventory.getItemByItemId(shortcut.getId()).getObjectId();
				newChar.registerShortCut(new ShortCut(shortcut.getSlot(), shortcut.getPage(), shortcut.getType(), obj, -1, 1));
			}
			else
				newChar.registerShortCut(new ShortCut(shortcut.getSlot(), shortcut.getPage(), shortcut.getType(), shortcut.getId(), shortcut.getLevel(), 1));
		}

		startTutorialQuest(newChar);

		newChar.setCurrentHpMp(newChar.getMaxHp(), newChar.getMaxMp());
		newChar.setCurrentCp(0); // retail
		newChar.setIsOnline(false);

		newChar.store(false);
		inventory.store();
		newChar.deleteMe();

		client.setCharSelection(CharacterSelectionInfo.loadCharacterSelectInfo(client.getLogin()));
	}

	public static void startTutorialQuest(Player player)
	{
		Quest q = QuestManager.getQuest(255);
		if(q != null)
			q.newQuestState(player, Quest.CREATED);
	}
}