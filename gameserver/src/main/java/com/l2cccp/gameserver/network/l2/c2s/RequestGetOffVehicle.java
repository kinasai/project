package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.boat.Boat;
import com.l2cccp.gameserver.utils.Location;

public class RequestGetOffVehicle extends L2GameClientPacket
{
	// Format: cdddd
	private int _objectId;
	private Location _location = new Location();

	@Override
	protected void readImpl()
	{
		_objectId = readD();
		_location.x = readD();
		_location.y = readD();
		_location.z = readD();
	}

	@Override
	protected void runImpl()
	{
		final Player player = getClient().getActiveChar();
		if(player == null)
			return;

		final Boat boat = player.getBoat();
		if(boat == null || boat.isMoving || !boat.isVehicle() || boat.getObjectId() != _objectId || !boat.isInRange(_location, 500))
		{
			player.sendActionFailed();
			return;
		}

		boat.oustPlayer(player, _location, false);
	}
}