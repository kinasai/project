package com.l2cccp.gameserver.network.authcomm.gs2as;

import com.l2cccp.gameserver.network.authcomm.SendablePacket;

public class ChangeAllowedShare extends SendablePacket
{

	private String account;
	private boolean share;

	public ChangeAllowedShare(String account, boolean share)
	{
		this.account = account;
		this.share = share;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0x12);
		writeS(account);
		writeD(share ? 1 : 0);
	}
}