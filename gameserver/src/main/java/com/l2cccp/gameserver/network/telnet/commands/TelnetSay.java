package com.l2cccp.gameserver.network.telnet.commands;

import java.util.LinkedHashSet;
import java.util.Set;

import com.l2cccp.gameserver.Announcements;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.network.telnet.TelnetCommand;
import com.l2cccp.gameserver.network.telnet.TelnetCommandHolder;


public class TelnetSay implements TelnetCommandHolder
{
	private Set<TelnetCommand> _commands = new LinkedHashSet<TelnetCommand>();

	public TelnetSay()
	{
		_commands.add(new TelnetCommand("announce", "ann")
		{
			@Override
			public String getUsage()
			{
				return "announce <text>";
			}

			@Override
			public String handle(String[] args)
			{
				if(args.length == 0)
					return null;
				
				Announcements.getInstance().announceToAll(args[0]);
				
				return "Announcement sent.\r\n";
			}			
		});
		
		_commands.add(new TelnetCommand("message", "msg")
		{
			@Override
			public String getUsage()
			{
				return "message <player> <text>";
			}

			@Override
			public String handle(String[] args)
			{
				if(args.length < 2)
					return null;

				Player player = World.getPlayer(args[0]);
				if(player == null)
					return "Player not found.\r\n";

				Say2 cs = new Say2(0, ChatType.TELL, "[Admin]", args[1], null);
				player.sendPacket(cs);

				return "Message sent.\r\n";
			}
			
		});
	}

	@Override
	public Set<TelnetCommand> getCommands()
	{
		return _commands;
	}
}