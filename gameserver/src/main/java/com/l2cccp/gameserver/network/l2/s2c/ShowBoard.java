package com.l2cccp.gameserver.network.l2.s2c;

import java.lang.reflect.Field;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.Shutdown;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.utils.OnlineUtils;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

public class ShowBoard extends L2GameServerPacket
{
	private static final String[] DIRECT_BYPASS = new String[] {
			"bypass _bbshome",
			"bypass _bbsgetfav",
			"bypass _bbsloc",
			"bypass _bbsclan",
			"bypass _bbsmemo",
			"bypass _maillist_0_1_0_",
			"bypass _friendlist_0_" };

	private String _html;
	private String _fav;

	public static void separateAndSend(String html, Player player)
	{
		html = html.replace("\t", "");

		if(Config.BBS_SERVER_INFO)
		{
			html = html.replace("<?time?>", String.valueOf(TimeUtils.time()));
			html = html.replace("<?date?>", String.valueOf(TimeUtils.date()));
			html = html.replace("<?online?>", Util.formatAdena(OnlineUtils.get(true)));
			html = html.replace("<?offtrade?>", Util.formatAdena(OnlineUtils.get(false)));
		}

		if(Config.BBS_SERVER_RESTART)
			html = html.replace("<?restart?>", Util.formatTimeDot(Shutdown.getInstance().getSeconds()));

		// %object(htmlpath)%
		String path_file_community = Config.BBS_PATH + "/";
		Pattern p = Pattern.compile("\\%include\\(([^\\)]+)\\)\\%");
		Matcher m = p.matcher(html);
		while(m.find())
		{
			html = html.replace(m.group(0), HtmCache.getInstance().getHtml(path_file_community + m.group(1), player));
		}

		// %object(Config,BBS_FOLDER)%
		p = Pattern.compile("\\%object\\(([^\\)]+),([^\\)]+)\\)\\%");
		m = p.matcher(html);
		while(m.find())
		{
			if(m.group(1).equals("Config"))
				html = html.replace(m.group(0), Config.getField(m.group(2)));
			else
			{
				try
				{
					Object c = Class.forName(m.group(1)).newInstance();
					Field field = c.getClass().getField(m.group(2));
					html = html.replace(m.group(0), field.get(c).toString());
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}
		}
		send(html, player);
	}

	public static void send(String html, Player player)
	{
		String fav = "";
		if(player.getSessionVarS("add_fav") != null)
			fav = "bypass _bbsaddfav_List";

		player.getBypassStorage().parseHtml(html, true);

		if(html.length() < 8180)
		{
			player.sendPacket(new ShowBoard("101", html, fav));
			player.sendPacket(new ShowBoard("102", "", fav));
			player.sendPacket(new ShowBoard("103", "", fav));
		}
		else if(html.length() < 8180 * 2)
		{
			player.sendPacket(new ShowBoard("101", html.substring(0, 8180), fav));
			player.sendPacket(new ShowBoard("102", html.substring(8180, html.length()), fav));
			player.sendPacket(new ShowBoard("103", "", fav));
		}
		else if(html.length() < 8180 * 3)
		{
			player.sendPacket(new ShowBoard("101", html.substring(0, 8180), fav));
			player.sendPacket(new ShowBoard("102", html.substring(8180, 8180 * 2), fav));
			player.sendPacket(new ShowBoard("103", html.substring(8180 * 2, html.length()), fav));
		}
		else
			throw new IllegalArgumentException("Html is too long!");
	}

	public static void separateAndSend(String html, List<String> arg, Player player)
	{
		html = html.replace("\t", "");
		String fav = "";
		if(player.getSessionVarS("add_fav") != null)
			fav = "bypass _bbsaddfav_List";

		player.setLastNpc(null);
		player.getBypassStorage().parseHtml(html, true);

		if(html.length() < 8180)
		{
			player.sendPacket(new ShowBoard("1001", html, fav));
			player.sendPacket(new ShowBoard("1002", arg, fav));
		}
		else
			throw new IllegalArgumentException("Html is too long!");
	}

	private ShowBoard(String id, String html, String fav)
	{
		_html = id + "\u0008";
		if(html != null)
			_html += html;

		_fav = fav;
	}

	private ShowBoard(String id, List<String> arg, String fav)
	{
		_html = id + "\u0008";
		for(String a : arg)
			_html += a + " \u0008";
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x7b);
		writeC(0x01); //c4 1 to show community 00 to hide
		for(String bbsBypass : DIRECT_BYPASS)
			writeS(bbsBypass);
		writeS(_fav);
		writeS(_html);
	}
}