package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.RecommendSystem;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;

public class RequestVoteNew extends L2GameClientPacket
{
	private int _targetObjectId;

	@Override
	protected void readImpl()
	{
		_targetObjectId = readD();
	}

	@Override
	protected void runImpl()
	{
		Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		GameObject target = activeChar.getTarget();
		if(target == null || !target.isPlayer() || target.getObjectId() != _targetObjectId)
		{
			activeChar.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
			return;
		}

		if(target.getObjectId() == activeChar.getObjectId())
		{
			activeChar.sendPacket(SystemMsg.YOU_CANNOT_RECOMMEND_YOURSELF);
			return;
		}

		Player targetPlayer = (Player) target;
		final RecommendSystem system = activeChar.getRecommendSystem();
		if(system.getRecommendsLeft() <= 0)
		{
			activeChar.sendPacket(SystemMsg.YOU_ARE_OUT_OF_RECOMMENDATIONS);
			return;
		}

		if(targetPlayer.getRecommendSystem().getRecommendsHave() >= 255)
		{
			activeChar.sendPacket(SystemMsg.YOUR_SELECTED_TARGET_CAN_NO_LONGER_RECEIVE_A_RECOMMENDATION);
			return;
		}

		system.giveRecommend(targetPlayer);
		SystemMessage sm = new SystemMessage(SystemMsg.YOU_HAVE_RECOMMENDED_C1_YOU_HAVE_S2_RECOMMENDATIONS_LEFT);
		sm.addName(target);
		sm.addNumber(system.getRecommendsLeft());
		activeChar.sendPacket(sm);

		sm = new SystemMessage(SystemMsg.YOU_HAVE_BEEN_RECOMMENDED_BY_C1);
		sm.addName(activeChar);
		targetPlayer.sendPacket(sm);
	}
}