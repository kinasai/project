package com.l2cccp.gameserver.network.l2.s2c;

import java.util.Collection;

import com.l2cccp.gameserver.data.xml.holder.PetitionGroupHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.petition.PetitionMainGroup;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author VISTALL
 */
public class ExResponseShowStepOne extends L2GameServerPacket
{
	private Language _language;

	public ExResponseShowStepOne(Player player)
	{
		_language = player.getLanguage();
	}

	@Override
	protected void writeImpl()
	{
		writeEx(0xAE);
		Collection<PetitionMainGroup> petitionGroups = PetitionGroupHolder.getInstance().getPetitionGroups();
		writeD(petitionGroups.size());
		for(PetitionMainGroup group : petitionGroups)
		{
			writeC(group.getId());
			writeS(group.getName(_language));
		}
	}
}