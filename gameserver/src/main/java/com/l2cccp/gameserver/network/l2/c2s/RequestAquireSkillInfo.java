package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.commons.lang.ArrayUtils;
import com.l2cccp.gameserver.data.xml.holder.SkillAcquireHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.SkillLearn;
import com.l2cccp.gameserver.model.base.AcquireType;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.s2c.AcquireSkillInfo;
import com.l2cccp.gameserver.tables.SkillTable;

/**
 * Reworked: VISTALL
 */
public class RequestAquireSkillInfo extends L2GameClientPacket
{
	private int _id;
	private int _level;
	private AcquireType _type;

	@Override
	protected void readImpl()
	{
		_id = readD();
		_level = readD();
		_type = ArrayUtils.valid(AcquireType.VALUES, readD());
	}

	@Override
	protected void runImpl()
	{
		Player player = getClient().getActiveChar();
		if(player == null || player.getTransformation() != 0 || SkillTable.getInstance().getSkillEntry(_id, _level) == null || _type == null)
			return;

		NpcInstance trainer = player.getLastNpc();
		boolean cb = player.getSessionVarB("clanskill", false);
		if(!cb && (trainer == null || (!player.isInRangeZ(trainer, Creature.INTERACTION_DISTANCE))))
			return;

		SkillLearn skillLearn = SkillAcquireHolder.getInstance().getSkillLearn(player, _id, _level, _type);
		if(skillLearn == null)
			return;

		sendPacket(new AcquireSkillInfo(_type, skillLearn));
	}
}