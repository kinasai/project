package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.model.pledge.RankPrivs;
import com.l2cccp.gameserver.network.l2.s2c.PledgePowerGradeList;

public class RequestPledgePowerGradeList extends L2GameClientPacket
{
	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		Clan clan = activeChar.getClan();
		if(clan != null)
		{
			RankPrivs[] privs = clan.getAllRankPrivs();
			activeChar.sendPacket(new PledgePowerGradeList(privs));
		}
	}
}