package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.data.xml.holder.RecipeHolder;
import com.l2cccp.gameserver.mod.DailyTasks.DailyTaskEngine;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.Bonus;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.recipe.Recipe;
import com.l2cccp.gameserver.model.recipe.RecipeComponent;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ActionFail;
import com.l2cccp.gameserver.network.l2.s2c.RecipeItemMakeInfo;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.templates.item.EtcItemTemplate.EtcItemType;
import com.l2cccp.gameserver.utils.ItemFunctions;

public class RequestRecipeItemMakeSelf extends L2GameClientPacket
{
	private int _recipeId;

	/**
	 * packet type id 0xB8
	 * format:		cd
	 */
	@Override
	protected void readImpl()
	{
		_recipeId = readD();
	}

	@Override
	protected void runImpl()
	{
		Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.isActionsDisabled())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isInStoreMode())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isProcessingRequest())
		{
			activeChar.sendActionFailed();
			return;
		}

		if(activeChar.isFishing())
		{
			activeChar.sendPacket(SystemMsg.YOU_CANNOT_DO_THAT_WHILE_FISHING_);
			return;
		}

		Recipe recipeList = RecipeHolder.getInstance().getRecipeByRecipeId(_recipeId);

		if(recipeList == null || recipeList.getRecipes().length == 0)
		{
			activeChar.sendPacket(SystemMsg.THE_RECIPE_IS_INCORRECT);
			return;
		}

		if(activeChar.getCurrentMp() < recipeList.getMpCost())
		{
			activeChar.sendPacket(SystemMsg.NOT_ENOUGH_MP, new RecipeItemMakeInfo(activeChar, recipeList, 0));
			return;
		}

		if(!activeChar.findRecipe(_recipeId))
		{
			activeChar.sendPacket(SystemMsg.PLEASE_REGISTER_A_RECIPE, ActionFail.STATIC);
			return;
		}

		activeChar.getInventory().writeLock();
		try
		{
			RecipeComponent[] recipes = recipeList.getRecipes();

			for(RecipeComponent recipe : recipes)
			{
				if(recipe.getQuantity() == 0)
					continue;

				if(Config.ALT_GAME_UNREGISTER_RECIPE && ItemHolder.getInstance().getTemplate(recipe.getItemId()).getItemType() == EtcItemType.RECIPE)
				{
					Recipe rp = RecipeHolder.getInstance().getRecipeByRecipeItem(recipe.getItemId());
					if(activeChar.hasRecipe(rp))
						continue;
					activeChar.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_ENOUGH_MATERIALS_TO_PERFORM_THAT_ACTION, new RecipeItemMakeInfo(activeChar, recipeList, 0));
					return;
				}

				ItemInstance item = activeChar.getInventory().getItemByItemId(recipe.getItemId());
				if(item == null || item.getCount() < recipe.getQuantity())
				{
					activeChar.sendPacket(SystemMsg.YOU_DO_NOT_HAVE_ENOUGH_MATERIALS_TO_PERFORM_THAT_ACTION, new RecipeItemMakeInfo(activeChar, recipeList, 0));
					return;
				}
			}

			for(RecipeComponent recipe : recipes)
				if(recipe.getQuantity() != 0)
					if(Config.ALT_GAME_UNREGISTER_RECIPE && ItemHolder.getInstance().getTemplate(recipe.getItemId()).getItemType() == EtcItemType.RECIPE)
						activeChar.unregisterRecipe(RecipeHolder.getInstance().getRecipeByRecipeItem(recipe.getItemId()).getId());
					else
					{
						activeChar.getInventory().destroyItemByItemId(recipe.getItemId(), recipe.getQuantity());
						//TODO audit
						activeChar.sendPacket(SystemMessage.removeItems(recipe.getItemId(), recipe.getQuantity()));
					}
		}
		finally
		{
			activeChar.getInventory().writeUnlock();
		}

		activeChar.resetWaitSitTime();
		activeChar.reduceCurrentMp(recipeList.getMpCost(), null);

		int tryCount = 1, success = 0;
		if(Rnd.chance(Config.CRAFT_DOUBLECRAFT_CHANCE))
			tryCount++;

		for(int i = 0; i < tryCount; i++)
		{
			double chance = recipeList.getSuccessRate();
			double found = Config.CRAFT_MASTERWORK_CHANCE;
			Bonus bonus = activeChar.getBonus();
			chance += bonus.getCraftChance();
			found += bonus.getMasterWorkChance();

			if(Rnd.chance(chance))
			{
				int itemId = recipeList.getFoundation() != 0 ? Rnd.chance(found) ? recipeList.getFoundation() : recipeList.getItemId() : recipeList.getItemId();
				long count = recipeList.getCount();
				final long exp = recipeList.getExp(), sp = recipeList.getSp();
				if(exp > 0 || sp > 0)
					activeChar.addExpAndSp(exp, sp);
				ItemFunctions.addItem(activeChar, itemId, count);				
				if(DailyTaskEngine.isRequestAccepted(activeChar)){				
					String request = activeChar.getVar("@daily_request_accepted");
		            if(request.contains("craft")) {
			        	int neededItem = Integer.parseInt(request.split("_")[2]);
			        	if(neededItem == itemId) {
			                if(DailyTaskEngine.getInstance().itemCrafted(activeChar)) { // Выполнен реквест на крафт
			                	DailyTaskEngine.getInstance().requestComplete(activeChar);
			                }
			        	}
		            }
				}
				success = 1;
			}
		}

		if(success == 0)
			activeChar.sendPacket(new SystemMessage(SystemMsg.YOU_FAILED_TO_MANUFACTURE_S1).addItemName(recipeList.getItemId()));
		activeChar.sendPacket(new RecipeItemMakeInfo(activeChar, recipeList, success));
	}
}