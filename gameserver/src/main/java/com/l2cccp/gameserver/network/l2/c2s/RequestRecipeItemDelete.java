package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.data.xml.holder.RecipeHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.recipe.Recipe;
import com.l2cccp.gameserver.network.l2.s2c.RecipeBookItemList;

public class RequestRecipeItemDelete extends L2GameClientPacket
{
	private int _recipeId;

	@Override
	protected void readImpl()
	{
		_recipeId = readD();
	}

	@Override
	protected void runImpl()
	{
		Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		if(activeChar.getPrivateStoreType() == Player.STORE_PRIVATE_MANUFACTURE)
		{
			activeChar.sendActionFailed();
			return;
		}

		Recipe rp = RecipeHolder.getInstance().getRecipeByRecipeId(_recipeId);
		if(rp == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		activeChar.unregisterRecipe(_recipeId);
		activeChar.sendPacket(new RecipeBookItemList(activeChar, rp.isDwarvenCraft()));
	}
}