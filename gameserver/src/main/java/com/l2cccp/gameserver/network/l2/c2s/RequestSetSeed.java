package com.l2cccp.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.gameserver.data.xml.holder.ResidenceHolder;
import com.l2cccp.gameserver.instancemanager.CastleManorManager;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.residence.Castle;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.manor.Manor;
import com.l2cccp.gameserver.model.manor.Seed;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.templates.manor.SeedProduction;
import com.l2cccp.gameserver.utils.NpcUtils;


/**
 * Format: (ch) dd [ddd]
 * d - manor id
 * d - size
 * [
 * d - seed id
 * d - sales
 * d - price
 * ]
 */
public class RequestSetSeed extends L2GameClientPacket
{
	private int _count, _manorId;

	private long[] _items; // _size*3

	@Override
	protected void readImpl()
	{
		_manorId = readD();
		_count = readD();
		if(_count * 20 > _buf.remaining() || _count > Short.MAX_VALUE || _count < 1)
		{
			_count = 0;
			return;
		}
		_items = new long[_count * 3];
		for(int i = 0; i < _items.length;)
		{
			int id = readD();
			long sales = readQ();
			long price = readQ();
			if(id < 1 || sales < 0 || price < 0)
			{
				_count = 0;
				return;
			}
			_items[i++] = id;
			_items[i++] = sales;
			_items[i++] = price;
		}
	}

	@Override
	protected void runImpl()
	{
		final Player activeChar = getClient().getActiveChar();
		if(activeChar == null || _count == 0)
			return;

		if(activeChar.getClan() == null)
		{
			activeChar.sendActionFailed();
			return;
		}

		final Castle castle = ResidenceHolder.getInstance().getResidence(Castle.class, _manorId);
		if(castle == null || castle.getOwnerId() != activeChar.getClanId() // clan owns castle
				|| (activeChar.getClanPrivileges() & Clan.CP_CS_MANOR_ADMIN) != Clan.CP_CS_MANOR_ADMIN) // has manor rights
		{
			activeChar.sendActionFailed();
			return;
		}

		if (castle.isNextPeriodApproved())
		{
			activeChar.sendPacket(SystemMsg.A_MANOR_CANNOT_BE_SET_UP_BETWEEN_430_AM_AND_8_PM);
			activeChar.sendActionFailed();
			return;
		}

		final NpcInstance chamberlain = NpcUtils.canPassPacket(activeChar, this);
		if (chamberlain == null || chamberlain.getCastle() != castle)
		{
			activeChar.sendActionFailed();
			return;
		}

		final List<SeedProduction> seeds = new ArrayList<SeedProduction>(_count);
		final List<Seed> checkList = Manor.getInstance().getSeedsForCastle(_manorId);
		for(int i = 0; i < _count; i++)
		{
			int id = (int) _items[i * 3 + 0];
			long sales = _items[i * 3 + 1];
			long price = _items[i * 3 + 2];
			if(id > 0)
			{
				for (Seed check : checkList)
					if (check.getId() == id)
					{
						if (sales > check.getSeedLimit())
							break;

						long basePrice = Manor.getInstance().getSeedBasicPrice(id);
						if (price < basePrice * 60 / 100 || price > basePrice * 10)
							break;

						SeedProduction s = CastleManorManager.getInstance().getNewSeedProduction(id, sales, price, sales);
						seeds.add(s);
						break;
					}
			}
		}

		castle.setSeedProduction(seeds, CastleManorManager.PERIOD_NEXT);
		castle.saveSeedData(CastleManorManager.PERIOD_NEXT);
	}
}