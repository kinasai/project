package com.l2cccp.gameserver.network.l2.s2c;

import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.events.impl.UndergroundColiseumBattleEvent;
import com.l2cccp.gameserver.model.entity.events.objects.UCTeamObject;

/**
 * @author VISTALL
 */
public class ExPVPMatchUserDie extends L2GameServerPacket
{
	private int _blueKills, _redKills;

	public ExPVPMatchUserDie(UndergroundColiseumBattleEvent e)
	{
		UCTeamObject team = e.getFirstObject(TeamType.BLUE);
		_blueKills = team.getKills();
		team = e.getFirstObject(TeamType.RED);
		_redKills = team.getKills();
	}

	public ExPVPMatchUserDie(int blueKills, int redKills)
	{
		_blueKills = blueKills;
		_redKills = redKills;
	}

	@Override
	protected final void writeImpl()
	{
		writeEx(0x7F);
		writeD(_blueKills);
		writeD(_redKills);
	}
}