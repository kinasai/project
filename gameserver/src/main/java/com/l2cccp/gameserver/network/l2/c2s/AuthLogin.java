package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.Shutdown;
import com.l2cccp.gameserver.network.authcomm.AuthServerCommunication;
import com.l2cccp.gameserver.network.authcomm.SessionKey;
import com.l2cccp.gameserver.network.authcomm.gs2as.PlayerAuthRequest;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.s2c.LoginFail;
import com.l2cccp.gameserver.network.l2.s2c.ServerClose;
import com.l2cccp.gameserver.utils.Language;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.strixplatform.StrixPlatform;
import org.strixplatform.logging.Log;

/**
 * cSddddd
 * cSdddddQ
 * loginName + keys must match what the loginserver used.
 */
public class AuthLogin extends L2GameClientPacket
{
	private static final Logger _log = LoggerFactory.getLogger(AuthLogin.class);

	private String _loginName;
	private int _playKey1;
	private int _playKey2;
	private int _loginKey1;
	private int _loginKey2;
	private Language _language;

	@Override
	protected void readImpl()
	{
		_loginName = readS(32).toLowerCase();
		_playKey2 = readD();
		_playKey1 = readD();
		_loginKey1 = readD();
		_loginKey2 = readD();
		_language = Language.valueOf(readD());
	}

	@Override
	protected void runImpl()
	{
		GameClient client = getClient();
		client.setLanguage(_language);
		if(_language == null)
		{
			_log.error("Trying to auth with unknown lang. " + client.toString());
			client.close(new LoginFail(LoginFail.SYSTEM_ERROR_LOGIN_LATER));
			return;
		}

		if(Shutdown.getInstance().getMode() != Shutdown.NONE && Shutdown.getInstance().getSeconds() <= 30)
		{
			client.closeNow(false);
			return;
		}

		if(AuthServerCommunication.getInstance().isShutdown())
		{
			client.close(new LoginFail(LoginFail.SYSTEM_ERROR_LOGIN_LATER));
			return;
		}

		SessionKey key = new SessionKey(_loginKey1, _loginKey2, _playKey1, _playKey2);
		client.setSessionId(key);
		client.setLoginName(_loginName);

		GameClient oldClient = AuthServerCommunication.getInstance().addWaitingClient(client);
		if(oldClient != null)
			oldClient.close(ServerClose.STATIC);

		AuthServerCommunication.getInstance().sendPacket(new PlayerAuthRequest(client));
		// TODO[K] - Strix section start
		if(StrixPlatform.getInstance().isPlatformEnabled())
		{
			if(client.getStrixClientData() != null)
			{
				client.getStrixClientData().setClientAccount(_loginName);
				if(StrixPlatform.getInstance().isAuthLogEnabled())
				{
					Log.auth("Account: [" + _loginName + "] HWID: [" + client.getStrixClientData().getClientHWID() + "] SessionID: [" + client.getStrixClientData().getSessionId() + "] entered to Game Server");
				}
			}
			else
			{
				client.close(ServerClose.STATIC);
				return;
			}
		}
		// TODO[K] - Strix section end
	}
}