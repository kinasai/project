package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.gameserver.dao.MailDAO;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.mail.Mail;
import com.l2cccp.gameserver.network.l2.s2c.ExChangePostState;
import com.l2cccp.gameserver.network.l2.s2c.ExReplyReceivedPost;
import com.l2cccp.gameserver.network.l2.s2c.ExShowReceivedPostList;

/**
 * Запрос информации об полученном письме. Появляется при нажатии на письмо из списка {@link ExShowReceivedPostList}.
 * @see RequestExRequestSentPost
 */
public class RequestExRequestReceivedPost extends L2GameClientPacket
{
	private int postId;

	/**
	 * format: d
	 */
	@Override
	protected void readImpl()
	{
		postId = readD(); // id письма
	}

	@Override
	protected void runImpl()
	{
		Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		Mail mail = MailDAO.getInstance().getReceivedMailByMailId(activeChar.getObjectId(), postId);
		if(mail != null)
		{
			if(mail.isUnread())
			{
				mail.setUnread(false);
				mail.setJdbcState(JdbcEntityState.UPDATED);
				mail.update();
				activeChar.sendPacket(new ExChangePostState(true, Mail.READED, mail));
			}

			activeChar.sendPacket(new ExReplyReceivedPost(mail));
			return;
		}

		activeChar.sendPacket(new ExShowReceivedPostList(activeChar));
	}
}