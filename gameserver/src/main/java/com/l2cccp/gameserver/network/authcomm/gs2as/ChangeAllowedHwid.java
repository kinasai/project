package com.l2cccp.gameserver.network.authcomm.gs2as;

import com.l2cccp.gameserver.network.authcomm.SendablePacket;

public class ChangeAllowedHwid extends SendablePacket
{

	private String account;
	private String hwid;

	public ChangeAllowedHwid(String account, String hwid)
	{
		this.account = account;
		this.hwid = hwid;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0x09);
		writeS(account);
		writeS(hwid);
	}
}