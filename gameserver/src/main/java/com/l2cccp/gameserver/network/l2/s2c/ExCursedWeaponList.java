package com.l2cccp.gameserver.network.l2.s2c;

import java.util.List;

import com.l2cccp.gameserver.data.xml.holder.CursedWeaponsHolder;
import com.l2cccp.gameserver.model.CursedWeapon;

public class ExCursedWeaponList extends L2GameServerPacket
{
	private int[] cursedWeapon_ids;

	public ExCursedWeaponList()
	{
		List<CursedWeapon> list = CursedWeaponsHolder.getInstance().getCursed();
		cursedWeapon_ids = new int[list.size()];

		for(int i = 0; i < list.size(); i++)
			cursedWeapon_ids[i] = list.get(i).getItemId();
	}

	@Override
	protected final void writeImpl()
	{
		writeEx(0x46);
		writeDD(cursedWeapon_ids, true);
	}
}