package com.l2cccp.gameserver.network.l2;

import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.concurrent.Future;

import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.database.L2DatabaseFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.net.nio.impl.MMOClient;
import com.l2cccp.commons.net.nio.impl.MMOConnection;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.GameServer;
import com.l2cccp.gameserver.dao.CharacterDAO;
import com.l2cccp.gameserver.data.client.holder.NpcNameLineHolder;
import com.l2cccp.gameserver.model.CharSelectInfo;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.authcomm.AuthServerCommunication;
import com.l2cccp.gameserver.network.authcomm.SessionKey;
import com.l2cccp.gameserver.network.authcomm.gs2as.PlayerLogout;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ActionFail;
import com.l2cccp.gameserver.network.l2.s2c.CharSelected;
import com.l2cccp.gameserver.network.l2.s2c.Ex2ndPasswordCheck;
import com.l2cccp.gameserver.network.l2.s2c.ExNeedToChangeName;
import com.l2cccp.gameserver.network.l2.s2c.L2GameServerPacket;
import com.l2cccp.gameserver.network.l2.s2c.NetPing;
import com.l2cccp.gameserver.taskmanager.LazyPrecisionTaskManager;
import com.l2cccp.gameserver.utils.AutoBan;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.Util;

import org.strixplatform.network.IStrixClientData;
import org.strixplatform.network.cipher.StrixGameCrypt;
import org.strixplatform.utils.StrixClientData;
/**
 * Represents a client connected on Game Server
 */
//TODO[K] - Guard section start
public final class GameClient extends MMOClient<MMOConnection<GameClient>> implements IStrixClientData
//TODO[K] - Guard section end
{
	private static final Logger _log = LoggerFactory.getLogger(GameClient.class);

	public static enum GameClientState
	{
		CONNECTED,
		AUTHED,
		ENTER_GAME,
		IN_GAME,
		DISCONNECTED
	}

	private StrixClientData clientData;
	private GameClientState _state;
	public StrixGameCrypt gameCrypt = null;
	private String _ip;

	private boolean allowedShare;
	private boolean ShareBlock;
	private String allowedHwid;

	// LameGuard
	private String hwid;
	private int instances;
	private int patch;
	private boolean isProtected;

	//locked data
	private String _lhwid;
	private String _lip;

	private SessionKey _sessionKey;

	/** Данные аккаунта */
	private String _login;

	private int revision = 0;

	private int _selectedIndex = -1;
	private Language _language = Language.ENGLISH;
	private CharSelectInfo[] _csi = new CharSelectInfo[7];

	private Future<?> _pingTask;
	private int _pingTryCount;
	private int _pingTime;

	private int _failedPackets;
	private int _unknownPackets;

	private Player _activeChar;

	protected GameClient(MMOConnection<GameClient> con)
	{
		super(con);

		_state = GameClientState.CONNECTED;
		//TODO[K] - Guard section start
		// Удаляем эту строчку. _crypt = new GameCrypt();
		gameCrypt = new StrixGameCrypt();
		//TODO[K] - Guard section end
		_ip = con.getSocket().getInetAddress().getHostAddress();
	}

	@Override
	protected void onDisconnection()
	{
		final Player player;

		setState(GameClientState.DISCONNECTED);

		stopPingTask();

		player = getActiveChar();
		setActiveChar(null);

		if(player != null)
		{
			player.setNetConnection(null);
			player.scheduleDelete();
		}

		if(getSessionKey() != null)
		{
			if(isAuthed())
			{
				AuthServerCommunication.getInstance().removeAuthedClient(getLogin());
				AuthServerCommunication.getInstance().sendPacket(new PlayerLogout(getLogin()));
			}
			else
			{
				AuthServerCommunication.getInstance().removeWaitingClient(getLogin());
			}
		}
	}

	public void setAllowedHwid(String allowedHwid) {
		this.allowedHwid = allowedHwid;
	}


	@Override
	protected void onForcedDisconnection()
	{
		// TODO Auto-generated method stub

	}

	public void startPingTask()
	{
		_pingTask = LazyPrecisionTaskManager.getInstance().scheduleAtFixedRate(new Runnable(){
			@Override
			public void run()
			{
				if(++_pingTryCount > 2)
					closeNow(true);
				else
				{
					int time = (int) (System.currentTimeMillis() - GameServer.getInstance().getServerStartTime());
					sendPacket(new NetPing(time));
				}
			}
		}, 30000L, 30000L);
	}

	public void onNetPing(int time)
	{
		_pingTryCount--;
		_pingTime = (int) (System.currentTimeMillis() - GameServer.getInstance().getServerStartTime() - time);
		_pingTime /= 2;
	}

	public int getPing()
	{
		return _pingTime;
	}

	public void stopPingTask()
	{
		if(_pingTask != null)
		{
			_pingTask.cancel(false);
			_pingTask = null;
		}
	}

	public int getObjectIdByIndex(int charslot)
	{
		if(charslot < 0 || charslot >= _csi.length)
		{
			_log.warn(getLogin() + " tried to modify Character in slot " + charslot + " but no characters exits at that slot.");
			return -1;
		}
		CharSelectInfo p = _csi[charslot];
		return p == null ? 0 : p.getObjectId();
	}

	public Player getActiveChar()
	{
		return _activeChar;
	}

	public SessionKey getSessionKey()
	{
		return _sessionKey;
	}

	public String getLogin()
	{
		return _login;
	}

	public void setLoginName(String loginName)
	{
		_login = loginName;
	}

	public void setActiveChar(Player player)
	{
		_activeChar = player;
		if(player != null)
			player.setNetConnection(this);
	}

	public void setSessionId(SessionKey sessionKey)
	{
		_sessionKey = sessionKey;
	}

	public void setCharSelection(CharSelectInfo[] chars)
	{
		_csi = chars;
	}

	public int getRevision()
	{
		return revision;
	}

	public void setRevision(int revision)
	{
		this.revision = revision;
	}

	public void playerSelected(int index)
	{
		int objectIdByIndex = getObjectIdByIndex(index);
		if(objectIdByIndex <= 0 || getActiveChar() != null || AutoBan.isBanned(objectIdByIndex))
		{
			sendPacket(ActionFail.STATIC);
			return;
		}

		_selectedIndex = index;
		CharSelectInfo info = _csi[index];
		if(info == null)
			return;

		if(Config.EX_CHANGE_NAME_DIALOG)
		{
			if(!Util.isMatchingRegexp(info.getName(), Config.CNAME_TEMPLATE) || NpcNameLineHolder.getInstance().isBlackListContainsName(info.getName()))
			{
				sendPacket(new ExNeedToChangeName(ExNeedToChangeName.TYPE_PLAYER_NAME, ExNeedToChangeName.REASON_INVALID, info.getName()));
				return;
			}
			else if(CharacterDAO.getInstance().getPlayersCountByName(info.getName()) > 1)
			{
				sendPacket(new ExNeedToChangeName(ExNeedToChangeName.TYPE_PLAYER_NAME, ExNeedToChangeName.REASON_EXISTS, info.getName()));
				return;
			}
		}

		if(Config.SECOND_AUTH_ENABLED)
		{
			if(info.isPasswordEnable())
			{
				if(!info.isPasswordChecked())
				{
					sendPacket(new Ex2ndPasswordCheck(info.getPassword() == null ? Ex2ndPasswordCheck.PASSWORD_NEW : Ex2ndPasswordCheck.PASSWORD_PROMPT));
					return;
				}
			}
		}

		Player noCarrierPlayer = null;
		CharSelectInfo[] array = getCharacters();
		for(int i = 0; i < array.length; i++)
		{
			CharSelectInfo p = array[i];
			Player player = p != null ? GameObjectsStorage.getPlayer(p.getObjectId()) : null;
			if(player != null)
			{
				// если у нас чар в оффлайне, или выходит, и этот чар выбран - кикаем
				if(player.isInOfflineMode() || player.isLogoutStarted())
				{
					if(index == i)
						player.kick();
				}
				else
				{
					player.sendPacket(SystemMsg.ANOTHER_PERSON_HAS_LOGGED_IN_WITH_THE_SAME_ACCOUNT);

					// если есть чар
					// если это выбраный - обнуляем конект и используем, иначе кикаем)
					if(index == i)
					{
						noCarrierPlayer = player;

						GameClient oldClient = player.getNetConnection();
						if(oldClient != null)
						{
							oldClient.setActiveChar(null);
							oldClient.closeNow(false);
						}
					}
					else
						player.logout();
				}
			}
		}

		Player selectedPlayer = noCarrierPlayer == null ? Player.restore(objectIdByIndex) : noCarrierPlayer;
		if(selectedPlayer == null)
		{
			sendPacket(ActionFail.STATIC);
			return;
		}

		if(selectedPlayer.getAccessLevel() < 0)
			selectedPlayer.setAccessLevel(0);

		setActiveChar(selectedPlayer);
		setState(GameClient.GameClientState.ENTER_GAME);

		sendPacket(new CharSelected(selectedPlayer, getSessionKey().playOkID1));
	}

	@Override
	public boolean encrypt(final ByteBuffer buf, final int size)
	{
		//TODO[K] - Guard section start
		//_crypt.encrypt(buf.array(), buf.position(), size);
		gameCrypt.encrypt(buf.array(), buf.position(), size);
		//TODO[K] - Guard section end
		buf.position(buf.position() + size);
		return true;
	}

	@Override
	public boolean decrypt(ByteBuffer buf, int size)
	{
		//TODO[K] - Guard section start
		//return _crypt.decrypt(buf.array(), buf.position(), size);
		return gameCrypt.decrypt(buf.array(), buf.position(), size);
		//TODO[K] - Guard section end
	}

	public void sendPacket(L2GameServerPacket gsp)
	{
		if(isConnected())
			getConnection().sendPacket(gsp);
	}

	public void sendPacket(L2GameServerPacket... gsp)
	{
		if(isConnected())
			getConnection().sendPacket(gsp);
	}

	public void sendPackets(List<L2GameServerPacket> gsp)
	{
		if(isConnected())
			getConnection().sendPackets(gsp);
	}

	public void close(L2GameServerPacket gsp)
	{
		if(isConnected())
			getConnection().close(gsp);
	}

	public String getIpAddr()
	{
		return _ip;
	}

	public byte[] enableCrypt()
	{
		byte[] key = BlowFishKeygen.getRandomKey();
		//TODO[K] - Guard section start
		//// Удаляем эту строчку. _crypt.setKey(key);
		gameCrypt.setKey(key);
		// TODO[K] - Strix section end
		return key;
	}

	public GameClientState getState()

	{
		return _state;
	}

	public String getHWID()
	{
		return getStrixClientData().getClientHWID();
	}
	public void setState(GameClientState state)
	{
		_state = state;
	}

	public void onPacketReadFail()
	{
		if(_failedPackets++ >= 10)
		{
			_log.warn("Too many client packet fails, connection closed : " + this);
			closeNow(true);
		}
	}

	public void onUnknownPacket(int id)
	{
		if(_unknownPackets++ >= 10)
		{
			_log.warn("Too many client unknown packets, id:" + id + ", connection closed: " + this);
			closeNow(true);
		}
	}

	//TODO[K] - Guard section start
	@Override
	public void setStrixClientData(final StrixClientData clientData)
	{
		this.clientData = clientData;
	}

	@Override
	public StrixClientData getStrixClientData()
	{
		return clientData;
	}
	//TODO[K] - Guard section end

	public CharSelectInfo[] getCharacters()
	{
		return _csi;
	}

	public int getSelectedIndex()
	{
		return _selectedIndex;
	}

	public Language getLanguage()
	{
		return _language;
	}

	public void setLanguage(Language language)
	{
		_language = language;
	}

	@Override
	public String toString()
	{
		return _state + " IP: " + getIpAddr() + (_login == null ? "" : " Account: " + _login) + (_activeChar == null ? "" : " Player : " + _activeChar);
	}



	public void checkHwid()
	{
		boolean crit = false;
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		allowedHwid = null;
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT allow_hwid FROM accounts_lock WHERE login = ?");
			statement.setString(1, getLogin());
			rset = statement.executeQuery();

			if (rset.next())
			{
				setAllowedHwid(rset.getString("allow_hwid"));
			}


			if(notEmpty(allowedHwid) && getHWID() != null && !allowedHwid.equals(getStrixClientData().getClientHWID()))
			{
				if(allowedShare)
					setShareBlock(true);
				else
					crit = true;
			}
		}
		catch(Exception e)
		{
			_log.warn("Player in account " + getLogin() + " have incorrect HWID!");
			_log.warn("HWID DEBUG: Client HWID -> " + getHWID() + " (Length = " + getHWID().length() + ")");
			_log.warn("HWID DEBUG: Allowed -> " + allowedHwid + " (Length = " + allowedHwid.length() + ")");
			_log.warn("Error - > ", e);
			crit = true;
		}

		if(crit)
			closeNow(false);
	}

	private boolean notEmpty(String allowedHwid)
	{
		return allowedHwid != null && !allowedHwid.isEmpty() && allowedHwid.length() > 1;
	}

	public void setAllowedShare(boolean allowedShare)
	{
		this.allowedShare = allowedShare;
	}

	public boolean getShareBlock()
	{
		return ShareBlock;
	}

	public void setShareBlock(boolean ShareBlock)
	{
		this.ShareBlock = ShareBlock;
	}

	public void setLocked(String hwid, String ip)
	{
		_lhwid = hwid;
		_lip = ip;
	}

	public boolean hasLocked()
	{
		return (!_lhwid.equals(StringUtils.EMPTY) || !_lip.equals(StringUtils.EMPTY));
	}
}