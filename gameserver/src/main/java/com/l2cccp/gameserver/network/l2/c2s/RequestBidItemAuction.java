package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.data.xml.holder.ItemAuctionHolder;
import com.l2cccp.gameserver.instancemanager.itemauction.ItemAuction;
import com.l2cccp.gameserver.instancemanager.itemauction.ItemAuctionInstance;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;

/**
 * @author n0nam3
 */
public final class RequestBidItemAuction extends L2GameClientPacket
{
	private int _instanceId;
	private long _bid;

	@Override
	protected final void readImpl()
	{
		_instanceId = readD();
		_bid = readQ();
	}

	@Override
	protected final void runImpl()
	{
		final Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;
		if(_bid < 0 || _bid > activeChar.getAdena())
			return;

		final ItemAuctionInstance instance = ItemAuctionHolder.getInstance().get(_instanceId);
		NpcInstance broker = activeChar.getLastNpc();
		if(broker == null || broker.getNpcId() != _instanceId || !activeChar.isInRangeZ(broker, Creature.INTERACTION_DISTANCE))
			return;
		if(instance != null)
		{
			final ItemAuction auction = instance.getCurrentAuction();
			if(auction != null)
				auction.registerBid(activeChar, _bid);
		}
	}
}