package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.dao.CharacterTPBookmarkDAO;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.TpBookMark;
import com.l2cccp.gameserver.network.l2.s2c.ExGetBookMarkInfo;

public class RequestDeleteBookMarkSlot extends L2GameClientPacket
{
	private int _slot;

	@Override
	protected void readImpl()
	{
		_slot = readD();
	}

	@Override
	protected void runImpl()
	{
		Player player = getClient().getActiveChar();
		if(player == null)
			return;

		TpBookMark bookMark = player.getTpBookMarks().remove(_slot - 1);
		if(bookMark != null)
		{
			CharacterTPBookmarkDAO.getInstance().delete(player, bookMark);
			player.sendPacket(new ExGetBookMarkInfo(player));
		}
	}
}