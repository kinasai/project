package com.l2cccp.gameserver.network.authcomm.gs2as;

import java.util.List;

import com.l2cccp.commons.net.utils.NetInfo;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.network.authcomm.SendablePacket;
import com.l2cccp.gameserver.network.authcomm.channel.AbstractServerChannel;

public class AuthRequest2 extends SendablePacket
{
	private final AbstractServerChannel _channel;

	public AuthRequest2(AbstractServerChannel abstractServerChannel)
	{
		_channel = abstractServerChannel;
	}

	@Override
	protected void writeImpl()
	{
		writeC(0x00);
		writeD(0x03);
		writeC(_channel.getId());
		writeC(Config.ACCEPT_ALTERNATE_ID ? 0x01 : 0x00);
		writeD(Config.AUTH_SERVER_SERVER_TYPE);
		writeD(Config.AUTH_SERVER_AGE_LIMIT);
		writeC(Config.AUTH_SERVER_GM_ONLY ? 0x01 : 0x00);
		writeC(Config.AUTH_SERVER_BRACKETS ? 0x01 : 0x00);
		writeC(Config.AUTH_SERVER_IS_PVP ? 0x01 : 0x00);
		writeD(Config.MAXIMUM_ONLINE_USERS);

		List<NetInfo> infos = _channel.getInfos();
		writeD(infos.size());
		for(NetInfo info : infos)
		{
			writeD(info.net().netmask());
			writeD(info.net().address());
			writeS(info.host());
			writeH(info.ports().length);
			for(int PORT_GAME : info.ports())
				writeH(PORT_GAME);
		}
	}
}
