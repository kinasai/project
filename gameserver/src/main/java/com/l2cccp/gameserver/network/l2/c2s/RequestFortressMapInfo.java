package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.data.xml.holder.ResidenceHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.residence.Fortress;
import com.l2cccp.gameserver.network.l2.s2c.ExShowFortressMapInfo;

public class RequestFortressMapInfo extends L2GameClientPacket
{
	private int _fortressId;

	@Override
	protected void readImpl()
	{
		_fortressId = readD();
	}

	@Override
	protected void runImpl()
	{
		Player player = getClient().getActiveChar();
		if(player == null)
			return;
		Fortress fortress = ResidenceHolder.getInstance().getResidence(Fortress.class, _fortressId);
		if(fortress != null)
			sendPacket(new ExShowFortressMapInfo(fortress));
	}
}