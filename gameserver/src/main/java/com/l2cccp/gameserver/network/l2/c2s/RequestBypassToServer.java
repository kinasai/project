package com.l2cccp.gameserver.network.l2.c2s;

import java.lang.reflect.Method;
import java.util.StringTokenizer;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.MultiSellHolder;
import com.l2cccp.gameserver.handler.admincommands.AdminCommandHandler;
import com.l2cccp.gameserver.handler.bbs.BbsHandlerHolder;
import com.l2cccp.gameserver.handler.bbs.IBbsHandler;
import com.l2cccp.gameserver.handler.bypass.BypassHolder;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.instancemanager.OlympiadHistoryManager;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.Hero;
import com.l2cccp.gameserver.model.entity.events.impl.SingleMatchEvent;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.offlinebuffer.OfflineBufferManager;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.utils.BypassStorage.ValidBypass;
import com.l2cccp.gameserver.utils.NpcUtils;

public class RequestBypassToServer extends L2GameClientPacket
{
	private static final Logger _log = LoggerFactory.getLogger(RequestBypassToServer.class);

	private String _bypass = null;

	@Override
	protected void readImpl()
	{
		_bypass = readS();
	}

	@Override
	protected void runImpl()
	{
		Player activeChar = getClient().getActiveChar();
		if(activeChar == null || _bypass.isEmpty())
			return;

		ValidBypass bp = activeChar.getBypassStorage().validate(_bypass);
		if(bp == null)
		{
			_log.debug("RequestBypassToServer: Unexpected bypass : " + _bypass + " client : " + getClient() + "!");
			return;
		}

		NpcInstance npc = activeChar.getLastNpc();
		GameObject target = activeChar.getTarget();
		if(npc == null && target != null && target.isNpc())
			npc = (NpcInstance) target;

		try
		{
			if(_bypass.startsWith("admin_"))
				AdminCommandHandler.getInstance().useAdminCommandHandler(activeChar, _bypass);
			else if(_bypass.startsWith("player_help "))
				playerHelp(activeChar, _bypass.substring(12));
			else if(bp.bypass.startsWith("offbuff"))
				OfflineBufferManager.getInstance().processBypass(activeChar, _bypass);
			else if(_bypass.startsWith("scripts_"))
			{
				_log.error("Trying to call script bypass: " + _bypass + " " + activeChar);
			}
			else if(_bypass.startsWith("htmbypass_"))
			{
				String command = _bypass.substring(10).trim();
				String word = command.split("\\s+")[0];

				Pair<Object, Method> b = BypassHolder.getInstance().getBypass(word);
				if(b != null)
					b.getValue().invoke(b.getKey(), activeChar, npc, command.substring(word.length()).trim().split("\\s+"));
			}
			else if(_bypass.startsWith("user_"))
			{
				String command = _bypass.substring(5).trim();
				String word = command.split("\\s+")[0];
				String args = command.substring(word.length()).trim();
				IVoicedCommandHandler vch = VoicedCommandHandler.getInstance().getVoicedCommandHandler(word);

				if(vch != null)
					vch.useVoicedCommand(word, activeChar, args);
				else
					_log.warn("Unknow voiced command '" + word + "'");
			}
			else if((bp.bypass.startsWith("_bbs") || bp.bypass.startsWith("_mail") || bp.bypass.startsWith("_friend")) && (activeChar.isDead() && !Config.BBS_CHECK_DEATH || activeChar.isMovementDisabled() && !Config.BBS_CHECK_MOVEMENT_DISABLE || activeChar.isOnSiegeField() && !Config.BBS_CHECK_ON_SIEGE_FIELD || activeChar.isInCombat() && !Config.BBS_CHECK_IN_COMBAT || activeChar.isAttackingNow() && !Config.BBS_CHECK_ATTACKING_NOW || activeChar.isInOlympiadMode() && !Config.BBS_CHECK_IN_OLYMPIAD_MODE || activeChar.getVar("jailed") != null || activeChar.isFlying() && !Config.BBS_CHECK_FLYING || activeChar.isInDuel() && !Config.BBS_CHECK_IN_DUEL || activeChar.getReflectionId() > 0 && !Config.BBS_CHECK_IN_INSTANCE || activeChar.isOutOfControl() && !Config.BBS_CHECK_OUT_OF_CONTROL || (activeChar.getEvent(SingleMatchEvent.class) != null && activeChar.getEvent(SingleMatchEvent.class).isInProgress() && !Config.BBS_CHECK_IN_EVENT) || !activeChar.isInZonePeace() && !activeChar.getBonus().checkPremium() && Config.BBS_CHECK_OUT_OF_TOWN_ONLY_FOR_PREMIUM))
			{
				activeChar.sendPacket(new ExShowScreenMessage(new CustomMessage("communityboard.checkcondition.false.screen").toString(activeChar), 5000, ExShowScreenMessage.ScreenMessageAlign.TOP_CENTER, true));
				activeChar.sendMessage(new CustomMessage("communityboard.checkcondition.false.chat"));

				String html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/system/terms.htm", activeChar);
				String check = " <font color=\"LEVEL\">*</font>";
				String isTrue = "<font color=\"66FF33\">" + new CustomMessage("common.allowed").toString(activeChar) + "</font>";
				String isFalse = "<font color=\"FF0000\">" + new CustomMessage("common.prohibited").toString(activeChar) + "</font>";
				String onlyPremium = "<font color=\"LEVEL\">" + new CustomMessage("common.need.premium").toString(activeChar) + "</font>";

				html = html.replace("%config_isInZonePeace%", Config.BBS_CHECK_OUT_OF_TOWN_ONLY_FOR_PREMIUM ? onlyPremium : isTrue);
				html = html.replace("%config_isDead%", Config.BBS_CHECK_DEATH ? isTrue : isFalse);
				html = html.replace("%config_isMovementDisabled%", Config.BBS_CHECK_MOVEMENT_DISABLE ? isTrue : isFalse);
				html = html.replace("%config_isOnSiegeField%", Config.BBS_CHECK_ON_SIEGE_FIELD ? isTrue : isFalse);
				html = html.replace("%config_isInCombat%", Config.BBS_CHECK_IN_COMBAT ? isTrue : isFalse);
				html = html.replace("%config_isAttackingNow%", Config.BBS_CHECK_ATTACKING_NOW ? isTrue : isFalse);
				html = html.replace("%config_isInOlympiadMode%", Config.BBS_CHECK_IN_OLYMPIAD_MODE ? isTrue : isFalse);
				html = html.replace("%config_isFlying%", Config.BBS_CHECK_FLYING ? isTrue : isFalse);
				html = html.replace("%config_isInDuel%", Config.BBS_CHECK_IN_DUEL ? isTrue : isFalse);
				html = html.replace("%config_isInInstance%", Config.BBS_CHECK_IN_INSTANCE ? isTrue : isFalse);
				html = html.replace("%config_isInJailed%", Config.BBS_CHECK_IN_JAILED ? isTrue : isFalse);
				html = html.replace("%config_isOutOfControl%", Config.BBS_CHECK_OUT_OF_CONTROL ? isTrue : isFalse);
				html = html.replace("%config_isInEvent%", Config.BBS_CHECK_IN_EVENT ? isTrue : isFalse);

				html = html.replace("%check_isInZonePeace%", !activeChar.isInZonePeace() && !activeChar.getBonus().checkPremium() && Config.BBS_CHECK_OUT_OF_TOWN_ONLY_FOR_PREMIUM ? check : "");
				html = html.replace("%check_isDead%", activeChar.isDead() && !Config.BBS_CHECK_DEATH ? check : "");
				html = html.replace("%check_isMovementDisabled%", activeChar.isMovementDisabled() && !Config.BBS_CHECK_MOVEMENT_DISABLE ? check : "");
				html = html.replace("%check_isOnSiegeField%", activeChar.isOnSiegeField() && !Config.BBS_CHECK_ON_SIEGE_FIELD ? check : "");
				html = html.replace("%check_isInCombat%", activeChar.isInCombat() && !Config.BBS_CHECK_IN_COMBAT ? check : "");
				html = html.replace("%check_isAttackingNow%", activeChar.isAttackingNow() && !Config.BBS_CHECK_ATTACKING_NOW ? check : "");
				html = html.replace("%check_isInOlympiadMode%", activeChar.isInOlympiadMode() && !Config.BBS_CHECK_IN_OLYMPIAD_MODE ? check : "");
				html = html.replace("%check_isFlying%", activeChar.isFlying() && !Config.BBS_CHECK_FLYING ? check : "");
				html = html.replace("%check_isInDuel%", activeChar.isInDuel() && !Config.BBS_CHECK_IN_DUEL ? check : "");
				html = html.replace("%check_isInInstance%", activeChar.getReflectionId() > 0 && !Config.BBS_CHECK_IN_INSTANCE ? check : "");
				html = html.replace("%check_isInJailed%", activeChar.getVar("jailed") != null && !Config.BBS_CHECK_IN_JAILED ? check : "");
				html = html.replace("%check_isOutOfControl%", activeChar.isOutOfControl() && !Config.BBS_CHECK_OUT_OF_CONTROL ? check : "");
				html = html.replace("%check_isInEvent%", activeChar.getEvent(SingleMatchEvent.class) != null && activeChar.getEvent(SingleMatchEvent.class).isInProgress() && !Config.BBS_CHECK_IN_EVENT ? check : "");

				ShowBoard.separateAndSend(html, activeChar);
				return;
			}
			else if(_bypass.startsWith("npc_"))
			{
				int endOfId = _bypass.indexOf('_', 5);
				String id;
				if(endOfId > 0)
					id = _bypass.substring(4, endOfId);
				else
					id = _bypass.substring(4);
				GameObject object = activeChar.getVisibleObject(Integer.parseInt(id));
				if(object != null && object.isNpc() && endOfId > 0 && activeChar.isInRangeZ(object.getLoc(), Creature.INTERACTION_DISTANCE))
				{
					activeChar.setLastNpc((NpcInstance) object);
					((NpcInstance) object).onBypassFeedback(activeChar, _bypass.substring(endOfId + 1));
				}
			}
			else if(_bypass.startsWith("_olympiad?")) // _olympiad?command=move_op_field&field=1
			{
				// Переход в просмотр олимпа разрешен только от менеджера или с арены.
				final NpcInstance manager = NpcUtils.canPassPacket(activeChar, this, _bypass.split("&")[0]);
				if(manager != null)
					manager.onBypassFeedback(activeChar, _bypass);
			}
			else if(_bypass.startsWith("_diary"))
			{
				String params = _bypass.substring(_bypass.indexOf("?") + 1);
				StringTokenizer st = new StringTokenizer(params, "&");
				int heroclass = Integer.parseInt(st.nextToken().split("=")[1]);
				int heropage = Integer.parseInt(st.nextToken().split("=")[1]);
				int heroid = Hero.getInstance().getHeroByClass(heroclass);
				if(heroid > 0)
					Hero.getInstance().showHeroDiary(activeChar, heroclass, heroid, heropage);
			}
			else if(_bypass.startsWith("_match"))
			{
				String params = _bypass.substring(_bypass.indexOf("?") + 1);
				StringTokenizer st = new StringTokenizer(params, "&");
				int heroclass = Integer.parseInt(st.nextToken().split("=")[1]);
				int heropage = Integer.parseInt(st.nextToken().split("=")[1]);

				OlympiadHistoryManager.getInstance().showHistory(activeChar, heroclass, heropage);
			}
			else if(_bypass.startsWith("manor_menu_select?")) // Navigate throught Manor windows
			{
				GameObject object = activeChar.getTarget();
				if(object != null && object.isNpc())
					((NpcInstance) object).onBypassFeedback(activeChar, _bypass);
			}
			else if(_bypass.startsWith("multisell "))
			{
				MultiSellHolder.getInstance().SeparateAndSend(Integer.parseInt(_bypass.substring(10)), activeChar, npc != null ? npc.getObjectId() : -1, 0);
			}
			else if(_bypass.startsWith("Quest "))
			{
				/*String p = _bypass.substring(6).trim();
				int idx = p.indexOf(' ');
				if(idx < 0)
					activeChar.processQuestEvent(Integer.parseInt(p.split("_")[1]), StringUtils.EMPTY, npc);
				else
					activeChar.processQuestEvent(Integer.parseInt(p.substring(0, idx).split("_")[1]), p.substring(idx).trim(), npc); */
				_log.warn("Trying to call Quest bypass: " + _bypass + ", player: " + activeChar);
			}
			else if(bp.bbs)
			{
				if(!Config.COMMUNITYBOARD_ENABLED)
					activeChar.sendPacket(SystemMsg.THE_COMMUNITY_SERVER_IS_CURRENTLY_OFFLINE);
				else
				{
					IBbsHandler handler = BbsHandlerHolder.getInstance().getCommunityHandler(_bypass);
					if(handler != null)
						handler.onBypassCommand(activeChar, _bypass);
				}
			}
		}
		catch(Exception e)
		{
			String st = "Error while handling bypass: " + _bypass;
			if(npc != null)
				st = st + " via NPC " + npc;

			_log.error(st, e);
		}
	}

	private static void playerHelp(Player activeChar, String path)
	{
		HtmlMessage html = new HtmlMessage(5);
		html.setFile(path);
		activeChar.sendPacket(html);
	}
}