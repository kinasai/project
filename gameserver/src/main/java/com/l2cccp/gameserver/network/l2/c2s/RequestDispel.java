package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill.SkillType;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.EffectType;

public class RequestDispel extends L2GameClientPacket
{
	private int _objectId, _id, _level;

	@Override
	protected void readImpl() throws Exception
	{
		_objectId = readD();
		_id = readD();
		_level = readD();
	}

	@Override
	protected void runImpl() throws Exception
	{
		final Player activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		boolean showMsg = true;
		Creature target = activeChar;
		if(target.getObjectId() != _objectId)
		{
			showMsg = false;
			target = activeChar.getServitor();
			if(target == null || target.getObjectId() != _objectId)
				return;
		}

		for(Effect e : target.getEffectList().getAllEffects())
		{
			if(e.getDisplayId() == _id && e.getDisplayLevel() == _level)
			{
				if(!e.isOffensive() && e.getSkill().getTemplate().isSelfDispellable())
				{
					if(e.getSkill().getTemplate().isMusic() && !Config.SKILLS_DISPEL_SONGS)
						return;

					if(e.getSkill().getSkillType() == SkillType.TRANSFORMATION && !Config.SKILLS_DISPEL_TRANSFORMATION)
						return;

					if(e.getTemplate().getEffectType() == EffectType.Hourglass && !Config.SKILLS_DISPEL_HOURGLASS)
						return;

					e.exit(false);
					if(showMsg)
						activeChar.sendPacket(new SystemMessage(SystemMsg.THE_EFFECT_OF_S1_HAS_BEEN_REMOVED).addSkillName(e.getSkill().getId(), e.getSkill().getDisplayLevel()));
				}
				else
					return;
			}
		}
	}
}