package com.l2cccp.gameserver.network.authcomm.channel;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.net.utils.NetInfo;

public abstract class AbstractServerChannel
{
	private final List<NetInfo> _list = new ArrayList<NetInfo>();
	private final int _id;

	public AbstractServerChannel(int id)
	{
		_id = id;
	}

	public void add(NetInfo i)
	{
		_list.add(i);
	}

	public List<NetInfo> getInfos()
	{
		return _list;
	}

	public int getId()
	{
		return _id;
	}
}
