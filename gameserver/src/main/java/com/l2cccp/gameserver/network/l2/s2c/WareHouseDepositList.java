package com.l2cccp.gameserver.network.l2.s2c;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.lang.ArrayUtils;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.ItemInfo;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.items.Warehouse.ItemClassComparator;
import com.l2cccp.gameserver.model.items.Warehouse.WarehouseType;


public class WareHouseDepositList extends L2GameServerPacket
{
	private int _whtype;
	private long _adena;
	private List<ItemInfo> _itemList;

	public WareHouseDepositList(Player cha, WarehouseType whtype)
	{
		_whtype = whtype.ordinal();
		_adena = cha.getAdena();
		boolean privatewh = _whtype == 1;
		ItemInstance[] items = cha.getInventory().getItems();
		ArrayUtils.eqSort(items, ItemClassComparator.getInstance());
		_itemList = new ArrayList<ItemInfo>(items.length);
		for(ItemInstance item : items)
			if(item.canBeStored(cha, privatewh))
				_itemList.add(new ItemInfo(item));
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0x41);
		writeH(_whtype);
		writeQ(_adena);
		writeH(_itemList.size());
		for(ItemInfo item : _itemList)
		{
			writeItemInfo(item);
			writeD(item.getObjectId());
		}
	}
}