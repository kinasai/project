package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.petition.PetitionMainGroup;
import com.l2cccp.gameserver.model.petition.PetitionSubGroup;
import com.l2cccp.gameserver.network.l2.s2c.ExResponseShowContents;

/**
 * @author VISTALL
 */
public class RequestExShowStepThree extends L2GameClientPacket
{
	private int _subId;

	@Override
	protected void readImpl()
	{
		_subId = readC();
	}

	@Override
	protected void runImpl()
	{
		Player player = getClient().getActiveChar();
		if(player == null || !Config.EX_NEW_PETITION_SYSTEM)
			return;

		PetitionMainGroup group = player.getPetitionGroup();
		if(group == null)
			return;

		PetitionSubGroup subGroup = group.getSubGroup(_subId);
		if(subGroup == null)
			return;

		player.sendPacket(new ExResponseShowContents(subGroup.getDescription(player.getLanguage())));
	}
}