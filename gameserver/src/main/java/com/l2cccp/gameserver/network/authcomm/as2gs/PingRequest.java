package com.l2cccp.gameserver.network.authcomm.as2gs;

import com.l2cccp.gameserver.network.authcomm.AuthServerCommunication;
import com.l2cccp.gameserver.network.authcomm.ReceivablePacket;
import com.l2cccp.gameserver.network.authcomm.gs2as.PingResponse;

public class PingRequest extends ReceivablePacket
{
	@Override
	public void readImpl()
	{
		
	}

	@Override
	protected void runImpl()
	{
		AuthServerCommunication.getInstance().sendPacket(new PingResponse());
	}
}