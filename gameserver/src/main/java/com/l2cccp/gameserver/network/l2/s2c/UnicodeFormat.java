package com.l2cccp.gameserver.network.l2.s2c;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;

public final class UnicodeFormat extends L2GameServerPacket {

	private static final Pattern IP_PATTERN = Pattern.compile("^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$");

	private List<String> _addresses = new ArrayList<String>();

	public UnicodeFormat() {
		try {
			Enumeration<NetworkInterface> ni = NetworkInterface.getNetworkInterfaces();
			while(ni.hasMoreElements()) {
				NetworkInterface net = ni.nextElement();
				if(net.isVirtual() || net.isLoopback() || !net.isUp() || net.isPointToPoint())
					continue;

				Enumeration<InetAddress> ias = net.getInetAddresses();
				while(ias.hasMoreElements()) {
					String address = ias.nextElement().getHostAddress();
					if(IP_PATTERN.matcher(address).matches())
						_addresses.add(address);
				}
			}
		} catch (Exception e) {  /* */ }
	}

	@Override
	protected final void writeImpl()
	{
		writeC(0xFF);
		writeD(_addresses.size());
		for(String address : _addresses)
			writeS(address);
	}
}