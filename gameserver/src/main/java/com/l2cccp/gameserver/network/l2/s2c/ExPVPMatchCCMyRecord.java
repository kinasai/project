package com.l2cccp.gameserver.network.l2.s2c;

import com.l2cccp.gameserver.model.entity.events.objects.KrateisCubePlayerObject;

/**
 * @author VISTALL
 */
public class ExPVPMatchCCMyRecord extends L2GameServerPacket
{
	private int _points;

	public ExPVPMatchCCMyRecord(KrateisCubePlayerObject player)
	{
		_points = player.getPoints();
	}

	public ExPVPMatchCCMyRecord(final int points)
	{
		_points = points;
	}

	@Override
	public void writeImpl()
	{
		writeEx(0x8A);
		writeD(_points);
	}
}