package com.l2cccp.gameserver.network.authcomm.as2gs;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.authcomm.AuthServerCommunication;
import com.l2cccp.gameserver.network.authcomm.ReceivablePacket;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;

public class ChangePasswordResponse extends ReceivablePacket
{
	String account;
	boolean changed;

	@Override
	public void readImpl()
	{
		account = readS();
		changed = readD() == 1;
	}

	@Override
	protected void runImpl()
	{
		GameClient client = AuthServerCommunication.getInstance().getAuthedClient(account);
		if(client == null)
			return;

		Player activeChar = client.getActiveChar();
		if(activeChar == null)
			return;

		if(changed)
		{
			String result = new CustomMessage("cabinet.password.result.true").toString(activeChar);
			activeChar.setPasswordResult("<font color=\"33CC33\">" + result + "</font>");
			activeChar.sendMessage(result);
		}
		else
		{
			String result = new CustomMessage("cabinet.password.result.false").toString(activeChar);
			activeChar.setPasswordResult("<font color=\"33CC33\">" + result + "</font>");
			activeChar.sendMessage(result);
		}
	}
}
