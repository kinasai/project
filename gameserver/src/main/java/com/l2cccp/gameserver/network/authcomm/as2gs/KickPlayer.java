package com.l2cccp.gameserver.network.authcomm.as2gs;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.authcomm.AuthServerCommunication;
import com.l2cccp.gameserver.network.authcomm.ReceivablePacket;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.s2c.ServerClose;

public class KickPlayer extends ReceivablePacket
{
	String account;

	@Override
	public void readImpl()
	{
		account = readS();
	}

	@Override
	protected void runImpl()
	{
		GameClient client = AuthServerCommunication.getInstance().removeWaitingClient(account);
		if(client == null)
			client = AuthServerCommunication.getInstance().removeAuthedClient(account);
		if(client == null)
			return;

		Player activeChar = client.getActiveChar();
		if(activeChar != null)
			activeChar.kick();
		else
			client.close(ServerClose.STATIC);
	}
}