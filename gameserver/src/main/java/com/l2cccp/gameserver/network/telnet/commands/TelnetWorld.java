package com.l2cccp.gameserver.network.telnet.commands;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.telnet.TelnetCommand;
import com.l2cccp.gameserver.network.telnet.TelnetCommandHolder;
import com.l2cccp.gameserver.tables.GmListTable;


public class TelnetWorld implements TelnetCommandHolder
{
	private Set<TelnetCommand> _commands = new LinkedHashSet<TelnetCommand>();

	public TelnetWorld()
	{
		_commands.add(new TelnetCommand("find"){

			@Override
			public String getUsage()
			{
				return "find <name>";
			}

			@Override
			public String handle(String[] args)
			{
				if(args.length == 0)
					return null;

				Iterable<Player> players = GameObjectsStorage.getPlayers();
				Iterator<Player> itr = players.iterator();
				StringBuilder sb = new StringBuilder();
				int count = 0;
				Player player;
				Pattern pattern = Pattern.compile(args[0] + "\\S+", Pattern.CASE_INSENSITIVE);
				while(itr.hasNext())
				{
					player = itr.next();

					if(pattern.matcher(player.getName()).matches())
					{
						count++;
						sb.append(player).append("\r\n");
					}
				}

				if(count == 0)
					sb.append("Player not found.").append("\r\n");
				else
				{
					sb.append("=================================================\r\n");
					sb.append("Found: ").append(count).append(" players.").append("\r\n");
				}

				return sb.toString();
			}

		});
		_commands.add(new TelnetCommand("whois", "who"){

			@Override
			public String getUsage()
			{
				return "whois <name>";
			}

			@Override
			public String handle(String[] args)
			{
				if(args.length == 0)
					return null;

				Player player = GameObjectsStorage.getPlayer(args[0]);
				if(player == null)
					return "Player not found.\r\n";

				StringBuilder sb = new StringBuilder();

				sb.append("Name: .................... ").append(player.getName()).append("\r\n");
				sb.append("ID: ...................... ").append(player.getObjectId()).append("\r\n");
				sb.append("Account Name: ............ ").append(player.getAccountName()).append("\r\n");
				sb.append("IP: ...................... ").append(player.getIP()).append("\r\n");
				sb.append("Level: ................... ").append(player.getLevel()).append("\r\n");
				sb.append("Location: ................ ").append(player.getLoc()).append("\r\n");
				if(player.getClan() != null)
				{
					sb.append("Clan: .................... ").append(player.getClan().getName()).append("\r\n");
					if(player.getAlliance() != null)
						sb.append("Ally: .................... ").append(player.getAlliance().getAllyName()).append("\r\n");
				}
				sb.append("Offline: ................. ").append(player.isInOfflineMode()).append("\r\n");

				sb.append(player.toString()).append("\r\n");

				return sb.toString();
			}

		});
		_commands.add(new TelnetCommand("gmlist", "gms"){

			@Override
			public String getUsage()
			{
				return "gmlist";
			}

			@Override
			public String handle(String[] args)
			{
				List<Player> gms = GmListTable.getAllGMs();
				int count = gms.size();

				if(count == 0)
					return "GMs not found.\r\n";

				StringBuilder sb = new StringBuilder();
				for(int i = 0; i < count; i++)
					sb.append(gms.get(i)).append("\r\n");

				sb.append("Found: ").append(count).append(" GMs.").append("\r\n");

				return sb.toString();
			}

		});
	}

	@Override
	public Set<TelnetCommand> getCommands()
	{
		return _commands;
	}
}