package com.l2cccp.gameserver.network.l2.s2c;

import java.util.Map;

/**
 * @author VISTALL
 */
public class ExPVPMatchCCRecord extends L2GameServerPacket
{
	private final Map<String, Integer> _players;

	public ExPVPMatchCCRecord(Map<String, Integer> players)
	{
		_players = players;
	}

	@Override
	public void writeImpl()
	{
		writeEx(0x89);
		writeD(0x00); // Open/Dont Open
		writeD(_players.size());

		for(Map.Entry<String, Integer> p : _players.entrySet())
		{
			writeS(p.getKey());
			writeD(p.getValue().intValue());
		}
	}
}