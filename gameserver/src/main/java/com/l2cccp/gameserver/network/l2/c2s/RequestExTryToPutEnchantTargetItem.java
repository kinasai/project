package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.EnchantItemHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.items.PcInventory;
import com.l2cccp.gameserver.network.authcomm.AuthServerCommunication;
import com.l2cccp.gameserver.network.authcomm.gs2as.ChangeAccessLevel;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.EnchantResult;
import com.l2cccp.gameserver.network.l2.s2c.ExPutEnchantTargetItemResult;
import com.l2cccp.gameserver.network.l2.s2c.InventoryUpdate;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.templates.item.support.EnchantScroll;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Log;

public class RequestExTryToPutEnchantTargetItem extends L2GameClientPacket
{
	private int _objectId;

	@Override
	protected void readImpl()
	{
		_objectId = readD();
	}

	@Override
	protected void runImpl()
	{
		Player player = getClient().getActiveChar();
		if(player == null)
			return;

		if(player.isActionsDisabled() || player.isInStoreMode() || player.isInTrade())
		{
			player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			player.setEnchantScroll(null);
			return;
		}

		PcInventory inventory = player.getInventory();
		ItemInstance itemToEnchant = inventory.getItemByObjectId(_objectId);
		ItemInstance scroll = player.getEnchantScroll();

		if(itemToEnchant == null || scroll == null)
		{
			player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			player.setEnchantScroll(null);
			return;
		}

		Log.add(player.getName() + "|Trying to put enchant|" + itemToEnchant.getItemId() + "|+" + itemToEnchant.getEnchantLevel() + "|" + itemToEnchant.getObjectId(), "enchants");

		int scrollId = scroll.getItemId();
		int itemId = itemToEnchant.getItemId();

		EnchantScroll enchantScroll = EnchantItemHolder.getInstance().getEnchantScroll(scrollId);

		if(!itemToEnchant.canBeEnchanted(enchantScroll == null) || itemToEnchant.isStackable())
		{
			player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			player.sendPacket(SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
			player.setEnchantScroll(null);
			return;
		}

		if(itemToEnchant.getLocation() != ItemInstance.ItemLocation.INVENTORY && itemToEnchant.getLocation() != ItemInstance.ItemLocation.PAPERDOLL)
		{
			player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			player.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS);
			player.setEnchantScroll(null);
			return;
		}

		if(player.isInStoreMode())
		{
			player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			player.sendPacket(SystemMsg.YOU_CANNOT_ENCHANT_WHILE_OPERATING_A_PRIVATE_STORE_OR_PRIVATE_WORKSHOP);
			player.setEnchantScroll(null);
			return;
		}

		if((scroll = inventory.getItemByObjectId(scroll.getObjectId())) == null)
		{
			player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			player.setEnchantScroll(null);
			return;
		}

		if(enchantScroll == null)
		{
			doPutOld(player, itemToEnchant, scroll);
			//player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			//player.setEnchantScroll(null);
			return;
		}

		if(enchantScroll.getItems().size() > 0)
		{
			if(!enchantScroll.getItems().contains(itemId))
			{
				player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
				player.sendPacket(SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
				player.setEnchantScroll(null);
				return;
			}
		}
		else
		{
			if(!enchantScroll.getGrades().contains(itemToEnchant.getCrystalType()))
			{
				player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
				player.sendPacket(SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
				player.setEnchantScroll(null);
				return;
			}
		}

		if(enchantScroll.getMaxEnchant() != -1 && itemToEnchant.getEnchantLevel() >= enchantScroll.getMaxEnchant())
		{
			player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			player.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS);
			player.setEnchantScroll(null);
			return;
		}

		// Запрет на заточку чужих вещей, баг может вылезти на серверных лагах
		if(itemToEnchant.getOwnerId() != player.getObjectId())
		{
			player.sendPacket(ExPutEnchantTargetItemResult.FAIL);
			player.setEnchantScroll(null);
			return;
		}

		player.sendPacket(ExPutEnchantTargetItemResult.SUCCESS);
	}

	@Deprecated
	private static void doPutOld(Player activeChar, ItemInstance itemToEnchant, ItemInstance scroll)
	{
		int crystalId = ItemFunctions.getEnchantCrystalId(itemToEnchant, scroll, null);

		if(crystalId == -1)
		{
			activeChar.sendPacket(new ExPutEnchantTargetItemResult(0));
			activeChar.sendPacket(SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
			activeChar.setEnchantScroll(null);
			return;
		}

		int scrollId = scroll.getItemId();

		if(scrollId == 13540 && itemToEnchant.getItemId() != 13539)
		{
			activeChar.sendPacket(new ExPutEnchantTargetItemResult(0));
			activeChar.sendPacket(SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
			activeChar.setEnchantScroll(null);
			return;
		}

		// ольф 21580(21581/21582)
		if((scrollId == 21581 || scrollId == 21582) && itemToEnchant.getItemId() != 21580)
		{
			activeChar.sendPacket(new ExPutEnchantTargetItemResult(0));
			activeChar.sendPacket(SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
			activeChar.setEnchantScroll(null);
			return;
		}

		// TODO: [pchayka] временный хардкод до улучения системы описания свитков заточки
		if(ItemFunctions.isDestructionWpnEnchantScroll(scrollId) && itemToEnchant.getEnchantLevel() >= 15 || ItemFunctions.isDestructionArmEnchantScroll(scrollId) && itemToEnchant.getEnchantLevel() >= 6)
		{
			activeChar.sendPacket(new ExPutEnchantTargetItemResult(0));
			activeChar.sendPacket(SystemMsg.DOES_NOT_FIT_STRENGTHENING_CONDITIONS_OF_THE_SCROLL);
			activeChar.setEnchantScroll(null);
			return;
		}

		int itemType = itemToEnchant.getTemplate().getType2();
		boolean fail = false;
		switch(itemToEnchant.getItemId())
		{
			case 13539:
				if(itemToEnchant.getEnchantLevel() >= Config.ENCHANT_MAX_MASTER_YOGI_STAFF)
					fail = true;
				break;
			case 21580:
				if(itemToEnchant.getEnchantLevel() >= 9)
					fail = true;
				break;
			default:
				if((itemToEnchant.getEnchantLevel() >= Config.ENCHANT_MAX_WEAPON && itemType == ItemTemplate.TYPE2_WEAPON) || (itemToEnchant.getEnchantLevel() >= Config.ENCHANT_MAX_SHIELD_ARMOR && itemType == ItemTemplate.TYPE2_SHIELD_ARMOR) || (itemToEnchant.getEnchantLevel() >= Config.ENCHANT_MAX_ACCESSORY && itemType == ItemTemplate.TYPE2_ACCESSORY))
					fail = true;
				break;
		}

		if(fail)
		{
			clicker(activeChar, itemToEnchant, crystalId);
			activeChar.sendPacket(new ExPutEnchantTargetItemResult(0));
			activeChar.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS);
			activeChar.setEnchantScroll(null);
			return;
		}

		// Запрет на заточку чужих вещей, баг может вылезти на серверных лагах
		if(itemToEnchant.getOwnerId() != activeChar.getObjectId())
		{
			activeChar.sendPacket(new ExPutEnchantTargetItemResult(0));
			activeChar.setEnchantScroll(null);
			return;
		}

		activeChar.sendPacket(new ExPutEnchantTargetItemResult(1));
	}

	private static void clicker(Player player, ItemInstance item, int crystalId)
	{
		if(Config.ENCHANT_CHEATER_SYSTEM)
		{
			player.setMaybeClicker(player.getMaybeClicker() + 1);
			if(player.getMaybeClicker() >= Config.ENCHANT_CHEATER_MAX_COUNT)
			{
				punishment(player, item, crystalId);
				player.setMaybeClicker(0);
			}
			else
			{
				player.sendMessage("Внимание: Вы пытаетесь улучшить максимально заточенный предмет.");
				player.sendMessage("Осталось попыток: " + (Config.ENCHANT_CHEATER_MAX_COUNT - player.getMaybeClicker()) + ", после чего вы будете наказаны.");
			}
		}
	}

	private static void punishment(Player player, ItemInstance item, int crystal)
	{
		switch(Config.ENCHANT_CHEATER_PUNISHMENT)
		{
			case 0:
				if(crystal > 0 && item.getTemplate().getCrystalCount() > 0)
				{
					int count = (int) (item.getTemplate().getCrystalCount() * 0.87);
					if(item.getEnchantLevel() > 3)
						count += item.getTemplate().getCrystalCount() * 0.25 * (item.getEnchantLevel() - 3);
					if(count < 1)
						count = 1;

					player.sendPacket(new EnchantResult(1, crystal, count));
					ItemFunctions.addItem(player, crystal, count, true);
				}
				else
					player.sendPacket(EnchantResult.FAILED_NO_CRYSTALS);
				Log.add(player + " is cheater, try enchant by clicker. IP: " + player.getIP(), "tryEnchantByClicker", player);
				break;
			case 1:
				item.setEnchantLevel(0);
				item.setJdbcState(JdbcEntityState.UPDATED);
				item.update();
				player.sendPacket(new InventoryUpdate().addModifiedItem(item));
				Log.add(player + " is cheater. Item " + item.getName() + " will be unenchant.", "tryEnchantByClicker", player);
				break;
			case 2:
				player.kick();
				Log.add(player + " is cheater and will be kick from server.", "tryEnchantByClicker", player);
				break;
			case 3:
				AuthServerCommunication.getInstance().sendPacket(new ChangeAccessLevel(player.getAccountName(), -100, 0));
				player.kick();
				Log.add(player + " is cheater and well be ban and kick from server.", "tryEnchantByClicker", player);
				break;
		}
	}
}
