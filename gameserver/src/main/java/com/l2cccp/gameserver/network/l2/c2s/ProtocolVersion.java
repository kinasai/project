package com.l2cccp.gameserver.network.l2.c2s;


import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.network.l2.s2c.KeyPacket;
import com.l2cccp.gameserver.network.l2.s2c.SendStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.strixplatform.StrixPlatform;
import org.strixplatform.logging.Log;
import org.strixplatform.managers.ClientGameSessionManager;
import org.strixplatform.managers.ClientProtocolDataManager;
import org.strixplatform.utils.StrixClientData;

/**
 * packet type id 0x0E
 * format:	cdbd
 */
public class ProtocolVersion extends L2GameClientPacket
{
	private static final Logger _log = LoggerFactory.getLogger(ProtocolVersion.class);

	private int _version;
	//TODO[K] - Guard section start
	private byte[] _data;
	private int dataChecksum;
	//TODO[K] - Guard section end

	@Override
	protected void readImpl()
	{
		_version = readD();
		//TODO[K] - Guard section start
		if(StrixPlatform.getInstance().isPlatformEnabled())
		{
			try
			{
				if(_buf.remaining() >= StrixPlatform.getInstance().getProtocolVersionDataSize())
				{
					_data = new byte[StrixPlatform.getInstance().getClientDataSize()];
					readB(_data);
					dataChecksum = readD();
				}
			}
			catch(final Exception e)
			{
				Log.error("Client [IP=" + getClient().getIpAddr() + "] used unprotected client. Disconnect...");
				getClient().close(new KeyPacket(null));
				return;
			}
		}
		//TODO[K] - Guard section end
	}

	@Override
	protected void runImpl()
	{
		if(_version == -2)
		{
			_client.closeNow(false);
			return;
		}
		else if(_version == -3)
		{
			_log.info("Status request from IP : " + getClient().getIpAddr());
			getClient().close(new SendStatus());
			return;
		}
		else if(_version < Config.MIN_PROTOCOL_REVISION || _version > Config.MAX_PROTOCOL_REVISION)
		{
			_log.warn("Unknown protocol revision : " + _version + ", client : " + _client);
			getClient().close(new KeyPacket(null));
			return;
		}

		//TODO[K] - Strix section start
		if(!StrixPlatform.getInstance().isPlatformEnabled())
		{
			getClient().setRevision(_version);
			sendPacket(new KeyPacket(getClient().enableCrypt()));
			return;
		}
		else
		{
			if(_data == null)
			{
				Log.error("Client [IP=" + getClient().getIpAddr() + "] used unprotected client. Disconnect...");
				getClient().close(new KeyPacket(null));
				return;
			}
			else
			{
				final StrixClientData clientData = ClientProtocolDataManager.getInstance().getDecodedData(_data, dataChecksum);
				if(clientData != null)
				{
					if(!ClientGameSessionManager.getInstance().checkServerResponse(clientData))
					{
						getClient().close(new KeyPacket(null, clientData));
						return;
					}
					getClient().setStrixClientData(clientData);
					getClient().setRevision(_version);
					sendPacket(new KeyPacket(_client.enableCrypt()));
					return;
				}
				Log.error("Decode client data failed. See Strix-Platform log file. Disconected client " + getClient().getIpAddr());
				getClient().close(new KeyPacket(null));
			}
		}
		//TODO[K] - Strix section end
	}
}