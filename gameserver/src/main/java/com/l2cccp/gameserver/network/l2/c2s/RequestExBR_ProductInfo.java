package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.s2c.ExBR_ProductInfo;

public class RequestExBR_ProductInfo extends L2GameClientPacket
{
	private int _productId;

	@Override
	protected void readImpl()
	{
	 	_productId = readD();
	}

	@Override
	protected void runImpl()
	{
		Player activeChar = getClient().getActiveChar();

		if(activeChar == null)
			return;

		activeChar.sendPacket(new ExBR_ProductInfo(_productId));
	}
}