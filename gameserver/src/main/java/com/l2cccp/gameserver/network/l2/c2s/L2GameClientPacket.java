package com.l2cccp.gameserver.network.l2.c2s;

import java.nio.BufferUnderflowException;
import java.util.Arrays;
import java.util.List;

import com.l2cccp.commons.net.nio.impl.ReceivablePacket;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.s2c.L2GameServerPacket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Packets received by the game server from clients
 */
public abstract class L2GameClientPacket extends ReceivablePacket<GameClient>
{
	private static final Logger _log = LoggerFactory.getLogger(L2GameClientPacket.class);

	@Override
	public final boolean read()
	{
		try
		{
			readImpl();
			return true;
		}
		catch(BufferUnderflowException e)
		{
			_log.error("Client: " + _client + " - Failed reading: " + getType() + ". Buffer underflow!");
		}
		catch(Exception e)
		{
			_log.error("Client: " + _client + " - Failed reading: " + getType(), e);
		}

		_client.onPacketReadFail();

		return false;
	}

	protected abstract void readImpl() throws Exception;

	@Override
	public final void run()
	{
		GameClient client = getClient();
		try
		{
			runImpl();
		}
		catch(Exception e)
		{
			_log.error("Client: " + client + " - Failed running: " + getType() + ".", e);
		}
	}

	protected abstract void runImpl() throws Exception;

	protected String readS(int len)
	{
		String ret = readS();
		return ret.length() > len ? ret.substring(0, len) : ret;
	}

	protected void sendPacket(L2GameServerPacket packet)
	{
		getClient().sendPacket(packet);

		if(Config.DEV_DEBUG_PACKET && debug())
		{
			_log.info("########################L2GameClientPacket########################");
			_log.info("## From Account -> " + getClient().getLogin());
			_log.info("## -> Server packet: " + packet.getType() + ", Path -> " + packet.getClass().getCanonicalName());
			_log.info("## -> Client packet: " + this.getType() + ", Path -> " + this.getClass().getCanonicalName());
		}
	}

	protected void sendPacket(L2GameServerPacket... packets)
	{
		getClient().sendPacket(packets);

		if(Config.DEV_DEBUG_PACKET && debug())
		{
			_log.info("########################L2GameClientPacket########################");
			_log.info("## Packet count: " + packets.length);
			for(L2GameServerPacket packet : packets)
			{
				_log.info("##################################################################");
				_log.info("## From Account -> " + getClient().getLogin());
				_log.info("## -> Server packet: " + packet.getType() + ", Path -> " + packet.getClass().getCanonicalName());
				_log.info("## -> Client packet: " + this.getType() + ", Path -> " + this.getClass().getCanonicalName());
			}
		}
	}

	protected void sendPackets(List<L2GameServerPacket> packets)
	{
		getClient().sendPackets(packets);

		if(Config.DEV_DEBUG_PACKET && debug())
		{
			_log.info("########################L2GameClientPacket########################");
			_log.info("## Packet count: " + packets.size());
			for(L2GameServerPacket packet : packets)
			{
				_log.info("##################################################################");
				_log.info("## From Account -> " + getClient().getLogin());
				_log.info("## -> Server packet: " + packet.getType() + ", Path -> " + packet.getClass().getCanonicalName());
				_log.info("## -> Client packet: " + this.getType() + ", Path -> " + this.getClass().getCanonicalName());
			}
		}
	}

	private boolean debug()
	{
		if(!Config.DEV_DEBUG_PACKET_ALL)
		{
			final GameClient client = getClient();
			if(client != null && client.getActiveChar() != null)
			{
				List<String> list = Arrays.asList(Config.DEV_DEBUG_PACKET_TARGETS);
				return list.contains(client.getLogin());
			}
		}

		return true;
	}

	public String getType()
	{
		return "[C] " + getClass().getSimpleName();
	}
}