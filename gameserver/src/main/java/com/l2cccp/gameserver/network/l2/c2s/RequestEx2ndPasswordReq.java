package com.l2cccp.gameserver.network.l2.c2s;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.l2cccp.commons.lang.ArrayUtils;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.CharacterAccessDAO;
import com.l2cccp.gameserver.model.CharSelectInfo;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.s2c.Ex2ndPasswordAck;

/**
 * @author VISTALL
 */
public class RequestEx2ndPasswordReq extends L2GameClientPacket
{
	private int _type;
	private String _password, _newPassword;
	private static final Pattern digits = Pattern.compile("[0-9]{6,8}");

	@Override
	protected void readImpl()
	{
		_type = readC();
		_password = readS();
		if(_type == 2)
			_newPassword = readS();
	}

	@Override
	protected void runImpl()
	{
		GameClient client = getClient();

		if(client == null || !Config.SECOND_AUTH_ENABLED)
			return;

		CharSelectInfo csi = ArrayUtils.valid(client.getCharacters(), client.getSelectedIndex());
		if(csi == null)
			return;

		Matcher matcher = digits.matcher(_password);
		if(!matcher.matches())
		{
			sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.WRONG_PATTERN));
			return;
		}

		switch(_type)
		{
			case 0:
				if(csi.getPassword() != null)
					return;

				csi.setPassword(_password);
				sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.SUCCESS));

				CharacterAccessDAO.getInstance().update(csi.getObjectId(), _password);
				break;
			case 2:
				if(csi.getPassword() == null)
					return;

				if(!csi.getPassword().equals(_password))
				{
					sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.WRONG_PATTERN));
					return;
				}

				csi.setPassword(_newPassword);
				sendPacket(new Ex2ndPasswordAck(Ex2ndPasswordAck.SUCCESS));

				CharacterAccessDAO.getInstance().update(csi.getObjectId(), _newPassword);
				break;
		}
	}
}
