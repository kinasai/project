package com.l2cccp.gameserver.network.l2.c2s;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.autoAtt.AutoAtt;
import com.l2cccp.gameserver.model.base.Element;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.items.PcInventory;
import com.l2cccp.gameserver.network.authcomm.AuthServerCommunication;
import com.l2cccp.gameserver.network.authcomm.gs2as.ChangeAccessLevel;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ActionFail;
import com.l2cccp.gameserver.network.l2.s2c.EnchantResult;
import com.l2cccp.gameserver.network.l2.s2c.ExAttributeEnchantResult;
import com.l2cccp.gameserver.network.l2.s2c.InventoryUpdate;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Log;

/**
 * @author SYS
 * Format: d
 */
public class RequestEnchantItemAttribute extends L2GameClientPacket {
	private int _objectId;
	@Override
	protected void readImpl() {
		_objectId = readD();
	}

	@Override
	protected void runImpl() {
		Player activeChar = getClient().getActiveChar();
		if(activeChar == null) {
			return;
		}
		activeChar.updateActive();
		if(getClient().getShareBlock()) {
			activeChar.sendMessage(new CustomMessage("account.share.action.disable"));
			activeChar.sendActionFailed();
			return;
		}
		if(activeChar.isAttributeInsertingNow()) {
			return;
		}
		startEnchant(activeChar, _objectId);
	}

	private static void clicker(Player player, ItemInstance item, Element element) {
		if(Config.ENCHANT_CHEATER_SYSTEM) {
			player.setMaybeClicker(player.getMaybeClicker() + 1);
			if(player.getMaybeClicker() >= Config.ENCHANT_CHEATER_MAX_COUNT) {
				punishment(player, item, element);
				player.setMaybeClicker(0);
			} else {
				player.sendMessage("Внимание: Вы пытаетесь улучшить максимально заточенный предмет.");
				player.sendMessage("Осталось попыток: " + (Config.ENCHANT_CHEATER_MAX_COUNT - player.getMaybeClicker()) + ", после чего вы будете наказаны.");
			}
		}
	}

	private static void punishment(Player player, ItemInstance item, Element element) {
		switch(Config.ENCHANT_CHEATER_PUNISHMENT) {
			case 0:
				int crystal = item.getTemplate().getCrystalType().cry;
				if(crystal > 0 && item.getTemplate().getCrystalCount() > 0) {
					int count = (int) (item.getTemplate().getCrystalCount() * 0.87);
					if(item.getEnchantLevel() > 3) {
						count += item.getTemplate().getCrystalCount() * 0.25 * (item.getEnchantLevel() - 3);
					}
					if(count < 1) {
						count = 1;
					}
					player.sendPacket(new EnchantResult(1, crystal, count));
					ItemFunctions.addItem(player, crystal, count, true);
				} else {
					player.sendPacket(EnchantResult.FAILED_NO_CRYSTALS);
				}
				Log.add(player + " is cheater try enchant by clicker. IP: " + player.getIP() + " Item: " + item.getName() + ".", "tryEnchantByClicker", player);
				break;
			case 1:
				item.setEnchantLevel(0);
				item.setJdbcState(JdbcEntityState.UPDATED);
				item.update();
				player.sendPacket(new InventoryUpdate().addModifiedItem(item));
				Log.add(player + " is cheater. Item " + item.getName() + " will be unenchant.", "tryEnchantByClicker", player);
				break;
			case 2:
				player.kick();
				Log.add(player + " is cheater and will be kick from server.", "tryEnchantByClicker", player);
				break;
			case 3:
				AuthServerCommunication.getInstance().sendPacket(new ChangeAccessLevel(player.getAccountName(), -100, 0));
				player.kick();
				Log.add(player + " is cheater and well be ban and kick from server.", "tryEnchantByClicker", player);
				break;
		}
	}
	private void startEnchant(final Player activeChar, final int objectId) {
		int chance;
		long attNum = 0;
		boolean enableAutoAtt = false;
		AutoAtt att;
		if(!firstCheck(activeChar, objectId)) {
			return;
		}
		enableAutoAtt = activeChar.getVarB("enableAutoAtt");
		att = secondCheck(activeChar, objectId, true, enableAutoAtt);
		if(att.itemToEnchant.getTemplate().isArmor()) {
			chance = att.isCrystal ? Config.ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE_ARMOR : Config.ENCHANT_ATTRIBUTE_STONE_CHANCE_ARMOR;
		} else if(att.itemToEnchant.getTemplate().isMagicWeapon()) {
			chance = att.isCrystal ? Config.ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE_MAGE_WEAPON : Config.ENCHANT_ATTRIBUTE_STONE_CHANCE_MAGE_WEAPON;
		} else {
			chance = att.isCrystal ? Config.ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE_FIGHTER_WEAPON : Config.ENCHANT_ATTRIBUTE_STONE_CHANCE_FIGHTER_WEAPON;
		}
		if(Config.ALT_DEBUG_ENCHANT_CHANCE_ENABLED && activeChar.isDebug())
			activeChar.sendMessage("Enchant chance : " + chance);
		if(!att.possible) {
			return;
		}
		attNum = att.stoneCount;
		activeChar.setIsAttributeInsertingNow(true);
		for(long i = 1; i <= attNum; i++) {
			if(!firstCheck(activeChar, objectId)) {
				break;
			}
			att = secondCheck(activeChar, objectId, false, true);
			if(!att.possible) {
				break;
			}
			if(!activeChar.getInventory().destroyItem(att.stone, 1)) {
				activeChar.sendActionFailed();
				break;
			}
			if(Rnd.chance(chance)) {
				if(att.itemToEnchant.getEnchantLevel() == 0) {
					SystemMessage sm = new SystemMessage(SystemMsg.S2_ELEMENTAL_POWER_HAS_BEEN_ADDED_SUCCESSFULLY_TO_S1);
					sm.addItemName(att.itemToEnchant.getItemId());
					sm.addItemName(att.stone.getItemId());
					activeChar.sendPacket(sm);
				} else {
					SystemMessage sm = new SystemMessage(SystemMsg.S3_ELEMENTAL_POWER_HAS_BEEN_ADDED_SUCCESSFULLY_TO_S1_S2);
					sm.addNumber(att.itemToEnchant.getEnchantLevel());
					sm.addItemName(att.itemToEnchant.getItemId());
					sm.addItemName(att.stone.getItemId());
					activeChar.sendPacket(sm);
				}	
				int value = Config.ENCHANT_ATTRIBUTE_VALUE[att.itemToEnchant.isWeapon() ? 0 : 1];	
				final int current = att.itemToEnchant.getAttributeElementValue(att.element, false);
				if(current == 0 && att.itemToEnchant.isWeapon() && value == 5) {
					value = 20;
				}	
				boolean equipped = false;
				if(equipped = att.itemToEnchant.isEquipped()) {
					activeChar.getInventory().isRefresh = true;
					activeChar.getInventory().unEquipItem(att.itemToEnchant);
				}	
				final int set = Math.min(att.maxValue, current + value);
				att.itemToEnchant.setAttributeElement(att.element, set);
				att.itemToEnchant.setJdbcState(JdbcEntityState.UPDATED);
				att.itemToEnchant.update();	
				if(equipped) {
					activeChar.getInventory().equipItem(att.itemToEnchant);
					activeChar.getInventory().isRefresh = false;
				}	
				activeChar.sendPacket(new InventoryUpdate().addModifiedItem(att.itemToEnchant));
				activeChar.sendPacket(new ExAttributeEnchantResult(value));
			} else {
				activeChar.sendPacket(SystemMsg.YOU_HAVE_FAILED_TO_ADD_ELEMENTAL_POWER);
			}
			try { 
				Thread.sleep(500);
			} catch(InterruptedException ie) {				
			}
		}
		activeChar.setIsAttributeInsertingNow(false);
		activeChar.setEnchantScroll(null);
		activeChar.updateStats();
	}
		
	private boolean firstCheck(final Player activeChar, final int objectId) {
		if(activeChar == null) {
			return false;
		}
		if(objectId == -1) {
			activeChar.setEnchantScroll(null);
			activeChar.sendPacket(SystemMsg.ATTRIBUTE_ITEM_USAGE_HAS_BEEN_CANCELLED);
			return false;
		}
		if(activeChar.isActionsDisabled()) {
			activeChar.sendActionFailed();
			return false;
		}
		if(activeChar.isInStoreMode()) {
			activeChar.sendPacket(SystemMsg.YOU_CANNOT_ADD_ELEMENTAL_POWER_WHILE_OPERATING_A_PRIVATE_STORE_OR_PRIVATE_WORKSHOP, ActionFail.STATIC);
			return false;
		}
		if(activeChar.isInTrade()) {
			activeChar.sendActionFailed();
			return false;
		}
		return true;
	}
	
	private AutoAtt secondCheck(final Player activeChar, final int objectId, final boolean first, final boolean enableAutoAtt) {
		AutoAtt att = new AutoAtt();
		att.itemToEnchant = null;
		att.stone = null;
		att.maxValue = 0;
		att.element = null;
		att.stoneCount = 0;
		att.isCrystal = false;
		att.possible = false;
		if(activeChar == null) {
			return att;
		}
		PcInventory inventory = activeChar.getInventory();
		ItemInstance itemToEnchant = inventory.getItemByObjectId(_objectId);
		ItemInstance stone = activeChar.getEnchantScroll();
		activeChar.setEnchantScroll(null);
		if(itemToEnchant == null || stone == null) {
			activeChar.sendActionFailed();
			return att;
		}
		if(itemToEnchant.isAccessory()) {
			Log.debug("CHEATER -> (IP: " + activeChar.getIP() + ", Name: " + activeChar.getName() + "): Try put attribute to item: " + itemToEnchant.getName());
			activeChar.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS, ActionFail.STATIC);
			return att;
		}
		if(!itemToEnchant.isWeapon() && (itemToEnchant.getBodyPart() == ItemTemplate.SLOT_L_HAND && itemToEnchant.getBodyPart() != ItemTemplate.SLOT_LR_HAND)) {
			Log.debug("CHEATER -> (IP: " + activeChar.getIP() + ", Name: " + activeChar.getName() + "): Try put attribute to item: " + itemToEnchant.getName());
			activeChar.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS, ActionFail.STATIC);
			return att;
		}
		ItemTemplate item = itemToEnchant.getTemplate();
		if(!itemToEnchant.canBeEnchanted(true) || item.getCrystalType().cry < ItemTemplate.CRYSTAL_S) {
			activeChar.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS, ActionFail.STATIC);
			return att;
		}
		if(itemToEnchant.getLocation() != ItemInstance.ItemLocation.INVENTORY && itemToEnchant.getLocation() != ItemInstance.ItemLocation.PAPERDOLL) {
			activeChar.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS, ActionFail.STATIC);
			return att;
		}
		if(itemToEnchant.isStackable() || (stone = inventory.getItemByObjectId(stone.getObjectId())) == null) {
			activeChar.sendActionFailed();
			return att;
		}
		Element element = ItemFunctions.getEnchantAttributeStoneElement(stone.getItemId(), itemToEnchant.isArmor());
		if(itemToEnchant.isArmor()) {
			if(itemToEnchant.getAttributeElementValue(Element.getReverseElement(element), false) != 0) {
				activeChar.sendPacket(SystemMsg.ANOTHER_ELEMENTAL_POWER_HAS_ALREADY_BEEN_ADDED_THIS_ELEMENTAL_POWER_CANNOT_BE_ADDED, ActionFail.STATIC);
				return att;
			}
		} else if(itemToEnchant.isWeapon()) {
			if(itemToEnchant.getAttributeElement() != Element.NONE && itemToEnchant.getAttributeElement() != element) {
				activeChar.sendPacket(SystemMsg.ANOTHER_ELEMENTAL_POWER_HAS_ALREADY_BEEN_ADDED_THIS_ELEMENTAL_POWER_CANNOT_BE_ADDED, ActionFail.STATIC);
				return att;
			}
		} else {
			activeChar.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS, ActionFail.STATIC);
			return att;
		}
		if(item.isUnderwear() || item.isCloak() || item.isBracelet() || item.isBelt() || !item.isAttributable()) {
			activeChar.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS, ActionFail.STATIC);
			return att;
		}
		int maxValue = Config.ENCHANT_ATTRIBUTE_STONE[itemToEnchant.isWeapon() ? 0 : 1];;
		final boolean isCrystal = stone.getTemplate().isAttributeCrystal();
		if(isCrystal) {
			maxValue += Config.ENCHANT_ATTRIBUTE_CRYSTAL[itemToEnchant.isWeapon() ? 0 : 1];;
		}
		if(itemToEnchant.getAttributeElementValue(element, false) == maxValue) {
			if(!enableAutoAtt) {
				clicker(activeChar, itemToEnchant, element);
				activeChar.sendPacket(SystemMsg.ATTRIBUTE_ITEM_USAGE_HAS_BEEN_CANCELLED, ActionFail.STATIC);
			}
			return att;
		}
		// Запрет на заточку чужих вещей, баг может вылезти на серверных лагах
		if(itemToEnchant.getOwnerId() != activeChar.getObjectId()) {
			activeChar.sendPacket(SystemMsg.INAPPROPRIATE_ENCHANT_CONDITIONS, ActionFail.STATIC);
			return att;
		}
		att.itemToEnchant = itemToEnchant;
		att.stone = stone;
		if(enableAutoAtt) {
			if(first) {
				att.stoneCount = stone.getCount();
			}
		} else {
			att.stoneCount = 1;
		}
		att.maxValue = maxValue;
		att.element = element;
		att.isCrystal = isCrystal;
		att.possible = true;
		activeChar.setEnchantScroll(stone);
		return att;
	}
}