package com.l2cccp.gameserver.network.authcomm.gs2as;

import com.l2cccp.gameserver.network.authcomm.SendablePacket;

public class PingResponse extends SendablePacket
{
	protected void writeImpl()
	{
		writeC(0xff);
		writeQ(System.currentTimeMillis());
	}
}