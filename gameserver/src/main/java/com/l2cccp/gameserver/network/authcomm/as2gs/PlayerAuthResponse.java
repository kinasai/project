package com.l2cccp.gameserver.network.authcomm.as2gs;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.authcomm.AuthServerCommunication;
import com.l2cccp.gameserver.network.authcomm.ReceivablePacket;
import com.l2cccp.gameserver.network.authcomm.SessionKey;
import com.l2cccp.gameserver.network.authcomm.gs2as.PlayerInGame;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.CharacterSelectionInfo;
import com.l2cccp.gameserver.network.l2.s2c.LoginFail;
import com.l2cccp.gameserver.network.l2.s2c.ServerClose;

public class PlayerAuthResponse extends ReceivablePacket
{
	private String account;
	private boolean authed;
	private int playOkId1;
	private int playOkId2;
	private int loginOkId1;
	private int loginOkId2;
	private String hwid;
	private String ip;
	private boolean block;
	private boolean share;

	@Override
	public void readImpl()
	{
		account = readS();
		authed = readC() == 1;
		if(authed)
		{
			playOkId1 = readD();
			playOkId2 = readD();
			loginOkId1 = readD();
			loginOkId2 = readD();
		}
		hwid = readS();
		ip = readS();
		block = readD() == 1 ? true : false;
		share = readD() == 1 ? true : false;
	}

	@Override
	protected void runImpl()
	{
		SessionKey skey = new SessionKey(loginOkId1, loginOkId2, playOkId1, playOkId2);
		GameClient client = AuthServerCommunication.getInstance().removeWaitingClient(account);
		if(client == null)
			return;

		if(authed && client.getSessionKey().equals(skey))
		{
			client.setAuthed(true);
			client.setState(GameClient.GameClientState.AUTHED);
			client.startPingTask();

			GameClient oldClient = AuthServerCommunication.getInstance().addAuthedClient(client);
			if(oldClient != null)
			{
				oldClient.setAuthed(false);
				Player activeChar = oldClient.getActiveChar();
				if(activeChar != null)
				{
					//FIXME [G1ta0] сообщение чаще всего не показывается, т.к. при закрытии соединения очередь на отправку очищается
					activeChar.sendPacket(SystemMsg.ANOTHER_PERSON_HAS_LOGGED_IN_WITH_THE_SAME_ACCOUNT);

					oldClient.setActiveChar(null);
					activeChar.setNetConnection(null);
					activeChar.scheduleDelete();
				}
				oldClient.close(ServerClose.STATIC);
			}

			sendPacket(new PlayerInGame(client.getLogin()));

			CharacterSelectionInfo csi = new CharacterSelectionInfo(client.getLogin(), client.getSessionKey().playOkID1);
			client.setCharSelection(csi.getCharInfo());
			client.sendPacket(csi);
			client.setAllowedShare(share);
			client.setShareBlock(block);
			client.setLocked(hwid, ip);
			client.checkHwid();
		}
		else
		{
			client.close(new LoginFail(LoginFail.ACCESS_FAILED_TRY_LATER));
		}
	}
}