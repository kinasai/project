package com.l2cccp.gameserver.network.l2.s2c;

import org.strixplatform.StrixPlatform;
import org.strixplatform.utils.StrixClientData;

public class KeyPacket extends L2GameServerPacket
{
	private byte[] _key;
	//TODO[K] - Guard section start
	private final StrixClientData clientData;
	// TODO[K] - Strix section end

	public KeyPacket(byte[] key)
	{
		_key = key;
		//TODO[K] - Guard section start
		this.clientData = null;
		// TODO[K] - Strix section end
	}
	//TODO[K] - Guard section start
	public KeyPacket(final byte[] key, final StrixClientData clientData)
	{
		_key = key;
		this.clientData = clientData;
	}
	// TODO[K] - Strix section end

	@Override
	public void writeImpl()
	{
		writeC(0x2e);
		if(_key == null || _key.length == 0)
		{
			//TODO[K] - Guard section start
			if(StrixPlatform.getInstance().isBackNotificationEnabled() && clientData != null)
			{
				writeC(clientData.getServerResponse().ordinal() + 1);
			}
			else
			{
				writeC(0x00);
			}
			// TODO[K] - Strix section end
			return;
		}
		writeC(0x01);
		writeB(_key);
		writeD(0x01);
		writeD(0x00);
		writeC(0x00);
		writeD(0x00); // Seed (obfuscation key)
	}
}