package com.l2cccp.gameserver.network.l2.c2s;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.gameserver.data.xml.holder.CursedWeaponsHolder;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.CursedWeapon;
import com.l2cccp.gameserver.network.l2.s2c.ExCursedWeaponLocation;
import com.l2cccp.gameserver.network.l2.s2c.ExCursedWeaponLocation.CursedWeaponInfo;
import com.l2cccp.gameserver.utils.Location;

public class RequestCursedWeaponLocation extends L2GameClientPacket
{
	@Override
	protected void readImpl()
	{}

	@Override
	protected void runImpl()
	{
		Creature activeChar = getClient().getActiveChar();
		if(activeChar == null)
			return;

		List<CursedWeaponInfo> list = new ArrayList<CursedWeaponInfo>();
		for(CursedWeapon cw : CursedWeaponsHolder.getInstance().getCursed())
		{
			Location pos = cw.getWorldPosition();
			if(pos != null)
				list.add(new CursedWeaponInfo(pos, cw.getItemId(), cw.isActivated() ? 1 : 0));
		}

		activeChar.sendPacket(new ExCursedWeaponLocation(list));
	}
}