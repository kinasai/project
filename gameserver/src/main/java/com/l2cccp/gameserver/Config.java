package com.l2cccp.gameserver;

import com.l2cccp.commons.configuration.ExProperties;
import com.l2cccp.commons.net.nio.impl.SelectorConfig;
import com.l2cccp.gameserver.config.parser.ChatFilterParser;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.ExperienceHolder;
import com.l2cccp.gameserver.model.base.PlayerAccess;
import com.l2cccp.gameserver.network.authcomm.ServerType;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.utils.Util;
import com.l2cccp.gameserver.utils.velocity.VelocityVariable;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

public class Config
{
	private static final Logger _log = LoggerFactory.getLogger(Config.class);

	public static final int NCPUS = Runtime.getRuntime().availableProcessors();

	/** Configuration files */
	public static final String OTHER_CONFIG_FILE = "config/other.properties";
	public static final String RESIDENCE_CONFIG_FILE = "config/residence.properties";
	public static final String CLANHALL_CONFIG_FILE = "config/clanhall.properties";
	public static final String SPOIL_CONFIG_FILE = "config/spoil.properties";
	public static final String ALT_SETTINGS_FILE = "config/altsettings.properties";
	public static final String FORMULAS_CONFIGURATION_FILE = "config/formulas.properties";
	public static final String SERVICE_BONUS_CONFIG_FILE = "config/npc/bonus.properties";
	public static final String PVP_CONFIG_FILE = "config/pvp.properties";
	public static final String TELNET_CONFIGURATION_FILE = "config/telnet.properties";
	public static final String CONFIGURATION_FILE = "config/server.properties";
	public static final String AI_CONFIG_FILE = "config/ai.properties";
	public static final String GEODATA_CONFIG_FILE = "config/geodata.properties";
	public static final String EVENTS_CONFIG_FILE = "config/events.properties";
	public static final String SERVICES_FILE = "config/services.properties";
	public static final String OLYMPIAD = "config/olympiad.properties";
	public static final String DEVELOP_FILE = "config/develop.properties";
	public static final String EXT_FILE = "config/ext.properties";
	public static final String LOG = "config/log.properties";

	public static final String L2TOP_FILE = "config/top/l2top.properties";
	public static final String MMOTOP_FILE = "config/top/mmotop.properties";
	public static final String MMTOP_FILE = "config/top/mmtop.properties";

	public static final String CLAN = "config/alternative/clan.properties";
	public static final String AFK = "config/alternative/afk.properties";
	public static final String PLAYER_CREATE = "config/alternative/player_create.properties";
	public static final String ENTER_WORLD = "config/alternative/enter_world.properties";
	public static final String ENCHANT = "config/alternative/enchant.properties";
	public static final String RATE = "config/alternative/rate.properties";
	public static final String RAID = "config/alternative/raidbosses.properties";
	public static final String PREMIUM = "config/alternative/premium.properties";
	public static final String PK_SYSTEM = "config/alternative/pk_system.properties";
	public static final String PVP_SYSTEM = "config/alternative/pvp_system.properties";
	public static final String VITALITY = "config/alternative/vitality.properties";

	public static final String OLYMPIAD_DATA_FILE = "config/olympiad.properties";

	public static final String CHATFILTERS_CONFIG_FILE = "config/chatfilters.xml";

	public static final String GM_PERSONAL_ACCESS_FILE = "config/GMAccess.xml";
	public static final String GM_ACCESS_FILES_DIR = "config/GMAccess.d/";

	public static final String COMMAND_OFFLINE_BUFFER = "config/command/offbuff.properties";
	public static final String COMMAND_SHOW_EQUIP = "config/command/showequip.properties";
	public static final String COMMAND_SUMMON_CLAN = "config/command/clan_summon.properties";
	public static final String COMMAND_BOT_REPORT = "config/command/botreport.properties";

	public static final String COMMUNITY_CAREER = "config/bbs/career.properties";
	public static final String COMMUNITY_BOARD = "config/bbs/board.properties";
	public static final String COMMUNITY_BUFFER = "config/bbs/buffer.properties";
	public static final String COMMUNITY_TELEPORT = "config/bbs/teleport.properties";
	public static final String COMMUNITY_STATISTIC = "config/bbs/statistics.properties";
	public static final String COMMUNITY_FORGE = "config/bbs/forge.properties";
	public static final String COMMUNITY_CABINET = "config/bbs/cabinet.properties";
	public static final String COMMUNITY_RECRUITMENT = "config/bbs/recruitment.properties";
	public static final String COMMUNITY_DROPBASE = "config/bbs/drop.properties";
	public static final String COMMUNITY_BOSSES = "config/bbs/bosses.properties";

	public static boolean BONUS_SERVICE_ENABLE;
	public static long[] BONUS_SERVICE_CLAN_REWARD;
	public static int BONUS_SERVICE_CLAN_MIN_LEVEL;
	public static String[] BONUS_SERVICE_CLAN_LIST;
	public static long[] BONUS_SERVICE_PARTY_REWARD;
	public static int BONUS_SERVICE_PARTY_MIN_LEVEL;
	public static String[] BONUS_SERVICE_PARTY_LIST;

	public static int HTM_CACHE_MODE;
	public static boolean ITEM_CACHE;

	public static int WEB_SERVER_DELAY;
	public static String WEB_SERVER_ROOT;

	/** GameServer ports */
	public static int[] PORTS_GAME;
	public static String GAMESERVER_HOSTNAME;

	public static String DATABASE_DRIVER;
	public static int DATABASE_MAX_CONNECTIONS;
	public static int DATABASE_MAX_IDLE_TIMEOUT;
	public static int DATABASE_IDLE_TEST_PERIOD;
	public static String DATABASE_URL;
	public static String DATABASE_LOGIN_URL;
	public static String DATABASE_LOGIN;
	public static String DATABASE_PASSWORD;

	// Database additional options
	public static boolean AUTOSAVE;

	public static long USER_INFO_INTERVAL;
	public static long BROADCAST_CHAR_INFO_INTERVAL;

	public static int EFFECT_TASK_MANAGER_COUNT;

	public static int MAXIMUM_ONLINE_USERS;

	public static boolean DONTLOADSPAWN;
	public static boolean DONTLOADQUEST;
	public static int MAX_REFLECTIONS_COUNT;

	public static int SHIFT_BY;
	public static int SHIFT_BY_Z;
	public static int MAP_MIN_Z;
	public static int MAP_MAX_Z;

	/** ChatBan */
	public static int CHAT_MESSAGE_MAX_LEN;

	public static ChatType[] BAN_CHANNEL_LIST;

	public static boolean BANCHAT_ANNOUNCE;
	public static boolean BANCHAT_ANNOUNCE_FOR_ALL_WORLD;
	public static boolean BANCHAT_ANNOUNCE_NICK;

	public static boolean SAVING_SPS;

	public static int ALT_ADD_RECIPES;
	public static int ALT_MAX_ALLY_SIZE;

	public static int ALT_PARTY_DISTRIBUTION_RANGE;
	public static double[] ALT_PARTY_BONUS;
	public static double ALT_ABSORB_DAMAGE_MODIFIER;
	public static double DEBUFF_MIN_CAP;

	public static double ALT_POLE_DAMAGE_MODIFIER;

	public static boolean SKILLS_DISPEL_SONGS;
	public static boolean SKILLS_DISPEL_TRANSFORMATION;
	public static boolean SKILLS_DISPEL_HOURGLASS;
	public static int[] FULL_IMMUNITY_DEBUFF_NPC;
	public static double[] FORMULA_SKILLS_MONSTER_MODIFIER;
	public static boolean NEW_ATT_FORMULA;
	public static int[] STEAL_FORMULA_DATA;
	public static int[] CANCEL_COUNT_SIZE;
	public static double CANCEL_COUNT_LIMIT;
	public static double[] SOUL_BOOST_CAP;
	public static int SOUL_CONSUME_CAP;
	public static double[] DIVINITY_DATA;
	public static boolean CANCEL_IMMUNITY;

	public static boolean ALT_REMOVE_SKILLS_ON_DELEVEL;
	public static boolean ALT_REMOVE_FORGOTTEN_SCROLLS_ON_DELEVEL;

	public static Calendar CASTLE_VALIDATION_DATE;
	public static int[] CASTLE_SELECT_HOURS;

	public static boolean ALT_PCBANG_POINTS_ENABLED;
	public static double ALT_PCBANG_POINTS_BONUS_DOUBLE_CHANCE;
	public static int ALT_PCBANG_POINTS_BONUS;
	public static int ALT_PCBANG_POINTS_DELAY;
	public static int ALT_PCBANG_POINTS_MIN_LVL;

	public static boolean ALT_DEBUG_ENABLED;
	public static boolean ALT_DEBUG_PVP_ENABLED;
	public static boolean ALT_DEBUG_PVP_DUEL_ONLY;
	public static boolean ALT_DEBUG_PVE_ENABLED;
	public static boolean ALT_DEBUG_ENCHANT_CHANCE_ENABLED;

	public static double CRAFT_MASTERWORK_CHANCE;
	public static double CRAFT_DOUBLECRAFT_CHANCE;

	/** Thread pools size */
	public static int SCHEDULED_THREAD_POOL_SIZE;
	public static int EXECUTOR_THREAD_POOL_SIZE;

	public static boolean ENABLE_RUNNABLE_STATS;

	/** Network settings */
	public static SelectorConfig SELECTOR_CONFIG = new SelectorConfig();

	public static boolean AUTO_LOOT;
	public static boolean AUTO_LOOT_HERBS;
	public static boolean AUTO_LOOT_INDIVIDUAL;
	public static boolean AUTO_LOOT_FROM_RAIDS;
	public static boolean PREMIUM_AUTO_LOOT_BUY;
	public static int[] PREMIUM_AUTO_LOOT_DATA;

	/** Auto-loot for/from players with karma also? */
	public static boolean AUTO_LOOT_PK;

	/** Character name template */
	public static String CNAME_TEMPLATE;

	public static int CNAME_MAXLEN = 32;

	/** Clan name template */
	public static String CLAN_NAME_TEMPLATE;

	/** Clan title template */
	public static String CLAN_TITLE_TEMPLATE;

	/** Ally name template */
	public static String ALLY_NAME_TEMPLATE;

	/** Global chat state */
	public static boolean GLOBAL_SHOUT;
	public static boolean GLOBAL_TRADE_CHAT;
	public static int CHAT_RANGE;
	public static int SHOUT_SQUARE_OFFSET;

	/** For test servers - evrybody has admin rights */
	public static boolean EVERYBODY_HAS_ADMIN_RIGHTS;

	public static boolean ALT_GAME_UNREGISTER_RECIPE;

	/** Delay for announce SS period (in minutes) */
	public static int SS_ANNOUNCE_PERIOD;

	/** Petition manager */
	public static boolean PETITIONING_ALLOWED;
	public static int MAX_PETITIONS_PER_PLAYER;
	public static int MAX_PETITIONS_PENDING;

	/** Show mob stats/droplist to players? */
	public static boolean ALT_GAME_SHOW_DROPLIST;
	public static boolean ALT_FULL_NPC_STATS_PAGE;
	public static boolean ALLOW_NPC_SHIFTCLICK;

	public static boolean ALT_ALLOW_SELL_COMMON;
	public static boolean ALT_ALLOW_SHADOW_WEAPONS;
	public static int[] ALT_SHOP_FORBIDDEN_ITEMS;

	public static int[] ALT_ALLOWED_PET_POTIONS;

	public static boolean SKILLS_CHANCE_SHOW;
	public static double[] MODIFIER_CHANCE;
	public static double[] SKILLS_CHANCE_LIMIT;
	public static double[] MODIFIER_CHANCE_POWER;
	public static boolean ALT_SAVE_UNSAVEABLE;
	public static int ALT_SAVE_EFFECTS_REMAINING_TIME;
	public static boolean ALT_SHOW_REUSE_MSG;
	public static int ALT_REUSE_CORRECTION;
	public static int ALT_SPIRITSHOT_DISCHARGE_CORRECTION;
	public static int ALT_MUSIC_COST_GUARD_INTERVAL;

	public static int SKILLS_CAST_TIME_MIN;

	/** Таймаут на использование social action */
	public static boolean ALT_SOCIAL_ACTION_REUSE;

	/** Отключение книг для изучения скилов */
	public static boolean ALT_DISABLE_SPELLBOOKS;

	/** Alternative gameing - loss of XP on death */
	public static boolean ALT_GAME_DELEVEL;
	public static boolean ENABLE_FARM;

	public static boolean INFINITE_BEAST_SHOT;
	public static boolean INFINITE_FISH_SHOT;
	public static boolean INFINITE_SOUL_SHOT;
	public static boolean INFINITE_SPIRIT_SHOT;
	public static boolean INFINITE_BLESSED_SPIRIT_SHOT;
	public static boolean INFINITE_ARROWS_AND_BOLTS;

	public static boolean BARAKIEL_KILL_NOBLE;
	public static String[] PVP_ZONE_LIST;

	public static double[] AUGMENTATION_CHANCE_MOD;

	public static boolean ALT_GAME_SUBCLASS_WITHOUT_QUESTS;
	public static boolean ALT_ALLOW_SUBCLASS_WITHOUT_BAIUM;
	public static int ALT_GAME_LEVEL_TO_GET_SUBCLASS;
	public static int ALT_MAX_LEVEL;
	public static int ALT_MAX_SUB_LEVEL;
	public static int ALT_START_SUB_LEVEL;
	public static int ALT_GAME_SUB_ADD;
	public static boolean ALT_NO_LASTHIT;
	public static boolean ALT_KAMALOKA_NIGHTMARES_PREMIUM_ONLY;

	public static boolean ALT_PET_HEAL_BATTLE_ONLY;

	public static boolean ALT_DELETE_TRANSFORMATION_ON_DEATH;

	@VelocityVariable
	public static boolean ALT_SIMPLE_SIGNS;
	@VelocityVariable
	public static boolean ALT_TELE_TO_CATACOMBS;
	@VelocityVariable
	public static boolean ALT_BS_CRYSTALLIZE;

	@VelocityVariable
	public static boolean ALT_ALLOW_TATTOO;

	public static int[] ALT_EFFECT_LIMIT;

	public static int MULTISELL_SIZE;
	public static boolean MULTISELL_DEBUG;

	@VelocityVariable
	public static boolean SERVICES_ALLOW_ITEM_BROKER_SEARCH;
	public static int SERVICES_ITEM_BROKER_REFRESH_DELAY;

	public static boolean QUEST_SELL_ENABLE;
	public static int[] QUEST_SELL_PRICE_FOR_ALL;
	public static int[] QUEST_SELL_ID_ENABLE;
	public static String[] QUEST_SELL_PRICE_UNIQUE;

	public static boolean SERVICES_HERO_SELL_ENABLED;
	public static int[] SERVICES_HERO_SELL_PRICE;
	public static int[] SERVICES_HERO_SELL_ITEM;
	public static int[] SERVICES_HERO_SELL_DAY;
	public static boolean SERVICES_HERO_SELL_CHAT;
	public static boolean SERVICES_HERO_SELL_SKILL;
	public static boolean SERVICES_HERO_SELL_ITEMS;

	public static boolean SERVICES_CLAN_REPUTATION_SELL_ENABLED;
	public static int[] SERVICES_CLAN_REPUTATION_SELL_DATA;

	public static boolean SERVICES_CLAN_LEVEL_SELL_ENABLED;
	public static long[][] SERVICES_CLAN_LEVEL_SELL_DATA;

	public static boolean SERVICES_CHANGE_NICK_ENABLED;
	public static String SERVICES_CHANGE_NICK_TEMPLATE;
	public static int SERVICES_CHANGE_NICK_PRICE;
	public static int SERVICES_CHANGE_NICK_ITEM;

	public static boolean SERVICES_CHANGE_CLAN_NAME_ENABLED;
	public static int SERVICES_CHANGE_CLAN_NAME_PRICE;
	public static int SERVICES_CHANGE_CLAN_NAME_ITEM;

	@VelocityVariable
	public static boolean SERVICES_CHANGE_PET_NAME_ENABLED;
	public static int SERVICES_CHANGE_PET_NAME_PRICE;
	public static int SERVICES_CHANGE_PET_NAME_ITEM;

	@VelocityVariable
	public static boolean SERVICES_EXCHANGE_BABY_PET_ENABLED;
	public static int SERVICES_EXCHANGE_BABY_PET_PRICE;
	public static int SERVICES_EXCHANGE_BABY_PET_ITEM;

	public static boolean SERVICES_CHANGE_SEX_ENABLED;
	public static int SERVICES_CHANGE_SEX_PRICE;
	public static int SERVICES_CHANGE_SEX_ITEM;

	public static boolean SERVICES_CHANGE_BASE_ENABLED;
	public static int SERVICES_CHANGE_BASE_PRICE;
	public static int SERVICES_CHANGE_BASE_ITEM;

	public static boolean SERVICES_SEPARATE_SUB_ENABLED;
	public static int SERVICES_SEPARATE_SUB_PRICE;
	public static int SERVICES_SEPARATE_SUB_ITEM;

	public static boolean SERVICES_CHANGE_NICK_COLOR_ENABLED;
	public static int SERVICES_CHANGE_NICK_COLOR_PRICE;
	public static int SERVICES_CHANGE_NICK_COLOR_ITEM;
	public static String[] SERVICES_CHANGE_NICK_COLOR_LIST;

	public static boolean SERVICES_CHANGE_TITLE_COLOR_ENABLED;
	public static int SERVICES_CHANGE_TITLE_COLOR_PRICE;
	public static int SERVICES_CHANGE_TITLE_COLOR_ITEM;
	public static String[] SERVICES_CHANGE_TITLE_COLOR_LIST;

	public static int SERVICES_CLEAR_PK_PRICE;
	public static int SERVICES_CLEAR_PK_PRICE_ITEM_ID;
	public static int SERVICES_CLEAR_PK_COUNT;

	public static int SERVICES_WASH_SINS_PRICE;
	public static int SERVICES_WASH_SINS_PRICE_ITEM_ID;

	public static int SERVICES_HAIR_CHANGE_ITEM_ID;
	public static int SERVICES_HAIR_CHANGE_COUNT;

	public static boolean SERVICES_LEVEL_UP_ENABLE;
	public static int[] SERVICES_LEVEL_UP;
	public static boolean SERVICES_DELEVEL_ENABLE;
	public static int[] SERVICES_DELEVEL;

	@VelocityVariable
	public static boolean SERVICES_BASH_ENABLED;
	public static boolean SERVICES_BASH_SKIP_DOWNLOAD;
	public static int SERVICES_BASH_RELOAD_TIME;

	public static boolean SERVICES_NOBLESS_SELL_ENABLED;
	public static int SERVICES_NOBLESS_SELL_PRICE;
	public static int SERVICES_NOBLESS_SELL_ITEM;

	public static boolean SERVICES_RECOMMENDS_SELL_ENABLED;
	public static int SERVICES_RECOMMENDS_SELL_PRICE;
	public static int SERVICES_RECOMMENDS_SELL_ITEM;
	public static int SERVICES_RECOMMENDS_SELL_COUNT;

	public static boolean SERVICES_EXPAND_INVENTORY_ENABLED;
	public static int SERVICES_EXPAND_INVENTORY_PRICE;
	public static int SERVICES_EXPAND_INVENTORY_ITEM;
	public static int SERVICES_EXPAND_INVENTORY_SLOTS;
	public static int SERVICES_EXPAND_INVENTORY_MAX;

	public static boolean SERVICES_EXPAND_WAREHOUSE_ENABLED;
	public static int SERVICES_EXPAND_WAREHOUSE_PRICE;
	public static int SERVICES_EXPAND_WAREHOUSE_ITEM;
	public static int SERVICES_EXPAND_WAREHOUSE_SLOTS;
	public static int SERVICES_EXPAND_WAREHOUSE_MAX;

	public static boolean SERVICES_EXPAND_CWH_ENABLED;
	public static int SERVICES_EXPAND_CWH_PRICE;
	public static int SERVICES_EXPAND_CWH_ITEM;

	public static boolean SERVICES_OFFLINE_TRADE_ALLOW;
	public static boolean SERVICES_OFFLINE_TRADE_ALLOW_OFFSHORE;
	public static int SERVICES_OFFLINE_TRADE_MIN_LEVEL;
	public static int SERVICES_OFFLINE_TRADE_NAME_COLOR;
	public static int SERVICES_OFFLINE_TRADE_PRICE;
	public static int SERVICES_OFFLINE_TRADE_PRICE_ITEM;
	public static long SERVICES_OFFLINE_TRADE_SECONDS_TO_KICK;
	public static boolean SERVICES_OFFLINE_TRADE_RESTORE_AFTER_RESTART;
	@VelocityVariable
	public static boolean SERVICES_GIRAN_HARBOR_ENABLED;
	@VelocityVariable
	public static boolean SERVICES_PARNASSUS_ENABLED;
	public static boolean SERVICES_PARNASSUS_NOTAX;
	@VelocityVariable
	public static long SERVICES_PARNASSUS_PRICE;

	public static boolean SERVICES_ALLOW_LOTTERY;
	public static int SERVICES_LOTTERY_PRIZE;
	public static int SERVICES_ALT_LOTTERY_PRICE;
	public static int SERVICES_LOTTERY_TICKET_PRICE;
	public static double SERVICES_LOTTERY_5_NUMBER_RATE;
	public static double SERVICES_LOTTERY_4_NUMBER_RATE;
	public static double SERVICES_LOTTERY_3_NUMBER_RATE;
	public static int SERVICES_LOTTERY_2_AND_1_NUMBER_PRIZE;

	@VelocityVariable
	public static boolean SERVICES_ALLOW_ROULETTE;
	public static long SERVICES_ROULETTE_MIN_BET;
	public static long SERVICES_ROULETTE_MAX_BET;

	public static boolean ALT_ALLOW_OTHERS_WITHDRAW_FROM_CLAN_WAREHOUSE;
	public static boolean ALT_ALLOW_CLAN_COMMAND_ONLY_FOR_CLAN_LEADER;
	public static boolean ALT_GAME_REQUIRE_CLAN_CASTLE;
	public static boolean ALT_GAME_REQUIRE_CASTLE_DAWN;
	public static boolean ALT_GAME_ALLOW_ADENA_DAWN;

	/** Olympiad Compitition Starting time */
	public static int ALT_OLY_START_TIME;
	/** Olympiad Compition Min */
	public static int ALT_OLY_MIN;
	public static String OLYMPIAD_CRON;
	/** Olympaid Comptetition Period */
	public static long ALT_OLY_CPERIOD;
	/** Olympaid Weekly Period */
	public static long ALT_OLY_WPERIOD;
	/** Olympaid Validation Period */
	public static long ALT_OLY_VPERIOD;

	public static boolean ENABLE_OLYMPIAD;
	public static boolean ENABLE_OLYMPIAD_SPECTATING;

	public static int CLASS_GAME_MIN;
	public static int NONCLASS_GAME_MIN;
	public static int TEAM_GAME_MIN;

	public static int GAME_MAX_LIMIT;
	public static int GAME_CLASSES_COUNT_LIMIT;
	public static int GAME_NOCLASSES_COUNT_LIMIT;
	public static int GAME_TEAM_COUNT_LIMIT;

	public static int ALT_OLY_BATTLE_REWARD_ITEM;
	public static int ALT_OLY_CLASSED_RITEM_C;
	public static int ALT_OLY_NONCLASSED_RITEM_C;
	public static int ALT_OLY_TEAM_RITEM_C;
	public static int ALT_OLY_COMP_RITEM;
	public static int ALT_OLY_GP_PER_POINT;
	public static int ALT_OLY_HERO_POINTS;
	public static int ALT_OLY_RANK1_POINTS;
	public static int ALT_OLY_RANK2_POINTS;
	public static int ALT_OLY_RANK3_POINTS;
	public static int ALT_OLY_RANK4_POINTS;
	public static int ALT_OLY_RANK5_POINTS;
	public static int OLYMPIAD_STADIAS_COUNT;
	public static int OLYMPIAD_BATTLES_FOR_REWARD;
	public static int OLYMPIAD_POINTS_DEFAULT;
	public static int OLYMPIAD_POINTS_WEEKLY;
	public static boolean OLYMPIAD_OLDSTYLE_STAT;
	public static int[] HERO_DIARY_EXCLUDED_BOSSES;
	public static int OLYMPIAD_SKILL_COOLDOWN;
	//public static boolean CHECK_OLYMPIAD_IP;
	//public static boolean CHECK_OLYMPIAD_HWID;

	public static int ALT_OLY_WAIT_TIME;
	public static int ALT_OLY_PORT_BACK_TIME;

	public static boolean ALT_OLY_TALISMANS_MP_CONSUME;

	public static int ALT_OLY_PHYS_WEAPON_ENCHANT_LOCK;
	public static int ALT_OLY_MAGIC_WEAPON_ENCHANT_LOCK;
	public static int ALT_OLY_ARMOR_ENCHANT_LOCK;
	public static int ALT_OLY_ACCESSORY_ENCHANT_LOCK;
	public static int ALT_OLY_WEAPON_ATTRIBUTE_LOCK;
	public static int ALT_OLY_ARMOR_ATTRIBUTE_LOCK;
	public static boolean ALT_OLY_MAX_SKILL_ENCHANT;

	public static long NONOWNER_ITEM_PICKUP_DELAY;

	public static Map<Integer, PlayerAccess> gmlist = new HashMap<Integer, PlayerAccess>();

	/** Player Drop Rate control */
	public static boolean KARMA_DROP_GM;
	public static boolean KARMA_NEEDED_TO_DROP;

	public static int KARMA_DROP_ITEM_LIMIT;

	public static int KARMA_RANDOM_DROP_LOCATION_LIMIT;

	public static double KARMA_DROPCHANCE_BASE;
	public static double KARMA_DROPCHANCE_MOD;
	public static double NORMAL_DROPCHANCE_BASE;
	public static int DROPCHANCE_EQUIPMENT;
	public static int DROPCHANCE_EQUIPPED_WEAPON;
	public static int DROPCHANCE_ITEM;

	public static int AUTODESTROY_ITEM_AFTER;
	public static int AUTODESTROY_PLAYER_ITEM_AFTER;

	public static int DELETE_DAYS;

	/** Datapack root directory */
	public static File DATAPACK_ROOT;

	public static double CLANHALL_BUFFTIME_MODIFIER;
	public static double SONGDANCETIME_MODIFIER;

	public static double MAXLOAD_MODIFIER;
	public static double GATEKEEPER_MODIFIER;
	public static double GATEKEEPER_NOBLE_MODIFIER;
	public static boolean ALT_IMPROVED_PETS_LIMITED_USE;
	public static int GATEKEEPER_FREE;
	public static int CRUMA_GATEKEEPER_LVL;

	public static boolean ALT_SKILL_ENCHANT_UPDATE_REUSE;
	public static double ALT_SKILL_ENCHANT_ADENA_MODIFIER;
	public static double ALT_SKILL_ENCHANT_SP_MODIFIER;
	public static double ALT_SKILL_SAFE_ENCHANT_ADENA_MODIFIER;
	public static double ALT_SKILL_SAFE_ENCHANT_SP_MODIFIER;
	public static double ALT_SKILL_ROUTE_CHANGE_ADENA_MODIFIER;
	public static double ALT_SKILL_ROUTE_CHANGE_SP_MODIFIER;
	public static double ALT_SKILL_UNTRAIN_REFUND_SP_MODIFIER;

	public static boolean ALT_NO_FAME_FOR_DEAD;
	public static boolean ALT_NOT_ALLOW_TW_WARDS_IN_CLANHALLS;

	public static double ALT_CHAMPION_CHANCE1;
	public static double ALT_CHAMPION_CHANCE2;
	public static boolean ALT_CHAMPION_CAN_BE_AGGRO;
	public static boolean ALT_CHAMPION_CAN_BE_SOCIAL;
	public static int ALT_CHAMPION_MIN_LEVEL;
	public static int ALT_CHAMPION_TOP_LEVEL;

	public static boolean ALLOW_DISCARDITEM;
	public static boolean ALLOW_MAIL;
	public static boolean ALLOW_WAREHOUSE;
	public static boolean ALLOW_WATER;
	public static boolean ALLOW_CURSED_WEAPONS;
	public static boolean DROP_CURSED_WEAPONS_ON_KICK;
	public static boolean ALLOW_NOBLE_TP_TO_ALL;

	/** Pets */
	public static int SWIMING_SPEED;

	/** protocol revision */
	public static int MIN_PROTOCOL_REVISION;
	public static int MAX_PROTOCOL_REVISION;

	/** random animation interval */
	public static int MIN_NPC_ANIMATION;
	public static int MAX_NPC_ANIMATION;

	/** Время запланированного на определенное время суток рестарта */
	public static String RESTART_AT_TIME;

	public static int GAME_SERVER_LOGIN_PORT;
	public static String GAME_SERVER_LOGIN_HOST;
	public static String GAME_SERVER_LOGIN_DATABASE_NAME;
	public static String INTERNAL_HOSTNAME;
	public static String EXTERNAL_HOSTNAME;

	public static boolean SERVER_SIDE_NPC_NAME;
	public static boolean SERVER_SIDE_NPC_TITLE;
	public static boolean SERVER_SIDE_NPC_TITLE_AGGRO;
	public static boolean SERVER_SIDE_NPC_TITLE_LEVEL;

	public static int CLASS_MASTERS_PRICE_ITEM;
	public static List<Integer> ALLOW_CLASS_MASTERS_LIST = new ArrayList<Integer>();
	public static int[] CLASS_MASTERS_PRICE_LIST = new int[4];
	public static boolean ALLOW_EVENT_GATEKEEPER;

	/** Inventory slots limits */
	public static int INVENTORY_MAXIMUM_NO_DWARF;
	public static int INVENTORY_MAXIMUM_DWARF;
	public static int INVENTORY_MAXIMUM_GM;
	public static int QUEST_INVENTORY_MAXIMUM;

	/** Warehouse slots limits */
	public static int WAREHOUSE_SLOTS_NO_DWARF;
	public static int WAREHOUSE_SLOTS_DWARF;
	public static int WAREHOUSE_SLOTS_CLAN;

	public static int FREIGHT_SLOTS;

	/** Spoil Rates */
	public static double MINIMUM_SPOIL_RATE;
	public static double BASE_SPOIL_RATE;

	/** Manor Config */
	public static double MANOR_SOWING_BASIC_SUCCESS;
	public static double MANOR_SOWING_ALT_BASIC_SUCCESS;
	public static double MANOR_HARVESTING_BASIC_SUCCESS;
	public static int MANOR_DIFF_PLAYER_TARGET;
	public static double MANOR_DIFF_PLAYER_TARGET_PENALTY;
	public static int MANOR_DIFF_SEED_TARGET;
	public static double MANOR_DIFF_SEED_TARGET_PENALTY;

	/** Karma System Variables */
	public static int KARMA_MIN_KARMA;
	public static int KARMA_SP_DIVIDER;
	public static int KARMA_LOST_BASE;

	public static int MIN_PK_TO_ITEMS_DROP;
	public static boolean DROP_ITEMS_ON_DIE;
	public static boolean DROP_ITEMS_AUGMENTED;

	public static List<Integer> KARMA_LIST_NONDROPPABLE_ITEMS = new ArrayList<Integer>();

	public static int PVP_TIME;
	public static boolean AUTO_PVP;
	public static boolean AUTO_PVP_CLAN_WAR;
	public static boolean PVP_IN_BATTLE_ZONE;

	/** Karma Punishment */
	public static boolean ALT_GAME_KARMA_PLAYER_CAN_SHOP;

	public static boolean REGEN_SIT_WAIT;

	public static double RATE_RAID_REGEN;
	public static double RATE_RAID_DEFENSE;
	public static double RATE_RAID_ATTACK;
	public static double RATE_EPIC_DEFENSE;
	public static double RATE_EPIC_ATTACK;
	public static int RAID_MAX_LEVEL_DIFF;
	public static boolean PARALIZE_ON_RAID_DIFF;

	public static double ALT_PK_DEATH_RATE;

	/** Deep Blue Mobs' Drop Rules Enabled */
	public static boolean DEEPBLUE_DROP_RULES;
	public static int DEEPBLUE_DROP_MAXDIFF;
	public static int DEEPBLUE_DROP_RAID_MAXDIFF;
	public static boolean UNSTUCK_SKILL;

	/** telnet enabled */
	public static boolean IS_TELNET_ENABLED;
	public static String TELNET_DEFAULT_ENCODING;
	public static String TELNET_PASSWORD;
	public static String TELNET_HOSTNAME;
	public static int TELNET_PORT;

	/** Percent CP is restore on respawn */
	public static double RESPAWN_RESTORE_CP;
	/** Percent HP is restore on respawn */
	public static double RESPAWN_RESTORE_HP;
	/** Percent MP is restore on respawn */
	public static double RESPAWN_RESTORE_MP;

	/** Maximum number of available slots for pvt stores (sell/buy) - Dwarves */
	public static int MAX_PVTSTORE_SLOTS_DWARF;
	/** Maximum number of available slots for pvt stores (sell/buy) - Others */
	public static int MAX_PVTSTORE_SLOTS_OTHER;
	public static int MAX_PVTCRAFT_SLOTS;
	public static boolean PVTSTORE_OFFSHORE_ASK;
	public static int[] PVTSTORE_OFFSHORE_CORD;

	public static boolean SENDSTATUS_TRADE_JUST_OFFLINE;
	public static double SENDSTATUS_TRADE_MOD;

	public static boolean ALT_CH_ALL_BUFFS;
	public static boolean ALT_CH_ALLOW_1H_BUFFS;

	public static double RESIDENCE_LEASE_FUNC_MULTIPLIER;

	public static boolean ACCEPT_ALTERNATE_ID;
	public static int REQUEST_ID;
	public static boolean IPCONFIG_ENABLE;

	public static boolean ANNOUNCE_MAMMON_SPAWN;

	public static int GM_NAME_COLOUR;
	public static int NORMAL_NAME_COLOUR;
	public static int CLANLEADER_NAME_COLOUR;

	/** AI */
	public static int AI_TASK_MANAGER_COUNT;
	public static long AI_TASK_ATTACK_DELAY;
	public static long AI_TASK_ACTIVE_DELAY;
	public static boolean BLOCK_ACTIVE_TASKS;
	public static boolean ALWAYS_TELEPORT_HOME;
	public static boolean RND_WALK;
	public static int RND_WALK_RATE;
	public static int RND_ANIMATION_RATE;

	public static int AGGRO_CHECK_INTERVAL;
	public static long NONAGGRO_TIME_ONTELEPORT;

	/** Maximum range mobs can randomly go from spawn point */
	public static int MAX_DRIFT_RANGE;

	/** Maximum range mobs can pursue agressor from spawn point */
	public static int MAX_PURSUE_RANGE;
	public static int MAX_PURSUE_UNDERGROUND_RANGE;
	public static int MAX_PURSUE_RANGE_RAID;
	public static boolean COZY_MUCUS_BUFF;
	public static int[] KARIK_REPRODUCTION;
	public static double KARIK_REPRODUCTION_CHANCE;
	public static int[] DRAKOS_REPRODUCTION;
	public static double DRAKOS_REPRODUCTION_CHANCE;

	public static boolean ALT_DEATH_PENALTY;
	public static boolean ALLOW_DEATH_PENALTY_C5;
	public static int ALT_DEATH_PENALTY_C5_CHANCE;
	public static boolean ALT_DEATH_PENALTY_C5_CHAOTIC_RECOVERY;
	public static int ALT_DEATH_PENALTY_C5_EXPERIENCE_PENALTY;
	public static int ALT_DEATH_PENALTY_C5_KARMA_PENALTY;
	public static long ALT_SELLPRICE;

	public static boolean SAVE_GM_EFFECTS;

	public static boolean AUTO_LEARN_SKILLS;
	public static boolean AUTO_LEARN_FORGOTTEN_SKILLS;
	public static boolean AUTO_LEARN_DIVINE_INSPIRATION;

	public static int MOVE_PACKET_DELAY;
	public static int ATTACK_PACKET_DELAY;
	public static int ATTACK_END_DELAY;

	public static boolean DAMAGE_FROM_FALLING;

	/** Wedding Options */
	public static boolean ALLOW_WEDDING;
	public static int WEDDING_PRICE;
	public static boolean WEDDING_PUNISH_INFIDELITY;
	public static boolean WEDDING_TELEPORT_ENABLED;
	public static int WEDDING_TELEPORT_PRICE;
	public static int WEDDING_TELEPORT_INTERVAL;
	public static boolean WEDDING_SAMESEX;
	public static boolean WEDDING_FORMALWEAR;
	public static int WEDDING_DIVORCE_COSTS;

	public static int FOLLOW_RANGE;

	public static boolean ALT_PRIEST_BLESSING_ENABLED;

	public static boolean ALT_ITEM_AUCTION_ENABLED;
	public static boolean ALT_ITEM_AUCTION_CAN_REBID;
	public static boolean ALT_ITEM_AUCTION_START_ANNOUNCE;
	public static int ALT_ITEM_AUCTION_BID_ITEM_ID;
	public static long ALT_ITEM_AUCTION_MAX_BID;
	public static int ALT_ITEM_AUCTION_MAX_CANCEL_TIME_IN_MILLIS;

	public static boolean ALT_FISH_CHAMPIONSHIP_ENABLED;
	public static int ALT_FISH_CHAMPIONSHIP_REWARD_ITEM;
	public static int ALT_FISH_CHAMPIONSHIP_REWARD_1;
	public static int ALT_FISH_CHAMPIONSHIP_REWARD_2;
	public static int ALT_FISH_CHAMPIONSHIP_REWARD_3;
	public static int ALT_FISH_CHAMPIONSHIP_REWARD_4;
	public static int ALT_FISH_CHAMPIONSHIP_REWARD_5;

	public static boolean ALT_ENABLE_BLOCK_CHECKER_EVENT;
	public static int ALT_MIN_BLOCK_CHECKER_TEAM_MEMBERS;
	public static double ALT_RATE_COINS_REWARD_BLOCK_CHECKER;
	public static boolean ALT_HBCE_FAIR_PLAY;
	public static int ALT_PET_INVENTORY_LIMIT;

	/**limits of stats **/
	public static int LIM_FAME;
	public static int LIM_CRIT_DAM;

	public static double ALT_NPC_PATK_MODIFIER;
	public static double ALT_NPC_MATK_MODIFIER;
	public static double ALT_NPC_PDEF_MODIFIER;
	public static double ALT_NPC_MDEF_MODIFIER;
	public static double ALT_NPC_MAXHP_MODIFIER;
	public static double ALT_NPC_MAXMP_MODIFIER;

	/** Enchant Config **/
	public static int SAFE_ENCHANT_COMMON;
	public static int SAFE_ENCHANT_FULL_BODY;

	public static int FESTIVAL_MIN_PARTY_SIZE;
	public static double FESTIVAL_RATE_PRICE;

	/** Dimensional Rift Config **/
	public static int RIFT_MIN_PARTY_SIZE;
	public static int RIFT_SPAWN_DELAY; // Time in ms the party has to wait until the mobs spawn
	public static int RIFT_MAX_JUMPS;
	public static int RIFT_AUTO_JUMPS_TIME;
	public static int RIFT_AUTO_JUMPS_TIME_RAND;
	public static int RIFT_ENTER_COST_RECRUIT;
	public static int RIFT_ENTER_COST_SOLDIER;
	public static int RIFT_ENTER_COST_OFFICER;
	public static int RIFT_ENTER_COST_CAPTAIN;
	public static int RIFT_ENTER_COST_COMMANDER;
	public static int RIFT_ENTER_COST_HERO;

	public static boolean ALLOW_TALK_WHILE_SITTING;

	public static boolean PARTY_LEADER_ONLY_CAN_INVITE;

	/** Разрешено ли изучение скилов трансформации и саб классов без наличия выполненного квеста */
	public static boolean ALLOW_LEARN_TRANS_SKILLS_WO_QUEST;

	/** Allow Manor system */
	public static boolean ALLOW_MANOR;

	/** Manor Refresh Starting time */
	public static int MANOR_REFRESH_TIME;

	/** Manor Refresh Min */
	public static int MANOR_REFRESH_MIN;

	/** Manor Next Period Approve Starting time */
	public static int MANOR_APPROVE_TIME;

	/** Manor Next Period Approve Min */
	public static int MANOR_APPROVE_MIN;

	/** Manor Maintenance Time */
	public static int MANOR_MAINTENANCE_PERIOD;

	/** Master Yogi event enchant config */
	public static int ENCHANT_CHANCE_MASTER_YOGI_STAFF;
	public static int ENCHANT_MAX_MASTER_YOGI_STAFF;
	public static int SAFE_ENCHANT_MASTER_YOGI_STAFF;
	public static int[] IVORY_REWARD_ID;
	public static int[] CHEST_REWARD_ID;
	public static boolean DAILY_TASKS_ENABLE;


	public static boolean SERVICES_NO_TRADE_ONLY_OFFLINE;
	public static double SERVICES_TRADE_TAX;
	public static double SERVICES_OFFSHORE_TRADE_TAX;
	public static boolean SERVICES_OFFSHORE_NO_CASTLE_TAX;
	public static boolean SERVICES_TRADE_TAX_ONLY_OFFLINE;
	public static boolean SERVICES_TRADE_ONLY_FAR;
	public static int SERVICES_TRADE_RADIUS;
	public static int SERVICES_TRADE_MIN_LEVEL;

	public static boolean SERVICES_ENABLE_NO_CARRIER;

	public static int SERVICES_NO_CARRIER_MAX_TIME;
	public static int SERVICES_NO_CARRIER_MIN_TIME;

	public static boolean ALT_OPEN_CLOAK_SLOT;

	public static boolean ALT_SHOW_SERVER_TIME;

	public static boolean ALT_DISABLE_FEATHER_ON_SIEGES_AND_EPIC;

	/** Geodata config */
	public static int GEO_X_FIRST, GEO_Y_FIRST, GEO_X_LAST, GEO_Y_LAST;
	public static boolean ALLOW_GEODATA;
	public static boolean ALLOW_FALL_FROM_WALLS;
	public static boolean ALLOW_KEYBOARD_MOVE;
	public static boolean COMPACT_GEO;
	public static int CLIENT_Z_SHIFT;
	public static int MAX_Z_DIFF;
	public static int MIN_LAYER_HEIGHT;
	public static int REGION_EDGE_MAX_Z_DIFF;

	/** Geodata (Pathfind) config */
	public static int PATHFIND_BOOST;
	public static int PATHFIND_MAP_MUL;
	public static boolean PATHFIND_DIAGONAL;
	public static boolean PATH_CLEAN;
	public static int PATHFIND_MAX_Z_DIFF;
	public static long PATHFIND_MAX_TIME;
	public static String PATHFIND_BUFFERS;

	public static boolean DEBUG;

	/* Item-Mall Configs */
	public static int GAME_POINT_ITEM_ID;
	public static boolean AUTO_CONSUME;
	public static int[] AUTO_CONSUME_LIST;

	public static int WEAR_DELAY;

	public static boolean EX_GOODS_INVENTORY_ENABLED;
	public static boolean EX_NEW_PETITION_SYSTEM;
	public static boolean EX_JAPAN_MINIGAME;
	public static boolean EX_LECTURE_MARK;
	public static boolean EX_USE_TELEPORT_FLAG;
	public static boolean EX_CHANGE_NAME_DIALOG;
	public static boolean SECOND_AUTH_ENABLED;

	public static boolean AUTH_SERVER_GM_ONLY;
	public static boolean AUTH_SERVER_BRACKETS;
	public static boolean AUTH_SERVER_IS_PVP;
	public static int AUTH_SERVER_AGE_LIMIT;
	public static int AUTH_SERVER_SERVER_TYPE;

	public static boolean COMMUNITYBOARD_ENABLED;
	public static String BBS_DEFAULT;
	public static String BBS_PATH;
	public static boolean BBS_CHARACTER_INFO;
	public static boolean BBS_SERVER_INFO;
	public static boolean BBS_RATES_INFO;
	public static int BBS_ONLINE_CHEAT;
	public static boolean BBS_SERVER_RESTART;
	public static boolean BBS_CHECK_IN_COMBAT;
	public static boolean BBS_CHECK_DEATH;
	public static boolean BBS_CHECK_MOVEMENT_DISABLE;
	public static boolean BBS_CHECK_ON_SIEGE_FIELD;
	public static boolean BBS_CHECK_ATTACKING_NOW;
	public static boolean BBS_CHECK_IN_OLYMPIAD_MODE;
	public static boolean BBS_CHECK_FLYING;
	public static boolean BBS_CHECK_IN_DUEL;
	public static boolean BBS_CHECK_IN_INSTANCE;
	public static boolean BBS_CHECK_IN_JAILED;
	public static boolean BBS_CHECK_OUT_OF_CONTROL;
	public static boolean BBS_CHECK_OUT_OF_TOWN_ONLY_FOR_PREMIUM;
	public static boolean BBS_CHECK_IN_EVENT;

	public static void loadCommunityBoardSettings()
	{
		ExProperties board = load(COMMUNITY_BOARD);

		COMMUNITYBOARD_ENABLED = board.getProperty("Allow", true);
		BBS_DEFAULT = board.getProperty("Link", "_bbshome");
		BBS_PATH = board.getProperty("Path", "community/high");
		BBS_CHARACTER_INFO = board.getProperty("Character", true);
		BBS_SERVER_INFO = board.getProperty("Server", true);
		BBS_RATES_INFO = board.getProperty("Rates", true);
		BBS_ONLINE_CHEAT = board.getProperty("OnCheat", 0);
		BBS_SERVER_RESTART = board.getProperty("Restart", true);

		BBS_CHECK_OUT_OF_TOWN_ONLY_FOR_PREMIUM = board.getProperty("OutOfTownForPremium", false);
		BBS_CHECK_MOVEMENT_DISABLE = board.getProperty("MovementDisabled", false);
		BBS_CHECK_IN_COMBAT = board.getProperty("InCombat", false);
		BBS_CHECK_DEATH = board.getProperty("Death", false);
		BBS_CHECK_ON_SIEGE_FIELD = board.getProperty("OnSiegeField", false);
		BBS_CHECK_ATTACKING_NOW = board.getProperty("AttackingNow", false);
		BBS_CHECK_IN_OLYMPIAD_MODE = board.getProperty("InOlympiadMode", false);
		BBS_CHECK_FLYING = board.getProperty("Flying", false);
		BBS_CHECK_IN_DUEL = board.getProperty("InDuel", false);
		BBS_CHECK_IN_INSTANCE = board.getProperty("InInstance", false);
		BBS_CHECK_IN_JAILED = board.getProperty("InJailed", false);
		BBS_CHECK_OUT_OF_CONTROL = board.getProperty("OutOfControl", false);
		BBS_CHECK_IN_EVENT = board.getProperty("InEvent", false);
	}

	public static boolean BBS_BUFFER_ALLOWED_BUFFER;
	public static int[] BBS_BUFFER_PRICE;
	public static boolean BBS_BUFFER_PRICE_BY_LEVEL;
	public static boolean BBS_BUFFER_PRICE_PREMIUM_MOD;
	public static double BBS_BUFFER_PRICE_PREMIUM_MOD_COUNT;
	public static int BBS_BUFFER_SAVE_PRICE_ID;
	public static int BBS_BUFFER_SAVE_PRICE_ONE;
	public static int BBS_BUFFER_ALT_TIME;
	public static int BBS_BUFFER_MIN_LVL;
	public static int BBS_BUFFER_MAX_LVL;
	public static int BBS_BUFFER_FREE_LEVEL;
	public static boolean BBS_BUFFER_ALLOW_SIEGE;
	public static boolean BBS_BUFFER_ALLOW_PVP_FLAG;
	public static boolean BBS_BUFFER_ALLOW_EVENTS;
	public static boolean BBS_BUFFER_ALLOW_BATTLE;
	public static boolean BBS_BUFFER_RECOVER_HP_MP_CP;
	public static boolean BBS_BUFFER_RECOVER_HP_MP_CP_IN_PEACE;
	public static int[] BS_BUFFER_RECOVER_HP_MP_CP_PRICE;
	public static boolean BBS_BUFFER_CLEAR_BUFF;
	public static boolean BBS_BUFFER_RECOVER_CHECK_PVP_FLAG;
	public static boolean BBS_BUFFER_RECOVER_CHECK_BATTLE;

	public static void loadBufferSettings()
	{
		ExProperties buffer = load(COMMUNITY_BUFFER);

		BBS_BUFFER_ALLOWED_BUFFER = buffer.getProperty("Enable", false);
		BBS_BUFFER_PRICE = buffer.getProperty("Price", new int[] { 57, 1000 });
		BBS_BUFFER_PRICE_BY_LEVEL = buffer.getProperty("PriceMod", false);
		BBS_BUFFER_PRICE_PREMIUM_MOD = buffer.getProperty("PremiumMod", false);
		BBS_BUFFER_PRICE_PREMIUM_MOD_COUNT = buffer.getProperty("PAMOD", 1.);
		BBS_BUFFER_SAVE_PRICE_ID = buffer.getProperty("SaveId", 57);
		BBS_BUFFER_SAVE_PRICE_ONE = buffer.getProperty("SavePrice", 1000);
		BBS_BUFFER_ALT_TIME = buffer.getProperty("Time", 1);
		BBS_BUFFER_MIN_LVL = buffer.getProperty("MinLevel", 1);
		BBS_BUFFER_MAX_LVL = buffer.getProperty("MaxLevel", 85);
		BBS_BUFFER_FREE_LEVEL = buffer.getProperty("FreeLevel", 40);
		BBS_BUFFER_RECOVER_HP_MP_CP = buffer.getProperty("Recover", false);
		BBS_BUFFER_RECOVER_HP_MP_CP_IN_PEACE = buffer.getProperty("PeaceRecover", true);
		BS_BUFFER_RECOVER_HP_MP_CP_PRICE = buffer.getProperty("RecoverPrice", new int[] { 57, 75000 });
		BBS_BUFFER_CLEAR_BUFF = buffer.getProperty("Clear", false);
		BBS_BUFFER_ALLOW_SIEGE = buffer.getProperty("InSiege", false);
		BBS_BUFFER_ALLOW_PVP_FLAG = buffer.getProperty("InPvP", false);
		BBS_BUFFER_ALLOW_BATTLE = buffer.getProperty("InBattle", false);
		BBS_BUFFER_ALLOW_EVENTS = buffer.getProperty("OnEvents", false);
		BBS_BUFFER_RECOVER_CHECK_PVP_FLAG = buffer.getProperty("RecoverPvPFlag", false);
		BBS_BUFFER_RECOVER_CHECK_BATTLE = buffer.getProperty("RecoverBattle", false);
	}

	public static boolean BBS_TELEPORT_ALLOW;
	public static int BBS_TELEPORT_SAVE_ITEM_ID;
	public static int BBS_TELEPORT_SAVE_PRICE;
	public static int BBS_TELEPORT_PA_SAVE_ITEM_ID;
	public static int BBS_TELEPORT_PA_SAVE_PRICE;
	public static int BBS_TELEPORT_FREE_LEVEL;
	public static int BBS_TELEPORT_MAX_COUNT;
	public static boolean BBS_TELEPORT_POINT_PA;
	public static boolean BBS_TELEPORT_ALLOW_IN_INSTANCE;
	public static boolean BBS_TELEPORT_ALLOW_IN_UNDERWATHER;
	public static boolean BBS_TELEPORT_ALLOW_IN_COMBAT;
	public static boolean BBS_TELEPORT_ALLOW_IN_PVP_FLAG;
	public static boolean BBS_TELEPORT_ALLOW_ON_SIEGE;

	public static void loadTeleportSettings()
	{
		ExProperties teleport = load(COMMUNITY_TELEPORT);

		BBS_TELEPORT_ALLOW = teleport.getProperty("Allow", false);
		BBS_TELEPORT_FREE_LEVEL = teleport.getProperty("FreeByLevel", 40);
		BBS_TELEPORT_ALLOW_IN_INSTANCE = teleport.getProperty("InInstance", false);
		BBS_TELEPORT_ALLOW_IN_UNDERWATHER = teleport.getProperty("UnderWater", false);
		BBS_TELEPORT_ALLOW_IN_COMBAT = teleport.getProperty("InCombat", false);
		BBS_TELEPORT_ALLOW_IN_PVP_FLAG = teleport.getProperty("InPvPFlag", false);
		BBS_TELEPORT_ALLOW_ON_SIEGE = teleport.getProperty("InSiege", false);

		BBS_TELEPORT_SAVE_ITEM_ID = teleport.getProperty("SaveItemId", 57);
		BBS_TELEPORT_SAVE_PRICE = teleport.getProperty("SavePrice", 5000);
		BBS_TELEPORT_PA_SAVE_ITEM_ID = teleport.getProperty("PremiumSaveItemId", 57);
		BBS_TELEPORT_PA_SAVE_PRICE = teleport.getProperty("PremiumSavePrice", 2500);
		BBS_TELEPORT_MAX_COUNT = teleport.getProperty("MaxPointsCount", 12);
		BBS_TELEPORT_POINT_PA = teleport.getProperty("PointForPremium", false);
	}

	public static boolean BBS_STATISTIC_ALLOW;
	public static int BBS_STATISTIC_COUNT;
	public static int BBS_STATISTIC_UPDATE_TIME;
	public static int BBS_STATISTIC_ITEM;
	public static int[] BBS_STATISTIC_RAID;

	public static void loadStatisticSettings()
	{
		ExProperties statistic = load(COMMUNITY_STATISTIC);
		BBS_STATISTIC_ALLOW = statistic.getProperty("Allow", false);
		BBS_STATISTIC_COUNT = statistic.getProperty("Count", 10);
		BBS_STATISTIC_UPDATE_TIME = statistic.getProperty("Update", 5);
		BBS_STATISTIC_ITEM = statistic.getProperty("Item", 57);
	}

	public static boolean BBS_FORGE_ENABLED;
	public static int BBS_FORGE_ENCHANT_ITEM;
	public static int[] BBS_FORGE_ENCHANT_MAX;
	public static int[] BBS_FORGE_WEAPON_ENCHANT_LVL;
	public static int[] BBS_FORGE_ARMOR_ENCHANT_LVL;
	public static int[] BBS_FORGE_JEWELS_ENCHANT_LVL;
	public static int[] BBS_FORGE_ENCHANT_PRICE_WEAPON;
	public static int[] BBS_FORGE_ENCHANT_PRICE_ARMOR;
	public static int[] BBS_FORGE_ENCHANT_PRICE_JEWELS;
	public static int[] BBS_FORGE_AUGMENT_ITEMS_LIST;
	public static long[] BBS_FORGE_AUGMENT_COST_DATA;

	public static int BBS_FORGE_WEAPON_ATTRIBUTE_MAX;
	public static int BBS_FORGE_ARMOR_ATTRIBUTE_MAX;
	public static int[] BBS_FORGE_ATRIBUTE_LVL_WEAPON;
	public static int[] BBS_FORGE_ATRIBUTE_LVL_ARMOR;
	public static int[] BBS_FORGE_ATRIBUTE_PRICE_ARMOR;
	public static int[] BBS_FORGE_ATRIBUTE_PRICE_WEAPON;
	public static boolean BBS_FORGE_ATRIBUTE_PVP;
	public static String[] BBS_FORGE_GRADE_ATTRIBUTE;

	public static long[] BBS_FORGE_FOUNDATION_PRICE_WEAPON;
	public static long[] BBS_FORGE_FOUNDATION_PRICE_ARMOR;
	public static long[] BBS_FORGE_FOUNDATION_PRICE_ACCESORY;

	public static void loadForgeSettings()
	{
		ExProperties forge = load(COMMUNITY_FORGE);
		BBS_FORGE_ENABLED = forge.getProperty("Allow", false);
		BBS_FORGE_ENCHANT_ITEM = forge.getProperty("Item", 4356);
		BBS_FORGE_ENCHANT_MAX = forge.getProperty("MaxEnchant", new int[] { 25 });
		BBS_FORGE_WEAPON_ENCHANT_LVL = forge.getProperty("WValue", new int[] { 5 });
		BBS_FORGE_ARMOR_ENCHANT_LVL = forge.getProperty("AValue", new int[] { 5 });
		BBS_FORGE_JEWELS_ENCHANT_LVL = forge.getProperty("JValue", new int[] { 5 });
		BBS_FORGE_ENCHANT_PRICE_WEAPON = forge.getProperty("WPrice", new int[] { 5 });
		BBS_FORGE_ENCHANT_PRICE_ARMOR = forge.getProperty("APrice", new int[] { 5 });
		BBS_FORGE_ENCHANT_PRICE_JEWELS = forge.getProperty("JPrice", new int[] { 5 });

		BBS_FORGE_AUGMENT_ITEMS_LIST = forge.getProperty("AugmentBreak", new int[] { -1 });
		BBS_FORGE_AUGMENT_COST_DATA = forge.getProperty("AugmentCost", new long[] { 4037, 10 });

		BBS_FORGE_ATRIBUTE_LVL_WEAPON = forge.getProperty("AtributeWeaponValue", new int[] { 25 });
		BBS_FORGE_ATRIBUTE_PRICE_WEAPON = forge.getProperty("PriceForAtributeWeapon", new int[] { 25 });
		BBS_FORGE_ATRIBUTE_LVL_ARMOR = forge.getProperty("AtributeArmorValue", new int[] { 25 });
		BBS_FORGE_ATRIBUTE_PRICE_ARMOR = forge.getProperty("PriceForAtributeArmor", new int[] { 25 });
		BBS_FORGE_ATRIBUTE_PVP = forge.getProperty("AtributePvP", true);
		BBS_FORGE_WEAPON_ATTRIBUTE_MAX = forge.getProperty("MaxWAttribute", 25);
		BBS_FORGE_ARMOR_ATTRIBUTE_MAX = forge.getProperty("MaxAAttribute", 25);

		BBS_FORGE_GRADE_ATTRIBUTE = forge.getProperty("AtributeGrade", "NG:NO;D:NO;C:NO;B:NO;A:ON;S:ON;S80:ON;S84:ON").trim().replaceAll(" ", "").split(";");

		BBS_FORGE_FOUNDATION_PRICE_WEAPON = forge.getProperty("FoundationWeapon", new long[] { 4037, 25 });
		BBS_FORGE_FOUNDATION_PRICE_ARMOR = forge.getProperty("FoundationArmor", new long[] { 4037, 25 });
		BBS_FORGE_FOUNDATION_PRICE_ACCESORY = forge.getProperty("FoundationJewels", new long[] { 4037, 25 });
	}

	public static int[] BBS_BODY_CHANGE_WEAPON;
	public static int[] BBS_BODY_CHANGE_ARMOR;
	public static int[] BBS_BODY_CHANGE_ACCESSORIES;
	public static int[] BBS_BODY_CHANGE_COSTUME;
	public static int[] BBS_BODY_REMOVE_ITEM;
	public static boolean BBS_BODY_VISUAL_HERO;
	public static boolean BBS_BODY_VISUAL_TO_HERO;
	public static boolean BBS_ALLOW_IP_LOCK;
	public static boolean BBS_ALLOW_SHARE_ACCOUNT;
	public static boolean BBS_ALLOW_HWID_LOCK;
	public static int BBS_HWID_LOCK_MASK;

	public static void loadCabinetSettings()
	{
		ExProperties cabinet = load(COMMUNITY_CABINET);

		BBS_BODY_CHANGE_WEAPON = cabinet.getProperty("VisualWeapon", new int[] { 500, 4356 });
		BBS_BODY_CHANGE_ARMOR = cabinet.getProperty("VisualArmor", new int[] { 250, 4356 });
		BBS_BODY_CHANGE_ACCESSORIES = cabinet.getProperty("VisualAccessories", new int[] { 250, 4356 });
		BBS_BODY_CHANGE_COSTUME = cabinet.getProperty("VisualCostume", new int[] { 600, 4356 });
		BBS_BODY_REMOVE_ITEM = cabinet.getProperty("VisualRemove", new int[] { 10, 4356 });
		BBS_BODY_VISUAL_HERO = cabinet.getProperty("VisualHero", false);
		BBS_BODY_VISUAL_TO_HERO = cabinet.getProperty("VisualToHero", false);
		BBS_ALLOW_IP_LOCK = cabinet.getProperty("AllowLockIP", false);
		BBS_ALLOW_SHARE_ACCOUNT = cabinet.getProperty("AllowShare", false);
		BBS_ALLOW_HWID_LOCK = cabinet.getProperty("AllowLockHwid", false);
		BBS_HWID_LOCK_MASK = cabinet.getProperty("HwidLockMask", 10);
	}

	public static boolean BBS_RECRUITMENT_ALLOW;
	public static int BBS_RECRUITMENT_CLAN_DESCRIPTION_MIN;
	public static int[] BBS_RECRUITMENT_TIME;
	public static int[] BBS_RECRUITMENT_ITEMS;

	public static void loadRecruitmentSettings()
	{
		ExProperties recruitment = load(COMMUNITY_RECRUITMENT);

		BBS_RECRUITMENT_ALLOW = recruitment.getProperty("Allow", false);
		BBS_RECRUITMENT_CLAN_DESCRIPTION_MIN = recruitment.getProperty("Description", 10);
		BBS_RECRUITMENT_TIME = recruitment.getProperty("Time", new int[] { 2, 3, 5, 6 });
		BBS_RECRUITMENT_ITEMS = recruitment.getProperty("Items", new int[] { 57, 4036, 4356 });
	}

	public static boolean BBS_DATABASE_ALLOW;
	public static int[] BBS_DATABASE_DISABLED_TELEPORT;

	public static void loadDropBaseSettings()
	{
		ExProperties drop = load(COMMUNITY_DROPBASE);

		BBS_DATABASE_ALLOW = drop.getProperty("Allow", false);
		BBS_DATABASE_DISABLED_TELEPORT = drop.getProperty("Teleports", new int[] { 0 });
	}

	public static boolean BBS_BOSSES_ALLOW;
	public static int[] BBS_BOSSES_DISABLED_TELEPORT;
	public static int[] BBS_BOSSES_HIDE;

	public static void loadBossesBaseSettings()
	{
		ExProperties bosses = load(COMMUNITY_BOSSES);

		BBS_BOSSES_ALLOW = bosses.getProperty("Allow", false);
		BBS_BOSSES_HIDE = bosses.getProperty("Hide", new int[] { 0 });
		BBS_BOSSES_DISABLED_TELEPORT = bosses.getProperty("Teleports", new int[] { 0 });
	}

	public static boolean BBS_CLASS_MASTER_ALLOW;
	public static List<Integer> BBS_CLASS_MASTERS_ALLOW_LIST = new ArrayList<Integer>();
	public static int[] BBS_CLASS_MASTER_PRICE_ITEM;
	public static long[] BBS_CLASS_MASTER_PRICE_COUNT;
	public static int[] BBS_CLASS_MASTER_PRICE_SECOND_ITEM;
	public static long[] BBS_CLASS_MASTER_PRICE_SECOND_COUNT;
	public static boolean BBS_CLASS_MASTER_BUY_NOBLESSE;
	public static boolean BBS_CLASS_MASTER_ADD_SUB_CLASS;
	public static long[] BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT;
	public static boolean BBS_CLASS_MASTER_CHANGE_SUB_CLASS;
	public static long[] BBS_CLASS_MASTER_SUB_CHANGE_PRICE_COUNT;
	public static boolean BBS_CLASS_MASTER_CANCEL_SUB_CLASS;
	public static long[] BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT;

	public static void loadCarrierSettings()
	{
		ExProperties career = load(COMMUNITY_CAREER);

		BBS_CLASS_MASTER_ALLOW = career.getProperty("Allow", false);

		for(int id : career.getProperty("Class", ArrayUtils.EMPTY_INT_ARRAY))
		{
			if(id != 0)
				BBS_CLASS_MASTERS_ALLOW_LIST.add(id);
		}

		BBS_CLASS_MASTER_PRICE_ITEM = career.getProperty("Item", new int[] { 57, 57, 57 });
		BBS_CLASS_MASTER_PRICE_COUNT = career.getProperty("Price", new long[] { 1000, 1000, 1000 });

		BBS_CLASS_MASTER_PRICE_SECOND_ITEM = career.getProperty("SecondItem", new int[] { 57, 57, 57 });
		BBS_CLASS_MASTER_PRICE_SECOND_COUNT = career.getProperty("SecondPrice", new long[] { 1000, 1000, 1000 });

		BBS_CLASS_MASTER_ADD_SUB_CLASS = career.getProperty("SubAdd", false);
		BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT = career.getProperty("SubAPrice", new long[] { 0, 57 });
		BBS_CLASS_MASTER_CHANGE_SUB_CLASS = career.getProperty("SubChange", false);
		BBS_CLASS_MASTER_SUB_CHANGE_PRICE_COUNT = career.getProperty("SubCPrice", new long[] { 0, 57 });
		BBS_CLASS_MASTER_CANCEL_SUB_CLASS = career.getProperty("SubCancel", false);
		BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT = career.getProperty("SubDPrice", new long[] { 0, 57 });
		BBS_CLASS_MASTER_BUY_NOBLESSE = career.getProperty("Noble", false);
	}

	public static int CLAN_LEVEL_CREATE;
	public static int CLAN_POINT_CREATE;

	public static int FORTRESS_BLOOD_OATH_COUNT;
	public static int FORTRESS_BLOOD_OATH_FRQ;
	public static int CLAN_PRICE_CREATE_ROYAL_SUB;
	public static int CLAN_PRICE_CREATE_KNIGHT_SUB;

	public static int ACADEMY_SUB_LIMIT;
	public static int ACADEMY_SUB_LIMIT_LEVEL11;
	public static int ROYAL_SUB_LIMIT_1;
	public static int ROYAL_SUB_LIMIT_2;
	public static int ROYAL_SUB_LIMIT_1_LEVEL11;
	public static int ROYAL_SUB_LIMIT_2_LEVEL11;
	public static int KNIGHT_SUB_LIMIT_1;
	public static int KNIGHT_SUB_LIMIT_2;
	public static int KNIGHT_SUB_LIMIT_3;
	public static int KNIGHT_SUB_LIMIT_4;
	public static int KNIGHT_SUB_LIMIT_1_LEVEL9;
	public static int KNIGHT_SUB_LIMIT_2_LEVEL9;
	public static int KNIGHT_SUB_LIMIT_3_LEVEL10;
	public static int KNIGHT_SUB_LIMIT_4_LEVEL10;

	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_1;
	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_2;
	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_3;
	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_4;
	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_5;
	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_6;
	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_7;
	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_8;
	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_9;
	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_10;
	public static int REQUIREMEN_COST_CLAN_LEVEL_UP_TO_11;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_1;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_2;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_3;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_4;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_5;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_6;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_7;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_8;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_9;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_10;
	public static int REQUIREMEN_CLAN_LEVEL_UP_TO_11;
	public static int MEMBER_CLAN_LEVEL_UP_TO_6;
	public static int MEMBER_CLAN_LEVEL_UP_TO_7;
	public static int MEMBER_CLAN_LEVEL_UP_TO_8;
	public static int MEMBER_CLAN_LEVEL_UP_TO_9;
	public static int MEMBER_CLAN_LEVEL_UP_TO_10;
	public static int MEMBER_CLAN_LEVEL_UP_TO_11;
	public static int CLAN_MAIN_LIMIT_LEVEL0;
	public static int CLAN_MAIN_LIMIT_LEVEL1;
	public static int CLAN_MAIN_LIMIT_LEVEL2;
	public static int CLAN_MAIN_LIMIT_LEVEL3;
	public static int CLAN_MAIN_LIMIT_LEVEL4;
	public static int CLAN_MAIN_LIMIT_LEVEL5;
	public static int CLAN_MAIN_LIMIT_LEVEL6;
	public static int CLAN_MAIN_LIMIT_LEVEL7;
	public static int CLAN_MAIN_LIMIT_LEVEL8;
	public static int CLAN_MAIN_LIMIT_LEVEL9;
	public static int CLAN_MAIN_LIMIT_LEVEL10;
	public static int CLAN_MAIN_LIMIT_LEVEL11;
	public static int EXPELLED_MEMBER_PENALTY;
	public static int LEAVED_ALLY_PENALTY;
	public static int DISSOLVED_ALLY_PENALTY;
	public static int DISSOLVED_CLAN_PENALTY;
	public static int CHANGE_LEADER;
	public static int DISBAND_CLAN;
	public static int DISBAND_PENALTY;
	public static int CREATE_CLAN_PENALTY;
	public static int MIN_EARNED_ACADEM_POINT;
	public static int MAX_EARNED_ACADEM_POINT;
	public static int[] KILL_WAR_CRP;

	public static void loadClanSettings()
	{
		ExProperties clan = load(CLAN);

		CLAN_LEVEL_CREATE = clan.getProperty("Level", 0);
		CLAN_POINT_CREATE = clan.getProperty("Point", 0);
		FORTRESS_BLOOD_OATH_COUNT = clan.getProperty("FortressBloodOathCount", 1);
		FORTRESS_BLOOD_OATH_FRQ = clan.getProperty("FortressBloodOathFrequency", 360);
		CLAN_PRICE_CREATE_ROYAL_SUB = clan.getProperty("ClanPriceRoyalSub", 5000);
		CLAN_PRICE_CREATE_KNIGHT_SUB = clan.getProperty("ClanPriceKnightSub", 10000);
		ACADEMY_SUB_LIMIT = clan.getProperty("AcademyLimit", 20);
		ACADEMY_SUB_LIMIT_LEVEL11 = clan.getProperty("AcademyLimit11", 30);
		ROYAL_SUB_LIMIT_1 = clan.getProperty("RoyalLimit1", 20);
		ROYAL_SUB_LIMIT_2 = clan.getProperty("RoyalLimit2", 20);
		ROYAL_SUB_LIMIT_1_LEVEL11 = clan.getProperty("RoyalLimit1Level11", 30);
		ROYAL_SUB_LIMIT_2_LEVEL11 = clan.getProperty("RoyalLimit2Level11", 30);
		KNIGHT_SUB_LIMIT_1 = clan.getProperty("KnightLimit1", 10);
		KNIGHT_SUB_LIMIT_2 = clan.getProperty("KnightLimit2", 10);
		KNIGHT_SUB_LIMIT_3 = clan.getProperty("KnightLimit3", 10);
		KNIGHT_SUB_LIMIT_4 = clan.getProperty("KnightLimit4", 10);
		KNIGHT_SUB_LIMIT_1_LEVEL9 = clan.getProperty("KnightLimit1Level9", 25);
		KNIGHT_SUB_LIMIT_2_LEVEL9 = clan.getProperty("KnightLimit2Level9", 25);
		KNIGHT_SUB_LIMIT_3_LEVEL10 = clan.getProperty("KnightLimit3Level10", 25);
		KNIGHT_SUB_LIMIT_4_LEVEL10 = clan.getProperty("KnightLimit4Level10", 25);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_1 = clan.getProperty("AdenaForLevelUpTo1", 650000);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_2 = clan.getProperty("AdenaForLevelUpTo2", 2500000);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_3 = clan.getProperty("BloodMarkForLevelUpTo3", 1);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_4 = clan.getProperty("AllianceManifestoForLevelUpTo4", 1);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_5 = clan.getProperty("SealOfAspirationForLevelUpTo5", 1);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_6 = clan.getProperty("ReputationScoreForLevelUpTo6", 5000);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_7 = clan.getProperty("ReputationScoreForLevelUpTo7", 10000);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_8 = clan.getProperty("ReputationScoreForLevelUpTo8", 20000);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_9 = clan.getProperty("ReputationScoreForLevelUpTo9", 40000);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_10 = clan.getProperty("ReputationScoreForLevelUpTo10", 40000);
		REQUIREMEN_COST_CLAN_LEVEL_UP_TO_11 = clan.getProperty("ReputationScoreForLevelUpTo11", 75000);
		REQUIREMEN_CLAN_LEVEL_UP_TO_1 = clan.getProperty("SPForLevelUpTo1", 20000);
		REQUIREMEN_CLAN_LEVEL_UP_TO_2 = clan.getProperty("SPForLevelUpTo2", 100000);
		REQUIREMEN_CLAN_LEVEL_UP_TO_3 = clan.getProperty("SPForLevelUpTo3", 350000);
		REQUIREMEN_CLAN_LEVEL_UP_TO_4 = clan.getProperty("SPForLevelUpTo4", 1000000);
		REQUIREMEN_CLAN_LEVEL_UP_TO_5 = clan.getProperty("SPForLevelUpTo5", 2500000);
		REQUIREMEN_CLAN_LEVEL_UP_TO_9 = clan.getProperty("BloodOathsForLevelUpTo9", 150);
		REQUIREMEN_CLAN_LEVEL_UP_TO_10 = clan.getProperty("BloodPledgesForLevelUpTo10", 5);
		MEMBER_CLAN_LEVEL_UP_TO_6 = clan.getProperty("MemberClanLevelUpTo6", 30);
		MEMBER_CLAN_LEVEL_UP_TO_7 = clan.getProperty("MemberClanLevelUpTo7", 50);
		MEMBER_CLAN_LEVEL_UP_TO_8 = clan.getProperty("MemberClanLevelUpTo8", 80);
		MEMBER_CLAN_LEVEL_UP_TO_9 = clan.getProperty("MemberClanLevelUpTo9", 120);
		MEMBER_CLAN_LEVEL_UP_TO_10 = clan.getProperty("MemberClanLevelUpTo10", 140);
		MEMBER_CLAN_LEVEL_UP_TO_11 = clan.getProperty("MemberClanLevelUpTo11", 170);
		CLAN_MAIN_LIMIT_LEVEL0 = clan.getProperty("ClanLimitInLevel0", 10);
		CLAN_MAIN_LIMIT_LEVEL1 = clan.getProperty("ClanLimitInLevel1", 15);
		CLAN_MAIN_LIMIT_LEVEL2 = clan.getProperty("ClanLimitInLevel2", 20);
		CLAN_MAIN_LIMIT_LEVEL3 = clan.getProperty("ClanLimitInLevel3", 30);
		CLAN_MAIN_LIMIT_LEVEL4 = clan.getProperty("ClanLimitInLevel4", 30);
		CLAN_MAIN_LIMIT_LEVEL5 = clan.getProperty("ClanLimitInLevel5", 40);
		CLAN_MAIN_LIMIT_LEVEL6 = clan.getProperty("ClanLimitInLevel6", 40);
		CLAN_MAIN_LIMIT_LEVEL7 = clan.getProperty("ClanLimitInLevel7", 40);
		CLAN_MAIN_LIMIT_LEVEL8 = clan.getProperty("ClanLimitInLevel8", 40);
		CLAN_MAIN_LIMIT_LEVEL9 = clan.getProperty("ClanLimitInLevel9", 40);
		CLAN_MAIN_LIMIT_LEVEL10 = clan.getProperty("ClanLimitInLevel10", 40);
		CLAN_MAIN_LIMIT_LEVEL11 = clan.getProperty("ClanLimitInLevel11", 40);
		EXPELLED_MEMBER_PENALTY = clan.getProperty("ExpelledMemberPenalty", 24);
		LEAVED_ALLY_PENALTY = clan.getProperty("LeavedAllyPenalty", 24);
		DISSOLVED_ALLY_PENALTY = clan.getProperty("DissolvedAllyPenalty", 24);
		DISSOLVED_CLAN_PENALTY = clan.getProperty("JoinPenalty", 24);
		CREATE_CLAN_PENALTY = clan.getProperty("CreatePenalty", 10);
		CHANGE_LEADER = clan.getProperty("ChangeLeader", 48);
		DISBAND_CLAN = clan.getProperty("DisbandTime", 48);
		DISBAND_PENALTY = clan.getProperty("DisbandPenalty", 7);
		MIN_EARNED_ACADEM_POINT = clan.getProperty("MinEarnedAcademPoint", 190);
		MAX_EARNED_ACADEM_POINT = clan.getProperty("MaxEarnedAcademPoint", 650);
		KILL_WAR_CRP = clan.getProperty("WarPoint", new int[] { 2, 1 });
	}

	public static boolean CREATE_CHAR_TITLE;
	public static String CREATE_ADD_CHAR_TITLE;
	public static int CREATE_START_LVL;
	public static int CREATE_START_SP;
	public static int CREATE_MAX_CHAR;

	public static void loadPlayerCreateSettings()
	{
		ExProperties create = load(PLAYER_CREATE);

		CREATE_CHAR_TITLE = create.getProperty("SetTitle", false);
		CREATE_ADD_CHAR_TITLE = create.getProperty("Title", "");
		CREATE_START_LVL = create.getProperty("Level", 0);
		CREATE_START_SP = create.getProperty("Sp", 0);
		CREATE_MAX_CHAR = create.getProperty("Characters", 7);
	}

	public static boolean ENTER_WORLD_ANNOUNCEMENTS_HERO_LOGIN;
	public static boolean ENTER_WORLD_ANNOUNCEMENTS_LORD_LOGIN;
	public static boolean ENTER_WORLD_SHOW_HTML_WELCOME;
	public static boolean ENTER_WORLD_SHOW_HTML_LOCK;
	public static boolean ENTER_WORLD_SHOW_HTML_PREMIUM_BUY;
	public static boolean ENTER_WORLD_SHOW_HTML_PREMIUM_DONE;
	public static boolean ENTER_WORLD_SHOW_HTML_PREMIUM_ACTIVE;

	public static void loadEnterWorldSettings()
	{
		ExProperties enter = load(ENTER_WORLD);

		ENTER_WORLD_ANNOUNCEMENTS_HERO_LOGIN = enter.getProperty("Hero", false);
		ENTER_WORLD_ANNOUNCEMENTS_LORD_LOGIN = enter.getProperty("Lord", false);
		ENTER_WORLD_SHOW_HTML_WELCOME = enter.getProperty("WelcomeHTML", false);
		ENTER_WORLD_SHOW_HTML_PREMIUM_BUY = enter.getProperty("PremiumHTML", false);
		ENTER_WORLD_SHOW_HTML_LOCK = enter.getProperty("LockHTML", false);
		ENTER_WORLD_SHOW_HTML_PREMIUM_DONE = enter.getProperty("PremiumDone", false);
		ENTER_WORLD_SHOW_HTML_PREMIUM_ACTIVE = enter.getProperty("PremiumInfo", false);
	}

	public static boolean USE_OFFLIKE_ENCHANT;
	public static boolean USE_OFFLIKE_ENCHANT_MAGE_WEAPON;
	public static double USE_OFFLIKE_ENCHANT_MAGE_WEAPON_CHANCE;
	public static int[] OFFLIKE_ENCHANT_WEAPON;
	public static int[] OFFLIKE_ENCHANT_WEAPON_BLESSED;
	public static int[] OFFLIKE_ENCHANT_WEAPON_CRYSTAL;
	public static int[] OFFLIKE_ENCHANT_ARMOR;
	public static int[] OFFLIKE_ENCHANT_ARMOR_CRYSTAL;
	public static int[] OFFLIKE_ENCHANT_ARMOR_BLESSED;
	public static int[] OFFLIKE_ENCHANT_ARMOR_JEWELRY;
	public static int[] OFFLIKE_ENCHANT_ARMOR_JEWELRY_CRYSTAL;
	public static int[] OFFLIKE_ENCHANT_ARMOR_JEWELRY_BLESSED;

	public static int[] OFFLIKE_PREMIUM_ENCHANT_WEAPON;
	public static int[] OFFLIKE_PREMIUM_ENCHANT_WEAPON_BLESSED;
	public static int[] OFFLIKE_PREMIUM_ENCHANT_WEAPON_CRYSTAL;
	public static int[] OFFLIKE_PREMIUM_ENCHANT_ARMOR;
	public static int[] OFFLIKE_PREMIUM_ENCHANT_ARMOR_CRYSTAL;
	public static int[] OFFLIKE_PREMIUM_ENCHANT_ARMOR_BLESSED;
	public static int[] OFFLIKE_PREMIUM_ENCHANT_ARMOR_JEWELRY;
	public static int[] OFFLIKE_PREMIUM_ENCHANT_ARMOR_JEWELRY_CRYSTAL;
	public static int[] OFFLIKE_PREMIUM_ENCHANT_ARMOR_JEWELRY_BLESSED;

	public static int ENCHANT_CHANCE_WEAPON;
	public static int PREMIUM_ENCHANT_CHANCE_WEAPON;
	public static int ENCHANT_CHANCE_ARMOR;
	public static int PREMIUM_ENCHANT_CHANCE_ARMOR;
	public static int ENCHANT_CHANCE_ACCESSORY;
	public static int PREMIUM_ENCHANT_CHANCE_ACCESSORY;
	public static int ENCHANT_CHANCE_CRYSTAL_WEAPON;
	public static int PREMIUM_ENCHANT_CHANCE_CRYSTAL_WEAPON;
	public static int ENCHANT_CHANCE_CRYSTAL_ARMOR;
	public static int PREMIUM_ENCHANT_CHANCE_CRYSTAL_ARMOR;
	public static int ENCHANT_CHANCE_CRYSTAL_ACCESSORY;
	public static int PREMIUM_ENCHANT_CHANCE_CRYSTAL_ACCESSORY;
	public static int ENCHANT_MAX_WEAPON;
	public static int ENCHANT_MAX_SHIELD_ARMOR;
	public static int ENCHANT_MAX_ACCESSORY;
	public static int ENCHANT_CRYSTAL_FAILED;
	public static int ENCHANT_CRYSTAL_FAILED_PA;
	public static int ENCHANT_SCROLL_LEVEL_WEAPON;
	public static int ENCHANT_SCROLL_LEVEL_ARMOR;
	public static int ENCHANT_SCROLL_LEVEL_ACCESSORY;
	public static int ARMOR_OVERENCHANT_HPBONUS_LIMIT;
	public static boolean SHOW_ENCHANT_EFFECT_RESULT;
	public static boolean SHOW_ENCHANT_RESULT_UP_3;
	public static boolean ENCHANT_CHEATER_SYSTEM;
	public static int ENCHANT_CHEATER_MAX_COUNT;
	public static int ENCHANT_CHEATER_PUNISHMENT;
	public static int ENCHANT_MIN_SECOND;
	public static int ENCHANT_ATTRIBUTE_STONE_CHANCE_ARMOR;
	public static int ENCHANT_ATTRIBUTE_STONE_CHANCE_MAGE_WEAPON;
	public static int ENCHANT_ATTRIBUTE_STONE_CHANCE_FIGHTER_WEAPON;
	public static int ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE_ARMOR;
	public static int ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE_MAGE_WEAPON;
	public static int ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE_FIGHTER_WEAPON;
	public static int[] ENCHANT_ATTRIBUTE_VALUE;
	public static int[] ENCHANT_ATTRIBUTE_STONE;
	public static int[] ENCHANT_ATTRIBUTE_CRYSTAL;
	public static boolean ENCHANT_LOCK_ALL;

	public static void loadEnchantSettings()
	{
		ExProperties enchant = load(ENCHANT);

		USE_OFFLIKE_ENCHANT = enchant.getProperty("Offlike", false);
		USE_OFFLIKE_ENCHANT_MAGE_WEAPON = enchant.getProperty("OffMage", false);
		USE_OFFLIKE_ENCHANT_MAGE_WEAPON_CHANCE = enchant.getProperty("OffMChance", 0.6667);

		OFFLIKE_ENCHANT_WEAPON = enchant.getProperty("OffWeapon", new int[] { 0 });
		OFFLIKE_ENCHANT_WEAPON_BLESSED = enchant.getProperty("OffCWeapon", new int[] { 0 });
		OFFLIKE_ENCHANT_WEAPON_CRYSTAL = enchant.getProperty("OffBWeapon", new int[] { 0 });
		OFFLIKE_ENCHANT_ARMOR = enchant.getProperty("OffArmor", new int[] { 0 });
		OFFLIKE_ENCHANT_ARMOR_CRYSTAL = enchant.getProperty("OffCArmor", new int[] { 0 });
		OFFLIKE_ENCHANT_ARMOR_BLESSED = enchant.getProperty("OffBArmor", new int[] { 0 });
		OFFLIKE_ENCHANT_ARMOR_JEWELRY = enchant.getProperty("OffJewelry", new int[] { 0 });
		OFFLIKE_ENCHANT_ARMOR_JEWELRY_CRYSTAL = enchant.getProperty("OffCJewelry", new int[] { 0 });
		OFFLIKE_ENCHANT_ARMOR_JEWELRY_BLESSED = enchant.getProperty("OffBJewelry", new int[] { 0 });

		OFFLIKE_PREMIUM_ENCHANT_WEAPON = enchant.getProperty("OffPWeapon", new int[] { 0 });
		OFFLIKE_PREMIUM_ENCHANT_WEAPON_BLESSED = enchant.getProperty("OffPCWeapon", new int[] { 0 });
		OFFLIKE_PREMIUM_ENCHANT_WEAPON_CRYSTAL = enchant.getProperty("OffPBWeapon", new int[] { 0 });
		OFFLIKE_PREMIUM_ENCHANT_ARMOR = enchant.getProperty("OffPArmor", new int[] { 0 });
		OFFLIKE_PREMIUM_ENCHANT_ARMOR_CRYSTAL = enchant.getProperty("OffPCArmor", new int[] { 0 });
		OFFLIKE_PREMIUM_ENCHANT_ARMOR_BLESSED = enchant.getProperty("OffPBArmor", new int[] { 0 });
		OFFLIKE_PREMIUM_ENCHANT_ARMOR_JEWELRY = enchant.getProperty("OffPJewelry", new int[] { 0 });
		OFFLIKE_PREMIUM_ENCHANT_ARMOR_JEWELRY_CRYSTAL = enchant.getProperty("OffPCJewelry", new int[] { 0 });
		OFFLIKE_PREMIUM_ENCHANT_ARMOR_JEWELRY_BLESSED = enchant.getProperty("OffPBJewelry", new int[] { 0 });

		ENCHANT_CHANCE_WEAPON = enchant.getProperty("WChance", 66);
		PREMIUM_ENCHANT_CHANCE_WEAPON = enchant.getProperty("PWChance", 76);
		ENCHANT_CHANCE_CRYSTAL_WEAPON = enchant.getProperty("BWChance", 76);
		PREMIUM_ENCHANT_CHANCE_CRYSTAL_WEAPON = enchant.getProperty("BPWChance", 86);

		ENCHANT_CHANCE_ARMOR = enchant.getProperty("AChance", 66);
		PREMIUM_ENCHANT_CHANCE_ARMOR = enchant.getProperty("PAChance", 76);
		ENCHANT_CHANCE_CRYSTAL_ARMOR = enchant.getProperty("BAChance", 76);
		PREMIUM_ENCHANT_CHANCE_CRYSTAL_ARMOR = enchant.getProperty("BPAChance", 86);

		ENCHANT_CHANCE_ACCESSORY = enchant.getProperty("JChance", 66);
		PREMIUM_ENCHANT_CHANCE_ACCESSORY = enchant.getProperty("PJChance", 76);
		ENCHANT_CHANCE_CRYSTAL_ACCESSORY = enchant.getProperty("BJChance", 76);
		PREMIUM_ENCHANT_CHANCE_CRYSTAL_ACCESSORY = enchant.getProperty("BPJChance", 86);

		ENCHANT_LOCK_ALL = enchant.getProperty("LockAll", false);
		SAFE_ENCHANT_COMMON = enchant.getProperty("Safe", 3);
		SAFE_ENCHANT_FULL_BODY = enchant.getProperty("FBSafe", 4);

		ENCHANT_MAX_WEAPON = enchant.getProperty("MaxWeapon", 20);
		ENCHANT_MAX_SHIELD_ARMOR = enchant.getProperty("MaxArmor", 20);
		ENCHANT_MAX_ACCESSORY = enchant.getProperty("MaxAccessory", 20);

		ENCHANT_CRYSTAL_FAILED = enchant.getProperty("BFailed", 0);
		ENCHANT_CRYSTAL_FAILED_PA = enchant.getProperty("PremiumBFailed", 3);

		ENCHANT_SCROLL_LEVEL_WEAPON = enchant.getProperty("WLevel", 1);
		ENCHANT_SCROLL_LEVEL_ARMOR = enchant.getProperty("ALevel", 1);
		ENCHANT_SCROLL_LEVEL_ACCESSORY = enchant.getProperty("JLevel", 1);

		ARMOR_OVERENCHANT_HPBONUS_LIMIT = enchant.getProperty("HPBonus", 10) - 3;

		SHOW_ENCHANT_EFFECT_RESULT = enchant.getProperty("EffectResult", false);

		ENCHANT_ATTRIBUTE_STONE_CHANCE_ARMOR = enchant.getProperty("AAttChance", 70);
		ENCHANT_ATTRIBUTE_STONE_CHANCE_MAGE_WEAPON = enchant.getProperty("MAttChance", 40);
		ENCHANT_ATTRIBUTE_STONE_CHANCE_FIGHTER_WEAPON = enchant.getProperty("FAttChance", 50);

		ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE_ARMOR = enchant.getProperty("CAAttChance", 70);
		ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE_MAGE_WEAPON = enchant.getProperty("CMAttChance", 40);
		ENCHANT_ATTRIBUTE_CRYSTAL_CHANCE_FIGHTER_WEAPON = enchant.getProperty("CFAttChance", 5);
		ENCHANT_ATTRIBUTE_VALUE = enchant.getProperty("AttributeValue", new int[] { 5, 6 });
		ENCHANT_ATTRIBUTE_STONE = enchant.getProperty("AttributeStone", new int[] { 150, 60 });
		ENCHANT_ATTRIBUTE_CRYSTAL = enchant.getProperty("AttributeCrystal", new int[] { 150, 60 });
		ENCHANT_CHEATER_SYSTEM = enchant.getProperty("CheaterSystem", false);
		ENCHANT_CHEATER_MAX_COUNT = enchant.getProperty("CheatMaxCount", 10);
		ENCHANT_CHEATER_PUNISHMENT = enchant.getProperty("Punishment", -1);
		ENCHANT_MIN_SECOND = enchant.getProperty("Seconds", 3);
	}

	public static void loadServerConfig()
	{
		ExProperties serverSettings = load(CONFIGURATION_FILE);

		GAME_SERVER_LOGIN_HOST = serverSettings.getProperty("LoginHost", "127.0.0.1");
		GAME_SERVER_LOGIN_PORT = serverSettings.getProperty("LoginPort", 9013);
		GAME_SERVER_LOGIN_DATABASE_NAME = serverSettings.getProperty("LoginDatabase", "");

		AUTH_SERVER_AGE_LIMIT = serverSettings.getProperty("ServerAgeLimit", 0);
		AUTH_SERVER_GM_ONLY = serverSettings.getProperty("ServerGMOnly", false);
		AUTH_SERVER_BRACKETS = serverSettings.getProperty("ServerBrackets", false);
		AUTH_SERVER_IS_PVP = serverSettings.getProperty("PvPServer", false);
		for(String a : serverSettings.getProperty("ServerType", ArrayUtils.EMPTY_STRING_ARRAY))
		{
			if(a.trim().isEmpty())
				continue;

			ServerType t = ServerType.valueOf(a.toUpperCase());
			AUTH_SERVER_SERVER_TYPE |= t.getMask();
		}

		INTERNAL_HOSTNAME = serverSettings.getProperty("InternalHostname", "*");
		EXTERNAL_HOSTNAME = serverSettings.getProperty("ExternalHostname", "*");

		REQUEST_ID = serverSettings.getProperty("RequestServerID", 0);
		IPCONFIG_ENABLE = serverSettings.getProperty("IpConfigEnable", false);
		ACCEPT_ALTERNATE_ID = serverSettings.getProperty("AcceptAlternateID", true);

		GAMESERVER_HOSTNAME = serverSettings.getProperty("GameserverHostname");
		PORTS_GAME = serverSettings.getProperty("GameserverPort", new int[] { 7777 });

		EVERYBODY_HAS_ADMIN_RIGHTS = serverSettings.getProperty("EverybodyHasAdminRights", false);

		SAVE_GM_EFFECTS = serverSettings.getProperty("SaveGMEffects", false);

		CNAME_TEMPLATE = serverSettings.getProperty("CnameTemplate", "[A-Za-z0-9\u0410-\u042f\u0430-\u044f]{2,16}");
		CLAN_NAME_TEMPLATE = serverSettings.getProperty("ClanNameTemplate", "[A-Za-z0-9\u0410-\u042f\u0430-\u044f]{3,16}");
		CLAN_TITLE_TEMPLATE = serverSettings.getProperty("ClanTitleTemplate", "[A-Za-z0-9\u0410-\u042f\u0430-\u044f \\p{Punct}]{1,16}");
		ALLY_NAME_TEMPLATE = serverSettings.getProperty("AllyNameTemplate", "[A-Za-z0-9\u0410-\u042f\u0430-\u044f]{3,16}");

		GLOBAL_SHOUT = serverSettings.getProperty("GlobalShout", false);
		GLOBAL_TRADE_CHAT = serverSettings.getProperty("GlobalTradeChat", false);
		CHAT_RANGE = serverSettings.getProperty("ChatRange", 1250);
		SHOUT_SQUARE_OFFSET = serverSettings.getProperty("ShoutOffset", 0);
		SHOUT_SQUARE_OFFSET = SHOUT_SQUARE_OFFSET * SHOUT_SQUARE_OFFSET;

		RATE_RAID_REGEN = serverSettings.getProperty("RateRaidRegen", 1.);
		RATE_RAID_DEFENSE = serverSettings.getProperty("RateRaidDefense", 1.);
		RATE_RAID_ATTACK = serverSettings.getProperty("RateRaidAttack", 1.);
		RATE_EPIC_DEFENSE = serverSettings.getProperty("RateEpicDefense", RATE_RAID_DEFENSE);
		RATE_EPIC_ATTACK = serverSettings.getProperty("RateEpicAttack", RATE_RAID_ATTACK);
		RAID_MAX_LEVEL_DIFF = serverSettings.getProperty("RaidMaxLevelDiff", 8);
		PARALIZE_ON_RAID_DIFF = serverSettings.getProperty("ParalizeOnRaidLevelDiff", true);

		AUTODESTROY_ITEM_AFTER = serverSettings.getProperty("AutoDestroyDroppedItemAfter", 600); // 10 min
		AUTODESTROY_PLAYER_ITEM_AFTER = serverSettings.getProperty("AutoDestroyPlayerDroppedItemAfter", 600); // 10 min
		DELETE_DAYS = serverSettings.getProperty("DeleteCharAfterDays", 7);

		try
		{
			DATAPACK_ROOT = new File(serverSettings.getProperty("DatapackRoot", ".")).getCanonicalFile();
		}
		catch(IOException e)
		{
			throw new Error(e);
		}

		ALLOW_DISCARDITEM = serverSettings.getProperty("AllowDiscardItem", true);
		ALLOW_MAIL = serverSettings.getProperty("AllowMail", true);
		ALLOW_WAREHOUSE = serverSettings.getProperty("AllowWarehouse", true);
		ALLOW_WATER = serverSettings.getProperty("AllowWater", true);
		ALLOW_CURSED_WEAPONS = serverSettings.getProperty("AllowCursedWeapons", false);
		DROP_CURSED_WEAPONS_ON_KICK = serverSettings.getProperty("DropCursedWeaponsOnKick", false);

		MIN_PROTOCOL_REVISION = serverSettings.getProperty("MinProtocolRevision", 267);
		MAX_PROTOCOL_REVISION = serverSettings.getProperty("MaxProtocolRevision", 271);

		MIN_NPC_ANIMATION = serverSettings.getProperty("MinNPCAnimation", 5);
		MAX_NPC_ANIMATION = serverSettings.getProperty("MaxNPCAnimation", 90);

		SERVER_SIDE_NPC_NAME = serverSettings.getProperty("ServerSideNpcName", false);
		SERVER_SIDE_NPC_TITLE = serverSettings.getProperty("ServerSideNpcTitle", false);
		SERVER_SIDE_NPC_TITLE_AGGRO = serverSettings.getProperty("TitleAggro", false);
		SERVER_SIDE_NPC_TITLE_LEVEL = serverSettings.getProperty("TitleLevel", false);

		AUTOSAVE = serverSettings.getProperty("Autosave", true);

		MAXIMUM_ONLINE_USERS = serverSettings.getProperty("MaximumOnlineUsers", 3000);

		DATABASE_DRIVER = serverSettings.getProperty("Driver", "com.mysql.jdbc.Driver");
		DATABASE_MAX_CONNECTIONS = serverSettings.getProperty("MaximumDbConnections", 10);
		DATABASE_MAX_IDLE_TIMEOUT = serverSettings.getProperty("MaxIdleConnectionTimeout", 600);
		DATABASE_IDLE_TEST_PERIOD = serverSettings.getProperty("IdleConnectionTestPeriod", 60);

		DATABASE_URL = serverSettings.getProperty("URL", "jdbc:mysql://localhost/l2jdb");
		DATABASE_LOGIN_URL = serverSettings.getProperty("LoginURL", "jdbc:mysql://localhost/l2jdb");
		DATABASE_LOGIN = serverSettings.getProperty("Login", "root");
		DATABASE_PASSWORD = serverSettings.getProperty("Password", "");

		USER_INFO_INTERVAL = serverSettings.getProperty("UserInfoInterval", 100L);
		BROADCAST_CHAR_INFO_INTERVAL = serverSettings.getProperty("BroadcastCharInfoInterval", 100L);

		EFFECT_TASK_MANAGER_COUNT = serverSettings.getProperty("EffectTaskManagers", 2);
		if(!isPowerOfTwo(EFFECT_TASK_MANAGER_COUNT))
			throw new RuntimeException("EffectTaskManagers value should be power of 2!");

		SCHEDULED_THREAD_POOL_SIZE = serverSettings.getProperty("ScheduledThreadPoolSize", NCPUS * 4);
		EXECUTOR_THREAD_POOL_SIZE = serverSettings.getProperty("ExecutorThreadPoolSize", NCPUS * 2);

		ENABLE_RUNNABLE_STATS = serverSettings.getProperty("EnableRunnableStats", false);

		SELECTOR_CONFIG.SLEEP_TIME = serverSettings.getProperty("SelectorSleepTime", 10L);
		SELECTOR_CONFIG.INTEREST_DELAY = serverSettings.getProperty("InterestDelay", 30L);
		SELECTOR_CONFIG.MAX_SEND_PER_PASS = serverSettings.getProperty("MaxSendPerPass", 32);
		SELECTOR_CONFIG.READ_BUFFER_SIZE = serverSettings.getProperty("ReadBufferSize", 65536);
		SELECTOR_CONFIG.WRITE_BUFFER_SIZE = serverSettings.getProperty("WriteBufferSize", 131072);
		SELECTOR_CONFIG.HELPER_BUFFER_COUNT = serverSettings.getProperty("BufferPoolSize", 64);

		CHAT_MESSAGE_MAX_LEN = serverSettings.getProperty("ChatMessageLimit", 1000);

		StringTokenizer st = new StringTokenizer(serverSettings.getProperty("ChatBanChannels", "ALL,SHOUT,TELL,TRADE,HERO_VOICE,BATTLEFIELD"), ",");
		List<ChatType> channels = new ArrayList<ChatType>();
		while(st.hasMoreTokens())
			channels.add(ChatType.valueOf(st.nextToken()));
		BAN_CHANNEL_LIST = channels.toArray(new ChatType[channels.size()]);

		BANCHAT_ANNOUNCE = serverSettings.getProperty("BANCHAT_ANNOUNCE", true);
		BANCHAT_ANNOUNCE_FOR_ALL_WORLD = serverSettings.getProperty("BANCHAT_ANNOUNCE_FOR_ALL_WORLD", true);
		BANCHAT_ANNOUNCE_NICK = serverSettings.getProperty("BANCHAT_ANNOUNCE_NICK", true);

		RESTART_AT_TIME = serverSettings.getProperty("AutoRestartAt", "0 5 * * *");

		SHIFT_BY = serverSettings.getProperty("HShift", 12);
		SHIFT_BY_Z = serverSettings.getProperty("VShift", 11);
		MAP_MIN_Z = serverSettings.getProperty("MapMinZ", -32768);
		MAP_MAX_Z = serverSettings.getProperty("MapMaxZ", 32767);

		MOVE_PACKET_DELAY = serverSettings.getProperty("MovePacketDelay", 100);
		ATTACK_PACKET_DELAY = serverSettings.getProperty("AttackPacketDelay", 200);
		ATTACK_END_DELAY = serverSettings.getProperty("AttackEndDelay", 50);

		DAMAGE_FROM_FALLING = serverSettings.getProperty("DamageFromFalling", true);

		DONTLOADSPAWN = serverSettings.getProperty("StartWithoutSpawn", false);
		DONTLOADQUEST = serverSettings.getProperty("StartWithoutQuest", false);

		MAX_REFLECTIONS_COUNT = serverSettings.getProperty("MaxReflectionsCount", 300);

		WEAR_DELAY = serverSettings.getProperty("WearDelay", 5);
		HTM_CACHE_MODE = serverSettings.getProperty("HtmCacheMode", HtmCache.LAZY);
		ITEM_CACHE = serverSettings.getProperty("ItemCache", true);
		WEB_SERVER_DELAY = serverSettings.getProperty("WebServerDelay", 10) * 1000;
		WEB_SERVER_ROOT = serverSettings.getProperty("WebServerRoot", "./webserver/");
	}

	public static double RATE_CLAN_REP_SCORE;
	public static int RATE_CLAN_REP_SCORE_MAX_AFFECTED;
	public static boolean RATE_MULTI_DROP;
	public static double RATE_CHANCE;
	public static double RATE_GROUP_CHANCE;
	public static boolean RATE_RAID_CHANCE;
	public static double RATE_DROP_SIEGE_GUARD;
	public static double RATE_DROP_ENERGY_SEED;
	public static double RATE_MANOR;
	public static double RATE_FISH_DROP_COUNT;
	public static boolean RATE_PARTY_MIN;
	public static double RATE_HELLBOUND_CONFIDENCE;
	public static double RATE_SPOIL_CHANCE;
	public static boolean USE_NEW_DROP;

	public static double RATE_MOB_SPAWN;
	public static int RATE_MOB_SPAWN_MIN_LEVEL;
	public static int RATE_MOB_SPAWN_MAX_LEVEL;

	public static boolean RATE_ITEMS_CHANCE;
	public static String[] RATE_ITEMS_CHANCE_LIST;
	public static int RATE_ADENA_GROUP_CHANCE;

	public static int[] NO_RATE_ITEMS;
	public static boolean NO_RATE_EQUIPMENT;
	public static boolean NO_RATE_KEY_MATERIAL;
	public static boolean NO_RATE_RECIPES;
	public static boolean NO_RATE_ATTRIBUTE;
	public static boolean NO_RATE_LIFE_STONE;
	public static boolean NO_RATE_ENCHANT_SCROLL;
	public static boolean NO_RATE_FORGOTTEN_SCROLL;

	public static void loadRateSettings()
	{
		ExProperties rate = load(RATE);

		RATE_CLAN_REP_SCORE = rate.getProperty("ClanRep", 1.);
		RATE_CLAN_REP_SCORE_MAX_AFFECTED = rate.getProperty("ClanAffected", 2);
		RATE_CHANCE = rate.getProperty("IChance", 1.);
		RATE_GROUP_CHANCE = rate.getProperty("DChance", 1.);
		RATE_RAID_CHANCE = rate.getProperty("RaidChance", false);
		RATE_MULTI_DROP = rate.getProperty("MultiDrop", false);
		RATE_DROP_SIEGE_GUARD = rate.getProperty("Siege", 1.);
		RATE_DROP_ENERGY_SEED = rate.getProperty("Energy", 1.);
		RATE_MANOR = rate.getProperty("Manor", 1.);
		RATE_FISH_DROP_COUNT = rate.getProperty("Fish", 1.);
		RATE_PARTY_MIN = rate.getProperty("Party", false);
		RATE_HELLBOUND_CONFIDENCE = rate.getProperty("Hellbound", 1.);
		RATE_SPOIL_CHANCE = rate.getProperty("Spoil", 1.);

		USE_NEW_DROP = rate.getProperty("NewDrop", false);
		RATE_MOB_SPAWN = rate.getProperty("Spawn", 1.);
		RATE_MOB_SPAWN_MIN_LEVEL = rate.getProperty("MobMin", 1);
		RATE_MOB_SPAWN_MAX_LEVEL = rate.getProperty("MobMax", 100);
		RATE_ITEMS_CHANCE = rate.getProperty("Chances", false);
		RATE_ITEMS_CHANCE_LIST = rate.getProperty("ChanceMod", "0:0").trim().split(";");
		RATE_ADENA_GROUP_CHANCE = rate.getProperty("AdenaGroup", 70);

		NO_RATE_ITEMS = rate.getProperty("NoRateItemIds", new int[] {
				6660,
				6662,
				6661,
				6659,
				6656,
				6658,
				8191,
				6657,
				10170,
				10314,
				16025,
				16026,
				16027,
				10296 });
		NO_RATE_EQUIPMENT = rate.getProperty("NoRateEquipment", true);
		NO_RATE_KEY_MATERIAL = rate.getProperty("NoRateKeyMaterial", true);
		NO_RATE_RECIPES = rate.getProperty("NoRateRecipes", true);
		NO_RATE_ATTRIBUTE = rate.getProperty("NoRateAttribute", true);
		NO_RATE_LIFE_STONE = rate.getProperty("NoRateLifeStone", true);
		NO_RATE_ENCHANT_SCROLL = rate.getProperty("NoRateEnchantScroll", true);
		NO_RATE_FORGOTTEN_SCROLL = rate.getProperty("NoRateForgottenScroll", true);
	}

	public static int RESPAWN_ANTHARAS;
	public static int WHAIT_ANTHARAS;
	public static int RANDOM_ANTHARAS;
	public static int ANTHARAS_MINIONS_NUMBER;
	public static int ANTHARAS_INSIDE_LIMIT;
	public static double ANTHARAS_MINIONS_SPAWN_CHANCE;
	public static double ANTHARAS_MINIONS_TARASKA_CHANCE;
	public static int RESPAWN_VALAKAS;
	public static int WHAIT_VALAKAS;
	public static int RANDOM_VALAKAS;
	public static double VALAKAS_MINIONS_SPAWN_CHANCE;
	public static int VALAKAS_MINIONS_NUMBER;
	public static int VALAKAS_INSIDE_LIMIT;
	public static int RESPAWN_BAIUM;
	public static int RANDOM_BAIUM;
	public static int RESPAWN_BELETH;
	public static int BELETH_MIN_PLAYERS;
	public static double RAID_RESPAWN_MULTIPLIER;

	public static boolean DRAGON_VORTEX_RAID_LIKE_OFF;
	public static int DRAGON_VORTEX_RAID_TIME;
	public static int DRAGON_VORTEX_CHANCE_EMERALD_HORN;
	public static int DRAGON_VORTEX_CHANCE_DUST_RIDER;
	public static int DRAGON_VORTEX_CHANCE_BLEEDING_FLY;
	public static int DRAGON_VORTEX_CHANCE_BLACKDAGGER_WING;
	public static int DRAGON_VORTEX_CHANCE_SPIKE_SLASHER;
	public static int DRAGON_VORTEX_CHANCE_SHADOW_SUMMONER;
	public static int DRAGON_VORTEX_CHANCE_MUSCLE_BOMBER;

	public static void loadRaidSettings()
	{
		ExProperties raid = load(RAID);
		RAID_RESPAWN_MULTIPLIER = raid.getProperty("Respawn", 1.0);
		ANTHARAS_MINIONS_NUMBER = raid.getProperty("AntharasMinions", 30);
		ANTHARAS_INSIDE_LIMIT = raid.getProperty("AntharasLimit", 200);
		RESPAWN_ANTHARAS = raid.getProperty("Antharas", 11);
		WHAIT_ANTHARAS = raid.getProperty("WhaitAntharas", 15);
		RANDOM_ANTHARAS = raid.getProperty("RandomAntharas", 8);
		ANTHARAS_MINIONS_SPAWN_CHANCE = raid.getProperty("AMinionsChance", 5);
		ANTHARAS_MINIONS_TARASKA_CHANCE = raid.getProperty("Taraska", 50);
		RESPAWN_VALAKAS = raid.getProperty("Valakas", 11);
		WHAIT_VALAKAS = raid.getProperty("WhaitValakas", 25);
		RANDOM_VALAKAS = raid.getProperty("RandomValakas", 8);
		VALAKAS_MINIONS_NUMBER = raid.getProperty("ValakasMinions", 36);
		VALAKAS_INSIDE_LIMIT = raid.getProperty("ValakasLimit", 200);
		VALAKAS_MINIONS_SPAWN_CHANCE = raid.getProperty("VMinionsChance", 5);
		RESPAWN_BAIUM = raid.getProperty("Baium", 5);
		RANDOM_BAIUM = raid.getProperty("RandomBaium", 8);
		RESPAWN_BELETH = raid.getProperty("Beleth", 2);
		BELETH_MIN_PLAYERS = raid.getProperty("BelethPlayers", 50);
		DRAGON_VORTEX_RAID_LIKE_OFF = raid.getProperty("Vortex", true);
		DRAGON_VORTEX_RAID_TIME = raid.getProperty("VortexReuse", 30);
		DRAGON_VORTEX_CHANCE_EMERALD_HORN = raid.getProperty("EmeraldHorn", 72);
		DRAGON_VORTEX_CHANCE_DUST_RIDER = raid.getProperty("DustRider", 67);
		DRAGON_VORTEX_CHANCE_BLEEDING_FLY = raid.getProperty("BleedingFly", 45);
		DRAGON_VORTEX_CHANCE_BLACKDAGGER_WING = raid.getProperty("BlackdaggerWing", 25);
		DRAGON_VORTEX_CHANCE_SPIKE_SLASHER = raid.getProperty("SpikeSlasher", 15);
		DRAGON_VORTEX_CHANCE_SHADOW_SUMMONER = raid.getProperty("ShadowSummoner", 8);
		DRAGON_VORTEX_CHANCE_MUSCLE_BOMBER = raid.getProperty("MuscleBomber", 3);
	}

	public static int PREMIUM_ACCOUNT_TYPE;
	public static int[] PREMIUM_ACCOUNT_PARTY_GIFT_DATA;

	public static void loadPremiumSettings()
	{
		ExProperties premium = load(PREMIUM);
		PREMIUM_ACCOUNT_TYPE = premium.getProperty("Type", 1);
		PREMIUM_ACCOUNT_PARTY_GIFT_DATA = premium.getProperty("PartyGift", new int[] { 1, 1 });
	}

	public static void loadServicesBonusSettings()
	{
		ExProperties bonus = load(SERVICE_BONUS_CONFIG_FILE);

		BONUS_SERVICE_ENABLE = bonus.getProperty("BonusEnabled", false);
		BONUS_SERVICE_CLAN_REWARD = bonus.getProperty("ClanBonus", new long[] { 57, 1000 });
		BONUS_SERVICE_CLAN_MIN_LEVEL = bonus.getProperty("ClanMin", 20);
		BONUS_SERVICE_CLAN_LIST = bonus.getProperty("ClanList", new String[] { "Clan" });
		BONUS_SERVICE_PARTY_REWARD = bonus.getProperty("PartyBonus", new long[] { 57, 1000 });
		BONUS_SERVICE_PARTY_MIN_LEVEL = bonus.getProperty("PartyMin", 20);
		BONUS_SERVICE_PARTY_LIST = bonus.getProperty("PartyList", new String[] { "Party" });
	}

	public static void loadTelnetConfig()
	{
		ExProperties telnetSettings = load(TELNET_CONFIGURATION_FILE);

		IS_TELNET_ENABLED = telnetSettings.getProperty("EnableTelnet", false);
		TELNET_DEFAULT_ENCODING = telnetSettings.getProperty("TelnetEncoding", "UTF-8");
		TELNET_PORT = telnetSettings.getProperty("Port", 7000);
		TELNET_HOSTNAME = telnetSettings.getProperty("BindAddress", "127.0.0.1");
		TELNET_PASSWORD = telnetSettings.getProperty("Password", "");
	}

	public static void loadResidenceConfig()
	{
		ExProperties residenceSettings = load(RESIDENCE_CONFIG_FILE);

		RESIDENCE_LEASE_FUNC_MULTIPLIER = residenceSettings.getProperty("ResidenceLeaseFuncMultiplier", 1.);
		CASTLE_SELECT_HOURS = residenceSettings.getProperty("CastleSelectHours", new int[] { 16, 20 });
		int[] tempCastleValidatonTime = residenceSettings.getProperty("CastleValidationDate", new int[] { 2, 4, 2003 });
		CASTLE_VALIDATION_DATE = Calendar.getInstance();
		CASTLE_VALIDATION_DATE.set(Calendar.DAY_OF_MONTH, tempCastleValidatonTime[0]);
		CASTLE_VALIDATION_DATE.set(Calendar.MONTH, tempCastleValidatonTime[1] - 1);
		CASTLE_VALIDATION_DATE.set(Calendar.YEAR, tempCastleValidatonTime[2]);
		CASTLE_VALIDATION_DATE.set(Calendar.HOUR_OF_DAY, 0);
		CASTLE_VALIDATION_DATE.set(Calendar.MINUTE, 0);
		CASTLE_VALIDATION_DATE.set(Calendar.SECOND, 0);
		CASTLE_VALIDATION_DATE.set(Calendar.MILLISECOND, 0);
	}

	public static double CLANHALL_RENTAL_MULTIPLIER;
	public static double CLANHALL_MIN_BID_MULTIPLIER;
	public static int CLANHALL_CLAN_MIN_LEVEL_FOR_BID;

	public static void loadClanHallConfig()
	{
		ExProperties clanhall = load(CLANHALL_CONFIG_FILE);

		CLANHALL_RENTAL_MULTIPLIER = clanhall.getProperty("RentalFee", 1.);
		CLANHALL_MIN_BID_MULTIPLIER = clanhall.getProperty("MinBid", 1.);
		CLANHALL_CLAN_MIN_LEVEL_FOR_BID = clanhall.getProperty("ClanLevel", 2);
	}

	public static void loadOtherConfig()
	{
		ExProperties otherSettings = load(OTHER_CONFIG_FILE);

		DEEPBLUE_DROP_RULES = otherSettings.getProperty("UseDeepBlueDropRules", true);
		DEEPBLUE_DROP_MAXDIFF = otherSettings.getProperty("DeepBlueDropMaxDiff", 8);
		DEEPBLUE_DROP_RAID_MAXDIFF = otherSettings.getProperty("DeepBlueDropRaidMaxDiff", 2);

		SWIMING_SPEED = otherSettings.getProperty("SwimingSpeedTemplate", 50);

		/* Inventory slots limits */
		INVENTORY_MAXIMUM_NO_DWARF = otherSettings.getProperty("MaximumSlotsForNoDwarf", 80);
		INVENTORY_MAXIMUM_DWARF = otherSettings.getProperty("MaximumSlotsForDwarf", 100);
		INVENTORY_MAXIMUM_GM = otherSettings.getProperty("MaximumSlotsForGMPlayer", 250);
		QUEST_INVENTORY_MAXIMUM = otherSettings.getProperty("MaximumSlotsForQuests", 100);

		MULTISELL_SIZE = otherSettings.getProperty("MultisellPageSize", 10);
		MULTISELL_DEBUG = otherSettings.getProperty("MultisellDebug", false);

		/* Warehouse slots limits */
		WAREHOUSE_SLOTS_NO_DWARF = otherSettings.getProperty("BaseWarehouseSlotsForNoDwarf", 100);
		WAREHOUSE_SLOTS_DWARF = otherSettings.getProperty("BaseWarehouseSlotsForDwarf", 120);
		WAREHOUSE_SLOTS_CLAN = otherSettings.getProperty("MaximumWarehouseSlotsForClan", 200);
		FREIGHT_SLOTS = otherSettings.getProperty("MaximumFreightSlots", 10);

		REGEN_SIT_WAIT = otherSettings.getProperty("RegenSitWait", false);

		UNSTUCK_SKILL = otherSettings.getProperty("UnstuckSkill", true);

		/* Amount of HP, MP, and CP is restored */
		RESPAWN_RESTORE_CP = otherSettings.getProperty("RespawnRestoreCP", 0.) / 100;
		RESPAWN_RESTORE_HP = otherSettings.getProperty("RespawnRestoreHP", 65.) / 100;
		RESPAWN_RESTORE_MP = otherSettings.getProperty("RespawnRestoreMP", 0.) / 100;

		/* Maximum number of available slots for pvt stores */
		MAX_PVTSTORE_SLOTS_DWARF = otherSettings.getProperty("MaxPvtStoreSlotsDwarf", 5);
		MAX_PVTSTORE_SLOTS_OTHER = otherSettings.getProperty("MaxPvtStoreSlotsOther", 4);
		MAX_PVTCRAFT_SLOTS = otherSettings.getProperty("MaxPvtManufactureSlots", 20);

		SENDSTATUS_TRADE_JUST_OFFLINE = otherSettings.getProperty("SendStatusTradeJustOffline", false);
		SENDSTATUS_TRADE_MOD = otherSettings.getProperty("SendStatusTradeMod", 1.);

		ANNOUNCE_MAMMON_SPAWN = otherSettings.getProperty("AnnounceMammonSpawn", true);

		GM_NAME_COLOUR = Util.parseColor(otherSettings.getProperty("GMNameColour", "FFFFFF"));
		NORMAL_NAME_COLOUR = Util.parseColor(otherSettings.getProperty("NormalNameColour", "FFFFFF"));
		CLANLEADER_NAME_COLOUR = Util.parseColor(otherSettings.getProperty("ClanleaderNameColour", "FFFFFF"));

		GAME_POINT_ITEM_ID = otherSettings.getProperty("GamePointItemId", -1);

		AUTO_CONSUME = otherSettings.getProperty("AutoConsume", false);
		AUTO_CONSUME_LIST = otherSettings.getProperty("ConusmeList", new int[] { -1 });
	}

	public static void loadSpoilConfig()
	{
		ExProperties spoilSettings = load(SPOIL_CONFIG_FILE);

		BASE_SPOIL_RATE = spoilSettings.getProperty("BasePercent", 80.);
		MINIMUM_SPOIL_RATE = spoilSettings.getProperty("MinimumPercentChanceOfSpoilSuccess", 1.);

		MANOR_SOWING_BASIC_SUCCESS = spoilSettings.getProperty("BasePercentChanceOfSowingSuccess", 100.);
		MANOR_SOWING_ALT_BASIC_SUCCESS = spoilSettings.getProperty("BasePercentChanceOfSowingAltSuccess", 10.);
		MANOR_HARVESTING_BASIC_SUCCESS = spoilSettings.getProperty("BasePercentChanceOfHarvestingSuccess", 90.);
		MANOR_DIFF_PLAYER_TARGET = spoilSettings.getProperty("MinDiffPlayerMob", 5);
		MANOR_DIFF_PLAYER_TARGET_PENALTY = spoilSettings.getProperty("DiffPlayerMobPenalty", 5.);
		MANOR_DIFF_SEED_TARGET = spoilSettings.getProperty("MinDiffSeedMob", 5);
		MANOR_DIFF_SEED_TARGET_PENALTY = spoilSettings.getProperty("DiffSeedMobPenalty", 5.);
		ALLOW_MANOR = spoilSettings.getProperty("AllowManor", true);
		MANOR_REFRESH_TIME = spoilSettings.getProperty("AltManorRefreshTime", 20);
		MANOR_REFRESH_MIN = spoilSettings.getProperty("AltManorRefreshMin", 00);
		MANOR_APPROVE_TIME = spoilSettings.getProperty("AltManorApproveTime", 6);
		MANOR_APPROVE_MIN = spoilSettings.getProperty("AltManorApproveMin", 00);
		MANOR_MAINTENANCE_PERIOD = spoilSettings.getProperty("AltManorMaintenancePeriod", 360000);
	}

	public static void loadFormulasConfig()
	{
		ExProperties formulasSettings = load(FORMULAS_CONFIGURATION_FILE);

		SKILLS_CHANCE_SHOW = formulasSettings.getProperty("ShowChance", false);
		MODIFIER_CHANCE = formulasSettings.getProperty("СhanceModifier", new double[] { 11., 11. });
		MODIFIER_CHANCE_POWER = formulasSettings.getProperty("ChancePower", new double[] { 0.5, 0.5, 1 });
		SKILLS_CHANCE_LIMIT = formulasSettings.getProperty("ChanceLimits", new double[] { 0.5, 0.5 });
		SKILLS_CAST_TIME_MIN = formulasSettings.getProperty("SkillsCastTimeMin", 333);
		DEBUFF_MIN_CAP = formulasSettings.getProperty("DebufMinCap", 1.0);

		ALT_ABSORB_DAMAGE_MODIFIER = formulasSettings.getProperty("AbsorbDamageModifier", 1.0);

		LIM_CRIT_DAM = formulasSettings.getProperty("LimitCriticalDamage", 2000);
		LIM_FAME = formulasSettings.getProperty("LimitFame", 50000);

		ALT_NPC_PATK_MODIFIER = formulasSettings.getProperty("NpcPAtkModifier", 1.0);
		ALT_NPC_MATK_MODIFIER = formulasSettings.getProperty("NpcMAtkModifier", 1.0);
		ALT_NPC_MAXHP_MODIFIER = formulasSettings.getProperty("NpcMaxHpModifier", 1.0);
		ALT_NPC_MAXMP_MODIFIER = formulasSettings.getProperty("NpcMaxMpModifier", 1.0);
		ALT_NPC_PDEF_MODIFIER = formulasSettings.getProperty("NpcPDefModifier", 1.0);
		ALT_NPC_MDEF_MODIFIER = formulasSettings.getProperty("NpcMDefModifier", 1.0);

		ALT_POLE_DAMAGE_MODIFIER = formulasSettings.getProperty("PoleDamageModifier", 1.0);
		SKILLS_DISPEL_SONGS = formulasSettings.getProperty("CancelSongs", false);
		SKILLS_DISPEL_TRANSFORMATION = formulasSettings.getProperty("CancelTransformation", false);
		SKILLS_DISPEL_HOURGLASS = formulasSettings.getProperty("CancelHourGlass", false);
		FULL_IMMUNITY_DEBUFF_NPC = formulasSettings.getProperty("FullImmunityNpc", new int[] { 0 });
		FORMULA_SKILLS_MONSTER_MODIFIER = formulasSettings.getProperty("MonsterSkill", new double[] { -10, 4 });
		NEW_ATT_FORMULA = formulasSettings.getProperty("AttributeFormula", false);
		STEAL_FORMULA_DATA = formulasSettings.getProperty("StealData", new int[] { -1, 25, 75 });
		CANCEL_COUNT_SIZE = formulasSettings.getProperty("CancelSize", new int[] { 1, 7 });
		CANCEL_COUNT_LIMIT = formulasSettings.getProperty("CancelMod", 0.01);
		SOUL_BOOST_CAP = formulasSettings.getProperty("SoulBoost", new double[] { 0.0225, 0.0875 });
		SOUL_CONSUME_CAP = formulasSettings.getProperty("SoulsCap", 5);
		DIVINITY_DATA = formulasSettings.getProperty("DivinityData", new double[] { 0.3, 0.0875 });
		CANCEL_IMMUNITY = formulasSettings.getProperty("CancelImmunity", true);
	}

	public static boolean DEV_DEBUG_PACKET;
	public static boolean DEV_DEBUG_PACKET_ALL;
	public static String[] DEV_DEBUG_PACKET_TARGETS;
	public static boolean DEV_UNDERGROUND_COLISEUM;
	public static int DEV_UNDERGROUND_COLISEUM_MEMBER_COUNT;
	public static boolean CHETER;
	public static boolean MEM_DEBUG;
	public static boolean LEVIATHAN_DEBUG;
	public static int MOUNT_TYPE;
	public static int[] BOXEX_LIST;

	public static void loadDevelopSettings()
	{
		ExProperties dev = load(DEVELOP_FILE);

		DEV_UNDERGROUND_COLISEUM = dev.getProperty("UndergroundColiseum", false);
		DEV_UNDERGROUND_COLISEUM_MEMBER_COUNT = dev.getProperty("UndergroundColiseumMemberCount", 7);
		DEV_DEBUG_PACKET = dev.getProperty("PacketDebug", false);
		DEV_DEBUG_PACKET_ALL = dev.getProperty("PacketDebugAll", false);
		DEV_DEBUG_PACKET_TARGETS = dev.getProperty("PacketDebugList", new String[] { "null" });
		CHETER = dev.getProperty("Cheter", false);
		MEM_DEBUG = dev.getProperty("MemDebug", false);
		BOXEX_LIST = dev.getProperty("Boxes", new int[] { -1 });
		MOUNT_TYPE = dev.getProperty("MountType", 1);
		LEVIATHAN_DEBUG = dev.getProperty("Leviathan", false);
	}

	public static void loadExtSettings()
	{
		ExProperties properties = load(EXT_FILE);

		EX_NEW_PETITION_SYSTEM = properties.getProperty("NewPetitionSystem", false);
		EX_JAPAN_MINIGAME = properties.getProperty("JapanMinigame", false);
		EX_LECTURE_MARK = properties.getProperty("LectureMark", false);
		EX_USE_TELEPORT_FLAG = properties.getProperty("UseTeleportFlag", true);
		EX_CHANGE_NAME_DIALOG = properties.getProperty("ChangeNameDialogIfIncorrect", false);
		SECOND_AUTH_ENABLED = properties.getProperty("2ndPasswordCheck", false);
	}

	public static boolean LOG_CHAT;
	public static boolean LOG_SERVICES;
	public static boolean LOG_REPORT;

	public static void loadLogSettings()
	{
		ExProperties log = load(LOG);

		LOG_CHAT = log.getProperty("Chat", false);
		LOG_SERVICES = log.getProperty("Services", false);
		LOG_REPORT = log.getProperty("Report", false);
	}

	public static void loadAltSettings()
	{
		ExProperties altSettings = load(ALT_SETTINGS_FILE);

		ALT_GAME_DELEVEL = altSettings.getProperty("Delevel", true);
		ALT_SAVE_UNSAVEABLE = altSettings.getProperty("AltSaveUnsaveable", false);
		ALT_SAVE_EFFECTS_REMAINING_TIME = altSettings.getProperty("AltSaveEffectsRemainingTime", 5);
		ALT_SHOW_REUSE_MSG = altSettings.getProperty("AltShowSkillReuseMessage", true);
		ALT_REUSE_CORRECTION = altSettings.getProperty("AltReuseCorrection", 1050);
		ALT_SPIRITSHOT_DISCHARGE_CORRECTION = altSettings.getProperty("AltSpiritShotDischargeCorrection", 100);
		ALT_MUSIC_COST_GUARD_INTERVAL = altSettings.getProperty("AltMusicCostGuardInterval", 0);
		AUTO_LOOT = altSettings.getProperty("AutoLoot", false);
		AUTO_LOOT_HERBS = altSettings.getProperty("AutoLootHerbs", false);
		AUTO_LOOT_INDIVIDUAL = altSettings.getProperty("AutoLootIndividual", false);
		AUTO_LOOT_FROM_RAIDS = altSettings.getProperty("AutoLootFromRaids", false);
		PREMIUM_AUTO_LOOT_BUY = altSettings.getProperty("PremiumLoot", false);
		PREMIUM_AUTO_LOOT_DATA = altSettings.getProperty("PremiumLootData", new int[] { 4037, 2000000 });
		AUTO_LOOT_PK = altSettings.getProperty("AutoLootPK", false);
		ALT_GAME_KARMA_PLAYER_CAN_SHOP = altSettings.getProperty("AltKarmaPlayerCanShop", false);
		SAVING_SPS = altSettings.getProperty("SavingSpS", false);
		CRAFT_MASTERWORK_CHANCE = altSettings.getProperty("CraftMasterworkChance", 3.);
		CRAFT_DOUBLECRAFT_CHANCE = altSettings.getProperty("CraftDoubleCraftChance", 3.);
		ALT_GAME_UNREGISTER_RECIPE = altSettings.getProperty("AltUnregisterRecipe", true);
		ALT_GAME_SHOW_DROPLIST = altSettings.getProperty("AltShowDroplist", true);
		ALLOW_NPC_SHIFTCLICK = altSettings.getProperty("AllowShiftClick", true);
		ALT_FULL_NPC_STATS_PAGE = altSettings.getProperty("AltFullStatsPage", false);
		ALT_GAME_SUBCLASS_WITHOUT_QUESTS = altSettings.getProperty("AltAllowSubClassWithoutQuest", false);
		ALT_ALLOW_SUBCLASS_WITHOUT_BAIUM = altSettings.getProperty("AltAllowSubClassWithoutBaium", true);
		ALT_GAME_LEVEL_TO_GET_SUBCLASS = altSettings.getProperty("AltLevelToGetSubclass", 75);
		ALT_GAME_SUB_ADD = altSettings.getProperty("AltSubAdd", 0);
		ALT_MAX_LEVEL = Math.min(altSettings.getProperty("AltMaxLevel", 85), ExperienceHolder.getInstance().getLevels().length - 1);
		ALT_MAX_SUB_LEVEL = Math.min(altSettings.getProperty("AltMaxSubLevel", 80), ExperienceHolder.getInstance().getLevels().length - 1);
		ALT_START_SUB_LEVEL = altSettings.getProperty("AltStartSubLevel", 40);
		ALT_ALLOW_OTHERS_WITHDRAW_FROM_CLAN_WAREHOUSE = altSettings.getProperty("AltAllowOthersWithdrawFromClanWarehouse", false);
		ALT_ALLOW_CLAN_COMMAND_ONLY_FOR_CLAN_LEADER = altSettings.getProperty("AltAllowClanCommandOnlyForClanLeader", true);

		ALT_DELETE_TRANSFORMATION_ON_DEATH = altSettings.getProperty("DeleteTransOnDeath", false);

		ALT_GAME_REQUIRE_CLAN_CASTLE = altSettings.getProperty("AltRequireClanCastle", false);
		ALT_GAME_REQUIRE_CASTLE_DAWN = altSettings.getProperty("AltRequireCastleDawn", true);
		ALT_GAME_ALLOW_ADENA_DAWN = altSettings.getProperty("AltAllowAdenaDawn", true);
		ALT_ADD_RECIPES = altSettings.getProperty("AltAddRecipes", 0);
		SS_ANNOUNCE_PERIOD = altSettings.getProperty("SSAnnouncePeriod", 0);
		PETITIONING_ALLOWED = altSettings.getProperty("PetitioningAllowed", true);
		MAX_PETITIONS_PER_PLAYER = altSettings.getProperty("MaxPetitionsPerPlayer", 5);
		MAX_PETITIONS_PENDING = altSettings.getProperty("MaxPetitionsPending", 25);
		AUTO_LEARN_SKILLS = altSettings.getProperty("AutoLearnSkills", false);
		AUTO_LEARN_FORGOTTEN_SKILLS = altSettings.getProperty("AutoLearnForgottenSkills", false);
		AUTO_LEARN_DIVINE_INSPIRATION = altSettings.getProperty("AutoLearnDivineInspiration", false);
		ALT_SOCIAL_ACTION_REUSE = altSettings.getProperty("AltSocialActionReuse", false);
		ALT_DISABLE_SPELLBOOKS = altSettings.getProperty("AltDisableSpellbooks", false);
		ALT_SIMPLE_SIGNS = altSettings.getProperty("PushkinSignsOptions", false);
		ALT_TELE_TO_CATACOMBS = altSettings.getProperty("TeleToCatacombs", false);
		ALT_BS_CRYSTALLIZE = altSettings.getProperty("BSCrystallize", false);
		ALT_ALLOW_TATTOO = altSettings.getProperty("AllowTattoo", false);
		ALT_EFFECT_LIMIT = altSettings.getProperty("EffectLimit", new int[] { 20, 12 });
		ALT_DEATH_PENALTY = altSettings.getProperty("EnableAltDeathPenalty", false);
		ALLOW_DEATH_PENALTY_C5 = altSettings.getProperty("EnableDeathPenaltyC5", true);
		ALT_DEATH_PENALTY_C5_CHANCE = altSettings.getProperty("DeathPenaltyC5Chance", 10);
		ALT_DEATH_PENALTY_C5_CHAOTIC_RECOVERY = altSettings.getProperty("ChaoticCanUseScrollOfRecovery", false);
		ALT_DEATH_PENALTY_C5_EXPERIENCE_PENALTY = altSettings.getProperty("DeathPenaltyC5RateExpPenalty", 1);
		ALT_DEATH_PENALTY_C5_KARMA_PENALTY = altSettings.getProperty("DeathPenaltyC5RateKarma", 1);
		ALT_SELLPRICE = altSettings.getProperty("SellPrice", -1);
		ALT_PK_DEATH_RATE = altSettings.getProperty("AltPKDeathRate", 0.);
		NONOWNER_ITEM_PICKUP_DELAY = altSettings.getProperty("NonOwnerItemPickupDelay", 15L) * 1000L;
		ALT_NO_LASTHIT = altSettings.getProperty("NoLasthitOnRaid", false);
		ALT_KAMALOKA_NIGHTMARES_PREMIUM_ONLY = altSettings.getProperty("KamalokaNightmaresPremiumOnly", false);
		ALT_PET_HEAL_BATTLE_ONLY = altSettings.getProperty("PetsHealOnlyInBattle", true);
		ENABLE_FARM = altSettings.getProperty("EnableFarm", false);

		ALT_ALLOW_SELL_COMMON = altSettings.getProperty("AllowSellCommon", true);
		ALT_ALLOW_SHADOW_WEAPONS = altSettings.getProperty("AllowShadowWeapons", true);
		ALT_SHOP_FORBIDDEN_ITEMS = altSettings.getProperty("ShopForbiddenItems", ArrayUtils.EMPTY_INT_ARRAY);

		ALT_ALLOWED_PET_POTIONS = altSettings.getProperty("AllowedPetPotions", new int[] { 735, 1060, 1061, 1062, 1374, 1375, 1539, 1540, 6035, 6036 });

		FESTIVAL_MIN_PARTY_SIZE = altSettings.getProperty("FestivalMinPartySize", 5);
		FESTIVAL_RATE_PRICE = altSettings.getProperty("FestivalRatePrice", 1.0);

		RIFT_MIN_PARTY_SIZE = altSettings.getProperty("RiftMinPartySize", 5);
		RIFT_SPAWN_DELAY = altSettings.getProperty("RiftSpawnDelay", 10000);
		RIFT_MAX_JUMPS = altSettings.getProperty("MaxRiftJumps", 4);
		RIFT_AUTO_JUMPS_TIME = altSettings.getProperty("AutoJumpsDelay", 8);
		RIFT_AUTO_JUMPS_TIME_RAND = altSettings.getProperty("AutoJumpsDelayRandom", 120000);

		RIFT_ENTER_COST_RECRUIT = altSettings.getProperty("RecruitFC", 18);
		RIFT_ENTER_COST_SOLDIER = altSettings.getProperty("SoldierFC", 21);
		RIFT_ENTER_COST_OFFICER = altSettings.getProperty("OfficerFC", 24);
		RIFT_ENTER_COST_CAPTAIN = altSettings.getProperty("CaptainFC", 27);
		RIFT_ENTER_COST_COMMANDER = altSettings.getProperty("CommanderFC", 30);
		RIFT_ENTER_COST_HERO = altSettings.getProperty("HeroFC", 33);

		ALLOW_LEARN_TRANS_SKILLS_WO_QUEST = altSettings.getProperty("AllowLearnTransSkillsWOQuest", false);
		PARTY_LEADER_ONLY_CAN_INVITE = altSettings.getProperty("PartyLeaderOnlyCanInvite", true);
		ALLOW_TALK_WHILE_SITTING = altSettings.getProperty("AllowTalkWhileSitting", true);
		ALLOW_NOBLE_TP_TO_ALL = altSettings.getProperty("AllowNobleTPToAll", false);

		CLANHALL_BUFFTIME_MODIFIER = altSettings.getProperty("ClanHallBuffTimeModifier", 1.0);
		SONGDANCETIME_MODIFIER = altSettings.getProperty("SongDanceTimeModifier", 1.0);
		MAXLOAD_MODIFIER = altSettings.getProperty("MaxLoadModifier", 1.0);
		GATEKEEPER_MODIFIER = altSettings.getProperty("GkCostMultiplier", 1.0);
		GATEKEEPER_NOBLE_MODIFIER = altSettings.getProperty("GkNobleCostMultiplier", 1.0);
		GATEKEEPER_FREE = altSettings.getProperty("GkFree", 40);
		CRUMA_GATEKEEPER_LVL = altSettings.getProperty("GkCruma", 65);
		ALT_IMPROVED_PETS_LIMITED_USE = altSettings.getProperty("ImprovedPetsLimitedUse", false);

		ALT_SKILL_ENCHANT_UPDATE_REUSE = altSettings.getProperty("AltSkillEnchantUpdateReuse", false);
		ALT_SKILL_ENCHANT_ADENA_MODIFIER = altSettings.getProperty("AltSkillEnchantAdenaMultiplier", 1.0);
		ALT_SKILL_ENCHANT_SP_MODIFIER = altSettings.getProperty("AltSkillEnchantSPMultiplier", 1.0);
		ALT_SKILL_SAFE_ENCHANT_ADENA_MODIFIER = altSettings.getProperty("AltSkillSafeEnchantAdenaMultiplier", 5.0);
		ALT_SKILL_SAFE_ENCHANT_SP_MODIFIER = altSettings.getProperty("AltSkillSafeEnchantSPMultiplier", 5.0);
		ALT_SKILL_ROUTE_CHANGE_ADENA_MODIFIER = altSettings.getProperty("AltSkillRouteChangeAdenaMultiplier", 0.2);
		ALT_SKILL_ROUTE_CHANGE_SP_MODIFIER = altSettings.getProperty("AltSkillRouteChangeSPMultiplier", 0.2);
		ALT_SKILL_UNTRAIN_REFUND_SP_MODIFIER = altSettings.getProperty("AltSkillUntrainRefundSPMultiplier", 0.8);

		ALT_NO_FAME_FOR_DEAD = altSettings.getProperty("AltNoFameForDead", false);
		ALT_NOT_ALLOW_TW_WARDS_IN_CLANHALLS = altSettings.getProperty("AltNotAllowTWWardsInClanHalls", true);

		ALT_CHAMPION_CHANCE1 = altSettings.getProperty("AltChampionChance1", 0.);
		ALT_CHAMPION_CHANCE2 = altSettings.getProperty("AltChampionChance2", 0.);
		ALT_CHAMPION_CAN_BE_AGGRO = altSettings.getProperty("AltChampionAggro", false);
		ALT_CHAMPION_CAN_BE_SOCIAL = altSettings.getProperty("AltChampionSocial", false);
		ALT_CHAMPION_MIN_LEVEL = altSettings.getProperty("AltChampionMinLevel", 75);
		ALT_CHAMPION_TOP_LEVEL = altSettings.getProperty("AltChampionTopLevel", 75);

		ALT_PCBANG_POINTS_ENABLED = altSettings.getProperty("AltPcBangPointsEnabled", false);
		ALT_PCBANG_POINTS_BONUS_DOUBLE_CHANCE = altSettings.getProperty("AltPcBangPointsDoubleChance", 10.);
		ALT_PCBANG_POINTS_BONUS = altSettings.getProperty("AltPcBangPointsBonus", 0);
		ALT_PCBANG_POINTS_DELAY = altSettings.getProperty("AltPcBangPointsDelay", 20);
		ALT_PCBANG_POINTS_MIN_LVL = altSettings.getProperty("AltPcBangPointsMinLvl", 1);

		ALT_DEBUG_ENABLED = altSettings.getProperty("AltDebugEnabled", false);
		ALT_DEBUG_PVP_ENABLED = altSettings.getProperty("AltDebugPvPEnabled", false);
		ALT_DEBUG_PVP_DUEL_ONLY = altSettings.getProperty("AltDebugPvPDuelOnly", true);
		ALT_DEBUG_PVE_ENABLED = altSettings.getProperty("AltDebugPvEEnabled", false);
		ALT_DEBUG_ENCHANT_CHANCE_ENABLED = altSettings.getProperty("AltDebugEnchantEnabled", false);

		ALT_MAX_ALLY_SIZE = altSettings.getProperty("AltMaxAllySize", 3);
		ALT_PARTY_DISTRIBUTION_RANGE = altSettings.getProperty("AltPartyDistributionRange", 1500);
		ALT_PARTY_BONUS = altSettings.getProperty("AltPartyBonus", new double[] { 1.00, 1.10, 1.20, 1.30, 1.40, 1.50, 2.00, 2.10, 2.20 });

		ALT_REMOVE_SKILLS_ON_DELEVEL = altSettings.getProperty("AltRemoveSkillsOnDelevel", true);
		ALT_REMOVE_FORGOTTEN_SCROLLS_ON_DELEVEL = altSettings.getProperty("AltRemoveForgottenScrollsOnDelevel", false);
		ALT_CH_ALL_BUFFS = altSettings.getProperty("AltChAllBuffs", false);
		ALT_CH_ALLOW_1H_BUFFS = altSettings.getProperty("AltChAllowHourBuff", false);

		ALT_OPEN_CLOAK_SLOT = altSettings.getProperty("OpenCloakSlot", false);

		ALT_SHOW_SERVER_TIME = altSettings.getProperty("ShowServerTime", false);

		ALT_DISABLE_FEATHER_ON_SIEGES_AND_EPIC = altSettings.getProperty("AltDisableFeatherOnSiegeAndEpic", false);

		FOLLOW_RANGE = altSettings.getProperty("FollowRange", 100);

		ALT_PRIEST_BLESSING_ENABLED = altSettings.getProperty("PriestBlessingEnabled", false);

		ALT_ITEM_AUCTION_ENABLED = altSettings.getProperty("AltItemAuctionEnabled", true);
		ALT_ITEM_AUCTION_CAN_REBID = altSettings.getProperty("AltItemAuctionCanRebid", false);
		ALT_ITEM_AUCTION_START_ANNOUNCE = altSettings.getProperty("AltItemAuctionAnnounce", true);
		ALT_ITEM_AUCTION_BID_ITEM_ID = altSettings.getProperty("AltItemAuctionBidItemId", 57);
		ALT_ITEM_AUCTION_MAX_BID = altSettings.getProperty("AltItemAuctionMaxBid", 1000000L);
		ALT_ITEM_AUCTION_MAX_CANCEL_TIME_IN_MILLIS = altSettings.getProperty("AltItemAuctionMaxCancelTimeInMillis", 604800000);

		ALT_FISH_CHAMPIONSHIP_ENABLED = altSettings.getProperty("AltFishChampionshipEnabled", true);
		ALT_FISH_CHAMPIONSHIP_REWARD_ITEM = altSettings.getProperty("AltFishChampionshipRewardItemId", 57);
		ALT_FISH_CHAMPIONSHIP_REWARD_1 = altSettings.getProperty("AltFishChampionshipReward1", 800000);
		ALT_FISH_CHAMPIONSHIP_REWARD_2 = altSettings.getProperty("AltFishChampionshipReward2", 500000);
		ALT_FISH_CHAMPIONSHIP_REWARD_3 = altSettings.getProperty("AltFishChampionshipReward3", 300000);
		ALT_FISH_CHAMPIONSHIP_REWARD_4 = altSettings.getProperty("AltFishChampionshipReward4", 200000);
		ALT_FISH_CHAMPIONSHIP_REWARD_5 = altSettings.getProperty("AltFishChampionshipReward5", 100000);

		ALT_ENABLE_BLOCK_CHECKER_EVENT = altSettings.getProperty("EnableBlockCheckerEvent", true);
		ALT_MIN_BLOCK_CHECKER_TEAM_MEMBERS = Math.min(Math.max(altSettings.getProperty("BlockCheckerMinTeamMembers", 1), 1), 6);
		ALT_RATE_COINS_REWARD_BLOCK_CHECKER = altSettings.getProperty("BlockCheckerRateCoinReward", 1.);

		ALT_HBCE_FAIR_PLAY = altSettings.getProperty("HBCEFairPlay", false);

		ALT_PET_INVENTORY_LIMIT = altSettings.getProperty("AltPetInventoryLimit", 12);

		INFINITE_BEAST_SHOT = altSettings.getProperty("BeastShot", false);
		INFINITE_FISH_SHOT = altSettings.getProperty("FishShot", false);
		INFINITE_SOUL_SHOT = altSettings.getProperty("SoulShot", false);
		INFINITE_SPIRIT_SHOT = altSettings.getProperty("SpiritShot", false);
		INFINITE_BLESSED_SPIRIT_SHOT = altSettings.getProperty("BlessedSpiritShot", false);
		INFINITE_ARROWS_AND_BOLTS = altSettings.getProperty("ArrowsBolts", false);
		AUGMENTATION_CHANCE_MOD = altSettings.getProperty("AugmentChance", new double[] { 1.0, 1.0 });
		BARAKIEL_KILL_NOBLE = altSettings.getProperty("BarakielNoble", false);
		PVP_ZONE_LIST = altSettings.getProperty("PvPZones", new String[] {});
	}

	public static boolean AFK_ENABLE;
	public static int AFK_INTERAVL;
	public static String AFK_TITLE;
	public static boolean AFK_VISUAL;
	public static int[] AFK_COLORS = new int[2];

	public static void loadAfkSettings()
	{
		ExProperties afk = load(AFK);

		AFK_ENABLE = afk.getProperty("Enable", false);
		AFK_INTERAVL = afk.getProperty("Interval", 30);
		AFK_VISUAL = afk.getProperty("Visual", false);
		AFK_TITLE = afk.getProperty("Title", "AFK");
		final String[] colors = afk.getProperty("Colors", "FFFF33,FFFF33").split(",");
		AFK_COLORS[0] = Util.parseColor(colors[0]);
		AFK_COLORS[1] = Util.parseColor(colors[1]);
	}

	public static void loadServicesSettings()
	{
		ExProperties servicesSettings = load(SERVICES_FILE);

		for(int id : servicesSettings.getProperty("AllowClassMasters", ArrayUtils.EMPTY_INT_ARRAY))
			if(id != 0)
				ALLOW_CLASS_MASTERS_LIST.add(id);

		String price = servicesSettings.getProperty("ClassMastersPrice", "0,0,0");
		if(price.length() >= 5)
		{
			int level = 1;
			for(String id : price.split(","))
			{
				CLASS_MASTERS_PRICE_LIST[level] = Integer.parseInt(id);
				level++;
			}
		}
		CLASS_MASTERS_PRICE_ITEM = servicesSettings.getProperty("ClassMastersPriceItem", 57);

		SERVICES_ALLOW_ITEM_BROKER_SEARCH = servicesSettings.getProperty("UseItemBrokerItemSearch", false);
		SERVICES_ITEM_BROKER_REFRESH_DELAY = servicesSettings.getProperty("ItemBrokerRefreshDelay", 0);

		SERVICES_CHANGE_NICK_ENABLED = servicesSettings.getProperty("NickChangeEnabled", false);
		SERVICES_CHANGE_NICK_TEMPLATE = servicesSettings.getProperty("NickChangeTemplate", "[A-Za-z0-9\u0410-\u042f\u0430-\u044f]{2,16}");
		SERVICES_CHANGE_NICK_PRICE = servicesSettings.getProperty("NickChangePrice", 100);
		SERVICES_CHANGE_NICK_ITEM = servicesSettings.getProperty("NickChangeItem", 4037);

		SERVICES_CHANGE_CLAN_NAME_ENABLED = servicesSettings.getProperty("ClanNameChangeEnabled", false);
		SERVICES_CHANGE_CLAN_NAME_PRICE = servicesSettings.getProperty("ClanNameChangePrice", 100);
		SERVICES_CHANGE_CLAN_NAME_ITEM = servicesSettings.getProperty("ClanNameChangeItem", 4037);

		SERVICES_CHANGE_PET_NAME_ENABLED = servicesSettings.getProperty("PetNameChangeEnabled", false);
		SERVICES_CHANGE_PET_NAME_PRICE = servicesSettings.getProperty("PetNameChangePrice", 100);
		SERVICES_CHANGE_PET_NAME_ITEM = servicesSettings.getProperty("PetNameChangeItem", 4037);

		SERVICES_EXCHANGE_BABY_PET_ENABLED = servicesSettings.getProperty("BabyPetExchangeEnabled", false);
		SERVICES_EXCHANGE_BABY_PET_PRICE = servicesSettings.getProperty("BabyPetExchangePrice", 100);
		SERVICES_EXCHANGE_BABY_PET_ITEM = servicesSettings.getProperty("BabyPetExchangeItem", 4037);

		SERVICES_CHANGE_SEX_ENABLED = servicesSettings.getProperty("SexChangeEnabled", false);
		SERVICES_CHANGE_SEX_PRICE = servicesSettings.getProperty("SexChangePrice", 100);
		SERVICES_CHANGE_SEX_ITEM = servicesSettings.getProperty("SexChangeItem", 4037);

		SERVICES_CHANGE_BASE_ENABLED = servicesSettings.getProperty("BaseChangeEnabled", false);
		SERVICES_CHANGE_BASE_PRICE = servicesSettings.getProperty("BaseChangePrice", 100);
		SERVICES_CHANGE_BASE_ITEM = servicesSettings.getProperty("BaseChangeItem", 4037);

		SERVICES_SEPARATE_SUB_ENABLED = servicesSettings.getProperty("SeparateSubEnabled", false);
		SERVICES_SEPARATE_SUB_PRICE = servicesSettings.getProperty("SeparateSubPrice", 100);
		SERVICES_SEPARATE_SUB_ITEM = servicesSettings.getProperty("SeparateSubItem", 4037);

		SERVICES_CHANGE_NICK_COLOR_ENABLED = servicesSettings.getProperty("NickColorChangeEnabled", false);
		SERVICES_CHANGE_NICK_COLOR_PRICE = servicesSettings.getProperty("NickColorChangePrice", 100);
		SERVICES_CHANGE_NICK_COLOR_ITEM = servicesSettings.getProperty("NickColorChangeItem", 4037);
		SERVICES_CHANGE_NICK_COLOR_LIST = servicesSettings.getProperty("NickColorChangeList", new String[] { "00FF00" });

		SERVICES_CHANGE_TITLE_COLOR_ENABLED = servicesSettings.getProperty("TitleColorChangeEnabled", false);
		SERVICES_CHANGE_TITLE_COLOR_PRICE = servicesSettings.getProperty("TitleColorChangePrice", 100);
		SERVICES_CHANGE_TITLE_COLOR_ITEM = servicesSettings.getProperty("TitleColorChangeItem", 4037);
		SERVICES_CHANGE_TITLE_COLOR_LIST = servicesSettings.getProperty("TitleColorChangeList", new String[] { "00FF00" });

		SERVICES_BASH_ENABLED = servicesSettings.getProperty("BashEnabled", false);
		SERVICES_BASH_SKIP_DOWNLOAD = servicesSettings.getProperty("BashSkipDownload", false);
		SERVICES_BASH_RELOAD_TIME = servicesSettings.getProperty("BashReloadTime", 24);

		SERVICES_NOBLESS_SELL_ENABLED = servicesSettings.getProperty("NoblessSellEnabled", false);
		SERVICES_NOBLESS_SELL_PRICE = servicesSettings.getProperty("NoblessSellPrice", 1000);
		SERVICES_NOBLESS_SELL_ITEM = servicesSettings.getProperty("NoblessSellItem", 4037);

		SERVICES_RECOMMENDS_SELL_ENABLED = servicesSettings.getProperty("RecommendsSellEnabled", false);
		SERVICES_RECOMMENDS_SELL_PRICE = servicesSettings.getProperty("RecommendsPrice", 100);
		SERVICES_RECOMMENDS_SELL_ITEM = servicesSettings.getProperty("RecommendsItem", 4037);
		SERVICES_RECOMMENDS_SELL_COUNT = servicesSettings.getProperty("RecommendsCount", 255);

		SERVICES_EXPAND_INVENTORY_ENABLED = servicesSettings.getProperty("ExpandInventoryEnabled", false);
		SERVICES_EXPAND_INVENTORY_PRICE = servicesSettings.getProperty("ExpandInventoryPrice", 1000);
		SERVICES_EXPAND_INVENTORY_ITEM = servicesSettings.getProperty("ExpandInventoryItem", 4037);
		SERVICES_EXPAND_INVENTORY_SLOTS = servicesSettings.getProperty("ExpandInventorySlots", 1);
		SERVICES_EXPAND_INVENTORY_MAX = servicesSettings.getProperty("ExpandInventoryMax", 250);

		SERVICES_EXPAND_WAREHOUSE_ENABLED = servicesSettings.getProperty("ExpandWarehouseEnabled", false);
		SERVICES_EXPAND_WAREHOUSE_PRICE = servicesSettings.getProperty("ExpandWarehousePrice", 1000);
		SERVICES_EXPAND_WAREHOUSE_ITEM = servicesSettings.getProperty("ExpandWarehouseItem", 4037);
		SERVICES_EXPAND_WAREHOUSE_SLOTS = servicesSettings.getProperty("ExpandWarehouseSlots", 1);
		SERVICES_EXPAND_WAREHOUSE_MAX = servicesSettings.getProperty("ExpandWarehouseMax", 300);

		SERVICES_EXPAND_CWH_ENABLED = servicesSettings.getProperty("ExpandCWHEnabled", false);
		SERVICES_EXPAND_CWH_PRICE = servicesSettings.getProperty("ExpandCWHPrice", 1000);
		SERVICES_EXPAND_CWH_ITEM = servicesSettings.getProperty("ExpandCWHItem", 4037);

		SERVICES_OFFLINE_TRADE_ALLOW = servicesSettings.getProperty("AllowOfflineTrade", false);
		SERVICES_OFFLINE_TRADE_ALLOW_OFFSHORE = servicesSettings.getProperty("AllowOfflineTradeOnlyOffshore", true);
		SERVICES_OFFLINE_TRADE_MIN_LEVEL = servicesSettings.getProperty("OfflineMinLevel", 0);
		SERVICES_OFFLINE_TRADE_NAME_COLOR = Util.parseColor(servicesSettings.getProperty("OfflineTradeNameColor", "B0FFFF"));
		SERVICES_OFFLINE_TRADE_PRICE_ITEM = servicesSettings.getProperty("OfflineTradePriceItem", 0);
		SERVICES_OFFLINE_TRADE_PRICE = servicesSettings.getProperty("OfflineTradePrice", 0);
		SERVICES_OFFLINE_TRADE_SECONDS_TO_KICK = servicesSettings.getProperty("OfflineTradeDaysToKick", 14) * 86400L;
		SERVICES_OFFLINE_TRADE_RESTORE_AFTER_RESTART = servicesSettings.getProperty("OfflineRestoreAfterRestart", true);
		PVTSTORE_OFFSHORE_ASK = servicesSettings.getProperty("OffshoreAsk", false);
		PVTSTORE_OFFSHORE_CORD = servicesSettings.getProperty("OffshoreCord", new int[] { 116872, 76120, -2736 });

		SERVICES_NO_TRADE_ONLY_OFFLINE = servicesSettings.getProperty("NoTradeOnlyOffline", false);
		SERVICES_TRADE_TAX = servicesSettings.getProperty("TradeTax", 0.0);
		SERVICES_OFFSHORE_TRADE_TAX = servicesSettings.getProperty("OffshoreTradeTax", 0.0);
		SERVICES_TRADE_TAX_ONLY_OFFLINE = servicesSettings.getProperty("TradeTaxOnlyOffline", false);
		SERVICES_OFFSHORE_NO_CASTLE_TAX = servicesSettings.getProperty("NoCastleTaxInOffshore", false);
		SERVICES_TRADE_ONLY_FAR = servicesSettings.getProperty("TradeOnlyFar", false);
		SERVICES_TRADE_MIN_LEVEL = servicesSettings.getProperty("MinLevelForTrade", 0);
		SERVICES_TRADE_RADIUS = servicesSettings.getProperty("TradeRadius", 30);

		SERVICES_GIRAN_HARBOR_ENABLED = servicesSettings.getProperty("GiranHarborZone", false);
		SERVICES_PARNASSUS_ENABLED = servicesSettings.getProperty("ParnassusZone", false);
		SERVICES_PARNASSUS_NOTAX = servicesSettings.getProperty("ParnassusNoTax", false);
		SERVICES_PARNASSUS_PRICE = servicesSettings.getProperty("ParnassusPrice", 500000);

		SERVICES_ALLOW_LOTTERY = servicesSettings.getProperty("AllowLottery", false);
		SERVICES_LOTTERY_PRIZE = servicesSettings.getProperty("LotteryPrize", 50000);
		SERVICES_ALT_LOTTERY_PRICE = servicesSettings.getProperty("AltLotteryPrice", 2000);
		SERVICES_LOTTERY_TICKET_PRICE = servicesSettings.getProperty("LotteryTicketPrice", 2000);
		SERVICES_LOTTERY_5_NUMBER_RATE = servicesSettings.getProperty("Lottery5NumberRate", 0.6);
		SERVICES_LOTTERY_4_NUMBER_RATE = servicesSettings.getProperty("Lottery4NumberRate", 0.4);
		SERVICES_LOTTERY_3_NUMBER_RATE = servicesSettings.getProperty("Lottery3NumberRate", 0.2);
		SERVICES_LOTTERY_2_AND_1_NUMBER_PRIZE = servicesSettings.getProperty("Lottery2and1NumberPrize", 200);

		SERVICES_ALLOW_ROULETTE = servicesSettings.getProperty("AllowRoulette", false);
		SERVICES_ROULETTE_MIN_BET = servicesSettings.getProperty("RouletteMinBet", 1L);
		SERVICES_ROULETTE_MAX_BET = servicesSettings.getProperty("RouletteMaxBet", Long.MAX_VALUE);

		SERVICES_ENABLE_NO_CARRIER = servicesSettings.getProperty("EnableNoCarrier", false);
		SERVICES_NO_CARRIER_MIN_TIME = servicesSettings.getProperty("NoCarrierMinTime", 0);
		SERVICES_NO_CARRIER_MAX_TIME = servicesSettings.getProperty("NoCarrierMaxTime", 90);

		ALLOW_WEDDING = servicesSettings.getProperty("AllowWedding", false);
		WEDDING_PRICE = servicesSettings.getProperty("WeddingPrice", 500000);
		WEDDING_PUNISH_INFIDELITY = servicesSettings.getProperty("WeddingPunishInfidelity", true);
		WEDDING_TELEPORT_ENABLED = servicesSettings.getProperty("WeddingTeleportEnabled", true);
		WEDDING_TELEPORT_PRICE = servicesSettings.getProperty("WeddingTeleportPrice", 500000);
		WEDDING_TELEPORT_INTERVAL = servicesSettings.getProperty("WeddingTeleportInterval", 120);
		WEDDING_SAMESEX = servicesSettings.getProperty("WeddingAllowSameSex", false);
		WEDDING_FORMALWEAR = servicesSettings.getProperty("WeddingFormalWear", true);
		WEDDING_DIVORCE_COSTS = servicesSettings.getProperty("WeddingDivorceCosts", 20);

		ALLOW_EVENT_GATEKEEPER = servicesSettings.getProperty("AllowEventGatekeeper", false);

		QUEST_SELL_ENABLE = servicesSettings.getProperty("QuestSell", false);
		QUEST_SELL_PRICE_FOR_ALL = servicesSettings.getProperty("AQuestPrice", new int[] { 57, 500000 });
		QUEST_SELL_ID_ENABLE = servicesSettings.getProperty("QuestId", new int[] { 0 });
		QUEST_SELL_PRICE_UNIQUE = servicesSettings.getProperty("UQuestPrice", "-1:57-10000;0:57-10000".trim().replaceAll(" ", "").split(";"));

		SERVICES_HERO_SELL_ENABLED = servicesSettings.getProperty("HeroSell", false);
		SERVICES_HERO_SELL_PRICE = servicesSettings.getProperty("HeroPrice", new int[] { 0 });
		SERVICES_HERO_SELL_ITEM = servicesSettings.getProperty("HeroItem", new int[] { 0 });
		SERVICES_HERO_SELL_DAY = servicesSettings.getProperty("HeroDay", new int[] { 0 });
		SERVICES_HERO_SELL_CHAT = servicesSettings.getProperty("HeroChat", false);
		SERVICES_HERO_SELL_SKILL = servicesSettings.getProperty("HeroSkills", false);
		SERVICES_HERO_SELL_ITEMS = servicesSettings.getProperty("HeroItems", false);

		SERVICES_WASH_SINS_PRICE = servicesSettings.getProperty("WashSinsPrice", 10000);
		SERVICES_WASH_SINS_PRICE_ITEM_ID = servicesSettings.getProperty("WashSinsPriceID", 57);

		SERVICES_CLEAR_PK_PRICE = servicesSettings.getProperty("ClearPkPrice", 10000);
		SERVICES_CLEAR_PK_PRICE_ITEM_ID = servicesSettings.getProperty("ClearPkPriceID", 57);
		SERVICES_CLEAR_PK_COUNT = servicesSettings.getProperty("ClearPkCount", 1);

		SERVICES_HAIR_CHANGE_ITEM_ID = servicesSettings.getProperty("HairID", 4037);
		SERVICES_HAIR_CHANGE_COUNT = servicesSettings.getProperty("HairCount", 10);

		SERVICES_LEVEL_UP_ENABLE = servicesSettings.getProperty("Level", false);
		SERVICES_LEVEL_UP = servicesSettings.getProperty("LevelUp", new int[] { 57, 50000 });
		SERVICES_DELEVEL_ENABLE = servicesSettings.getProperty("Delevel", false);
		SERVICES_DELEVEL = servicesSettings.getProperty("LevelLower", new int[] { 57, 50000 });

		SERVICES_CLAN_REPUTATION_SELL_ENABLED = servicesSettings.getProperty("ClanRepEnable", false);
		SERVICES_CLAN_REPUTATION_SELL_DATA = servicesSettings.getProperty("ClanRepData", new int[] { 40000, 4037, 20 });

		SERVICES_CLAN_LEVEL_SELL_ENABLED = servicesSettings.getProperty("ClanLevelEnable", false);
		String[] level = servicesSettings.getProperty("ClanLevelData", "").split(",");
		SERVICES_CLAN_LEVEL_SELL_DATA = new long[11][2];
		for(int i = 0; i < 11; i++)
		{
			String[] data = level[i].split(":");
			SERVICES_CLAN_LEVEL_SELL_DATA[i][0] = Integer.parseInt(data[0]);
			SERVICES_CLAN_LEVEL_SELL_DATA[i][1] = Long.parseLong(data[1]);
		}
	}

	public static void loadPvPSettings()
	{
		ExProperties pvpSettings = load(PVP_CONFIG_FILE);

		/* KARMA SYSTEM */
		KARMA_MIN_KARMA = pvpSettings.getProperty("MinKarma", 240);
		KARMA_SP_DIVIDER = pvpSettings.getProperty("SPDivider", 7);
		KARMA_LOST_BASE = pvpSettings.getProperty("BaseKarmaLost", 0);

		KARMA_DROP_GM = pvpSettings.getProperty("CanGMDropEquipment", false);
		KARMA_NEEDED_TO_DROP = pvpSettings.getProperty("KarmaNeededToDrop", true);
		DROP_ITEMS_ON_DIE = pvpSettings.getProperty("DropOnDie", false);
		DROP_ITEMS_AUGMENTED = pvpSettings.getProperty("DropAugmented", false);

		KARMA_DROP_ITEM_LIMIT = pvpSettings.getProperty("MaxItemsDroppable", 10);
		MIN_PK_TO_ITEMS_DROP = pvpSettings.getProperty("MinPKToDropItems", 5);

		KARMA_RANDOM_DROP_LOCATION_LIMIT = pvpSettings.getProperty("MaxDropThrowDistance", 70);

		KARMA_DROPCHANCE_BASE = pvpSettings.getProperty("ChanceOfPKDropBase", 20.);
		KARMA_DROPCHANCE_MOD = pvpSettings.getProperty("ChanceOfPKsDropMod", 1.);
		NORMAL_DROPCHANCE_BASE = pvpSettings.getProperty("ChanceOfNormalDropBase", 1.);
		DROPCHANCE_EQUIPPED_WEAPON = pvpSettings.getProperty("ChanceOfDropWeapon", 3);
		DROPCHANCE_EQUIPMENT = pvpSettings.getProperty("ChanceOfDropEquippment", 17);
		DROPCHANCE_ITEM = pvpSettings.getProperty("ChanceOfDropOther", 80);

		KARMA_LIST_NONDROPPABLE_ITEMS = new ArrayList<Integer>();
		for(int id : pvpSettings.getProperty("ListOfNonDroppableItems", new int[] {
				57,
				1147,
				425,
				1146,
				461,
				10,
				2368,
				7,
				6,
				2370,
				2369,
				3500,
				3501,
				3502,
				4422,
				4423,
				4424,
				2375,
				6648,
				6649,
				6650,
				6842,
				6834,
				6835,
				6836,
				6837,
				6838,
				6839,
				6840,
				5575,
				7694,
				6841,
				8181 }))
			KARMA_LIST_NONDROPPABLE_ITEMS.add(id);

		PVP_TIME = pvpSettings.getProperty("PvPTime", 40000);
		AUTO_PVP = pvpSettings.getProperty("AutoPvP", true);
		AUTO_PVP_CLAN_WAR = pvpSettings.getProperty("AutoClanPvP", true);
		PVP_IN_BATTLE_ZONE = pvpSettings.getProperty("BattlePoint", false);
	}

	public static void loadAISettings()
	{
		ExProperties aiSettings = load(AI_CONFIG_FILE);

		AI_TASK_MANAGER_COUNT = aiSettings.getProperty("AiTaskManagers", 1);
		if(!isPowerOfTwo(AI_TASK_MANAGER_COUNT))
			throw new RuntimeException("AiTaskManagers value should be power of 2!");
		AI_TASK_ATTACK_DELAY = aiSettings.getProperty("AiTaskDelay", 1000);
		AI_TASK_ACTIVE_DELAY = aiSettings.getProperty("AiTaskActiveDelay", 1000);
		BLOCK_ACTIVE_TASKS = aiSettings.getProperty("BlockActiveTasks", false);
		ALWAYS_TELEPORT_HOME = aiSettings.getProperty("AlwaysTeleportHome", false);

		RND_WALK = aiSettings.getProperty("RndWalk", true);
		RND_WALK_RATE = aiSettings.getProperty("RndWalkRate", 1);
		RND_ANIMATION_RATE = aiSettings.getProperty("RndAnimationRate", 2);

		AGGRO_CHECK_INTERVAL = aiSettings.getProperty("AggroCheckInterval", 250);
		NONAGGRO_TIME_ONTELEPORT = aiSettings.getProperty("NonAggroTimeOnTeleport", 15000);
		MAX_DRIFT_RANGE = aiSettings.getProperty("MaxDriftRange", 100);
		MAX_PURSUE_RANGE = aiSettings.getProperty("MaxPursueRange", 4000);
		MAX_PURSUE_UNDERGROUND_RANGE = aiSettings.getProperty("MaxPursueUndergoundRange", 2000);
		MAX_PURSUE_RANGE_RAID = aiSettings.getProperty("MaxPursueRangeRaid", 5000);

		COZY_MUCUS_BUFF = aiSettings.getProperty("CozyMucus", true);
		KARIK_REPRODUCTION_CHANCE = aiSettings.getProperty("KarikChance", 2.5);
		KARIK_REPRODUCTION = aiSettings.getProperty("KarikReproduction", new int[] { 1, 10 });
		DRAKOS_REPRODUCTION_CHANCE = aiSettings.getProperty("DrakosChance", 1.);
		DRAKOS_REPRODUCTION = aiSettings.getProperty("DrakosReproduction", new int[] { 2, 5 });
	}

	public static void loadGeodataSettings()
	{
		ExProperties geodataSettings = load(GEODATA_CONFIG_FILE);

		GEO_X_FIRST = geodataSettings.getProperty("GeoFirstX", 11);
		GEO_Y_FIRST = geodataSettings.getProperty("GeoFirstY", 10);
		GEO_X_LAST = geodataSettings.getProperty("GeoLastX", 26);
		GEO_Y_LAST = geodataSettings.getProperty("GeoLastY", 26);

		ALLOW_GEODATA = geodataSettings.getProperty("AllowGeodata", true);
		ALLOW_FALL_FROM_WALLS = geodataSettings.getProperty("AllowFallFromWalls", false);
		ALLOW_KEYBOARD_MOVE = geodataSettings.getProperty("AllowMoveWithKeyboard", true);
		COMPACT_GEO = geodataSettings.getProperty("CompactGeoData", false);
		CLIENT_Z_SHIFT = geodataSettings.getProperty("ClientZShift", 16);
		PATHFIND_BOOST = geodataSettings.getProperty("PathFindBoost", 2);
		PATHFIND_DIAGONAL = geodataSettings.getProperty("PathFindDiagonal", true);
		PATHFIND_MAP_MUL = geodataSettings.getProperty("PathFindMapMul", 2);
		PATH_CLEAN = geodataSettings.getProperty("PathClean", true);
		PATHFIND_MAX_Z_DIFF = geodataSettings.getProperty("PathFindMaxZDiff", 32);
		MAX_Z_DIFF = geodataSettings.getProperty("MaxZDiff", 64);
		MIN_LAYER_HEIGHT = geodataSettings.getProperty("MinLayerHeight", 64);
		REGION_EDGE_MAX_Z_DIFF = geodataSettings.getProperty("RegionEdgeMaxZDiff", 128);
		PATHFIND_MAX_TIME = geodataSettings.getProperty("PathFindMaxTime", 10000000);
		PATHFIND_BUFFERS = geodataSettings.getProperty("PathFindBuffers", "8x96;8x128;8x160;8x192;4x224;4x256;4x288;2x320;2x384;2x352;1x512");
	}

	public static void loadEventsSettings()
	{
		ExProperties eventSettings = load(EVENTS_CONFIG_FILE);

		ENCHANT_CHANCE_MASTER_YOGI_STAFF = eventSettings.getProperty("MasterYogiEnchantChance", 66);
		ENCHANT_MAX_MASTER_YOGI_STAFF = eventSettings.getProperty("MasterYogiEnchantMaxWeapon", 28);
		SAFE_ENCHANT_MASTER_YOGI_STAFF = eventSettings.getProperty("MasterYogiSafeEnchant", 3);
		IVORY_REWARD_ID = eventSettings.getProperty("IvoryId", new int[] { 22352, 10, 30 });
		CHEST_REWARD_ID = eventSettings.getProperty("ChestId", new int[] { 22352, 1, 1 });
		DAILY_TASKS_ENABLE = eventSettings.getProperty("EnableDailyTaskEngine", false);
	}


	public static void loadOlympiadSettings()
	{
		ExProperties olympSettings = load(OLYMPIAD);

		ENABLE_OLYMPIAD = olympSettings.getProperty("EnableOlympiad", true);
		ENABLE_OLYMPIAD_SPECTATING = olympSettings.getProperty("EnableOlympiadSpectating", true);
		ALT_OLY_START_TIME = olympSettings.getProperty("AltOlyStartTime", 18);
		ALT_OLY_MIN = olympSettings.getProperty("AltOlyMin", 0);
		OLYMPIAD_CRON = olympSettings.getProperty("OlympiadCron", "0 0 1 * *");
		ALT_OLY_CPERIOD = olympSettings.getProperty("AltOlyCPeriod", 21600000);
		ALT_OLY_WPERIOD = olympSettings.getProperty("AltOlyWPeriod", 604800000);
		ALT_OLY_VPERIOD = olympSettings.getProperty("AltOlyVPeriod", 43200000);
		CLASS_GAME_MIN = olympSettings.getProperty("ClassGameMin", 5);
		NONCLASS_GAME_MIN = olympSettings.getProperty("NonClassGameMin", 9);
		TEAM_GAME_MIN = olympSettings.getProperty("TeamGameMin", 4);

		GAME_MAX_LIMIT = olympSettings.getProperty("GameMaxLimit", 70);
		GAME_CLASSES_COUNT_LIMIT = olympSettings.getProperty("GameClassesCountLimit", 30);
		GAME_NOCLASSES_COUNT_LIMIT = olympSettings.getProperty("GameNoClassesCountLimit", 60);
		GAME_TEAM_COUNT_LIMIT = olympSettings.getProperty("GameTeamCountLimit", 10);

		ALT_OLY_BATTLE_REWARD_ITEM = olympSettings.getProperty("AltOlyBattleRewItem", 13722);
		ALT_OLY_CLASSED_RITEM_C = olympSettings.getProperty("AltOlyClassedRewItemCount", 50);
		ALT_OLY_NONCLASSED_RITEM_C = olympSettings.getProperty("AltOlyNonClassedRewItemCount", 40);
		ALT_OLY_TEAM_RITEM_C = olympSettings.getProperty("AltOlyTeamRewItemCount", 50);
		ALT_OLY_COMP_RITEM = olympSettings.getProperty("AltOlyCompRewItem", 13722);
		ALT_OLY_GP_PER_POINT = olympSettings.getProperty("AltOlyGPPerPoint", 1000);
		ALT_OLY_HERO_POINTS = olympSettings.getProperty("AltOlyHeroPoints", 180);
		ALT_OLY_RANK1_POINTS = olympSettings.getProperty("AltOlyRank1Points", 120);
		ALT_OLY_RANK2_POINTS = olympSettings.getProperty("AltOlyRank2Points", 80);
		ALT_OLY_RANK3_POINTS = olympSettings.getProperty("AltOlyRank3Points", 55);
		ALT_OLY_RANK4_POINTS = olympSettings.getProperty("AltOlyRank4Points", 35);
		ALT_OLY_RANK5_POINTS = olympSettings.getProperty("AltOlyRank5Points", 20);
		OLYMPIAD_STADIAS_COUNT = olympSettings.getProperty("OlympiadStadiasCount", 160);
		OLYMPIAD_BATTLES_FOR_REWARD = olympSettings.getProperty("OlympiadBattlesForReward", 15);
		OLYMPIAD_POINTS_DEFAULT = olympSettings.getProperty("OlympiadPointsDefault", 10);
		OLYMPIAD_POINTS_WEEKLY = olympSettings.getProperty("OlympiadPointsWeekly", 10);
		OLYMPIAD_OLDSTYLE_STAT = olympSettings.getProperty("OlympiadOldStyleStat", false);
		HERO_DIARY_EXCLUDED_BOSSES = olympSettings.getProperty("ExcludedBossesFromDairy", new int[] { 29177 });

		OLYMPIAD_SKILL_COOLDOWN = olympSettings.getProperty("Cooldown", 15);
		ALT_OLY_WAIT_TIME = olympSettings.getProperty("AltOlyWaitTime", 120);
		ALT_OLY_PORT_BACK_TIME = olympSettings.getProperty("AltOlyPortBackTime", 20);

		//CHECK_OLYMPIAD_HWID = olympSettings.getProperty("HWIDCheker", true);
		//CHECK_OLYMPIAD_IP = olympSettings.getProperty("IPCheker", true);

		ALT_OLY_TALISMANS_MP_CONSUME = olympSettings.getProperty("AltOlyTalismansMpConsume", true);

		ALT_OLY_PHYS_WEAPON_ENCHANT_LOCK = olympSettings.getProperty("AltOlyPhysWeaponEnchantLock", -1);
		ALT_OLY_MAGIC_WEAPON_ENCHANT_LOCK = olympSettings.getProperty("AltOlyMagicWeaponEnchantLock", -1);
		ALT_OLY_ARMOR_ENCHANT_LOCK = olympSettings.getProperty("AltOlyArmorEnchantLock", -1);
		ALT_OLY_ACCESSORY_ENCHANT_LOCK = olympSettings.getProperty("AltOlyAccessoryEnchantLock", -1);
		ALT_OLY_WEAPON_ATTRIBUTE_LOCK = olympSettings.getProperty("AltOlyWeaponAttributeLock", -1);
		ALT_OLY_ARMOR_ATTRIBUTE_LOCK = olympSettings.getProperty("AltOlyArmorAttributeLock", -1);
		ALT_OLY_MAX_SKILL_ENCHANT = olympSettings.getProperty("AltOlyMaxSkillEnchant", false);
	}

	public static boolean COMMAND_SUMMON_CLAN_ENABLED;
	public static int[] COMMAND_SUMMON_CLAN_COST;
	public static int[] COMMAND_LEADER_SUMMON_CLAN_COST;

	public static void loadSummonClanConfig()
	{
		ExProperties summon = load(COMMAND_SUMMON_CLAN);

		COMMAND_SUMMON_CLAN_ENABLED = summon.getProperty("Enabled", false);
		COMMAND_SUMMON_CLAN_COST = summon.getProperty("Cost", new int[] { 8615, 1 });
		COMMAND_LEADER_SUMMON_CLAN_COST = summon.getProperty("Summon", new int[] { 57, 10000000 });
	}

	public static boolean COMMAND_SHOW_EQUIP_ENABLED;
	public static boolean COMMAND_SHOW_EQUIP_TIMER;
	public static int COMMAND_SHOW_EQUIP_TIME;

	public static void loadShowEquipConfig()
	{
		ExProperties show = load(COMMAND_SHOW_EQUIP);

		COMMAND_SHOW_EQUIP_ENABLED = show.getProperty("Enabled", false);
		COMMAND_SHOW_EQUIP_TIMER = show.getProperty("Timer", true);
		COMMAND_SHOW_EQUIP_TIME = show.getProperty("Time", 30);
	}

	public static boolean OFFLINE_BUFFER_STORE_ENABLED;
	public static boolean OFFLINE_BUFFER_STORE_MP_ENABLED;
	public static double OFFLINE_BUFFER_STORE_MP_CONSUME_MULTIPLIER;
	public static int OFFLINE_BUFFER_STORE_NAME_COLOR;
	public static int OFFLINE_BUFFER_STORE_TITLE_COLOR;
	public static List<Integer> OFFLINE_BUFFER_STORE_ALLOWED_CLASS_LIST;
	public static List<Integer> OFFLINE_BUFFER_STORE_FORBIDDEN_SKILL_LIST;
	public static long OFFLINE_BUFFER_STORE_SECONDS_TO_KICK;
	public static long[] OFFLINE_BUFFER_STORE_PRICE_DATA;
	public static double OFFLINE_BUFFER_STORE_FEE;
	public static int OFFLINE_BUFFER_STORE_TIMER;
	public static boolean OFFLINE_BUFFER_STORE_MP_RESTORE_ENABLED;
	public static long[] OFFLINE_BUFFER_STORE_MP_RESTORE_PRICE_DATA;

	public static void loadOffBuffConfig()
	{
		ExProperties offbuff = load(COMMAND_OFFLINE_BUFFER);

		OFFLINE_BUFFER_STORE_ENABLED = offbuff.getProperty("Enabled", false);
		OFFLINE_BUFFER_STORE_MP_ENABLED = offbuff.getProperty("Mp", true);
		OFFLINE_BUFFER_STORE_MP_CONSUME_MULTIPLIER = offbuff.getProperty("MpMultiplier", 1.0f);

		OFFLINE_BUFFER_STORE_NAME_COLOR = Util.parseColor(offbuff.getProperty("NameColor", "808080"));
		OFFLINE_BUFFER_STORE_TITLE_COLOR = Util.parseColor(offbuff.getProperty("TitleColor", "808080"));
		OFFLINE_BUFFER_STORE_PRICE_DATA = offbuff.getProperty("Price", new long[] { 1, Long.MAX_VALUE });

		OFFLINE_BUFFER_STORE_SECONDS_TO_KICK = offbuff.getProperty("DaysToKick", 14) * 86400L;
		OFFLINE_BUFFER_STORE_FEE = offbuff.getProperty("Fee", -10);
		OFFLINE_BUFFER_STORE_TIMER = offbuff.getProperty("Time", 40);
		OFFLINE_BUFFER_STORE_MP_RESTORE_ENABLED = offbuff.getProperty("MpRestore", true);
		OFFLINE_BUFFER_STORE_MP_RESTORE_PRICE_DATA = offbuff.getProperty("MpPrice", new long[] { 726, 5 });

		final String[] classes = offbuff.getProperty("ClassList", "").split(",");
		OFFLINE_BUFFER_STORE_ALLOWED_CLASS_LIST = new ArrayList<Integer>();
		if(classes.length > 0)
		{
			for(String classId : classes)
			{
				OFFLINE_BUFFER_STORE_ALLOWED_CLASS_LIST.add(Integer.parseInt(classId));
			}
		}

		final String[] skills = offbuff.getProperty("ForbiddenSkills", "").split(",");
		OFFLINE_BUFFER_STORE_FORBIDDEN_SKILL_LIST = new ArrayList<Integer>();
		if(skills.length > 0)
		{
			for(String skillId : skills)
			{
				OFFLINE_BUFFER_STORE_FORBIDDEN_SKILL_LIST.add(Integer.parseInt(skillId));
			}
		}
	}

	public static boolean COMMAND_BOT_REPORT_ENABLED;
	public static int COMMAND_BOT_REPORT_REUSE;
	public static int COMMAND_BOT_REPORT_ANSWER_TIME;
	public static boolean COMMAND_BOT_REPORT_PEACE;
	public static boolean COMMAND_BOT_REPORT_PVP;
	public static int[] COMMAND_BOT_REPORT_LEVELS;

	public static void loadBotReportConfig()
	{
		ExProperties report = load(COMMAND_BOT_REPORT);

		COMMAND_BOT_REPORT_ENABLED = report.getProperty("Enabled", false);
		COMMAND_BOT_REPORT_REUSE = report.getProperty("Reuse", 15);
		COMMAND_BOT_REPORT_ANSWER_TIME = report.getProperty("Answer", 60);
		COMMAND_BOT_REPORT_PEACE = report.getProperty("Peace", true);
		COMMAND_BOT_REPORT_PVP = report.getProperty("PvP", true);
		COMMAND_BOT_REPORT_LEVELS = report.getProperty("Levels", new int[] { 1, 85 });
	}

	/* PK System */
	public static boolean PK_SYSTEM_ENABLE;
	public static int[] PK_SYSTEM_KILLER_LEVEL_FOR_REWARD;
	public static int[] PK_SYSTEM_TARGET_LEVEL_FOR_REWARD;
	public static boolean PK_SYSTEM_ALLOW_REWARD;
	public static int[] PK_SYSTEM_ITEM_INFO;
	public static boolean PK_SYSTEM_ADD_EXP_SP;
	public static int[] PK_SYSTEM_EXP_SP;
	public static boolean PK_SYSTEM_ENABLE_BLOCK_TIME;
	public static int PK_SYSTEM_BLOCK_TIME_AFTER_KILL;
	public static boolean PK_SYSTEM_ANNOUNCE;
	public static int PK_SYSTEM_ANNOUNCE_RADIUS;
	public static boolean PK_SYSTEM_IN_ZONE;
	public static String PK_SYSTEM_ZONE;

	public static void loadPKSystemSettings()
	{
		ExProperties pk = load(PK_SYSTEM);

		PK_SYSTEM_ENABLE = pk.getProperty("Enable", false);
		PK_SYSTEM_KILLER_LEVEL_FOR_REWARD = pk.getProperty("Killer", new int[] { 20, 85 });
		PK_SYSTEM_TARGET_LEVEL_FOR_REWARD = pk.getProperty("Target", new int[] { 20, 85 });
		PK_SYSTEM_ALLOW_REWARD = pk.getProperty("Item", false);
		PK_SYSTEM_ITEM_INFO = pk.getProperty("Items", new int[] { 0, 0, 0 });
		PK_SYSTEM_ADD_EXP_SP = pk.getProperty("ExpSP", false);
		PK_SYSTEM_EXP_SP = pk.getProperty("ExpSpData", new int[] { 0, 0 });
		PK_SYSTEM_ENABLE_BLOCK_TIME = pk.getProperty("Time", false);
		PK_SYSTEM_BLOCK_TIME_AFTER_KILL = pk.getProperty("Kill", 900);
		PK_SYSTEM_ANNOUNCE = pk.getProperty("Announce", false);
		PK_SYSTEM_ANNOUNCE_RADIUS = pk.getProperty("Radius", 900);
		PK_SYSTEM_IN_ZONE = pk.getProperty("InZone", false);
		PK_SYSTEM_ZONE = pk.getProperty("Zone", "[colosseum_battle]");
	}

	/* PvP System */
	public static boolean PvP_SYSTEM_ENABLE;
	public static int[] PvP_SYSTEM_KILLER_LEVEL_FOR_REWARD;
	public static int[] PvP_SYSTEM_TARGET_LEVEL_FOR_REWARD;
	public static boolean PvP_SYSTEM_ALLOW_REWARD;
	public static int[] PvP_SYSTEM_ITEM_INFO;
	public static boolean PvP_SYSTEM_ADD_EXP_SP;
	public static int[] PvP_SYSTEM_EXP_SP;
	public static boolean PvP_SYSTEM_ENABLE_BLOCK_TIME;
	public static int PvP_SYSTEM_BLOCK_TIME_AFTER_KILL;
	public static boolean PvP_SYSTEM_ANNOUNCE;
	public static int PvP_SYSTEM_ANNOUNCE_RADIUS;
	public static boolean PvP_SYSTEM_IN_ZONE;
	public static String PvP_SYSTEM_ZONE;

	public static void loadPvPSystemSettings()
	{
		ExProperties pvp = load(PVP_SYSTEM);

		PvP_SYSTEM_ENABLE = pvp.getProperty("Enable", false);
		PvP_SYSTEM_KILLER_LEVEL_FOR_REWARD = pvp.getProperty("Killer", new int[] { 20, 85 });
		PvP_SYSTEM_TARGET_LEVEL_FOR_REWARD = pvp.getProperty("Target", new int[] { 20, 85 });
		PvP_SYSTEM_ALLOW_REWARD = pvp.getProperty("Item", false);
		PvP_SYSTEM_ITEM_INFO = pvp.getProperty("Items", new int[] { 0, 0, 0 });
		PvP_SYSTEM_ADD_EXP_SP = pvp.getProperty("ExpSP", false);
		PvP_SYSTEM_EXP_SP = pvp.getProperty("ExpSpData", new int[] { 0, 0 });
		PvP_SYSTEM_ENABLE_BLOCK_TIME = pvp.getProperty("Time", false);
		PvP_SYSTEM_BLOCK_TIME_AFTER_KILL = pvp.getProperty("Kill", 900);
		PvP_SYSTEM_ANNOUNCE = pvp.getProperty("Announce", false);
		PvP_SYSTEM_ANNOUNCE_RADIUS = pvp.getProperty("Radius", 900);
		PvP_SYSTEM_IN_ZONE = pvp.getProperty("InZone", false);
		PvP_SYSTEM_ZONE = pvp.getProperty("Zone", "[colosseum_battle]");
	}

	public static double VITALITY_RATE;
	public static double VITALITY_RECOVERY_RATE;
	public static int VITALITY_RECOVERY_TIME;
	public static int VITALITY_MAX_PLAYER_LEVEL;
	public static int VITALITY_MAX_POINTS;
	public static boolean VITALITY_LUCKY_SKILL;
	public static int[] VITALITY_LEVELS_POINT;
	public static double[] VITALITY_LEVELS_RATE;
	public static boolean VITALITY_DEBUG;

	public static void loadVitalitySettings()
	{
		ExProperties vitality = load(VITALITY);

		VITALITY_DEBUG = vitality.getProperty("Debug", false);
		VITALITY_RATE = vitality.getProperty("Rate", 1.0);
		VITALITY_RECOVERY_RATE = vitality.getProperty("Recovery", 4.0);
		VITALITY_RECOVERY_TIME = vitality.getProperty("Time", 5000);
		VITALITY_LEVELS_POINT = vitality.getProperty("Points", new int[] { 240, 3000, 13000, 17000, 20000 });
		VITALITY_LEVELS_RATE = vitality.getProperty("Levels", new double[] { 0.0, 0.5, 1.0, 1.5, 2.0 });
		VITALITY_LUCKY_SKILL = vitality.getProperty("Lucky", false);
		VITALITY_MAX_PLAYER_LEVEL = vitality.getProperty("MaxLevel", 9);
		VITALITY_MAX_POINTS = vitality.getProperty("MaxPoints", 20000);
	}

	/* Tops */
	public static boolean L2TOPENABLE;
	public static String L2TOPSMS;
	public static String L2TOPWEB;
	public static int L2TOPREFRESHTIME;
	public static String[] L2TOP_REWARD_WEB_ARRAY;
	public static String[] L2TOP_REWARD_SMS_ARRAY;
	public static String L2_TOP_SERVER_PREFIX;

	public static void loadL2TopSettings()
	{
		ExProperties top = load(L2TOP_FILE);

		L2TOPENABLE = top.getProperty("Enable", false);
		L2TOPSMS = top.getProperty("SmS", "");
		L2TOPWEB = top.getProperty("Web", "");
		L2TOPREFRESHTIME = top.getProperty("RefreshTime", 10);
		L2TOP_REWARD_WEB_ARRAY = top.getProperty("RewardWeb", "{(57[1,1])100.0};").split(";");
		L2TOP_REWARD_SMS_ARRAY = top.getProperty("RewardSms", "{(57[1,4])100.0};").split(";");
		L2_TOP_SERVER_PREFIX = top.getProperty("ServerPrefix", "");
	}

	public static boolean MMOTOPENABLE;
	public static String MMOTOPURL;
	public static int MMOTOPREFRESHTIME;
	public static String[] MMOTOP_REWARD_WEB_ARRAY;
	public static String[] MMOTOP_REWARD_SMS_ARRAY;

	public static void loadMMOTopSettings()
	{
		ExProperties top = load(MMOTOP_FILE);

		MMOTOPENABLE = top.getProperty("Enable", false);
		MMOTOPURL = top.getProperty("Url", "");
		MMOTOPREFRESHTIME = top.getProperty("RefreshTime", 10);
		MMOTOP_REWARD_WEB_ARRAY = top.getProperty("RewardWeb", "{(57[1,1])100.0};").split(";");
		MMOTOP_REWARD_SMS_ARRAY = top.getProperty("RewardSms", "{(57[1,4])100.0};").split(";");
	}

	public static boolean MMTOPENABLE;
	public static String MMTOPURL;
	public static int MMTOPREFRESHTIME;
	public static String[] MMTOP_REWARD_WEB_ARRAY;
	public static String[] MMTOP_REWARD_SMS_ARRAY;

	public static void loadMMTopSettings()
	{
		ExProperties top = load(MMTOP_FILE);

		MMTOPENABLE = top.getProperty("Enable", false);
		MMTOPURL = top.getProperty("Url", "");
		MMTOPREFRESHTIME = top.getProperty("RefreshTime", 10);
		MMTOP_REWARD_WEB_ARRAY = top.getProperty("RewardWeb", "{(57[1,1])100.0};").split(";");
		MMTOP_REWARD_SMS_ARRAY = top.getProperty("RewardSms", "{(57[1,4])100.0};").split(";");
	}

	public static void load()
	{
		loadServerConfig();
		loadTelnetConfig();
		loadServicesBonusSettings();
		loadResidenceConfig();
		loadClanHallConfig();
		loadOtherConfig();
		loadSpoilConfig();
		loadFormulasConfig();
		loadAltSettings();
		loadAfkSettings();
		loadServicesSettings();
		loadPvPSettings();
		loadAISettings();
		loadGeodataSettings();
		loadEventsSettings();
		loadOlympiadSettings();
		loadDevelopSettings();
		loadExtSettings();
		loadLogSettings();

		// Alternative
		loadClanSettings();
		loadPlayerCreateSettings();
		loadEnterWorldSettings();
		loadEnchantSettings();
		loadRateSettings();
		loadRaidSettings();
		loadPremiumSettings();

		// Community
		loadCommunityBoardSettings();
		loadBufferSettings();
		loadTeleportSettings();
		loadStatisticSettings();
		loadForgeSettings();
		loadCabinetSettings();
		loadRecruitmentSettings();
		loadDropBaseSettings();
		loadBossesBaseSettings();
		loadCarrierSettings();

		loadChatFilters();
		loadGMAccess();

		loadShowEquipConfig();
		loadSummonClanConfig();
		loadOffBuffConfig();
		loadBotReportConfig();

		loadPKSystemSettings();
		loadPvPSystemSettings();

		loadVitalitySettings();

		// Tops
		loadL2TopSettings();
		loadMMOTopSettings();
		loadMMTopSettings();
	}

	private Config()
	{}

	public static void loadChatFilters()
	{
		ChatFilterParser.getInstance().reload();
	}

	public static void loadGMAccess()
	{
		gmlist.clear();
		loadGMAccess(new File(GM_PERSONAL_ACCESS_FILE));
		File dir = new File(GM_ACCESS_FILES_DIR);
		if(!dir.exists() || !dir.isDirectory())
		{
			_log.info("Dir " + dir.getAbsolutePath() + " not exists.");
			return;
		}
		for(File f : dir.listFiles())
			// hidden файлы НЕ игнорируем
			if(!f.isDirectory() && f.getName().endsWith(".xml"))
				loadGMAccess(f);
	}

	public static void loadGMAccess(File file)
	{
		try
		{
			Field fld;
			//File file = new File(filename);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setIgnoringComments(true);
			Document doc = factory.newDocumentBuilder().parse(file);

			for(Node z = doc.getFirstChild(); z != null; z = z.getNextSibling())
				for(Node n = z.getFirstChild(); n != null; n = n.getNextSibling())
				{
					if(!n.getNodeName().equalsIgnoreCase("char"))
						continue;

					PlayerAccess pa = new PlayerAccess();
					for(Node d = n.getFirstChild(); d != null; d = d.getNextSibling())
					{
						Class<?> cls = pa.getClass();
						String node = d.getNodeName();

						if(node.equalsIgnoreCase("#text"))
							continue;
						try
						{
							fld = cls.getField(node);
						}
						catch(NoSuchFieldException e)
						{
							_log.info("Not found desclarate ACCESS name: " + node + " in XML Player access Object");
							continue;
						}

						if(fld.getType().getName().equalsIgnoreCase("boolean"))
							fld.setBoolean(pa, Boolean.parseBoolean(d.getAttributes().getNamedItem("set").getNodeValue()));
						else if(fld.getType().getName().equalsIgnoreCase("int"))
							fld.setInt(pa, Integer.valueOf(d.getAttributes().getNamedItem("set").getNodeValue()));
					}
					gmlist.put(pa.PlayerID, pa);
				}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static String getField(String fieldName)
	{
		Field field = FieldUtils.getField(Config.class, fieldName);

		if(field == null)
			return null;

		try
		{
			return String.valueOf(field.get(null));
		}
		catch(IllegalArgumentException e)
		{

		}
		catch(IllegalAccessException e)
		{

		}

		return null;
	}

	public static boolean setField(String fieldName, String value)
	{
		Field field = FieldUtils.getField(Config.class, fieldName);

		if(field == null)
			return false;

		try
		{
			if(field.getType() == boolean.class)
				field.setBoolean(null, BooleanUtils.toBoolean(value));
			else if(field.getType() == int.class)
				field.setInt(null, NumberUtils.toInt(value));
			else if(field.getType() == long.class)
				field.setLong(null, NumberUtils.toLong(value));
			else if(field.getType() == double.class)
				field.setDouble(null, NumberUtils.toDouble(value));
			else if(field.getType() == String.class)
				field.set(null, value);
			else
				return false;
		}
		catch(IllegalArgumentException e)
		{
			return false;
		}
		catch(IllegalAccessException e)
		{
			return false;
		}

		return true;
	}

	public static ExProperties load(String filename)
	{
		return load(new File(filename));
	}

	public static ExProperties load(File file)
	{
		ExProperties result = new ExProperties();

		try
		{
			result.load(file);
		}
		catch(IOException e)
		{
			_log.error("Error loading config : " + file.getName() + "!");
		}

		return result;
	}

	private static boolean isPowerOfTwo(int n)
	{
		return ((n != 0) && (n & (n - 1)) == 0);
	}
}