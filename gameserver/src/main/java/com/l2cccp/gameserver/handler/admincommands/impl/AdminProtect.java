package com.l2cccp.gameserver.handler.admincommands.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.CharacterDAO;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.utils.OnlineUtils;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

import ru.akumu.smartguard.manager.LicenseManager;
import ru.akumu.smartguard.manager.UpdateManager;
import ru.akumu.smartguard.manager.bans.BanManager;
import ru.akumu.smartguard.manager.bans.model.Ban;
import ru.akumu.smartguard.manager.session.ClientSessionManager;
import ru.akumu.smartguard.manager.session.model.ClientSession;
import ru.akumu.smartguard.manager.session.model.HWID;
import ru.akumu.smartguard.model.TimedObject;
import ru.akumu.smartguard.utils.log.GuardLog;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class AdminProtect implements IAdminCommandHandler
{
	private static SimpleDateFormat ban_format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
	private static SimpleDateFormat license = new SimpleDateFormat("dd.MM.yyyy");

	private static enum Commands
	{
		admin_protect,
		admin_protect_ban,
		admin_protect_unban,
		admin_protect_show_bans,
		admin_protect_info
	}

	private static enum Type
	{
		player,
		hwid,
		account;
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player player)
	{
		if(player.getPlayerAccess().CanBan)
		{
			Commands command = (Commands) comm;
			StringTokenizer st = new StringTokenizer(fullString, " ");
			st.nextToken(); // command

			switch(command)
			{
				case admin_protect:
				{
					final int page = st.hasMoreTokens() ? Integer.parseInt(st.nextToken()) : 1;
					showMain(player, page);
					return true;
				}
				case admin_protect_ban:
				{
					if(st.hasMoreTokens())
					{
						final Type type = Type.valueOf(st.nextToken());
						final long time = TimeUtils.considerMinutes(Integer.parseInt(st.nextToken()));
						final String owner = st.nextToken();
						String reason = "NO REASON";
						if(st.hasMoreTokens())
						{
							reason = st.nextToken();
							while(st.hasMoreTokens())
								reason += " " + st.nextToken();
						}

						showMain(player, 1);
						switch(type)
						{
							case account:
								break;
							case hwid:
							{
								HWID hwid = HWID.fromString(owner);
								if(hwid == null)
								{
									player.sendMessage(String.format("Hwid '%s' has bad format.", owner));
									return false;
								}
								else
								{
									Ban ban = new Ban(hwid, time, reason);
									BanManager.addBan(ban);

									player.sendMessage(String.format("Hwid '%s' has been banned.", new Object[] { hwid }));

									ClientSession session = ClientSessionManager.getSession(hwid);
									if(session != null)
										session.disconnect();
									GuardLog.getLogger().info(String.format("Admin '%s' has banned HWID %S, reason: '%s'", player.getName(), hwid, reason));
									return true;
								}

							}
							case player:
							{
								Player target = GameObjectsStorage.getPlayer(owner);
								if(target == null)
								{
									player.sendMessage(String.format("Player '%s' was not found!", owner));
									return false;
								}
								else
								{
									ClientSession session = ClientSessionManager.getSession(target.getNetConnection());
									if(session == null)
									{
										String error = String.format("Error! Session for player '%s' does not exist!", owner);
										player.sendMessage(error);
										GuardLog.getLogger().severe(error);
										return false;
									}
									else
									{
										Ban ban = new Ban(session.hwid, time, reason);
										BanManager.addBan(ban);

										player.sendMessage(String.format("Hwid %s has been banned.", session.hwid()));

										session.disconnect();

										GuardLog.getLogger().info(String.format("Admin '%s' has banned player '%s' by hwid. HWID: %S", player.getName(), owner, session.hwid()));
										return true;
									}
								}
							}
						}
					}
				}
				case admin_protect_unban:
				{
					if(st.hasMoreTokens())
					{
						final String hwid = st.nextToken();
						BanManager.removeBan(hwid);

						player.sendMessage(String.format("Hwid %s has been un-banned.", hwid));
						GuardLog.getLogger().info(String.format("Admin '%s' has removed ban from HWID %S", player.getName(), hwid));
						return true;
					}
				}
				case admin_protect_show_bans:
				{
					final int page = st.hasMoreTokens() ? Integer.parseInt(st.nextToken()) : 1;
					final String html = buildBans(player, page);
					ShowBoard.separateAndSend(html, player);
					return true;
				}
				case admin_protect_info:
				{
					if(st.hasMoreTokens())
					{
						useAdminCommand(Commands.admin_protect_show_bans, wordList, "page 1", player);
						final String hwid = st.nextToken();
						final Ban ban = BanManager.getBan(hwid);
						if(ban != null)
						{
							HtmlMessage html = new HtmlMessage(5);
							html.setFile(Config.BBS_PATH + "/protect/info.htm");

							final int perpage = 11;
							int counter = 0;
							for(final String account : ban.getAccountNames())
							{
								html.replace("%param_" + counter + "%", "Account");
								html.replace("%value_" + counter + "%", account);
								counter++;
							}

							for(final TimedObject<String> saccount : ban.sessionAccounts)
							{
								html.replace("%param_" + counter + "%", "Session Account");
								html.replace("%value_" + counter + "%", saccount.value);
								counter++;
							}

							for(final TimedObject<String> sip : ban.sessionIPs)
							{
								html.replace("%param_" + counter + "%", "Session IP");
								html.replace("%value_" + counter + "%", sip.value);
								counter++;
							}

							while(counter < perpage)
							{
								html.replace("%param_" + counter + "%", "Empty slot");
								html.replace("%value_" + counter + "%", "...");
								counter++;
							}

							html.replace("%hwid%", ban.hwid.toString());
							html.replace("%end%", ban_format.format(ban.time));
							player.sendPacket(html);
							return true;
						}
						else
							return false;
					}
				}
			}
		}

		showMain(player, 1);
		return false;
	}

	private void showMain(final Player player, final int page)
	{
		calcClear();
		String html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/protect/index.htm", player);
		final long end = UpdateManager.getInstance().LicenseExpiry;
		html = html.replace("{lic_id}", String.valueOf(LicenseManager.getInstance().LicenseID));
		html = html.replace("{lic_token}", String.valueOf(LicenseManager.getInstance().LicenseToken));
		html = html.replace("{lic_time}", license.format(end));
		html = html.replace("{lic_color}", end >= System.currentTimeMillis() ? "669900" : "CC0000");
		html = html.replace("{online_all}", OnlineUtils.getformat(true));
		html = html.replace("{online_clear}", Util.formatAdena(clear_online));
		html = html.replace("{bans}", Util.formatAdena(BanManager.getBans().size()));

		ShowBoard.separateAndSend(html, player);
	}

	private int clear_online;
	private long last_update;

	private void calcClear()
	{
		if(last_update < (System.currentTimeMillis() + TimeUtils.addMinutes(5)))
		{
			clear_online = 0;
			last_update = System.currentTimeMillis();
			List<String> online = new ArrayList<String>();
			for(final Player player : GameObjectsStorage.getPlayers())
			{
				final GameClient client = player.getNetConnection();
				if(client != null)
				{
					final String hwid = client.getHWID();
					if(!online.contains(hwid))
						clear_online++;
				}
			}
		}
	}

	private String buildBans(final Player player, final int page)
	{
		List<Ban> bans = new ArrayList<Ban>(BanManager.getBans());

		String banned = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/protect/banned.htm", player);

		final int perpage = 15;
		int counter = 0;
		int skip = (page - 1) * perpage;
		skip = skip >= bans.size() ? 0 : skip;
		for(final Ban ban : bans)
		{
			if(skip-- > 0)
				continue;

			banned = banned.replace("{color_" + counter + "}", counter % 2 == 1 ? "333333" : "666666");
			banned = banned.replace("{info_" + counter + "}", "bypass admin_protect_info");
			final String comment = ban.comment;
			banned = banned.replace("{comment_" + counter + "}", comment == null ? "No Reason" : comment);
			banned = banned.replace("{hwid_" + counter + "}", ban.hwid.toString());
			final String gm = CharacterDAO.getInstance().getNameByObjectId(ban.gmObjId);
			banned = banned.replace("{gm_" + counter + "}", gm.equals("") ? "SYSTEM" : gm);
			banned = banned.replace("{time_" + counter + "}", ban_format.format(ban.time));
			banned = banned.replace("{type_" + counter + "}", ban.type.name());
			counter++;
		}

		while(counter <= perpage)
		{
			banned = banned.replace("{color_" + counter + "}", counter % 2 == 1 ? "333333" : "666666");
			banned = banned.replace("{info_" + counter + "}", "");
			banned = banned.replace("{comment_" + counter + "}", "...");
			banned = banned.replace("{hwid_" + counter + "}", "...");
			banned = banned.replace("{gm_" + counter + "}", "...");
			banned = banned.replace("{time_" + counter + "}", "...");
			banned = banned.replace("{type_" + counter + "}", "...");
			counter++;
		}

		double count = Math.ceil((double) bans.size() / (double) perpage); // Используем округление для получения последней страницы с остатком!
		int inline = 1;
		String navigation = "";
		for(int i = 1; i <= count; i++)
		{
			if(i == page)
				navigation += "<td width=25 align=center valign=top><button value=\"[" + i + "]\" action=\"bypass admin_protect_show_bans " + i + "\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";
			else
				navigation += "<td width=25 align=center valign=top><button value=\"" + i + "\" action=\"bypass admin_protect_show_bans " + i + "\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";

			if(inline % 7 == 0)
				navigation += "</tr><tr>";

			inline++;
		}

		if(inline == 2)
			navigation = "<td width=30 align=center valign=top>...</td>";

		banned = banned.replace("{navigation}", navigation);
		return banned;
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}
}