package com.l2cccp.gameserver.handler.voicecommands;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.handler.voicecommands.impl.Augments;
import com.l2cccp.gameserver.handler.voicecommands.impl.BanChat;
import com.l2cccp.gameserver.handler.voicecommands.impl.BotReport;
import com.l2cccp.gameserver.handler.voicecommands.impl.Cfg;
import com.l2cccp.gameserver.handler.voicecommands.impl.CombineTalismans;
import com.l2cccp.gameserver.handler.voicecommands.impl.Debug;
import com.l2cccp.gameserver.handler.voicecommands.impl.Hellbound;
import com.l2cccp.gameserver.handler.voicecommands.impl.Help;
import com.l2cccp.gameserver.handler.voicecommands.impl.Loc;
import com.l2cccp.gameserver.handler.voicecommands.impl.OffBuff;
import com.l2cccp.gameserver.handler.voicecommands.impl.Offline;
import com.l2cccp.gameserver.handler.voicecommands.impl.Ping;
import com.l2cccp.gameserver.handler.voicecommands.impl.Premium;
import com.l2cccp.gameserver.handler.voicecommands.impl.Repair;
import com.l2cccp.gameserver.handler.voicecommands.impl.ServerInfo;
import com.l2cccp.gameserver.handler.voicecommands.impl.Wedding;
import com.l2cccp.gameserver.handler.voicecommands.impl.WhoAmI;

public class VoicedCommandHandler extends AbstractHolder
{
	private static final VoicedCommandHandler _instance = new VoicedCommandHandler();

	public static VoicedCommandHandler getInstance()
	{
		return _instance;
	}

	private Map<String, IVoicedCommandHandler> _datatable = new HashMap<String, IVoicedCommandHandler>();

	private VoicedCommandHandler()
	{
		registerVoicedCommandHandler(new Help());
		registerVoicedCommandHandler(new Hellbound());
		registerVoicedCommandHandler(new BotReport());
		registerVoicedCommandHandler(new Cfg());
		registerVoicedCommandHandler(new Offline());
		registerVoicedCommandHandler(new Premium());
		registerVoicedCommandHandler(new Repair());
		registerVoicedCommandHandler(new ServerInfo());
		registerVoicedCommandHandler(new Wedding());
		registerVoicedCommandHandler(new WhoAmI());
		registerVoicedCommandHandler(new Debug());
		registerVoicedCommandHandler(new Augments());
		registerVoicedCommandHandler(new CombineTalismans());
		registerVoicedCommandHandler(new BanChat());
		registerVoicedCommandHandler(new Loc());
		registerVoicedCommandHandler(new Ping());
		registerVoicedCommandHandler(new OffBuff());
	}

	public void registerVoicedCommandHandler(IVoicedCommandHandler handler)
	{
		String[] ids = handler.getVoicedCommandList();
		for(String element : ids)
			_datatable.put(element, handler);
	}

	public IVoicedCommandHandler getVoicedCommandHandler(String voicedCommand)
	{
		String command = voicedCommand;
		if(voicedCommand.indexOf(" ") != -1)
			command = voicedCommand.substring(0, voicedCommand.indexOf(" "));

		return _datatable.get(command);
	}

	@Override
	public int size()
	{
		return _datatable.size();
	}

	@Override
	public void clear()
	{
		_datatable.clear();
	}
}
