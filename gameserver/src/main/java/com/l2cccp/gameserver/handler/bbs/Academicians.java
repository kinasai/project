package com.l2cccp.gameserver.handler.bbs;

public class Academicians
{
	private long time;
	private int objId;
	private int clanId;

	public Academicians(long time, int objId, int clanId)
	{
		this.time = time;
		this.objId = objId;
		this.clanId = clanId;
		AcademiciansStorage.getInstance().get().add(this);
	}

	public long getTime()
	{
		return time;
	}

	public int getObjId()
	{
		return objId;
	}

	public int getClanId()
	{
		return clanId;
	}
}
