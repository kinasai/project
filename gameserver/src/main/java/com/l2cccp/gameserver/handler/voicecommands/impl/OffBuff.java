package com.l2cccp.gameserver.handler.voicecommands.impl;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;

public class OffBuff implements IVoicedCommandHandler
{
	private static final String[] VOICED_COMMANDS = { "offbuff" };

	@Override
	public boolean useVoicedCommand(String command, Player activeChar, String params)
	{
		try
		{
			if(!Config.OFFLINE_BUFFER_STORE_ALLOWED_CLASS_LIST.contains(activeChar.getClassId().getId()))
			{
				activeChar.sendMessage("Your profession is not allowed to set an Buff Store");
				return false;
			}

			final HtmlMessage html = new HtmlMessage(5);
			html.setFile("command/buffstore/store.htm");
			if(activeChar.getPrivateStoreType() == Player.STORE_PRIVATE_BUFF)
			{
				html.replace("%link%", "Stop Store");
				html.replace("%bypass%", "bypass -h offbuff stopstore");
			}
			else
			{
				html.replace("%link%", "Create Store");
				html.replace("%bypass%", "bypass -h player_help command/buffstore/create.htm");
			}
			activeChar.sendPacket(html);

			return true;
		}
		catch(Exception e)
		{
			activeChar.sendMessage("Use: .offbuff");
		}

		return false;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return VOICED_COMMANDS;
	}
}
