package com.l2cccp.gameserver.handler.admincommands.impl;

import com.l2cccp.gameserver.Announcements;
import com.l2cccp.gameserver.data.xml.holder.AnnouncementHolder;
import com.l2cccp.gameserver.data.xml.parser.AnnouncementParser;
import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.NpcString;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.utils.Util;

/**
 * This class handles following admin commands: - announce text = announces text
 * to all players - list_announcements = show menu - reload_announcements =
 * reloads announcements from txt file - announce_announcements = announce all
 * stored announcements to all players - add_announcement text = adds text to
 * startup announcements - del_announcement id = deletes announcement with
 * respective id
 */
public class AdminAnnouncements implements IAdminCommandHandler
{
	private static enum Commands
	{
		admin_announce,
		admin_a,
		admin_crit_announce,
		admin_c,
		admin_toscreen,
		admin_s,
		admin_reload_announcements,
		admin_a_stop
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player activeChar)
	{
		Commands command = (Commands) comm;

		if(!activeChar.getPlayerAccess().CanAnnounce)
			return false;

		switch(command)
		{
			case admin_announce:
				Announcements.getInstance().announceToAll(fullString.substring(15));
				break;
			case admin_a:
				Announcements.getInstance().announceToAll(fullString.substring(8));
				break;
			case admin_crit_announce:
			case admin_c:
				if(wordList.length < 2)
					return false;
				Announcements.getInstance().announceToAll(activeChar.getName() + ": " + fullString.replaceFirst("admin_crit_announce ", "").replaceFirst("admin_c ", ""), ChatType.CRITICAL_ANNOUNCE);
				break;
			case admin_toscreen:
			case admin_s:
				if(wordList.length < 2)
					return false;
				String text = activeChar.getName() + ": " + fullString.replaceFirst("admin_toscreen ", "").replaceFirst("admin_s ", "");
				int time = 3000 + text.length() * 100; // 3 секунды + 100мс на символ
				ExShowScreenMessage sm = new ExShowScreenMessage(NpcString.NONE, time, ScreenMessageAlign.TOP_CENTER, text.length() < 64, text);
				for(Player player : GameObjectsStorage.getPlayers())
					player.sendPacket(sm);
				break;
			case admin_reload_announcements:
				AnnouncementParser.getInstance().reload();
				activeChar.sendMessage("Announcements reloaded.");
				break;
			case admin_a_stop:
				if(wordList.length < 2)
					return false;

				if(!Util.isNumber(wordList[1]))
					return false;

				final int id = Integer.parseInt(wordList[1]);
				AnnouncementHolder.getInstance().stop(id);
		}

		return true;
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}
}