package com.l2cccp.gameserver.handler.bbs;

public class AcademyRequest
{
	private int time;
	private int clanId;
	private int seats;
	private long price;
	private int item;

	public AcademyRequest(int time, int clanId, int seats, long price, int item)
	{
		this.time = time;
		this.clanId = clanId;
		this.seats = seats;
		this.price = price;
		this.item = item;
		AcademyStorage.getInstance().get().add(this);
		AcademyStorage.getInstance().updateList();
	}

	public int getTime()
	{
		return time;
	}

	public int getClanId()
	{
		return clanId;
	}

	public long getPrice()
	{
		return price;
	}

	public int getItem()
	{
		return item;
	}

	public int getSeats()
	{
		return seats;
	}

	public void updateSeats()
	{
		seats++;
	}

	public void reduceSeats()
	{
		seats--;
	}
}
