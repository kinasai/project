package com.l2cccp.gameserver.handler.voicecommands.impl;

import com.l2cccp.gameserver.data.xml.holder.OptionDataHolder;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.Inventory;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.stats.triggers.TriggerInfo;
import com.l2cccp.gameserver.templates.OptionDataTemplate;

public class Augments implements IVoicedCommandHandler
{
	private final String[] _commandList = new String[] { "augments" };

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, Player player, String args)
	{
		for (int slot = 0; slot < Inventory.PAPERDOLL_MAX; slot++)
		{
			int[] augmentations = player.getInventory().getPaperdollAugmentationId(slot);
			if(augmentations == ItemInstance.EMPTY_AUGMENTATIONS)
				continue;

			StringBuilder info = new StringBuilder(30);
			info.append(slot);
			info.append(" ");
			info.append(augmentations[0]);
			info.append(":");
			info.append(augmentations[1]);

			getInfo(info, augmentations[0]);
			getInfo(info, augmentations[1]);
			player.sendMessage(info.toString());
		}
		return true;
	}

	private void getInfo(StringBuilder info, int id)
	{
		OptionDataTemplate template = OptionDataHolder.getInstance().getTemplate(id);
		if (template != null)
		{
			if (!template.getSkills().isEmpty())
				for (SkillEntry s : template.getSkills())
				{
					info.append(" ");
					info.append(s.getId());
					info.append("/");
					info.append(s.getLevel());
				}
			if (!template.getTriggerList().isEmpty())
				for (TriggerInfo t : template.getTriggerList())
				{
					info.append(" ");
					info.append(t.id);
					info.append("/");
					info.append(t.level);
					info.append(" ");
					info.append(t.getType());
					info.append(":");
					info.append(t.getChance());
				}
		}
	}
}