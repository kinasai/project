package com.l2cccp.gameserver.handler.bbs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.l2cccp.gameserver.dao.AcademyRequestDAO;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.tables.ClanTable;

public class AcademyStorage
{
	private static final AcademyStorage _instance = new AcademyStorage();

	public static AcademyStorage getInstance()
	{
		return _instance;
	}

	private static List<Clan> clanList = new ArrayList<Clan>();
	private static List<AcademyRequest> academy = new ArrayList<AcademyRequest>();

	public void updateList()
	{
		AcademyRequest[] list = academy.toArray(new AcademyRequest[academy.size()]);

		Arrays.sort(list, new Comparator<AcademyRequest>(){
			@Override
			public int compare(AcademyRequest o1, AcademyRequest o2)
			{
				return (int) (o2.getPrice() - o1.getPrice());
			}
		});

		clanList.clear();
		for(AcademyRequest request : list)
		{
			if(request.getSeats() > 0)
			{
				Clan clan = ClanTable.getInstance().getClan(request.getClanId());
				if(clan != null)
					clanList.add(clan);
			}
			else
			{
				AcademyRequestDAO.getInstance().delete(request.getClanId());
				academy.remove(request);
				Clan clan = ClanTable.getInstance().getClan(request.getClanId());
				clanList.remove(clan);
				request = null;
			}
		}
	}

	public List<Clan> getAcademyList()
	{
		return clanList;
	}

	public AcademyRequest getReguest(int clan)
	{
		for(AcademyRequest request : academy)
		{
			if(request.getClanId() == clan)
				return request;
		}

		return null;
	}

	public List<AcademyRequest> get()
	{
		return academy;
	}
}
