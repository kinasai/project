package com.l2cccp.gameserver.handler.voicecommands.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.l2cccp.gameserver.data.xml.holder.PremiumHolder;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.Bonus;
import com.l2cccp.gameserver.model.premium.PremiumAccount;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;

public class Premium implements IVoicedCommandHandler
{
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy, HH:mm");
	private String[] _commandList = new String[] { "premium" };

	@Override
	public boolean useVoicedCommand(String command, Player player, String args)
	{
		HtmlMessage dialog = new HtmlMessage(5);
		dialog.setFile("command/premium.htm");

		int i = 1;
		final Bonus bonus = player.getBonus();
		for(final PremiumAccount premium : PremiumHolder.getInstance().getAllPremiums())
		{
			dialog.replace("%name_" + i + "%", premium.getName());
			final String status = "%status_" + i + "%";
			if(bonus.isActive(premium.getId()))
				dialog.replace(status, DATE_FORMAT.format(new Date(bonus.getExpire(premium))));
			else
				dialog.replace(status, "<font color=\"FF1111\">Not Active</font>");

			i++;
		}

		player.sendPacket(dialog);
		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}