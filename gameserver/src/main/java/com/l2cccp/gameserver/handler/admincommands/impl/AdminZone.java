package com.l2cccp.gameserver.handler.admincommands.impl;

import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.ResidenceHolder;
import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.instancemanager.MapRegionManager;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.entity.residence.Castle;
import com.l2cccp.gameserver.templates.mapregion.DomainArea;

public class AdminZone implements IAdminCommandHandler
{
	private static List<String> polygons = new ArrayList<String>();

	private static enum Commands
	{
		admin_zone_check,
		admin_region,
		admin_pos,
		admin_vis_count,
		admin_domain,
		admin_polygon,
		admin_polygon_clear,
		admin_polygon_save
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player activeChar)
	{
		Commands command = (Commands) comm;

		if(activeChar == null || !activeChar.getPlayerAccess().CanTeleport)
			return false;

		switch(command)
		{
			case admin_zone_check:
			{
				activeChar.sendMessage("Current region: " + activeChar.getCurrentRegion());
				activeChar.sendMessage("Zone list:");
				List<Zone> zones = new ArrayList<Zone>();
				World.getZones(zones, activeChar.getLoc(), activeChar.getReflection());
				for(Zone zone : zones)
					activeChar.sendMessage(zone.getType().toString() + ", name: " + zone.getName() + ", state: " + (zone.isActive() ? "active" : "not active") + ", inside: " + zone.checkIfInZone(activeChar) + "/" + zone.checkIfInZone(activeChar.getX(), activeChar.getY(), activeChar.getZ()));

				break;
			}
			case admin_region:
			{
				activeChar.sendMessage("Current region: " + activeChar.getCurrentRegion());
				activeChar.sendMessage("Objects list:");
				for(GameObject o : activeChar.getCurrentRegion())
					if(o != null)
						activeChar.sendMessage(o.toString());
				break;
			}
			case admin_vis_count:
			{
				activeChar.sendMessage("Current region: " + activeChar.getCurrentRegion());
				activeChar.sendMessage("Players count: " + World.getAroundPlayers(activeChar).size());
				break;
			}
			case admin_pos:
			{
				String pos = activeChar.getX() + ", " + activeChar.getY() + ", " + activeChar.getZ() + ", " + activeChar.getHeading() + " Geo [" + (activeChar.getX() - World.MAP_MIN_X >> 4) + ", " + (activeChar.getY() - World.MAP_MIN_Y >> 4) + "] Ref " + activeChar.getReflectionId();
				activeChar.sendMessage("Pos: " + pos);
				break;
			}
			case admin_domain:
			{
				DomainArea domain = MapRegionManager.getInstance().getRegionData(DomainArea.class, activeChar);
				Castle castle = domain != null ? ResidenceHolder.getInstance().getResidence(Castle.class, domain.getId()) : null;
				if(castle != null)
					activeChar.sendMessage("Domain: " + castle.getName());
				else
					activeChar.sendMessage("Domain: Unknown");
			}
			case admin_polygon:
			{
				final String pos = activeChar.getX() + " " + activeChar.getY() + " " + (activeChar.getZ() - 750) + "" + (activeChar.getZ() + 750);
				polygons.add(pos);
				activeChar.sendMessage("Cords: " + pos + " is save!");
				break;
			}
			case admin_polygon_clear:
			{
				activeChar.sendMessage("All cords is removed: " + polygons.size() + " cords!");
				polygons.clear();
				break;
			}
			case admin_polygon_save:
			{
				activeChar.sendMessage("Polygon is save: " + polygons.size() + " cords!");
				buildPolygon();
				break;
			}
		}
		return true;
	}

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

	private void buildPolygon()
	{
		Document document = DocumentHelper.createDocument();
		document.addDocType("list", null, "zone.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");

		Element element = document.addElement("list");

		Element zone = element.addElement("zone");
		zone.addAttribute("name", "[AutoBuild_" + Rnd.get(100000) + "]");
		zone.addAttribute("type", "battle_zone");
		Element polygon = zone.addElement("polygon");
		for(String loc : polygons)
		{
			Element coords = polygon.addElement("coords");
			coords.addAttribute("loc", loc);
		}

		try
		{
			String path = Config.DATAPACK_ROOT + "/polygon.xml";

			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
			prettyPrint.setIndent("\t");
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
			xmlWriter.write(document);
			xmlWriter.close();
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}

		polygons.clear();
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}
}