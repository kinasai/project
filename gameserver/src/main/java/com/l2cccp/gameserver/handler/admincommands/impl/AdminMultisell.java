package com.l2cccp.gameserver.handler.admincommands.impl;

import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.l2cccp.gameserver.data.xml.holder.ArmorSetsHolder;
import com.l2cccp.gameserver.data.xml.holder.HennaHolder;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.data.xml.holder.SkillAcquireHolder;
import com.l2cccp.gameserver.model.ArmorSet;
import com.l2cccp.gameserver.model.SkillLearn;
import com.l2cccp.gameserver.templates.Henna;
import com.l2cccp.gameserver.templates.item.ArmorTemplate.ArmorType;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.templates.item.ItemTemplate.Grade;
import com.l2cccp.gameserver.templates.item.ItemTemplate.ItemClass;
import com.l2cccp.gameserver.templates.item.WeaponTemplate.WeaponType;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class AdminMultisell
{
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");
	private static final AtomicInteger multisell = new AtomicInteger(100048);

	public static void parse()
	{
		//		books(false, false);
		//		books(false, true);
		//		weapons(false, false);
		//		weapons(true, false);
		//		armors(false, false);
		//		armors(true, false);
		//		jewels(false, false);
		//		jewels(true, false);
		//		cloaks(false, false);
		//		cloaks(true, false);
		//		agathions(false, false);
		//		agathions(false, true);
		//		decoration(false, false);
		//		decoration(false, true);
		//		shots(false);
		//		hennas(false);
		//
		//		multisell.set(99999);
		//		weapons(false, true);
		//		weapons(true, true);
		//		armors(false, true);
		//		armors(true, true);
		//		jewels(false, true);
		//		jewels(true, true);
		//		cloaks(false, true);
		//		cloaks(true, true);
		//		shots(true);
		//		hennas(true);
	}

	public static void weapons(final boolean masterwork, final boolean obt)
	{
		for(final WeaponType type : WeaponType.values())
		{
			if(type == WeaponType.NONE || type == WeaponType.ETC || type == WeaponType.FIST || type == WeaponType.PET || type == WeaponType.ROD)
				continue;

			org.dom4j.Document document = DocumentHelper.createDocument();
			document.addDocType("list", null, "multisell.dtd");
			Date date = new Date();
			document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
			document.addComment(" Site: http://l2cccp.com/ ");
			document.addComment(" Примечание: Генерация оружия типа " + type.name() + "!");

			Element element = document.addElement("list");
			Element config = element.addElement("config");
			config.addAttribute("showall", "true");

			int count = 0;
			for(final ItemTemplate template : ItemHolder.getInstance().getAllTemplates())
			{
				if(template == null || !template.isWeapon())
					continue;
				else if(template.getItemType() != type)
					continue;

				final Grade grade = template.getItemGrade();
				final String name = template.getName();
				if(grade == Grade.B || grade == Grade.NONE || grade == Grade.D || grade == Grade.C || grade == Grade.S84 || grade == Grade.S80)
					continue;

				final String additional = template.getAdditionalName();

				final String low = name.toLowerCase();
				if((masterwork ? low.contains(" - ") : !low.contains(" - ")) && !low.contains("pvp") && !low.contains("npc") && !low.contains("event") && !low.contains("friendship") && !low.contains("pc cafe") && !low.contains("limited") && ((additional != null && !additional.equals("")) || type == WeaponType.DUAL || type == WeaponType.DUALDAGGER))
				{
					count++;
					Element item = element.addElement("item");

					Element ingredient = item.addElement("ingredient");
					ingredient.addAttribute("id", "57");
					ingredient.addAttribute("count", String.valueOf(obt ? 1 : grade == Grade.A ? 77000000 : low.contains("dynasty") ? 165000000 : grade == Grade.S ? 105000000 : 0));
					item.addComment(" Adena ");

					Element production = item.addElement("production");
					production.addAttribute("id", String.valueOf(template.getItemId()));
					production.addAttribute("count", "1");
					item.addComment(" " + name + (additional != null ? (" - " + additional + " ") : " "));
				}
			}

			try
			{
				final int id = multisell.incrementAndGet();
				String path = "A:\\files\\multisell\\" + (obt ? "obt" : "live") + "\\" + id + ".xml";
				System.out.println((masterwork ? "Masterwork " : "") + "Weapon " + (obt ? "OBT " : "") + "multisell " + type.name() + " id: " + id + " (" + count + " weapons)");
				OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
				prettyPrint.setIndent("\t");
				XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
				xmlWriter.write(document);
				xmlWriter.close();
			}
			catch(Exception e1)
			{
				e1.printStackTrace();
			}
		}
	}

	public static void armors(final boolean masterwork, final boolean obt)
	{
		for(final ArmorType type : ArmorType.values())
		{
			if(type == ArmorType.NONE || type == ArmorType.PET || type == ArmorType.SIGIL)
				continue;

			org.dom4j.Document document = DocumentHelper.createDocument();
			document.addDocType("list", null, "multisell.dtd");
			Date date = new Date();
			document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
			document.addComment(" Site: http://l2cccp.com/ ");
			document.addComment(" Примечание: Генерация брони типа " + type.name() + "!");

			Element element = document.addElement("list");
			Element config = element.addElement("config");
			config.addAttribute("showall", "true");

			int count = 0;
			for(final ArmorSet set : ArmorSetsHolder.getInstance().getArmorSets())
			{
				if(set == null)
					continue;

				List<Integer> list = new ArrayList<Integer>();
				int index = masterwork ? 1 : 0;
				if(set.getHeadItemIds().size() > index)
					list.add(set.getHeadItemIds().get(index));
				if(set.getChestItemIds().size() > 0)
				{
					if(!masterwork)
						list.add(set.getChestItemIds().get(0));
					if(ItemHolder.getInstance().getTemplate(set.getChestItemIds().get(0)).getItemType() != type)
						continue;
				}

				if(set.getLegsItemIds().size() > index)
					list.add(set.getLegsItemIds().get(index));

				if(set.getGlovesItemIds().size() > index)
					list.add(set.getGlovesItemIds().get(index));
				if(set.getFeetsItemIds().size() > index)
					list.add(set.getFeetsItemIds().get(index));

				for(final int armor : list)
				{
					final ItemTemplate template = ItemHolder.getInstance().getTemplate(armor);
					final Grade grade = template.getItemGrade();
					final String name = template.getName();
					if((masterwork ? template.getAttachedSkills() == null && template.getAttachedSkills().length <= 0 : template.getAttachedSkills() != null && template.getAttachedSkills().length > 0) || (grade != Grade.A && grade != Grade.S && grade != Grade.S80))
						continue;

					final String additional = template.getAdditionalName();

					final String low = name.toLowerCase();
					if(!low.contains(" - ") && !low.contains("use") && !low.contains("pvp") && !low.contains("npc") && !low.contains("event") && !low.contains("friendship") && !low.contains("pc cafe") && !low.contains("limited"))
					{
						count++;
						Element item = element.addElement("item");

						Element ingredient = item.addElement("ingredient");
						ingredient.addAttribute("id", "57");
						ingredient.addAttribute("count", String.valueOf(obt ? 1 : grade == Grade.A ? 77000000 : low.contains("dynasty") ? 165000000 : grade == Grade.S ? 105000000 : 0));
						item.addComment(" Adena ");

						Element production = item.addElement("production");
						production.addAttribute("id", String.valueOf(template.getItemId()));
						production.addAttribute("count", "1");
						item.addComment(" " + name + " " + (additional != null && !additional.isEmpty() ? "- " + additional + " " : ""));
					}
				}
			}

			if(count == 0)
				return;

			try
			{
				final int id = multisell.incrementAndGet();
				String path = "A:\\files\\multisell\\" + (obt ? "obt" : "live") + "\\" + id + ".xml";
				System.out.println((masterwork ? "Masterwork " : "") + "Armor " + (obt ? "OBT " : "") + "multisell " + type.name() + " id: " + id + " (" + count + " armors)");
				OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
				prettyPrint.setIndent("\t");
				XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
				xmlWriter.write(document);
				xmlWriter.close();
			}
			catch(Exception e1)
			{
				e1.printStackTrace();
			}
		}
	}

	public static void jewels(final boolean masterwork, final boolean obt)
	{
		for(final ArmorType type : ArmorType.values())
		{
			if(type == ArmorType.PET || type == ArmorType.SIGIL)
				continue;

			org.dom4j.Document document = DocumentHelper.createDocument();
			document.addDocType("list", null, "multisell.dtd");
			Date date = new Date();
			document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
			document.addComment(" Site: http://l2cccp.com/ ");
			document.addComment(" Примечание: Генерация бижутерии типа " + type.name() + "!");

			Element element = document.addElement("list");
			Element config = element.addElement("config");
			config.addAttribute("showall", "true");

			int count = 0;
			for(final ItemTemplate template : ItemHolder.getInstance().getAllTemplates())
			{
				if(template == null || template.getItemClass() != ItemClass.JEWELRY)
					continue;

				final Grade grade = template.getItemGrade();
				final String name = template.getName();
				if((masterwork ? template.getAttachedSkills() == null && template.getAttachedSkills().length <= 0 : template.getAttachedSkills() != null && template.getAttachedSkills().length > 0) || (grade != Grade.A && grade != Grade.S && grade != Grade.S80) || type != template.getItemType())
					continue;

				final String additional = template.getAdditionalName();

				final String low = name.toLowerCase();
				if((additional == null || additional.isEmpty()) && !low.contains(" - ") && !low.contains("use") && !low.contains("pvp") && !low.contains("npc") && !low.contains("event") && !low.contains("friendship") && !low.contains("pc cafe") && !low.contains("limited"))
				{
					count++;
					Element item = element.addElement("item");

					Element ingredient = item.addElement("ingredient");
					ingredient.addAttribute("id", "57");
					ingredient.addAttribute("count", String.valueOf(obt ? 1 : grade == Grade.A ? 77000000 : low.contains("dynasty") ? 165000000 : grade == Grade.S ? 105000000 : 0));
					item.addComment(" Adena ");

					Element production = item.addElement("production");
					production.addAttribute("id", String.valueOf(template.getItemId()));
					production.addAttribute("count", "1");
					item.addComment(" " + name + " ");
				}
			}

			if(count == 0)
				return;

			try
			{
				final int id = multisell.incrementAndGet();
				String path = "A:\\files\\multisell\\" + (obt ? "obt" : "live") + "\\" + id + ".xml";
				System.out.println((masterwork ? "Masterwork " : "") + "Jewels " + (obt ? "OBT " : "") + "multisell " + type.name() + " id: " + id + " (" + count + " armors)");
				OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
				prettyPrint.setIndent("\t");
				XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
				xmlWriter.write(document);
				xmlWriter.close();
			}
			catch(Exception e1)
			{
				e1.printStackTrace();
			}
		}
	}

	public static void hennas(final boolean obt)
	{
		org.dom4j.Document document = DocumentHelper.createDocument();
		document.addDocType("list", null, "multisell.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");
		document.addComment(" Примечание: Генерация красок!");

		Element element = document.addElement("list");
		Element config = element.addElement("config");
		config.addAttribute("showall", "true");

		int count = 0;
		for(final Henna henna : HennaHolder.getInstance().get().valueCollection())
		{
			final int id = henna.getSymbolId();
			if(id > 144 || (id >= 25 && id <= 36))
				continue;

			final ItemTemplate template = ItemHolder.getInstance().getTemplate(henna.getDyeId());
			final String name = template.getName();

			count++;
			Element item = element.addElement("item");

			Element ingredient = item.addElement("ingredient");
			ingredient.addAttribute("id", "57");
			ingredient.addAttribute("count", String.valueOf(obt ? 1 : 75));
			item.addComment(" Adena ");

			Element production = item.addElement("production");
			production.addAttribute("id", String.valueOf(template.getItemId()));
			production.addAttribute("count", "1");
			item.addComment(" " + name + " ");
		}

		if(count == 0)
			return;

		try
		{
			final int id = multisell.incrementAndGet();
			String path = "A:\\files\\multisell\\" + (obt ? "obt" : "live") + "\\" + id + ".xml";
			System.out.println("Hennas" + (obt ? "OBT " : "") + "multisell id: " + id + " (" + count + " armors)");
			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
			prettyPrint.setIndent("\t");
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
			xmlWriter.write(document);
			xmlWriter.close();
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}

	public static void shots(final boolean obt)
	{

		org.dom4j.Document document = DocumentHelper.createDocument();
		document.addDocType("list", null, "multisell.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");
		document.addComment(" Примечание: Генерация плащей!");

		Element element = document.addElement("list");
		Element config = element.addElement("config");
		config.addAttribute("showall", "true");

		int count = 0;
		for(final int id : new int[] {
				5789,
				1835,
				1463,
				1464,
				1465,
				1466,
				1467,
				13037,
				13045,
				13055,
				22082,
				22083,
				22084,
				22085,
				22086,
				5790,
				2509,
				2510,
				2511,
				2512,
				2513,
				2514,
				22077,
				22078,
				22079,
				22080,
				22081,
				3947,
				3948,
				3949,
				3950,
				3951,
				3952,
				22072,
				22073,
				22074,
				22075,
				22076,
				6535,
				6536,
				6537,
				6538,
				6539,
				6540,
				6645,
				20332,
				6646,
				20333,
				6647,
				20334 })
		{
			final ItemTemplate template = ItemHolder.getInstance().getTemplate(id);

			final String name = template.getName();

			count++;
			Element item = element.addElement("item");

			Element ingredient = item.addElement("ingredient");
			ingredient.addAttribute("id", "57");
			ingredient.addAttribute("count", String.valueOf(obt ? 1 : 75));
			item.addComment(" Adena ");

			Element production = item.addElement("production");
			production.addAttribute("id", String.valueOf(template.getItemId()));
			production.addAttribute("count", "1");
			item.addComment(" " + name + " ");
		}

		if(count == 0)
			return;

		try
		{
			final int id = multisell.incrementAndGet();
			String path = "A:\\files\\multisell\\" + (obt ? "obt" : "live") + "\\" + id + ".xml";
			System.out.println("Shots" + (obt ? "OBT " : "") + "multisell id: " + id + " (" + count + " armors)");
			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
			prettyPrint.setIndent("\t");
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
			xmlWriter.write(document);
			xmlWriter.close();
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}

	public static void cloaks(final boolean masterwork, final boolean obt)
	{
		org.dom4j.Document document = DocumentHelper.createDocument();
		document.addDocType("list", null, "multisell.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");
		document.addComment(" Примечание: Генерация плащей!");

		Element element = document.addElement("list");
		Element config = element.addElement("config");
		config.addAttribute("showall", "true");

		int count = 0;
		for(final ItemTemplate template : ItemHolder.getInstance().getAllTemplates())
		{
			if(template == null || !template.isCloak())
				continue;

			final Grade grade = template.getItemGrade();
			final String name = template.getName();
			if((masterwork ? template.getAttachedSkills() == null && template.getAttachedSkills().length <= 0 : template.getAttachedSkills() != null && template.getAttachedSkills().length > 0) || (grade != Grade.A && grade != Grade.S && grade != Grade.S80))
				continue;

			final String additional = template.getAdditionalName();

			final String low = name.toLowerCase();
			if((additional == null || additional.isEmpty()) && !low.contains(" - ") && !low.contains("use") && !low.contains("pvp") && !low.contains("npc") && !low.contains("event") && !low.contains("friendship") && !low.contains("pc cafe") && !low.contains("limited"))
			{
				count++;
				Element item = element.addElement("item");

				Element ingredient = item.addElement("ingredient");
				ingredient.addAttribute("id", "57");
				ingredient.addAttribute("count", String.valueOf(obt ? 1 : grade == Grade.A ? 77000000 : low.contains("dynasty") ? 165000000 : grade == Grade.S ? 105000000 : 0));
				item.addComment(" Adena ");

				Element production = item.addElement("production");
				production.addAttribute("id", String.valueOf(template.getItemId()));
				production.addAttribute("count", "1");
				item.addComment(" " + name + " ");
			}
		}

		if(count == 0)
			return;

		try
		{
			final int id = multisell.incrementAndGet();
			String path = "A:\\files\\multisell\\" + (obt ? "obt" : "live") + "\\" + id + ".xml";
			System.out.println((masterwork ? "Masterwork " : "") + "Cloak " + (obt ? "OBT " : "") + "multisell id: " + id + " (" + count + " armors)");
			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
			prettyPrint.setIndent("\t");
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
			xmlWriter.write(document);
			xmlWriter.close();
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}

	public static void decoration(final boolean masterwork, final boolean obt)
	{
		org.dom4j.Document document = DocumentHelper.createDocument();
		document.addDocType("list", null, "multisell.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");
		document.addComment(" Примечание: Генерация агатионов!");

		Element element = document.addElement("list");
		Element config = element.addElement("config");
		config.addAttribute("showall", "true");

		int count = 0;
		for(final ItemTemplate template : ItemHolder.getInstance().getAllTemplates())
		{
			if(template == null || !template.isDecoration())
				continue;

			final String name = template.getName();
			if((masterwork ? template.getAttachedSkills() == null && template.getAttachedSkills().length <= 0 : template.getAttachedSkills() != null && template.getAttachedSkills().length > 0))
				continue;
			else if(template.isTemporal())
				continue;

			final String additional = template.getAdditionalName();
			if(additional.toLowerCase().contains("event") || additional.toLowerCase().contains("limited"))
				continue;

			final String low = name.toLowerCase();
			if(!low.contains("pvp") && !low.contains("event") && !low.contains("shadow") && !low.contains("friendship") && !low.contains("pc cafe") && !low.contains("limited"))
			{
				count++;
				Element item = element.addElement("item");

				Element ingredient = item.addElement("ingredient");
				ingredient.addAttribute("id", "57");
				ingredient.addAttribute("count", String.valueOf(obt ? 1 : 105000000));
				item.addComment(" Adena ");

				Element production = item.addElement("production");
				production.addAttribute("id", String.valueOf(template.getItemId()));
				production.addAttribute("count", "1");
				item.addComment(" " + name + " " + (additional != null && !additional.isEmpty() ? "- " + additional + " " : ""));
			}
		}

		if(count == 0)
			return;

		try
		{
			final int id = multisell.incrementAndGet();
			String path = "A:\\files\\multisell\\" + (obt ? "obt" : "live") + "\\" + id + ".xml";
			System.out.println((masterwork ? "Masterwork " : "") + "Armor " + (obt ? "OBT " : "") + "multisell agathions id: " + id + " (" + count + " armors)");
			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
			prettyPrint.setIndent("\t");
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
			xmlWriter.write(document);
			xmlWriter.close();
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}

	public static void agathions(final boolean masterwork, final boolean obt)
	{
		org.dom4j.Document document = DocumentHelper.createDocument();
		document.addDocType("list", null, "multisell.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");
		document.addComment(" Примечание: Генерация агатионов!");

		Element element = document.addElement("list");
		Element config = element.addElement("config");
		config.addAttribute("showall", "true");

		int count = 0;
		for(final ItemTemplate template : ItemHolder.getInstance().getAllTemplates())
		{
			if(template == null || !template.isBracelet())
				continue;

			final String name = template.getName();
			if(template.getAttachedSkills() == null && template.getAttachedSkills().length <= 0)
				continue;
			else if(template.isTemporal())
				continue;

			final String additional = template.getAdditionalName();
			if(additional.toLowerCase().contains("event") || additional.toLowerCase().contains("limited"))
				continue;

			final String low = name.toLowerCase();
			if(!low.contains("pvp") && !low.contains("event") && !low.contains("shadow") && !low.contains("friendship") && !low.contains("pc cafe") && !low.contains("limited"))
			{
				count++;
				Element item = element.addElement("item");

				Element ingredient = item.addElement("ingredient");
				ingredient.addAttribute("id", "57");
				ingredient.addAttribute("count", String.valueOf(obt ? 1 : 105000000));
				item.addComment(" Adena ");

				Element production = item.addElement("production");
				production.addAttribute("id", String.valueOf(template.getItemId()));
				production.addAttribute("count", "1");
				item.addComment(" " + name + " " + (additional != null && !additional.isEmpty() ? "- " + additional + " " : ""));
			}
		}

		if(count == 0)
			return;

		try
		{
			final int id = multisell.incrementAndGet();
			String path = "A:\\files\\multisell\\" + (obt ? "obt" : "live") + "\\" + id + ".xml";
			System.out.println((masterwork ? "Masterwork " : "") + "Armor " + (obt ? "OBT " : "") + "multisell agathions id: " + id + " (" + count + " armors)");
			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
			prettyPrint.setIndent("\t");
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
			xmlWriter.write(document);
			xmlWriter.close();
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}

	public static void books(final boolean masterwork, final boolean obt)
	{
		org.dom4j.Document document = DocumentHelper.createDocument();
		document.addDocType("list", null, "multisell.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");
		document.addComment(" Примечание: Генерация агатионов!");

		Element element = document.addElement("list");
		Element config = element.addElement("config");
		config.addAttribute("showall", "true");

		int count = 0;
		List<Integer> list = new ArrayList<Integer>();
		for(final SkillLearn skill : SkillAcquireHolder.getInstance().getAllNormalSkillTreeWithForgottenScrolls())
		{
			final Integer id = skill.getItemId();
			if(skill == null || id == 0 || !skill.isClicked())
				continue;

			if(!list.contains(id))
				list.add(id);
		}

		for(final Integer skill : list)
		{
			final ItemTemplate template = ItemHolder.getInstance().getTemplate(skill);
			count++;
			Element item = element.addElement("item");

			Element ingredient = item.addElement("ingredient");
			ingredient.addAttribute("id", "57");
			ingredient.addAttribute("count", String.valueOf(obt ? 1 : 105000000));
			item.addComment(" Adena ");

			Element production = item.addElement("production");
			production.addAttribute("id", String.valueOf(template.getItemId()));
			production.addAttribute("count", "1");
			item.addComment(" " + template.getName() + " ");
		}

		if(count == 0)
			return;

		try

		{
			final int id = multisell.incrementAndGet();
			String path = "A:\\files\\multisell\\" + (obt ? "obt" : "live") + "\\" + id + ".xml";
			System.out.println((masterwork ? "Masterwork " : "") + "Armor " + (obt ? "OBT " : "") + "multisell books id: " + id + " (" + count + " armors)");
			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
			prettyPrint.setIndent("\t");
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
			xmlWriter.write(document);
			xmlWriter.close();
		}
		catch(

		Exception e1)

		{
			e1.printStackTrace();
		}
	}
}