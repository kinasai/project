package com.l2cccp.gameserver.handler.voicecommands.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collection;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.dao.ItemsDAO;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.actor.instances.player.AccountPlayerInfo;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.items.ItemInstance.ItemLocation;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;

import org.napile.pair.primitive.IntObjectPair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Repair implements IVoicedCommandHandler
{
	private static final Logger _log = LoggerFactory.getLogger(Repair.class);
	private static final String GET_CHARACTER_VAR = "SELECT * FROM character_variables WHERE `obj_id` = ? AND `name` = 'jailed';";

	private final String[] _commandList = new String[] { "repair" };

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, Player activeChar, String target)
	{
		if(!target.isEmpty())
		{
			if(activeChar.getName().equalsIgnoreCase(target))
			{
				activeChar.sendMessage(new CustomMessage("voicedcommandhandlers.Repair.YouCantRepairYourself"));
				return false;
			}

			int objId = 0;

			for(IntObjectPair<AccountPlayerInfo> e : activeChar.getAccountChars().entrySet())
			{
				if(e.getValue().getName().equalsIgnoreCase(target))
				{
					objId = e.getKey();
					break;
				}
			}

			if(objId == 0)
			{
				activeChar.sendMessage(new CustomMessage("voicedcommandhandlers.Repair.YouCanRepairOnlyOnSameAccount"));
				return false;
			}
			else if(World.getPlayer(objId) != null)
			{
				activeChar.sendMessage(new CustomMessage("voicedcommandhandlers.Repair.CharIsOnline"));
				return false;
			}

			if(isJail(objId))
			{
				activeChar.sendMessage("Player is jailed!");
				return false;
			}

			Connection con = null;
			PreparedStatement statement = null;
			ResultSet rs = null;
			try
			{
				con = DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("SELECT karma FROM characters WHERE obj_Id=?");
				statement.setInt(1, objId);
				statement.execute();
				rs = statement.getResultSet();

				int karma = 0;

				rs.next();

				karma = rs.getInt("karma");

				DbUtils.close(statement, rs);

				if(karma > 0)
				{
					statement = con.prepareStatement("UPDATE characters SET x=17144, y=170156, z=-3502 WHERE obj_Id=?");
					statement.setInt(1, objId);
					statement.execute();
					DbUtils.close(statement);
				}
				else
				{
					statement = con.prepareStatement("UPDATE characters SET x=0, y=0, z=0 WHERE obj_Id=?");
					statement.setInt(1, objId);
					statement.execute();
					DbUtils.close(statement);

					Collection<ItemInstance> items = ItemsDAO.getInstance().getItemsByOwnerIdAndLoc(objId, ItemLocation.PAPERDOLL);
					for(ItemInstance item : items)
					{
						item.setEquipped(false);
						item.setLocData(0);
						item.setLocation(item.getTemplate().isStoreable() ? ItemLocation.WAREHOUSE : ItemLocation.INVENTORY);
						item.setJdbcState(JdbcEntityState.UPDATED);
						item.update();
					}
				}

				statement = con.prepareStatement("DELETE FROM character_variables WHERE obj_id=? AND type='user-var' AND name='reflection'");
				statement.setInt(1, objId);
				statement.execute();
				DbUtils.close(statement);

				activeChar.sendMessage(new CustomMessage("voicedcommandhandlers.Repair.RepairDone"));
			}
			catch(Exception e)
			{
				_log.error("", e);
				return false;
			}
			finally
			{
				DbUtils.closeQuietly(con, statement, rs);
			}
			return true;
		}
		else
			activeChar.sendMessage(".repair <name>");

		return false;
	}

	private boolean isJail(int objId)
	{
		boolean isJail = false;

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(GET_CHARACTER_VAR);
			statement.setInt(1, objId);
			rset = statement.executeQuery();
			if(rset.next())
				isJail = true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return isJail;
	}
}