package com.l2cccp.gameserver.handler.admincommands.impl;


import org.apache.commons.lang3.math.NumberUtils;

import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Spawner;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;

public class AdminDelete implements IAdminCommandHandler
{
	private static enum Commands
	{
		admin_delete
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player activeChar)
	{
		Commands command = (Commands) comm;

		if(!activeChar.getPlayerAccess().CanEditNPC)
			return false;

		switch(command)
		{
			case admin_delete:
				GameObject obj = wordList.length == 1 ? activeChar.getTarget() : GameObjectsStorage.getNpc(NumberUtils.toInt(wordList[1]));
				if(obj != null && obj.isNpc())
				{
					NpcInstance target = (NpcInstance) obj;
					target.deleteMe();

					Spawner spawn = target.getSpawn();
					if(spawn != null)
						spawn.stopRespawn();
				}
				else
					activeChar.sendPacket(SystemMsg.INVALID_TARGET);
				break;
		}

		return true;
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}
}