package com.l2cccp.gameserver.handler.admincommands;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminAdmin;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminAnnouncements;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminBan;
//import com.l2cccp.gameserver.handler.admincommands.impl.AdminBot;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminCamera;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminCancel;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminClanHall;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminCreateItem;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminCursedWeapons;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminDelete;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminDisconnect;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminDoorControl;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminEditChar;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminEffects;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminEnchant;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminEvents;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminGeodata;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminGm;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminGmChat;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminHeal;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminHellbound;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminHelpPage;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminIP;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminInstance;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminKill;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminLevel;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminLeviathan;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminMammon;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminManor;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminMenu;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminMonsterRace;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminOlympiad;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminPetition;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminPledge;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminPolymorph;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminProtect;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminQuests;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminReload;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminRepairChar;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminReparse;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminRes;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminRide;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminSS;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminScripts;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminServer;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminShop;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminShutdown;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminSkill;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminSpawn;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminTarget;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminTeleport;
import com.l2cccp.gameserver.handler.admincommands.impl.AdminZone;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.utils.Log;

public class AdminCommandHandler extends AbstractHolder
{
	private static final AdminCommandHandler _instance = new AdminCommandHandler();

	public static AdminCommandHandler getInstance()
	{
		return _instance;
	}

	private Map<String, IAdminCommandHandler> _datatable = new HashMap<String, IAdminCommandHandler>();

	private AdminCommandHandler()
	{
		registerAdminCommandHandler(new AdminAdmin());
		registerAdminCommandHandler(new AdminAnnouncements());
		registerAdminCommandHandler(new AdminBan());

		registerAdminCommandHandler(new AdminCamera());
		registerAdminCommandHandler(new AdminCancel());
		registerAdminCommandHandler(new AdminClanHall());
		registerAdminCommandHandler(new AdminCreateItem());
		registerAdminCommandHandler(new AdminCursedWeapons());
		registerAdminCommandHandler(new AdminDelete());
		registerAdminCommandHandler(new AdminDisconnect());
		registerAdminCommandHandler(new AdminDoorControl());
		registerAdminCommandHandler(new AdminEditChar());
		registerAdminCommandHandler(new AdminEffects());
		registerAdminCommandHandler(new AdminEnchant());
		registerAdminCommandHandler(new AdminEvents());
		registerAdminCommandHandler(new AdminGeodata());
		registerAdminCommandHandler(new AdminGm());
		registerAdminCommandHandler(new AdminGmChat());
		registerAdminCommandHandler(new AdminHeal());
		registerAdminCommandHandler(new AdminHellbound());
		registerAdminCommandHandler(new AdminHelpPage());
		registerAdminCommandHandler(new AdminInstance());
		registerAdminCommandHandler(new AdminIP());
		registerAdminCommandHandler(new AdminLevel());
		registerAdminCommandHandler(new AdminMammon());
		registerAdminCommandHandler(new AdminManor());
		registerAdminCommandHandler(new AdminMenu());
		registerAdminCommandHandler(new AdminMonsterRace());
		registerAdminCommandHandler(new AdminOlympiad());
		registerAdminCommandHandler(new AdminPetition());
		registerAdminCommandHandler(new AdminProtect());
		registerAdminCommandHandler(new AdminPledge());
		registerAdminCommandHandler(new AdminPolymorph());
		registerAdminCommandHandler(new AdminQuests());
		registerAdminCommandHandler(new AdminReload());
		registerAdminCommandHandler(new AdminRepairChar());
		registerAdminCommandHandler(new AdminReparse());
		registerAdminCommandHandler(new AdminRes());
		registerAdminCommandHandler(new AdminRide());
		registerAdminCommandHandler(new AdminServer());
		registerAdminCommandHandler(new AdminShop());
		registerAdminCommandHandler(new AdminShutdown());
		registerAdminCommandHandler(new AdminSkill());
		registerAdminCommandHandler(new AdminScripts());
		registerAdminCommandHandler(new AdminSpawn());
		registerAdminCommandHandler(new AdminSS());
		registerAdminCommandHandler(new AdminTarget());
		registerAdminCommandHandler(new AdminTeleport());
		registerAdminCommandHandler(new AdminLeviathan());
		registerAdminCommandHandler(new AdminZone());
		registerAdminCommandHandler(new AdminKill());
	}

	public void registerAdminCommandHandler(IAdminCommandHandler handler)
	{
		for(Enum<?> e : handler.getAdminCommandEnum())
			_datatable.put(e.toString().toLowerCase(), handler);
	}

	public IAdminCommandHandler getAdminCommandHandler(String adminCommand)
	{
		String command = adminCommand;
		if(adminCommand.indexOf(" ") != -1)
			command = adminCommand.substring(0, adminCommand.indexOf(" "));
		return _datatable.get(command);
	}

	public void useAdminCommandHandler(Player activeChar, String adminCommand)
	{
		if(!(activeChar.isGM() || activeChar.getPlayerAccess().CanUseGMCommand))
		{
			activeChar.sendMessage(new CustomMessage("l2p.gameserver.clientpackets.SendBypassBuildCmd.NoCommandOrAccess").addString(adminCommand));
			return;
		}

		String[] wordList = adminCommand.split(" ");
		IAdminCommandHandler handler = _datatable.get(wordList[0]);
		if(handler != null)
		{
			boolean success = false;
			try
			{
				for(Enum<?> e : handler.getAdminCommandEnum())
					if(e.toString().equalsIgnoreCase(wordList[0]))
					{
						success = handler.useAdminCommand(e, wordList, adminCommand, activeChar);
						break;
					}
			}
			catch(Exception e)
			{
				error("", e);
			}

			Log.LogCommand(activeChar, activeChar.getTarget(), adminCommand, success);
		}
	}

	@Override
	public void process()
	{

	}

	@Override
	public int size()
	{
		return _datatable.size();
	}

	@Override
	public void clear()
	{
		_datatable.clear();
	}

	/**
	 * Получение списка зарегистрированных админ команд
	 * @return список команд
	 */
	public Set<String> getAllCommands()
	{
		return _datatable.keySet();
	}
}