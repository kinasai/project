package com.l2cccp.gameserver.handler.admincommands.impl;

import com.l2cccp.gameserver.data.xml.holder.MultiSellHolder;
import com.l2cccp.gameserver.data.xml.parser.MultiSellParser;
import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.model.Player;

public class AdminReparse implements IAdminCommandHandler
{
	private static enum Commands
	{
		admin_reparse_multisell
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player activeChar)
	{
		Commands command = (Commands) comm;

		if(!activeChar.getPlayerAccess().IsGM)
			return false;

		switch(command)
		{
			case admin_reparse_multisell:
				MultiSellParser.getInstance().setReparse(true);
				MultiSellHolder.getInstance().clear();
				MultiSellParser.getInstance().reload();
				MultiSellParser.getInstance().setReparse(false);
				break;
		}

		return true;
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}

}