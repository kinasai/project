package com.l2cccp.gameserver.handler.admincommands.impl;

import com.l2cccp.gameserver.data.xml.holder.LeviathanEventHolder;
import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;

public class AdminLeviathan implements IAdminCommandHandler
{
	private static enum Commands
	{
		admin_leviathan,
		admin_leviathan_get,
		admin_leviathan_team
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player activeChar)
	{
		Commands command = (Commands) comm;

		if(!activeChar.getPlayerAccess().IsEventGm)
			return false;

		switch(command)
		{
			case admin_leviathan:
			{
				final HtmlMessage html = new HtmlMessage(5);
				if(wordList.length == 1)
					html.setFile("admin/leviathan/index.htm");
				else
					html.setFile("admin/leviathan/" + wordList[1].trim());

				activeChar.sendPacket(html);
				break;
			}
			case admin_leviathan_get:
			{
				final HtmlMessage html = new HtmlMessage(5);
				html.setFile("admin/leviathan/index.htm");
				final int id = Integer.parseInt(wordList[1]);
				final AbstractLeviathan event = LeviathanEventHolder.getInstance().getEvent(id);
				event.getTemplate().getConditions();

				activeChar.sendPacket(html);
				break;
			}
			case admin_leviathan_team:
			{
				break;
			}
		}

		return true;
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}
}