package com.l2cccp.gameserver.handler.voicecommands.impl;

import java.util.List;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.BotReportHolder;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.botreport.ReportStorage;
import com.l2cccp.gameserver.model.botreport.ReportTemplate;
import com.l2cccp.gameserver.model.botreport.ReportVariant;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Log;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BotReport implements IVoicedCommandHandler
{
	private final String[] _commandList = new String[] { "bot" };

	@Override
	public boolean useVoicedCommand(String command, Player player, String arg)
	{
		if(!Config.COMMAND_BOT_REPORT_ENABLED)
		{
			player.sendMessage(new CustomMessage("common.Disabled"));
			return false;
		}

		if(command.equals("bot"))
		{
			GameObject obj = player.getTarget();
			if(obj == null || !obj.isPlayer())
			{
				player.sendMessage(new CustomMessage("report.bot.wrong.target"));
				return false;
			}

			final int min = Config.COMMAND_BOT_REPORT_LEVELS[0];
			final int max = Config.COMMAND_BOT_REPORT_LEVELS[1];
			if(player.getLevel() < min || player.getLevel() > max)
			{
				player.sendMessage(new CustomMessage("common.level.between").addNumber(min).addNumber(max));
				return false;
			}

			Player target = obj.getPlayer();
			if(!check(player.getName(), target))
			{
				player.sendMessage(new CustomMessage("report.bot.wrong.target"));
				return false;
			}
			else if(!ReportStorage.getInstance().report(target.getObjectId()))
			{
				player.sendMessage("Target is checked, try again later!");
				return false;
			}
			else
			{
				ReportStorage.getInstance().put(target.getObjectId());
				final ReportTemplate report = BotReportHolder.getInstance().getRandom();

				target.block();
				target.sendMessage(new CustomMessage("report.bot.start.target"));
				target.makeReport(report);
				show(target);
				player.sendMessage(new CustomMessage("report.bot.start.requestor").addString(target.getName()));
				Log.report("Player " + player.getName() + " make report to player " + target.getName() + "!", "BotReport");
				return true;
			}
		}
		else
			return false;
	}

	private void show(Player player)
	{
		final ReportTemplate report = player.getReport();
		String template = HtmCache.getInstance().getHtml(report.getType().getHtml(), player);
		String list = null;
		List<ReportVariant> variants = report.getVariants();
		final int inline = 6;
		final int size = variants.size() - 1;

		boolean loop = true;
		while(loop)
		{
			String block = "";
			list = "";
			for(int i = 1; i <= (inline * 6); i++)
			{
				final int rnd = Rnd.get(size);
				ReportVariant variant = variants.get(rnd);
				block = template;
				block = block.replace("{bypass}", "bypass -h htmbypass_report " + variant.getId());
				block = block.replace("{value}", variant.getValue());

				if(i % inline == 0)
					block += "</tr><tr>";

				list += block;

				if(variant.isCorrect())
					loop = false;
			}
		}

		HtmlMessage html = new HtmlMessage(0).setFile("command/bot_report/index.htm");
		html.replace("%msg%", report.getMsg(player.getLanguage()));
		html.replace("%variants%", list);
		player.sendPacket(html);
	}

	private boolean check(final String name, Player target)
	{
		if(!Config.COMMAND_BOT_REPORT_PEACE && target.isInPeaceZone())
		{
			Log.report("Player " + name + " try make report to player " + target.getName() + " but he is in peace zone!", "BotReport");
			return false;
		}
		else if(!Config.COMMAND_BOT_REPORT_PVP && target.getPvpFlag() > 0)
		{
			Log.report("Player " + name + " try make report to player " + target.getName() + " but he is in pvp battle" + (target.getTarget() != null ? " [" + target.getTarget().getName() + "]" : "") + "!", "BotReport");
			return false;
		}
		else if(!target.isAttackingNow() || !target.isInCombat())
		{
			Log.report("Player " + name + " try make report to player " + target.getName() + " but he isn't attack anyone!", "BotReport");
			return false;
		}
		else if(target.isAfk())
		{
			Log.report("Player " + name + " try make report to player " + target.getName() + " but he is AFK!", "BotReport");
			return false;
		}
		else
			return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}