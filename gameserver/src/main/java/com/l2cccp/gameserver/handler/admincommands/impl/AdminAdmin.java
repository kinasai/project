package com.l2cccp.gameserver.handler.admincommands.impl;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang3.math.NumberUtils;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.PremiumDAO;
import com.l2cccp.gameserver.data.xml.holder.PremiumHolder;
import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.instancemanager.SoDManager;
import com.l2cccp.gameserver.instancemanager.SoIManager;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.SpecialEffectState;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.premium.PremiumAccount;
import com.l2cccp.gameserver.model.premium.PremiumStart;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.EventTrigger;
import com.l2cccp.gameserver.network.l2.s2c.ExChangeClientEffectInfo;
import com.l2cccp.gameserver.network.l2.s2c.ExSendUIEvent;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.network.l2.s2c.PlaySound;
import com.l2cccp.gameserver.stats.Stats;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.Log;
import com.l2cccp.gameserver.utils.TimeUtils;

public class AdminAdmin implements IAdminCommandHandler
{
	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");

	private static enum Commands
	{
		admin_admin,
		admin_play_sounds,
		admin_play_sound,
		admin_silence,
		admin_tradeoff,
		admin_show_html,
		admin_setnpcstate,
		admin_setareanpcstate,
		admin_showmovie,
		admin_setzoneinfo,
		admin_eventtrigger,
		admin_debug,
		admin_uievent,
		admin_opensod,
		admin_closesod,
		admin_setsoistage,
		admin_soinotify,
		admin_forcenpcinfo,
		admin_loc,
		admin_locdump,
		admin_undying,
		admin_who,
		admin_moder,
		admin_supermoder,
		admin_come,
		admin_setpa
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player activeChar)
	{
		Commands command = (Commands) comm;
		StringTokenizer st;

		if(activeChar.getPlayerAccess().Menu)
		{
			switch(command)
			{
				case admin_admin:
				{
					activeChar.sendPacket(new HtmlMessage(5).setFile("admin/admin.htm"));
					break;
				}
				case admin_play_sounds:
					if(wordList.length == 1)
						activeChar.sendPacket(new HtmlMessage(5).setFile("admin/songs/songs.htm"));
					else
						try
						{
							activeChar.sendPacket(new HtmlMessage(5).setFile("admin/songs/songs" + wordList[1] + ".htm"));
						}
						catch(StringIndexOutOfBoundsException e)
						{}
					break;
				case admin_play_sound:
					try
					{
						playAdminSound(activeChar, wordList[1]);
					}
					catch(StringIndexOutOfBoundsException e)
					{}
					break;
				case admin_silence:
					if(activeChar.getMessageRefusal()) // already in message refusal
					// mode
					{
						activeChar.unsetVar("gm_silence");
						activeChar.setMessageRefusal(false);
						activeChar.sendPacket(SystemMsg.MESSAGE_ACCEPTANCE_MODE);
						activeChar.sendEtcStatusUpdate();
					}
					else
					{
						if(Config.SAVE_GM_EFFECTS)
							activeChar.setVar("gm_silence", "true", -1);
						activeChar.setMessageRefusal(true);
						activeChar.sendPacket(SystemMsg.MESSAGE_REFUSAL_MODE);
						activeChar.sendEtcStatusUpdate();
					}
					break;
				case admin_tradeoff:
					try
					{
						if(wordList[1].equalsIgnoreCase("on"))
						{
							activeChar.setTradeRefusal(true);
							activeChar.sendMessage("tradeoff enabled");
						}
						else if(wordList[1].equalsIgnoreCase("off"))
						{
							activeChar.setTradeRefusal(false);
							activeChar.sendMessage("tradeoff disabled");
						}
					}
					catch(Exception ex)
					{
						if(activeChar.getTradeRefusal())
							activeChar.sendMessage("tradeoff currently enabled");
						else
							activeChar.sendMessage("tradeoff currently disabled");
					}
					break;
				case admin_show_html:
					String html = wordList[1];
					try
					{
						if(html != null)
							activeChar.sendPacket(new HtmlMessage(5).setFile("admin/" + html));
						else
							activeChar.sendMessage("Html page not found");
					}
					catch(Exception npe)
					{
						activeChar.sendMessage("Html page not found");
					}
					break;
				case admin_setnpcstate:
					if(wordList.length < 2)
					{
						activeChar.sendMessage("USAGE: //setnpcstate state");
						return false;
					}
					int state;
					GameObject target = activeChar.getTarget();
					try
					{
						state = Integer.parseInt(wordList[1]);
					}
					catch(NumberFormatException e)
					{
						activeChar.sendMessage("You must specify state");
						return false;
					}
					if(!target.isNpc())
					{
						activeChar.sendMessage("You must target an NPC");
						return false;
					}
					NpcInstance npc = (NpcInstance) target;
					npc.setNpcState(state);
					break;
				case admin_setareanpcstate:
					try
					{
						final String val = fullString.substring(15).trim();

						String[] vals = val.split(" ");
						int range = NumberUtils.toInt(vals[0], 0);
						int astate = vals.length > 1 ? NumberUtils.toInt(vals[1], 0) : 0;

						for(NpcInstance n : activeChar.getAroundNpc(range, 200))
							n.setNpcState(astate);
					}
					catch(Exception e)
					{
						activeChar.sendMessage("Usage: //setareanpcstate [range] [state]");
					}
					break;
				case admin_showmovie:
					if(wordList.length < 2)
					{
						activeChar.sendMessage("USAGE: //showmovie id");
						return false;
					}
					int id;
					try
					{
						id = Integer.parseInt(wordList[1]);
					}
					catch(NumberFormatException e)
					{
						activeChar.sendMessage("You must specify id");
						return false;
					}
					activeChar.showQuestMovie(id);
					break;
				case admin_setzoneinfo:
					if(wordList.length < 2)
					{
						activeChar.sendMessage("USAGE: //setzoneinfo id");
						return false;
					}
					int stateid;
					try
					{
						stateid = Integer.parseInt(wordList[1]);
					}
					catch(NumberFormatException e)
					{
						activeChar.sendMessage("You must specify id");
						return false;
					}
					activeChar.sendPacket(new ExChangeClientEffectInfo(stateid));
					break;
				case admin_eventtrigger:
					if(wordList.length < 2)
					{
						activeChar.sendMessage("USAGE: //eventtrigger id");
						return false;
					}
					int triggerid;
					try
					{
						triggerid = Integer.parseInt(wordList[1]);
					}
					catch(NumberFormatException e)
					{
						activeChar.sendMessage("You must specify id");
						return false;
					}
					activeChar.broadcastPacket(new EventTrigger(triggerid, true));
					break;
				case admin_debug:
					GameObject ob = activeChar.getTarget();
					if(!ob.isPlayer())
					{
						activeChar.sendMessage("Only player target is allowed");
						return false;
					}
					Player pl = ob.getPlayer();
					List<String> _s = new ArrayList<String>();
					_s.add("==========TARGET STATS:");
					_s.add("==Magic Resist: " + pl.calcStat(Stats.MAGIC_RESIST, null, null));
					_s.add("==Magic Power: " + pl.calcStat(Stats.MAGIC_POWER, 1, null, null));
					_s.add("==Skill Power: " + pl.calcStat(Stats.SKILL_POWER, 1, null, null));
					_s.add("==Cast Break Rate: " + pl.calcStat(Stats.CAST_INTERRUPT, 1, null, null));

					_s.add("==========Powers:");
					_s.add("==Bleed: " + pl.calcStat(Stats.BLEED_POWER, 1, null, null));
					_s.add("==Poison: " + pl.calcStat(Stats.POISON_POWER, 1, null, null));
					_s.add("==Stun: " + pl.calcStat(Stats.STUN_POWER, 1, null, null));
					_s.add("==Root: " + pl.calcStat(Stats.ROOT_POWER, 1, null, null));
					_s.add("==Mental: " + pl.calcStat(Stats.MENTAL_POWER, 1, null, null));
					_s.add("==Sleep: " + pl.calcStat(Stats.SLEEP_POWER, 1, null, null));
					_s.add("==Paralyze: " + pl.calcStat(Stats.PARALYZE_POWER, 1, null, null));
					_s.add("==Cancel: " + pl.calcStat(Stats.CANCEL_POWER, 1, null, null));
					_s.add("==Debuff: " + pl.calcStat(Stats.DEBUFF_POWER, 1, null, null));

					_s.add("==========PvP Stats:");
					_s.add("==Phys Attack Dmg: " + pl.calcStat(Stats.PVP_PHYS_DMG_BONUS, 1, null, null));
					_s.add("==Phys Skill Dmg: " + pl.calcStat(Stats.PVP_PHYS_SKILL_DMG_BONUS, 1, null, null));
					_s.add("==Magic Skill Dmg: " + pl.calcStat(Stats.PVP_MAGIC_SKILL_DMG_BONUS, 1, null, null));
					_s.add("==Phys Attack Def: " + pl.calcStat(Stats.PVP_PHYS_DEFENCE_BONUS, 1, null, null));
					_s.add("==Phys Skill Def: " + pl.calcStat(Stats.PVP_PHYS_SKILL_DEFENCE_BONUS, 1, null, null));
					_s.add("==Magic Skill Def: " + pl.calcStat(Stats.PVP_MAGIC_SKILL_DEFENCE_BONUS, 1, null, null));

					_s.add("==========Reflects:");
					_s.add("==Phys Dmg Chance: " + pl.calcStat(Stats.REFLECT_AND_BLOCK_DAMAGE_CHANCE, null, null));
					_s.add("==Phys Skill Dmg Chance: " + pl.calcStat(Stats.REFLECT_AND_BLOCK_PSKILL_DAMAGE_CHANCE, null, null));
					_s.add("==Magic Skill Dmg Chance: " + pl.calcStat(Stats.REFLECT_AND_BLOCK_MSKILL_DAMAGE_CHANCE, null, null));
					_s.add("==Counterattack: Phys Dmg Chance: " + pl.calcStat(Stats.REFLECT_DAMAGE_PERCENT, null, null));
					_s.add("==Counterattack: Phys Skill Dmg Chance: " + pl.calcStat(Stats.REFLECT_PSKILL_DAMAGE_PERCENT, null, null));
					_s.add("==Counterattack: Magic Skill Dmg Chance: " + pl.calcStat(Stats.REFLECT_MSKILL_DAMAGE_PERCENT, null, null));

					_s.add("==========MP Consume Rate:");
					_s.add("==Magic Skills: " + pl.calcStat(Stats.MP_MAGIC_SKILL_CONSUME, 1, null, null));
					_s.add("==Phys Skills: " + pl.calcStat(Stats.MP_PHYSICAL_SKILL_CONSUME, 1, null, null));
					_s.add("==Music: " + pl.calcStat(Stats.MP_DANCE_SKILL_CONSUME, 1, null, null));

					_s.add("==========Shield:");
					_s.add("==Shield Defence: " + pl.calcStat(Stats.SHIELD_DEFENCE, null, null));
					_s.add("==Shield Defence Rate: " + pl.calcStat(Stats.SHIELD_RATE, null, null));
					_s.add("==Shield Defence Angle: " + pl.calcStat(Stats.SHIELD_ANGLE, null, null));

					_s.add("==========Etc:");
					_s.add("==Fatal Blow Rate: " + pl.calcStat(Stats.FATALBLOW_RATE, null, null));
					_s.add("==Phys Skill Evasion Rate: " + pl.calcStat(Stats.PSKILL_EVASION, null, null));
					_s.add("==Counterattack Rate: " + pl.calcStat(Stats.COUNTER_ATTACK, null, null));
					_s.add("==Pole Attack Angle: " + pl.calcStat(Stats.POLE_ATTACK_ANGLE, null, null));
					_s.add("==Pole Target Count: " + pl.calcStat(Stats.POLE_TARGET_COUNT, 1, null, null));
					_s.add("==========DONE.");

					for(String s : _s)
						activeChar.sendMessage(s);
					break;
				case admin_uievent:
					if(wordList.length < 5)
					{
						activeChar.sendMessage("USAGE: //uievent isHide doIncrease startTime endTime Text");
						return false;
					}
					boolean hide;
					boolean increase;
					int startTime;
					int endTime;
					String text;
					try
					{
						hide = Boolean.parseBoolean(wordList[1]);
						increase = Boolean.parseBoolean(wordList[2]);
						startTime = Integer.parseInt(wordList[3]);
						endTime = Integer.parseInt(wordList[4]);
						text = wordList[5];
					}
					catch(NumberFormatException e)
					{
						activeChar.sendMessage("Invalid format");
						return false;
					}
					activeChar.broadcastPacket(new ExSendUIEvent(activeChar, hide, increase, startTime, endTime, text));
					break;
				case admin_opensod:
					if(wordList.length < 1)
					{
						activeChar.sendMessage("USAGE: //opensod minutes");
						return false;
					}
					SoDManager.openSeed(Integer.parseInt(wordList[1]) * 60 * 1000L);
					break;
				case admin_closesod:
					SoDManager.closeSeed();
					break;
				case admin_setsoistage:
					if(wordList.length < 1)
					{
						activeChar.sendMessage("USAGE: //setsoistage stage[1-5]");
						return false;
					}
					SoIManager.setCurrentStage(Integer.parseInt(wordList[1]));
					break;
				case admin_soinotify:
					if(wordList.length < 1)
					{
						activeChar.sendMessage("USAGE: //soinotify [1-3]");
						return false;
					}
					switch(Integer.parseInt(wordList[1]))
					{
						case 1:
							SoIManager.notifyCohemenesKill();
							break;
						case 2:
							SoIManager.notifyEkimusKill();
							break;
						case 3:
							SoIManager.notifyHoEDefSuccess();
							break;
					}
					break;
				case admin_forcenpcinfo:
					GameObject obj2 = activeChar.getTarget();
					if(!obj2.isNpc())
					{
						activeChar.sendMessage("Only NPC target is allowed");
						return false;
					}
					((NpcInstance) obj2).broadcastCharInfo();
					break;
				case admin_loc:
					activeChar.sendMessage("Coords: X:" + activeChar.getLoc().x + " Y:" + activeChar.getLoc().y + " Z:" + activeChar.getLoc().z + " H:" + activeChar.getLoc().h);
					break;
				case admin_locdump:
					st = new StringTokenizer(fullString, " ");
					try
					{
						st.nextToken();
						try
						{
							new File("dumps").mkdir();
							File f = new File("dumps/locdump.txt");
							if(!f.exists())
								f.createNewFile();
							activeChar.sendMessage("Coords: X:" + activeChar.getLoc().x + " Y:" + activeChar.getLoc().y + " Z:" + activeChar.getLoc().z + " H:" + activeChar.getLoc().h);
							FileWriter writer = new FileWriter(f, true);
							writer.write("Loc: " + activeChar.getLoc().x + ", " + activeChar.getLoc().y + ", " + activeChar.getLoc().z + "\n");
							writer.close();
						}
						catch(Exception e)
						{

						}
					}
					catch(Exception e)
					{
						// Case of wrong monster data
					}
					break;
				case admin_undying:
					if(activeChar.isUndying(null))
					{
						activeChar.unsetVar("gm_undying");
						activeChar.setUndying(SpecialEffectState.FALSE);
						activeChar.sendMessage("Undying state has been disabled.");
					}
					else
					{
						activeChar.setUndying(SpecialEffectState.GM);
						activeChar.sendMessage("Undying state has been enabled.");
						if(Config.SAVE_GM_EFFECTS)
							activeChar.setVar("gm_undying", "true", -1);
					}
					break;
				case admin_who:
					int i = 0;
					int j = 0;
					for(Player player : GameObjectsStorage.getPlayers())
					{
						i++;
						if(player.isInStoreMode() || player.isInOfflineMode())
							j++;
					}
					activeChar.sendMessage("Players online: " + i + ", Traders: " + j);
					break;
				case admin_moder:
				{
					GameObject obj = activeChar.getTarget();
					if(obj == null || !obj.isPlayer())
						return false;

					String var = "Moderator";

					Player moder = obj.getPlayer();
					if(moder.getVarB(var))
					{
						moder.unsetVar(var);
						moder.sendMessage("Admin remove moderator access!");
					}
					else
					{
						moder.setVar(var, "true", -1);
						moder.sendMessage("Admin give you super moderator access! Use command .moder for open moderator panel.");
					}
					break;
				}
				case admin_supermoder:
				{
					GameObject obj = activeChar.getTarget();
					if(obj == null || !obj.isPlayer())
						return false;

					String var = "SuperModerator";

					Player moder = obj.getPlayer();
					if(moder.getVarB(var))
					{
						moder.unsetVar(var);
						moder.sendMessage("Admin remove super moderator access!");
					}
					else
					{
						moder.setVar(var, "true", -1);
						moder.sendMessage("Admin give you super moderator access! Use command .moder for open moderator panel.");
					}
					break;
				}
				case admin_come:
				{
					comeHere(activeChar);
				}
				case admin_setpa:
					if(wordList.length < 3)
					{
						activeChar.sendMessage("USAGE: //setpa value day");
						return false;
					}

					if(activeChar.getTarget() == null)
					{
						activeChar.sendMessage("Select target.");
						return false;
					}

					final Player player = activeChar.getTarget().getPlayer();
					final PremiumAccount premium = PremiumHolder.getInstance().getPremium(Integer.parseInt(wordList[1]));
					final long time = TimeUtils.addDay(Integer.parseInt(wordList[2]));
					final long expire = player.getBonus().getExpire(premium) + time;
					PremiumStart.getInstance().start(player, premium.getId(), expire);
					PremiumDAO.getInstance().insert(player, premium.getId(), expire);

					String end = TIME_FORMAT.format(new Date(expire));

					player.broadcastPacket(new MagicSkillUse(player, player, 6463, 1, 0, 0));
					Log.service("Admin give Premium-Account to " + player + " (id:" + premium.getId() + ", name: " + premium.getName() + ") at " + TimeUtils.formatTime((int) (time / 1000L), Language.ENGLISH) + ", End: " + end + ".", this.getClass().getName());

					player.sendMessage("Админ выдал вам премиум на " + wordList[1] + " дней");
					activeChar.sendMessage("Вы выдали премиум игроку " + player.getName() + " на " + wordList[1] + " дней");
					return true;
			}

			return true;
		}

		if(activeChar.getPlayerAccess().CanTeleport)
		{
			switch(command)
			{
				case admin_show_html:
					String html = wordList[1];
					try
					{
						if(html != null)
							if(html.startsWith("tele"))
								activeChar.sendPacket(new HtmlMessage(5).setFile("admin/" + html));
							else
								activeChar.sendMessage("Access denied");
						else
							activeChar.sendMessage("Html page not found");
					}
					catch(Exception npe)
					{
						activeChar.sendMessage("Html page not found");
					}
					break;
				default:
					break;
			}
			return true;
		}

		return false;
	}

	private static void comeHere(Player activeChar)
	{
		GameObject obj = activeChar.getTarget();
		if(obj != null && obj.isNpc())
		{
			NpcInstance temp = (NpcInstance) obj;
			temp.setTarget(activeChar);
			temp.moveToLocation(activeChar.getLoc(), 0, true);
		}
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}

	public void playAdminSound(Player activeChar, String sound)
	{
		activeChar.broadcastPacket(new PlaySound(sound));
		activeChar.sendPacket(new HtmlMessage(5).setFile("admin/admin.htm"));
		activeChar.sendMessage("Playing " + sound + ".");
	}
}