package com.l2cccp.gameserver.handler.voicecommands.impl;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.s2c.InventoryUpdate;
import com.l2cccp.gameserver.utils.Util;

public class CombineTalismans implements IVoicedCommandHandler
{
	private static final String[] COMMANDS = new String[] { "ct" };

	@Override
	public boolean useVoicedCommand(String command, Player activeChar, String args)
	{
		if(command.equals("ct"))
		{
			List<int[]> items = new ArrayList<int[]>();

			for(ItemInstance item : activeChar.getInventory().getItems())
			{
				// Getting talisman
				if(item.getLifeTime() > 0 && item.getName().contains("Talisman"))
					talismanAddToCurrent(items, item.getItemId());
			}

			boolean combined = false;
			for(int[] item : items)
			{
				// Item Count > 1
				if(item[1] > 1)
				{
					int lifeTime = 0;
					List<ItemInstance> talismans = activeChar.getInventory().getItemsByItemId(item[0]);
					for(ItemInstance talisman : talismans)
					{
						lifeTime += talisman.getLifeTime();
						activeChar.getInventory().destroyItem(talisman);
					}

					ItemInstance newTalisman = activeChar.getInventory().addItem(item[0], 1L);
					newTalisman.setLifeTime(lifeTime);
					newTalisman.setJdbcState(JdbcEntityState.UPDATED);
					newTalisman.update();
					activeChar.sendPacket(new InventoryUpdate().addModifiedItem(newTalisman));
					combined = true;
				}
			}

			if(combined)
			{
				for(int[] item : items)
					activeChar.sendMessage("Talismans " + Util.getItemName(item[0]) + " combined into one.");
			}
			else
				activeChar.sendMessage("You don't have Talismans to combine!");
			return true;
		}
		else
			return false;
	}

	private static void talismanAddToCurrent(List<int[]> sameIds, int itemId)
	{
		for(int i = 0; i < sameIds.size(); i++)
		{
			if(sameIds.get(i)[0] == itemId)
			{
				sameIds.get(i)[1] = sameIds.get(i)[1] + 1;
				return;
			}
		}

		sameIds.add(new int[] { itemId, 1 });
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return COMMANDS;
	}
}