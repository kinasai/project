package com.l2cccp.gameserver.handler.bbs;

import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.dao.AcademiciansDAO;
import com.l2cccp.gameserver.dao.AcademyRequestDAO;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.network.l2.s2c.PledgeShowMemberListDelete;
import com.l2cccp.gameserver.network.l2.s2c.PledgeShowMemberListDeleteAll;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;

import java.util.Iterator;

public class AcademicCheck extends RunnableImpl
{
	public static boolean canRun() // Проверяем есть ли смысл запускать пул!
	{
		for(Academicians academic : AcademiciansStorage.getInstance().get())
		{
			Player player = GameObjectsStorage.getPlayer(academic.getObjId());
			if(player != null)
				return true;
		}

		return false;
	}

	@Override
	public void runImpl() throws Exception
	{

         for(Iterator<Academicians> iter = AcademiciansStorage.getInstance().get().iterator(); iter.hasNext();) {
             Academicians academic = iter.next();

			Player player = GameObjectsStorage.getPlayer(academic.getObjId());
			if(player != null)
			{
                if (academic.getTime() < System.currentTimeMillis())
                {   iter.remove();
                    AcademyRequest academy = AcademyStorage.getInstance().getReguest(academic.getClanId());
                    AcademyRequest request = AcademyStorage.getInstance().getReguest(academic.getClanId());
                    AcademiciansDAO.getInstance().delete(academic);
                    if(request != null) {
                        academy.updateSeats();
                        AcademyRequestDAO.getInstance().update(request);
                    }
					player.getClan().removeClanMember(academic.getClanId());
                    player.getClan().broadcastToOnlineMembers(new SystemMessage(SystemMsg.CLAN_MEMBER_S1_HAS_BEEN_EXPELLED).addString(player.getName()), new PledgeShowMemberListDelete(player.getName()));
					player.setClan(null);
					player.setTitle("");
					player.setLeaveClanTime(0);
					player.broadcastCharInfo();
					player.sendPacket(PledgeShowMemberListDeleteAll.STATIC);
                    player.sendPacket(new ExShowScreenMessage("Вы исключены из академии", 3000, ScreenMessageAlign.TOP_CENTER, true));
				}
				else
                    if(player.getClan() != null)
				    {
                    int time = (int) ((academic.getTime() - System.currentTimeMillis()) / 1000);
                    if(time / 3600 < 1)
                        player.sendPacket(new ExShowScreenMessage("На прохождение академии осталось: " + time / 60 + " м.", 3000, ScreenMessageAlign.TOP_CENTER, true));
                    }
                    else
                    {
                        AcademyRequest academy = AcademyStorage.getInstance().getReguest(academic.getClanId());
                        AcademyRequest request = AcademyStorage.getInstance().getReguest(academic.getClanId());
                        AcademiciansDAO.getInstance().delete(academic);
                        AcademiciansStorage.getInstance().get().remove(academic);
                        if(request != null) {
                            academy.updateSeats();
                            AcademyRequestDAO.getInstance().update(request);
                        }
                        player.sendPacket(new ExShowScreenMessage("Вы покинули академию", 3000, ScreenMessageAlign.TOP_CENTER, true));
                    }
			}
		}


        if(canRun())
            ThreadPoolManager.getInstance().schedule(this, 60000L);
    }
}