package com.l2cccp.gameserver.handler.voicecommands.impl;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.TradeHelper;

public class Offline implements IVoicedCommandHandler
{
	private String[] _commandList = new String[] { "offline" };

	@Override
	public boolean useVoicedCommand(String command, Player activeChar, String args)
	{
		if(!Config.SERVICES_OFFLINE_TRADE_ALLOW)
			return false;

		if(activeChar.isInObserverMode() || activeChar.getOlympiadGame() != null  || Olympiad.isRegisteredInComp(activeChar) || activeChar.getKarma() > 0)
		{
			activeChar.sendActionFailed();
			return false;
		}

		if(!activeChar.isInStoreMode())
		{
			activeChar.sendPacket(new HtmlMessage(0).setFile("command/offline_err01.htm"));
			return false;
		}

		if(activeChar.getLevel() < Config.SERVICES_OFFLINE_TRADE_MIN_LEVEL)
		{
			activeChar.sendPacket(new HtmlMessage(0).setFile("command/offline_err02.htm").replace("%level%", String.valueOf(Config.SERVICES_OFFLINE_TRADE_MIN_LEVEL)));
			return false;
		}

		if(Config.SERVICES_OFFLINE_TRADE_ALLOW_OFFSHORE && !activeChar.isInZone(Zone.ZoneType.offshore))
		{
			activeChar.sendPacket(new HtmlMessage(0).setFile("command/offline_err03.htm"));
			return false;
		}

		if(Config.SERVICES_OFFLINE_TRADE_PRICE > 0 && Config.SERVICES_OFFLINE_TRADE_PRICE_ITEM != 0)
		{
			long adena = Config.SERVICES_OFFLINE_TRADE_PRICE_ITEM == ItemTemplate.ITEM_ID_ADENA ? Config.SERVICES_OFFLINE_TRADE_PRICE : 0;
			if(!TradeHelper.validateStore(activeChar, adena) || !ItemFunctions.deleteItem(activeChar, Config.SERVICES_OFFLINE_TRADE_PRICE_ITEM, Config.SERVICES_OFFLINE_TRADE_PRICE))
			{
				activeChar.sendPacket(new HtmlMessage(0).setFile("command/offline_err04.htm").replace("%item_id%", String.valueOf(Config.SERVICES_OFFLINE_TRADE_PRICE_ITEM)).replace("%item_count%", String.valueOf(Config.SERVICES_OFFLINE_TRADE_PRICE)));
				return false;
			}
		}

		activeChar.offline();
		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}