package com.l2cccp.gameserver.handler.voicecommands.impl;

import org.apache.commons.lang3.math.NumberUtils;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.config.PlayerConfig;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.ClientSetTime;
import com.l2cccp.gameserver.utils.OnlineUtils;
import com.l2cccp.gameserver.utils.TimeUtils;

public class Cfg implements IVoicedCommandHandler
{
	private String[] _commandList = new String[] { "cfg" };

	@Override
	public boolean useVoicedCommand(String command, Player activeChar, String args)
	{
		PlayerConfig config = activeChar.getConfig();
		if(command.equals("cfg"))
		{
			if(args != null)
			{
				String[] param = args.split(" ");
				if(param.length == 2)
				{
					String key = param[0];
					String value = param[1];
					if(key.equalsIgnoreCase("dli"))
					{
						if(value.equalsIgnoreCase("on"))
							activeChar.getConfig().setImages(true);
						else if(value.equalsIgnoreCase("of"))
							activeChar.getConfig().setImages(false);
					}
					else if(key.equalsIgnoreCase("noe"))
					{
						if(value.equalsIgnoreCase("on"))
							activeChar.getConfig().setExpOff(true);
						else if(value.equalsIgnoreCase("of"))
							activeChar.getConfig().setExpOff(false);
					}
					else if(key.equalsIgnoreCase("notraders"))
					{
						if(value.equalsIgnoreCase("on"))
						{
							activeChar.setNotShowTraders(true);
							activeChar.getConfig().setHideTraders(true);
						}
						else if(value.equalsIgnoreCase("of"))
						{
							activeChar.setNotShowTraders(false);
							activeChar.getConfig().setHideTraders(false);
						}
					}
					else if(key.equalsIgnoreCase("autoAtt")) {
						if(value.equalsIgnoreCase("on")) {
							if(!activeChar.getVarB("enableAutoAtt")) {
								activeChar.setVar("enableAutoAtt", "1", -1);
							}
						} else if(value.equalsIgnoreCase("of")) {
							if(activeChar.getVarB("enableAutoAtt")) {
								activeChar.unsetVar("enableAutoAtt");
							}
						}
					}
					else if(key.equalsIgnoreCase("buffAnimRange"))
					{
						int range = 15 * NumberUtils.toInt(value, 0);

						if(range < 0)
							range = -1;
						else if(range > 1500)
							range = 1500;

						activeChar.setBuffAnimRange(range);
						activeChar.getConfig().setBuffDistance(range);
					}
					else if(key.equalsIgnoreCase("disableFogAndRain"))
					{
						if(value.equalsIgnoreCase("on"))
						{
							activeChar.setDisableFogAndRain(true);
							activeChar.sendPacket(new ClientSetTime(activeChar));
							activeChar.getConfig().setRain(true);
						}
						else if(value.equalsIgnoreCase("of"))
						{
							activeChar.setDisableFogAndRain(false);
							activeChar.sendPacket(new ClientSetTime(activeChar));
							activeChar.getConfig().setRain(false);
						}
					}
					else if(key.equalsIgnoreCase("noShift"))
					{
						if(value.equalsIgnoreCase("on"))
							activeChar.getConfig().setClassicShift(true);
						else if(value.equalsIgnoreCase("of"))
							activeChar.getConfig().setClassicShift(false);
					}
					else if(Config.SERVICES_ENABLE_NO_CARRIER && key.equalsIgnoreCase("noCarrier"))
					{
						int time = NumberUtils.toInt(value, 0);

						if(time <= 0)
							time = 0;
						else if(time > Config.SERVICES_NO_CARRIER_MAX_TIME)
							time = Config.SERVICES_NO_CARRIER_MAX_TIME;
						else if(time < Config.SERVICES_NO_CARRIER_MIN_TIME)
							time = Config.SERVICES_NO_CARRIER_MIN_TIME;
						activeChar.getConfig().setNoCarrier(time);
					}
					else if(key.equalsIgnoreCase("translit"))
					{
						if(value.equalsIgnoreCase("on"))
							activeChar.getConfig().setTranslit("tl");
						else if(value.equalsIgnoreCase("la"))
							activeChar.getConfig().setTranslit("tc");
						else if(value.equalsIgnoreCase("of"))
							activeChar.getConfig().setTranslit("Off");
					}
					else if(Config.AUTO_LOOT_INDIVIDUAL)
					{
						int id = NumberUtils.toInt(value, 0);
						if(key.equalsIgnoreCase("autoloot"))
						{
							activeChar.getConfig().setAutoLoot(id);
							config.setAutoLoot(id);
						}

						if(key.equalsIgnoreCase("autolooth"))
						{
							boolean on = id == 1;
							activeChar.getConfig().setAutoLootHerb(on);
							config.setAutoLootHerb(on);
						}
					}
				}
			}
		}

		HtmlMessage dialog = new HtmlMessage(5);
		dialog.setFile("command/cfg.htm");
		String ON = "<font color=\"669900\">On</font>";
		String OFF = "<font color=\"FF3333\">Off</font>";
		String NA = "<font color=\"FF2222\">N/A</font>";
		dialog.replace("%dli%", config.showImages() ? ON : OFF);
		dialog.replace("%noe%", config.isExpOff() ? ON : OFF);
		dialog.replace("%notraders%", config.isHideTraders() ? ON : OFF);
		dialog.replace("%autoAtt%", activeChar.getVarB("enableAutoAtt") ? ON : OFF);
		dialog.replace("%noShift%", config.isClassicShift() ? ON : OFF);
		dialog.replace("%noCarrier%", Config.SERVICES_ENABLE_NO_CARRIER ? String.valueOf(config.getNoCarrier()) : NA);
		dialog.replace("%disableFogAndRain%", config.showRain() ? ON : OFF);

		if(activeChar.buffAnimRange() < 0)
			dialog.replace("%buffAnimRange%", OFF);
		else if(activeChar.buffAnimRange() == 0)
		{
			if(activeChar.isLangRus())
				dialog.replace("%buffAnimRange%", "<font color=\"FFFF00\">Свои</font>");
			else
				dialog.replace("%buffAnimRange%", "<font color=\"FFFF00\">Self</font>");
		}
		else
			dialog.replace("%buffAnimRange%", String.valueOf(activeChar.buffAnimRange() / 15));

		String tl = config.getTranslit();
		if(tl == null || tl.equals("Off"))
			dialog.replace("%translit%", OFF);
		else if(tl.equals("tl"))
			dialog.replace("%translit%", ON);
		else
			dialog.replace("%translit%", "<font color=\"FFFF00\">Lt</font>");

		if(Config.AUTO_LOOT_INDIVIDUAL)
		{
			switch(config.getAutoLoot())
			{
				case 1:
					dialog.replace("%autoLoot%", ON);
					break;
				case 2:
					if(activeChar.isLangRus())
						dialog.replace("%autoLoot%", "<font color=\"FFFF00\">Кроме стрел</font>");
					else
						dialog.replace("%autoLoot%", "<font color=\"FFFF00\">Except Arrows</font>");
					break;
				default:
					dialog.replace("%autoLoot%", OFF);
					break;
			}
			dialog.replace("%autoLootH%", config.isAutoLootHerb() ? ON : OFF);
		}
		else
		{
			dialog.replace("%autoLoot%", NA);
			dialog.replace("%autoLootH%", NA);
		}

		dialog.replace("%online%", OnlineUtils.getformat(true));
		dialog.replace("%offline%", OnlineUtils.getformat(false));
		if(activeChar.hasBonus())
			dialog.replace("%premium%", "<font color=\"FFFF00\">Is Active</font>");
		else
			dialog.replace("%premium%", "<font color=\"FF1111\">No Premium</font>");

		dialog.replace("%time%", String.valueOf(TimeUtils.time()));
		activeChar.sendPacket(dialog);

		return true;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}