package com.l2cccp.gameserver.handler.admincommands.impl;

import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.config.parser.ChatFilterParser;
import com.l2cccp.gameserver.dao.CharacterQuestDAO;
import com.l2cccp.gameserver.dao.OlympiadNobleDAO;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.ProductHolder;
import com.l2cccp.gameserver.data.xml.parser.BuyListParser;
import com.l2cccp.gameserver.data.xml.parser.CharTemplateParser;
import com.l2cccp.gameserver.data.xml.parser.DressArmorParser;
import com.l2cccp.gameserver.data.xml.parser.DressCloakParser;
import com.l2cccp.gameserver.data.xml.parser.DressShieldParser;
import com.l2cccp.gameserver.data.xml.parser.DressWeaponParser;
import com.l2cccp.gameserver.data.xml.parser.FortuneBoxParser;
import com.l2cccp.gameserver.data.xml.parser.MultiSellParser;
import com.l2cccp.gameserver.data.xml.parser.NpcParser;
import com.l2cccp.gameserver.data.xml.parser.PlayerBalancerParser;
import com.l2cccp.gameserver.data.xml.parser.PremiumParser;
import com.l2cccp.gameserver.data.xml.parser.PromoCodeParser;
import com.l2cccp.gameserver.data.xml.parser.ReplacementParser;
import com.l2cccp.gameserver.data.xml.parser.StringParser;
import com.l2cccp.gameserver.data.xml.parser.TeleportPointParser;
import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.instancemanager.SpawnManager;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.olympiad.OlympiadDatabase;
import com.l2cccp.gameserver.model.quest.QuestState;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.tables.FishTable;
import com.l2cccp.gameserver.tables.PetDataTable;
import com.l2cccp.gameserver.tables.SkillTable;

public class AdminReload implements IAdminCommandHandler
{
	private static enum Commands
	{
		admin_reload,
		admin_reload_config,
		admin_reload_multisell,
		admin_reload_gmaccess,
		admin_reload_htm,
		admin_reload_qs,
		admin_reload_qs_help,
		admin_reload_skills,
		admin_reload_npc,
		admin_reload_spawn,
		admin_reload_fish,
		admin_reload_chatfilters,
		admin_reload_translit,
		admin_reload_shops,
		admin_reload_static,
		admin_reload_pets,
		admin_reload_locale,
		admin_reload_nobles,
		admin_reload_im,
		admin_reload_promocodes,
		admin_reload_teleport,
		admin_reload_dress,
		admin_reload_fbox,
		admin_reload_premium,
		admin_reload_pbalance,
		admin_reload_pcparam
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player activeChar)
	{
		Commands command = (Commands) comm;

		if(!activeChar.getPlayerAccess().CanReload)
			return false;

		switch(command)
		{
			case admin_reload:
				break;
			case admin_reload_config:
			{
				try
				{
					Config.load();
				}
				catch(Exception e)
				{
					activeChar.sendMessage("Error: " + e.getMessage() + "!");
					return false;
				}
				activeChar.sendMessage("Config reloaded!");
				break;
			}
			case admin_reload_multisell:
			{
				MultiSellParser.getInstance().reload();
				activeChar.sendMessage("Multisell list reloaded!");
				break;
			}
			case admin_reload_gmaccess:
			{
				try
				{
					Config.loadGMAccess();
					for(Player player : GameObjectsStorage.getPlayers())
						if(!Config.EVERYBODY_HAS_ADMIN_RIGHTS)
							player.setPlayerAccess(Config.gmlist.get(player.getObjectId()));
						else
							player.setPlayerAccess(Config.gmlist.get(new Integer(0)));
				}
				catch(Exception e)
				{
					return false;
				}
				activeChar.sendMessage("GMAccess reloaded!");
				break;
			}
			case admin_reload_htm:
			{
				HtmCache.getInstance().reload();
				activeChar.sendMessage("HTML cache clearned.");
				break;
			}
			case admin_reload_qs:
			{
				if(fullString.endsWith("all"))
					for(Player p : GameObjectsStorage.getPlayers())
						reloadQuestStates(p);
				else
				{
					GameObject t = activeChar.getTarget();

					if(t != null && t.isPlayer())
					{
						Player p = (Player) t;
						reloadQuestStates(p);
					}
					else
						reloadQuestStates(activeChar);
				}
				break;
			}
			case admin_reload_qs_help:
			{
				activeChar.sendMessage("");
				activeChar.sendMessage("Quest Help:");
				activeChar.sendMessage("reload_qs_help - This Message.");
				activeChar.sendMessage("reload_qs <selected target> - reload all quest states for target.");
				activeChar.sendMessage("reload_qs <no target or target is not player> - reload quests for self.");
				activeChar.sendMessage("reload_qs all - reload quests for all players in world.");
				activeChar.sendMessage("");
				break;
			}
			case admin_reload_skills:
			{
				SkillTable.getInstance().reload();
				break;
			}
			case admin_reload_npc:
			{
				NpcParser.getInstance().reload();
				break;
			}
			case admin_reload_spawn:
			{
				ThreadPoolManager.getInstance().execute(new RunnableImpl(){
					@Override
					public void runImpl() throws Exception
					{
						SpawnManager.getInstance().reloadAll();
					}
				});
				break;
			}
			case admin_reload_fish:
			{
				FishTable.getInstance().reload();
				break;
			}
			case admin_reload_chatfilters:
			{
				ChatFilterParser.getInstance().reload();
				break;
			}
			case admin_reload_translit:
			{
				ReplacementParser.getInstance().reload();
				break;
			}
			case admin_reload_shops:
			{
				BuyListParser.getInstance().reload();
				break;
			}
			case admin_reload_static:
			{
				//StaticObjectsTable.getInstance().reloadStaticObjects();
				break;
			}
			case admin_reload_pets:
			{
				PetDataTable.getInstance().reload();
				break;
			}
			case admin_reload_locale:
			{
				StringParser.getInstance().reload();
				break;
			}
			case admin_reload_nobles:
			{
				OlympiadNobleDAO.getInstance().select();
				OlympiadDatabase.loadNoblesRank();
				break;
			}
			case admin_reload_im:
			{
				ProductHolder.getInstance().reload();
				break;
			}
			case admin_reload_promocodes:
			{
				PromoCodeParser.getInstance().reload();
				break;
			}
			case admin_reload_teleport:
			{
				TeleportPointParser.getInstance().reload();
				break;
			}
			case admin_reload_dress:
			{
				DressArmorParser.getInstance().reload();
				DressCloakParser.getInstance().reload();
				DressShieldParser.getInstance().reload();
				DressWeaponParser.getInstance().reload();
				break;
			}
			case admin_reload_fbox:
			{
				FortuneBoxParser.getInstance().reload();
				break;
			}
			case admin_reload_premium:
			{
				PremiumParser.getInstance().reload();
				break;
			}
			case admin_reload_pbalance:
			{
				PlayerBalancerParser.getInstance().reload();
				break;
			}
			case admin_reload_pcparam:
			{
				CharTemplateParser.getInstance().reload();
				break;
			}
		}
		activeChar.sendPacket(new HtmlMessage(5).setFile("admin/reload.htm"));
		return true;
	}

	private void reloadQuestStates(Player p)
	{
		for(QuestState qs : p.getAllQuestsStates())
			p.removeQuestState(qs.getQuest().getId());
		CharacterQuestDAO.getInstance().select(p);
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}
}