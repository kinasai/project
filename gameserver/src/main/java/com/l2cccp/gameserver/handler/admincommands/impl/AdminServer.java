package com.l2cccp.gameserver.handler.admincommands.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.ai.DefaultAI;
import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.instancemanager.ServerVariables;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.WorldRegion;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.instances.RaidBossInstance;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;

/**
 * This class handles following admin commands: - help path = shows
 * admin/path file to char, should not be used by GM's directly
 */
public class AdminServer implements IAdminCommandHandler
{
	private static final Logger _log = LoggerFactory.getLogger(AdminServer.class);

	private static enum Commands
	{
		admin_server,
		admin_check_actor,
		admin_setvar,
		admin_set_ai_interval,
		admin_pool
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player activeChar)
	{
		Commands command = (Commands) comm;

		if(!activeChar.getPlayerAccess().Menu)
			return false;

		switch(command)
		{
			case admin_server:
				try
				{
					String val = fullString.substring(13);
					showHelpPage(activeChar, val);
				}
				catch(StringIndexOutOfBoundsException e)
				{
					// case of empty filename
				}
				break;
			case admin_check_actor:
				GameObject obj = activeChar.getTarget();
				if(obj == null)
				{
					activeChar.sendMessage("target == null");
					return false;
				}

				if(!obj.isCreature())
				{
					activeChar.sendMessage("target is not a character");
					return false;
				}

				Creature target = (Creature) obj;
				CharacterAI ai = target.getAI();
				if(ai == null)
				{
					activeChar.sendMessage("ai == null");
					return false;
				}

				Creature actor = ai.getActor();
				if(actor == null)
				{
					activeChar.sendMessage("actor == null");
					return false;
				}

				activeChar.sendMessage("actor: " + actor);
				break;
			case admin_setvar:
				if(wordList.length != 3)
				{
					activeChar.sendMessage("Incorrect argument count!!!");
					return false;
				}
				ServerVariables.set(wordList[1], wordList[2]);
				activeChar.sendMessage("Value changed.");
				break;
			case admin_set_ai_interval:
				if(wordList.length != 2)
				{
					activeChar.sendMessage("Incorrect argument count!!!");
					return false;
				}
				int interval = Integer.parseInt(wordList[1]);
				int count = 0;
				int count2 = 0;
				for(final NpcInstance npc : GameObjectsStorage.getNpcs())
				{
					if(npc == null || npc instanceof RaidBossInstance)
						continue;
					final CharacterAI char_ai = npc.getAI();
					if(char_ai instanceof DefaultAI)
						try
						{
							final java.lang.reflect.Field field = com.l2cccp.gameserver.ai.DefaultAI.class.getDeclaredField("AI_TASK_DELAY");
							field.setAccessible(true);
							field.set(char_ai, interval);

							if(char_ai.isActive())
							{
								char_ai.stopAITask();
								count++;
								WorldRegion region = npc.getCurrentRegion();
								if(region != null && region.isActive())
								{
									char_ai.startAITask();
									count2++;
								}
							}
						}
						catch(Exception e)
						{

						}
				}
				activeChar.sendMessage(count + " AI stopped, " + count2 + " AI started");
				break;
			case admin_pool:
			{
				final int CPU = Runtime.getRuntime().availableProcessors();
				final int REC_POOL_SIZE = CPU * 4;
				final int REC_QUEUE_SIZE = CPU * 2;
				if(Config.SCHEDULED_THREAD_POOL_SIZE != REC_POOL_SIZE || Config.EXECUTOR_THREAD_POOL_SIZE != REC_QUEUE_SIZE)
				{
					activeChar.sendMessage("Server setting is wrong!");
					activeChar.sendMessage("Active Core (CPU) -> " + CPU);
					activeChar.sendMessage("Scheduled Thread Pool Size: " + Config.SCHEDULED_THREAD_POOL_SIZE);
					activeChar.sendMessage("Scheduled Thread Pool Recommended Size: " + REC_POOL_SIZE);
					activeChar.sendMessage("Executor Thread Pool Size: " + Config.EXECUTOR_THREAD_POOL_SIZE);
					activeChar.sendMessage("Executor Thread Pool Recommended Size: " + REC_QUEUE_SIZE);
				}

				_log.info(ThreadPoolManager.getInstance().getStats().toString());
			}
		}

		return true;
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}

	public static void showHelpPage(Player activeChar, String filename)
	{
		HtmlMessage adminReply = new HtmlMessage(5);
		adminReply.setFile("admin/" + filename);
		activeChar.sendPacket(adminReply);
	}
}