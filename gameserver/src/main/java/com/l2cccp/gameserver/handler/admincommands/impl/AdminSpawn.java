package com.l2cccp.gameserver.handler.admincommands.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.model.HardSpawner;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.templates.spawn.PeriodOfDay;
import com.l2cccp.gameserver.templates.spawn.SpawnNpcInfo;
import com.l2cccp.gameserver.templates.spawn.SpawnTemplate;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;

public class AdminSpawn implements IAdminCommandHandler
{
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

	private Map<Integer, List<SpawnModel>> population;

	private static enum Commands
	{
		admin_spawn,
		admin_populate
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player admin)
	{
		Commands command = (Commands) comm;

		if(!admin.getPlayerAccess().CanEditNPC)
			return false;

		switch(command)
		{
			case admin_spawn:
			{
				try
				{
					if(wordList.length == 1)
						throw new IllegalArgumentException();

					int npcId = Integer.parseInt(wordList[1]);
					int count = wordList.length >= 3 ? Integer.parseInt(wordList[2]) : 1;
					int respawn = wordList.length >= 4 ? Integer.parseInt(wordList[2]) : 0;

					spawn(admin, npcId, count, respawn);
					return true;
				}
				catch(Exception e)
				{
					e.printStackTrace();
					admin.sendMessage("USAGE: //spawn npcId [count] [respawn]");
				}
			}
			case admin_populate:
			{
				if(population == null)
					population = new HashMap<Integer, List<SpawnModel>>();

				try
				{
					final String key = wordList[1];
					if(key.equalsIgnoreCase("save"))
					{
						if(population.containsKey(admin.getObjectId()))
						{
							final List<SpawnModel> populate = population.get(admin.getObjectId());
							save(populate);
							return true;
						}
						else
						{
							admin.sendMessage("You don't make any spawn yet!");
							return false;
						}
					}

					int npcId = Integer.parseInt(key);
					int count = wordList.length >= 3 ? Integer.parseInt(wordList[2]) : 1;
					int respawn = wordList.length >= 4 ? Integer.parseInt(wordList[3]) : 0;
					spawn(admin, npcId, count, respawn);

					if(!population.containsKey(admin.getObjectId()))
						population.put(admin.getObjectId(), new ArrayList<SpawnModel>());

					final List<SpawnModel> populate = population.get(admin.getObjectId());
					populate.add(new SpawnModel(npcId, admin.getLoc(), count, respawn));
					return true;
				}
				catch(Exception e)
				{
					e.printStackTrace();
					admin.sendMessage("USAGE: //populate npcId [count] [respawn]");
				}
			}
		}

		return false;
	}

	private void save(List<SpawnModel> populate)
	{
		Document document = DocumentHelper.createDocument();
		document.addDocType("list", null, "spawn.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");

		Element list = document.addElement("list");

		for(SpawnModel model : populate)
		{
			Element spawn = list.addElement("spawn");
			spawn.addAttribute("count", String.valueOf(model.count));
			spawn.addAttribute("respawn", String.valueOf(model.respawn));
			spawn.addAttribute("period_of_day", String.valueOf(PeriodOfDay.NONE.name()));

			Element point = spawn.addElement("point");
			final Location loc = model.location;
			point.addAttribute("x", String.valueOf(loc.x));
			point.addAttribute("y", String.valueOf(loc.y));
			point.addAttribute("z", String.valueOf(loc.z));
			point.addAttribute("h", String.valueOf(loc.h));

			Element npc = spawn.addElement("npc");
			npc.addAttribute("id", String.valueOf(model.npcId));
		}

		try
		{
			File f = new File(Config.DATAPACK_ROOT, "data/parser/spawn/save/[" + DATE_FORMAT.format(new Date(System.currentTimeMillis())) + " - " + TIME_FORMAT.format(date) + "] " + populate.size() + " Points.xml");
			if(!f.exists())
				f.createNewFile();

			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
			prettyPrint.setIndent("\t");
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(f), prettyPrint);
			xmlWriter.write(document);
			xmlWriter.close();
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}

	private void spawn(final Player admin, final int npcId, final int count, final int respawn)
	{
		if(respawn == 0)
		{
			for(int i = 0; i < count; i++)
				NpcUtils.spawnSingle(npcId, admin.getLoc(), admin.getReflection());
		}
		else
		{
			SpawnTemplate template = new SpawnTemplate(PeriodOfDay.NONE, count, respawn, 0);
			template.addNpc(new SpawnNpcInfo(npcId, count, StatsSet.EMPTY));
			template.addSpawnRange(admin.getLoc());
			HardSpawner spawner = new HardSpawner(template);
			spawner.setAmount(count);
			spawner.setReflection(admin.getReflection());
			spawner.setRespawnDelay(respawn, 0);
			spawner.startRespawn();
			spawner.init();
		}
	}

	private class SpawnModel
	{
		private final int npcId;
		private final Location location;
		private final int count;
		private final int respawn;

		private SpawnModel(final int npcId, final Location location, final int count, final int respawn)
		{
			this.npcId = npcId;
			this.location = location;
			this.count = count;
			this.respawn = respawn;
		}
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}
}