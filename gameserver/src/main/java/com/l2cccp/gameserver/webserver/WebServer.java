package com.l2cccp.gameserver.webserver;

import java.io.File;
import java.lang.*;
import java.io.FileWriter;
import java.util.logging.Logger;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.utils.OnlineUtils;

public class WebServer
        implements Runnable
{
    protected static Logger _log = Logger.getLogger(WebServer.class.getName());

    public WebServer() {}

    public void run()
    {
        try {
            File workingDir = new File(Config.WEB_SERVER_ROOT);
            for (File f : workingDir.listFiles())
            {
                if (f.getName().endsWith(".fst"))
                {

                   String content = String.valueOf(OnlineUtils.get(true) + OnlineUtils.get(false));

                    if (content != null)
                    {
                        try
                        {

                            String text = content;
                            String name = f.getPath();
                            name = name.substring(0, name.length() - 4);
                            name = name + (text.startsWith("<!DOCTYPE") ? ".html" : ".txt");
                            File out = new File(name);
                            out.delete();
                            out.createNewFile();
                            FileWriter fw = new FileWriter(out);
                            fw.write(text);
                            fw.close();
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (Exception e) {
        e.printStackTrace();
        }
        }
        }

