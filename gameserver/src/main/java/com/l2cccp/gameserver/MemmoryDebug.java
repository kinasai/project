package com.l2cccp.gameserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.lang.StatsUtils;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class MemmoryDebug extends RunnableImpl
{
	private final Logger _log = LoggerFactory.getLogger(MemmoryDebug.class);
	private static int old = 0;

	@Override
	public void runImpl()
	{
		final int max = (int) (StatsUtils.getMemMax() / 0x100000);
		final int used = (int) (StatsUtils.getMemUsed() / 0x100000);
		final int diff = Math.abs(old - used);
		final String info = "Load has " + (old < used ? "increased" : "fell") + " by " + diff + " MB Diff: " + Util.cutOff(employ(diff, old), 2) + "%";
		old = used;
		_log.info("MEM DEBUG: Current used: " + used + " MB. Max: " + max + ". Employ -> " + Util.cutOff(employ(used, max), 2) + "%");
		_log.info("MEM RESULT: " + info);
	}

	private static double employ(double curr, double max)
	{
		return (100 + (100 * (curr * 100 / max)) / 100) - 100;
	}
}