package com.l2cccp.gameserver.data.client.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.client.holder.NpcNameLineHolder;
import com.l2cccp.gameserver.templates.client.NpcNameLine;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class NpcNameLineParser extends AbstractDirParser<NpcNameLineHolder>
{
	private static final NpcNameLineParser _instance = new NpcNameLineParser();

	public static NpcNameLineParser getInstance()
	{
		return _instance;
	}

	private NpcNameLineParser()
	{
		super(NpcNameLineHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/client");
	}

	@Override
	public String getDTDFileName()
	{
		return "npc_name.dtd";
	}

	@Override
	public boolean isIgnored(File f)
	{
		return !f.getName().startsWith("npc_name-");
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		Language lang = Language.valueOf(rootElement.attributeValue("lang"));
		for(Iterator<Element> iterator = rootElement.elementIterator(); iterator.hasNext();)
		{
			Element dataElement = iterator.next();

			int npcId = Integer.parseInt(dataElement.attributeValue("id"));
			String name = dataElement.attributeValue("name");
			String title = dataElement.attributeValue("title");

			NpcNameLine line = new NpcNameLine(lang, npcId, name, title);
			getHolder().put(lang, line);
		}
	}
}