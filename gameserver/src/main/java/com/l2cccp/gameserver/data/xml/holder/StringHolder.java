package com.l2cccp.gameserver.data.xml.holder;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.utils.Language;

public class StringHolder extends AbstractHolder
{
	private static final StringHolder _instance = new StringHolder();
	private Map<Language, Map<String, String>> _strings = new HashMap<Language, Map<String, String>>();

	public static StringHolder getInstance()
	{
		return _instance;
	}

	private StringHolder()
	{

	}

	public String getString(String name, Player player)
	{
		Language lang = player == null ? Language.ENGLISH : player.getLanguage();
		return getString(name, lang);
	}

	public String getString(Player player, String name)
	{
		Language lang = player == null ? Language.ENGLISH : player.getLanguage();

		String text = getString(name, lang);
		if(text == null && player != null)
		{
			text = "Not find string: " + name + "; for lang: " + lang;
			_strings.get(lang).put(name, text);
		}

		return text;
	}

	public String getString(String address, Language lang)
	{
		Map<String, String> strings = _strings.get(lang);

		return strings.get(address);
	}

	@Override
	public void log()
	{
		for(Map.Entry<Language, Map<String, String>> entry : _strings.entrySet())
			info("load strings: " + entry.getValue().size() + " for lang: " + entry.getKey());
	}

	@Override
	public int size()
	{
		return 0;
	}

	@Override
	public void clear()
	{
		_strings.clear();
	}

	public void addString(String key, String lang, String text)
	{
		Language language = null;
		for(Language value : Language.VALUES)
		{
			if(value.getShortName().equals(lang))
			{
				language = value;
				break;
			}
		}

		if(language == null)
		{
			_log.error("Unknown language: " + lang + ". Key: " + key);
			return;
		}

		Map<String, String> map = _strings.get(language);
		if(map == null)
		{
			_strings.put(language, map = new LinkedHashMap<String, String>());
		}
		map.put(key, text);
	}
}
