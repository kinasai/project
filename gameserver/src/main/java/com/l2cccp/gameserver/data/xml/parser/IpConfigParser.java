package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;

import org.dom4j.Element;
import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.commons.net.utils.Net;
import com.l2cccp.commons.net.utils.NetInfo;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.IpConfigHolder;
import com.l2cccp.gameserver.network.authcomm.channel.BaseServerChannel;
import com.l2cccp.gameserver.network.authcomm.channel.ProxyServerChannel;

public class IpConfigParser extends AbstractFileParser<IpConfigHolder>
{
	public static final boolean ENABLED = true;
	public static final IpConfigParser _instance = new IpConfigParser();

	public static IpConfigParser getInstance()
	{
		return _instance;
	}

	private IpConfigParser()
	{
		super(IpConfigHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File("config/ipconfig/ipconfig.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "ipconfig.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		if(!Config.IPCONFIG_ENABLE)
			return;

		for(Element element : rootElement.elements("server"))
		{
			String ipAddress = element.attributeValue("ip_address");

			BaseServerChannel baseServerChannel = new BaseServerChannel(Config.REQUEST_ID);

			for(Element subnet : element.elements("subnet"))
			{
				Net mask = Net.valueOf(subnet.attributeValue("mask"));
				String ip = subnet.attributeValue("ip_address");

				if(ip == null)
					ip = ipAddress;

				baseServerChannel.add(new NetInfo(mask, ip, Config.PORTS_GAME));
			}
			getHolder().addChannel(baseServerChannel);
		}

		for(Element channelElement : rootElement.elements("channel"))
		{
			int id = Integer.parseInt(channelElement.attributeValue("gameserverId"));

			Element confElement = channelElement.element("conf");

			if(confElement == null)
				return;

			String ip = confElement.attributeValue("address");
			int port = Integer.parseInt(confElement.attributeValue("port"));

			getHolder().addChannel(new ProxyServerChannel(id, ip, port));
		}
	}
}
