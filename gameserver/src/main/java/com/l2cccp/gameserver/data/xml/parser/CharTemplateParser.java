package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.CharTemplateHolder;
import com.l2cccp.gameserver.model.actor.instances.player.ShortCut;
import com.l2cccp.gameserver.model.base.Race;
import com.l2cccp.gameserver.model.base.Type;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.templates.item.StartItem;
import com.l2cccp.gameserver.templates.player.BaseMDef;
import com.l2cccp.gameserver.templates.player.BasePDef;
import com.l2cccp.gameserver.templates.player.BaseStats;
import com.l2cccp.gameserver.templates.player.ShortcutPage;
import com.l2cccp.gameserver.templates.player.ShortcutType;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CharTemplateParser extends AbstractDirParser<CharTemplateHolder>
{
	private static final CharTemplateParser _instance = new CharTemplateParser();

	public static CharTemplateParser getInstance()
	{
		return _instance;
	}

	protected CharTemplateParser()
	{
		super(CharTemplateHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/pc_parameter/char_templates/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "templates.dtd";
	}

	@Override
	protected void readData(Element element) throws Exception
	{
		for(Iterator<Element> iterator = element.elementIterator("character"); iterator.hasNext();)
		{
			Element character = iterator.next();
			final int id = Integer.parseInt(character.attributeValue("id"));
			final String name = character.attributeValue("name");
			final Race race = Race.valueOf((character.attributeValue("race").toLowerCase()));
			final Type char_type = Type.valueOf((character.attributeValue("type").toUpperCase()));
			//	final int level = Integer.parseInt(character.attributeValue("level"));

			Map<Integer, List<StartItem>> items = new HashMap<Integer, List<StartItem>>();
			StatsSet sets = new StatsSet();
			sets.set("name", name);
			BaseStats stats = null;
			BasePDef pDef = null;
			BaseMDef mDef = null;
			Location location = new Location();
			List<ShortCut> shortcuts = new ArrayList<ShortCut>();
			for(Iterator<Element> it = character.elementIterator(); it.hasNext();)
			{
				Element param = it.next();

				if("set".equalsIgnoreCase(param.getName()))
				{
					final String key = param.attributeValue("key");
					final String value = param.attributeValue("value");
					sets.set(key, value);
				}
				else if("armor".equalsIgnoreCase(param.getName()))
				{
					final int chest = Integer.parseInt(param.attributeValue("chest"));
					final int legs = Integer.parseInt(param.attributeValue("legs"));
					final int helmet = Integer.parseInt(param.attributeValue("helmet"));
					final int boots = Integer.parseInt(param.attributeValue("boots"));
					final int gloves = Integer.parseInt(param.attributeValue("gloves"));
					final int underwear = Integer.parseInt(param.attributeValue("underwear"));
					final int cloak = Integer.parseInt(param.attributeValue("cloak"));

					pDef = new BasePDef(chest, legs, helmet, boots, gloves, underwear, cloak);
				}
				else if("jewels".equalsIgnoreCase(param.getName()))
				{
					final int r_earring = Integer.parseInt(param.attributeValue("r_earring"));
					final int l_earring = Integer.parseInt(param.attributeValue("l_earring"));
					final int necklace = Integer.parseInt(param.attributeValue("necklace"));
					final int r_ring = Integer.parseInt(param.attributeValue("r_ring"));
					final int l_ring = Integer.parseInt(param.attributeValue("l_ring"));

					mDef = new BaseMDef(r_earring, l_earring, necklace, r_ring, l_ring);
				}
				//				else if("damage_range".equalsIgnoreCase(param.getName()))
				//				{
				//					final int vertical = Integer.parseInt(param.attributeValue("vertical"));
				//					final int horizontal = Integer.parseInt(param.attributeValue("horizontal"));
				//					final int distance = Integer.parseInt(param.attributeValue("distance"));
				//					final int width = Integer.parseInt(param.attributeValue("width"));
				//
				//					// TODO: Constructor
				//				}
				else if("stats".equalsIgnoreCase(param.getName()))
				{
					final byte STR = Byte.parseByte(param.attributeValue("STR"));
					final byte CON = Byte.parseByte(param.attributeValue("CON"));
					final byte DEX = Byte.parseByte(param.attributeValue("DEX"));
					final byte INT = Byte.parseByte(param.attributeValue("INT"));
					final byte WIT = Byte.parseByte(param.attributeValue("WIT"));
					final byte MEN = Byte.parseByte(param.attributeValue("MEN"));
					stats = new BaseStats(STR, CON, DEX, INT, WIT, MEN);
				}
				else if("location".equalsIgnoreCase(param.getName()))
				{
					final int x = Integer.parseInt(param.attributeValue("x"));
					final int y = Integer.parseInt(param.attributeValue("y"));
					final int z = Integer.parseInt(param.attributeValue("z"));
					location.set(x, y, z);
				}
				else if("shortcut".equalsIgnoreCase(param.getName()))
				{
					final int slot = Integer.parseInt(param.attributeValue("slot"));
					final int page = ShortcutPage.valueOf(param.attributeValue("page")).getId();
					final int type = ShortcutType.valueOf(param.attributeValue("type")).getId();
					final int _id = Integer.parseInt(param.attributeValue("id"));
					final int _level = Integer.parseInt(param.attributeValue("level", "-1"));
					shortcuts.add(new ShortCut(slot, page, type, _id, _level, 1));
				}
				else if("male".equalsIgnoreCase(param.getName()))
					parseSexSetting(param, sets, items, true);
				else if("female".equalsIgnoreCase(param.getName()))
					parseSexSetting(param, sets, items, false);
			}

			getHolder().addTemplate(id, race, char_type, stats, pDef, mDef, sets, items, location, shortcuts);
		}
	}

	private void parseSexSetting(Element param, final StatsSet sets, final Map<Integer, List<StartItem>> items, final boolean male)
	{
		List<StartItem> list = new ArrayList<StartItem>();
		for(Iterator<Element> it = param.elementIterator(); it.hasNext();)
		{
			Element settings = it.next();

			if("collision".equalsIgnoreCase(settings.getName()))
			{
				final double radius = Double.parseDouble(settings.attributeValue("radius"));
				final double height = Double.parseDouble(settings.attributeValue("height"));
				if(male)
				{
					sets.set("male_radius", radius);
					sets.set("male_height", height);
				}
				else
				{
					sets.set("female_radius", radius);
					sets.set("female_height", height);
				}
			}
			else if("item".equalsIgnoreCase(settings.getName()))
			{
				final int id = Integer.parseInt(settings.attributeValue("id"));
				final long count = Long.parseLong(settings.attributeValue("count"));
				final boolean equip = Boolean.parseBoolean((settings.attributeValue("equip", "false")));
				final StartItem item = new StartItem(id, count, equip);
				list.add(item);
			}
			else if("falling".equalsIgnoreCase(settings.getName()))
			{
				final int height = Integer.parseInt(settings.attributeValue("height"));
				if(male)
					sets.set("male_fall", height);
				else
					sets.set("female_fall", height);
			}
		}

		items.put(male ? 0 : 1, list);
	}
}