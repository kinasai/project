package com.l2cccp.gameserver.data.xml.holder;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.entity.rifts.DimensionalRiftRoom;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RiftsHolder extends AbstractHolder
{
	private static final RiftsHolder _instance = new RiftsHolder();

	private Map<Integer, Map<Integer, DimensionalRiftRoom>> _rooms = new ConcurrentHashMap<Integer, Map<Integer, DimensionalRiftRoom>>();

	public static RiftsHolder getInstance()
	{
		return _instance;
	}

	public void put(final int id)
	{
		_rooms.put(id, new ConcurrentHashMap<Integer, DimensionalRiftRoom>());
	}

	public boolean contains(final int id)
	{
		return _rooms.containsKey(id);
	}

	public DimensionalRiftRoom getRoom(final int type, final int room)
	{
		return _rooms.get(type).get(room);
	}

	public Map<Integer, DimensionalRiftRoom> getRooms(final int type)
	{
		return _rooms.get(type);
	}

	@Override
	public int size()
	{
		return _rooms.size();
	}

	@Override
	public void clear()
	{
		_rooms.clear();
	}
}