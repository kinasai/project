package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.balancing.NpcBalance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class NpcBalancerHolder extends AbstractHolder
{
	private static final NpcBalancerHolder _instance = new NpcBalancerHolder();

	public static NpcBalancerHolder getInstance()
	{
		return _instance;
	}

	private List<NpcBalance> _npcs = new ArrayList<NpcBalance>();

	public void addBalancer(NpcBalance npc)
	{
		_npcs.add(npc);
	}

	public List<NpcBalance> getAllBalancers()
	{
		return _npcs;
	}

	public NpcBalance getBalancer(int id)
	{
		for(NpcBalance npc : _npcs)
		{
			if(npc.getId() == id)
				return npc;
		}

		return null;
	}

	@Override
	public int size()
	{
		return _npcs.size();
	}

	@Override
	public void clear()
	{
		_npcs.clear();
	}

	// Configs
	private boolean enable;

	public void setConfig(boolean enable)
	{
		this.enable = enable;
	}

	public boolean isEnable()
	{
		return enable;
	}
}