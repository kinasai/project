package com.l2cccp.gameserver.data.reparse;

import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.templates.client.ItemNameLine;
import com.l2cccp.gameserver.utils.Language;

public class ItemNameReparse
{
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

	public static void reparse(Collection<ItemNameLine> collection, Language lang)
	{
		Document document = DocumentHelper.createDocument();
		document.addDocType("items", null, "item_name.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");

		Element element = document.addElement("items");
		element.addAttribute("lang", lang.name());

		for(ItemNameLine item : collection)
		{
			Element config = element.addElement("item");
			config.addAttribute("id", String.valueOf(item.getItemId()));
			config.addAttribute("simple", String.valueOf(item.getName()));
			if(item.getAugmentName() != null && !item.getAugmentName().isEmpty())
				config.addAttribute("augment", String.valueOf(item.getAugmentName()));
		}

		try
		{
			String path = Config.DATAPACK_ROOT + "/data/reparser/client/item_name-" + lang.getShortName() + ".xml";

			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
			prettyPrint.setIndent("\t");
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
			xmlWriter.write(document);
			xmlWriter.close();
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}
}