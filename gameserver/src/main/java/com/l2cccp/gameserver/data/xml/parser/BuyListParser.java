package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.BuyListHolder;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.model.items.TradeItem;
import com.l2cccp.gameserver.model.items.TradeList;
import com.l2cccp.gameserver.templates.item.ItemTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class BuyListParser extends AbstractDirParser<BuyListHolder>
{
	private static final BuyListParser _instance = new BuyListParser();

	public static BuyListParser getInstance()
	{
		return _instance;
	}

	private BuyListParser()
	{
		super(BuyListHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/shops/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "shops.dtd";
	}

	@Override
	protected void readData(Element element) throws Exception
	{
		for(Iterator<Element> iterator = element.elementIterator("shop"); iterator.hasNext();)
		{
			Element shop = iterator.next();
			final int npc = Integer.parseInt(shop.attributeValue("npc"));
			final int id = Integer.parseInt(shop.attributeValue("id"));
			final double mark = Double.parseDouble(shop.attributeValue("markup", "0"));
			final double markup = npc > 0 ? 1. + mark / 100. : 0.;
			final TradeList trade = new TradeList(id);
			trade.setNpcId(npc);
			parseItems(shop, trade, markup);

			getHolder().addToBuyList(id, trade);
		}

	}

	private void parseItems(Element shop, TradeList trade, double markup)
	{
		for(Iterator<Element> it = shop.elementIterator(); it.hasNext();)
		{
			Element items = it.next();

			if("item".equalsIgnoreCase(items.getName()))
			{
				final int id = Integer.parseInt(items.attributeValue("id"));
				final ItemTemplate template = ItemHolder.getInstance().getTemplate(id);
				if(template == null)
				{
					_log.warn("BuyListParser: Template not found for itemId: " + id + " for shop " + trade.getListId());
					continue;
				}

				if(!checkItem(template))
					continue;

				final TradeItem item = new TradeItem();
				item.setItemId(id);

				final long reference = Math.round(template.getReferencePrice() * markup);
				final long price = Long.parseLong(items.attributeValue("price", String.valueOf(reference)));
				final int count = Integer.parseInt(items.attributeValue("count", "0"));
				final int time = Integer.parseInt(items.attributeValue("time", "0"));

				item.setOwnersPrice(price);
				item.setCount(count);
				item.setCurrentValue(count);
				item.setLastRechargeTime((int) (System.currentTimeMillis() / 60000));
				item.setRechargeTime(time);
				trade.addItem(item);
			}
		}
	}

	public boolean checkItem(ItemTemplate template)
	{
		if(template.isCommonItem() && !Config.ALT_ALLOW_SELL_COMMON)
			return false;

		if(Config.ALT_SHOP_FORBIDDEN_ITEMS.length > 0)
		{
			for(int i : Config.ALT_SHOP_FORBIDDEN_ITEMS)
			{
				if(template.getItemId() == i)
					return false;
			}
		}

		return true;
	}
}