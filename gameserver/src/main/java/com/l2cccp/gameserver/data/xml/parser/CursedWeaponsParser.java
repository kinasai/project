package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.CursedWeaponsHolder;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.model.CursedWeapon;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CursedWeaponsParser extends AbstractFileParser<CursedWeaponsHolder>
{
	private static final CursedWeaponsParser _instance = new CursedWeaponsParser();

	public static CursedWeaponsParser getInstance()
	{
		return _instance;
	}

	private CursedWeaponsParser()
	{
		super(CursedWeaponsHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/cursed_weapons/cursed_weapons.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "cursed_weapons.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("weapon"); iterator.hasNext();)
		{
			Element weapon = iterator.next();
			final int id = Integer.parseInt(weapon.attributeValue("id"));
			final int skill = Integer.parseInt(weapon.attributeValue("skill"));
			final String name = weapon.attributeValue("name", ItemHolder.getInstance().getTemplate(id).getName());

			CursedWeapon cursed = new CursedWeapon(id, skill, name);
			parseParams(weapon, cursed);
			parseTransform(weapon, cursed);
			getHolder().addCursed(cursed);
		}
	}

	private void parseParams(Element weapon, CursedWeapon cursed)
	{
		for(Iterator<Element> it = weapon.elementIterator(); it.hasNext();)
		{
			Element type = it.next();

			if("param".equals(type.getName()))
			{
				final String name = type.attributeValue("name");
				final String val = type.attributeValue("val");
				if("drop".equalsIgnoreCase(name))
					cursed.setDropRate(Double.parseDouble(val));
				else if("duration".equalsIgnoreCase(name))
				{
					final String[] data = val.split(":");
					final int min = Integer.parseInt(data[0]);
					final int max = Integer.parseInt(data[0]);
					cursed.setDurationMin(min);
					cursed.setDurationMax(max);
				}
				else if("lost".equalsIgnoreCase(name))
					cursed.setDurationLost(Integer.parseInt(val));
				else if("disapear".equalsIgnoreCase(name))
					cursed.setDisapearChance(Integer.parseInt(val));
				else if("stage".equalsIgnoreCase(name))
					cursed.setStageKills(Integer.parseInt(val));
			}
		}
	}

	private void parseTransform(Element weapon, CursedWeapon cursed)
	{
		Element transform = weapon.element("transformation");

		final int id = Integer.parseInt(transform.attributeValue("id"));
		final int visual = Integer.parseInt(transform.attributeValue("visual"));
		final String name = transform.attributeValue("name");

		cursed.setTransformationId(id);
		cursed.setTransformationTemplateId(visual);
		cursed.setTransformationName(name);
	}
}
