package com.l2cccp.gameserver.data.xml.holder;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.farm.FarmFacility;
import com.l2cccp.gameserver.model.farm.FarmHandler;
import com.l2cccp.gameserver.templates.item.ItemTemplate;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FarmDataHolder extends AbstractHolder
{
	private static FarmDataHolder _instance;
	private final FarmHandler handler;
	private TIntObjectHashMap<FarmFacility> data = new TIntObjectHashMap<FarmFacility>();

	public FarmDataHolder()
	{
		this.handler = new FarmHandler();
	}

	public static FarmDataHolder getInstance()
	{
		if(_instance == null)
			_instance = new FarmDataHolder();

		return _instance;
	}

	public void add(final int id, final FarmFacility farm)
	{
		data.put(id, farm);
		ItemTemplate template = ItemHolder.getInstance().getTemplate(id, true);
		template.setHandler(handler);
	}

	public FarmFacility get(final int id)
	{
		return data.get(id);
	}

	@Override
	public int size()
	{
		return data.size();
	}

	@Override
	public void clear()
	{
		data.clear();
	}
}