package com.l2cccp.gameserver.data.xml.holder;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.Config;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ExperienceHolder extends AbstractHolder
{
	private static final ExperienceHolder _instance = new ExperienceHolder();
	private final long[] experience = new long[100];

	public static ExperienceHolder getInstance()
	{
		return _instance;
	}

	public void init(final int level, final long exp)
	{
		experience[level] = exp;
	}

	public long[] getLevels()
	{
		return experience;
	}

	public int getMaxLevel()
	{
		return Config.ALT_MAX_LEVEL;
	}

	public int getMaxSubLevel()
	{
		return Config.ALT_MAX_SUB_LEVEL;
	}

	public int getPetMaxLevel()
	{
		return 86;
	}

	public int getLevel(final boolean base, final long exp)
	{
		int level = 0;
		for(int i = 0; i < getLevels().length; i++)
		{
			long experience = getLevels()[i];
			if(exp >= experience)
				level = i;
		}
		//		if(level > (base ? getMaxLevel() : getMaxSubLevel()))
		//			_log.warn("Incorrect level calc, for exp [" + level + "] " + exp);

		return Math.min(level, (base ? getMaxLevel() : getMaxSubLevel()));
	}

	public long getExpForLevel(final int lvl)
	{
		if(lvl >= experience.length)
			return 0;
		return experience[lvl];
	}

	public double getExpPercent(final int level, final long exp)
	{
		return (exp - getExpForLevel(level)) / ((getExpForLevel(level + 1) - getExpForLevel(level)) / 100.0D) * 0.01D;
	}

	public double penaltyModifier(long count, double percents)
	{
		return Math.max(1. - count * percents / 100, 0);
	}

	@Override
	public int size()
	{
		return experience.length;
	}

	@Override
	public void clear()
	{}
}