package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.BotReportHolder;
import com.l2cccp.gameserver.model.botreport.ReportTemplate;
import com.l2cccp.gameserver.model.botreport.ReportType;
import com.l2cccp.gameserver.model.botreport.ReportVariant;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BotReportParser extends AbstractFileParser<BotReportHolder>
{
	private static final BotReportParser _instance = new BotReportParser();

	public static BotReportParser getInstance()
	{
		return _instance;
	}

	private BotReportParser()
	{
		super(BotReportHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/report_panel/reports.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "report.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("report"); iterator.hasNext();)
		{
			Element reports = iterator.next();
			int id = Integer.parseInt(reports.attributeValue("id"));
			ReportType type = ReportType.valueOf(reports.attributeValue("type"));
			double chance = Double.parseDouble(reports.attributeValue("chance"));

			ReportTemplate report = new ReportTemplate(id, type, chance);
			parseParams(reports, report);
			getHolder().add(report);
		}
	}

	private void parseParams(Element reports, ReportTemplate report)
	{
		for(Iterator<Element> it = reports.elementIterator(); it.hasNext();)
		{
			Element element = it.next();

			if("message".equalsIgnoreCase(element.getName()))
			{
				final Language lang = Language.findByShortName(element.attributeValue("lang"));
				final String msg = element.attributeValue("msg");
				report.addMsg(lang, msg);
			}
			else if("variant".equalsIgnoreCase(element.getName()))
			{
				final int id = Integer.parseInt(element.attributeValue("id"));
				final String value = element.attributeValue("value");
				final boolean correct = Boolean.valueOf(element.attributeValue("correct", "false"));

				ReportVariant variant = new ReportVariant(id, value, correct);
				report.addVariant(variant);
			}
		}
	}
}