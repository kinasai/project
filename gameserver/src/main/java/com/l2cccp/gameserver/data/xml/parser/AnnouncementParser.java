package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.announcement.Announcement;
import com.l2cccp.gameserver.data.xml.holder.AnnouncementHolder;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class AnnouncementParser extends AbstractFileParser<AnnouncementHolder>
{
	private static final AnnouncementParser _instance = new AnnouncementParser();

	public static AnnouncementParser getInstance()
	{
		return _instance;
	}

	private AnnouncementParser()
	{
		super(AnnouncementHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/announcements/announcements.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "announcements.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("announcement"); iterator.hasNext();)
		{
			Element announcement = iterator.next();
			final int id = Integer.parseInt(announcement.attributeValue("id"));
			final ChatType chat = ChatType.valueOf(announcement.attributeValue("chat"));

			final boolean auto = Boolean.parseBoolean(announcement.attributeValue("auto", "false"));
			final long initial = Long.parseLong(announcement.attributeValue("initial", "0"));
			final long interval = Long.parseLong(announcement.attributeValue("interval", "0"));
			final String[] levels = announcement.attributeValue("levels", "1:85").split(":");
			final int min = Integer.parseInt(levels[0]);
			final int max = Integer.parseInt(levels[1]);
			final int limit = Integer.parseInt(announcement.attributeValue("limit", "-1"));

			Announcement announce = new Announcement(id, chat, auto, initial, interval, limit);
			announce.setLevels(min, max);
			parseMessages(announcement, announce);

			getHolder().addAnnouncement(announce);
		}
	}

	private void parseMessages(Element announcement, Announcement announce)
	{
		for(Iterator<Element> it = announcement.elementIterator(); it.hasNext();)
		{
			Element message = it.next();

			if("message".equalsIgnoreCase(message.getName()))
			{
				final Language lang = Language.valueOf(message.attributeValue("lang"));
				final String text = message.attributeValue("text");
				announce.putMsg(lang, text);

				String author = message.attributeValue("author");
				if(author == null)
					author = new CustomMessage("common.sm").toString(lang);

				announce.putAuthor(lang, author);
			}
		}
	}
}