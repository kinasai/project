package com.l2cccp.gameserver.data.xml;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.client.parser.ItemNameLineParser;
import com.l2cccp.gameserver.data.client.parser.NpcNameLineParser;
import com.l2cccp.gameserver.data.client.parser.NpcStringLineParser;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.ProductHolder;
import com.l2cccp.gameserver.data.xml.parser.AirshipDockParser;
import com.l2cccp.gameserver.data.xml.parser.AnnouncementParser;
import com.l2cccp.gameserver.data.xml.parser.ArmorSetsParser;
import com.l2cccp.gameserver.data.xml.parser.AugmentationDataParser;
import com.l2cccp.gameserver.data.xml.parser.BaseStatsBonusParser;
import com.l2cccp.gameserver.data.xml.parser.BotReportParser;
import com.l2cccp.gameserver.data.xml.parser.BuffSchemeParser;
import com.l2cccp.gameserver.data.xml.parser.BuffsParser;
import com.l2cccp.gameserver.data.xml.parser.BuyListParser;
import com.l2cccp.gameserver.data.xml.parser.CharTemplateParser;
import com.l2cccp.gameserver.data.xml.parser.ClassDataParser;
import com.l2cccp.gameserver.data.xml.parser.CubicParser;
import com.l2cccp.gameserver.data.xml.parser.CursedWeaponsParser;
import com.l2cccp.gameserver.data.xml.parser.DailyTaskParser;
import com.l2cccp.gameserver.data.xml.parser.DomainParser;
import com.l2cccp.gameserver.data.xml.parser.DoorParser;
import com.l2cccp.gameserver.data.xml.parser.DressArmorParser;
import com.l2cccp.gameserver.data.xml.parser.DressCloakParser;
import com.l2cccp.gameserver.data.xml.parser.DressShieldParser;
import com.l2cccp.gameserver.data.xml.parser.DressWeaponParser;
import com.l2cccp.gameserver.data.xml.parser.DropListParser;
import com.l2cccp.gameserver.data.xml.parser.EnchantItemParser;
import com.l2cccp.gameserver.data.xml.parser.EventMapsParser;
import com.l2cccp.gameserver.data.xml.parser.EventParser;
import com.l2cccp.gameserver.data.xml.parser.ExchangeItemParser;
import com.l2cccp.gameserver.data.xml.parser.ExperienceParser;
import com.l2cccp.gameserver.data.xml.parser.FarmDataParser;
import com.l2cccp.gameserver.data.xml.parser.FastBuyParser;
import com.l2cccp.gameserver.data.xml.parser.FishDataParser;
import com.l2cccp.gameserver.data.xml.parser.FortuneBoxParser;
import com.l2cccp.gameserver.data.xml.parser.FoundationParse;
import com.l2cccp.gameserver.data.xml.parser.HennaParser;
import com.l2cccp.gameserver.data.xml.parser.InstantZoneParser;
import com.l2cccp.gameserver.data.xml.parser.IpConfigParser;
import com.l2cccp.gameserver.data.xml.parser.ItemAuctionParser;
import com.l2cccp.gameserver.data.xml.parser.ItemParser;
import com.l2cccp.gameserver.data.xml.parser.LeviathanEventParser;
import com.l2cccp.gameserver.data.xml.parser.LeviathanMapsParser;
import com.l2cccp.gameserver.data.xml.parser.MoveRouteParser;
import com.l2cccp.gameserver.data.xml.parser.MultiSellParser;
import com.l2cccp.gameserver.data.xml.parser.NpcBalancerParser;
import com.l2cccp.gameserver.data.xml.parser.NpcParser;
import com.l2cccp.gameserver.data.xml.parser.OptionDataParser;
import com.l2cccp.gameserver.data.xml.parser.PetitionGroupParser;
import com.l2cccp.gameserver.data.xml.parser.PlayerBalancerParser;
import com.l2cccp.gameserver.data.xml.parser.PremiumParser;
import com.l2cccp.gameserver.data.xml.parser.PromoCodeParser;
import com.l2cccp.gameserver.data.xml.parser.QuestParser;
import com.l2cccp.gameserver.data.xml.parser.RateParser;
import com.l2cccp.gameserver.data.xml.parser.RecipeParser;
import com.l2cccp.gameserver.data.xml.parser.RecommendsParser;
import com.l2cccp.gameserver.data.xml.parser.ReplacementParser;
import com.l2cccp.gameserver.data.xml.parser.ResidenceParser;
import com.l2cccp.gameserver.data.xml.parser.RestartPointParser;
import com.l2cccp.gameserver.data.xml.parser.RiftsParser;
import com.l2cccp.gameserver.data.xml.parser.SeedsParser;
import com.l2cccp.gameserver.data.xml.parser.SkillAcquireParser;
import com.l2cccp.gameserver.data.xml.parser.SoulCrystalParser;
import com.l2cccp.gameserver.data.xml.parser.SpawnParser;
import com.l2cccp.gameserver.data.xml.parser.StaticObjectParser;
import com.l2cccp.gameserver.data.xml.parser.StringParser;
import com.l2cccp.gameserver.data.xml.parser.TeleportPointParser;
import com.l2cccp.gameserver.data.xml.parser.ZoneParser;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.tables.SkillTable;

/**
 * @author VISTALL
 * @date  20:55/30.11.2010
 */
public abstract class Parsers
{
	public static void parseAll()
	{
		// cache
		HtmCache.getInstance().reload();
		StringParser.getInstance().load();

		// Experience
		ExperienceParser.getInstance().load();

		// IP Configs
		if(Config.IPCONFIG_ENABLE)
			IpConfigParser.getInstance().load();

		// client
		ItemNameLineParser.getInstance().load();
		NpcNameLineParser.getInstance().load();
		NpcStringLineParser.getInstance().load();

		// Balancer
		NpcBalancerParser.getInstance().load();
		RecommendsParser.getInstance().load();

		//minigame
		FortuneBoxParser.getInstance().load();

		// PC_parameter
		CharTemplateParser.getInstance().load();
		BaseStatsBonusParser.getInstance().load();
		ClassDataParser.getInstance().load();
		PlayerBalancerParser.getInstance().load();
		RateParser.getInstance().load();

		//
		SkillTable.getInstance().load(); // - SkillParser.getInstance();
		OptionDataParser.getInstance().load();
		ItemParser.getInstance().load();
		CursedWeaponsParser.getInstance().load();

		// Manor
		SeedsParser.getInstance().load();

		// item support
		AugmentationDataParser.getInstance().load();
		HennaParser.getInstance().load();
		EnchantItemParser.getInstance().load();
		SoulCrystalParser.getInstance().load();
		ArmorSetsParser.getInstance().load();
		FishDataParser.getInstance().load();
		if(Config.ALT_ITEM_AUCTION_ENABLED)
			ItemAuctionParser.getInstance().load();

		//
		SkillAcquireParser.getInstance().load();
		if(Config.USE_NEW_DROP)
			DropListParser.getInstance().load();
		NpcParser.getInstance().load();
		MoveRouteParser.getInstance().load();

		DomainParser.getInstance().load();
		RestartPointParser.getInstance().load();

		StaticObjectParser.getInstance().load();
		DoorParser.getInstance().load();
		ZoneParser.getInstance().load();
		SpawnParser.getInstance().load();
		InstantZoneParser.getInstance().load();

		ReflectionManager.getInstance();
		//
		AirshipDockParser.getInstance().load();
		RiftsParser.getInstance().load();
		//
		ResidenceParser.getInstance().load();
		EventMapsParser.getInstance().load();
		EventParser.getInstance().load();

		// support(cubic & agathion)
		CubicParser.getInstance().load();

		//
		BuyListParser.getInstance().load();
		RecipeParser.getInstance().load();
		MultiSellParser.getInstance().load();
		ProductHolder.getInstance();
		QuestParser.getInstance().load();

		// AgathionParser.getInstance();

		// etc
		PetitionGroupParser.getInstance().load();
		ReplacementParser.getInstance().load();

		// DressMe
		DressArmorParser.getInstance().load();
		DressCloakParser.getInstance().load();
		DressShieldParser.getInstance().load();
		DressWeaponParser.getInstance().load();

		// Promo Code
		PromoCodeParser.getInstance().load();

		// Leviathan engine
		LeviathanMapsParser.getInstance().load();
		LeviathanEventParser.getInstance().load();

		PremiumParser.getInstance().load();
		FastBuyParser.getInstance().load();

		//DonationParse.getInstance().load();
		ExchangeItemParser.getInstance().load();
		FoundationParse.getInstance().load();

		//Buffer
		BuffsParser.getInstance().load();
		BuffSchemeParser.getInstance().load();

		// Announcements
		AnnouncementParser.getInstance().load();

		BotReportParser.getInstance().load();
		TeleportPointParser.getInstance().load();
		DailyTaskParser.getInstance().load();

		if(Config.ENABLE_FARM)
			FarmDataParser.getInstance().load();
	}
}