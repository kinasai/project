package com.l2cccp.gameserver.data.xml.holder;

import gnu.trove.iterator.TIntObjectIterator;
import gnu.trove.map.hash.TIntObjectHashMap;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.commons.lang.ArrayUtils;
import com.l2cccp.gameserver.model.reward.RewardCalculator;
import com.l2cccp.gameserver.templates.item.ItemTemplate;

public final class ItemHolder extends AbstractHolder
{
	private static final ItemHolder _instance = new ItemHolder();

	private final TIntObjectHashMap<ItemTemplate> _items = new TIntObjectHashMap<ItemTemplate>();
	private ItemTemplate[] _allTemplates;
	private ItemTemplate[] _droppableTemplates;

	public static ItemHolder getInstance()
	{
		return _instance;
	}

	private ItemHolder()
	{
		//
	}

	public void addItem(ItemTemplate template)
	{
		_items.put(template.getItemId(), template);
	}

	private void buildFastLookupTable()
	{
		int highestId = 0;

		for(int id : _items.keys())
			if(id > highestId)
				highestId = id;

		_allTemplates = new ItemTemplate[highestId + 1];

		for(TIntObjectIterator<ItemTemplate> iterator = _items.iterator(); iterator.hasNext();)
		{
			iterator.advance();
			_allTemplates[iterator.key()] = iterator.value();
		}
	}

	/**
	 * Returns the item corresponding to the item ID
	 * @param id : int designating the item
	 */
	public ItemTemplate getTemplate(int id)
	{
		ItemTemplate item = ArrayUtils.valid(_allTemplates, id);
		if(item == null)
		{
			warn("Not defined item id : " + id + ", or out of range!", new Exception());
			return null;
		}
		return _allTemplates[id];
	}

	/**
	 * Returns the item corresponding to the item ID
	 * @param id : int designating the item
	 */
	public ItemTemplate getTemplate(int id, boolean silance)
	{
		if(!silance)
			return getTemplate(id);
		else
		{
			ItemTemplate item = ArrayUtils.valid(_allTemplates, id);
			if(item == null)
				return null;
			else
				return _allTemplates[id];
		}
	}

	public ItemTemplate[] getAllTemplates()
	{
		return _allTemplates;
	}

	public List<ItemTemplate> getItemsByName(CharSequence name, boolean onlyDroppable)
	{
		ItemTemplate[] toChooseFrom = onlyDroppable ? getDroppableTemplates() : _allTemplates;
		List<ItemTemplate> templates = new ArrayList<ItemTemplate>();
		for(ItemTemplate template : toChooseFrom)
		{
			if(template != null && StringUtils.containsIgnoreCase(template.getName(), name))
				templates.add(template);
		}

		return templates;
	}

	public ItemTemplate[] getDroppableTemplates()
	{
		if(_droppableTemplates == null)
		{
			List<ItemTemplate> templates = RewardCalculator.getDroppableItems();
			_droppableTemplates = templates.toArray(new ItemTemplate[templates.size()]);
		}

		return _droppableTemplates;
	}

	@Override
	protected void process()
	{
		buildFastLookupTable();
	}

	@Override
	public int size()
	{
		return _items.size();
	}

	@Override
	public void clear()
	{
		_items.clear();
	}
}