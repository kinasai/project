package com.l2cccp.gameserver.data.xml.holder;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.recipe.Recipe;

public class RecipeHolder extends AbstractHolder
{
	private static final RecipeHolder _instance = new RecipeHolder();

	private Map<Integer, Recipe> _listByRecipeId = new HashMap<Integer, Recipe>();
	private Map<Integer, Recipe> _listByRecipeItem = new HashMap<Integer, Recipe>();

	public static RecipeHolder getInstance()
	{
		return _instance;
	}

	public void addRecipe(final Recipe recipe)
	{
		_listByRecipeId.put(recipe.getId(), recipe);
		_listByRecipeItem.put(recipe.getRecipeId(), recipe);
	}

	public Collection<Recipe> getRecipes()
	{
		return _listByRecipeId.values();
	}

	public Recipe getRecipeByRecipeId(int listId)
	{
		return _listByRecipeId.get(listId);
	}

	public Recipe getRecipeByRecipeItem(int itemId)
	{
		return _listByRecipeItem.get(itemId);
	}

	@Override
	public int size()
	{
		return _listByRecipeId.size();
	}

	@Override
	public void clear()
	{
		_listByRecipeId.clear();
		_listByRecipeItem.clear();
	}
}