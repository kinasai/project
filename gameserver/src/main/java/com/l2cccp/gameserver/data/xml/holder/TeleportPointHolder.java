package com.l2cccp.gameserver.data.xml.holder;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.bbs.teleport.TeleportPoint;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TeleportPointHolder extends AbstractHolder
{
	private static final TeleportPointHolder _instance = new TeleportPointHolder();
	private Map<Integer, Map<Integer, TeleportPoint>> teleports = new HashMap<Integer, Map<Integer, TeleportPoint>>();

	public static TeleportPointHolder getInstance()
	{
		return _instance;
	}

	public void put(final int subsection, final TeleportPoint teleport)
	{
		if(teleports.containsKey(subsection))
		{
			teleports.get(subsection).put(teleport.getId(), teleport);
			return;
		}

		final Map<Integer, TeleportPoint> map = new HashMap<Integer, TeleportPoint>();
		map.put(teleport.getId(), teleport);
		teleports.put(subsection, map);
	}

	public Map<Integer, TeleportPoint> getTeleports(final int subsection)
	{
		return teleports.get(subsection);
	}

	public TeleportPoint getTeleport(final int subsection, final int id)
	{
		TeleportPoint point = null;
		if(teleports.containsKey(subsection))
			point = teleports.get(subsection).get(id);

		return point;
	}

	@Override
	public int size()
	{
		return teleports.size();
	}

	@Override
	public void clear()
	{
		teleports.clear();
	}
}