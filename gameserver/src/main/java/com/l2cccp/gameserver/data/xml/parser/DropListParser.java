package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.apache.commons.lang3.ArrayUtils;
import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.commons.data.xml.AbstractParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.AugmentationDataHolder;
import com.l2cccp.gameserver.data.xml.holder.DropListHolder;
import com.l2cccp.gameserver.data.xml.holder.SkillAcquireHolder;
import com.l2cccp.gameserver.model.reward.RewardData;
import com.l2cccp.gameserver.model.reward.RewardGroup;
import com.l2cccp.gameserver.model.reward.RewardList;
import com.l2cccp.gameserver.model.reward.RewardType;
import com.l2cccp.gameserver.utils.ItemFunctions;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class DropListParser extends AbstractFileParser<DropListHolder>
{
	private static final DropListParser _instance = new DropListParser();

	public static DropListParser getInstance()
	{
		return _instance;
	}

	private DropListParser()
	{
		super(DropListHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/droplist/droplist.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "droplist.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("data"); iterator.hasNext();)
		{
			Element data = iterator.next();
			int npc = Integer.parseInt(data.attributeValue("npc"));
			parseDrop(npc, data);
		}
	}

	private void parseDrop(int npc, Element data)
	{
		for(Iterator<Element> iterator = data.elementIterator("rewardlist"); iterator.hasNext();)
		{
			Element element = iterator.next();
			final RewardList reward = parseRewardList(this, element, "Npc: " + npc);
			getHolder().add(npc, reward);
		}
	}

	public static RewardList parseRewardList(AbstractParser<?> parser, Element element, String debugString)
	{
		RewardType type = RewardType.valueOf(element.attributeValue("type"));
		boolean autoLoot = element.attributeValue("auto_loot") != null && Boolean.parseBoolean(element.attributeValue("auto_loot"));
		RewardList list = new RewardList(type, autoLoot);

		for(Iterator<Element> nextIterator = element.elementIterator(); nextIterator.hasNext();)
		{
			final Element nextElement = nextIterator.next();
			final String nextName = nextElement.getName();
			boolean notGroupType = type == RewardType.SWEEP || type == RewardType.NOT_RATED_NOT_GROUPED || type == RewardType.EVENT;
			if(nextName.equalsIgnoreCase("group"))
			{
				double enterChance = nextElement.attributeValue("chance") == null ? RewardList.MAX_CHANCE : Double.parseDouble(nextElement.attributeValue("chance")) * 10000;

				RewardGroup group = notGroupType ? null : new RewardGroup(enterChance);
				for(Iterator<Element> rewardIterator = nextElement.elementIterator(); rewardIterator.hasNext();)
				{
					Element rewardElement = rewardIterator.next();
					RewardData data = parseReward(rewardElement, type, debugString);
					if(notGroupType)
						parser.warn("Can't load rewardlist from group: " + debugString + "; type: " + type);
					else
						group.addData(data);
				}

				if(group != null)
					list.add(group);
			}
			else if(nextName.equalsIgnoreCase("reward"))
			{
				if(!notGroupType)
				{
					parser.warn("Reward can't be without group(and not grouped): " + debugString + "; type: " + type);
					continue;
				}

				RewardData data = parseReward(nextElement, type, debugString);
				RewardGroup g = new RewardGroup(RewardList.MAX_CHANCE);
				g.addData(data);
				list.add(g);
			}
		}

		if(type == RewardType.RATED_GROUPED || type == RewardType.NOT_RATED_GROUPED)
			if(!list.validate())
				parser.warn("Problems with rewardlist: " + debugString + "; type: " + type);

		return list;
	}

	private static RewardData parseReward(final org.dom4j.Element rewardElement, final RewardType type, final String npc)
	{
		final int itemId = Integer.parseInt(rewardElement.attributeValue("item_id"));
		final RewardData data = new RewardData(itemId);

		long min = Long.parseLong(rewardElement.attributeValue("min"));
		long max = Long.parseLong(rewardElement.attributeValue("max"));

		if(data.getItem().isCommonItem())
		{
			//		min *= Config.RATE_DROP_COMMON_ITEMS;
			//		max *= Config.RATE_DROP_COMMON_ITEMS;
		}

		double chance = Double.parseDouble(rewardElement.attributeValue("chance"));
		if(type == RewardType.SWEEP)
			chance *= Config.RATE_SPOIL_CHANCE;

		chance = chance > 100 ? 100 : chance < 0 ? 0.0001 : chance;
		chance *= 10000; // Системный вид!

		// Сортируем потолки 
		chance = Math.min(chance, RewardList.MAX_CHANCE);
		chance = Math.max(chance, RewardList.MIN_CHANCE);

		min = Math.max(min, 1);
		max = Math.max(max, 1);
		min = Math.min(min, Long.MAX_VALUE);
		max = Math.min(max, Long.MAX_VALUE);

		data.setChance(chance);
		data.setMinDrop(min);
		data.setMaxDrop(max);

		final boolean arrow = data.getItem().isArrow(); // стрелы не рейтуются
		final boolean equip = Config.NO_RATE_EQUIPMENT && data.getItem().isEquipment(); // отключаемая рейтовка эквипа
		final boolean keymat = Config.NO_RATE_KEY_MATERIAL && data.getItem().isKeyMatherial(); // отключаемая рейтовка ключевых материалов
		final boolean recipe = Config.NO_RATE_RECIPES && data.getItem().isRecipe(); // отключаемая рейтовка рецептов
		final boolean att = Config.NO_RATE_ATTRIBUTE && ItemFunctions.isAttribute(itemId); // отключаемая рейтовка атрибутов
		final boolean lifestone = Config.NO_RATE_LIFE_STONE && AugmentationDataHolder.getInstance().isStone(itemId); // отключаемая рейтовка камней жизни
		final boolean enchant = Config.NO_RATE_ENCHANT_SCROLL && ItemFunctions.isEnchantScroll(itemId); // отключаемая рейтовка свитков заточек
		final boolean forgotten = Config.NO_RATE_FORGOTTEN_SCROLL && SkillAcquireHolder.getInstance().isLearnItem(itemId); // отключаемая рейтовка забытых свитков скилов
		data.setNotRate((arrow || equip || keymat || recipe || att || lifestone || enchant || forgotten), ArrayUtils.contains(Config.NO_RATE_ITEMS, itemId));

		return data;
	}
}