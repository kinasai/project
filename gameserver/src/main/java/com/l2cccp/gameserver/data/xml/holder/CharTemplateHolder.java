package com.l2cccp.gameserver.data.xml.holder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.actor.instances.player.ShortCut;
import com.l2cccp.gameserver.model.base.ClassId;
import com.l2cccp.gameserver.model.base.Race;
import com.l2cccp.gameserver.model.base.Type;
import com.l2cccp.gameserver.templates.PlayerTemplate;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.templates.item.StartItem;
import com.l2cccp.gameserver.templates.player.BaseMDef;
import com.l2cccp.gameserver.templates.player.BasePDef;
import com.l2cccp.gameserver.templates.player.BaseStats;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CharTemplateHolder extends AbstractHolder
{
	private static CharTemplateHolder _instance = new CharTemplateHolder();
	private final Map<Integer, PlayerTemplate> _templates = new HashMap<Integer, PlayerTemplate>();

	public static CharTemplateHolder getInstance()
	{
		return _instance;
	}

	public void addTemplate(final int id, final Race race, Type type, final BaseStats stats, final BasePDef pDef, final BaseMDef mDef, final StatsSet sets, final Map<Integer, List<StartItem>> items, final Location location, final List<ShortCut> shortcuts)
	{
		PlayerTemplate template = null;

		sets.set("classId", id);
		sets.set("raceId", race.ordinal());
		sets.set("baseSTR", stats.getSTR());
		sets.set("baseCON", stats.getCON());
		sets.set("baseDEX", stats.getDEX());
		sets.set("baseINT", stats.getINT());
		sets.set("baseWIT", stats.getWIT());
		sets.set("baseMEN", stats.getMEN());
		sets.set("className", sets.get("name"));

		sets.set("collision_radius", sets.get("male_radius"));
		sets.set("collision_height", sets.get("male_height"));
		sets.set("falling", sets.get("male_fall"));
		template = new PlayerTemplate(id, race, type, stats, pDef, mDef, sets, true, items.get(0), shortcuts);
		template.spawnLoc = location;
		_templates.put(id, template);

		sets.set("collision_radius", sets.get("female_radius"));
		sets.set("collision_height", sets.get("female_height"));
		sets.set("falling", sets.get("female_fall"));
		template = new PlayerTemplate(id, race, type, stats, pDef, mDef, sets, false, items.get(1), shortcuts);
		template.spawnLoc = location;
		_templates.put(id | 0x100, template);
	}

	public PlayerTemplate getTemplate(ClassId classId, boolean female)
	{
		return getTemplate(classId.getId(), female);
	}

	public PlayerTemplate getTemplate(int classId, boolean female)
	{
		int key = classId;
		if(female)
			key |= 0x100;
		return _templates.get(key);
	}

	@Override
	public int size()
	{
		return _templates.size();
	}

	@Override
	public void clear()
	{
		_templates.clear();
	}
}
