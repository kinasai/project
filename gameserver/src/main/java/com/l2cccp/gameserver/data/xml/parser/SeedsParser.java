package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.SeedsHolder;
import com.l2cccp.gameserver.model.manor.Seed;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class SeedsParser extends AbstractDirParser<SeedsHolder>
{
	private static final SeedsParser _instance = new SeedsParser();

	public static SeedsParser getInstance()
	{
		return _instance;
	}

	protected SeedsParser()
	{
		super(SeedsHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/seeds/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "seeds.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("seed"); iterator.hasNext();)
		{
			Element _seed = iterator.next();
			final int seed_id = Integer.parseInt(_seed.attributeValue("id"));
			final int level = Integer.parseInt(_seed.attributeValue("level"));

			int crop_id = 0, mature_id = 0, reward1_id = 0, reward2_id = 0, manor_id = 0;
			boolean is_alternative = false;
			long limit_for_seeds = 0, limit_for_crops = 0;

			for(Iterator<Element> it = _seed.elementIterator(); it.hasNext();)
			{
				Element element = it.next();
				if("set".equalsIgnoreCase(element.getName()))
				{
					String crop = element.attributeValue("crop");
					if(crop != null)
						crop_id = Integer.parseInt(crop);

					String mature = element.attributeValue("mature");
					if(mature != null)
						mature_id = Integer.parseInt(mature);

					String rewards = element.attributeValue("rewards");
					if(rewards != null)
					{
						String[] reward = rewards.split(":");
						if(reward != null)
						{
							reward1_id = Integer.parseInt(reward[0]);
							reward2_id = Integer.parseInt(reward[1]);
						}
					}

					String manor = element.attributeValue("manor");
					if(manor != null)
						manor_id = Integer.parseInt(manor);

					String alternative = element.attributeValue("is_alternative");
					if(alternative != null)
						is_alternative = Boolean.parseBoolean(alternative);

					String seeds = element.attributeValue("limit_for_seeds");
					if(seeds != null)
						limit_for_seeds = Math.round(Integer.parseInt(seeds) * Config.RATE_MANOR);

					String crops = element.attributeValue("limit_for_crops");
					if(crops != null)
						limit_for_crops = Math.round(Integer.parseInt(crops) * Config.RATE_MANOR);
				}
			}

			Seed seed = new Seed(level, crop_id, mature_id);
			seed.setData(seed_id, reward1_id, reward2_id, manor_id, is_alternative, limit_for_seeds, limit_for_crops);

			getHolder().addSeed(seed);
		}
	}
}