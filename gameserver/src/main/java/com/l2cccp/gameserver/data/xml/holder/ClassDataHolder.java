package com.l2cccp.gameserver.data.xml.holder;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.templates.player.ClassData;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ClassDataHolder extends AbstractHolder
{
	private static final ClassDataHolder _instance = new ClassDataHolder();
	private final TIntObjectHashMap<ClassData> _list = new TIntObjectHashMap<ClassData>();

	public static ClassDataHolder getInstance()
	{
		return _instance;
	}

	public void addClassData(ClassData classData)
	{
		_list.put(classData.getClassId(), classData);
	}

	public ClassData getClassData(int classId)
	{
		return _list.get(classId);
	}

	@Override
	public int size()
	{
		return _list.size();
	}

	@Override
	public void clear()
	{
		_list.clear();
	}
}