package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.reward.RewardList;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class DropListHolder extends AbstractHolder
{
	private static final DropListHolder _instance = new DropListHolder();
	TIntObjectHashMap<List<RewardList>> list = new TIntObjectHashMap<List<RewardList>>();

	public static DropListHolder getInstance()
	{
		return _instance;
	}

	public void add(final int npc, final RewardList reward)
	{
		if(!list.contains(npc))
		{
			final List<RewardList> rewards = new ArrayList<RewardList>();
			rewards.add(reward);
			list.put(npc, rewards);
		}
		else
			list.get(npc).add(reward);
	}

	public void init(final NpcTemplate template)
	{
		final int id = template.getNpcId();
		if(list.contains(id))
		{
			final List<RewardList> rewards = list.get(id);
			for(final RewardList reward : rewards)
				template.addRewardList(reward);
		}
		else
			warn("Can't find droplist for npc [" + template.getNpcId() + "] " + template.getName() + " in new drop!");
	}

	@Override
	public int size()
	{
		return list.size();
	}

	@Override
	public void clear()
	{
		list.clear();
	}
}