package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.minigame.FortuneBox;

public final class FortuneBoxHolder extends AbstractHolder
{
	private static final FortuneBoxHolder _instance = new FortuneBoxHolder();

	public static FortuneBoxHolder getInstance()
	{
		return _instance;
	}

	private List<FortuneBox> _box = new ArrayList<FortuneBox>();

	public void addBox(FortuneBox box)
	{
		_box.add(box);
	}

	public List<FortuneBox> getAllBox()
	{
		return _box;
	}

	public FortuneBox getBox(int id)
	{
		for(FortuneBox box : _box)
		{
			if(box.getId() == id)
				return box;
		}

		return null;
	}

	@Override
	public int size()
	{
		return _box.size();
	}

	@Override
	public void clear()
	{
		_box.clear();
	}

	// Configs
	private boolean enable;
	private boolean log;
	private int min;
	private int max;

	public void setConfig(boolean enable, boolean log, int min, int max)
	{
		this.enable = enable;
		this.log = log;
		this.min = min;
		this.max = max;
	}

	public boolean isEnable()
	{
		return enable;
	}

	public boolean doLog()
	{
		return log;
	}

	public int getMin()
	{
		return min;
	}

	public int getMax()
	{
		return max;
	}

	public boolean canPlay(int level)
	{
		return level >= min && level <= max;
	}
}
