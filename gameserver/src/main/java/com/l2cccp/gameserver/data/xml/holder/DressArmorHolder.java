package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.dress.DressArmorData;

public final class DressArmorHolder extends AbstractHolder
{
	private static final DressArmorHolder _instance = new DressArmorHolder();

	public static DressArmorHolder getInstance()
	{
		return _instance;
	}

	private List<DressArmorData> _dress = new ArrayList<DressArmorData>();

	public void addDress(DressArmorData armorset)
	{
		_dress.add(armorset);
	}

	public List<DressArmorData> getAllDress()
	{
		return _dress;
	}

	public DressArmorData getArmor(int id)
	{
		for(DressArmorData dress : _dress)
		{
			if(dress.getId() == id)
				return dress;
		}

		return null;
	}

	@Override
	public int size()
	{
		return _dress.size();
	}

	@Override
	public void clear()
	{
		_dress.clear();
	}
}
