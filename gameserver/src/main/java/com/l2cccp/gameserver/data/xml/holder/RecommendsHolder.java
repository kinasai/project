package com.l2cccp.gameserver.data.xml.holder;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.commons.time.cron.SchedulingPattern;
import com.l2cccp.gameserver.utils.TimeUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RecommendsHolder extends AbstractHolder
{
	private static RecommendsHolder _instance = new RecommendsHolder();
	private final int[][] bonuses = new int[9][10];
	private SchedulingPattern cron;
	private long time;

	public static RecommendsHolder getInstance()
	{
		return _instance;
	}

	public void putBonus(final int key, final int count, final int bonus)
	{
		bonuses[key][count] = bonus;
	}

	public int getBonus(final int key, final int count)
	{
		return bonuses[key][count];
	}

	public void setCron(SchedulingPattern cron)
	{
		this.cron = cron;
		recalc();
	}

	public long getTime()
	{
		return time;
	}

	public void recalc()
	{
		time = cron.next(System.currentTimeMillis());
		info("Recalculate cron time: Next recalculate at " + (TimeUtils.toSimpleFormat(time)));
	}

	@Override
	public int size()
	{
		return bonuses.length * bonuses[0].length;
	}

	@Override
	public void clear()
	{}
}