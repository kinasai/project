package com.l2cccp.gameserver.data.xml.holder;

import gnu.trove.map.hash.TIntObjectHashMap;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.templates.pet.PetTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PetHolder extends AbstractHolder
{
	private static final PetHolder _instance = new PetHolder();
	private final TIntObjectHashMap<PetTemplate> _pets = new TIntObjectHashMap<PetTemplate>();

	public static PetHolder getInstance()
	{
		return _instance;
	}

	public PetTemplate getInfo(final int id, final int level)
	{
		return _pets.get(id * 100 + level);
	}

	public void put(final int index, final PetTemplate template)
	{
		_pets.put(index, template);
	}

	@Override
	public int size()
	{
		return _pets.size() / 87;
	}

	@Override
	public void clear()
	{
		_pets.clear();
	}
}