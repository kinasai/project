package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.BuffsHolder;
import com.l2cccp.gameserver.model.bbs.buffer.Buff;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class BuffsParser extends AbstractFileParser<BuffsHolder>
{
	private static final BuffsParser _instance = new BuffsParser();

	public static BuffsParser getInstance()
	{
		return _instance;
	}

	private BuffsParser()
	{
		super(BuffsHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/buffer/buffs.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "buffs.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("buff"); iterator.hasNext();)
		{
			final Element data = iterator.next();
			final int id = Integer.parseInt(data.attributeValue("id"));
			final int level = Integer.parseInt(data.attributeValue("level"));;
			final Buff buff = new Buff(id, level);
			final List<Integer> list = parseAllowedClass(data.attributeValue("allowedClass"));
			buff.addAllowedList(list);

			getHolder().add(buff);
		}
	}

	private List<Integer> parseAllowedClass(String allowed)
	{
		List<Integer> list = null;

		if(allowed != null)
		{
			list = new ArrayList<Integer>();
			for(String info : allowed.split(","))
			{
				final int _class = Integer.parseInt(info);
				list.add(_class);
			}
		}

		return list;
	}
}