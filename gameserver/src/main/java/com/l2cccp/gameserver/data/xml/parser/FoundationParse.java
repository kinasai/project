package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.FoundationHolder;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class FoundationParse extends AbstractFileParser<FoundationHolder>
{
	private static final FoundationParse _instance = new FoundationParse();

	public static FoundationParse getInstance()
	{
		return _instance;
	}

	private FoundationParse()
	{
		super(FoundationHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/foundation/foundation.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "foundation.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("foundation"); iterator.hasNext();)
		{
			Element foundation = iterator.next();
			int simple = Integer.parseInt(foundation.attributeValue("simple"));
			int found = Integer.parseInt(foundation.attributeValue("found"));

			getHolder().addFoundation(simple, found);
		}
	}
}