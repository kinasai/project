package com.l2cccp.gameserver.data.xml.holder;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.balancing.PlayerBalance;
import com.l2cccp.gameserver.model.balancing.player.BalancerType;
import com.l2cccp.gameserver.model.balancing.player.PlayerLimits;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PlayerBalancerHolder extends AbstractHolder
{
	private static final PlayerBalancerHolder _instance = new PlayerBalancerHolder();
	private Map<Integer, PlayerBalance> _accuracy = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _evasion = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _magic_attack = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _magic_attack_speed = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _magic_critical_attack = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _magic_defence = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _max_cp = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _max_hp = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _max_mp = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _physical_attack = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _physical_attack_speed = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _physical_critical_attack = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _physical_defence = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerBalance> _speed = new HashMap<Integer, PlayerBalance>();
	private Map<Integer, PlayerLimits> _limits = new HashMap<Integer, PlayerLimits>();

	public static PlayerBalancerHolder getInstance()
	{
		return _instance;
	}

	public void addLimits(final int id, final PlayerLimits limits)
	{
		_limits.put(id, limits);
	}

	public void addBalancer(final BalancerType type, final int id, final PlayerBalance balance)
	{
		switch(type)
		{
			case accuracy:
				_accuracy.put(id, balance);
				break;
			case evasion:
				_evasion.put(id, balance);
				break;
			case magic_attack:
				_magic_attack.put(id, balance);
				break;
			case magic_attack_speed:
				_magic_attack_speed.put(id, balance);
				break;
			case magic_critical_attack:
				_magic_critical_attack.put(id, balance);
				break;
			case magic_defence:
				_magic_defence.put(id, balance);
				break;
			case max_cp:
				_max_cp.put(id, balance);
				break;
			case max_hp:
				_max_hp.put(id, balance);
				break;
			case max_mp:
				_max_mp.put(id, balance);
				break;
			case physical_attack:
				_physical_attack.put(id, balance);
				break;
			case physical_attack_speed:
				_physical_attack_speed.put(id, balance);
				break;
			case physical_critical_attack:
				_physical_critical_attack.put(id, balance);
				break;
			case physical_defence:
				_physical_defence.put(id, balance);
				break;
			case speed:
				_speed.put(id, balance);
				break;
		}
	}

	public PlayerLimits getLimits(final int id)
	{
		return _limits.get(id);
	}

	public double getBalancer(final BalancerType type, final int id, final boolean olymp)
	{
		PlayerBalance balance = null;

		switch(type)
		{
			case accuracy:
				balance = _accuracy.get(id);
				break;
			case evasion:
				balance = _evasion.get(id);
				break;
			case magic_attack:
				balance = _magic_attack.get(id);
				break;
			case magic_attack_speed:
				balance = _magic_attack_speed.get(id);
				break;
			case magic_critical_attack:
				balance = _magic_critical_attack.get(id);
				break;
			case magic_defence:
				balance = _magic_defence.get(id);
				break;
			case max_cp:
				balance = _max_cp.get(id);
				break;
			case max_hp:
				balance = _max_hp.get(id);
				break;
			case max_mp:
				balance = _max_mp.get(id);
				break;
			case physical_attack:
				balance = _physical_attack.get(id);
				break;
			case physical_attack_speed:
				balance = _physical_attack_speed.get(id);
				break;
			case physical_critical_attack:
				balance = _physical_critical_attack.get(id);
				break;
			case physical_defence:
				balance = _physical_defence.get(id);
				break;
			case speed:
				balance = _speed.get(id);
				break;
		}

		return balance.getBalancer(olymp);
	}

	@Override
	public int size()
	{
		return _limits.size();
	}

	@Override
	public void clear()
	{
		_accuracy.clear();
		_evasion.clear();
		_magic_attack.clear();
		_magic_attack_speed.clear();
		_magic_critical_attack.clear();
		_magic_defence.clear();
		_max_cp.clear();
		_max_hp.clear();
		_max_mp.clear();
		_physical_attack.clear();
		_physical_attack_speed.clear();
		_physical_critical_attack.clear();
		_physical_defence.clear();
		_speed.clear();
		_limits.clear();
	}
}