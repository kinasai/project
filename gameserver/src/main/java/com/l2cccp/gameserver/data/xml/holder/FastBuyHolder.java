package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.items.FastBuyData;

public final class FastBuyHolder extends AbstractHolder
{
	private static final FastBuyHolder _instance = new FastBuyHolder();

	public static FastBuyHolder getInstance()
	{
		return _instance;
	}

	private List<FastBuyData> _product = new ArrayList<FastBuyData>();

	public void addProduct(FastBuyData shield)
	{
		_product.add(shield);
	}

	public List<FastBuyData> getAllProducts()
	{
		return _product;
	}

	public FastBuyData getProduct(int id)
	{
		for(FastBuyData shield : _product)
		{
			if(shield.getId() == id)
				return shield;
		}

		return null;
	}

	@Override
	public int size()
	{
		return _product.size();
	}

	@Override
	public void clear()
	{
		_product.clear();
	}
}
