package com.l2cccp.gameserver.data.xml.holder;

import java.util.concurrent.atomic.AtomicInteger;

import gnu.trove.map.hash.TIntObjectHashMap;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.dao.ItemAuctionDAO;
import com.l2cccp.gameserver.instancemanager.itemauction.ItemAuctionInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ItemAuctionHolder extends AbstractHolder
{
	private static final ItemAuctionHolder _instance = new ItemAuctionHolder();
	private static TIntObjectHashMap<ItemAuctionInstance> _managers = new TIntObjectHashMap<ItemAuctionInstance>();
	private final AtomicInteger _nextId = ItemAuctionDAO.getInstance().next();

	public static ItemAuctionHolder getInstance()
	{
		return _instance;
	}

	public int getNextId()
	{
		return _nextId.incrementAndGet();
	}

	public void put(final int id, final ItemAuctionInstance manager)
	{
		_managers.put(id, manager);
	}

	public ItemAuctionInstance get(final int id)
	{
		return _managers.get(id);
	}

	@Override
	public int size()
	{
		return _managers.size();
	}

	@Override
	public void clear()
	{
		_managers.clear();
	}
}