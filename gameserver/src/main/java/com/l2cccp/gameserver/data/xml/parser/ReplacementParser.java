package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.ReplacementHolder;
import com.l2cccp.gameserver.model.replacement.Translate;

public final class ReplacementParser extends AbstractFileParser<ReplacementHolder>
{
	private static final ReplacementParser _instance = new ReplacementParser();

	public static ReplacementParser getInstance()
	{
		return _instance;
	}

	private ReplacementParser()
	{
		super(ReplacementHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/replacement/replacement.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "replacement.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("replacement"); iterator.hasNext();)
		{
			Element replacement = iterator.next();
			final String key = replacement.attributeValue("key");
			parseReplacement(replacement, key);
		}
	}

	private void parseReplacement(Element replacement, String key)
	{
		for(Iterator<Element> it = replacement.elementIterator(); it.hasNext();)
		{
			Element type = it.next();

			final String back = type.attributeValue("key");
			if("replace".equalsIgnoreCase(type.getName()))
				getHolder().addTranslate(new Translate(key, back));
			else if("back".equalsIgnoreCase(type.getName()))
				getHolder().addTransback(new Translate(key, back));
			else if("transcode".equalsIgnoreCase(type.getName()))
				getHolder().addTranscode(new Translate(key, back));
		}
	}
}