package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.templates.augmentation.AugmentationInfo;

/**
 * @author VISTALL
 * @date 15:10/14.03.2012
 */
public class AugmentationDataHolder extends AbstractHolder
{
	private static AugmentationDataHolder _instance = new AugmentationDataHolder();
	private Set<AugmentationInfo> _augmentationInfos = new HashSet<AugmentationInfo>();
	private List<Integer> _lifestone = new ArrayList<Integer>();

	public static AugmentationDataHolder getInstance()
	{
		return _instance;
	}

	private AugmentationDataHolder()
	{}

	@Override
	public int size()
	{
		return _augmentationInfos.size();
	}

	@Override
	public void clear()
	{
		_augmentationInfos.clear();
	}

	public void addAugmentationInfo(AugmentationInfo augmentationInfo)
	{
		_augmentationInfos.add(augmentationInfo);
	}

	public void addStone(int item)
	{
		_lifestone.add(item);
	}

	public boolean isStone(int item)
	{
		for(int id : _lifestone)
		{
			if(id == item)
				return true;
		}

		return false;
	}
}