package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.FarmDataHolder;
import com.l2cccp.gameserver.model.farm.FarmFacility;
import com.l2cccp.gameserver.model.farm.FarmType;
import com.l2cccp.gameserver.model.farm.FarmZone;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FarmDataParser extends AbstractDirParser<FarmDataHolder>
{
	private static final FarmDataParser _instance = new FarmDataParser();

	public static FarmDataParser getInstance()
	{
		return _instance;
	}

	protected FarmDataParser()
	{
		super(FarmDataHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/farm/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "farm.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("farm"); iterator.hasNext();)
		{
			Element farm = iterator.next();
			final String name = farm.attributeValue("name");
			final FarmType type = FarmType.valueOf(farm.attributeValue("type"));
			final int npc = Integer.parseInt(farm.attributeValue("object"));
			final int lifetime = Integer.parseInt(farm.attributeValue("lifetime"));
			final int handler = Integer.parseInt(farm.attributeValue("handler"));

			final FarmFacility object = new FarmFacility(name, "icon." + handler, type, npc, lifetime);
			object.setFertilize(farm.attributeValue("fertilize").split(":"));
			object.setReward(farm.attributeValue("reward").split(":"));

			getHolder().add(handler, object);
		}

		FarmZone.getInstance().init();
	}
}
