package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.CursedWeapon;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CursedWeaponsHolder extends AbstractHolder
{
	private static final CursedWeaponsHolder _instance = new CursedWeaponsHolder();
	private static List<CursedWeapon> cursed = new ArrayList<CursedWeapon>();

	public static CursedWeaponsHolder getInstance()
	{
		return _instance;
	}

	public void addCursed(CursedWeapon weapon)
	{
		cursed.add(weapon);
	}

	public List<CursedWeapon> getCursed()
	{
		return cursed;
	}

	public CursedWeapon get(final int id)
	{
		for(CursedWeapon weapon : cursed)
		{
			if(weapon.getItemId() == id)
				return weapon;
		}

		return null;
	}

	public boolean isCursed(final int id)
	{
		return get(id) != null;
	}

	@Override
	public int size()
	{
		return cursed.size();
	}

	@Override
	public void clear()
	{
		cursed.clear();
	}
}