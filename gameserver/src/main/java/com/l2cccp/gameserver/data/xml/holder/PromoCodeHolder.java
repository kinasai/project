package com.l2cccp.gameserver.data.xml.holder;

import gnu.trove.map.hash.TObjectIntHashMap;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.dao.PromoCodeDAO;
import com.l2cccp.gameserver.dao.PromoCodeLimitDAO;
import com.l2cccp.gameserver.dao.PromoCodeUserDAO;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.promocode.PromoCode;
import com.l2cccp.gameserver.model.promocode.PromoCodeReward;

/**
 
* @author VISTALL
* @since 24.02.14
*/
public class PromoCodeHolder extends AbstractHolder
{
	public static enum ActivateResult
	{
		ALREADY,
		OUT_OF_DATE,
		OUT_OF_LIMIT,
		NOT_FOR_YOU,
		INVALID_TRY_LATER,
		OK
	}

	private Map<String, PromoCode> _codes = new HashMap<String, PromoCode>();
	private TObjectIntHashMap<String> _limits = PromoCodeLimitDAO.INSTANCE.select();

	private static final PromoCodeHolder _instance = new PromoCodeHolder();

	public static PromoCodeHolder getInstance()
	{
		return _instance;
	}

	public void addPromoCode(String name, PromoCode promoCode)
	{
		_codes.put(name, promoCode);
	}

	public PromoCode getPromoCode(String name)
	{
		return _codes.get(name);
	}

	public ActivateResult tryActivate(Player player, String code)
	{
		final PromoCode promoCode = _codes.get(code);
		if(promoCode == null){ return ActivateResult.INVALID_TRY_LATER; }

		String accountName = player.getAccountName();
		if(PromoCodeDAO.INSTANCE.isActivated(accountName, code)){ return ActivateResult.ALREADY; }

		if(promoCode.getLimit() > 0)
		{
			synchronized (_limits)
			{
				int i = _limits.get(code);
				if(i >= promoCode.getLimit()){ return ActivateResult.OUT_OF_LIMIT; }

				_limits.put(code, ++i);
				PromoCodeLimitDAO.INSTANCE.insert(code, i);
			}
		}

		long l = System.currentTimeMillis();
		if(promoCode.getFromDate() > 0 && l < promoCode.getFromDate()){ return ActivateResult.OUT_OF_DATE; }

		if(promoCode.getToDate() > 0 && l > promoCode.getToDate()){ return ActivateResult.OUT_OF_DATE; }

		if(promoCode.isLimitByUser())
		{
			if(!PromoCodeUserDAO.INSTANCE.isContains(accountName, code)){ return ActivateResult.NOT_FOR_YOU; }
		}

		for(PromoCodeReward promoCodeReward : promoCode.getRewards())
		{
			if(!promoCodeReward.validate()){ return ActivateResult.INVALID_TRY_LATER; }
		}

		for(PromoCodeReward promoCodeReward : promoCode.getRewards())
		{
			promoCodeReward.giveReward(player);
		}

		PromoCodeDAO.INSTANCE.insert(accountName, code);
		return ActivateResult.OK;
	}

	@Override
	public int size()
	{
		return _codes.size();
	}

	@Override
	public void clear()
	{
		_codes.clear();
	}
}
