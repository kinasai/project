package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.donatesystem.Donation;

public final class DonationHolder extends AbstractHolder
{
	private static final DonationHolder _instance = new DonationHolder();

	public static DonationHolder getInstance()
	{
		return _instance;
	}

	private List<Donation> _donate = new ArrayList<Donation>();

	public void addDonate(Donation donate)
	{
		_donate.add(donate);
	}

	public List<Donation> getAllDonates()
	{
		return _donate;
	}

	public Donation getDonate(int id)
	{
		for(Donation donate : _donate)
		{
			if(donate.getId() == id)
				return donate;
		}

		return null;
	}

	public List<Donation> getGroup(int id)
	{
		List<Donation> group = new ArrayList<Donation>();
		for(Donation donate : _donate)
		{
			if(donate.getGroup() == id)
				group.add(donate);
		}

		return group;
	}

	@Override
	public int size()
	{
		return _donate.size();
	}

	@Override
	public void clear()
	{
		_donate.clear();
	}
}
