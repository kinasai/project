package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.model.botreport.ReportTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BotReportHolder extends AbstractHolder
{
	private static final BotReportHolder _instance = new BotReportHolder();
	private final List<ReportTemplate> _reports = new ArrayList<ReportTemplate>();

	public static BotReportHolder getInstance()
	{
		return _instance;
	}

	public ReportTemplate getRandom()
	{
		boolean loop = true;
		ReportTemplate random = null;
		List<ReportTemplate> reports = new ArrayList<ReportTemplate>();
		while(loop)
		{
			for(ReportTemplate report : _reports)
			{
				final double chance = report.getChance();
				if(Rnd.chance(chance))
					reports.add(report);
			}

			final int size = reports.size() - 1;
			if(size >= 0)
			{
				random = reports.get(size == 0 ? 0 : Rnd.get(size));
				loop = false;
			}
		}

		return random;
	}

	public ReportTemplate get(final int id)
	{
		for(ReportTemplate report : _reports)
		{
			if(report.getId() == id)
				return report;
		}

		return null;
	}

	public void add(ReportTemplate report)
	{
		_reports.add(report);
	}

	@Override
	public int size()
	{
		return _reports.size();
	}

	@Override
	public void clear()
	{
		_reports.clear();
	}
}