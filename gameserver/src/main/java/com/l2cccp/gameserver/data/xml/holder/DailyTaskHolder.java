package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.jdom2.AbstractHolder;
import com.l2cccp.gameserver.mod.DailyTasks.DailyTask;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Trigger;

public class DailyTaskHolder extends AbstractHolder {	
	private final List<DailyTask> tasks = new ArrayList<>();
	public void addTask(DailyTask task) {
		tasks.add(task);
    }
	
	public static DailyTaskHolder getInstance() {
        return LazyHolder.INSTANCE;
    }
	
	public DailyTask getTask(final Trigger trig) {
		for(DailyTask task : tasks) {
			if (task.getTrigger() == trig) {
				return task;
			}
		}
		return null;
	}
	
	public List<DailyTask> getTasks() {
		return tasks;
	}
	
	@Override
	public int size() {
		return tasks.size();
	}

	@Override
	public void clear() {
		tasks.clear();
	}
	
	private static class LazyHolder {
        private static final DailyTaskHolder INSTANCE = new DailyTaskHolder();
    }
}