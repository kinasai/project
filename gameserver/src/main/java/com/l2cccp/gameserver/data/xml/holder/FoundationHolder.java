package com.l2cccp.gameserver.data.xml.holder;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class FoundationHolder extends AbstractHolder
{
	private static final FoundationHolder _instance = new FoundationHolder();

	public static FoundationHolder getInstance()
	{
		return _instance;
	}

	private Map<Integer, Integer> _foundation = new HashMap<Integer, Integer>();

	public void addFoundation(int simple, int found)
	{
		_foundation.put(simple, found);
	}

	public int getFoundation(int id)
	{
		if(_foundation.containsKey(id))
			return _foundation.get(id);
		else
			return -1;
	}

	@Override
	public int size()
	{
		return _foundation.size();
	}

	@Override
	public void clear()
	{
		_foundation.clear();
	}
}
