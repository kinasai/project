package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.PremiumHolder;
import com.l2cccp.gameserver.model.premium.PremiumAccount;
import com.l2cccp.gameserver.model.premium.PremiumChat;
import com.l2cccp.gameserver.model.premium.PremiumGift;
import com.l2cccp.gameserver.model.premium.PremiumHero;
import com.l2cccp.gameserver.model.premium.PremiumKeys;
import com.l2cccp.gameserver.model.premium.PremiumVariant;
import com.l2cccp.gameserver.utils.TimeUtils;

public final class PremiumParser extends AbstractFileParser<PremiumHolder>
{
	private static final PremiumParser _instance = new PremiumParser();

	public static PremiumParser getInstance()
	{
		return _instance;
	}

	private PremiumParser()
	{
		super(PremiumHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/premium/premium.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "premium.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("premium"); iterator.hasNext();)
		{
			String name = null, icon = null;
			int id;
			List<PremiumGift> list = new ArrayList<PremiumGift>();

			Element premium = iterator.next();
			id = Integer.parseInt(premium.attributeValue("id"));
			name = premium.attributeValue("name");
			icon = premium.attributeValue("icon");

			Element gifts = premium.element("gifts");
			if(gifts != null)
				parseGifts(gifts, list);

			PremiumAccount pa = new PremiumAccount(id, name, icon, list);
			parseRates(premium, pa);
			parseChat(premium, pa);
			parseStatus(premium, pa);
			parseVariant(premium.element("variants"), pa);

			getHolder().addPremium(pa);
		}
	}

	private void parseVariant(Element variants, final PremiumAccount premium)
	{
		for(Iterator<Element> iterator = variants.elementIterator("variant"); iterator.hasNext();)
		{
			Element variant = iterator.next();
			final int id = Integer.parseInt(variant.attributeValue("id"));
			final PremiumVariant var = new PremiumVariant(id);
			premium.putVariant(var);

			for(Iterator<Element> it = variant.elementIterator(); it.hasNext();)
			{
				Element next = it.next();
				if("time".equals(next.getName()))
					var.setTime(parseTime(next));
				else if("price".equals(next.getName()))
				{
					final int price_id = Integer.parseInt(next.attributeValue("id"));
					final long price_count = Long.parseLong(next.attributeValue("count"));
					var.setPrice(price_id, price_count);
				}
			}
		}
	}

	private long parseTime(final Element times)
	{
		final int days = Integer.parseInt(times.attributeValue("days"));
		final int hours = Integer.parseInt(times.attributeValue("hours"));
		final int minutes = Integer.parseInt(times.attributeValue("minutes"));

		return (TimeUtils.addDay(days) + TimeUtils.addHours(hours) + TimeUtils.addMinutes(minutes));
	}

	private void parseGifts(Element gifts, List<PremiumGift> list)
	{
		for(Iterator<Element> it = gifts.elementIterator(); it.hasNext();)
		{
			Element gift = it.next();

			if("gift".equalsIgnoreCase(gift.getName()))
			{
				int id = Integer.parseInt(gift.attributeValue("id"));
				long count = Long.parseLong(gift.attributeValue("count"));
				boolean removable = Boolean.parseBoolean(gift.attributeValue("removable"));
				PremiumGift _gift = new PremiumGift(id, count, removable);
				list.add(_gift);
			}
		}
	}

	private void parseRates(Element premium, PremiumAccount pa)
	{
		for(Iterator<Element> it = premium.elementIterator(); it.hasNext();)
		{
			Element rate = it.next();

			if("rate".equalsIgnoreCase(rate.getName()))
			{
				String type = rate.attributeValue("type");
				PremiumKeys key = PremiumKeys.find(type);
				if(key != null)
					pa.setRate(key, rate.attributeValue("value"));
				else
					_log.error("Try parse rate by type: " + type, this);
			}
		}
	}

	private void parseChat(Element premium, PremiumAccount pa)
	{
		for(Iterator<Element> it = premium.elementIterator(); it.hasNext();)
		{
			final Element chat = it.next();

			if("chat".equalsIgnoreCase(chat.getName()))
			{
				final String type = chat.attributeValue("type");
				final PremiumChat key = PremiumChat.find(type);
				if(key != null)
					pa.setChat(key, chat.attributeValue("value"));
				else
					_log.error("Try parse chat by type: " + type, this);
			}
		}
	}

	private void parseStatus(Element premium, PremiumAccount pa)
	{
		for(Iterator<Element> it = premium.elementIterator(); it.hasNext();)
		{
			final Element status = it.next();

			if("status".equalsIgnoreCase(status.getName()))
			{
				final String type = status.attributeValue("type");
				if(type.equals("HERO"))
				{
					final PremiumHero key = PremiumHero.find(status.attributeValue("value"));
					if(key != null)
						pa.setHero(key);
					else
						_log.error("Try parse premium hero status by type: " + type, this);
				}
				else if(type.equals("NOBLESSE"))
				{
					final boolean noble = Boolean.parseBoolean(status.attributeValue("value"));
					pa.setNoble(noble);
				}
			}
		}
	}
}