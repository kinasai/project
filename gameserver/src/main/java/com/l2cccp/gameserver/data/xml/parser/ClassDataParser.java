package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.ClassDataHolder;
import com.l2cccp.gameserver.templates.player.ClassData;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ClassDataParser extends AbstractDirParser<ClassDataHolder>
{
	private static final ClassDataParser _instance = new ClassDataParser();

	public static ClassDataParser getInstance()
	{
		return _instance;
	}

	private ClassDataParser()
	{
		super(ClassDataHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/pc_parameter/health/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "health.dtd";
	}

	@Override
	protected void readData(Element element) throws Exception
	{
		for(Iterator<Element> iter = element.elementIterator("class"); iter.hasNext();)
		{
			Element _class = iter.next();
			final int id = Integer.parseInt(_class.attributeValue("id"));
			ClassData template = new ClassData(id);

			for(Iterator<Element> it = _class.elementIterator("data"); it.hasNext();)
			{
				Element el = it.next();
				final int level = Integer.parseInt(el.attributeValue("level"));
				parseParams(el, level, template);
			}

			getHolder().addClassData(template);
		}

	}

	private void parseParams(final Element reports, final int level, final ClassData template)
	{
		for(Iterator<Element> it = reports.elementIterator(); it.hasNext();)
		{
			Element element = it.next();

			if("base".equalsIgnoreCase(element.getName()))
			{
				final double hp = Double.parseDouble(element.attributeValue("hp"));
				final double mp = Double.parseDouble(element.attributeValue("mp"));
				final double cp = Double.parseDouble(element.attributeValue("cp"));

				template.addHealthData(level, hp, mp, cp);
			}
			else if("regeneration".equalsIgnoreCase(element.getName()))
			{
				final double hp = Double.parseDouble(element.attributeValue("hp"));
				final double mp = Double.parseDouble(element.attributeValue("mp"));
				final double cp = Double.parseDouble(element.attributeValue("cp"));

				template.addRegenData(level, hp, mp, cp);
			}
		}
	}
}