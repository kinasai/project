package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.replacement.Translate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ReplacementHolder extends AbstractHolder
{
	private static final ReplacementHolder _instance = new ReplacementHolder();

	public static ReplacementHolder getInstance()
	{
		return _instance;
	}

	private static List<Translate> tr = new ArrayList<Translate>();
	private static List<Translate> trb = new ArrayList<Translate>();
	private static List<Translate> trcode = new ArrayList<Translate>();

	public void addTranslate(Translate translate)
	{
		tr.add(translate);
	}

	public void addTranscode(Translate transcode)
	{
		trb.add(transcode);
	}

	public void addTransback(Translate transback)
	{
		trcode.add(transback);
	}

	public String translate(String text) // Не юзается?
	{
		return translate(text, 0);
	}

	public String translate(String text, int type)
	{
		for(Translate translate : type == 1 ? trb : type == 2 ? trcode : tr)
		{
			text = text.replace(translate.getFrom(), translate.getTo());
		}

		return text;
	}

	@Override
	public int size()
	{
		return tr.size() + trb.size() + trcode.size();
	}

	@Override
	public void clear()
	{
		// Массив не нужно чистить! Переопределение при репарсе!
	}
}