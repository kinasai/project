package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.commons.time.cron.SchedulingPattern;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.RecommendsHolder;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class RecommendsParser extends AbstractFileParser<RecommendsHolder>
{
	private static final RecommendsParser _instance = new RecommendsParser();

	public static RecommendsParser getInstance()
	{
		return _instance;
	}

	private RecommendsParser()
	{
		super(RecommendsHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "config/recommends/recommends.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "recommends.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("setting"); iterator.hasNext();)
		{
			Element setting = iterator.next();
			final String cooldown = setting.attributeValue("cooldown");
			SchedulingPattern time = new SchedulingPattern(cooldown);
			parseBonuses(setting);
			getHolder().setCron(time);
		}
	}

	private void parseBonuses(Element setting)
	{
		for(Iterator<Element> it = setting.elementIterator(); it.hasNext();)
		{
			Element bonus = it.next();

			if("bonus".equalsIgnoreCase(bonus.getName()))
			{
				final int key = Integer.parseInt(bonus.attributeValue("levels").split(":")[0]) / 10;
				for(Iterator<Element> it2 = bonus.elementIterator(); it2.hasNext();)
				{
					Element recommends = it2.next();

					if("recommends".equalsIgnoreCase(recommends.getName()))
					{
						final String[] array = recommends.attributeValue("count").split(":");
						final int min = Integer.parseInt(array[0]);
						final int count = min / 10;
						final int exp = Integer.parseInt(recommends.attributeValue("bonus"));
						getHolder().putBonus(key, count, exp);
					}
				}
			}
		}
	}
}