package com.l2cccp.gameserver.data.xml.holder;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.bbs.buffer.Schemes;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BuffSchemeHolder extends AbstractHolder
{
	private static final BuffSchemeHolder _instance = new BuffSchemeHolder();
	public static Map<Integer, Schemes> _schemes = new HashMap<Integer, Schemes>();

	public static BuffSchemeHolder getInstance()
	{
		return _instance;
	}

	public void put(int id, Schemes buffs)
	{
		_schemes.put(id, buffs);
	}

	public Schemes get(int id)
	{
		if(_schemes.containsKey(id))
			return _schemes.get(id);
		else
			return null;
	}

	public Map<Integer, Schemes> get()
	{
		return _schemes;
	}

	@Override
	public int size()
	{
		return _schemes.size();
	}

	@Override
	public void clear()
	{
		_schemes.clear();
	}
}