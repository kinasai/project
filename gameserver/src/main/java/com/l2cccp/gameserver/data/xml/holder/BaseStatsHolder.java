package com.l2cccp.gameserver.data.xml.holder;

import com.l2cccp.commons.data.xml.AbstractHolder;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BaseStatsHolder extends AbstractHolder
{
	private static final BaseStatsHolder _instance = new BaseStatsHolder();
	private static int size = 0;

	private static final int MAX_STAT_VALUE = 100;
	private final double[] STR = new double[MAX_STAT_VALUE];
	private final double[] INT = new double[MAX_STAT_VALUE];
	private final double[] DEX = new double[MAX_STAT_VALUE];
	private final double[] WIT = new double[MAX_STAT_VALUE];
	private final double[] CON = new double[MAX_STAT_VALUE];
	private final double[] MEN = new double[MAX_STAT_VALUE];

	public static BaseStatsHolder getInstance()
	{
		return _instance;
	}

	public void addAttributeBonus(int level, double STR, double INT, double CON, double MEN, double DEX, double WIT)
	{
		this.STR[level] = STR;
		this.INT[level] = INT;
		this.CON[level] = CON;
		this.MEN[level] = MEN;
		this.DEX[level] = DEX;
		this.WIT[level] = WIT;
		size++;
	}

	public double getSTR(int level)
	{
		return STR[level];
	}

	public double getINT(int level)
	{
		return INT[level];
	}

	public double getCON(int level)
	{
		return CON[level];
	}

	public double getMEN(int level)
	{
		return MEN[level];
	}

	public double getDEX(int level)
	{
		return DEX[level];
	}

	public double getWIT(int level)
	{
		return WIT[level];
	}

	@Override
	public int size()
	{
		return size;
	}

	@Override
	public void clear()
	{
		size = 0;
	}
}