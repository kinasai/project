package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.templates.OptionDataTemplate;
import com.l2cccp.gameserver.templates.augmentation.AugmentationFilter;

import org.napile.pair.primitive.IntObjectPair;
import org.napile.primitive.maps.IntObjectMap;
import org.napile.primitive.maps.impl.HashIntObjectMap;

/**
 * @author VISTALL
 * @date 20:35/19.05.2011
 */
public final class OptionDataHolder extends AbstractHolder
{
	private static final OptionDataHolder _instance = new OptionDataHolder();

	private IntObjectMap<OptionDataTemplate> _templates = new HashIntObjectMap<OptionDataTemplate>();

	public static OptionDataHolder getInstance()
	{
		return _instance;
	}

	public void addTemplate(OptionDataTemplate template)
	{
		_templates.put(template.getId(), template);
	}

	public OptionDataTemplate getTemplate(int id)
	{
		return _templates.get(id);
	}

	public Collection<OptionDataTemplate> getUniqueOptions(AugmentationFilter filter)
	{
		if(filter == AugmentationFilter.NONE)
			return _templates.values();

		final Map<Integer, OptionDataTemplate> options = new HashMap<Integer, OptionDataTemplate>();
		switch(filter)
		{
			case ACTIVE_SKILL:
			{
				for(OptionDataTemplate option : _templates.values())
				{
					if(!option.getTriggerList().isEmpty())
						continue;

					if(option.getSkills().isEmpty() || !option.getSkills().get(0).getTemplate().isActive())
						continue;

					if(!options.containsKey(option.getSkills().get(0).getId()) || options.get(option.getSkills().get(0).getId()).getSkills().get(0).getLevel() < option.getSkills().get(0).getLevel())
						options.put(option.getSkills().get(0).getId(), option);
				}
				break;
			}
			case PASSIVE_SKILL:
			{
				for(OptionDataTemplate option : _templates.values())
				{
					if(!option.getTriggerList().isEmpty())
						continue;

					if(option.getSkills().isEmpty() || !option.getSkills().get(0).getTemplate().isPassive())
						continue;

					if(!options.containsKey(option.getSkills().get(0).getId()) || options.get(option.getSkills().get(0).getId()).getSkills().get(0).getLevel() < option.getSkills().get(0).getLevel())
						options.put(option.getSkills().get(0).getId(), option);
				}
				break;
			}
			case CHANCE_SKILL:
			{
				for(OptionDataTemplate option : _templates.values())
				{
					if(option.getTriggerList().isEmpty())
						continue;

					if(!options.containsKey(option.getTriggerList().get(0).id) || options.get(option.getTriggerList().get(0).id).getTriggerList().get(0).level < option.getTriggerList().get(0).level)
						options.put(option.getTriggerList().get(0).id, option);
				}
				break;
			}
			case STATS:
			{
				for(OptionDataTemplate option : _templates.values())
				{
					switch(option.getId())
					{
						case 16341: // +1 STR
						case 16342: // +1 CON
						case 16343: // +1 INT
						case 16344: // +1 MEN
							options.put(option.getId(), option);
							break;
					}
				}
				break;
			}
			case NONE:
			default:
				break;
		}

		final List<OptionDataTemplate> augs = new ArrayList<OptionDataTemplate>(options.values());
		Collections.sort(augs, new AugmentationComparator());

		return augs;
	}

	protected static class AugmentationComparator implements Comparator<OptionDataTemplate>
	{
		@Override
		public int compare(final OptionDataTemplate left, final OptionDataTemplate right)
		{
			if(left.getSkills().isEmpty() || right.getSkills().isEmpty())
				return 0;

			return Integer.valueOf(left.getSkills().get(0).getId()).compareTo(right.getSkills().get(0).getId());
		}
	}

	@Override
	public int size()
	{
		return _templates.size();
	}

	@Override
	public void clear()
	{
		_templates.clear();
	}

	public OptionDataTemplate getAugment(int augId)
	{
		for(IntObjectPair<OptionDataTemplate> opt : _templates.entrySet())
		{
			OptionDataTemplate template = opt.getValue();
			System.out.print(" Point A ");
			if(template.getSkills().size() > 0)
			{
				System.out.print("- Point B ");
				for(SkillEntry skill : template.getSkills())
				{
					System.out.print("- Point C ");
					if(skill.getId() == augId)
					{
						System.out.println("Get skill by name:" + skill.getName() + "m id: " + skill.getId());
						return template;
					}
				}
				System.out.println("");
			}
		}
		return null;
	}
}
