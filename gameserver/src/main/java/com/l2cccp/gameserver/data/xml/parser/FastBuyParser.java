package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.FastBuyHolder;
import com.l2cccp.gameserver.model.items.FastBuyData;

public final class FastBuyParser extends AbstractFileParser<FastBuyHolder>
{
	private static final FastBuyParser _instance = new FastBuyParser();

	public static FastBuyParser getInstance()
	{
		return _instance;
	}

	private FastBuyParser()
	{
		super(FastBuyHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/fastbuy/fastbuy.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "fastbuy.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("product"); iterator.hasNext();)
		{
			String name = null;
			int id, itemId;
			long itemCount;
			Element product = iterator.next();
			id = Integer.parseInt(product.attributeValue("id"));
			name = product.attributeValue("name");

			Element price = product.element("price");
			itemId = Integer.parseInt(price.attributeValue("id"));
			itemCount = Long.parseLong(price.attributeValue("count"));

			getHolder().addProduct(new FastBuyData(id, name, itemId, itemCount, parse(product)));
		}
	}

	private Map<Integer, Long> parse(Element n)
	{
		Map<Integer, Long> map = new HashMap<Integer, Long>();

		for(Iterator<Element> iterator = n.elementIterator(); iterator.hasNext();)
		{
			Element d = iterator.next();

			if("item".equalsIgnoreCase(d.getName()))
			{
				int id = Integer.parseInt(d.attributeValue("id"));
				long count = Long.parseLong(d.attributeValue("count"));
				map.put(id, count);
			}
		}

		return map;
	}
}