package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.LeviathanEventHolder;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;
import com.l2cccp.gameserver.leviathan.action.CallAction;
import com.l2cccp.gameserver.leviathan.condition.AbstractCondition;
import com.l2cccp.gameserver.leviathan.controller.Controller;
import com.l2cccp.gameserver.leviathan.enums.ActionState;
import com.l2cccp.gameserver.leviathan.enums.Type;
import com.l2cccp.gameserver.leviathan.listeners.AbstractListener;
import com.l2cccp.gameserver.leviathan.listeners.inventory.AbstractEquipListener;
import com.l2cccp.gameserver.leviathan.listeners.zone.AbstractZoneListener;
import com.l2cccp.gameserver.leviathan.template.LeviathanTemplate;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class LeviathanEventParser extends AbstractDirParser<LeviathanEventHolder>
{
	private static final LeviathanEventParser _instance = new LeviathanEventParser();

	private static final String link = "com.l2cccp.gameserver.leviathan.";

	public static LeviathanEventParser getInstance()
	{
		return _instance;
	}

	protected LeviathanEventParser()
	{
		super(LeviathanEventHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/leviathan/events/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "leviathan.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("event"); iterator.hasNext();)
		{
			Element event = iterator.next();
			final int id = Integer.parseInt(event.attributeValue("id"));
			final Type type = Type.valueOf(event.attributeValue("type"));
			final byte min = Byte.parseByte(event.attributeValue("min", "1"));
			final byte max = Byte.parseByte(event.attributeValue("max", "99"));
			final String cron = event.attributeValue("cron");

			final LeviathanTemplate template = new LeviathanTemplate(id, type, cron);
			template.setLevels(min, max);
			parseTemplateData(event, template);

			final AbstractLeviathan leviathan = type.create(template);
			parseEventData(event, leviathan);

			getHolder().addEvent(leviathan);
		}
	}

	private void parseTemplateData(Element element, LeviathanTemplate template) throws Exception
	{
		for(Iterator<Element> it = element.elementIterator(); it.hasNext();)
		{
			Element info = it.next();

			if("names".equalsIgnoreCase(info.getName()))
			{
				for(Iterator<Element> iterator = info.elementIterator(); iterator.hasNext();)
				{
					Element name = iterator.next();

					if("name".equalsIgnoreCase(name.getName()))
					{
						final Language language = Language.valueOf(name.attributeValue("language"));
						final String string = name.attributeValue("string");
						template.addName(language, string);
					}
				}
			}
			else if("conditions".equalsIgnoreCase(info.getName()))
			{
				for(Iterator<Element> iterator = info.elementIterator(); iterator.hasNext();)
				{
					Element condition = iterator.next();

					if("condition".equalsIgnoreCase(condition.getName()))
					{
						final String impl = condition.attributeValue("impl");
						Class<?> aClass = Class.forName(link + impl);
						Constructor<?> constructor = aClass.getConstructor(Element.class);
						template.addCondition((AbstractCondition) constructor.newInstance(condition));
					}
				}
			}
		}
	}

	private void parseEventData(Element element, AbstractLeviathan leviathan) throws Exception
	{
		for(Iterator<Element> it = element.elementIterator(); it.hasNext();)
		{
			Element data = it.next();

			if("controllers".equalsIgnoreCase(data.getName()))
			{
				for(Iterator<Element> iterator = data.elementIterator(); iterator.hasNext();)
				{
					Element controller = iterator.next();

					if("controller".equalsIgnoreCase(controller.getName()))
					{
						final String object = controller.attributeValue("object");
						final String impl = controller.attributeValue("impl");
						Class<?> aClass = Class.forName(link + impl);
						Constructor<?> constructor = aClass.getConstructor(AbstractLeviathan.class, Element.class);
						leviathan.getTemplate().addController(object, (Controller) constructor.newInstance(leviathan, controller));
					}
				}
			}
			else if("listeners".equalsIgnoreCase(data.getName()))
			{
				for(Iterator<Element> iterator = data.elementIterator(); iterator.hasNext();)
				{
					Element listener = iterator.next();

					if("listener".equalsIgnoreCase(listener.getName()))
					{
						final String impl = listener.attributeValue("impl");
						Class<?> aClass = Class.forName(link + impl);
						Constructor<?> constructor = aClass.getConstructor(AbstractLeviathan.class, Element.class);
						leviathan.getTemplate().addListener((AbstractListener) constructor.newInstance(leviathan, listener));
					}
					else if("inventory".equalsIgnoreCase(listener.getName()))
					{
						final String impl = listener.attributeValue("impl");
						Class<?> aClass = Class.forName(link + impl);
						Constructor<?> constructor = aClass.getConstructor(AbstractLeviathan.class, Element.class);
						leviathan.getTemplate().addEquipListener((AbstractEquipListener) constructor.newInstance(leviathan, listener));
					}
					else if("zone".equalsIgnoreCase(listener.getName()))
					{
						final String zone = listener.attributeValue("zone");
						final String impl = listener.attributeValue("impl");
						Class<?> aClass = Class.forName(link + impl);
						Constructor<?> constructor = aClass.getConstructor(AbstractLeviathan.class, Element.class);
						leviathan.getTemplate().addZoneListener(zone, (AbstractZoneListener) constructor.newInstance(leviathan, listener));
					}
				}
			}
			else if("actions".equalsIgnoreCase(data.getName()))
			{
				for(Iterator<Element> iterator = data.elementIterator(); iterator.hasNext();)
				{
					Element action = iterator.next();

					if("action".equalsIgnoreCase(action.getName()))
					{
						final ActionState state = ActionState.valueOf(action.attributeValue("state"));
						final CallAction call = new CallAction(leviathan, state, action);
						leviathan.getTemplate().addAction(state, call);
					}
				}
			}
		}
	}
}
