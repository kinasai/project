package com.l2cccp.gameserver.data.client.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.client.holder.NpcStringLineHolder;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class NpcStringLineParser extends AbstractDirParser<NpcStringLineHolder>
{
	private static final NpcStringLineParser _instance = new NpcStringLineParser();

	public static NpcStringLineParser getInstance()
	{
		return _instance;
	}

	private NpcStringLineParser()
	{
		super(NpcStringLineHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/client");
	}

	@Override
	public String getDTDFileName()
	{
		return "npc_string.dtd";
	}

	@Override
	public boolean isIgnored(File f)
	{
		return !f.getName().startsWith("npc_string-");
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		Language lang = Language.valueOf(rootElement.attributeValue("lang"));
		for(Iterator<Element> iterator = rootElement.elementIterator(); iterator.hasNext();)
		{
			Element dataElement = iterator.next();

			int id = Integer.parseInt(dataElement.attributeValue("id"));
			String value = dataElement.attributeValue("text");

			getHolder().put(lang, id, value);
		}
	}
}