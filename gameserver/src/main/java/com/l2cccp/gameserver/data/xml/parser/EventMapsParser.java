package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.EventMapsHolder;
import com.l2cccp.gameserver.data.xml.holder.ZoneHolder;
import com.l2cccp.gameserver.model.entity.events.maps.EventMaps;
import com.l2cccp.gameserver.templates.ZoneTemplate;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class EventMapsParser extends AbstractDirParser<EventMapsHolder>
{
	private static final EventMapsParser _instance = new EventMapsParser();

	public static EventMapsParser getInstance()
	{
		return _instance;
	}

	protected EventMapsParser()
	{
		super(EventMapsHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/events/maps/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "maps.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("map"); iterator.hasNext();)
		{
			Element eventElement = iterator.next();
			String name = eventElement.attributeValue("name");
			String events = eventElement.attributeValue("events");
			String instances = eventElement.attributeValue("events");

			MultiValueSet<String> set = new MultiValueSet<String>();
			set.set("name", name);
			set.set("events", events);
			set.set("instances", instances);

			for(Iterator<Element> parameterIterator = eventElement.elementIterator("parameter"); parameterIterator.hasNext();)
			{
				Element parameterElement = parameterIterator.next();
				set.set(parameterElement.attributeValue("name"), parameterElement.attributeValue("value"));
			}

			Map<Integer, Location[]> team = null;
			ZoneTemplate territory = null;
			Location[] key = null;

			for(Iterator<Element> objectIterator = eventElement.elementIterator("objects"); objectIterator.hasNext();)
			{
				Element element = objectIterator.next();
				String objects = element.attributeValue("name");

				if(objects.equals("team"))
				{
					if(team == null)
						team = new HashMap<Integer, Location[]>();

					int teamId = Integer.parseInt(element.attributeValue("team", "-1"));
					team.put(teamId, parseLocations(element));
				}
				else if(objects.equals("territory"))
					territory = parseZoneTemplate(element);
				else if(objects.equals("keyPosition"))
					key = parseLocations(element);
			}

			getHolder().addMap(new EventMaps(set, team, territory, key));
		}
	}

	private Location[] parseLocations(Element element)
	{
		List<Location> locs = new ArrayList<Location>();
		for(Iterator<Element> iterator = element.elementIterator(); iterator.hasNext();)
		{
			Element e = iterator.next();
			final String node = e.getName();

			if(node.equalsIgnoreCase("point"))
				locs.add(Location.parse(e));
		}

		Location[] locArray = new Location[locs.size()];

		for(int i = 0; i < locs.size(); i++)
		{
			locArray[i] = locs.get(i);
		}

		return locArray;
	}

	private ZoneTemplate parseZoneTemplate(Element element)
	{
		ZoneTemplate territory = null;
		for(Iterator<Element> iterator = element.elementIterator(); iterator.hasNext();)
		{
			Element objects = iterator.next();
			final String nodeName = objects.getName();

			if(nodeName.equalsIgnoreCase("zone"))
			{
				ZoneTemplate template = ZoneHolder.getInstance().getTemplate(objects.attributeValue("name"));
				territory = template;
			}
		}

		return territory;
	}
}