package com.l2cccp.gameserver.data.xml.holder;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.items.TradeList;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BuyListHolder extends AbstractHolder
{
	private static final BuyListHolder _instance = new BuyListHolder();

	private Map<Integer, TradeList> _lists = new HashMap<Integer, TradeList>();

	public static BuyListHolder getInstance()
	{
		return _instance;
	}

	public TradeList getBuyList(int listId)
	{
		return _lists.get(listId);
	}

	public void addToBuyList(int listId, TradeList list)
	{
		_lists.put(listId, list);
	}

	@Override
	public int size()
	{
		return _lists.size();
	}

	@Override
	public void clear()
	{
		_lists.clear();
	}
}