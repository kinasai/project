package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.NpcBalancerHolder;
import com.l2cccp.gameserver.model.balancing.DropConstructor;
import com.l2cccp.gameserver.model.balancing.NpcBalance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class NpcBalancerParser extends AbstractFileParser<NpcBalancerHolder>
{
	private static final NpcBalancerParser _instance = new NpcBalancerParser();

	public static NpcBalancerParser getInstance()
	{
		return _instance;
	}

	private NpcBalancerParser()
	{
		super(NpcBalancerHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/balancing/balancing.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "balancing.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		Iterator<Element> iter = rootElement.elementIterator("config");

		if(iter.hasNext())
		{
			Element config = iter.next();
			boolean enable = Boolean.parseBoolean(config.attributeValue("enable"));
			getHolder().setConfig(enable);
		}

		if(getHolder().isEnable())
		{
			for(Iterator<Element> iterator = rootElement.elementIterator("npc"); iterator.hasNext();)
			{
				final Element npc = iterator.next();
				final int id = Integer.parseInt(npc.attributeValue("id"));
				NpcBalance blancer = new NpcBalance(id);

				String _drop = npc.attributeValue("drop");
				if(_drop != null && !_drop.isEmpty())
					blancer.setDrop(DropConstructor.getInstance().construct(_drop));

				String _adena = npc.attributeValue("adena");
				if(_adena != null && !_adena.isEmpty())
					blancer.setAdena(Double.parseDouble(_adena));
				else
					blancer.setAdena(1.);

				String _spoil = npc.attributeValue("spoil");
				if(_spoil != null && !_spoil.isEmpty())
					blancer.setSpoil(DropConstructor.getInstance().construct(_spoil));

				getHolder().addBalancer(blancer);
			}
		}
	}
}