package com.l2cccp.gameserver.data.reparse;

import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.items.ItemAttributes;
import com.l2cccp.gameserver.templates.multisell.MultiSellEntry;
import com.l2cccp.gameserver.templates.multisell.MultiSellIngredient;
import com.l2cccp.gameserver.templates.multisell.MultiSellListContainer;
import com.l2cccp.gameserver.utils.Util;

public class MultisellReparse
{
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

	public static void reparse(int id, MultiSellListContainer list)
	{
		Document document = DocumentHelper.createDocument();
		document.addDocType("list", null, "multisell.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");

		Element element = document.addElement("list");
		Element config = element.addElement("config");
		config.addAttribute("showall", String.valueOf(list.isShowAll()));
		if(list.isNoTax())
			config.addAttribute("notax", String.valueOf(list.isNoTax()));
		if(list.isKeepEnchant())
			config.addAttribute("keepenchanted", String.valueOf(list.isKeepEnchant()));
		if(list.isNoKey())
			config.addAttribute("nokey", String.valueOf(list.isNoKey()));
		if(list.isBBSAllowed())
			config.addAttribute("bbsallowed", String.valueOf(list.isBBSAllowed()));
		if(list.isDisabled())
			config.addAttribute("disabled", String.valueOf(list.isDisabled()));

		for(MultiSellEntry entry : list.getEntries())
		{
			Element item = element.addElement("item");
			for(MultiSellIngredient entry_1 : entry.getIngredients())
			{
				Element ingredient = item.addElement("ingredient");
				ingredient.addAttribute("id", String.valueOf(entry_1.getItemId()));
				ingredient.addAttribute("count", String.valueOf(entry_1.getItemCount()));
				if(entry_1.getItemEnchant() > 0)
					ingredient.addAttribute("enchant", String.valueOf(entry_1.getItemEnchant()));
				if(entry_1.getMantainIngredient())
					ingredient.addAttribute("mantainIngredient", String.valueOf(entry_1.getMantainIngredient()));

				ItemAttributes att = entry_1.getItemAttributes();
				if(att != null)
				{
					if(att.getFire() > 0)
						ingredient.addAttribute("fireAttr", String.valueOf(att.getFire()));
					if(att.getWater() > 0)
						ingredient.addAttribute("waterAttr", String.valueOf(att.getWater()));
					if(att.getEarth() > 0)
						ingredient.addAttribute("earthAttr", String.valueOf(att.getEarth()));
					if(att.getWind() > 0)
						ingredient.addAttribute("windAttr", String.valueOf(att.getWind()));
					if(att.getHoly() > 0)
						ingredient.addAttribute("holyAttr", String.valueOf(att.getHoly()));
					if(att.getUnholy() > 0)
						ingredient.addAttribute("unholyAttr", String.valueOf(att.getUnholy()));
				}
				item.addText("\t<!-- " + getName(entry_1.getItemId()) + " -->");
			}

			for(MultiSellIngredient entry_1 : entry.getProduction())
			{
				Element ingredient = item.addElement("production");
				ingredient.addAttribute("id", String.valueOf(entry_1.getItemId()));
				ingredient.addAttribute("count", String.valueOf(entry_1.getItemCount()));
				if(entry_1.getItemEnchant() > 0)
					ingredient.addAttribute("enchant", String.valueOf(entry_1.getItemEnchant()));

				ItemAttributes att = entry_1.getItemAttributes();
				if(att != null)
				{
					if(att.getFire() > 0)
						ingredient.addAttribute("fireAttr", String.valueOf(att.getFire()));
					if(att.getWater() > 0)
						ingredient.addAttribute("waterAttr", String.valueOf(att.getWater()));
					if(att.getEarth() > 0)
						ingredient.addAttribute("earthAttr", String.valueOf(att.getEarth()));
					if(att.getWind() > 0)
						ingredient.addAttribute("windAttr", String.valueOf(att.getWind()));
					if(att.getHoly() > 0)
						ingredient.addAttribute("holyAttr", String.valueOf(att.getHoly()));
					if(att.getUnholy() > 0)
						ingredient.addAttribute("unholyAttr", String.valueOf(att.getUnholy()));
				}
				item.addText("\t<!-- " + getName(entry_1.getItemId()) + " -->");
			}
		}

		try
		{
			String path = Config.DATAPACK_ROOT + "/data/reparser/multisell/" + id + ".xml";
			File file = new File(path);
			file.delete();
			file.createNewFile();

			FileWriter writer = new FileWriter(file, true);
			String test = document.asXML();
			test = test.replace("&lt;", "<").replace("&gt;", ">").replace("><", ">\n<").replace("<c", "\t<c").replace("<it", "\t<it").replace("</it", "\t</it").replace("<ing", "\t\t<ing").replace("<pr", "\t\t<pr");
			writer.write(test);
			writer.close();
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}

	private static String getName(int id)
	{
		try
		{
			return Util.getItemName(id);
		}
		catch(Exception e)
		{
			return "UNKNOWN ITEM";
		}
	}
}
