package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.TeleportPointHolder;
import com.l2cccp.gameserver.model.bbs.teleport.Price;
import com.l2cccp.gameserver.model.bbs.teleport.PriceType;
import com.l2cccp.gameserver.model.bbs.teleport.TeleportPoint;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class TeleportPointParser extends AbstractFileParser<TeleportPointHolder>
{
	private static final TeleportPointParser _instance = new TeleportPointParser();

	public static TeleportPointParser getInstance()
	{
		return _instance;
	}

	private TeleportPointParser()
	{
		super(TeleportPointHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/teleports/teleports.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "teleports.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("point"); iterator.hasNext();)
		{
			Element point = iterator.next();

			final int id = Integer.parseInt(point.attributeValue("id"));
			final int subsection = Integer.parseInt(point.attributeValue("subsection"));

			TeleportPoint teleport = new TeleportPoint(id, subsection);
			parseData(point, teleport);

			getHolder().put(subsection, teleport);
		}
	}

	private void parseData(Element point, TeleportPoint teleport)
	{
		for(Iterator<Element> it = point.elementIterator(); it.hasNext();)
		{
			Element data = it.next();
			if("coordinates".equalsIgnoreCase(data.getName()))
			{
				final int x = Integer.parseInt(data.attributeValue("x"));
				final int y = Integer.parseInt(data.attributeValue("y"));
				final int z = Integer.parseInt(data.attributeValue("z"));
				teleport.initCoords(x, y, z);
			}
			else if("settings".equalsIgnoreCase(data.getName()))
			{
				final String[] levels = data.attributeValue("levels").split(":");
				final int min = Integer.parseInt(levels[0]), max = Integer.parseInt(levels[1]);
				teleport.initLevels(min, max);

				final boolean premium = Boolean.parseBoolean(data.attributeValue("premium"));
				final boolean pk = Boolean.parseBoolean(data.attributeValue("pk"));
				final boolean noblesse = Boolean.parseBoolean(data.attributeValue("noblesse"));
				teleport.initStates(premium, pk, noblesse);
			}
			else if("costs".equalsIgnoreCase(data.getName()))
				parseCosts(data, teleport);
			else if("names".equalsIgnoreCase(data.getName()))
				parseNames(data, teleport);
		}
	}

	private void parseCosts(Element costs, TeleportPoint teleport)
	{
		for(Iterator<Element> it = costs.elementIterator(); it.hasNext();)
		{
			Element data = it.next();
			final PriceType type = PriceType.valueOf(data.getName().toUpperCase());
			final int id = Integer.parseInt(data.attributeValue("item"));
			final int count = Integer.parseInt(data.attributeValue("count"));
			teleport.initCost(type, new Price(id, count));
		}
	}

	private void parseNames(Element names, TeleportPoint teleport)
	{
		for(Iterator<Element> it = names.elementIterator(); it.hasNext();)
		{
			Element data = it.next();
			final Language type = Language.valueOf(data.attributeValue("lang"));
			final String string = data.attributeValue("string");
			teleport.initName(type, string);
		}
	}
}