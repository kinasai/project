package com.l2cccp.gameserver.data.xml.holder;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.exchange.Change;

public final class ExchangeItemHolder extends AbstractHolder
{
	private static final ExchangeItemHolder _instance = new ExchangeItemHolder();

	public static ExchangeItemHolder getInstance()
	{
		return _instance;
	}

	private Map<Integer, Change> _changes = new HashMap<Integer, Change>();

	public void addChanges(Change armorset)
	{
		_changes.put(armorset.getId(), armorset);
	}

	public Change getChanges(int id)
	{
		return _changes.get(id);
	}

	@Override
	public int size()
	{
		return _changes.size();
	}

	@Override
	public void clear()
	{
		_changes.clear();
	}
}
