package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.network.authcomm.channel.AbstractServerChannel;

public class IpConfigHolder extends AbstractHolder
{
	public static final IpConfigHolder _instance = new IpConfigHolder();

	private List<AbstractServerChannel> _channels = new ArrayList<AbstractServerChannel>();

	public static IpConfigHolder getInstance()
	{
		return _instance;
	}

	@Override
	public int size()
	{
		return _channels.size();
	}

	@Override
	public void clear()
	{
		_channels.clear();
	}

	public List<AbstractServerChannel> getChannels()
	{
		return _channels;
	}

	public void addChannel(AbstractServerChannel abstractServerChannel)
	{
		_channels.add(abstractServerChannel);
	}
}
