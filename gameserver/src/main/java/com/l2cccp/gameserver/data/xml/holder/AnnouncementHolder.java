package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.announcement.Announcement;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class AnnouncementHolder extends AbstractHolder
{
	private static final AnnouncementHolder _instance = new AnnouncementHolder();
	private static List<Announcement> announcements = new ArrayList<Announcement>();
	private static List<Announcement> auto = new ArrayList<Announcement>();

	public static AnnouncementHolder getInstance()
	{
		return _instance;
	}

	public void addAnnouncement(Announcement announcement)
	{
		if(announcement.isAuto())
		{
			auto.add(announcement);
			announcement.startTask();
		}
		else
			announcements.add(announcement);
	}

	public void show(Player player)
	{
		for(Announcement announce : announcements)
		{
			final int level = player.getLevel();
			if(level >= announce.getMinLevel() && level <= announce.getMaxLevel())
			{
				final Language lang = player.getLanguage();
				final Say2 packet = new Say2(0, announce.getChatType(), announce.getAuthor(lang), announce.getMsg(lang), null);
				player.sendPacket(packet);
			}
		}
	}

	public void stop(int id)
	{
		for(Announcement announce : auto)
		{
			if(announce.getId() == id)
			{
				announce.stopTask();
				break;
			}
		}
	}

	@Override
	public int size()
	{
		return announcements.size() + auto.size();
	}

	@Override
	public void clear()
	{
		for(Announcement announce : auto)
			announce.stopTask();

		announcements.clear();
		auto.clear();
	}
}