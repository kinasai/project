package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.LeviathanMapsHolder;
import com.l2cccp.gameserver.leviathan.component.maps.EventMap;
import com.l2cccp.gameserver.leviathan.component.maps.Point;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class LeviathanMapsParser extends AbstractDirParser<LeviathanMapsHolder>
{
	private static final LeviathanMapsParser _instance = new LeviathanMapsParser();

	public static LeviathanMapsParser getInstance()
	{
		return _instance;
	}

	protected LeviathanMapsParser()
	{
		super(LeviathanMapsHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/leviathan/maps/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "maps.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("map"); iterator.hasNext();)
		{
			Element element = iterator.next();
			final int event = Integer.parseInt(element.attributeValue("event"));
			final int min = Integer.parseInt(element.attributeValue("min"));
			final int max = Integer.parseInt(element.attributeValue("max"));
			final int instances = Integer.parseInt(element.attributeValue("instances"));
			final EventMap map = new EventMap(min, max, instances);
			parseNames(element, map);
			parseData(element, map);

			getHolder().addMap(event, map);
		}
	}

	private void parseNames(Element element, EventMap map) throws Exception
	{
		for(Iterator<Element> it = element.elementIterator(); it.hasNext();)
		{
			Element names = it.next();

			if("names".equalsIgnoreCase(names.getName()))
			{
				for(Iterator<Element> it2 = names.elementIterator(); it2.hasNext();)
				{
					Element name = it2.next();

					if("name".equalsIgnoreCase(name.getName()))
					{
						final Language language = Language.valueOf(name.attributeValue("language"));
						final String string = name.attributeValue("string");
						map.addName(language, string);
					}
				}
			}
		}
	}

	private void parseData(Element element, EventMap map) throws Exception
	{
		for(Iterator<Element> it = element.elementIterator(); it.hasNext();)
		{
			Element data = it.next();

			if("points".equalsIgnoreCase(data.getName()))
			{
				final int team = Integer.parseInt(data.attributeValue("team"));
				for(Iterator<Element> it2 = data.elementIterator(); it2.hasNext();)
				{
					Element point = it2.next();

					if("point".equalsIgnoreCase(point.getName()))
					{
						final int x = Integer.parseInt(point.attributeValue("x"));
						final int y = Integer.parseInt(point.attributeValue("y"));
						final int z = Integer.parseInt(point.attributeValue("z"));
						final Point cords = new Point(team, x, y, z);
						map.addPoint(team, cords);
					}
				}
			}
			else if("zones".equalsIgnoreCase(data.getName()))
			{
				for(Iterator<Element> it2 = data.elementIterator(); it2.hasNext();)
				{
					Element point = it2.next();

					if("zone".equalsIgnoreCase(point.getName()))
					{
						final String zone = point.attributeValue("name");
						map.addZone(zone);
					}
				}
			}
		}
	}
}
