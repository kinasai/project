package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.commons.time.cron.SchedulingPattern;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.ItemAuctionHolder;
import com.l2cccp.gameserver.instancemanager.itemauction.AuctionItem;
import com.l2cccp.gameserver.instancemanager.itemauction.ItemAuctionInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class ItemAuctionParser extends AbstractDirParser<ItemAuctionHolder>
{
	private static final ItemAuctionParser _instance = new ItemAuctionParser();

	public static ItemAuctionParser getInstance()
	{
		return _instance;
	}

	private ItemAuctionParser()
	{
		super(ItemAuctionHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/auctions/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "auctions.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("auction"); iterator.hasNext();)
		{
			Element auction = iterator.next();
			final int npc = Integer.parseInt(auction.attributeValue("npc"));
			final String schedule = auction.attributeValue("schedule");
			final SchedulingPattern date = new SchedulingPattern(schedule);

			List<AuctionItem> items = new ArrayList<AuctionItem>();
			parseItems(auction, items);

			getHolder().put(npc, new ItemAuctionInstance(npc, date, items));
		}
	}

	private void parseItems(final Element auction, final List<AuctionItem> items)
	{
		for(Iterator<Element> it = auction.elementIterator(); it.hasNext();)
		{
			Element item = it.next();

			if("item".equalsIgnoreCase(item.getName()))
			{
				final int id = Integer.parseInt(item.attributeValue("id"));
				final String[] data = item.attributeValue("item").split(":");
				parseSets(item, items, id, data);
			}
		}
	}

	private void parseSets(final Element item, final List<AuctionItem> items, final int id, final String[] data)
	{
		final int _id = Integer.parseInt(data[0]);
		final int _count = Integer.parseInt(data[1]);
		int bid = 100000;
		int time = 300;
		int enchant = 0;

		for(Iterator<Element> it = item.elementIterator(); it.hasNext();)
		{
			Element set = it.next();

			if("set".equalsIgnoreCase(set.getName()))
			{
				final String name = set.attributeValue("name");
				final int value = Integer.parseInt(set.attributeValue("value"));
				if(name.equals("bid"))
					bid = value;
				else if(name.equals("time"))
					time = value;
				else if(name.equals("enchant"))
					enchant = value;
			}
		}

		items.add(new AuctionItem(id, time, bid, _id, _count, enchant));
	}
}