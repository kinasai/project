package com.l2cccp.gameserver.data.client.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.client.holder.ItemNameLineHolder;
import com.l2cccp.gameserver.templates.client.ItemNameLine;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ItemNameLineParser extends AbstractDirParser<ItemNameLineHolder>
{
	private static final ItemNameLineParser _instance = new ItemNameLineParser();

	public static ItemNameLineParser getInstance()
	{
		return _instance;
	}

	private ItemNameLineParser()
	{
		super(ItemNameLineHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/client");
	}

	@Override
	public String getDTDFileName()
	{
		return "item_name.dtd";
	}

	@Override
	public boolean isIgnored(File f)
	{
		return !f.getName().startsWith("item_name-");
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		Language lang = Language.valueOf(rootElement.attributeValue("lang"));
		for(Iterator<Element> iterator = rootElement.elementIterator(); iterator.hasNext();)
		{
			Element dataElement = iterator.next();

			int itemId = Integer.parseInt(dataElement.attributeValue("id"));
			String name = dataElement.attributeValue("simple");
			String augment = "";
			if(dataElement.attributeValue("augment") != null)
				augment = dataElement.attributeValue("augment");

			ItemNameLine line = new ItemNameLine(lang, itemId, name, augment);
			getHolder().put(lang, line);
		}
	}
}