package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.DonationHolder;
import com.l2cccp.gameserver.model.donatesystem.Attribution;
import com.l2cccp.gameserver.model.donatesystem.DonateItem;
import com.l2cccp.gameserver.model.donatesystem.Donation;
import com.l2cccp.gameserver.model.donatesystem.Enchant;
import com.l2cccp.gameserver.model.donatesystem.FoundList;
import com.l2cccp.gameserver.model.donatesystem.SimpleList;

public final class DonationParse extends AbstractFileParser<DonationHolder>
{
	private static final DonationParse _instance = new DonationParse();

	public static DonationParse getInstance()
	{
		return _instance;
	}

	private DonationParse()
	{
		super(DonationHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/donation/donation.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "donation.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("donation"); iterator.hasNext();)
		{
			Element donation = iterator.next();
			int id = Integer.parseInt(donation.attributeValue("id"));
			String name = donation.attributeValue("name");
			String icon = donation.attributeValue("icon");
			int group = Integer.parseInt(donation.attributeValue("group"));
			boolean found = Boolean.parseBoolean(donation.attributeValue("found"));
			Donation donate = new Donation(id, name, icon, group, found);

			Element simple = donation.element("simples");
			int s_id = Integer.parseInt(simple.attributeValue("cost_id"));
			long s_count = Long.parseLong(simple.attributeValue("cost_count"));
			SimpleList s_list = new SimpleList(s_id, s_count, simple_parse(simple));
			donate.addSimple(s_list);

			Element foundation = donation.element("foundations");
			if(found)
			{
				if(foundation != null)
				{
					int f_id = Integer.parseInt(foundation.attributeValue("cost_id"));
					long f_count = Long.parseLong(foundation.attributeValue("cost_count"));
					FoundList f_list = new FoundList(f_id, f_count, donate_parse(foundation));
					donate.addFound(f_list);
				}
				else
					_log.error("Problem with donate " + name + ", found is on but is null!");
			}

			Element enchant = donation.element("enchant");
			if(enchant != null)
			{
				int e_id = Integer.parseInt(enchant.attributeValue("cost_id"));
				long e_count = Long.parseLong(enchant.attributeValue("cost_count"));
				int e_value = Integer.parseInt(enchant.attributeValue("value"));
				Enchant ench = new Enchant(e_id, e_count, e_value);
				donate.setEnchant(ench);
			}

			Element attribution = donation.element("attribution");
			if(attribution != null)
			{
				int a_id = Integer.parseInt(attribution.attributeValue("cost_id"));
				long a_count = Long.parseLong(attribution.attributeValue("cost_count"));
				int a_value = Integer.parseInt(attribution.attributeValue("value"));
				int size = Integer.parseInt(attribution.attributeValue("size"));
				Attribution atr = new Attribution(a_id, a_count, a_value, size);
				donate.setAttribution(atr);
			}

			getHolder().addDonate(donate);
			//donate.print();
		}
	}

	private List<DonateItem> simple_parse(Element n)
	{
		List<DonateItem> list = new ArrayList<DonateItem>();

		for(Iterator<Element> iterator = n.elementIterator(); iterator.hasNext();)
		{
			Element d = iterator.next();

			if("simple".equalsIgnoreCase(d.getName()))
			{
				int id = Integer.parseInt(d.attributeValue("id"));
				long count = Long.parseLong(d.attributeValue("count"));
				int enchant = Integer.parseInt(d.attributeValue("enchant"));
				DonateItem donate = new DonateItem(id, count, enchant);
				list.add(donate);
			}
		}

		return list;
	}

	private List<DonateItem> donate_parse(Element n)
	{
		List<DonateItem> list = new ArrayList<DonateItem>();

		for(Iterator<Element> iterator = n.elementIterator(); iterator.hasNext();)
		{
			Element d = iterator.next();

			if("foundation".equalsIgnoreCase(d.getName()))
			{
				int id = Integer.parseInt(d.attributeValue("id"));
				long count = Long.parseLong(d.attributeValue("count"));
				int enchant = Integer.parseInt(d.attributeValue("enchant"));
				DonateItem donate = new DonateItem(id, count, enchant);
				list.add(donate);
			}
		}

		return list;
	}
}