package com.l2cccp.gameserver.data.reparse;

import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.napile.pair.primitive.IntObjectPair;
import org.napile.primitive.maps.IntObjectMap;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.templates.client.NpcStringLine;
import com.l2cccp.gameserver.utils.Language;

public class NpcStringReparse
{
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

	public static void reparse(IntObjectMap<NpcStringLine> collection, Language lang)
	{
		Document document = DocumentHelper.createDocument();
		document.addDocType("strings", null, "npc_string.dtd");
		Date date = new Date();
		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
		document.addComment(" Site: http://l2cccp.com/ ");

		Element element = document.addElement("strings");
		element.addAttribute("lang", lang.name());

		for(IntObjectPair<NpcStringLine> string : collection.entrySet())
		{
			Element config = element.addElement("string");
			NpcStringLine str = string.getValue();
			config.addAttribute("id", String.valueOf(str.getId()));
			config.addAttribute("text", String.valueOf(str.getValue()));
		}

		try
		{
			String path = Config.DATAPACK_ROOT + "/data/reparser/client/npc_string-" + lang.getShortName() + ".xml";

			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
			prettyPrint.setIndent("\t");
			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
			xmlWriter.write(document);
			xmlWriter.close();
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
	}
}