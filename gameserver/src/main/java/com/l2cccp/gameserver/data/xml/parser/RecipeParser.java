package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.data.xml.holder.RecipeHolder;
import com.l2cccp.gameserver.model.recipe.Recipe;
import com.l2cccp.gameserver.model.recipe.RecipeComponent;
import com.l2cccp.gameserver.model.recipe.RecipeHandler;
import com.l2cccp.gameserver.templates.item.ItemTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RecipeParser extends AbstractFileParser<RecipeHolder>
{
	private static RecipeParser _instance;

	public static RecipeParser getInstance()
	{
		if(_instance == null)
			_instance = new RecipeParser();

		return _instance;
	}

	protected RecipeParser()
	{
		super(RecipeHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/recipe/recipe.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "recipe.dtd";
	}

	@Override
	protected void readData(Element root) throws Exception
	{
		for(Iterator<Element> iterator = root.elementIterator("recipes"); iterator.hasNext();)
		{
			final Element recipes = iterator.next();
			final int id = Integer.parseInt(recipes.attributeValue("id"));
			final String name = recipes.attributeValue("name");
			Recipe recipe = new Recipe(id, name);
			init(recipe, recipes);
			getHolder().addRecipe(recipe);
		}
	}

	private void init(final Recipe recipe, final Element element)
	{
		final RecipeHandler handler = new RecipeHandler();
		for(Iterator<Element> iterator = element.elementIterator(); iterator.hasNext();)
		{
			final Element att = iterator.next();
			if("info".equals(att.getName()))
			{
				final int blueprint = Integer.parseInt(att.attributeValue("blueprint"));
				ItemTemplate template = ItemHolder.getInstance().getTemplate(blueprint, true);
				template.setHandler(handler);
				final int level = Integer.parseInt(att.attributeValue("level"));
				final int chance = Integer.parseInt(att.attributeValue("chance"));
				final String mp = att.attributeValue("spending");
				final String foundation = att.attributeValue("foundation");
				final String dwarven = att.attributeValue("dwarven");
				final int spending = mp != null ? Integer.parseInt(mp) : 0;
				final int found = foundation != null ? Integer.parseInt(foundation) : 0;
				final boolean isdwarven = dwarven != null ? Boolean.parseBoolean(dwarven) : false;

				recipe.setDwarvenCraft(isdwarven);
				recipe.setFoundation(found);
				recipe.setRecipeId(blueprint);
				recipe.setMpCost(spending);
				recipe.setLevel(level);
				recipe.setSuccessRate(chance);
			}
			else if("creation".equals(att.getName()))
			{
				final int id = Integer.parseInt(att.attributeValue("id"));
				final int count = Integer.parseInt(att.attributeValue("count"));
				recipe.setItemId(id);
				recipe.setCount(count);
			}
			else if("reward".equals(att.getName()))
			{
				final int exp = Integer.parseInt(att.attributeValue("exp"));
				final int sp = Integer.parseInt(att.attributeValue("sp"));
				recipe.setExp(exp);
				recipe.setSp(sp);
			}
			else if("consume".equals(att.getName()))
			{
				final int id = Integer.parseInt(att.attributeValue("id"));
				final int quantity = Integer.parseInt(att.attributeValue("quantity"));
				recipe.addRecipe(new RecipeComponent(id, quantity));
			}
		}
	}
}