package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;
import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.StringHolder;

/**
 * @author VISTALL
 * @since 13.03.14
 */
public class StringParser extends AbstractFileParser<StringHolder>
{
	private static final StringParser _instance = new StringParser();

	public static StringParser getInstance()
	{
		return _instance;
	}

	private StringParser()
	{
		super(StringHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/strings/strings.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "strings.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator(); iterator.hasNext();)
		{
			Element element = iterator.next();

			String key = element.attributeValue("key");

			for(Element childElement : element.elements())
			{
				getHolder().addString(key, childElement.getName(), childElement.getTextTrim());
			}
		}

	}
}
