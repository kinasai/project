package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.leviathan.component.maps.EventMap;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class LeviathanMapsHolder extends AbstractHolder
{
	private static final LeviathanMapsHolder _instance = new LeviathanMapsHolder();
	private TIntObjectHashMap<List<EventMap>> _maps = new TIntObjectHashMap<List<EventMap>>();

	public static LeviathanMapsHolder getInstance()
	{
		return _instance;
	}

	public void addMap(final int event, final EventMap map)
	{
		List<EventMap> maps = _maps.get(event);
		if(maps == null)
			_maps.put(event, maps = new ArrayList<EventMap>());

		maps.add(map);
	}

	public List<EventMap> getMaps(final int event)
	{
		return _maps.get(event);
	}

	@Override
	public int size()
	{
		return _maps.size();
	}

	@Override
	public void clear()
	{
		_maps.clear();
	}
}
