package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.RateHolder;
import com.l2cccp.gameserver.model.rate.RateSetting;
import com.l2cccp.gameserver.model.rate.RateType;
import com.l2cccp.gameserver.templates.StatsSet;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class RateParser extends AbstractFileParser<RateHolder>
{
	private static final RateParser _instance = new RateParser();

	public static RateParser getInstance()
	{
		return _instance;
	}

	private RateParser()
	{
		super(RateHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "config/rates/rates.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "rates.dtd";
	}

	@Override
	protected void readData(Element element) throws Exception
	{
		for(Iterator<Element> iterator = element.elementIterator("setting"); iterator.hasNext();)
		{
			Element data = iterator.next();
			final int start = Integer.parseInt(data.attributeValue("start"));
			final int next = Integer.parseInt(data.attributeValue("next"));
			final boolean announce = Boolean.parseBoolean(data.attributeValue("announce"));

			final StatsSet rates = new StatsSet();
			parseRates(data, rates);
			final RateSetting setting = new RateSetting(start, next, announce, rates);
			getHolder().add(setting);
		}
	}

	private void parseRates(final Element element, final StatsSet rates)
	{
		for(Iterator<Element> iterator = element.elementIterator("rate"); iterator.hasNext();)
		{
			Element rate = iterator.next();
			final RateType type = RateType.valueOf(rate.attributeValue("type"));
			final double val = Double.parseDouble(rate.attributeValue("val"));
			rates.set(type.name(), val);
		}
	}
}