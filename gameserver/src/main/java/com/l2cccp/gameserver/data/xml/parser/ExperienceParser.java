package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.ExperienceHolder;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class ExperienceParser extends AbstractFileParser<ExperienceHolder>
{
	private static final ExperienceParser _instance = new ExperienceParser();

	public static ExperienceParser getInstance()
	{
		return _instance;
	}

	private ExperienceParser()
	{
		super(ExperienceHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/experience/experience.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "experience.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("experience"); iterator.hasNext();)
		{
			Element announcement = iterator.next();
			final int level = Integer.parseInt(announcement.attributeValue("level"));
			final long exp = Long.parseLong(announcement.attributeValue("exp"));
			getHolder().init(level, exp);
		}
	}
}