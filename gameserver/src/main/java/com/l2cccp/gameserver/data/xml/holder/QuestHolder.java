package com.l2cccp.gameserver.data.xml.holder;

import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.quest.QuestTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class QuestHolder extends AbstractHolder
{
	private final static QuestHolder _instance = new QuestHolder();
	private Map<Integer, QuestTemplate> _quests = new HashMap<Integer, QuestTemplate>();

	public static QuestHolder getInstance()
	{
		return _instance;
	}

	public void add(final QuestTemplate quest)
	{
		_quests.put(quest.getId(), quest);
	}

	public QuestTemplate get(final int quest)
	{
		return _quests.get(quest);
	}

	@Override
	public int size()
	{
		return _quests.size();
	}

	@Override
	public void clear()
	{
		_quests.clear();
	}
}
