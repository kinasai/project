package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.FortuneBoxHolder;
import com.l2cccp.gameserver.model.minigame.FortuneBox;
import com.l2cccp.gameserver.model.minigame.FortuneBoxReward;

public final class FortuneBoxParser extends AbstractFileParser<FortuneBoxHolder>
{
	private static final FortuneBoxParser _instance = new FortuneBoxParser();

	public static FortuneBoxParser getInstance()
	{
		return _instance;
	}

	private FortuneBoxParser()
	{
		super(FortuneBoxHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/minigame/fortune_box/fortune_box.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "fortune_box.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		Iterator<Element> iter = rootElement.elementIterator("config");

		if(iter.hasNext())
		{
			Element config = iter.next();
			boolean enable = Boolean.parseBoolean(config.attributeValue("enable"));
			boolean log = Boolean.parseBoolean(config.attributeValue("log"));
			String[] level = String.valueOf(config.attributeValue("level")).split(";");

			int min = Integer.parseInt(level[0]);
			int max = Integer.parseInt(level[1]);
			getHolder().setConfig(enable, log, min, max);
		}

		for(Iterator<Element> iterator = rootElement.elementIterator("box"); iterator.hasNext();)
		{
			String name = null;
			int id, itemId = 0, max = 1;
			long itemCount = 0L;

			//data
			Element data = iterator.next();
			id = Integer.parseInt(data.attributeValue("id"));
			name = data.attributeValue("name");

			//price
			Element price = data.element("price");
			itemId = Integer.parseInt(price.attributeValue("id"));
			itemCount = Long.parseLong(price.attributeValue("count"));

			//rewards
			Element rewards = data.element("rewards");
			max = Integer.parseInt(rewards.attributeValue("max"));

			List<FortuneBoxReward> _rewards = new ArrayList<FortuneBoxReward>();
			for(Element element2 : rewards.elements())
			{
				String name1 = element2.getName();

				if(name1.equals("reward"))
				{
					int _id = Integer.parseInt(element2.attributeValue("id"));

					long[] _count = null;
					String _element2 = element2.attributeValue("count");
					if(_element2 != null)
					{
						String[] array = _element2.split(";");
						_count = new long[] { Long.parseLong(array[0]), Long.parseLong(array[1]) };
					}

					double _chance = 0;
					String _element3 = element2.attributeValue("chance");
					if(_element3 != null)
						_chance = Double.parseDouble(_element3);

					double[] _enchant = null;
					String _element4 = element2.attributeValue("enchant");
					if(_element4 != null)
					{
						String[] array = _element4.split(";");
						_enchant = new double[] { Double.parseDouble(array[0]), Double.parseDouble(array[1]), Double.parseDouble(array[2]) };
					}
					boolean _announce = false;
					String _element5 = element2.attributeValue("announce");
					if(_element5 != null)
						_announce = Boolean.parseBoolean(_element5);

					_rewards.add(new FortuneBoxReward(_id, _count, _chance, _enchant, _announce));
				}
			}

			getHolder().addBox(new FortuneBox(id, name, max, itemId, itemCount, _rewards));
		}
	}
}