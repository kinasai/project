package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.commons.geometry.Rectangle;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
import com.l2cccp.gameserver.data.xml.holder.RiftsHolder;
import com.l2cccp.gameserver.model.SimpleSpawner;
import com.l2cccp.gameserver.model.Territory;
import com.l2cccp.gameserver.model.entity.rifts.DimensionalRiftRoom;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class RiftsParser extends AbstractDirParser<RiftsHolder>
{
	private static final RiftsParser _instance = new RiftsParser();

	public static RiftsParser getInstance()
	{
		return _instance;
	}

	private RiftsParser()
	{
		super(RiftsHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/rifts/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "rifts.dtd";
	}

	@Override
	protected void readData(Element element) throws Exception
	{
		for(Iterator<Element> iterator = element.elementIterator("area"); iterator.hasNext();)
		{
			Element area = iterator.next();
			final int id = Integer.parseInt(area.attributeValue("id"));

			if(!getHolder().contains(id))
				getHolder().put(id);

			parseRooms(area, id);
		}
	}

	private void parseRooms(Element area, int arena_id)
	{
		for(Iterator<Element> it = area.elementIterator("room"); it.hasNext();)
		{
			Element room = it.next();

			final int id = Integer.parseInt(room.attributeValue("id"));
			final boolean boss = Boolean.valueOf(room.attributeValue("boss", "false"));
			parseParams(room, arena_id, id, boss);
		}
	}

	@SuppressWarnings("deprecation")
	private void parseParams(Element room, int arena_id, int id, boolean boss)
	{
		Territory territory = null;
		Location teleport = new Location();
		List<SimpleSpawner> spawns = null;

		for(Iterator<Element> it = room.elementIterator(); it.hasNext();)
		{
			Element items = it.next();

			if("zone".equalsIgnoreCase(items.getName()))
			{
				String[] xdat = items.attributeValue("x").split(":");
				final int x_min = Integer.parseInt(xdat[0]);
				final int x_max = Integer.parseInt(xdat[1]);

				String[] ydat = items.attributeValue("y").split(":");
				final int y_min = Integer.parseInt(ydat[0]);
				final int y_max = Integer.parseInt(ydat[1]);

				String[] zdat = items.attributeValue("z").split(":");
				final int z_min = Integer.parseInt(zdat[0]);
				final int z_max = Integer.parseInt(zdat[1]);

				territory = new Territory().add(new Rectangle(x_min, y_min, x_max, y_max).setZmin(z_min).setZmax(z_max));
			}
			else if("teleport".equalsIgnoreCase(items.getName()))
			{
				final int x = Integer.parseInt(items.attributeValue("x"));
				final int y = Integer.parseInt(items.attributeValue("y"));
				final int z = Integer.parseInt(items.attributeValue("z"));
				teleport.set(x, y, z);
			}
			else if("spawn".equalsIgnoreCase(items.getName()))
			{
				final int npc_id = Integer.parseInt(items.attributeValue("id"));
				final int count = Integer.parseInt(items.attributeValue("count"));
				final int respawn = Integer.parseInt(items.attributeValue("respawn"));

				NpcTemplate template = NpcHolder.getInstance().getTemplate(npc_id);

				if(spawns == null)
					spawns = new ArrayList<SimpleSpawner>();

				SimpleSpawner spawn = new SimpleSpawner(template);
				spawn.setTerritory(territory);
				spawn.setHeading(-1);
				spawn.setRespawnDelay(respawn);
				spawn.setAmount(count);
				spawns.add(spawn);
			}
		}

		final DimensionalRiftRoom rift = new DimensionalRiftRoom(territory, teleport, boss);
		if(spawns != null)
			rift.setSpawns(spawns);

		getHolder().getRooms(arena_id).put(id, rift);
	}
}