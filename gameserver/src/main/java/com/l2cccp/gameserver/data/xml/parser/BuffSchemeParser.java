package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.BuffSchemeHolder;
import com.l2cccp.gameserver.model.bbs.buffer.Buff;
import com.l2cccp.gameserver.model.bbs.buffer.Schemes;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class BuffSchemeParser extends AbstractFileParser<BuffSchemeHolder>
{
	private static final BuffSchemeParser _instance = new BuffSchemeParser();

	public static BuffSchemeParser getInstance()
	{
		return _instance;
	}

	private BuffSchemeParser()
	{
		super(BuffSchemeHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/buffer/scheme.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "scheme.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("scheme"); iterator.hasNext();)
		{
			final Element data = iterator.next();

			final int id = Integer.parseInt(data.attributeValue("id"));
			final String name = data.attributeValue("name");
			final int item = Integer.parseInt(data.attributeValue("item"));
			final int count = Integer.parseInt(data.attributeValue("count"));
			final Schemes scheme = new Schemes(id, name, item, count);
			final List<Integer> list = parseAllowedClass(data.attributeValue("allowedClass"));
			scheme.addAllowedClass(list);

			parseBuffs(data, scheme);

			getHolder().put(id, scheme);
		}
	}

	private void parseBuffs(Element n, Schemes scheme)
	{
		for(Iterator<Element> iterator = n.elementIterator(); iterator.hasNext();)
		{
			Element element = iterator.next();
			if("buff".equalsIgnoreCase(element.getName()))
			{
				final int id = Integer.parseInt(element.attributeValue("id"));
				final int level = Integer.parseInt(element.attributeValue("level"));

				final Buff buff = new Buff(id, level);
				final List<Integer> list = parseAllowedClass(element.attributeValue("allowedClass"));
				buff.addAllowedList(list);

				scheme.addBuff(buff);
			}
		}
	}

	private List<Integer> parseAllowedClass(String allowed)
	{
		List<Integer> list = null;

		if(allowed != null)
		{
			list = new ArrayList<Integer>();
			for(String info : allowed.split(","))
			{
				final int _class = Integer.parseInt(info);
				list.add(_class);
			}
		}

		return list;
	}
}