package com.l2cccp.gameserver.data.xml.holder;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.leviathan.AbstractLeviathan;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class LeviathanEventHolder extends AbstractHolder
{
	private static final LeviathanEventHolder _instance = new LeviathanEventHolder();
	private TIntObjectHashMap<AbstractLeviathan> _events = new TIntObjectHashMap<AbstractLeviathan>();

	public static LeviathanEventHolder getInstance()
	{
		return _instance;
	}

	public void addEvent(final AbstractLeviathan event)
	{
		_events.put(event.getTemplate().getEventId(), event);
	}

	public AbstractLeviathan getEvent(final int id)
	{
		return _events.get(id);
	}

	@Override
	public int size()
	{
		return _events.size();
	}

	@Override
	public void clear()
	{
		_events.clear();
	}

	public void initAll()
	{
		for(final AbstractLeviathan event : _events.valueCollection())
		{
			info("[" + event.getTemplate().getEventId() + "] " + event.getName() + " load successful...");
			event.init();
		}
	}
}
