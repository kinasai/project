package com.l2cccp.gameserver.data.xml.holder;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.rate.RateSetting;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class RateHolder extends AbstractHolder
{
	private final static RateHolder _instance = new RateHolder();
	private Map<Integer, RateSetting> rates = new HashMap<Integer, RateSetting>();

	public static RateHolder getInstance()
	{
		return _instance;
	}

	public void add(final RateSetting setting)
	{
		rates.put(setting.getStart(), setting);
	}

	public RateSetting get(final int level)
	{
		if(rates.containsKey(level))
			return rates.get(level);

		final RateSetting rate = find(level);
		if(rate != null)
			return rate;
		else
			return rates.get(1);
	}

	public RateSetting find(final int level)
	{
		for(Entry<Integer, RateSetting> entry : rates.entrySet())
		{
			final RateSetting rate = entry.getValue();
			if(level >= rate.getStart() && level < rate.getNext())
				return rate;
		}

		return null;
	}

	@Override
	public int size()
	{
		return rates.size();
	}

	@Override
	public void clear()
	{
		rates.clear();
	}
}