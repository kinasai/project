package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.entity.events.maps.EventMaps;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class EventMapsHolder extends AbstractHolder
{
	private static final EventMapsHolder _instance = new EventMapsHolder();
	private final List<EventMaps> _maps = new ArrayList<EventMaps>();

	public static EventMapsHolder getInstance()
	{
		return _instance;
	}

	public void addMap(EventMaps map)
	{
		_maps.add(map);
	}

	public List<EventMaps> getMapsForEvent(int event)
	{
		List<EventMaps> maps = new ArrayList<EventMaps>();
		for(EventMaps map : _maps)
		{
			for(int id : map.getEvents())
			{
				if(id == event)
					maps.add(map);
			}
		}
		return maps;
	}

	@Override
	public int size()
	{
		return _maps.size();
	}

	@Override
	public void clear()
	{
		_maps.clear();
	}
}