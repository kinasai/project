package com.l2cccp.gameserver.data.xml.holder;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.bbs.buffer.Buff;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BuffsHolder extends AbstractHolder
{
	private static final BuffsHolder _instance = new BuffsHolder();
	private List<Buff> _buffs = new ArrayList<Buff>();

	public static BuffsHolder getInstance()
	{
		return _instance;
	}

	public void add(Buff buffs)
	{
		_buffs.add(buffs);
	}

	public Buff get(int id)
	{
		for(Buff buff : _buffs)
		{
			if(buff.getId() == id)
				return buff;
		}

		return null;
	}

	public List<Buff> get()
	{
		return _buffs;
	}

	@Override
	public int size()
	{
		return _buffs.size();
	}

	@Override
	public void clear()
	{
		_buffs.clear();
	}
}