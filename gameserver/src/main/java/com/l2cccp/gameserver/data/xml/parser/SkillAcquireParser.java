package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.SkillAcquireHolder;
import com.l2cccp.gameserver.model.SkillLearn;

import gnu.trove.map.hash.TIntObjectHashMap;

/**
 * @author VISTALL
 * @date  20:55/30.11.2010
 */
public final class SkillAcquireParser extends AbstractDirParser<SkillAcquireHolder>
{
	private static final SkillAcquireParser _instance = new SkillAcquireParser();

	public static SkillAcquireParser getInstance()
	{
		return _instance;
	}

	protected SkillAcquireParser()
	{
		super(SkillAcquireHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/skill_tree/");
	}

	@Override
	public boolean isIgnored(File b)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "tree.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("certification_skill_tree"); iterator.hasNext();)
			getHolder().addAllCertificationLearns(parseSkillLearn((Element) iterator.next(), false));

		for(Iterator<Element> iterator = rootElement.elementIterator("sub_unit_skill_tree"); iterator.hasNext();)
			getHolder().addAllSubUnitLearns(parseSkillLearn((Element) iterator.next(), false));

		for(Iterator<Element> iterator = rootElement.elementIterator("pledge_skill_tree"); iterator.hasNext();)
			getHolder().addAllPledgeLearns(parseSkillLearn((Element) iterator.next(), false));

		for(Iterator<Element> iterator = rootElement.elementIterator("collection_skill_tree"); iterator.hasNext();)
			getHolder().addAllCollectionLearns(parseSkillLearn((Element) iterator.next(), false));

		for(Iterator<Element> iterator = rootElement.elementIterator("fishing_skill_tree"); iterator.hasNext();)
			getHolder().addAllFishingLearns(parseSkillLearn((Element) iterator.next(), false));

		for(Iterator<Element> iterator = rootElement.elementIterator("fishing_non_dwarf_skill_tree"); iterator.hasNext();)
			getHolder().addAllFishingNonDwarfLearns(parseSkillLearn((Element) iterator.next(), false));

		for(Iterator<Element> iterator = rootElement.elementIterator("transfer_skill_tree"); iterator.hasNext();)
		{
			Element nxt = iterator.next();
			for(Iterator<Element> classIterator = nxt.elementIterator("class"); classIterator.hasNext();)
			{
				Element classElement = classIterator.next();
				int classId = Integer.parseInt(classElement.attributeValue("id"));
				List<SkillLearn> learns = parseSkillLearn(classElement, false);
				getHolder().addAllTransferLearns(classId, learns);
			}
		}

		for(Iterator<Element> iterator = rootElement.elementIterator("normal_skill_tree"); iterator.hasNext();)
		{
			TIntObjectHashMap<List<SkillLearn>> map = new TIntObjectHashMap<List<SkillLearn>>();
			Element nxt = iterator.next();
			for(Iterator<Element> classIterator = nxt.elementIterator("class"); classIterator.hasNext();)
			{
				Element classElement = classIterator.next();
				int classId = Integer.parseInt(classElement.attributeValue("id"));
				List<SkillLearn> learns = parseSkillLearn(classElement, true);
				map.put(classId, learns);
			}

			getHolder().addAllNormalSkillLearns(map);
		}

		for(Iterator<Element> iterator = rootElement.elementIterator("transformation_skill_tree"); iterator.hasNext();)
		{
			Element nxt = iterator.next();
			for(Iterator<Element> classIterator = nxt.elementIterator("race"); classIterator.hasNext();)
			{
				Element classElement = classIterator.next();
				int race = Integer.parseInt(classElement.attributeValue("id"));
				List<SkillLearn> learns = parseSkillLearn(classElement, true);
				getHolder().addAllTransformationLearns(race, learns);
			}
		}
	}
	//
	//	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
	//	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");
	//
	//	private void parseBotSkill(int classId, List<SkillLearn> learns)
	//	{
	//		org.dom4j.Document document = DocumentHelper.createDocument();
	//		document.addDocType("list", null, "skills.dtd");
	//		Date date = new Date();
	//		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
	//		document.addComment(" Site: http://l2cccp.com/ ");
	//
	//		Element element = document.addElement("list");
	//		Element skill = element.addElement("skill");
	//		skill.addAttribute("id", String.valueOf(classId));
	//		skill.addAttribute("name", Util.className(Language.ENGLISH, classId));
	//
	//		for(final SkillLearn learn : learns)
	//		{
	//			Element data = skill.addElement("learn");
	//			data.addAttribute("id", String.valueOf(learn.getId()));
	//			data.addAttribute("level", String.valueOf(learn.getLevel()));
	//			data.addAttribute("learn_level", String.valueOf(learn.getMinLevel()));
	//			skill.addComment(" " + SkillTable.getInstance().getSkillEntry(learn.getId(), learn.getLevel()).getName() + " ");
	//		}
	//
	//		try
	//		{
	//			String path = "A:/files/bot/skills/[" + classId + "] " + Util.className(Language.ENGLISH, classId) + ".xml";
	//
	//			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
	//			prettyPrint.setIndent("\t");
	//			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
	//			xmlWriter.write(document);
	//			xmlWriter.close();
	//		}
	//		catch(Exception e1)
	//		{
	//			e1.printStackTrace();
	//		}
	//	}

	private List<SkillLearn> parseSkillLearn(Element tree, boolean normal)
	{
		List<SkillLearn> skillLearns = new ArrayList<SkillLearn>();
		for(Iterator<Element> iterator = tree.elementIterator("skill"); iterator.hasNext();)
		{
			Element element = iterator.next();
			String value;

			int id = Integer.parseInt(element.attributeValue("id"));
			int level = Integer.parseInt(element.attributeValue("level"));
			value = element.attributeValue("cost");
			int cost = value == null ? 0 : Integer.parseInt(value);
			int min_level = Integer.parseInt(element.attributeValue("min_level"));
			value = element.attributeValue("item_id");
			int item_id = value == null ? 0 : Integer.parseInt(value);
			if(normal && item_id != 0)
				getHolder().addLearnItem(item_id);

			value = element.attributeValue("item_count");
			long item_count = value == null ? 1 : Long.parseLong(value);
			value = element.attributeValue("clicked");
			boolean clicked = value != null && Boolean.parseBoolean(value);
			value = element.attributeValue("auto_learn");
			Boolean autoLearn = value == null ? null : Boolean.parseBoolean(value);

			skillLearns.add(new SkillLearn(id, level, min_level, cost, item_id, item_count, clicked, autoLearn));
		}

		return skillLearns;
	}
}
