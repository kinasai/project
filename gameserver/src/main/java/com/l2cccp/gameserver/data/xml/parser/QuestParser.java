package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Element;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.QuestHolder;
import com.l2cccp.gameserver.model.quest.QuestEventType;
import com.l2cccp.gameserver.model.quest.QuestTemplate;
import com.l2cccp.gameserver.model.quest.QuestType;
import com.l2cccp.gameserver.utils.Language;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class QuestParser extends AbstractDirParser<QuestHolder>
{
	private static final QuestParser _instance = new QuestParser();

	public static QuestParser getInstance()
	{
		return _instance;
	}

	private QuestParser()
	{
		super(QuestHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/quests/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "quests.dtd";
	}

	@Override
	protected void readData(Element element) throws Exception
	{
		for(Iterator<Element> iterator = element.elementIterator("quest"); iterator.hasNext();)
		{
			Element quest = iterator.next();
			final int id = Integer.parseInt(quest.attributeValue("id"));
			final QuestType type = QuestType.valueOf(quest.attributeValue("type"));

			QuestTemplate template = new QuestTemplate(id, type);
			parseData(quest, template);
		}
	}

	private void parseData(Element quest, final QuestTemplate template)
	{
		Map<Language, String> name = new HashMap<Language, String>();
		for(Iterator<Element> it = quest.elementIterator(); it.hasNext();)
		{
			Element element = it.next();

			final String link = element.getName();
			if("name".equalsIgnoreCase(link))
			{
				final Language lang = Language.valueOf(element.attributeValue("lang"));
				final String val = element.attributeValue("val");
				name.put(lang, val);
			}
			else if("setting".equalsIgnoreCase(link))
				parseSetting(element, template);
			else
				_log.error("Unknown element in quest data -> " + link);
		}

		template.setNames(name);
		getHolder().add(template);
	}

	private void parseSetting(Element quest, final QuestTemplate template)
	{
		final Map<QuestEventType, List<Integer>> events = new HashMap<QuestEventType, List<Integer>>();
		final List<Integer> items = new ArrayList<Integer>();
		final MultiValueSet<String> params = new MultiValueSet<String>();
		for(Iterator<Element> it = quest.elementIterator(); it.hasNext();)
		{
			Element element = it.next();

			final String link = element.getName();
			if("rate".equalsIgnoreCase(link))
			{
				final double drop = Double.parseDouble(element.attributeValue("drop", "1.0"));
				final double reward = Double.parseDouble(element.attributeValue("reward", "1.0"));
				final double exp = Double.parseDouble(element.attributeValue("exp", "1.0"));
				final double sp = Double.parseDouble(element.attributeValue("sp", "1.0"));

				template.setDrop(drop);
				template.setReward(reward);
				template.setExp(exp);
				template.setSp(sp);
			}
			else if("event".equalsIgnoreCase(link))
			{
				final QuestEventType type = QuestEventType.valueOf(element.attributeValue("type"));
				final int id = Integer.parseInt(element.attributeValue("val"));

				if(events.containsKey(type))
				{
					List<Integer> list = events.get(type);
					list.add(id);
				}
				else
				{
					List<Integer> list = new ArrayList<Integer>();
					list.add(id);
					events.put(type, list);
				}
			}
			else if("item".equalsIgnoreCase(link))
			{
				final int id = Integer.parseInt(element.attributeValue("id"));
				items.add(id);
			}
			else if("param".equalsIgnoreCase(link))
			{
				final String key = element.attributeValue("key");
				final String value = element.attributeValue("value");
				params.set(key, value);
			}
			else
				_log.error("Unknown element in quest setting -> " + link);
		}

		template.setEvents(events);
		template.setItems(items);
		template.setParams(params);
	}
}