package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Element;

import com.l2cccp.commons.data.xml.jdom2.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.DailyTaskHolder;
import com.l2cccp.gameserver.mod.DailyTasks.DailyTask;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Craft;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Farm;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Level;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Monster;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Pk;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.PvP;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Reward;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Spoil;
import com.l2cccp.gameserver.mod.DailyTasks.Impl.Trigger;

public final class DailyTaskParser extends AbstractFileParser<DailyTaskHolder> {
	private DailyTaskParser() {
        super(DailyTaskHolder.getInstance());
    }

    public static DailyTaskParser getInstance() {
        return LazyHolder.INSTANCE;
    }

    @Override
    public File getXMLFile() {
        return new File(Config.DATAPACK_ROOT, "data/custom/DailyTasks/DailyTasks.xml");
    }

    @Override
    public String getDTDFileName() {
        return "DailyTasks.dtd";
    }
	
	@Override
    protected void readData(final DailyTaskHolder holder, final Element rootElement) throws Exception {
        for (final Element ElementTask : rootElement.getChildren("task")) {
        	final DailyTask task = new DailyTask();
			final List<Level> lvls = new ArrayList<>();
        	final Trigger trigger = Trigger.valueOf(ElementTask.getAttributeValue("trigger"));
        	task.setTrigger(trigger);
        	for(final Element taskElement : ElementTask.getChildren()) {
        		if(taskElement.getName().equalsIgnoreCase("msg")) {
        			final String name = taskElement.getAttributeValue("name");
        			final String valRu = taskElement.getAttributeValue("valRu");
        			final String valEn = taskElement.getAttributeValue("valEn");
        			if(name.equalsIgnoreCase("senderName")) {
        				task.setSenderNameRu(valRu);
        				task.setSenderNameEn(valEn);
        			}
        			else if(name.equalsIgnoreCase("topicName")) {
        				task.setTopicNameRu(valRu);
        				task.setTopicNameEn(valEn);
        			} else if(name.equalsIgnoreCase("body")) {
        				task.setBodyRu(valRu);
        				task.setBodyEn(valEn);
        			} else if(name.equalsIgnoreCase("systemStringOnKill")) {
        				task.setSystemStringOnKillRu(valRu);
        				task.setSystemStringOnKillEn(valEn);
        			} else if(name.equalsIgnoreCase("systemStringOnComplete")) {
        				task.setSystemStringOnCompleteRu(valRu);
        				task.setSystemStringOnCompleteEn(valEn);
        			}
        		} else if(taskElement.getName().equalsIgnoreCase("level")) {
        			final Level lvl = new Level();
        			lvl.setMin(Integer.parseInt(taskElement.getAttributeValue("min", "1")));
        			lvl.setMax(Integer.parseInt(taskElement.getAttributeValue("max", "999")));
        			lvl.setRewardExp(Long.parseLong(taskElement.getAttributeValue("exp", "0")));
        			lvl.setRewardSp(Long.parseLong(taskElement.getAttributeValue("sp", "0")));
        			lvl.setEnableRates(Boolean.parseBoolean(taskElement.getAttributeValue("enableRates", "false")));
        			lvl.setPerUnit(Boolean.parseBoolean(taskElement.getAttributeValue("isPerUnit", "false")));
        			lvl.setPartyAll(Boolean.parseBoolean(taskElement.getAttributeValue("isPartyAll", "false")));
        			for(final Element lvlElement : taskElement.getChildren()) {
        				if(lvlElement.getName().equalsIgnoreCase("rewardItems")) {
        					final List<Reward> rewards = new ArrayList<>();
        					for(final Element itemElement : lvlElement.getChildren()) {
	        					if(itemElement.getName().equalsIgnoreCase("item")) {
	        						final int id = Integer.parseInt(itemElement.getAttributeValue("id"));
	        						final long count = Integer.parseInt(itemElement.getAttributeValue("count")); 
	        						final boolean isEnableRate = Boolean.parseBoolean(itemElement.getAttributeValue("enableRates", "false")); 
	        						final boolean isPerUnit = Boolean.parseBoolean(itemElement.getAttributeValue("isPerUnit", "false")); 
	        						final Reward reward = new Reward(id, count, isEnableRate, isPerUnit);
	        						rewards.add(reward);
	        					}
        					}
        					lvl.setRewardList(rewards);
        				} else if(lvlElement.getName().equalsIgnoreCase("monsters")) {
        					final List<Monster> monsters = new ArrayList<>();
        					for(final Element mobElement : lvlElement.getChildren()) {
        						if(mobElement.getName().equalsIgnoreCase("mob")) {
	        						final int id = Integer.parseInt(mobElement.getAttributeValue("id"));
	        						final long minCount = Long.parseLong(mobElement.getAttributeValue("minCount"));
	        						final long maxCount = Long.parseLong(mobElement.getAttributeValue("maxCount"));
	        						final Monster monster = new Monster(id, minCount, maxCount);
	        						monsters.add(monster);
        						}
        					}
        					lvl.setMonsterList(monsters);
        				} else if(lvlElement.getName().equalsIgnoreCase("pvps")) {
        					final List<PvP> pvps = new ArrayList<>();
        					for(final Element pvpElement : lvlElement.getChildren()) {
	        					if(pvpElement.getName().equalsIgnoreCase("pvp")) {
	        						final long count = Long.parseLong(pvpElement.getAttributeValue("count"));
	        						final PvP pvp = new PvP(count);
	        						pvps.add(pvp);
	        					}
        					}
        					lvl.setPvpList(pvps);
        				} else if(lvlElement.getName().equalsIgnoreCase("pks")) {
        					final List<Pk> pks = new ArrayList<>();
        					for(final Element pkElement : lvlElement.getChildren()) {
	        					if(pkElement.getName().equalsIgnoreCase("pk")) {
	        						final long count = Long.parseLong(pkElement.getAttributeValue("count"));
	        						final Pk pk = new Pk(count);
	        						pks.add(pk);
	        					}
        					}
        					lvl.setPkList(pks);
        				} else if(lvlElement.getName().equalsIgnoreCase("crafts")) {
        					final List<Craft> crafts = new ArrayList<>();
        					for(final Element craftElement : lvlElement.getChildren()) {
	        					if(craftElement.getName().equalsIgnoreCase("craft")) {
	        						final int id = Integer.parseInt(craftElement.getAttributeValue("id"));
	        						final long minCount = Long.parseLong(craftElement.getAttributeValue("minCount"));
	        						final long maxCount = Long.parseLong(craftElement.getAttributeValue("maxCount"));
	        						final Craft craft = new Craft(id, minCount, maxCount);
	        						crafts.add(craft);
	        					}
        					}
        					lvl.setCraftList(crafts);
        				} else if(lvlElement.getName().equalsIgnoreCase("farms")) {
        					final List<Farm> farms = new ArrayList<>();
        					for(final Element farmElement : lvlElement.getChildren()) {
	        					if(farmElement.getName().equalsIgnoreCase("farm")) {
	        						final int id = Integer.parseInt(farmElement.getAttributeValue("id"));
	        						final long minCount = Long.parseLong(farmElement.getAttributeValue("minCount"));
	        						final long maxCount = Long.parseLong(farmElement.getAttributeValue("maxCount"));
	        						final Farm farm = new Farm(id, minCount, maxCount);
	        						farms.add(farm);
	        					}
        					}
        					lvl.setFarmList(farms);
        				} else if(lvlElement.getName().equalsIgnoreCase("spoils")) {
        					final List<Spoil> spoils = new ArrayList<>();
        					for(final Element spoilElement : lvlElement.getChildren()) {
	        					if(spoilElement.getName().equalsIgnoreCase("spoil")) {
	        						final int id = Integer.parseInt(spoilElement.getAttributeValue("id"));
	        						final long minCount = Long.parseLong(spoilElement.getAttributeValue("minCount"));
	        						final long maxCount = Long.parseLong(spoilElement.getAttributeValue("maxCount"));
	        						final Spoil spoil = new Spoil(id, minCount, maxCount);
	        						spoils.add(spoil);
	        					}
        					}
        					lvl.setSpoilList(spoils);
        				}
        			}
        			lvls.add(lvl);
        		}
        		task.setLevels(lvls);
        	}
        	holder.addTask(task);
        }
    }

    private static class LazyHolder {
        private static final DailyTaskParser INSTANCE = new DailyTaskParser();
    }
}