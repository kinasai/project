package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractDirParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.PlayerBalancerHolder;
import com.l2cccp.gameserver.model.balancing.PlayerBalance;
import com.l2cccp.gameserver.model.balancing.player.BalancerType;
import com.l2cccp.gameserver.model.balancing.player.PlayerLimits;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PlayerBalancerParser extends AbstractDirParser<PlayerBalancerHolder>
{
	private static final PlayerBalancerParser _instance = new PlayerBalancerParser();

	public static PlayerBalancerParser getInstance()
	{
		return _instance;
	}

	private PlayerBalancerParser()
	{
		super(PlayerBalancerHolder.getInstance());
	}

	@Override
	public File getXMLDir()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/pc_parameter/balancer/");
	}

	@Override
	public boolean isIgnored(File f)
	{
		return false;
	}

	@Override
	public String getDTDFileName()
	{
		return "balancer.dtd";
	}

	@Override
	protected void readData(Element element) throws Exception
	{
		for(Iterator<Element> iterator = element.elementIterator("balancer"); iterator.hasNext();)
		{
			Element balancer = iterator.next();
			final int id = Integer.parseInt(balancer.attributeValue("id"));

			PlayerLimits limits = new PlayerLimits();

			for(Iterator<Element> it = balancer.elementIterator(); it.hasNext();)
			{
				Element set = it.next();

				if("set".equalsIgnoreCase(set.getName()))
				{
					final BalancerType type = BalancerType.valueOf(set.attributeValue("param"));
					final double base = Double.parseDouble(set.attributeValue("base"));
					final double olympiad = Double.parseDouble(set.attributeValue("olympiad"));
					final PlayerBalance balance = new PlayerBalance(base, olympiad);

					getHolder().addBalancer(type, id, balance);
				}
				else if("limit".equalsIgnoreCase(set.getName()))
				{
					final BalancerType type = BalancerType.valueOf(set.attributeValue("param"));
					final int max = Integer.parseInt(set.attributeValue("max"));
					limits.setLimit(type, max);
				}
			}

			getHolder().addLimits(id, limits);
		}
	}
}