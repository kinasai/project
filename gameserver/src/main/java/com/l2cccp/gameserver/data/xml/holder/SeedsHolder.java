package com.l2cccp.gameserver.data.xml.holder;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.l2cccp.commons.data.xml.AbstractHolder;
import com.l2cccp.gameserver.model.manor.Seed;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class SeedsHolder extends AbstractHolder
{
	private static final SeedsHolder _instance = new SeedsHolder();

	private static Map<Integer, Seed> _seeds = new ConcurrentHashMap<Integer, Seed>();

	public static SeedsHolder getInstance()
	{
		return _instance;
	}

	public void addSeed(Seed seed)
	{
		_seeds.put(seed.getId(), seed);
	}

	public Collection<Seed> getSeeds()
	{
		return _seeds.values();
	}

	public Map<Integer, Seed> getAllSeeds()
	{
		return _seeds;
	}

	public Seed getSeed(final int id)
	{
		return _seeds.get(id);
	}

	@Override
	public int size()
	{
		return _seeds.size();
	}

	@Override
	public void clear()
	{
		_seeds.clear();
	}
}