package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.util.Iterator;

import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.BaseStatsHolder;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public final class BaseStatsBonusParser extends AbstractFileParser<BaseStatsHolder>
{
	private static final BaseStatsBonusParser _instance = new BaseStatsBonusParser();

	public static BaseStatsBonusParser getInstance()
	{
		return _instance;
	}

	private BaseStatsBonusParser()
	{
		super(BaseStatsHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/pc_parameter/bonus/base_stats_bonus.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "base_stats_bonus.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Iterator<Element> iterator = rootElement.elementIterator("bonus"); iterator.hasNext();)
		{
			Element foundation = iterator.next();
			int level = Integer.parseInt(foundation.attributeValue("level"));
			double STR = Double.parseDouble(foundation.attributeValue("STR"));
			double INT = Double.parseDouble(foundation.attributeValue("INT"));
			double CON = Double.parseDouble(foundation.attributeValue("CON"));
			double MEN = Double.parseDouble(foundation.attributeValue("MEN"));
			double DEX = Double.parseDouble(foundation.attributeValue("DEX"));
			double WIT = Double.parseDouble(foundation.attributeValue("WIT"));

			getHolder().addAttributeBonus(level, STR, INT, CON, MEN, DEX, WIT);
		}
	}
}