package com.l2cccp.gameserver.data.xml.parser;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;

import com.l2cccp.commons.data.xml.AbstractFileParser;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.PromoCodeHolder;
import com.l2cccp.gameserver.model.promocode.PromoCode;
import com.l2cccp.gameserver.model.promocode.PromoCodeReward;
import com.l2cccp.gameserver.utils.TimeUtils;

public class PromoCodeParser extends AbstractFileParser<PromoCodeHolder>
{
	private static final PromoCodeParser _instance = new PromoCodeParser();

	public static PromoCodeParser getInstance()
	{
		return _instance;
	}

	public PromoCodeParser()
	{
		super(PromoCodeHolder.getInstance());
	}

	@Override
	public File getXMLFile()
	{
		return new File(Config.DATAPACK_ROOT, "data/parser/promocodes/promocodes.xml");
	}

	@Override
	public String getDTDFileName()
	{
		return "promocodes.dtd";
	}

	@Override
	protected void readData(Element rootElement) throws Exception
	{
		for(Element element : rootElement.elements())
		{
			String name = element.attributeValue("name");
			int limit = Integer.parseInt(element.attributeValue("limit", "0"));
			boolean limitByUser = Boolean.parseBoolean(element.attributeValue("limitByUser", "false"));

			List<PromoCodeReward> rewards = new ArrayList<PromoCodeReward>();
			long from = 0, to = 0;

			for(Element element2 : element.elements())
			{
				String name1 = element2.getName();
				if(name1.equals("date"))
				{
					String fromValue = element2.attributeValue("from");
					String toValue = element2.attributeValue("to");

					if(fromValue != null)
					{
						from = TimeUtils.parse(fromValue);
					}

					if(toValue != null)
					{
						to = TimeUtils.parse(toValue);
					}
				}
				else
				{
					Class<?> aClass = Class.forName("com.l2cccp.gameserver.model.promocode." + StringUtils.capitalize(name1) + "PromoCodeReward");

					Constructor<?> constructor = aClass.getConstructor(Element.class);

					rewards.add((PromoCodeReward) constructor.newInstance(element2));
				}
			}

			getHolder().addPromoCode(name, new PromoCode(limit, from, to, rewards, limitByUser));
		}
	}
}
