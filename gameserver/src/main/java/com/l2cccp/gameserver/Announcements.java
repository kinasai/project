package com.l2cccp.gameserver;

import com.l2cccp.gameserver.data.xml.holder.StringHolder;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.IBroadcastPacket;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.utils.ChatUtils;

public class Announcements
{
	private static final Announcements _instance = new Announcements();

	public static final Announcements getInstance()
	{
		return _instance;
	}

	public void announceToAll(String text)
	{
		announceToAll(text, ChatType.CRITICAL_ANNOUNCE);
	}

	public static void shout(Player activeChar, String text, ChatType type)
	{
		Say2 cs = new Say2(activeChar.getObjectId(), type, activeChar.getName(), text, null);
		ChatUtils.shout(activeChar, cs);
		activeChar.sendPacket(cs);
	}

	public void announceToAll(String text, ChatType type)
	{
		Say2 cs = new Say2(0, type, "", text, null);
		for(Player player : GameObjectsStorage.getPlayers())
			player.sendPacket(cs);
	}

	public void announceToAll(String text, String from)
	{
		Say2 cs = new Say2(0, ChatType.CRITICAL_ANNOUNCE, "", from + ": " + text, null);
		for(Player player : GameObjectsStorage.getPlayers())
			player.sendPacket(cs);
	}

	public void announceToAllFromStringHolder(String add, Object... arg)
	{
		for(Player player : GameObjectsStorage.getPlayers())
			player.sendPacket(new Say2(0, ChatType.ANNOUNCEMENT, "", String.format(StringHolder.getInstance().getString(add, player), arg), null));
	}

	public void announceToAll(CustomMessage cm)
	{
		for(Player player : GameObjectsStorage.getPlayers())
			player.sendPacket(cm);
	}

	public void announceToAll(CustomMessage cm, ChatType type)
	{
		for(Player player : GameObjectsStorage.getPlayers())
		{
			Say2 cs = new Say2(0, type, "", cm.toString(player), null);
			player.sendPacket(cs);
		}
	}

	public void announceToAll(SystemMessage sm)
	{
		for(Player player : GameObjectsStorage.getPlayers())
			player.sendPacket(sm);
	}

	public void announceToAll(IBroadcastPacket sm)
	{
		for(Player player : GameObjectsStorage.getPlayers())
			player.sendPacket(sm);
	}
	public void announceByCustomMessage(String address, String[] replacements)
	{
		for(Player player : GameObjectsStorage.getPlayers())
			announceToPlayerByCustomMessage(player, address, replacements);
	}

	public void announceByCustomMessage(String address, String[] replacements, ChatType type)
	{
		for(Player player : GameObjectsStorage.getPlayers()) {
			if(!canAnnounce(player)) {
				continue;
			}
			announceToPlayerByCustomMessage(player, address, replacements, type);
		}
	}

	private boolean canAnnounce(Player player) {
		if(player == null) {
			return false;
		}
		if(player.isInOfflineMode()) {
			return false;
		}
		if(player.isInStoreBuff()) {
			return false;
		}
		if(player.isInStoreMode()) {
			return false;
		}
		if(player.getNetConnection().getHWID() == null) {
			return false;
		}
		if(player.getNetConnection().getIpAddr() == null) {
			return false;
		}
		return true;
	}

	public void announceToPlayerByCustomMessage(Player player, String address, String[] replacements)
	{
		CustomMessage cm = new CustomMessage(address);
		if(replacements != null)
			for(String s : replacements)
				cm.addString(s);
		player.sendPacket(new Say2(0, ChatType.ANNOUNCEMENT, "", cm.toString(player)));
	}

	public void announceToPlayerByCustomMessage(Player player, String address, String[] replacements, ChatType type)
	{
		CustomMessage cm = new CustomMessage(address);
		if(replacements != null)
			for(String s : replacements)
				cm.addString(s);
		player.sendPacket(new Say2(0, type, "", cm.toString(player)));
	}
}