//package com.l2cccp.gameserver;
//
//import java.io.FileOutputStream;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//import java.util.Map.Entry;
//
//import org.dom4j.DocumentHelper;
//import org.dom4j.Element;
//import org.dom4j.io.OutputFormat;
//import org.dom4j.io.XMLWriter;
//
//import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
//import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
//import com.l2cccp.gameserver.model.quest.Quest;
//import com.l2cccp.gameserver.model.quest.QuestEventType;
//import com.l2cccp.gameserver.utils.Language;
//
///**
// * @author L2CCCP
// * @site http://l2cccp.com/
// */
//public class QuestSaver implements Runnable
//{
//	private Quest _quest;
//
//	public QuestSaver(Quest quest)
//	{
//		_quest = quest;
//	}
//
//	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
//	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");
//
//	public static final int PARTY_NONE = 0;
//	public static final int PARTY_ONE = 1;
//	public static final int PARTY_ALL = 2;
//	public static final int PARTY_ANY = 3; // Специальный тип аналогичный PARTY_ONE, но вызывается если в пати есть хотя бы кто-то с квестом. Используется в квесте 350 (прокачка СА)
//
//	private void save() throws InstantiationException, IllegalAccessException
//	{
//		org.dom4j.Document document = DocumentHelper.createDocument();
//		document.addDocType("quests", null, "quests.dtd");
//		Date date = new Date();
//		document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
//		document.addComment(" Site: http://l2cccp.com/ ");
//		document.addComment(" Примечание: Часть данных не обрабатывается! ");
//
//		Element element = document.addElement("quests");
//		if(_quest.getId() == 704)
//			element.addComment(" Не понятно что это за квест! По оффу его нет! ");
//		else if(_quest.getId() == 1103 || _quest.getId() == 1102)
//			element.addComment(" Какая то хрень ");
//		Element general = element.addElement("quest");
//		general.addAttribute("id", String.valueOf(_quest.getId()));
//		String name = _quest.getCustomName().toString(Language.ENGLISH);
//
//		switch(_quest.getParty())
//		{
//			case PARTY_NONE:
//				general.addAttribute("type", "SOLO");
//				break;
//			case PARTY_ONE:
//				general.addAttribute("type", "PARTY_ONE");
//				break;
//			case PARTY_ALL:
//				general.addAttribute("type", "PARTY_ALL");
//				break;
//			case PARTY_ANY:
//				general.addAttribute("type", "PARTY_ANY");
//				break;
//		}
//
//		Element data = general.addElement("name");
//		data.addAttribute("lang", Language.ENGLISH.name());
//		if(_quest.getId() == 255)
//			data.addAttribute("val", "Tutorial");
//		else if(_quest.getId() == 999)
//			data.addAttribute("val", "T1 Tutorial");
//		else if(_quest.getId() == 704)
//			data.addAttribute("val", "Miss Queen");
//		else if(_quest.getId() == 1201)
//			data.addAttribute("val", "Dark Cloud Mansion");
//		else if(_quest.getId() == 1102)
//			data.addAttribute("val", "Wizard Nottingale");
//		else if(_quest.getId() == 1103)
//			data.addAttribute("val", "Oracle Teleport");
//		else
//			data.addAttribute("val", name);
//
//		data = general.addElement("name");
//		data.addAttribute("lang", Language.RUSSIAN.name());
//		if(_quest.getId() == 255)
//			data.addAttribute("val", "Учебник");
//		else if(_quest.getId() == 999)
//			data.addAttribute("val", "T1 Учебник");
//		else if(_quest.getId() == 704)
//			data.addAttribute("val", "Принцесса");
//		else if(_quest.getId() == 1201)
//			data.addAttribute("val", "Темный Особняк");
//		else if(_quest.getId() == 1102)
//			data.addAttribute("val", "Чародей Нотингейль");
//		else if(_quest.getId() == 1103)
//			data.addAttribute("val", "Телепорт Оракула");
//		else
//			data.addAttribute("val", _quest.getCustomName().toString(Language.RUSSIAN));
//
//		data = general.addElement("setting");
//
//		Element rate = data.addElement("rate");
//		rate.addAttribute("drop", "1.0");
//		rate.addAttribute("reward", "1.0");
//		rate.addAttribute("exp", "1.0");
//		rate.addAttribute("sp", "1.0");
//
//		for(Entry<QuestEventType, List<Integer>> map : _quest.map.entrySet())
//		{
//			for(int id : map.getValue())
//			{
//				if(map.getKey() == QuestEventType.QUEST_ITEM)
//					continue;
//				Element event = data.addElement("event");
//				event.addAttribute("type", map.getKey().name());
//				event.addAttribute("val", String.valueOf(id));
//				data.addComment(" " + NpcHolder.getInstance().getTemplate(id).getName() + " ");
//			}
//		}
//		
//		for(Entry<QuestEventType, List<Integer>> map : _quest.map.entrySet())
//		{
//			for(int id : map.getValue())
//			{
//				if(map.getKey() != QuestEventType.QUEST_ITEM)
//					continue;
//				
//				Element event = data.addElement("item");
//				event.addAttribute("id", String.valueOf(id));
//				data.addComment(" " + ItemHolder.getInstance().getTemplate(id).getName() + " ");
//			}
//		}
//
//		try
//		{
//			if(_quest.getId() == 255)
//				name = "Tutorial";
//			else if(_quest.getId() == 704)
//				name = "Miss Queen";
//			else if(_quest.getId() == 999)
//				name = "T1 Tutorial";
//			else if(_quest.getId() == 1201)
//				name = "Dark Cloud Mansion";
//			else if(_quest.getId() == 1102)
//				name = "Wizard Nottingale";
//			else if(_quest.getId() == 1103)
//				name = "Oracle Teleport";
//
//			String path = "A:\\files\\quests\\[" + _quest.getId() + "] " + name.replace("\\", " ").replace("?", "").replace("/", " ") + ".xml";
//
//			OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
//			prettyPrint.setIndent("\t");
//			XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
//			xmlWriter.write(document);
//			xmlWriter.close();
//		}
//		catch(Exception e1)
//		{
//			e1.printStackTrace();
//		}
//	}
//
//	@Override
//	public void run()
//	{
//		try
//		{
//			save();
//		}
//		catch(InstantiationException e)
//		{
//			e.printStackTrace();
//		}
//		catch(IllegalAccessException e)
//		{
//			e.printStackTrace();
//		}
//	}
//}
