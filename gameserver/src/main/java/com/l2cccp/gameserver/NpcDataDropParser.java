package com.l2cccp.gameserver;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.napile.primitive.maps.IntObjectMap;
import org.napile.primitive.maps.impl.HashIntObjectMap;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.gameserver.data.client.holder.ItemNameLineHolder;
import com.l2cccp.gameserver.data.client.holder.NpcNameLineHolder;
import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
import com.l2cccp.gameserver.model.Skill.SkillType;
import com.l2cccp.gameserver.model.TeleportLocation;
import com.l2cccp.gameserver.model.base.ClassId;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.templates.client.NpcNameLine;
import com.l2cccp.gameserver.templates.npc.AbsorbInfo;
import com.l2cccp.gameserver.templates.npc.Faction;
import com.l2cccp.gameserver.templates.npc.MinionData;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class NpcDataDropParser
{
	private static List<String> items;
	private static List<String> npcsname;
	private static List<String> skills;
	private static List<String> skillnames;
	private static List<Integer> notdone = new ArrayList<Integer>();
	private static Map<Integer, NpcDrop> drops = new HashMap<Integer, NpcDrop>();
	private static List<Npc> npcs = new ArrayList<Npc>();

	public static void main(String[] args) throws IOException
	{
		final String path = "A:/files/pts/npcdata.txt";
		List<String> list = FileUtils.readLines(new File(path));
		items = FileUtils.readLines(new File("A:/files/pts/itemdata.txt"));
		npcsname = FileUtils.readLines(new File(path));
		skills = FileUtils.readLines(new File("A:/files/pts/skilldata.txt"));
		skillnames = FileUtils.readLines(new File("A:/files/pts/skillname.txt"));

		List<NpcDrop> droplist = new ArrayList<NpcDrop>();
		for(final String line : list)
		{
			final String[] data = line.split("\t");
			final int id = Integer.parseInt(data[2]);
			final String name = data[3];
			final Npc npc = new Npc(id, name);
			for(final String str : data)
			{

				if(str.contains("="))
				{
					final String[] arr = str.split("=");
					npc.params.put(arr[0], arr[1].replace("{", "").replace("}", ""));
				}
			}
			npcs.add(npc);

			final NpcDrop drop = new NpcDrop(id, name);
			drop.setSpoil(new Spoil(data[73]));
			drop.setNotRatedNoGrouped(new Spoil(data[74]));
			drop.setDrop(new Drop(data[75]));
			drop.setDropNotRated(new Drop(data[76]));
			droplist.add(drop);
		}

		Collections.sort(npcs, new Comp2());
		Collections.sort(droplist, new Comp());
		for(final NpcDrop drop : droplist)
			drops.put(drop.id, drop);

		reparse();
	}

	static class Comp2 implements Comparator<Npc>
	{
		@Override
		public int compare(Npc o1, Npc o2)
		{
			return o1.id - o2.id;
		}
	}

	static class Comp implements Comparator<NpcDrop>
	{
		@Override
		public int compare(NpcDrop o1, NpcDrop o2)
		{
			return o1.id - o2.id;
		}
	}

	static class Npc
	{
		final int id;
		final String name;
		protected final MultiValueSet<String> params;

		public Npc(final int id, final String name)
		{
			this.id = id;
			this.name = name;
			this.params = new MultiValueSet<String>();
		}
	}

	private static final String delim = "{}[]";

	static class NpcDrop
	{
		final int id;
		final String name;
		Spoil SWEEP;
		Spoil NOT_RATED_NOT_GROUPED;
		Drop RATED_GROUPED;
		Drop NOT_RATED_GROUPED;

		public NpcDrop(final int id, final String name)
		{
			this.id = id;
			this.name = name;
		}

		public void setSpoil(final Spoil spoil)
		{
			this.SWEEP = spoil;
		}

		public void setDrop(final Drop drop)
		{
			this.RATED_GROUPED = drop;
		}

		public void setDropNotRated(final Drop drop)
		{
			this.NOT_RATED_GROUPED = drop;
		}

		public void setNotRatedNoGrouped(final Spoil drop)
		{
			this.NOT_RATED_NOT_GROUPED = drop;
		}
	}

	static class Spoil
	{
		final String data;
		final List<String> lines;

		public Spoil(final String data)
		{
			this.data = data;
			this.lines = new ArrayList<String>();
			parse();
		}

		private void parse()
		{
			StringTokenizer st = new StringTokenizer(data, delim);
			String name = "none", id = "none", min = "none", max = "none", chance = "none";
			while(st.hasMoreTokens())
			{
				final String token = st.nextToken();
				if(!token.equals("corpse_make_list=") && !token.equals("additional_make_list=") && !token.equals(";"))
				{
					name = token;
					id = getItemId(token);
					final String[] info = st.nextToken().split(";");
					min = info[1];
					max = info[2];
					chance = info[3];
					addLine(id, min, max, chance, name);
				}
			}
		}

		private void addLine(final String... args)
		{
			final String line = args[0] + "\t" + args[1] + "\t" + args[2] + "\t" + args[3] + "\t" + args[4];
			lines.add(line);
		}
	}

	static class Drop
	{
		final String drop;
		final List<Group> groups;

		public Drop(final String drop)
		{
			this.drop = drop;
			this.groups = new ArrayList<Group>();
			parse();
		}

		private void parse()
		{
			final List<String> lines = new ArrayList<String>();
			StringTokenizer st = new StringTokenizer(drop, delim);
			String name = "none", id = "none";
			while(st.hasMoreTokens())
			{
				String token = st.nextToken();
				if(!token.equals("additional_make_multi_list=") && !token.equals("ex_item_drop_list=") && !token.equals(";"))
				{
					final int size = token.split(";").length;
					if(!token.contains(";"))
					{
						name = token;
						id = getItemId(token);
					}
					else if(size > 2)
					{
						final String[] info = token.split(";");
						lines.add(id + "\t" + info[1] + "\t" + info[2] + "\t" + info[3] + "\t" + name);
					}
					else if(size == 2)
					{
						final String chance = token.split(";")[1];
						final Group g = new Group(chance, new ArrayList<String>(lines));
						groups.add(g);
						lines.clear();
					}
				}
			}
		}
	}

	static class Group
	{
		final String chance;
		final List<String> lines;

		public Group(final String chance, final List<String> lines)
		{
			this.chance = chance;
			this.lines = lines;
		}
	}

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");
	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

	public static void reparse()
	{
		int min = 0;
		while((min += 100) < 40000)
		{
			boolean parse = false;
			int max = min + 99;
			org.dom4j.Document document = DocumentHelper.createDocument();
			document.addDocType("list", null, "npc.dtd");
			Date date = new Date();
			document.addComment(" Author: L2CCCP, Date: " + DATE_FORMAT.format(date) + ", Time: " + TIME_FORMAT.format(date) + " ");
			document.addComment(" Site: http://l2cccp.com/ ");

			Element element = document.addElement("list");

			for(Npc npc : npcs)
			{
				final int id = npc.id;
				if(id < min || id > max)
					continue;

				Element data = element.addElement("npc");
				data.addAttribute("id", String.valueOf(id));
				data.addAttribute("name", NpcNameLineHolder.getInstance().get(Language.ENGLISH, id).getName());
				String title = null;
				if((title = NpcNameLineHolder.getInstance().get(Language.ENGLISH, id).getTitle()) == null)
					title = "";
				data.addAttribute("title", title);
				final NpcTemplate template = NpcHolder.getInstance().getTemplate(id);
				if(template == null)
					continue;
				final MultiValueSet<String> param = npc.params;
				Element set = data.addElement("set");
				if(template.displayId != 0)
				{
					set.addAttribute("name", "displayId");
					set.addAttribute("value", String.valueOf(template.displayId));
					set = data.addElement("set");
				}

				set.addAttribute("name", "aggroRange");
				set.addAttribute("value", param.getString("agro_range"));

				set = data.addElement("set");
				set.addAttribute("name", "ai_type");
				set.addAttribute("value", template.ai_type);

				set = data.addElement("set");
				set.addAttribute("name", "baseAtkRange");
				set.addAttribute("value", param.getString("base_attack_range"));

				set = data.addElement("set");
				set.addAttribute("name", "baseAtkType");
				set.addAttribute("value", param.getString("base_attack_type").toUpperCase());

				set = data.addElement("set");
				set.addAttribute("name", "baseCON");
				set.addAttribute("value", param.getString("con"));

				set = data.addElement("set");
				set.addAttribute("name", "baseCritRate");
				set.addAttribute("value", String.valueOf(param.getInteger("base_critical") * 10));

				set = data.addElement("set");
				set.addAttribute("name", "baseDEX");
				set.addAttribute("value", param.getString("dex"));

				set = data.addElement("set");
				set.addAttribute("name", "baseHpMax");
				set.addAttribute("value", param.getString("org_hp"));

				set = data.addElement("set");
				set.addAttribute("name", "baseHpRate");
				set.addAttribute("value", param.getString("s_npc_prop_hp_rate"));
				set = data.addElement("set");
				set.addAttribute("name", "baseHpReg");
				set.addAttribute("value", param.getString("org_hp_regen"));
				set = data.addElement("set");
				set.addAttribute("name", "baseINT");
				set.addAttribute("value", param.getString("int"));
				set = data.addElement("set");
				set.addAttribute("name", "baseMAtk");
				set.addAttribute("value", param.getString("base_magic_attack"));
				set = data.addElement("set");
				set.addAttribute("name", "baseMAtkSpd");
				set.addAttribute("value", param.getString("base_attack_speed"));
				set = data.addElement("set");
				set.addAttribute("name", "baseMDef");
				set.addAttribute("value", param.getString("base_magic_defend"));
				set = data.addElement("set");
				set.addAttribute("name", "baseMEN");
				set.addAttribute("value", param.getString("men"));
				set = data.addElement("set");
				set.addAttribute("name", "baseMpMax");
				set.addAttribute("value", param.getString("org_mp"));
				set = data.addElement("set");
				set.addAttribute("name", "baseMpReg");
				set.addAttribute("value", param.getString("org_mp_regen"));
				set = data.addElement("set");
				set.addAttribute("name", "basePAtk");
				set.addAttribute("value", param.getString("base_physical_attack"));
				set = data.addElement("set");
				set.addAttribute("name", "basePAtkSpd");
				set.addAttribute("value", param.getString("base_attack_speed"));
				set = data.addElement("set");
				set.addAttribute("name", "basePDef");
				set.addAttribute("value", param.getString("base_defend"));
				set = data.addElement("set");
				set.addAttribute("name", "baseRunSpd");
				set.addAttribute("value", param.getString("ground_high").split(";")[0]);
				set = data.addElement("set");
				set.addAttribute("name", "baseSTR");
				set.addAttribute("value", param.getString("str"));
				set = data.addElement("set");
				set.addAttribute("name", "baseShldDef");
				set.addAttribute("value", param.getString("shield_defense"));
				set = data.addElement("set");
				set.addAttribute("name", "baseShldRate");
				set.addAttribute("value", param.getString("shield_defense_rate"));
				set = data.addElement("set");
				set.addAttribute("name", "baseWIT");
				set.addAttribute("value", param.getString("wit"));
				set = data.addElement("set");
				set.addAttribute("name", "baseWalkSpd");
				set.addAttribute("value", param.getString("ground_low").split(";")[0]);
				set = data.addElement("set");
				set.addAttribute("name", "collision_height");
				set.addAttribute("value", param.getString("collision_height").split(";")[0]);
				set = data.addElement("set");
				set.addAttribute("name", "collision_radius");
				set.addAttribute("value", param.getString("collision_radius").split(";")[0]);
				set = data.addElement("set");
				set.addAttribute("name", "corpse_time");
				set.addAttribute("value", param.getString("corpse_time"));
				set = data.addElement("set");
				set.addAttribute("name", "level");
				int level = param.getInteger("level");
				set.addAttribute("value", param.getString("level"));
				set = data.addElement("set");
				set.addAttribute("name", "race");
				set.addAttribute("value", param.getString("race").toUpperCase());
				set = data.addElement("set");
				set.addAttribute("name", "rewardExp");
				double acquire_exp_rate = param.getDouble("acquire_exp_rate");
				set.addAttribute("value", String.valueOf((int) Math.round(acquire_exp_rate * (Math.pow(level, 2) * 0.076923))));
				set = data.addElement("set");
				set.addAttribute("name", "rewardRp");
				set.addAttribute("value", param.getString("acquire_rp"));
				set = data.addElement("set");
				set.addAttribute("name", "rewardSp");
				set.addAttribute("value", param.getString("acquire_sp"));
				set = data.addElement("set");
				set.addAttribute("name", "soulshot_count");
				set.addAttribute("value", param.getString("soulshot_count"));
				set = data.addElement("set");
				set.addAttribute("name", "spiritshot_count");
				set.addAttribute("value", param.getString("spiritshot_count"));
				set = data.addElement("set");
				set.addAttribute("name", "type");
				set.addAttribute("value", template.type);
				if(template.isRaid)
				{
					set = data.addElement("set");
					set.addAttribute("name", "isRaid");
					set.addAttribute("value", "true");
				}
				boolean slot_rhand = !param.getString("slot_rhand").equals("[]"), slot_lhand = !param.getString("slot_lhand").equals("[]");
				if(slot_rhand || slot_lhand)
				{
					Element equip = data.addElement("equip");
					if(slot_rhand)
					{
						Element params = equip.addElement("rhand");
						params.addAttribute("item_id", getItemId(param.getString("slot_rhand")));
						equip.addComment(" " + ItemNameLineHolder.getInstance().get(Language.ENGLISH, Integer.parseInt(getItemId(param.getString("slot_rhand")))).getName() + " ");
					}

					if(slot_lhand)
					{
						Element params = equip.addElement("lhand");
						params.addAttribute("item_id", getItemId(param.getString("slot_lhand")));
						equip.addComment(" " + ItemNameLineHolder.getInstance().get(Language.ENGLISH, Integer.parseInt(getItemId(param.getString("slot_lhand")))).getName() + " ");
					}
				}

				if(!template.getAIParams().isEmpty())
				{
					Element ai_params = data.addElement("ai_params");
					for(final Entry<String, Object> info : template.getAIParams().entrySet())
					{
						Element params = ai_params.addElement("set");
						params.addAttribute("name", info.getKey());
						params.addAttribute("value", String.valueOf(info.getValue()));
					}
				}

				if(!template.getMinionData().isEmpty())
				{
					Element minions = data.addElement("minions");
					for(final MinionData minion : template.getMinionData())
					{
						Element obj = minions.addElement("minion");
						obj.addAttribute("npc_id", String.valueOf(minion.getMinionId()));
						obj.addAttribute("count", String.valueOf(minion.getAmount()));
						minions.addComment(" " + NpcNameLineHolder.getInstance().get(Language.ENGLISH, minion.getMinionId()).getName() + " ");
					}
				}

				if(template.getFaction() == Faction.NONE && !param.getString("clan").isEmpty())
				{
					Element factio = data.addElement("faction");
					factio.addAttribute("name", param.getString("clan").replace("@", ""));
					factio.addAttribute("range", param.getString("clan_help_range"));

					if(!param.getString("ignore_clan_list").isEmpty())
					{
						for(final String ignored : param.getString("ignore_clan_list").split(";"))
						{
							Element ignore = factio.addElement("ignore");
							final String npc_igid = getNpcId(ignored.replace("@", ""));
							ignore.addAttribute("npc_id", npc_igid);

							final NpcNameLine line = NpcNameLineHolder.getInstance().get(Language.ENGLISH, Integer.parseInt(npc_igid));
							factio.addComment(" " + (line == null ? "WTF" : line.getName()) + " ");
						}
					}
				}
				else
				{
					final Faction faction = template.getFaction();
					if(faction != Faction.NONE ^ true)
					{
						Element factio = data.addElement("faction");
						factio.addAttribute("name", faction.toString().replace("{", "").replace("}", "").replace(",", ";"));
						factio.addAttribute("range", String.valueOf(template.getFaction().factionRange));
						if(faction.ignoreId.size() > 0)
						{
							for(final int ignored : faction.ignoreId.toArray())
							{
								Element ignore = factio.addElement("ignore");
								ignore.addAttribute("npc_id", String.valueOf(ignored));

								final NpcNameLine line = NpcNameLineHolder.getInstance().get(Language.ENGLISH, ignored);
								factio.addComment(" " + (line == null ? "WTF" : line.getName()) + " ");
							}
						}
					}
				}

				String skill_list = param.getString("skill_list");
				if(!skill_list.isEmpty() && !skill_list.equals("{}") || template.getSkills().size() > 0)
				{
					skill_list = skill_list.replace("@", "");

					Element skills = data.addElement("skills");
					final IntObjectMap<SkillEntry> skills_list = new HashIntObjectMap<SkillEntry>(template.getSkills());

					for(final String skill_name : skill_list.split(";"))
					{
						final Skill sk = getSkill(skill_name);
						if(skill_name.isEmpty())
						{
							System.out.println("Skill list = " + param.getString("skill_list"));
							continue;
						}

						final SkillEntry ski = skills_list.get(Integer.parseInt(sk.id));
						if(ski != null)
						{
							if(ski.getLevel() != Integer.parseInt(sk.level))
								System.out.println("Incorrect level for skill " + ski.getId() + " info: " + ski.getLevel() + ":" + sk.level + " -> " + id);

							skills_list.remove(ski.getId());
						}
					}

					for(final String skill_name : skill_list.split(";"))
					{
						final Skill sk = getSkill(skill_name);
						if(skill_name.isEmpty())
						{
							System.out.println("Skill list = " + param.getString("skill_list"));
							continue;
						}

						Element skill = skills.addElement("skill");
						skill.addAttribute("id", sk.id);
						skill.addAttribute("level", sk.level);
						try
						{
							final SkillEntry object = SkillTable.getInstance().getSkillEntry(Integer.parseInt(sk.id), Integer.parseInt(sk.level));
							if(object.getSkillType() == SkillType.NOTDONE && !notdone.contains(Integer.parseInt(sk.id)))
								notdone.add(Integer.parseInt(sk.id));

							skills.addComment(" " + getSkillName(sk.id, sk.level) + " ");
						}
						catch(Exception e)
						{
							System.out.println("Can't find skill " + sk.id);
						}
					}

					for(final SkillEntry ski : skills_list.values())
					{
						Element skill = skills.addElement("skill");
						skill.addAttribute("id", String.valueOf(ski.getId()));
						skill.addAttribute("level", String.valueOf(ski.getLevel()));

						if(ski.getSkillType() == SkillType.NOTDONE && !notdone.contains(ski.getId()))
							notdone.add(ski.getId());

						skills.addComment(" " + getSkillName(String.valueOf(ski.getId()), String.valueOf(ski.getLevel())) + " ");
					}
				}
				if(!template.getTeleportList().isEmpty())
				{
					Element teleportlist = data.addElement("teleportlist");

					for(final int s : template.getTeleportList().keys())
					{
						Element sublist = teleportlist.addElement("sublist");
						sublist.addAttribute("id", String.valueOf(s));
						for(final TeleportLocation tp : template.getTeleportList(s))
						{
							Element target = sublist.addElement("target");
							target.addAttribute("loc", tp.x + " " + tp.y + " " + tp.z + (tp.h != 0 ? " " + tp.h : ""));
							target.addAttribute("item_id", String.valueOf(tp.getItem()));
							target.addAttribute("price", String.valueOf(tp.getPrice()));
							target.addAttribute("name", String.valueOf(tp.getName()));
							if(tp.getCastleId() != 0)
								target.addAttribute("castle_id", String.valueOf(tp.getCastleId()));
						}
					}
				}

				if(!template.getTeachInfo().isEmpty())
				{
					Element teach_classes = data.addElement("teach_classes");
					for(final ClassId info : template.getTeachInfo())
					{
						Element cl = teach_classes.addElement("class");
						cl.addAttribute("id", String.valueOf(info.getId()));
						teach_classes.addComment(" " + Util.className(Language.ENGLISH, info.getId()));
					}
				}

				if(!template.getAbsorbInfo().isEmpty())
				{
					Element absorblist = data.addElement("absorblist");
					for(final AbsorbInfo info : template.getAbsorbInfo())
					{
						Element absorb = absorblist.addElement("absorb");
						absorb.addAttribute("chance", String.valueOf(info.getChance()));
						if(info.getCursedChance() > 0)
							absorb.addAttribute("cursed_chance", String.valueOf(info.getCursedChance()));
						absorb.addAttribute("min_level", String.valueOf(info.min));
						absorb.addAttribute("max_level", String.valueOf(info.max));
						if(info.isSkill())
							absorb.addAttribute("skill", String.valueOf(true));
						absorb.addAttribute("type", String.valueOf(info.getAbsorbType().name()));
					}
				}

				parse = true;
				//				set.addAttribute("name", "0000000");
				//				set.addAttribute("value", param.getString("0000000"));

				boolean drop = false;

				final NpcDrop collection = drops.get(id);
				if(collection.RATED_GROUPED.groups.size() > 0)
					drop = true;
				if(collection.NOT_RATED_GROUPED.groups.size() > 0)
					drop = true;
				if(collection.NOT_RATED_NOT_GROUPED.lines.size() > 0)
					drop = true;
				if(collection.SWEEP.lines.size() > 0)
					drop = true;

				if(drop)
				{
					if(collection.RATED_GROUPED.groups.size() > 0)
					{
						Element rewardlist = data.addElement("rewardlist");
						rewardlist.addAttribute("type", "RATED_GROUPED");
						for(Group g : collection.RATED_GROUPED.groups)
						{
							Element group = rewardlist.addElement("group");
							group.addAttribute("chance", g.chance);
							for(String line : g.lines)
							{
								final String[] array = line.split("\t");
								Element reward = group.addElement("reward");
								reward.addAttribute("item_id", array[0]);
								reward.addAttribute("min", array[1]);
								reward.addAttribute("max", array[2]);
								reward.addAttribute("chance", array[3]);
								group.addComment(" " + ItemNameLineHolder.getInstance().get(Language.ENGLISH, Integer.parseInt(array[0])).getName() + " ");
							}
						}
					}
					if(collection.NOT_RATED_GROUPED.groups.size() > 0)
					{
						Element rewardlist = data.addElement("rewardlist");
						rewardlist.addAttribute("type", "NOT_RATED_GROUPED");
						for(Group g : collection.NOT_RATED_GROUPED.groups)
						{
							Element group = rewardlist.addElement("group");
							group.addAttribute("chance", g.chance);
							for(String line : g.lines)
							{
								final String[] array = line.split("\t");
								Element reward = group.addElement("reward");
								reward.addAttribute("item_id", array[0]);
								reward.addAttribute("min", array[1]);
								reward.addAttribute("max", array[2]);
								reward.addAttribute("chance", array[3]);
								group.addComment(" " + ItemNameLineHolder.getInstance().get(Language.ENGLISH, Integer.parseInt(array[0])).getName() + " ");
							}
						}
					}
					if(collection.NOT_RATED_NOT_GROUPED.lines.size() > 0)
					{
						Element rewardlist = data.addElement("rewardlist");
						rewardlist.addAttribute("type", "NOT_RATED_NOT_GROUPED");
						for(String line : collection.NOT_RATED_NOT_GROUPED.lines)
						{
							final String[] array = line.split("\t");
							Element reward = rewardlist.addElement("reward");
							reward.addAttribute("item_id", array[0]);
							reward.addAttribute("min", array[1]);
							reward.addAttribute("max", array[2]);
							reward.addAttribute("chance", array[3]);
							rewardlist.addComment(" " + ItemNameLineHolder.getInstance().get(Language.ENGLISH, Integer.parseInt(array[0])).getName() + " ");
						}
					}

					if(collection.SWEEP.lines.size() > 0)
					{
						Element rewardlist = data.addElement("rewardlist");
						rewardlist.addAttribute("type", "SWEEP");
						for(String line : collection.SWEEP.lines)
						{
							final String[] array = line.split("\t");
							Element reward = rewardlist.addElement("reward");
							reward.addAttribute("item_id", array[0]);
							reward.addAttribute("min", array[1]);
							reward.addAttribute("max", array[2]);
							reward.addAttribute("chance", array[3]);
							rewardlist.addComment(" " + ItemNameLineHolder.getInstance().get(Language.ENGLISH, Integer.parseInt(array[0])).getName() + " ");
						}
					}
				}

				Element attributes = data.addElement("attributes");
				if(!param.getString("base_attribute_attack").contains("none"))
				{
					Element defence = attributes.addElement("attack");
					defence.addAttribute("attribute", com.l2cccp.gameserver.model.base.Element.getElementByName(param.getString("base_attribute_attack").split(";")[0]).name().toLowerCase());
					defence.addAttribute("value", param.getString("base_attribute_attack").split(";")[1]);
				}
				final String[] attr = param.getString("base_attribute_defend").split(";");

				for(int i = 0; i < 6; i++)
				{
					Element defence = attributes.addElement("defence");
					defence.addAttribute("attribute", com.l2cccp.gameserver.model.base.Element.getElementById(i).name().toLowerCase());
					defence.addAttribute("value", attr[i]);
				}
			}

			if(!parse)
				continue;

			try
			{
				String path = "A:/files/pts/npc/" + min + "-" + max + ".xml";

				OutputFormat prettyPrint = OutputFormat.createPrettyPrint();
				prettyPrint.setIndent("\t");
				XMLWriter xmlWriter = new XMLWriter(new FileOutputStream(path), prettyPrint);
				xmlWriter.write(document);
				xmlWriter.close();
			}
			catch(Exception e1)
			{
				e1.printStackTrace();
			}
		}

		System.out.println("NotDone skill list: ###################");
		for(int id : notdone)
			System.out.println("Skill: " + id);
	}

	static Map<String, Map<String, String>> names;

	private static String getSkillName(final String id, final String level)
	{
		if(names == null)
		{
			names = new HashMap<String, Map<String, String>>();
			for(final String line : skillnames)
			{
				final String[] data = line.split("\t");
				if(!names.containsKey(data[0]))
					names.put(data[0], new HashMap<String, String>());
				names.get(data[0]).put(data[1], data[2]);
			}
		}

		return names.get(id).get(level);
	}

	static Map<String, String> map;

	private static String getItemId(String npc_name)
	{
		if(map == null)
		{
			map = new HashMap<String, String>();

			for(String line : items)
			{
				String[] array = line.split("	");
				map.put(array[3], array[2]);
			}
		}

		final String key = "[" + npc_name.replace("[", "").replace("]", "") + "]";
		if(!map.containsKey(key))
			System.out.println("Can't find " + npc_name);

		return map.get(key);
	}

	static Map<String, Skill> map2;

	static class Skill
	{
		public String id;
		public String level;

		public Skill(String id, String level)
		{
			this.id = id;
			this.level = level;
		}
	}

	private static Skill getSkill(String skill_name)
	{
		if(map2 == null)
		{
			map2 = new HashMap<String, Skill>();

			for(String line : skills)
			{
				String[] array = line.split("	");
				final Skill skill = new Skill(array[3].replace("skill_id = ", ""), array[4].replace("level = ", ""));
				map2.put(array[1].replace("skill_name = ", ""), skill);
			}
		}

		final String key = "[" + skill_name + "]";
		if(!map2.containsKey(key))
			System.out.println("Can't find " + key);

		return map2.get(key);
	}

	static Map<String, String> map3;

	private static String getNpcId(String npc_name)
	{
		if(map3 == null)
		{
			map3 = new HashMap<String, String>();

			for(String line : npcsname)
			{
				String[] array = line.split("	");
				map3.put(array[3], array[2]);
			}
		}

		final String key = "[" + npc_name.replace("[", "").replace("]", "") + "]";
		if(!map3.containsKey(key))
			System.out.println("Can't find " + npc_name);

		return map3.get(key);
	}
}
