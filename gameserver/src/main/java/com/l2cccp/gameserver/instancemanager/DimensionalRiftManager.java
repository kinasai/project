package com.l2cccp.gameserver.instancemanager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.RiftsHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.entity.rifts.DimensionalRift;
import com.l2cccp.gameserver.model.entity.rifts.DimensionalRiftRoom;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.TeleportToLocation;
import com.l2cccp.gameserver.utils.HtmlUtils;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Location;

public class DimensionalRiftManager
{
	private static final Logger _log = LoggerFactory.getLogger(DimensionalRiftManager.class);
	private static final DimensionalRiftManager _instance = new DimensionalRiftManager();

	private final static int DIMENSIONAL_FRAGMENT_ITEM_ID = 7079;

	public static DimensionalRiftManager getInstance()
	{
		return _instance;
	}

	public boolean checkIfInRiftZone(Location loc, boolean ignorePeaceZone)
	{
		final DimensionalRiftRoom rift = RiftsHolder.getInstance().getRoom(0, 1);
		if(ignorePeaceZone)
			return rift.checkIfInZone(loc);

		final DimensionalRiftRoom rift2 = RiftsHolder.getInstance().getRoom(0, 0);

		return rift.checkIfInZone(loc) && !rift2.checkIfInZone(loc);
	}

	public boolean checkIfInPeaceZone(Location loc)
	{
		final DimensionalRiftRoom rift = RiftsHolder.getInstance().getRoom(0, 0);
		return rift.checkIfInZone(loc);
	}

	public void teleportToWaitingRoom(Player player)
	{
		final DimensionalRiftRoom rift = RiftsHolder.getInstance().getRoom(0, 0);
		teleToLocation(player, Location.findPointToStay(rift.getTeleportCoords(), 0, 250, ReflectionManager.DEFAULT.getGeoIndex()), null);
	}

	public void start(Player player, int type, NpcInstance npc)
	{
		if(!player.isInParty())
		{
			showHtmlFile(player, "rift/NoParty.htm", npc);
			return;
		}

		if(!player.isGM())
		{
			if(!player.getParty().isLeader(player))
			{
				showHtmlFile(player, "rift/NotPartyLeader.htm", npc);
				return;
			}

			if(player.getParty().isInDimensionalRift())
			{
				showHtmlFile(player, "rift/Cheater.htm", npc);
				_log.warn("Player " + player.getName() + "(" + player.getObjectId() + ") was cheating in dimension rift area!");
				return;
			}

			if(player.getParty().getMemberCount() < Config.RIFT_MIN_PARTY_SIZE)
			{
				showHtmlFile(player, "rift/SmallParty.htm", npc);
				return;
			}

			for(Player p : player.getParty().getPartyMembers())
			{
				if(!checkIfInPeaceZone(p.getLoc()))
				{
					showHtmlFile(player, "rift/NotInWaitingRoom.htm", npc);
					return;
				}
			}

			for(Player p : player.getParty().getPartyMembers())
			{
				if(ItemFunctions.getItemCount(p, DIMENSIONAL_FRAGMENT_ITEM_ID) < getNeededItems(type))
				{
					showHtmlFile(player, "rift/NoFragments.htm", npc);
					return;
				}
			}

			for(Player p : player.getParty().getPartyMembers())
			{
				if(!ItemFunctions.deleteItem(p, DIMENSIONAL_FRAGMENT_ITEM_ID, getNeededItems(type)))
				{
					showHtmlFile(player, "rift/NoFragments.htm", npc);
					return;
				}
			}
		}
		final int room = RiftsHolder.getInstance().getRooms(type).size();
		new DimensionalRift(player.getParty(), type, Rnd.get(1, room));
	}

	private long getNeededItems(int type)
	{
		switch(type)
		{
			case 1:
				return Config.RIFT_ENTER_COST_RECRUIT;
			case 2:
				return Config.RIFT_ENTER_COST_SOLDIER;
			case 3:
				return Config.RIFT_ENTER_COST_OFFICER;
			case 4:
				return Config.RIFT_ENTER_COST_CAPTAIN;
			case 5:
				return Config.RIFT_ENTER_COST_COMMANDER;
			case 6:
				return Config.RIFT_ENTER_COST_HERO;
			default:
				return Long.MAX_VALUE;
		}
	}

	public void showHtmlFile(Player player, String file, NpcInstance npc)
	{
		HtmlMessage html = new HtmlMessage(npc);
		html.setFile(file);
		html.replace("%t_name%", HtmlUtils.htmlNpcName(npc.getNpcId()));
		player.sendPacket(html);
	}

	public static void teleToLocation(Player player, Location loc, Reflection ref)
	{
		if(player.isTeleporting() || player.isDeleted())
			return;

		player.setIsTeleporting(true);
		player.setTarget(null);
		player.stopMove();

		if(player.isInBoat())
			player.setBoat(null);

		player.breakFakeDeath();
		player.decayMe();
		player.setLoc(loc);

		if(ref == null)
			player.setReflection(ReflectionManager.DEFAULT);

		// Нужно при телепорте с более высокой точки на более низкую, иначе наносится вред от "падения"
		player.setLastClientPosition(null);
		player.setLastServerPosition(null);
		player.sendPacket(new TeleportToLocation(player, loc));

		if(player.getServitor() != null)
			player.getServitor().teleportToOwner();
	}
}