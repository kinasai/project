package com.l2cccp.gameserver.instancemanager;

import java.util.concurrent.ScheduledFuture;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.dao.CursedWeaponsDAO;
import com.l2cccp.gameserver.data.xml.holder.CursedWeaponsHolder;
import com.l2cccp.gameserver.model.CursedWeapon;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.tables.SkillTable;

public class CursedWeaponsManager
{
	private static final Logger _log = LoggerFactory.getLogger(CursedWeaponsManager.class);

	private static final CursedWeaponsManager _instance = new CursedWeaponsManager();

	public static final CursedWeaponsManager getInstance()
	{
		return _instance;
	}

	private ScheduledFuture<?> _removeTask;

	private static final int CURSEDWEAPONS_MAINTENANCE_INTERVAL = 5 * 60 * 1000; // 5 min in millisec

	public CursedWeaponsManager()
	{
		if(!Config.ALLOW_CURSED_WEAPONS)
			return;

		CursedWeaponsDAO.getInstance().restore();
		CursedWeaponsDAO.getInstance().check();

		cancelTask();
		_removeTask = ThreadPoolManager.getInstance().scheduleAtFixedRate(new RemoveTask(), CURSEDWEAPONS_MAINTENANCE_INTERVAL, CURSEDWEAPONS_MAINTENANCE_INTERVAL);
	}

	private void cancelTask()
	{
		if(_removeTask != null)
		{
			_removeTask.cancel(false);
			_removeTask = null;
		}
	}

	private class RemoveTask extends RunnableImpl
	{
		@Override
		public void runImpl() throws Exception
		{
			for(CursedWeapon cw : CursedWeaponsHolder.getInstance().getCursed())
			{
				if(cw.isActive() && cw.getTimeLeft() <= 0)
					endOfLife(cw);
			}
		}
	}

	public void endOfLife(CursedWeapon cw)
	{
		if(cw.isActivated())
		{
			Player player = cw.getOnlineOwner();
			if(player != null)
			{
				// Remove from player
				_log.info("CursedWeaponsManager: " + cw.getName() + " being removed online from " + player + ".");

				player.abortAttack(true, true);

				player.setKarma(cw.getPlayerKarma(), true);
				player.setPkKills(cw.getPlayerPkKills());
				player.setCursedWeaponEquippedId(0);
				player.setTransformation(0);
				player.setTransformationName(null);
				player.removeSkill(SkillTable.getInstance().getSkillEntry(cw.getSkillId(), player.getSkillLevel(cw.getSkillId())), false);
				player.getInventory().destroyItemByItemId(cw.getItemId(), 1L);
				player.broadcastCharInfo();
			}
			else
			{
				// Remove from Db
				_log.info("CursedWeaponsManager: " + cw.getName() + " being removed offline.");
				CursedWeaponsDAO.getInstance().remove(cw);
			}
		}
		else if(cw.getPlayer() != null && cw.getPlayer().getInventory().getItemByItemId(cw.getItemId()) != null) // either this cursed weapon is in the inventory of someone who has another cursed weapon equipped, OR this cursed weapon is on the ground.
		{
			Player player = cw.getPlayer();
			if(!cw.getPlayer().getInventory().destroyItemByItemId(cw.getItemId(), 1))
				_log.info("CursedWeaponsManager[453]: Error! Cursed weapon not found!!!");

			player.sendChanges();
			player.broadcastUserInfo(true);
		}
		else if(cw.getItem() != null) // is dropped on the ground
		{
			cw.getItem().deleteMe();
			cw.getItem().delete();
			_log.info("CursedWeaponsManager: " + cw.getName() + " item has been removed from World.");
		}

		cw.initWeapon();
		CursedWeaponsDAO.getInstance().remove(cw.getItemId());

		announce(new SystemMessage(SystemMsg.ID1818_S1_HAS_DISAPPEARED).addItemName(cw.getItemId()));
	}

	public void saveData()
	{
		for(CursedWeapon cw : CursedWeaponsHolder.getInstance().getCursed())
			CursedWeaponsDAO.getInstance().save(cw);
	}

	/**
	 * вызывается, когда проклятое оружие оказывается в инвентаре игрока
	 */
	public void checkPlayer(Player player, ItemInstance item)
	{
		if(player == null || item == null || player.isInOlympiadMode())
			return;

		CursedWeapon cw = CursedWeaponsHolder.getInstance().get(item.getItemId());
		if(cw == null)
			return;

		if(player.getObjectId() == cw.getPlayerId() || cw.getPlayerId() == 0 || cw.isDropped())
		{
			activate(player, item);
			showUsageTime(player, cw);
		}
		else
		{
			// wtf? how you get it?
			_log.warn("CursedWeaponsManager: " + player + " tried to obtain " + item + " in wrong way");
			player.getInventory().destroyItem(item, item.getCount());
		}
	}

	public void activate(Player player, ItemInstance item)
	{
		if(player == null || player.isInOlympiadMode())
			return;
		CursedWeapon cw = CursedWeaponsHolder.getInstance().get(item.getItemId());
		if(cw == null)
			return;

		if(player.isCursedWeaponEquipped()) // cannot own 2 cursed swords
		{
			if(player.getCursedWeaponEquippedId() != item.getItemId())
			{
				CursedWeapon cw2 = CursedWeaponsHolder.getInstance().get(player.getCursedWeaponEquippedId());
				cw2.setNbKills(cw2.getStageKills() - 1);
				cw2.increaseKills();
			}

			// erase the newly obtained cursed weapon
			endOfLife(cw);
			player.getInventory().destroyItem(item, 1);
		}
		else if(cw.getTimeLeft() > 0)
		{
			cw.activate(player, item);
			CursedWeaponsDAO.getInstance().save(cw);
			announce(new SystemMessage(SystemMsg.THE_OWNER_OF_S2_HAS_APPEARED_IN_THE_S1_REGION).addZoneName(player.getLoc()).addItemName(cw.getItemId()));
		}
		else
		{
			endOfLife(cw);
			player.getInventory().destroyItem(item, 1);
		}
	}

	public void doLogout(Player player)
	{
		for(CursedWeapon cw : CursedWeaponsHolder.getInstance().getCursed())
		{
			if(player.getInventory().getItemByItemId(cw.getItemId()) != null)
			{
				cw.setPlayer(null);
				cw.setItem(null);
			}
		}
	}

	/**
	 * drop from L2NpcInstance killed by L2Player
	 */
	public void dropAttackable(NpcInstance attackable, Player killer)
	{
		if(killer.isInOlympiadMode() || killer.isCursedWeaponEquipped() || CursedWeaponsHolder.getInstance().getCursed().size() == 0 || killer.getReflection() != ReflectionManager.DEFAULT)
			return;

		synchronized (CursedWeaponsHolder.getInstance().getCursed())
		{
			CursedWeapon[] cursedWeapons = new CursedWeapon[0];
			for(CursedWeapon cw : CursedWeaponsHolder.getInstance().getCursed())
			{
				if(cw.isActive())
					continue;
				cursedWeapons = ArrayUtils.add(cursedWeapons, cw);
			}

			if(cursedWeapons.length > 0)
			{
				CursedWeapon cw = cursedWeapons[Rnd.get(cursedWeapons.length)];
				if(Rnd.chance(cw.getDropRate()))
					cw.create(attackable, killer);
			}
		}
	}

	/**
	 * Выпадение оружия из владельца, или исчезновение с определенной вероятностью.
	 * Вызывается при смерти игрока.
	 */
	public void dropPlayer(Player player)
	{
		CursedWeapon cw = CursedWeaponsHolder.getInstance().get(player.getCursedWeaponEquippedId());
		if(cw == null)
			return;

		if(cw.dropIt(null, null, player))
		{
			CursedWeaponsDAO.getInstance().save(cw);
			announce(new SystemMessage(SystemMsg.S2_WAS_DROPPED_IN_THE_S1_REGION).addZoneName(player.getLoc()).addItemName(cw.getItemId()));
		}
		else
			endOfLife(cw);
	}

	public void increaseKills(int itemId)
	{
		CursedWeapon cw = CursedWeaponsHolder.getInstance().get(itemId);
		if(cw != null)
		{
			cw.increaseKills();
			CursedWeaponsDAO.getInstance().save(cw);
		}
	}

	public int getLevel(int itemId)
	{
		CursedWeapon cw = CursedWeaponsHolder.getInstance().get(itemId);
		return cw != null ? cw.getLevel() : 0;
	}

	public void announce(SystemMessage sm)
	{
		for(Player player : GameObjectsStorage.getPlayers())
			player.sendPacket(sm);
	}

	public void showUsageTime(Player player, int itemId)
	{
		CursedWeapon cw = CursedWeaponsHolder.getInstance().get(itemId);
		if(cw != null)
			showUsageTime(player, cw);
	}

	public void showUsageTime(Player player, CursedWeapon cw)
	{
		SystemMessage sm = new SystemMessage(SystemMsg.S1_HAS_S2_MINUTES_OF_USAGE_TIME_REMAINING);
		sm.addItemName(cw.getItemId());
		sm.addNumber(new Long(cw.getTimeLeft() / 60000).intValue());
		player.sendPacket(sm);
	}
}