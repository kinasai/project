package com.l2cccp.gameserver.instancemanager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.listener.actor.OnDeathListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.utils.ReflectionUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class HellboundManager
{
	private static final Logger _log = LoggerFactory.getLogger(HellboundManager.class);
	private static HellboundManager _instance;
	private static int _initialStage;
	private static final long _taskDelay = 2 * 60 * 1000L; //30min
	private static String CONFIDENCE = "HellboundConfidence";
	DeathListener _deathListener = new DeathListener();

	public static HellboundManager getInstance()
	{
		if(_instance == null)
			_instance = new HellboundManager();
		return _instance;
	}

	public HellboundManager()
	{
		_initialStage = getHellboundLevel();
		spawnHellbound();
		doorHandler();
		ThreadPoolManager.getInstance().scheduleAtFixedRate(new StageCheckTask(), _taskDelay, _taskDelay);
		_log.info("Hellbound Manager: Loaded");
	}

	public static long getConfidence()
	{
		return ServerVariables.getLong(CONFIDENCE, 0);
	}

	public static void addConfidence(long value)
	{
		ServerVariables.set(CONFIDENCE, Math.round(getConfidence() + value * Config.RATE_HELLBOUND_CONFIDENCE));
	}

	public static void reduceConfidence(long value)
	{
		long i = getConfidence() - value;
		if(i < 1)
			i = 1;
		ServerVariables.set(CONFIDENCE, i);
	}

	public static void setConfidence(long value)
	{
		ServerVariables.set(CONFIDENCE, value);
	}

	public static int getHellboundLevel()
	{
		long confidence = ServerVariables.getLong(CONFIDENCE, 0);
		boolean judesBoxes = ServerVariables.getBool("HB_judesBoxes", false);
		boolean bernardBoxes = ServerVariables.getBool("HB_bernardBoxes", false);
		boolean derekKilled = ServerVariables.getBool("HB_derekKilled", false);
		boolean captainKilled = ServerVariables.getBool("HB_captainKilled", false);

		if(confidence < 1)
			return 0;
		else if(confidence >= 1 && confidence < 300000)
			return 1;
		else if(confidence >= 300000 && confidence < 600000)
			return 2;
		else if(confidence >= 600000 && confidence < 1000000)
			return 3;
		else if(confidence >= 1000000 && confidence < 1200000)
		{
			if(derekKilled && judesBoxes && bernardBoxes)
				return 5;
			else if(!derekKilled && judesBoxes && bernardBoxes)
				return 4;
			else if(!derekKilled && (!judesBoxes || !bernardBoxes))
				return 3;
		}
		else if(confidence >= 1200000 && confidence < 1500000)
			return 6;
		else if(confidence >= 1500000 && confidence < 1800000)
			return 7;
		else if(confidence >= 1800000 && confidence < 2100000)
		{
			if(captainKilled)
				return 9;
			else
				return 8;
		}
		else if(confidence >= 2100000 && confidence < 2200000)
			return 10;
		else if(confidence >= 2200000)
			return 11;

		return 0;
	}

	private class DeathListener implements OnDeathListener
	{
		@Override
		public void onDeath(Creature cha, Creature killer)
		{
			if(killer == null || !cha.isMonster() || !killer.isPlayable())
				return;

			switch(getHellboundLevel())
			{
				case 0:
					break;
				case 1:
				{
					switch(cha.getNpcId())
					{
						case 22320: // Junior Watchman
						case 22321: // Junior Summoner
						case 22324: // Blind Huntsman
						case 22325: // Blind Watchman
							addConfidence(1);
							break;
						case 22327: // Arcane Scout
						case 22328: // Arcane Guardian
						case 22329: // Arcane Watchman
							addConfidence(3); // confirmed
							break;
						case 22322: // Subjugated Native
						case 22323: // Charmed Native
						case 32299: // Quarry Slave
							reduceConfidence(10);
							break;
					}
					break;
				}
				case 2:
				{
					switch(cha.getNpcId())
					{
						case 18463: // Remnant Diabolist
						case 18464: // Remnant Diviner
							addConfidence(5);
							break;
						case 22322: // Subjugated Native
						case 22323: // Charmed Native
						case 32299: // Quarry Slave
							reduceConfidence(10);
							break;
					}
					break;
				}
				case 3:
				{
					switch(cha.getNpcId())
					{
						case 22342: // Darion's Enforcer
						case 22343: // Darion's Executioner
							addConfidence(3);
							break;
						case 22341: // Keltas
							addConfidence(100);
							break;
						case 22322: // Subjugated Native
						case 22323: // Charmed Native
						case 32299: // Quarry Slave
							reduceConfidence(10);
							break;
					}
					break;
				}
				case 4:
				{
					switch(cha.getNpcId())
					{
						case 18465: // Derek
							addConfidence(10000);
							ServerVariables.set("HB_derekKilled", true);
							break;
						case 22322: // Subjugated Native
						case 22323: // Charmed Native
						case 32299: // Quarry Slave
							reduceConfidence(10);
							break;
					}
					break;
				}
				case 5:
				{
					switch(cha.getNpcId())
					{
						case 22448: // Leodas
							reduceConfidence(50);
							break;
					}
					break;
				}
				case 6:
				{
					switch(cha.getNpcId())
					{
						case 22326: // Hellinark
							addConfidence(500);
							break;
						case 18484: // Naia Failan
							addConfidence(5);
							break;
					}
					break;
				}
				case 8:
				{
					switch(cha.getNpcId())
					{
						case 18466: // Outpost Captain
							addConfidence(10000);
							ServerVariables.set("HB_captainKilled", true);
							break;
					}
					break;
				}
				default:
					break;
			}
		}
	}

	private void spawnHellbound()
	{
		if(Config.DONTLOADSPAWN)
			return;

		SpawnManager.getInstance().spawn("hellbound_stage_" + _initialStage);
	}

	private void despawnHellbound()
	{
		if(Config.DONTLOADSPAWN)
			return;

		SpawnManager.getInstance().despawn("hellbound_stage_" + _initialStage);
	}

	private class StageCheckTask extends RunnableImpl
	{
		@Override
		public void runImpl() throws Exception
		{
			if(_initialStage != getHellboundLevel())
			{
				despawnHellbound();
				_initialStage = getHellboundLevel();
				spawnHellbound();
				doorHandler();
			}
		}
	}

	private static void doorHandler()
	{
		final int NativeHell_native0131 = 19250001; // Kief room
		final int NativeHell_native0132 = 19250002;
		final int NativeHell_native0133 = 19250003; // Another room
		final int NativeHell_native0134 = 19250004;

		final int sdoor_trans_mesh00 = 20250002;
		final int Hell_gate_door = 20250001;

		final int[] _doors = {
				NativeHell_native0131,
				NativeHell_native0132,
				NativeHell_native0133,
				NativeHell_native0134,
				sdoor_trans_mesh00,
				Hell_gate_door };

		for(int i = 0; i < _doors.length; i++)
			ReflectionUtils.getDoor(_doors[i]).closeMe();

		switch(getHellboundLevel())
		{
			case 0:
				break;
			case 1:
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				ReflectionUtils.getDoor(NativeHell_native0131).openMe();
				ReflectionUtils.getDoor(NativeHell_native0132).openMe();
				break;
			case 6:
				ReflectionUtils.getDoor(NativeHell_native0131).openMe();
				ReflectionUtils.getDoor(NativeHell_native0132).openMe();
				break;
			case 7:
				ReflectionUtils.getDoor(NativeHell_native0131).openMe();
				ReflectionUtils.getDoor(NativeHell_native0132).openMe();
				ReflectionUtils.getDoor(sdoor_trans_mesh00).openMe();
				break;
			case 8:
				ReflectionUtils.getDoor(NativeHell_native0131).openMe();
				ReflectionUtils.getDoor(NativeHell_native0132).openMe();
				ReflectionUtils.getDoor(sdoor_trans_mesh00).openMe();
				break;
			case 9:
				ReflectionUtils.getDoor(NativeHell_native0131).openMe();
				ReflectionUtils.getDoor(NativeHell_native0132).openMe();
				ReflectionUtils.getDoor(sdoor_trans_mesh00).openMe();
				ReflectionUtils.getDoor(Hell_gate_door).openMe();
				break;
			case 10:
				ReflectionUtils.getDoor(NativeHell_native0131).openMe();
				ReflectionUtils.getDoor(NativeHell_native0132).openMe();
				ReflectionUtils.getDoor(sdoor_trans_mesh00).openMe();
				ReflectionUtils.getDoor(Hell_gate_door).openMe();
				break;
			case 11:
				ReflectionUtils.getDoor(NativeHell_native0131).openMe();
				ReflectionUtils.getDoor(NativeHell_native0132).openMe();
				ReflectionUtils.getDoor(sdoor_trans_mesh00).openMe();
				ReflectionUtils.getDoor(Hell_gate_door).openMe();
				break;
			default:
				break;
		}
	}
}