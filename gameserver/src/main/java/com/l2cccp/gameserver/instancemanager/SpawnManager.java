package com.l2cccp.gameserver.instancemanager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.GameTimeController;
import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
import com.l2cccp.gameserver.data.xml.holder.SpawnHolder;
import com.l2cccp.gameserver.listener.game.OnDayNightChangeListener;
import com.l2cccp.gameserver.listener.game.OnSSPeriodListener;
import com.l2cccp.gameserver.model.HardSpawner;
import com.l2cccp.gameserver.model.Spawner;
import com.l2cccp.gameserver.model.entity.SevenSigns;
import com.l2cccp.gameserver.model.instances.MonsterInstance;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.templates.spawn.PeriodOfDay;
import com.l2cccp.gameserver.templates.spawn.SpawnTemplate;
import com.l2cccp.gameserver.utils.Location;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SpawnManager
{
	private class Listeners implements OnDayNightChangeListener, OnSSPeriodListener
	{
		@Override
		public void onDay()
		{
			despawn(PeriodOfDay.NIGHT.name());
			spawn(PeriodOfDay.DAY.name());
		}

		@Override
		public void onNight()
		{
			despawn(PeriodOfDay.DAY.name());
			spawn(PeriodOfDay.NIGHT.name());
		}

		@Override
		public void onPeriodChange(int mode)
		{
			switch(mode)
			{
				case 0: // all spawns
					despawn(DAWN_GROUP);
					despawn(DUSK_GROUP);
					spawn(DAWN_GROUP);
					spawn(DUSK_GROUP);
					break;
				case 1: // dusk spawns
					despawn(DAWN_GROUP);
					despawn(DUSK_GROUP);
					spawn(DUSK_GROUP);
					break;
				case 2: // dawn spawns
					despawn(DAWN_GROUP);
					despawn(DUSK_GROUP);
					spawn(DAWN_GROUP);
					break;
			}
		}
	}

	private static final Logger _log = LoggerFactory.getLogger(SpawnManager.class);

	private static SpawnManager _instance = new SpawnManager();

	private static final String DAWN_GROUP = "dawn_spawn";
	private static final String DUSK_GROUP = "dusk_spawn";

	private Map<String, List<Spawner>> _spawns = new ConcurrentHashMap<String, List<Spawner>>();
	private Listeners _listeners = new Listeners();

	private final Map<Integer, Integer> spawnCountByNpcId = new HashMap<Integer, Integer>();
	private final Map<Integer, List<Location>> spawnLocationsByNpcId = new HashMap<Integer, List<Location>>();

	public static SpawnManager getInstance()
	{
		return _instance;
	}

	private SpawnManager()
	{
		for(Map.Entry<String, List<SpawnTemplate>> entry : SpawnHolder.getInstance().getSpawns().entrySet())
			fillSpawn(entry.getKey(), entry.getValue());

		GameTimeController.getInstance().addListener(_listeners);
		SevenSigns.getInstance().addListener(_listeners);
	}

	public List<Spawner> fillSpawn(String group, List<SpawnTemplate> templateList)
	{
		if(Config.DONTLOADSPAWN)
			return Collections.emptyList();

		List<Spawner> spawnerList = _spawns.get(group);
		if(spawnerList == null)
			_spawns.put(group, spawnerList = new ArrayList<Spawner>(templateList.size()));

		for(SpawnTemplate template : templateList)
		{
			Spawner spawner = new HardSpawner(template);
			spawnerList.add(spawner);

			NpcTemplate npcTemplate = NpcHolder.getInstance().getTemplate(spawner.getCurrentNpcId());

			int toAdd;
			if(Config.RATE_MOB_SPAWN > 1 && npcTemplate.getInstanceClass() == MonsterInstance.class && npcTemplate.level >= Config.RATE_MOB_SPAWN_MIN_LEVEL && npcTemplate.level <= Config.RATE_MOB_SPAWN_MAX_LEVEL)
			{
				toAdd = (int) (template.getCount() * Config.RATE_MOB_SPAWN);
				spawner.setAmount(toAdd);
			}
			else
			{
				toAdd = template.getCount();
				spawner.setAmount(toAdd);
			}

			if(Config.BBS_DATABASE_ALLOW)
			{
				int currentCount = spawnCountByNpcId.containsKey(npcTemplate.getNpcId()) ? spawnCountByNpcId.get(npcTemplate.getNpcId()).intValue() : 0;
				spawnCountByNpcId.put(npcTemplate.getNpcId(), currentCount + toAdd);
			}
			spawner.setRespawnDelay(template.getRespawn(), template.getRespawnRandom());
			spawner.setReflection(ReflectionManager.DEFAULT);
			spawner.setRespawnTime(0);

			if(Config.BBS_DATABASE_ALLOW)
			{
				Location spawnLoc = spawner.getCurrentSpawnRange().getRandomLoc(ReflectionManager.DEFAULT.getGeoIndex());

				if(!spawnLocationsByNpcId.containsKey(npcTemplate.getNpcId()))
					spawnLocationsByNpcId.put(npcTemplate.getNpcId(), new ArrayList<Location>());

				spawnLocationsByNpcId.get(npcTemplate.getNpcId()).add(spawnLoc);
			}

			if(npcTemplate.isRaid && group.equals(PeriodOfDay.NONE.name()))
				RaidBossSpawnManager.getInstance().addNewSpawn(npcTemplate.getNpcId(), spawner);
		}

		return spawnerList;
	}

	public void spawnAll()
	{
		spawn(PeriodOfDay.NONE.name());
		if(Config.ALLOW_EVENT_GATEKEEPER)
		{
			spawn("event_gatekeeper");
			_log.info("SpawnManager: spawned Event Gatekeeper!");
		}

		if(!Config.ALLOW_CLASS_MASTERS_LIST.isEmpty())
		{
			spawn("class_master");
			_log.info("SpawnManager: spawned Class Master!");
		}

		if(Config.BONUS_SERVICE_ENABLE)
		{
			spawn("bonus_manager");
			_log.info("SpawnManager: spawned Bonus Manager!");
		}
	}

	public void spawn(String group)
	{
		List<Spawner> spawnerList = _spawns.get(group);
		if(spawnerList == null)
			return;

		int npc = 0;

		for(Spawner spawner : spawnerList)
			npc += spawner.init();

		_log.info("SpawnManager: spawned " + npc + " npc from spawns group -> " + group);
	}

	public void despawn(String group)
	{
		List<Spawner> spawnerList = _spawns.get(group);
		if(spawnerList == null)
			return;

		for(Spawner spawner : spawnerList)
			spawner.deleteAll();
	}

	public List<Spawner> getSpawners(String group)
	{
		List<Spawner> list = _spawns.get(group);
		return list == null ? Collections.<Spawner> emptyList() : list;
	}

	public int getSpawnedCountByNpc(int npcId)
	{
		if(!spawnCountByNpcId.containsKey(npcId))
			return 0;

		return spawnCountByNpcId.get(npcId).intValue();
	}

	public List<Location> getRandomSpawnsByNpc(int npcId)
	{
		return spawnLocationsByNpcId.get(npcId);
	}

	public void reloadAll()
	{
		RaidBossSpawnManager.getInstance().cleanUp();
		for(List<Spawner> spawnerList : _spawns.values())
			for(Spawner spawner : spawnerList)
				spawner.deleteAll();

		RaidBossSpawnManager.getInstance().reloadBosses();

		spawnAll();

		//FIXME [VISTALL] придумать другой способ
		int mode = 0;
		if(SevenSigns.getInstance().getCurrentPeriod() == SevenSigns.PERIOD_SEAL_VALIDATION)
			mode = SevenSigns.getInstance().getCabalHighestScore();

		_listeners.onPeriodChange(mode);

		if(GameTimeController.getInstance().isNowNight())
			_listeners.onNight();
		else
			_listeners.onDay();
	}
}