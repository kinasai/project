package handler.voicecommands;

import java.util.List;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.IBroadcastPacket;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.skills.skillclasses.Call;
import com.l2cccp.gameserver.utils.Util;

public class SummonClan implements IVoicedCommandHandler, OnInitScriptListener
{
	private final String[] _commandList = new String[] { "summon_clan", "km-all-to-me" };

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, Player player, String target)
	{
		if(!Config.COMMAND_SUMMON_CLAN_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return false;
		}

		if(command.equalsIgnoreCase("summon_clan") || command.equalsIgnoreCase("km-all-to-me"))
		{
			if(!player.isClanLeader())
			{
				player.sendPacket(SystemMsg.ONLY_THE_CLAN_LEADER_IS_ENABLED);
				return false;
			}

			if(Util.getPay(player, Config.COMMAND_LEADER_SUMMON_CLAN_COST[0], Config.COMMAND_LEADER_SUMMON_CLAN_COST[1], true))
			{
				IBroadcastPacket msg = Call.canSummonHere(player);
				if(msg != null)
				{
					player.sendPacket(msg);
					return false;
				}

				List<Player> clan = player.getClan().getOnlineMembers(player.getObjectId());

				for(Player pl : clan)
				{
					if(Call.canBeSummoned(pl) == null)
						pl.summonCharacterRequest(player, Config.COMMAND_SUMMON_CLAN_COST[0], Config.COMMAND_SUMMON_CLAN_COST[1]);
					else
						player.sendMessage(new CustomMessage("command.summon.clan.cant.summon").addString(pl.getName()));
				}

				return true;
			}
		}

		return false;
	}

	@Override
	public void onInit()
	{
		VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
	}
}