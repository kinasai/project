package handler.voicecommands;

import com.l2cccp.gameserver.dao.SiegeClanDAO;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.EventHolder;
import com.l2cccp.gameserver.data.xml.holder.ResidenceHolder;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.EventType;
import com.l2cccp.gameserver.model.entity.events.impl.DominionSiegeRunnerEvent;
import com.l2cccp.gameserver.model.entity.events.impl.FortressSiegeEvent;
import com.l2cccp.gameserver.model.entity.events.impl.SiegeEvent;
import com.l2cccp.gameserver.model.entity.events.objects.SiegeClanObject;
import com.l2cccp.gameserver.model.entity.residence.Castle;
import com.l2cccp.gameserver.model.entity.residence.Fortress;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.model.pledge.Privilege;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.CastleSiegeInfo;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.utils.Util;

public class SiegeRegister implements IVoicedCommandHandler, OnInitScriptListener
{
	@Override
	public void onInit()
	{
		VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
	}

	private String[] _commandList = new String[] { "siege", "castle_siege", "forte", "fort_siege" };

	@Override
	public boolean useVoicedCommand(String command, Player player, String target)
	{
		if(command.equals("castle_siege") || command.equals("siege"))
		{
			HtmlMessage html = new HtmlMessage(5).setFile("command/siege/castle.htm");
			player.sendPacket(html);
			if(!target.isEmpty() && Util.isNumber(target))
			{
				int castleId = Integer.parseInt(target);
				Castle castle = ResidenceHolder.getInstance().getResidence(castleId);
				player.sendPacket(new CastleSiegeInfo(castle, player));
			}
			return true;
		}
		else if(command.equals("fort_siege") || command.equals("forte"))
		{
			String[] data = target.split(" ");
			show(player, data[0]);
			if(data.length > 1)
				register(player, data[1]);

			return true;
		}
		else
			return false;
	}

	private void register(Player player, String string)
	{
		Clan clan = player.getClan();
		if(clan == null)
		{
			showChatWindow(player, "residence2/fortress/fortress_ordery002.htm");
			return;
		}

		if(clan.isPlacedForDisband())
		{
			player.sendPacket(SystemMsg.YOU_HAVE_ALREADY_REQUESTED_THE_DISSOLUTION_OF_YOUR_CLAN);
			return;
		}
		int id = Integer.parseInt(string);

		Fortress fortress = ResidenceHolder.getInstance().getResidence(id);
		if(clan.getHasFortress() == fortress.getId())
		{
			showChatWindow(player, "residence2/fortress/fortress_ordery014.htm", "%clan_name%", clan.getName());
			return;
		}

		if(!player.hasPrivilege(Privilege.CS_FS_SIEGE_WAR))
		{
			showChatWindow(player, "residence2/fortress/fortress_ordery012.htm");
			return;
		}

		if(clan.getCastle() > 0)
		{
			Castle relatedCastle = null;
			for(Castle castle : fortress.getRelatedCastles())
				if(castle.getId() == clan.getCastle())
					relatedCastle = castle;

			if(relatedCastle != null)
			{
				if(fortress.getContractState() == Fortress.CONTRACT_WITH_CASTLE)
				{
					showChatWindow(player, "residence2/fortress/fortress_ordery022.htm");
					return;
				}

				if(relatedCastle.getSiegeEvent().isRegistrationOver())
				{
					showChatWindow(player, "residence2/fortress/fortress_ordery_debug001.htm");
					return;
				}
			}
			else
			{
				showChatWindow(player, "residence2/fortress/fortress_ordery021.htm");
				return;
			}
		}

		FortressSiegeEvent siegeEvent = fortress.getSiegeEvent();
		SiegeClanObject siegeClan = siegeEvent.getSiegeClan(FortressSiegeEvent.ATTACKERS, clan);
		if(siegeClan != null)
		{
			showChatWindow(player, "residence2/fortress/fortress_ordery007.htm");
			return;
		}

		// 1 рега возможна всего
		for(Fortress $ : ResidenceHolder.getInstance().getResidenceList(Fortress.class))
			if($.getSiegeEvent().getSiegeClan(FortressSiegeEvent.ATTACKERS, clan) != null)
			{
				showChatWindow(player, "residence2/fortress/fortress_ordery_debug002.htm");
				return;
			}

		if(clan.getLevel() < 4)
		{
			showChatWindow(player, "residence2/fortress/fortress_ordery_debug003.htm");
			return;
		}

		// если у нас есть форт, запрещаем регатся на форт, если на носу осада своего форта(во избежания абуза, участия в 2 осадах)
		if(clan.getHasFortress() > 0)
		{
			Fortress clanFortress = ResidenceHolder.getInstance().getResidence(clan.getHasFortress());
			if(clanFortress.getSiegeDate().getTimeInMillis() > 0)
			{
				showChatWindow(player, "residence2/fortress/fortress_ordery_debug004.htm");
				return;
			}
		}

		DominionSiegeRunnerEvent runnerEvent = EventHolder.getInstance().getEvent(EventType.MAIN_EVENT, 1);
		if(runnerEvent.isRegistrationOver() || siegeEvent.isRegistrationOver())
		{
			showChatWindow(player, "residence2/fortress/fortress_ordery_debug005.htm");
			return;
		}

		int attackersSize = siegeEvent.getObjects(SiegeEvent.ATTACKERS).size();
		if(attackersSize == 0)
			if(!player.reduceAdena(250000L, true))
			{
				showChatWindow(player, "residence2/fortress/fortress_ordery003.htm");
				return;
			}

		siegeClan = new SiegeClanObject(FortressSiegeEvent.ATTACKERS, clan, 0);
		siegeEvent.addObject(FortressSiegeEvent.ATTACKERS, siegeClan);
		SiegeClanDAO.getInstance().insert(fortress, siegeClan);

		siegeEvent.reCalcNextTime(false);

		player.sendPacket(new SystemMessage(SystemMsg.YOUR_CLAN_HAS_BEEN_REGISTERED_TO_S1S_FORTRESS_BATTLE).addResidenceName(fortress));
		showChatWindow(player, "residence2/fortress/fortress_ordery005.htm");
	}

	public void showChatWindow(Player player, String filename, Object... replace)
	{
		HtmlMessage packet = new HtmlMessage(5).setFile(filename);
		if(replace.length % 2 == 0)
			for(int i = 0; i < replace.length; i += 2)
				packet.replace(String.valueOf(replace[i]), String.valueOf(replace[i + 1]));
		player.sendPacket(packet);
	}

	private boolean canReg(Fortress fort)
	{
		FortressSiegeEvent siege = fort.getSiegeEvent();

		DominionSiegeRunnerEvent runner = EventHolder.getInstance().getEvent(EventType.MAIN_EVENT, 1);
		if(runner.isRegistrationOver() || siege.isRegistrationOver())
			return false;

		return true;
	}

	public void show(Player player, String arg)
	{
		int page = 1;
		if(!arg.isEmpty() && Util.isNumber(arg))
			page = Integer.parseInt(arg);

		HtmlMessage html = new HtmlMessage(5).setFile("command/siege/fort.htm");
		String template = HtmCache.getInstance().getHtml("command/siege/fort_template.htm", player);
		String list = "";
		String data = "";

		int current = 1;
		int inpage = 11;
		int start = (page - 1) * inpage;
		int end = Math.min(page * inpage, Fortress.getFortress().size());
		for(int i = start; i < end; i++)
		{
			Fortress fort = Fortress.getFortress().get(i);
			list = template;
			list = list.replace("<?id?>", String.valueOf(fort.getId()));
			list = list.replace("<?color?>", (canReg(fort) ? "669900" : "FF3333"));
			list = list.replace("<?bg_color?>", (current % 2 == 0 ? "666666" : "333333"));
			list = list.replace("<?page?>", String.valueOf(page));
			data += list;
			current++;
		}

		html.replace("%navigate%", parseNavigate(page));
		html.replace("%data%", data);
		player.sendPacket(html);

	}

	private String parseNavigate(int page)
	{
		StringBuilder pg = new StringBuilder();

		double size = Fortress.getFortress().size();
		double inpage = 11;

		if(size > inpage)
		{
			double max = Math.ceil(size / inpage);

			pg.append("<center><table width=25 border=0><tr>");
			int line = 1;

			for(int current = 1; current <= max; current++)
			{
				if(page == current)
					pg.append("<td width=25 align=center><button value=\"[").append(current).append("]\" width=38 height=25 back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\"></td>");
				else
					pg.append("<td width=25 align=center><button value=\"").append(current).append("\" action=\"bypass -h user_fort_siege ").append(current).append("\" width=28 height=25 back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\"></td>");

				if(line == 22)
				{
					pg.append("</tr><tr>");
					line = 0;
				}
				line++;
			}

			pg.append("</tr></table></center>");
		}

		return pg.toString();
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
