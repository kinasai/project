package handler.voicecommands;

import com.l2cccp.gameserver.data.xml.holder.EventHolder;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.EventType;
import com.l2cccp.gameserver.model.entity.events.impl.TerritoryWarsEvent;

/**
 * @author Java-man
 */
public class TerritoryWarsEventHandler implements IVoicedCommandHandler
{
	private String[] _commandList = { "twreg" };


	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, Player activeChar, String args)
	{
		if("twreg".equals(command))
		{
			TerritoryWarsEvent event1 = EventHolder.getInstance().getEvent(EventType.PVP_EVENT, 5);
			TerritoryWarsEvent event2 = EventHolder.getInstance().getEvent(EventType.PVP_EVENT, 6);
			if(!event1.isRegistrationOver())
				event1.registerPlayer(activeChar);
			else if(!event2.isRegistrationOver())
				event2.registerPlayer(activeChar);
			else
				activeChar.sendMessage("Can't register in TW event.");
		}
		return false;
	}
}