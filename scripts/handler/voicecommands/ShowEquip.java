package handler.voicecommands;

import com.l2cccp.commons.lang.reference.HardReference;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.dress.DressData;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.CharInfo;

public class ShowEquip implements IVoicedCommandHandler, OnInitScriptListener
{
	@Override
	public String[] getVoicedCommandList()
	{
		return new String[] { "showequip" };
	}

	@Override
	public boolean useVoicedCommand(String command, Player player, String target)
	{
		if(!Config.COMMAND_SHOW_EQUIP_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return false;
		}

		final boolean force = player.isForceVisualFlag();
		final boolean timer = Config.COMMAND_SHOW_EQUIP_TIMER;

		if(timer && force)
			return true;
		else
			change(player, !force);

		if(timer)
		{
			final HardReference<Player> ref = player.getRef();
			ThreadPoolManager.getInstance().schedule(new RunnableImpl(){
				@Override
				public void runImpl()
				{
					Player player = ref.get();
					if(player == null)
						return;

					change(player, false);
				}
			}, Config.COMMAND_SHOW_EQUIP_TIME * 1000L);
		}
		return true;
	}

	private void change(Player player, boolean hide)
	{
		player.setForceVisualFlag(hide);

		// Двойная отправка, из за затупа клиента!
		player.sendUserInfo(true);
		player.sendUserInfo(true);

		for(Player temp : World.getAroundPlayers(player))
		{
			if(temp.isInvisible() && !player.getPlayerAccess().IsGM)
				continue;

			player.sendPacket(new CharInfo(temp, DressData.check(player)));
		}
	}

	@Override
	public void onInit()
	{
		VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
	}
}
