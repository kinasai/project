package handler.voicecommands;

import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Player;

public class NextAction implements IVoicedCommandHandler, OnInitScriptListener
{
	private String[] _commandList = new String[] { "action-on", "action-off" };

	@Override
	public void onInit()
	{
		VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, Player player, String args)
	{
		if(player == null)
			return false;

		if(command.equals("action-off"))
		{
			if(!player.getVarB("DisableAttackNext"))
				player.setVar("DisableAttackNext", "true", -1);
			player.sendMessage("Disable Attack Next: ON");
			return true;
		}
		else if(command.equals("action-on"))
		{
			if(player.getVarB("DisableAttackNext"))
				player.unsetVar("DisableAttackNext");
			player.sendMessage("Disable Attack Next: OFF");
			return true;
		}
		else
			return false;
	}
}
