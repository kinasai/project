package handler.voicecommands;

import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Player;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BlockBuff implements IVoicedCommandHandler, OnInitScriptListener
{
	@Override
	public void onInit()
	{
		VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
	}

	private String[] _commandList = new String[] { "blockbuff" };

	@Override
	public boolean useVoicedCommand(String command, Player player, String target)
	{
		if(command.equals("blockbuff"))
		{
			if(player.getSessionVarB("blockbuff", false))
			{
				player.deleteSessionVar("blockbuff");
				player.sendMessage("Block Buff: OFF");
			}
			else
			{
				player.addSessionVar("blockbuff", true);
				player.sendMessage("Block Buff: ON");
			}

			return true;
		}
		else
			return false;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
