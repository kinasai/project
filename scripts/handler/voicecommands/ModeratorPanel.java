package handler.voicecommands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.l2cccp.gameserver.Announcements;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.AdminFunctions;
import com.l2cccp.gameserver.utils.AutoBan;
import com.l2cccp.gameserver.utils.DeclensionKey;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.Util;

public class ModeratorPanel implements IVoicedCommandHandler, OnInitScriptListener
{
	private static List<String> _chats = new ArrayList<String>();
	private static List<String> _jails = new ArrayList<String>();
	private final String[] _commandList = new String[] {
			"moder",
			"moder_jail",
			"moder_gojail",
			"moder_unjail",
			"moder_ban",
			"moder_banchat",
			"moder_unbanchat" };

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}

	@Override
	public boolean useVoicedCommand(String command, Player player, String target)
	{
		boolean moder = player.getVarB("Moderator") || player.isGM();
		boolean super_moder = player.getVarB("SuperModerator") || player.isGM();
		if(!moder && !super_moder)
		{
			player.sendMessage(new CustomMessage("common.Error"));
			return false;
		}

		if(command.equalsIgnoreCase("moder"))
		{
			if(super_moder)
				showPanel(player, "panel", 0, false);
			else
				showPanel(player, "chat", 6, false);
			return true;
		}
		else if(command.equalsIgnoreCase("moder_jail"))
		{
			if(super_moder)
			{
				showPanel(player, "jail", 6, false);
				return true;
			}
			else
			{
				showPanel(player, "panel", 0, false);
				player.sendMessage("You are not super moderator!");
				return false;
			}
		}
		else if(command.equalsIgnoreCase("moder_gojail"))
		{
			if(super_moder)
			{
				String[] param = target.split(" ");

				if(param.length < 1)
				{
					player.sendMessage("Complite name fields!");
					return false;
				}

				String name = param[0];
				String period = "60";
				String reason = "";

				if(param.length >= 2)
					period = param[1];

				if(param.length >= 3)
					reason = Util.ArrayToString(param, 2);

				Player _target = World.getPlayer(name);

				if(_target != null)
				{
					_target.setVar("jailedFrom", _target.getX() + ";" + _target.getY() + ";" + _target.getZ() + ";" + _target.getReflectionId(), -1);
					_target.setVar("jailed", period, -1);
					int time = Integer.parseInt(period);
					_target.startUnjailTask(_target, time);
					_target.teleToLocation(Location.findPointToStay(_target, AdminFunctions.JAIL_SPAWN, 50, 200), ReflectionManager.JAIL);
					if(_target.isInStoreMode())
						_target.setPrivateStoreType(Player.STORE_PRIVATE_NONE);
					_target.sitDown(null);
					_target.block();
					Announcements.getInstance().announceToAll("Игрок " + _target.getName() + " был перемещен в тюрьму. Период: " + time + " " + Util.declension(player.getLanguage(), time, DeclensionKey.MINUTES) + ". Причина: " + (reason.isEmpty() ? "Неуказана!" : reason));
					_target.sendMessage("You moved to jail, time to escape - " + period + " minutes, reason - " + reason);
					player.sendMessage("You jailed " + _target + ".");
					_jails.add(_target.getName() + " go to jail for " + period + " min.");
				}
				else
					player.sendMessage("Can't find char " + name + ".");
				showPanel(player, "jail", 6, false);
				return true;
			}
			else
			{
				showPanel(player, "panel", 0, false);
				player.sendMessage("You are not super moderator!");
				return false;
			}
		}
		else if(command.equalsIgnoreCase("moder_unjail"))
		{
			if(super_moder)
			{
				showPanel(player, "jail", 6, false);

				String[] param = target.split(" ");

				if(param.length < 1)
				{
					player.sendMessage("Complite name fields!");
					return false;
				}

				String name = param[0];
				Player _target = World.getPlayer(name);

				if(_target != null && _target.getVar("jailed") != null)
				{
					String[] re = _target.getVar("jailedFrom").split(";");
					_target.teleToLocation(Integer.parseInt(re[0]), Integer.parseInt(re[1]), Integer.parseInt(re[2]));
					_target.setReflection(re.length > 3 ? Integer.parseInt(re[3]) : 0);
					_target.stopUnjailTask();
					_target.unsetVar("jailedFrom");
					_target.unsetVar("jailed");
					_target.unblock();
					_target.standUp();
					player.sendMessage("You unjailed " + _target + ".");
				}
				else
					player.sendMessage("Can't find char " + name + ".");

				return true;
			}
			else
			{
				showPanel(player, "panel", 0, false);
				player.sendMessage("You are not super moderator!");
				return false;
			}
		}
		else if(command.equalsIgnoreCase("moder_ban"))
		{
			showPanel(player, "chat", 6, true);
			return true;
		}
		else if(command.equalsIgnoreCase("moder_banchat"))
		{
			String[] param = target.split(" ");

			if(param.length < 3)
			{
				player.sendMessage("Complite all fields!");
				return false;
			}

			String name = param[0];
			Player baned = GameObjectsStorage.getPlayer(name); // Тупо для получения нормального имени :)

			if(baned == null)
			{
				player.sendMessage("Can't find player with name: " + name);
				return false;
			}

			int time = 20;
			if(Util.isNumber(param[1]))
				time = Integer.parseInt(param[1]);

			String reason = Util.ArrayToString(param, 2);

			if(AutoBan.ChatBan(baned.getName(), time, reason, "Moderator"))
			{
				Announcements.getInstance().announceToAll("Забанен чат игроку " + baned.getName() + ". Период: " + time + " " + Util.declension(player.getLanguage(), time, DeclensionKey.MINUTES) + ". Причина: " + (reason.isEmpty() ? "Неуказана!" : reason));
				_chats.add(baned.getName() + " received a ban for " + time + " min.");
			}
			else
				player.sendMessage("Error, something wrong!");
			showPanel(player, "chat", 6, true);

			return true;
		}
		else if(command.equalsIgnoreCase("moder_unbanchat"))
		{
			showPanel(player, "chat", 6, true);
			String[] param = target.split(" ");

			if(param.length < 1)
			{
				player.sendMessage("Complite name fields!");
				return false;
			}

			String name = param[0];
			if(AutoBan.ChatUnBan(name, "Moderator"))
				player.sendMessage("You unban chat for " + name + ".");
			else
				player.sendMessage("Can't find char " + name + ".");
		}

		return false;
	}

	private void showPanel(Player player, String page, int log, boolean chat_logs)
	{
		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Moderator/" + page + ".htm");

		if(log > 0)
			addLogs(html, log, chat_logs);

		player.sendPacket(html);
	}

	private void addLogs(HtmlMessage html, int size, boolean chat_logs)
	{
		List<String> logs = chat_logs ? _chats : _jails;
		Collections.reverse(logs);

		int i = 1;
		for(String _log : logs)
		{
			html.replace("%log_" + i + "%", _log);
			i++;
		}

		if(i < size)
		{
			while(i <= size)
			{
				html.replace("%log_" + i + "%", "...");
				i++;
			}
		}
	}

	@Override
	public void onInit()
	{
		VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
	}
}
