package handler.voicecommands;

import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Party;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;

public class PartyInfo implements IVoicedCommandHandler, OnInitScriptListener
{
	private String[] _commandList = new String[] { "party" };

	@Override
	public boolean useVoicedCommand(String command, Player activeChar, String target)
	{
		if(command.equals("party"))
		{
			final Party party;

			if((party = activeChar.getParty()) == null)
				return false;

			String html = HtmCache.getInstance().getHtml("scripts/services/Party/index.htm", activeChar);
			int i = 1;

			final String leader = party.getPartyLeader().getName();
			html = html.replace("{leader}", leader);

			final int result = (int) ((activeChar.getBonus().getRateXp() - 1) * 100);
			html = html.replace("{myrate}", (result > 0 ? "<font color=\"LEVEL\">+" + result + "%</font>" : "<font color=\"669933\">+0%</font>"));
			double rate = 0;
			int level = 0;
			for(final Player player : party.getPartyMembers())
			{
				final String name = player.getName().length() > 15 ? player.getName().substring(0, 14) : player.getName();
				html = html.replace("{player_" + i + "}", player.getName().equals(activeChar.getName()) ? "<font color=\"FF3333\">" + name + "</font>" : player.getName().equals(leader) ? "<font color=\"3399CC\">" + name + "</font>" : name);
				final int myLevel = player.getLevel();
				level += myLevel;
				html = html.replace("{level_" + i + "}", String.valueOf(myLevel));
				if(player.getNetConnection() != null) //перестраховка чтобы нпе не поймать.
				{
					int result2 = (int) ((player.getBonus().getRateXp() - 1) * 100);
					html = html.replace("{rate_" + i + "}", (result2 > 0 ? "<font color=\"LEVEL\">+" + result2 + "%</font>" : "<a action=\"bypass -h htmbypass_services.Premium:giftAsk " + player.getObjectId() + "\"><font color=66CC00>" + new CustomMessage("premium.gift").toString(player) + "</font></a>"));
					rate += result2;
				}
				else
				{
					html = html.replace("{rate_" + i + "}", "<font color\"FF0000\">no connection</font>");
					rate += 1.0;
				}
				i++;
			}

			final int count = party.getMemberCount();
			double result3 = rate / count;

			if(result3 < 0)
				result3 = 0;

			html = html.replace("{count}", String.valueOf(count));
			html = html.replace("{rate}", "+" + result3 + "%");
			html = html.replace("{level}", String.valueOf(level / count));

			//Заменяем поля которые остались пустые
			for(int j = i; j <= 10; j++)
			{
				html = html.replace("{player_" + j + "}", "...");
				html = html.replace("{rate_" + j + "}", "...");
				html = html.replace("{level_" + j + "}", "...");
			}

			activeChar.sendPacket(new HtmlMessage(5).setHtml(html));
			return true;
		}
		else
			return true;
	}

	@Override
	public void onInit()
	{
		VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}
