package handler.voicecommands;

import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.network.l2.s2c.ActionFail;
import com.l2cccp.gameserver.network.l2.s2c.Die;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Restart implements IVoicedCommandHandler, OnInitScriptListener
{
	@Override
	public void onInit()
	{
		VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
	}

	private String[] _commandList = new String[] { "restart" };

	@Override
	public boolean useVoicedCommand(String command, Player player, String target)
	{
		if(command.equals("restart"))
		{
			if(player.isDead())
			{
				player.sendPacket(ActionFail.STATIC, new Die(player));
				return true;
			}
		}

		return false;
	}

	@Override
	public String[] getVoicedCommandList()
	{
		return _commandList;
	}
}