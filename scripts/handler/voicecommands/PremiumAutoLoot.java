//package handler.voicecommands;
//
//import com.l2cccp.gameserver.Config;
//import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
//import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
//import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
//import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
//import com.l2cccp.gameserver.model.Player;
//import com.l2cccp.gameserver.network.l2.components.CustomMessage;
//import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
//import com.l2cccp.gameserver.network.l2.components.SystemMsg;
//import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
//import com.l2cccp.gameserver.utils.DeclensionKey;
//import com.l2cccp.gameserver.utils.Util;
//
//public class PremiumAutoLoot implements IVoicedCommandHandler, OnInitScriptListener
//{
//	private String[] _commandList = new String[] { "paloot" };
//
//	@Override
//	public void onInit()
//	{
//		VoicedCommandHandler.getInstance().registerVoicedCommandHandler(this);
//	}
//
//	@Override
//	public String[] getVoicedCommandList()
//	{
//		return _commandList;
//	}
//
//	@Override
//	public boolean useVoicedCommand(String command, Player player, String args)
//	{
//		if(Config.AUTO_LOOT || !Config.PREMIUM_AUTO_LOOT_BUY)
//			return false;
//
//		else if(player.getVar("AutoLoot") != null)
//		{
//			player.sendMessage("You are already using premium auto-loot!");
//			return false;
//		}
//
//		if(command.equals("paloot"))
//		{
//			String[] param = args.split(" ");
//			if(param.length <= 1)
//			{
//				HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/PremiumAutoLoot/index.htm");
//				html.replace("%pay%", Util.formatPay(player, Config.PREMIUM_AUTO_LOOT_DATA[1], Config.PREMIUM_AUTO_LOOT_DATA[0]));
//
//				player.sendPacket(html);
//				return true;
//			}
//			else
//			{
//				if(param[0].equalsIgnoreCase("days"))
//				{
//					String _days = param[1];
//					if(!Util.isNumber(_days))
//					{
//						player.sendMessage("Set count of days!");
//						return false;
//					}
//
//					int days = Integer.valueOf(_days);
//
//					if(days < 1)
//						days = 1;
//					else if(days > 100)
//						days = 100;
//
//					String msg = new CustomMessage("services.premium.autoloot.ask").addNumber(days).addString(Util.declension(player.getLanguage(), days, DeclensionKey.DAYS)).addString(Util.formatPay(player, days * Config.PREMIUM_AUTO_LOOT_DATA[1], Config.PREMIUM_AUTO_LOOT_DATA[0])).toString(player);
//					ConfirmDlg ask = new ConfirmDlg(SystemMsg.S1, 60000);
//					ask.addString(msg);
//
//					player.ask(ask, new AnswerListener(days));
//					return false;
//				}
//			}
//		}
//		return false;
//	}
//
//	private class AnswerListener implements OnAnswerListener
//	{
//		private int days;
//
//		protected AnswerListener(int days)
//		{
//			this.days = days;
//		}
//
//		@Override
//		public void sayYes(Player player)
//		{
//			if(!player.isOnline())
//				return;
//			if(Util.getPay(player, Config.PREMIUM_AUTO_LOOT_DATA[0], days * Config.PREMIUM_AUTO_LOOT_DATA[1], true))
//			{
//				player.setAutoLoot(Player.AUTO_LOOT_ALL);
//				player.unsetVar("AutoLoot");
//				player.setVar("AutoLoot", Player.AUTO_LOOT_ALL, System.currentTimeMillis() + (86400000 * days));
//				player.sendMessage("You have successfully bought the premium auto-loot for " + days + " " + Util.declension(player.getLanguage(), days, DeclensionKey.DAYS));
//			}
//		}
//
//		@Override
//		public void sayNo(Player player)
//		{}
//	}
//}
//FIXME: Переписать под новый движек конфигов!