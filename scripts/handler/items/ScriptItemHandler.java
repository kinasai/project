package handler.items;

import com.l2cccp.gameserver.handler.items.IItemHandler;
import com.l2cccp.gameserver.handler.items.ItemHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author VISTALL
 * @date 21:09/12.07.2011
 */
public abstract class ScriptItemHandler implements OnInitScriptListener, IItemHandler
{
	@Override
	public boolean dropItem(Player player, ItemInstance item, long count, Location loc)
	{
		return true;
	}

	@Override
	public boolean pickupItem(Playable playable, ItemInstance item)
	{
		return true;
	}

	@Override
	public void onInit()
	{
		ItemHandler.getInstance().registerItemHandler(this);
	}
}
