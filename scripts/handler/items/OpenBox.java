//package handler.items;
//
//import com.l2cccp.commons.util.Rnd;
//import com.l2cccp.gameserver.Config;
//import com.l2cccp.gameserver.data.htm.HtmCache;
//import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
//import com.l2cccp.gameserver.model.Playable;
//import com.l2cccp.gameserver.model.Player;
//import com.l2cccp.gameserver.model.items.Inventory;
//import com.l2cccp.gameserver.model.items.ItemInstance;
//import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
//import com.l2cccp.gameserver.skills.SkillEntry;
//import com.l2cccp.gameserver.tables.SkillTable;
//import com.l2cccp.gameserver.templates.item.ItemTemplate;
//import com.l2cccp.gameserver.utils.Util;
//
///**
// * @author L2CCCP
// * @site http://l2cccp.com/
// */
//public class OpenBox extends ScriptItemHandler
//{
//	public static int[] keys = new int[] { 40007, 40008 };
//
//	@Override
//	public boolean useItem(Playable playable, ItemInstance item, boolean ctrl)
//	{
//		if(playable == null || !playable.isPlayer())
//			return false;
//
//		final int id = item.getItemId();
//		final Player player = (Player) playable;
//		final Inventory inv = player.getInventory();
//		final ItemInstance simple = inv.getItemByItemId(keys[0]);
//		final ItemInstance royal = inv.getItemByItemId(keys[1]);
//		final boolean si = simple != null;
//		final boolean ro = royal != null;
//
//		if(!si && !ro)
//		{
//			player.sendMessage("You don't have any key!");
//			return false;
//		}
//
//		if(!si && ro)
//			open(id, player, royal.getItemId());
//		else if(si && !ro)
//			open(id, player, simple.getItemId());
//		else
//			page(id, player);
//
//		return true;
//	}
//
//	public static void open(int id, Player player, int key)
//	{
//		int skillId = 0;
//		switch(id)
//		{
//			case 40004:
//				skillId = 40010;
//				break;
//			case 40005:
//				skillId = 40011;
//				break;
//			case 40006:
//				skillId = 40012;
//				break;
//		}
//
//		if(Util.getPay(player, key, 1, true))
//		{
//			if(Rnd.chance(key == keys[0] ? 20 : 100))
//			{
//				final SkillEntry skill = SkillTable.getInstance().getSkillEntry(skillId, 1);
//				player.altUseSkill(skill, player);
//			}
//			else
//				player.sendMessage("Oh no, key is broken! Try again!");
//		}
//	}
//
//	private void page(int num, Player player)
//	{
//		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/OpenBox/index.htm");
//		String template = HtmCache.getInstance().getHtml("scripts/services/OpenBox/template.htm", player);
//		String block = "";
//		String list = "";
//
//		for(int i = 0; i < keys.length; i++)
//		{
//			final int id = keys[i];
//			final ItemTemplate tpl = ItemHolder.getInstance().getTemplate(id);
//			block = template;
//			block = block.replace("{name}", tpl.getName());
//			block = block.replace("{icon}", tpl.getIcon());
//			block = block.replace("{chance}", String.valueOf(id == keys[0] ? 20 : 100));
//			block = block.replace("{id}", String.valueOf(num));
//			block = block.replace("{i}", String.valueOf(i));
//			list += block;
//		}
//
//		html.replace("%list%", list);
//
//		player.sendPacket(html);
//	}
//
//	@Override
//	public int[] getItemIds()
//	{
//		return Config.BOXEX_LIST;
//	}
//}