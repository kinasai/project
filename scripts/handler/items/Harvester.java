package handler.items;

import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.MonsterInstance;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;

public class Harvester extends SimpleItemHandler
{
	private static final int[] ITEM_IDS = new int[] { 5125 };

	@Override
	public int[] getItemIds()
	{
		return ITEM_IDS;
	}

	@Override
	protected boolean useItemImpl(Player player, ItemInstance item, boolean ctrl)
	{
		GameObject target = player.getTarget();
		if(target == null || !target.isMonster())
		{
			player.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
			return false;
		}

		MonsterInstance monster = (MonsterInstance) player.getTarget();

		if(!monster.isDead())
		{
			player.sendPacket(SystemMsg.THAT_IS_AN_INCORRECT_TARGET);
			return false;
		}

		SkillEntry skill = SkillTable.getInstance().getSkillEntry(2098, 1);
		if(skill != null && skill.checkCondition(player, monster, false, false, true))
		{
			player.getAI().Cast(skill, monster);
			return true;
		}

		return false;
	}
}
