package handler.items;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExAutoSoulShot;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.SkillEntry;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class AutoRegen extends ScriptItemHandler
{
	@Override
	public boolean useItem(Playable playable, ItemInstance item, boolean ctrl)
	{
		if(!Config.AUTO_CONSUME)
			return false;

		if(!playable.isPlayer())
			return false;

		if(item.getCount() < 1)
		{
			playable.sendPacket(SystemMsg.INCORRECT_ITEM_COUNT);
			return false;
		}

		final Player player = playable.getPlayer();
		change(player, item);

		return true;
	}

	private void change(Player player, ItemInstance item)
	{
		final int id = item.getItemId();
		if(!player.getAutoConsume().contains(id))
		{
			final SkillEntry[] skills = item.getTemplate().getAttachedSkills();
			for(SkillEntry skill : skills)
				player.doCast(skill, player, true);
		}
		else
		{
			final SkillEntry[] skills = item.getTemplate().getAttachedSkills();
			for(SkillEntry skill : skills)
				player.getEffectList().stopEffect(skill);

			player.removeAutoConsume(id);
			player.sendPacket(new ExAutoSoulShot(id, false), new SystemMessage(SystemMsg.THE_AUTOMATIC_USE_OF_S1_HAS_BEEN_DEACTIVATED).addItemName(id));
		}
	}

	@Override
	public int[] getItemIds()
	{
		return Config.AUTO_CONSUME_LIST;
	}
}