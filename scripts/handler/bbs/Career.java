package handler.bbs;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.ClassId;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.utils.Util;

public class Career extends ScriptBbsHandler
{
	private static Career _instance = new Career();

	public static Career getInstance()
	{
		return _instance;
	}

	@Override
	public void onInit()
	{
		super.onInit();
	}

	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbscareer" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.BBS_CLASS_MASTER_ALLOW)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			Util.communityNextPage(player, "_bbshome");
			return;
		}

		if(bypass.equals("_bbscareer"))
			showClassPage(player);
		else
		{
			final String[] data = bypass.split(":");
			final String key = data[1];
			if(key.equals("change_class"))
			{
				changeClass(player, Integer.parseInt(data[2]), Integer.parseInt(data[3]), Integer.parseInt(data[4]));
				return;
			}
			else if(key.equals("window"))
			{
				onBypassCommand(player, "_bbscareer");
				HtmlMessage window = new HtmlMessage(5).setFile("scripts/window/" + data[2] + ".htm");
				player.sendPacket(window);
				return;
			}
		}
	}

	private String page(String text)
	{
		StringBuilder html = new StringBuilder();

		html.append("<tr>");
		html.append("<td width=20></td>");
		html.append("<td width=690 height=15 align=left valign=top>");
		html.append(text);
		html.append("</td>");
		html.append("</tr>");

		return html.toString();
	}

	public void showClassPage(Player player)
	{
		ClassId classId = player.getClassId();
		int jobLevel = classId.getLevel();
		int level = player.getLevel();

		String html = "";

		if(Config.BBS_CLASS_MASTERS_ALLOW_LIST.isEmpty() || !Config.BBS_CLASS_MASTERS_ALLOW_LIST.contains(jobLevel))
			jobLevel = 4;

		String content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/career/index.htm", player);
		String template = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/career/template.htm", player);
		String block = null;
		if((level >= 20 && jobLevel == 1 || level >= 40 && jobLevel == 2 || level >= 76 && jobLevel == 3) && Config.BBS_CLASS_MASTERS_ALLOW_LIST.contains(jobLevel))
		{
			html += ("<table width=755>");
			html += (page(("<center>" + new CustomMessage("career.choice.class").addString(player.getName()).toString(player) + "</center>")));
			html += ("</table>");

			int id = jobLevel - 1;
			for(ClassId cid : ClassId.VALUES)
			{
				if(cid == ClassId.inspector)
					continue;

				if(cid.childOf(classId) && cid.level() == classId.level() + 1)
				{
					block = template;
					block = block.replace("{icon}", "icon.etc_royal_membership_i00");
					block = block.replace("{name}", Util.className(player, cid.getId()));
					switch(payType(id))
					{
						case 0:
							block = block.replace("{action_name}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, Config.BBS_CLASS_MASTER_PRICE_COUNT[id], Config.BBS_CLASS_MASTER_PRICE_ITEM[id])).toString(player));
							block = block.replace("{link}", "bypass _bbscareer:change_class:" + cid.getId() + ":" + id + ":0");
							break;
						case 1:
							block = block.replace("{action_name}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, Config.BBS_CLASS_MASTER_PRICE_SECOND_COUNT[id], Config.BBS_CLASS_MASTER_PRICE_SECOND_ITEM[id])).toString(player));
							block = block.replace("{link}", "bypass _bbscareer:change_class:" + cid.getId() + ":" + id + ":1");
							break;
						case 2:
							block = block.replace("{action_name}", new CustomMessage("scripts.services.cost.choice").toString(player));
							block = block.replace("{link}", "bypass _bbsbypass:services.Class:choice " + cid.getId() + " " + id + ";_bbscareer");
							break;
					}

					block = block.replace("{value}", new CustomMessage("career.change").toString(player));
					html += block;
				}
			}
			content = content.replace("{info}", "");
		}
		else
		{
			String info = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/career/info.htm", player);
			info = info.replace("{current}", Util.className(player, player.getClassId().getId()));

			html += ("<table width=755>");

			switch(jobLevel)
			{
				case 1:
				{
					String need = new CustomMessage("career.profession.need").addNumber(20).toString(player);
					info = info.replace("{info}", need);
					break;
				}
				case 2:
				{
					String need = new CustomMessage("career.profession.need").addNumber(40).toString(player);
					info = info.replace("{info}", need);
					break;
				}
				case 3:
				{
					String need = new CustomMessage("career.profession.need").addNumber(76).toString(player);
					info = info.replace("{info}", need);
					break;
				}
				case 4:
					info = info.replace("{info}", "You have learned all available professions");
			}
			content = content.replace("{info}", info);
			html += ("</table>");
		}

		if(Config.BBS_CLASS_MASTER_ADD_SUB_CLASS)
		{
			block = template;
			block = block.replace("{icon}", "icon.etc_quest_subclass_reward_i00");
			block = block.replace("{name}", new CustomMessage("career.subclass.add.info").toString(player));
			block = block.replace("{link}", "bypass _bbsbypass:services.Subclass:page choice;_bbscareer");
			block = block.replace("{action_name}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[0], (int) Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[1])).toString(player));
			block = block.replace("{value}", new CustomMessage("career.subclass.add").toString(player));
			html += block;
		}

		if(Config.BBS_CLASS_MASTER_CHANGE_SUB_CLASS)
		{
			block = template;
			block = block.replace("{icon}", "icon.etc_quest_subclass_reward_i00");
			block = block.replace("{name}", new CustomMessage("career.subclass.change.info").toString(player));
			block = block.replace("{link}", "bypass _bbsbypass:services.Subclass:change;_bbscareer");
			block = block.replace("{action_name}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, Config.BBS_CLASS_MASTER_SUB_CHANGE_PRICE_COUNT[0], (int) Config.BBS_CLASS_MASTER_SUB_CHANGE_PRICE_COUNT[1])).toString(player));
			block = block.replace("{value}", new CustomMessage("career.subclass.change").toString(player));
			html += block;
		}

		if(Config.BBS_CLASS_MASTER_CANCEL_SUB_CLASS)
		{
			block = template;
			block = block.replace("{icon}", "icon.etc_quest_subclass_reward_i00");
			block = block.replace("{name}", new CustomMessage("career.subclass.cancel.info").toString(player));
			block = block.replace("{link}", "bypass _bbsbypass:services.Subclass:cancel;_bbscareer");
			block = block.replace("{action_name}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, Config.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[0], (int) Config.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[1])).toString(player));
			block = block.replace("{value}", new CustomMessage("career.subclass.cancel").toString(player));
			html += block;
		}

		if(Config.BBS_CLASS_MASTER_BUY_NOBLESSE)
		{
			if(!player.isNoble())
			{
				block = template;
				block = block.replace("{icon}", "icon.skill1323");
				block = block.replace("{name}", new CustomMessage("career.noblesse.buy.info").toString(player));
				block = block.replace("{link}", "bypass _bbsbypass:services.NoblessSell:get;_bbscareer");
				block = block.replace("{action_name}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, Config.SERVICES_NOBLESS_SELL_PRICE, (int) Config.SERVICES_NOBLESS_SELL_ITEM)).toString(player));
				block = block.replace("{value}", new CustomMessage("career.noblesse.buy").toString(player));
				html += block;
			}
			else
			{
				block = template;
				block = block.replace("{icon}", "icon.skill1323");
				block = block.replace("{name}", new CustomMessage("career.noblesse.buy.info").toString(player));
				block = block.replace("{link}", "bypass _bbscareer");
				block = block.replace("{action_name}", new CustomMessage("career.isnoblesse").toString(player));
				block = block.replace("{value}", "...");
				html += block;
			}
		}

		content = content.replace("{classmaster}", html.toString());
		ShowBoard.separateAndSend(content, player);
	}

	public void changeClass(Player player, int classID, int id, int pay)
	{
		if(player == null)
			return;
		int item = 0;
		long count = -1;

		switch(pay)
		{
			case 0:
				item = Config.BBS_CLASS_MASTER_PRICE_ITEM[id];
				count = Config.BBS_CLASS_MASTER_PRICE_COUNT[id];
				break;
			case 1:
				item = Config.BBS_CLASS_MASTER_PRICE_SECOND_ITEM[id];
				count = Config.BBS_CLASS_MASTER_PRICE_SECOND_COUNT[id];
				break;
		}

		if(Util.getPay(player, item, count, true))
		{
			if(player.getClassId().getLevel() == 3)
				player.sendPacket(SystemMsg.CONGRATULATIONS__YOUVE_COMPLETED_YOUR_THIRDCLASS_TRANSFER_QUEST); // для 3 профы
			else
				player.sendPacket(SystemMsg.CONGRATULATIONS__YOUVE_COMPLETED_A_CLASS_TRANSFER); // для 1 и 2 профы

			player.setClassId(classID, false, false);
			player.broadcastCharInfo();

		}
		showClassPage(player);
	}

	private int payType(int id)
	{
		if(Config.BBS_CLASS_MASTER_PRICE_ITEM[id] != 0 && Config.BBS_CLASS_MASTER_PRICE_SECOND_ITEM[id] == 0)
			return 0;
		else if(Config.BBS_CLASS_MASTER_PRICE_ITEM[id] == 0 && Config.BBS_CLASS_MASTER_PRICE_SECOND_ITEM[id] != 0)
			return 1;
		else if(Config.BBS_CLASS_MASTER_PRICE_ITEM[id] != 0 && Config.BBS_CLASS_MASTER_PRICE_SECOND_ITEM[id] != 0)
			return 2;
		return 0;
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}