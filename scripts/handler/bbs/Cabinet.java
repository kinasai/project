package handler.bbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.PremiumHolder;
import com.l2cccp.gameserver.data.xml.holder.PromoCodeHolder;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.premium.PremiumAccount;
import com.l2cccp.gameserver.model.promocode.AddLevelPromoCodeReward;
import com.l2cccp.gameserver.model.promocode.ExpPromoCodeReward;
import com.l2cccp.gameserver.model.promocode.ItemPromoCodeReward;
import com.l2cccp.gameserver.model.promocode.PremiumPromoCodeReward;
import com.l2cccp.gameserver.model.promocode.PromoCode;
import com.l2cccp.gameserver.model.promocode.PromoCodeReward;
import com.l2cccp.gameserver.model.promocode.SetLevelPromoCodeReward;
import com.l2cccp.gameserver.model.promocode.SpPromoCodeReward;
import com.l2cccp.gameserver.network.authcomm.AuthServerCommunication;
import com.l2cccp.gameserver.network.authcomm.gs2as.ChangePassword;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.utils.DeclensionKey;
import com.l2cccp.gameserver.utils.HtmlUtils;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

public class Cabinet extends ScriptBbsHandler
{
	private static final Logger _log = LoggerFactory.getLogger(Cabinet.class);
	private static String _msg;

	private static Cabinet _instance = new Cabinet();

	public static Cabinet getInstance()
	{
		return _instance;
	}

	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbscabinet" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.COMMUNITYBOARD_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			Util.communityNextPage(player, "_bbshome");
			return;
		}

		String html = "";
		if(bypass.startsWith("_bbscabinet"))
		{
			String[] page = bypass.split(":");
			if(page[1].startsWith("security"))
			{
				if(page[2].startsWith("lockip"))
					Security.lock(player, true, false);
				else if(page[2].startsWith("unlockip"))
					Security.unlock(player, true, false);
				else if(page[2].startsWith("lockhwid"))
					Security.lock(player, false, true);
				else if(page[2].startsWith("unlockhwid"))
					Security.unlock(player, false, true);
				else if(page[2].startsWith("share"))
					Security.share(player, true, false);
				else if(page[2].startsWith("unshare"))
					Security.share(player, false, true);

				String[] s = bypass.split(";");
				Util.communityNextPage(player, s[1]);
				return;
			}
			else if(page[1].equals("show") || page[1].equals("games") || page[1].equals("body") || page[1].equals("code"))
			{
				if(page[1].equals("games"))
					html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/cabinet/games/" + page[2] + ".htm", player);
				else if(page[1].equals("body"))
				{
					if(page[2].equals("show"))
					{
						html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/cabinet/body/" + page[3] + ".htm", player);

						ItemInstance mypeace = player.getInventory().getItemByObjectId(player.getMyPeace());
						ItemInstance newpeace = player.getInventory().getItemByObjectId(player.getNewPeace());

						html = html.replace("{info}", doblock(player, mypeace, newpeace));
						html = html.replace("{my_id}", String.valueOf(mypeace == null ? 0 : mypeace.getObjectId()));
						html = html.replace("{new_id}", String.valueOf(newpeace == null ? 0 : newpeace.getObjectId()));

						html = html.replace("{my_type}", String.valueOf(mypeace == null ? "<font color=\"FF0000\">...</font>" : getBodyPart(player, mypeace)));
						html = html.replace("{new_type}", String.valueOf(newpeace == null ? "<font color=\"FF0000\">...</font>" : getBodyPart(player, newpeace)));
						html = html.replace("{my_bypass}", String.valueOf(mypeace == null ? "<button action=\"bypass _bbsbypass:services.Body.Peace:page choice_my;_bbscabinet:body:show:peace\" width=\"15\" height=\"15\" back=\"L2UI_CT1.Button_DF_Input_Down\" fore=\"L2UI_CT1.Button_DF_Input\">" : "<button action=\"bypass _bbsbypass:services.Body.Peace:removemy;_bbscabinet:body:show:peace\" width=\"15\" height=\"15\" back=\"L2UI_CT1.Button_DF_Delete_Down\" fore=\"L2UI_CT1.Button_DF_Delete\">"));
						html = html.replace("{new_bypass}", String.valueOf(newpeace == null ? "<button action=\"bypass _bbsbypass:services.Body.Peace:visuallist;_bbscabinet:body:show:peace\" width=\"15\" height=\"15\" back=\"L2UI_CT1.Button_DF_Input_Down\" fore=\"L2UI_CT1.Button_DF_Input\">" : "<button action=\"bypass _bbsbypass:services.Body.Peace:removevisual;_bbscabinet:body:show:peace\" width=\"15\" height=\"15\" back=\"L2UI_CT1.Button_DF_Delete_Down\" fore=\"L2UI_CT1.Button_DF_Delete\">"));
						html = html.replace("{my_name}", String.valueOf(mypeace == null ? "<font color=\"FF0000\">" + new CustomMessage("visualchange.select.item").toString(player) + "</font>" : mypeace.getName()));
						html = html.replace("{new_name}", String.valueOf(newpeace == null ? "<font color=\"FF0000\">" + new CustomMessage("visualchange.select.item").toString(player) + "</font>" : newpeace.getName()));
						html = html.replace("{remove_price}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, Config.BBS_BODY_REMOVE_ITEM[0], Config.BBS_BODY_REMOVE_ITEM[1])).toString(player));

						html = html.replace("{my_icon}", String.valueOf(mypeace == null ? "icon.NOIMAGE" : mypeace.getTemplate().getIcon()));
						html = html.replace("{new_icon}", String.valueOf(newpeace == null ? "icon.NOIMAGE" : newpeace.getTemplate().getIcon()));
						html = html.replace("{my_panel}", String.valueOf(mypeace == null ? "L2UI_CT1.ItemWindow_DF_SlotBox_Disable" : "icon.panel_2"));
						html = html.replace("{new_panel}", String.valueOf(newpeace == null ? "L2UI_CT1.ItemWindow_DF_SlotBox_Disable" : "icon.panel_2"));
					}
					else if(page[2].equals("set"))
					{
						String data = page[3];
						if(data.equals("my"))
						{
							int id = Integer.parseInt(page[4]);
							if(check(player, id, false))
								player.setMyPeace(id);
							else
								player.sendMessage(new CustomMessage("visualchange.incorrect.type"));
						}
						else if(data.equals("new"))
						{
							int id = Integer.parseInt(page[4]);
							if(check(player, id, false))
							{
								ItemInstance my = player.getInventory().getItemByObjectId(player.getMyPeace());
								ItemInstance visual = player.getInventory().getItemByObjectId(id);
								if(!ChangeVisualItemIdHelper.isValid(my.getTemplate(), visual.getTemplate()))
									player.sendMessage(new CustomMessage("visualchange.incorrect.new"));
								else
									player.setNewPeace(id);
							}
							else
								player.sendMessage(new CustomMessage("visualchange.incorrect.type"));
						}

						Util.communityNextPage(player, "_bbscabinet:body:show:" + page[5]);
						return;
					}
					else if(page[2].equals("change"))
					{
						int my_id = Integer.parseInt(page[3]);
						int new_id = Integer.parseInt(page[4]);

						ItemInstance item = player.getInventory().getItemByObjectId(my_id);
						ItemInstance newitem = player.getInventory().getItemByObjectId(new_id);
						int id = item.isArmor() && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_COSTUME ? Config.BBS_BODY_CHANGE_ARMOR[1] : item.isDecoration() ? Config.BBS_BODY_CHANGE_ACCESSORIES[1] : item.isWeapon() ? Config.BBS_BODY_CHANGE_WEAPON[1] : item.isArmor() && item.getTemplate().getBodyPart() == ItemTemplate.SLOT_COSTUME ? Config.BBS_BODY_CHANGE_COSTUME[1] : 1000000000;
						int count = item.isArmor() && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_COSTUME ? Config.BBS_BODY_CHANGE_ARMOR[0] : item.isDecoration() ? Config.BBS_BODY_CHANGE_ACCESSORIES[0] : item.isWeapon() ? Config.BBS_BODY_CHANGE_WEAPON[0] : item.isArmor() && item.getTemplate().getBodyPart() == ItemTemplate.SLOT_COSTUME ? Config.BBS_BODY_CHANGE_COSTUME[0] : 1000000000;

						ConfirmDlg dlg = new ConfirmDlg(SystemMsg.S1, 60000).addString(new CustomMessage("visualchange.ask").addString(item.getName()).addString(newitem.getName()).addString(Util.formatAdena(count)).addString(Util.getItemName(id)).toString(player));

						player.ask(dlg, new Ask(my_id, new_id));
						Util.communityNextPage(player, "_bbscabinet:body:show:" + page[5]);
						return;
					}
				}
				else if(page[1].equals("code"))
				{
					String[] _code = bypass.split(" ");

					html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/cabinet/code.htm", player);
					String codename = "";
					if(page[2].startsWith("enter") && _code.length > 1)
					{
						for(int i = 1; i < _code.length; i++)
						{
							codename += (i > 1 ? " " : "") + _code[i];
						}

						PromoCodeHolder.ActivateResult code = PromoCodeHolder.getInstance().tryActivate(player, codename);

						CustomMessage result;
						switch(code)
						{
							case ALREADY:
								result = new CustomMessage("cabinet.code.already");
								break;
							case NOT_FOR_YOU:
								result = new CustomMessage("cabinet.code.not.for.you");
								break;
							case OK:
								result = new CustomMessage("cabinet.code.ok");
								showReward(codename, player);
								player.broadcastPacket(new MagicSkillUse(player, player, 6234, 1, 1000, 0));
								break;
							case OUT_OF_DATE:
								result = new CustomMessage("cabinet.code.out.of.date");
								break;
							case OUT_OF_LIMIT:
								result = new CustomMessage("cabinet.code.out.of.limit");
								break;
							case INVALID_TRY_LATER:
							default:
								result = new CustomMessage("cabinet.code.invalid.try.later").addString(codename);
								break;
						}

						html = html.replace("{result}", result.toString(player));
					}
					else
						html = html.replace("{result}", "<font color=\"222222\">...</font>");
				}
				else if(page[1].equals("show") && page[2].equals("security"))
				{
					html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/cabinet/security.htm", player);
					DateFormat TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

					AccountLog data = getLogAccount(player.getAccountName());

					for(int i = 0; i < 10; i++)
					{
						try
						{
							html = html.replace("{last_ip_" + (i + 1) + "}", data.ip[i]);
							html = html.replace("{last_time_" + (i + 1) + "}", data.time[i] == 0L ? "..." : TIME_FORMAT.format(new Date(data.time[i] * 1000)));
						}
						catch(NullPointerException e)
						{
							html = html.replace("{last_ip_" + (i + 1) + "}", "...");
							html = html.replace("{last_time_" + (i + 1) + "}", "...");
						}
					}

					html = html.replace("{bypass_ip}", Security.check(player, true, false, false, false, false, false));
					html = html.replace("{bypass_hwid}", Security.check(player, false, true, false, false, false, false));
					html = html.replace("{status_ip}", Security.check(player, false, false, true, false, false, false));
					html = html.replace("{status_hwid}", Security.check(player, false, false, false, true, false, false));
					html = html.replace("{status_share}", Security.check(player, false, false, false, false, true, false));
					html = html.replace("{bypass_share}", Security.check(player, false, false, false, false, false, true));
					html = html.replace("{status_pin}", Security.PIN(player, true));
					html = html.replace("{bypass_pin}", Security.PIN(player, false));
				}
				else
					html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/cabinet/" + page[2] + ".htm", player);

				final String no = new CustomMessage("common.result.no").toString(player);
				html = html.replace("<?player_name?>", String.valueOf(player.getName()));
				html = html.replace("<?player_class?>", String.valueOf(Util.className(player, player.getClassId().getId())));
				html = html.replace("<?player_clan1?>", String.valueOf(player.getClan() != null ? player.getClan().getName() : "<font color=\"FF0000\">" + no + "</font>"));
				html = html.replace("<?player_ally?>", String.valueOf(player.getClan() != null && player.getClan().getAlliance() != null ? player.getClan().getAlliance().getAllyName() : "<font color=\"FF0000\">" + no + "</font>"));
				html = html.replace("<?player_level?>", String.valueOf(player.getLevel()));
				html = html.replace("<?player_pvp?>", String.valueOf(player.getPvpKills()));
				html = html.replace("<?player_pk?>", String.valueOf(player.getPkKills()));
				html = html.replace("<?online_time?>", TimeUtils.formatTime((int) player.getOnlineTime() / 1000, player.getLanguage()));
				html = html.replace("<?premium_img?>", String.valueOf(images(player)));

				if(page[2].equals("password"))
				{
					html = html.replace("<?password_change_result?>", String.valueOf(player.getPasswordResult()));
					html = html.replace("<?n1?>", String.valueOf(doCaptcha(true, false)));
					html = html.replace("<?n2?>", String.valueOf(doCaptcha(false, true)));
				}
			}
			else if(page[1].startsWith("password"))
			{
				String[] s = bypass.split(" ");
				String old;
				String newPass1;
				String newPass2;
				String n1;
				String n2;
				String captcha;
				try
				{
					old = s[1];
					newPass1 = s[2];
					newPass2 = s[3];
					n1 = s[4];
					n2 = s[5];
					captcha = s[6];

					changePassword(player, old, newPass1, newPass2, n1, n2, captcha);
				}
				catch(Exception e)
				{
					player.setPasswordResult(new CustomMessage("cabinet.password.incorrect.input").toString(player));
				}
				Util.communityNextPage(player, "_bbscabinet:show:password");
				return;
			}
		}

		ShowBoard.separateAndSend(html, player);
	}

	private void showReward(String _code, Player player)
	{
		PromoCode code = PromoCodeHolder.getInstance().getPromoCode(_code);

		if(code == null)
			return;

		String html = HtmCache.getInstance().getHtml("scripts/services/Promo/index.htm", player);
		String box = HtmCache.getInstance().getHtml("scripts/services/Promo/box.htm", player);
		String body = "";

		for(PromoCodeReward reward : code.getRewards())
		{
			if(reward instanceof ItemPromoCodeReward)
			{
				ItemPromoCodeReward item = (ItemPromoCodeReward) reward;
				String template = box;
				template = template.replace("{icon}", Util.getItemIcon(item._itemId));
				template = template.replace("{data}", new CustomMessage("promo.box.item").addString(Util.getItemName(item._itemId)).addString(Util.formatAdena(item._itemCount)).addString(Util.declension(player.getLanguage(), item._itemCount, DeclensionKey.PIECE)).toString(player));
				body += template;
			}
			else if(reward instanceof SpPromoCodeReward)
			{
				SpPromoCodeReward sp = (SpPromoCodeReward) reward;
				String template = box;
				template = template.replace("{icon}", "icon.etc_sp_point_i00");
				template = template.replace("{data}", new CustomMessage("promo.box.sp").addString(Util.formatAdena(sp._value)).addString(Util.declension(player.getLanguage(), sp._value, DeclensionKey.POINT)).toString(player));
				body += template;
			}
			else if(reward instanceof ExpPromoCodeReward)
			{
				ExpPromoCodeReward exp = (ExpPromoCodeReward) reward;
				String template = box;
				template = template.replace("{icon}", "icon.etc_exp_point_i00");
				template = template.replace("{data}", new CustomMessage("promo.box.exp").addString(Util.formatAdena(exp._value)).addString(Util.declension(player.getLanguage(), exp._value, DeclensionKey.POINT)).toString(player));
				body += template;
			}
			else if(reward instanceof SetLevelPromoCodeReward)
			{
				SetLevelPromoCodeReward level = (SetLevelPromoCodeReward) reward;
				String template = box;
				template = template.replace("{icon}", "icon.etc_exp_point_i00");
				template = template.replace("{data}", new CustomMessage("promo.box.level").addNumber(level._level).toString(player));
				body += template;
			}
			else if(reward instanceof AddLevelPromoCodeReward)
			{
				AddLevelPromoCodeReward level = (AddLevelPromoCodeReward) reward;
				String template = box;
				template = template.replace("{icon}", "icon.etc_exp_point_i00");
				template = template.replace("{data}", new CustomMessage("promo.box.level").addNumber(level._level).toString(player));
				body += template;
			}
			else if(reward instanceof PremiumPromoCodeReward)
			{
				PremiumPromoCodeReward premium = (PremiumPromoCodeReward) reward;
				String template = box;
				template = template.replace("{icon}", "branchsys2.br_vitality_day_i00");
				PremiumAccount pa = PremiumHolder.getInstance().getPremium(premium._id);
				final int days = (int) TimeUtils.addDay(premium._days) / 1000;
				template = template.replace("{data}", new CustomMessage("promo.box.premium").addString(String.valueOf(pa.getName())).addString(TimeUtils.formatTime(days, player.getLanguage())).toString(player));
				body += template;
			}
		}

		html = html.replace("{body}", body);
		html = html.replace("{code}", _code);

		HtmlMessage page = new HtmlMessage(5).setHtml(HtmlUtils.bbParse(html));
		player.sendPacket(page);
	}

	public String doblock(Player player, ItemInstance my_item, ItemInstance new_item)
	{
		String text = "";

		if(my_item == null)
			text = new CustomMessage("visualchange.need.1").toString(player);
		else if(new_item == null)
			text = new CustomMessage("visualchange.need.2").toString(player);
		else
			text = new CustomMessage("visualchange.info").toString(player);

		return text;
	}

	public boolean check(Player player, int id, boolean remove)
	{
		ItemInstance item = player.getInventory().getItemByObjectId(id);

		if(item == null)
			return false;

		if(!remove && item.getVisualItemId() != 0)
			return false;

		switch(item.getTemplate().getBodyPart())
		{
			case ItemTemplate.SLOT_HEAD:
			case ItemTemplate.SLOT_GLOVES:
			case ItemTemplate.SLOT_CHEST:
			case ItemTemplate.SLOT_FULL_ARMOR:
			case ItemTemplate.SLOT_LEGS:
			case ItemTemplate.SLOT_FEET:
			case ItemTemplate.SLOT_BACK:
			case ItemTemplate.SLOT_R_HAND:
			case ItemTemplate.SLOT_L_HAND:
			case ItemTemplate.SLOT_LR_HAND:
			case ItemTemplate.SLOT_HAIR:
			case ItemTemplate.SLOT_DHAIR:
			case ItemTemplate.SLOT_HAIRALL:
				return true;
			case ItemTemplate.SLOT_COSTUME:
				ItemInstance my = player.getInventory().getItemByObjectId(player.getMyPeace());
				return my != null && (my.getTemplate().getBodyPart() == ItemTemplate.SLOT_CHEST || my.getTemplate().getBodyPart() == ItemTemplate.SLOT_CHEST);
			default:
				return false;
		}
	}

	public static void changePassword(Player player, String old, String newPass1, String newPass2, String number1, String number2, String captcha)
	{
		if(player == null)
			return;

		if(old.equals(newPass1))
		{
			player.setPasswordResult(new CustomMessage("cabinet.password.incorrect.newisold").toString(player));
			return;
		}

		if(newPass1.length() < 4 || newPass1.length() > 20)
		{
			player.setPasswordResult(new CustomMessage("cabinet.password.incorrect.size").toString(player));
			return;
		}

		if(!newPass1.equals(newPass2))
		{
			player.setPasswordResult(new CustomMessage("cabinet.password.incorrect.confirmation").toString(player));
			return;
		}

		if(Integer.valueOf(number1) + Integer.valueOf(number2) != Integer.valueOf(captcha))
		{
			int captchaA = Integer.valueOf(number1) + Integer.valueOf(number2);
			player.setPasswordResult(new CustomMessage("cabinet.password.incorrect.captcha").addNumber(captchaA).toString(player));
			return;
		}

		AuthServerCommunication.getInstance().sendPacket(new ChangePassword(player.getAccountName(), old, newPass1));
		return;
	}

	public int doCaptcha(boolean n1, boolean n2)
	{
		int captcha = 0;
		if(n1)
			captcha = Rnd.get(1, 499);
		if(n2)
			captcha = Rnd.get(1, 499);

		return captcha;
	}

	public String images(Player player)
	{
		if(player.hasBonus())
			_msg = "<img src=\"branchsys.primeitem_symbol\" width=\"14\" height=\"14\">";
		else
			_msg = "<img src=\"branchsys.br_freeserver_mark\" width=\"14\" height=\"14\">";
		return _msg;
	}

	public String getBodyPart(Player player, ItemInstance item)
	{
		if(item.getBodyPart() == ItemTemplate.SLOT_R_EAR || item.getBodyPart() == ItemTemplate.SLOT_L_EAR)
			return new CustomMessage("common.item.template.name.1").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_NECK)
			return new CustomMessage("common.item.template.name.2").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_R_FINGER || item.getBodyPart() == ItemTemplate.SLOT_L_FINGER)
			return new CustomMessage("common.item.template.name.3").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_HEAD)
			return new CustomMessage("common.item.template.name.4").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_L_HAND)
			return new CustomMessage("common.item.template.name.5").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_R_HAND || item.getBodyPart() == ItemTemplate.SLOT_LR_HAND)
			return new CustomMessage("common.item.template.name.6").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_GLOVES)
			return new CustomMessage("common.item.template.name.7").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_CHEST)
			return new CustomMessage("common.item.template.name.8").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_LEGS)
			return new CustomMessage("common.item.template.name.9").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_FEET)
			return new CustomMessage("common.item.template.name.10").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_BACK)
			return new CustomMessage("common.item.template.name.11").toString(player);
		else if(item.getBodyPart() == ItemTemplate.SLOT_FULL_ARMOR)
			return new CustomMessage("common.item.template.name.12").toString(player);
		else if(item.isDecoration())
			return new CustomMessage("common.item.template.name.13").toString(player);
		return new CustomMessage("common.not.recognized").toString(player);
	}

	private class Ask implements OnAnswerListener
	{
		private int my_id;
		private int new_id;

		protected Ask(int my_id, int new_id)
		{
			this.my_id = my_id;
			this.new_id = new_id;
		}

		@Override
		public void sayYes(Player player)
		{
			if(player == null || !player.isOnline())
				return;

			ItemInstance targetItem = player.getInventory().getItemByObjectId(my_id);
			ItemInstance visualItem = player.getInventory().getItemByObjectId(new_id);

			if(targetItem != null && visualItem != null)
				ChangeVisualItemIdHelper.changeVisualItemId(player, targetItem.getObjectId(), visualItem.getObjectId());

			player.setMyPeace(0);
			player.setNewPeace(0);
			Util.communityNextPage(player, "_bbscabinet:body:show:peace");
			return;
		}

		@Override
		public void sayNo(Player player)
		{
			player.setMyPeace(0);
			player.setNewPeace(0);
		}
	}

	public AccountLog getLogAccount(String account)
	{
		AccountLog data = new AccountLog();
		if(Config.GAME_SERVER_LOGIN_DATABASE_NAME.equals(StringUtils.EMPTY))
		{
			for(int i = 0; i < 10; i++)
			{
				data.ip[i] = "...";
				data.time[i] = 0L;
			}

			return data;
		}

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		int number = 0;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM " + Config.GAME_SERVER_LOGIN_DATABASE_NAME + ".account_log WHERE login=? ORDER BY time DESC LIMIT 0, 10;");
			statement.setString(1, account);
			rset = statement.executeQuery();

			while(rset.next())
			{
				if(!rset.getString("login").isEmpty())
				{
					data.ip[number] = rset.getString("ip");
					data.time[number] = rset.getLong("time");
				}
				else
				{
					data.ip[number] = "...";
					data.time[number] = 0L;
				}
				number++;
			}
		}
		catch(Exception e)
		{
			_log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return data;
	}

	public class AccountLog
	{
		private String[] ip = new String[10];
		private Long[] time = new Long[10];
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}
