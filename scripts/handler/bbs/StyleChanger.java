package handler.bbs;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class StyleChanger extends ScriptBbsHandler
{
	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbshair", "_bbsface" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{

		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(bypass.startsWith("_bbshair")) // _bbshair:try:style:1 || _bbshair:buy:style:1
		{
			String[] data = bypass.split(":");
			final boolean buy = data[1].equals("buy");
			final String type = data[2];
			final int id = Integer.parseInt(data[3]);
			if(type.equals("style"))
			{
				if(buy)
				{}
				else
				{}
				//do change hair style!!
			}
			else if(type.equals("color"))
			{
				if(buy)
				{}
				else
				{}
				//do change hair color!!
			}
		}
		else if(bypass.startsWith("_bbsface"))
		{
			String[] data = bypass.split(":");
			final boolean buy = data[1].equals("buy");
			final int id = Integer.parseInt(data[2]);
			if(buy)
			{}
			else
			{}
			//do change hair color!!
		}
	}

	public void ask(Player player, NpcInstance npc, String[] arg)
	{
		int id = Util.isNumber(arg[0]) ? Integer.parseInt(arg[0]) : 0;

		String msg = new CustomMessage("hair.ask.bbs").addString(Util.formatPay(player, Config.SERVICES_HAIR_CHANGE_COUNT, Config.SERVICES_HAIR_CHANGE_ITEM_ID)).toString(player);
		ConfirmDlg ask = new ConfirmDlg(SystemMsg.S1, 60000);
		ask.addString(msg);

		player.ask(ask, new AnswerListener(id));
	}

	private class AnswerListener implements OnAnswerListener
	{
		private int id;

		protected AnswerListener(int id)
		{
			this.id = id;
		}

		@Override
		public void sayYes(Player player)
		{
			if(!player.isOnline() || !correct(id))
				return;

			changeHair(player, id);
		}

		@Override
		public void sayNo(Player player)
		{}
	}

	private final static boolean correct(int id)
	{
		return id >= 0 || id <= 6;
	}

	private final static void changeHair(Player player, int id)
	{
		if(Util.getPay(player, Config.SERVICES_HAIR_CHANGE_ITEM_ID, Config.SERVICES_HAIR_CHANGE_COUNT, true))
		{
			player.setHairStyle(id);
			player.sendMessage(new CustomMessage("hair.change"));
			player.broadcastPacket(new MagicSkillUse(player, player, 6696, 1, 1000, 0));
			player.broadcastCharInfo();
		}
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}