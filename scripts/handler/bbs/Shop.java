package handler.bbs;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.MultiSellHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.utils.Util;

public class Shop extends ScriptBbsHandler
{

	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbsshop" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.COMMUNITYBOARD_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		String html = "";
		if(bypass.startsWith("_bbsshop"))
		{
			String[] link = bypass.split(":");
			if(link[1].equals("open"))
			{
				String[] data = bypass.split(";");

				int listId = Integer.parseInt(data[0].split(":")[2]);

				if(data.length > 1)
					Util.communityNextPage(player, data[1]);

				MultiSellHolder.getInstance().SeparateAndSend(listId, player, -1, 0);
				return;
			}
			else
				html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/shop/" + link[1] + ".htm", player);
		}

		ShowBoard.separateAndSend(html, player);
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}