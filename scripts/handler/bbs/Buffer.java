package handler.bbs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import com.l2cccp.commons.collections.CollectionUtils;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.dao.CharacterBuffsDAO;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.BuffSchemeHolder;
import com.l2cccp.gameserver.data.xml.holder.BuffsHolder;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill.SkillType;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.bbs.buffer.Buff;
import com.l2cccp.gameserver.model.bbs.buffer.GenerateElement;
import com.l2cccp.gameserver.model.bbs.buffer.Scheme;
import com.l2cccp.gameserver.model.bbs.buffer.Schemes;
import com.l2cccp.gameserver.model.entity.events.impl.SiegeEvent;
import com.l2cccp.gameserver.model.entity.events.impl.SingleMatchEvent;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.Util;

public class Buffer extends ScriptBbsHandler
{
	private static class BuffTask extends RunnableImpl
	{
		private final List<Buff> _buffList;
		private int _index;
		private Playable _playable;

		private BuffTask(List<Buff> buffList, Playable playable)
		{
			_buffList = buffList;
			_playable = playable;
		}

		@Override
		public void runImpl() throws Exception
		{
			Buff poll = CollectionUtils.safeGet(_buffList, _index);
			if(poll == null)
				return;

			buff0(poll.getId(), poll.getLevel(), _playable);

			_index++;

			ThreadPoolManager.getInstance().schedule(this, 25L);
		}
	}

	private static ArrayList<String> pageBuffPlayer;
	private static ArrayList<String> pageBuffPet;
	private static StringBuilder buffSchemes = new StringBuilder();

	@Override
	public String[] getBypassCommands()
	{
		return new String[] {
				"_bbsbuffer",
				"_bbsplayerbuffer",
				"_bbspetbuffer",
				"_bbscastfast",
				"_bbscastbuff",
				"_bbscastgroupbuff",
				"_bbsbuffersave",
				"_bbsbufferuse",
				"_bbsbufferdelete",
				"_bbsbufferheal",
				"_bbsbufferremovebuffs",
				"_bbsbuffertype" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.BBS_BUFFER_ALLOWED_BUFFER)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			Util.communityNextPage(player, "_bbshome");
			return;
		}
		else if(!check(player))
		{
			Util.communityNextPage(player, "_bbshome");
			return;
		}

		String html = "";
		if(bypass.equals("_bbsbuffer"))
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/buffer/index.htm", player);
			html = html.replace("%scheme%", buffSchemes.toString());

			String template = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/buffer/my-sheme.htm", player);
			String block = "";
			String list = null;
			for(String name : player.getSchemes().keySet())
			{
				block = template;
				block = block.replace("{bypass}", "bypass _bbsbufferuse " + name + " $Who");
				block = block.replace("{name}", name);
				block = block.replace("{delete}", "bypass _bbsbufferdelete " + name);
				list += block;
			}

			int priceId = Config.BBS_BUFFER_PRICE[0];
			int priceCount = Config.BBS_BUFFER_PRICE[1];

			if(Config.BBS_BUFFER_PRICE_BY_LEVEL)
				priceCount *= player.getLevel();

			if(Config.BBS_BUFFER_PRICE_PREMIUM_MOD && player.hasBonus())
				priceCount *= Config.BBS_BUFFER_PRICE_PREMIUM_MOD_COUNT;
			if(player.getActiveClass().getLevel() < Config.BBS_BUFFER_FREE_LEVEL)
				priceCount = 0;

			html = html.replace("%price%", Util.formatPay(player, priceCount, priceId));

			if(list != null)
				html = html.replace("%buffgrps%", list);
			else
				html = html.replace("%buffgrps%", HtmCache.getInstance().getHtml(Config.BBS_PATH + "/buffer/my-sheme-empty.htm", player));
		}
		else if(bypass.startsWith("_bbsplayerbuffer"))
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/buffer/scheme.htm", player);
			StringTokenizer st1 = new StringTokenizer(bypass, ":");
			st1.nextToken();
			int page = Integer.parseInt(st1.nextToken());
			if(pageBuffPlayer.get(page) != null)
				html = html.replace("%content%", pageBuffPlayer.get(page));
		}
		else if(bypass.startsWith("_bbsbuffertype"))
		{
			StringTokenizer st1 = new StringTokenizer(bypass, ":");
			st1.nextToken();
			final String type = st1.nextToken();
			player.addSessionVar("BuffTarget", type);

			onBypassCommand(player, "_bbsbuffer");
			return;
		}
		else if(bypass.startsWith("_bbspetbuffer"))
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/buffer/scheme.htm", player);
			StringTokenizer st1 = new StringTokenizer(bypass, ":");
			st1.nextToken();
			int page = Integer.parseInt(st1.nextToken());
			if(pageBuffPet.get(page) != null)
				html = html.replace("%content%", pageBuffPet.get(page));
		}
		else if(bypass.startsWith("_bbscastbuff"))
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/buffer/scheme.htm", player);
			StringTokenizer st1 = new StringTokenizer(bypass, ":");
			st1.nextToken();

			int id = Integer.parseInt(st1.nextToken());
			int level = Integer.parseInt(st1.nextToken());
			int page = Integer.parseInt(st1.nextToken());
			String type = st1.nextToken();

			Playable playable = null;
			if("Player".equals(type))
				playable = player;
			else if("Pet".equals(type))
				playable = player.getServitor();

			Buff buff = BuffsHolder.getInstance().get(id);

			int priceId = Config.BBS_BUFFER_PRICE[0];
			int priceCount = Config.BBS_BUFFER_PRICE[1];

			if(Config.BBS_BUFFER_PRICE_BY_LEVEL)
				priceCount *= player.getLevel();

			if(playable != null && buff != null && buff.getLevel() <= level)
			{
				final Buff _buff = BuffsHolder.getInstance().get(id);
				final boolean forbident = _buff.getAllowedClass() != null && !_buff.getAllowedClass().contains(player.getClassId().getId());
				if(!forbident)
				{
					if(player.getActiveClass().getLevel() < Config.BBS_BUFFER_FREE_LEVEL)
						buffList(Collections.singletonList(_buff), playable);
					else if(Util.getPay(player, priceId, priceCount, true))
						buffList(Collections.singletonList(_buff), playable);
				}
				else
					player.sendMessage("You can't use this buff!");
			}

			if("Player".equals(type))
				html = html.replace("%content%", pageBuffPlayer.get(page));
			else if("Pet".equals(type))
				html = html.replace("%content%", pageBuffPet.get(page));
		}
		else if(bypass.startsWith("_bbscastfast"))
		{
			String[] data = bypass.split(";");

			StringTokenizer st1 = new StringTokenizer(data[0], ":");
			st1.nextToken();

			int id = Integer.parseInt(st1.nextToken());
			int level = Integer.parseInt(st1.nextToken());
			String type = st1.nextToken();

			Playable playable = null;
			if("Player".equals(type))
				playable = player;
			else if("Pet".equals(type))
				playable = player.getServitor();

			Buff buff = BuffsHolder.getInstance().get(id);

			int priceId = Config.BBS_BUFFER_PRICE[0];
			int priceCount = Config.BBS_BUFFER_PRICE[1];

			if(Config.BBS_BUFFER_PRICE_BY_LEVEL)
				priceCount *= player.getLevel();

			if(playable != null && buff != null && buff.getLevel() <= level)
			{
				final Buff _buff = BuffsHolder.getInstance().get(id);
				final boolean forbident = _buff.getAllowedClass() != null && !_buff.getAllowedClass().contains(player.getClassId().getId());
				if(!forbident)
				{
					if(player.getActiveClass().getLevel() < Config.BBS_BUFFER_FREE_LEVEL)
						buffList(Collections.singletonList(_buff), playable);
					else if(Util.getPay(player, priceId, priceCount, true))
						buffList(Collections.singletonList(_buff), playable);
				}
				else
					player.sendMessage("You can't use this buff!");
			}

			if(data.length > 1)
				Util.communityNextPage(player, data[1]);
			return;
		}
		else if(bypass.startsWith("_bbscastgroupbuff"))
		{
			StringTokenizer st1 = new StringTokenizer(bypass, " ");
			st1.nextToken();
			int id = Integer.parseInt(st1.nextToken());
			String type = st1.nextToken();
			final Schemes scheme = BuffSchemeHolder.getInstance().get(id);
			int priceId = scheme.getPriceId();
			int priceCount = scheme.getPriceCount();

			Playable playable = null;
			if("Player".equals(type))
				playable = player;
			else if("Pet".equals(type))
				playable = player.getServitor();

			if(playable != null)
			{
				final boolean allowed = scheme.getAllowedClass() == null || scheme.getAllowedClass().contains(player.getClassId().getId());
				if(allowed)
				{
					if(player.getActiveClass().getLevel() < Config.BBS_BUFFER_FREE_LEVEL)
						buffList(scheme.getBuffIds(), playable);
					else if(Util.getPay(player, priceId, priceCount, true))
						buffList(scheme.getBuffIds(), playable);
				}
				else
					player.sendMessage("You can't use this buff scheme!");
			}

			Util.communityNextPage(player, "_bbsbuffer");
			return;
		}
		else if(bypass.startsWith("_bbsbuffersave"))
		{
			if(player.getSchemes().size() >= 5)
			{
				player.sendMessage("You can create only 4 scheme!");
				Util.communityNextPage(player, "_bbsbuffer");
				return;
			}

			StringTokenizer st1 = new StringTokenizer(bypass, " ");

			if(st1.countTokens() < 3)
			{
				Util.communityNextPage(player, "_bbsbuffer");
				return;
			}

			st1.nextToken();

			String name = st1.nextToken();
			String type = st1.nextToken();

			Playable playable = null;
			if("Player".equals(type))
				playable = player;
			else if("Pet".equals(type))
				playable = player.getServitor();

			if(playable == null)
				return;

			if(playable.getEffectList().getAllEffects().size() == 0)
			{
				Util.communityNextPage(player, "_bbsbuffer");
				return;
			}

			if(Util.getPay(player, Config.BBS_BUFFER_SAVE_PRICE_ID, Config.BBS_BUFFER_SAVE_PRICE_ONE, true))
			{
				StringBuilder buffs = new StringBuilder();
				Scheme scheme = new Scheme(name);
				for(Effect effect : playable.getEffectList().getAllEffects())
				{
					SkillEntry skill = effect.getSkill();
					int id = skill.getId();
					int level = skill.getLevel();
					Buff buff = BuffsHolder.getInstance().get(id);

					if(buff != null)
					{
						level = level > buff.getLevel() ? buff.getLevel() : level;
						buffs.append(id).append(",").append(level).append(";");
						scheme.addBuff(id, level);
					}
				}

				player.setScheme(name, scheme);
				CharacterBuffsDAO.getInstance().insert(player, buffs.toString(), name);
			}

			Util.communityNextPage(player, "_bbsbuffer");
			return;
		}
		else if(bypass.startsWith("_bbsbufferuse"))
		{
			StringTokenizer st1 = new StringTokenizer(bypass, " ");
			st1.nextToken();
			String name = st1.nextToken();
			String type = st1.nextToken();

			Playable playable = null;
			if("Player".equals(type))
				playable = player;
			else if("Pet".equals(type))
				playable = player.getServitor();

			int priceId = Config.BBS_BUFFER_PRICE[0];
			int priceCount = Config.BBS_BUFFER_PRICE[1];

			if(Config.BBS_BUFFER_PRICE_BY_LEVEL)
				priceCount *= player.getLevel();

			if(Config.BBS_BUFFER_PRICE_PREMIUM_MOD && player.hasBonus())
				priceCount *= Config.BBS_BUFFER_PRICE_PREMIUM_MOD_COUNT;

			if(playable != null)
			{
				List<Buff> buffs = new ArrayList<Buff>();
				Scheme my = player.getScheme(name);

				if(my != null)
				{
					if(player.getActiveClass().getLevel() > Config.BBS_BUFFER_FREE_LEVEL)
						priceCount *= my.getBuffs().size();
					else
						priceCount = 0;

					if(Util.getPay(player, priceId, priceCount, true))
					{
						for(Map.Entry<Integer, Integer> scheme : my.getBuffs().entrySet())
						{
							final Buff buff = BuffsHolder.getInstance().get(scheme.getKey());

							if(buff != null)
							{
								final boolean forbident = buff.getAllowedClass() != null && !buff.getAllowedClass().contains(player.getClassId().getId());
								if(!forbident)
									buffs.add(buff);
							}
						}
					}

					buffList(buffs, playable);
				}
			}
			Util.communityNextPage(player, "_bbsbuffer");
			return;
		}
		else if(bypass.startsWith("_bbsbufferdelete"))
		{
			StringTokenizer st1 = new StringTokenizer(bypass, " ");
			st1.nextToken();
			String name = st1.nextToken();
			CharacterBuffsDAO.getInstance().delete(player, name);
			player.removeScheme(name);
			Util.communityNextPage(player, "_bbsbuffer");
			return;
		}
		else if(bypass.startsWith("_bbsbufferheal"))
		{
			if(!Config.BBS_BUFFER_RECOVER_HP_MP_CP)
				player.sendMessage(new CustomMessage("scripts.services.off"));
			else if((Config.BBS_BUFFER_RECOVER_CHECK_PVP_FLAG && player.getPvpFlag() > 0) || Config.BBS_BUFFER_RECOVER_CHECK_BATTLE && player.isInCombat())
				player.sendMessage(new CustomMessage("scripts.services.combat.enable"));
			else
			{
				StringTokenizer st1 = new StringTokenizer(bypass, " ");
				st1.nextToken();
				String type = st1.nextToken();
				String target = st1.nextToken();

				Playable playable = null;
				if("Player".equals(target))
					playable = player;
				else if("Pet".equals(target))
					playable = player.getServitor();

				if(playable != null)
				{
					if(Config.BBS_BUFFER_RECOVER_HP_MP_CP_IN_PEACE && !playable.isInPeaceZone())
					{
						if(playable.isPlayer())
							playable.getPlayer().sendMessage("You must be in Peace zone!");
						Util.communityNextPage(player, "_bbsbuffer");
						return;
					}

					if(Util.getPay(player, Config.BS_BUFFER_RECOVER_HP_MP_CP_PRICE[0], Config.BS_BUFFER_RECOVER_HP_MP_CP_PRICE[1], true))
					{
						if("HP".equals(type))
						{
							if(playable.getCurrentHp() != playable.getMaxHp())
							{
								playable.setCurrentHp(playable.getMaxHp(), true, true);
								playable.broadcastPacket(new MagicSkillUse(playable, playable, 6696, 1, 1000, 0));
							}
						}
						else if("MP".equals(type))
						{
							if(playable.getCurrentMp() != playable.getMaxMp())
							{
								playable.setCurrentMp(playable.getMaxMp(), true);
								playable.broadcastPacket(new MagicSkillUse(playable, playable, 6696, 1, 1000, 0));
							}
						}
						else if("CP".equals(type))
						{
							if(playable.getCurrentCp() != playable.getMaxCp())
							{
								playable.setCurrentCp(playable.getMaxCp(), true);
								playable.broadcastPacket(new MagicSkillUse(playable, playable, 6696, 1, 1000, 0));
							}
						}
						else if("ALL".equals(type))
						{
							playable.setCurrentHp(playable.getMaxHp(), true, true);
							playable.setCurrentMp(playable.getMaxMp(), true);
							playable.setCurrentCp(playable.getMaxCp(), true);
							playable.broadcastPacket(new MagicSkillUse(playable, playable, 6696, 1, 1000, 0));
						}
					}
				}
			}
			Util.communityNextPage(player, "_bbsbuffer");
			return;
		}
		else if(bypass.startsWith("_bbsbufferremovebuffs"))
		{
			if(!Config.BBS_BUFFER_CLEAR_BUFF)
				player.sendMessage(new CustomMessage("scripts.services.off"));
			else
			{
				StringTokenizer st1 = new StringTokenizer(bypass, " ");
				st1.nextToken();
				String type = st1.nextToken();

				Playable playable = null;
				if("Player".equals(type))
					playable = player;
				else if("Pet".equals(type))
					playable = player.getServitor();

				if(playable != null)
				{
					for(Effect effect : playable.getEffectList().getAllEffects())
					{
						if(effect.getSkill().getSkillType() == SkillType.BUFF || effect.getSkill().getSkillType() == SkillType.COMBATPOINTHEAL)
							effect.exit();
					}
					playable.broadcastPacket(new MagicSkillUse(playable, playable, 6696, 1, 1000, 0));
				}
			}
			Util.communityNextPage(player, "_bbsbuffer");
			return;
		}
		else
		{
			player.sendMessage("Error");
			return;
		}

		ShowBoard.separateAndSend(html, player);
	}

	private static void genPage(ArrayList<String> list, String type)
	{
		StringBuilder sb = new StringBuilder("<table><tr>");
		sb.append("<td width=70>Navigation: </td>");

		for(int i = 0; i < list.size(); i++)
		{
			sb.append(GenerateElement.buttonTD(String.valueOf(i + 1), "_bbs" + type + "buffer:" + i, 30, 25, "L2UI_CT1.Button_DF_Down", "L2UI_CT1.Button_DF"));
		}

		sb.append("<td>" + GenerateElement.button("Back", "_bbsbuffer", 60, 25, "L2UI_CT1.Button_DF_Down", "L2UI_CT1.Button_DF") + "</td></tr></table><br><br>");

		for(int i = 0; i < list.size(); i++)
		{
			list.set(i, sb.toString() + list.get(i));
		}
	}

	private static void genPageBuff(ArrayList<String> list, int start, String type)
	{
		StringBuilder buffPages = new StringBuilder("<table><tr>");
		int i = start;
		Boolean next = false;
		final int size = BuffsHolder.getInstance().size();
		for(; i < size; i++)
		{
			if(next && i % 6 == 0)
				buffPages.append("</tr><tr>");

			if(next && i % (6 * 4) == 0)
				break;

			final Buff buff = BuffsHolder.getInstance().get().get(i);
			buffPages.append("<td>").append(buttonBuff(buff.getId(), buff.getLevel(), list.size(), type)).append("</td>");
			next = true;
		}
		buffPages.append("</tr></table>");

		list.add(buffPages.toString());

		if(i + 2 <= size)
		{
			genPageBuff(list, i, type);
		}
	}

	private static String buttonBuff(int id, int level, int page, String type)
	{
		String skillId = Integer.toString(id);
		StringBuilder sb = new StringBuilder("<table width=100>");
		String icon;

		if(skillId.length() < 4)
			icon = 0 + skillId;
		else if(skillId.length() < 3)
			icon = 00 + skillId;
		else if(id == 4700 || id == 4699)
			icon = "1331";
		else if(id == 4702 || id == 4703)
			icon = "1332";
		else if(id == 1517)
			icon = "1499";
		else if(id == 1518)
			icon = "1502";
		else
			icon = skillId;

		String name = SkillTable.getInstance().getSkillEntry(id, level).getName();
		name = name.replace("Dance of the", "D.");
		name = name.replace("Dance of", "D.");
		name = name.replace("Song of", "S.");
		name = name.replace("Improved", "I.");
		name = name.replace("Awakening", "A.");
		name = name.replace("Blessing", "Bless.");
		name = name.replace("Protection", "Protect.");
		name = name.replace("Critical", "C.");
		name = name.replace("Condition", "Con.");
		sb.append("<tr><td><center><img src=icon.skill").append(icon).append(" width=32 height=32><br><font color=F2C202>Level ").append(level).append("</font></center></td></tr>");
		sb.append(GenerateElement.buttonTR(name, "_bbscastbuff:" + id + ":" + level + ":" + page + ":" + type, 100, 25, "L2UI_CT1.Button_DF_Down", "L2UI_CT1.Button_DF"));
		sb.append("</table>");
		return sb.toString();
	}

	private static void buffList(List<Buff> list, Playable playable)
	{
		//		for(Buff buff : list)
		//			buff0(buff.getId(), buff.getLevel(), playable);
		new BuffTask(list, playable).run();
	}

	private boolean check(Player player)
	{
		if(Config.ENABLE_OLYMPIAD && Olympiad.isRegisteredInComp(player) || player.isInOlympiadMode())
		{
			player.sendMessage(new CustomMessage("scripts.services.olympiad.enable"));
			return false;
		}
		else if(!Config.BBS_BUFFER_ALLOW_SIEGE && player.getEvent(SiegeEvent.class) != null)
		{
			player.sendMessage(new CustomMessage("scripts.services.siege.disable"));
			return false;
		}
		else if((!Config.BBS_BUFFER_ALLOW_PVP_FLAG && player.getPvpFlag() > 0) || !Config.BBS_BUFFER_ALLOW_BATTLE && player.isInCombat())
		{
			player.sendMessage(new CustomMessage("scripts.services.combat.enable"));
			return false;
		}
		else if(!Config.BBS_BUFFER_ALLOW_EVENTS && player.getEvent(SingleMatchEvent.class) != null)
		{
			player.sendMessage(new CustomMessage("scripts.services.combat.events"));
			return false;
		}
		else if(!checkLevel(player.getLevel()))
		{
			int min = Config.BBS_BUFFER_MIN_LVL;
			int max = Config.BBS_BUFFER_MAX_LVL;

			player.sendMessage("Level should be between " + min + " and " + max);
			return false;
		}
		else
			return true;
	}

	private boolean checkLevel(int level)
	{
		return (level >= Config.BBS_BUFFER_MIN_LVL && level <= Config.BBS_BUFFER_MAX_LVL);
	}

	private static void buff0(int id, int level, Playable playable)
	{
		if(id < 20)
			return;

		SkillEntry skill = SkillTable.getInstance().getSkillEntry(id, level > 0 ? level : SkillTable.getInstance().getMaxLevel(id));
		skill.getEffects(playable, playable, false, false, Config.BBS_BUFFER_ALT_TIME * 60000, 0, 60);
	}

	@Override
	public void onInit()
	{
		super.onInit();

		pageBuffPlayer = new ArrayList<String>();
		pageBuffPet = new ArrayList<String>();

		genPageBuff(pageBuffPlayer, 0, "Player");
		genPage(pageBuffPlayer, "player");

		genPageBuff(pageBuffPet, 0, "Pet");
		genPage(pageBuffPet, "pet");

		for(Entry<Integer, Schemes> map : BuffSchemeHolder.getInstance().get().entrySet())
		{
			final Schemes scheme = BuffSchemeHolder.getInstance().get(map.getKey());
			StringBuilder parametrs = new StringBuilder();
			parametrs.append("_bbscastgroupbuff ").append(scheme.getId()).append(" $Who");

			StringBuilder name = new StringBuilder();
			name.append(scheme.getName());

			buffSchemes.append(GenerateElement.button(name.toString(), parametrs.toString(), 169, 25, "L2UI_CT1.Button_DF_Down", "L2UI_CT1.Button_DF") + "<br1>" + Util.formatAdena(scheme.getPriceCount()) + " " + Util.getItemName(scheme.getPriceId()));
		}
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}