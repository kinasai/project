package handler.bbs;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.StringTokenizer;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.MultiSellHolder;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.handler.bbs.BbsHandlerHolder;
import com.l2cccp.gameserver.handler.bypass.BypassHolder;
import com.l2cccp.gameserver.handler.voicecommands.IVoicedCommandHandler;
import com.l2cccp.gameserver.handler.voicecommands.VoicedCommandHandler;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.Rate;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.scripts.Scripts;
import com.l2cccp.gameserver.tables.ClanTable;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

public class CommunityBoard extends ScriptBbsHandler
{
	private static final Logger _log = LoggerFactory.getLogger(CommunityBoard.class);

	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbshome", "_bbslink", "_bbsmultisell", "_bbsopen", "_bbsscripts", "_bbsbypass", "_bbsnotdone", "_bbsuser" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		StringTokenizer st = new StringTokenizer(bypass, "_");
		String cmd = st.nextToken();
		String html = "";
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if("bbshome".equals(cmd))
		{
			StringTokenizer p = new StringTokenizer(Config.BBS_DEFAULT, "_");
			String dafault = p.nextToken();
			if(dafault.equals(cmd))
			{
				html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/bbs_top.htm", player);

				int favCount = 0;
				Connection con = null;
				PreparedStatement statement = null;
				ResultSet rset = null;
				try
				{
					con = DatabaseFactory.getInstance().getConnection();
					statement = con.prepareStatement("SELECT count(*) as cnt FROM `bbs_favorites` WHERE `object_id` = ?");
					statement.setInt(1, player.getObjectId());
					rset = statement.executeQuery();
					if(rset.next())
						favCount = rset.getInt("cnt");
				}
				catch(Exception e)
				{}
				finally
				{
					DbUtils.closeQuietly(con, statement, rset);
				}

				html = html.replace("<?fav_count?>", String.valueOf(favCount));
				html = html.replace("<?clan_count?>", String.valueOf(ClanTable.getInstance().getClans().length));
				html = html.replace("<?market_count?>", String.valueOf(BbsHandlerHolder.getInstance().getIntProperty("col_count")));
			}
			else
			{
				onBypassCommand(player, Config.BBS_DEFAULT);
				return;
			}

			if(Config.BBS_CHARACTER_INFO)
			{
				final String no = new CustomMessage("common.result.no").toString(player);
				final String yes = new CustomMessage("common.result.yes").toString(player);
				html = html.replace("<?player_name?>", player.getName());
				html = html.replace("<?player_class?>", Util.className(player, player.getClassId().getId()));
				html = html.replace("<?player_level?>", String.valueOf(player.getLevel()));
				html = html.replace("<?player_clan?>", player.getClan() != null ? player.getClan().getName() : "<font color=\"FF0000\">" + no + "</font>");
				html = html.replace("<?player_noobless?>", player.isNoble() ? "<font color=\"18FF00\">" + yes + "</font>" : "<font color=\"FF0000\">" + no + "</font>");
				html = html.replace("<?online_time?>", TimeUtils.formatTime((int) (player.getOnlineTime() / 1000), player.getLanguage(), true));
				html = html.replace("<?player_ip?>", player.getIP());
				html = html.replace("<?player_premium?>", player.hasBonus() ? "<font color=\"18FF00\">" + yes + "</font>" : "<font color=\"FF0000\">" + no + "</font>");
			}

			if(Config.BBS_RATES_INFO)
			{
				Rate rate = player.getRate();
				html = html.replace("<?rate_xp?>", String.valueOf(rate.getExp()));
				html = html.replace("<?rate_sp?>", String.valueOf(rate.getSp()));
				html = html.replace("<?rate_adena?>", String.valueOf(rate.getAdena()));
				html = html.replace("<?rate_items?>", String.valueOf(rate.getItems()));
				html = html.replace("<?rate_spoil?>", String.valueOf(rate.getSpoil()));
				html = html.replace("<?rate_raid?>", String.valueOf(rate.getRaid()));
				html = html.replace("<?rate_siege?>", String.valueOf(Config.RATE_DROP_SIEGE_GUARD));
				html = html.replace("<?rate_fish?>", String.valueOf(Config.RATE_FISH_DROP_COUNT));
				html = html.replace("<?rate_quest?>", "?");
				html = html.replace("<?rate_manor?>", String.valueOf(Config.RATE_MANOR));
				html = html.replace("<?rate_clanrep?>", String.valueOf(Config.RATE_CLAN_REP_SCORE));
				html = html.replace("<?rate_hellbound?>", String.valueOf(Config.RATE_HELLBOUND_CONFIDENCE));
			}
		}
		else if("bbslink".equals(cmd))
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/bbs_homepage.htm", player);
		else if(bypass.startsWith("_bbsopen"))
		{
			//Example: "bypass _bbsopen:pages:index".
			String[] b = bypass.split(":");
			String folder = b[1];
			String page = b[2];
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/" + folder + "/" + page + ".htm", player);
			if(html == null)
				return;
		}
		else if(bypass.startsWith("_bbsmultisell"))
		{
			//Example: "_bbsmultisell:10000;_bbspage:index" or "_bbsmultisell:10000;_bbshome" or "_bbsmultisell:10000"...
			StringTokenizer st2 = new StringTokenizer(bypass, ";");
			String[] mBypass = st2.nextToken().split(":");
			String pBypass = st2.hasMoreTokens() ? st2.nextToken() : null;
			if(pBypass != null)
				Util.communityNextPage(player, pBypass);

			int listId = Integer.parseInt(mBypass[1]);
			MultiSellHolder.getInstance().SeparateAndSend(listId, player, -1, 0);
			return;
		}
		else if(bypass.startsWith("_bbsscripts"))
		{
			//Example: "_bbsscripts:events.GvG.GvG:addGroup;_bbspage:index" or "_bbsscripts:events.GvG.GvG:addGroup;_bbshome" or "_bbsscripts:events.GvG.GvG:addGroup"...
			StringTokenizer st2 = new StringTokenizer(bypass, ";");
			String sBypass = st2.nextToken().substring(12);
			String pBypass = st2.hasMoreTokens() ? st2.nextToken() : null;
			if(pBypass != null) {
				Util.communityNextPage(player, pBypass);
			}
			String[] word = sBypass.split("\\s+");
			String[] args = sBypass.substring(word[0].length()).trim().split("\\s+");
			String[] path = word[0].split(":");
			if(path.length != 2) {
				return;
			}
			Scripts.getInstance().callScripts(player, path[0], path[1], word.length == 1 ? new Object[] {} : new Object[] { args });
			return;
		}
		else if(bypass.startsWith("_bbsbypass"))
		{
			//Example: bypass _bbsbypass:services.RateBonus:list;_bbshome
			StringTokenizer st2 = new StringTokenizer(bypass, ";");
			String command = st2.nextToken().substring(11);
			String pBypass = st2.hasMoreTokens() ? st2.nextToken() : null;
			if(pBypass != null)
				Util.communityNextPage(player, pBypass);

			String word = command.split("\\s+")[0];

			Pair<Object, Method> b = BypassHolder.getInstance().getBypass(word);
			if(b != null)
				try
				{
					b.getValue().invoke(b.getKey(), player, null, command.substring(word.length()).trim().split("\\s+"));
				}
				catch(Exception e)
				{
					_log.error("Exception: " + e, e);
				}
			return;
		}
		else if(bypass.startsWith("_bbsnotdone"))
		{
			player.sendMessage("This function is not implemented!\nTry again latter!");
			onBypassCommand(player, "_bbshome");
			return;
		}
		else if(bypass.startsWith("_bbsuser"))
		{
			String command = bypass.substring(9).trim();
			String[] words = command.split("\\s+");
			String[] word_split = words[0].split(";");
			String word = word_split[0];
			String args = command.substring(word.length()).trim();

			if(word_split.length == 1)
				onBypassCommand(player, "_bbshome");
			else
				Util.communityNextPage(player, word_split[1]);

			IVoicedCommandHandler vch = VoicedCommandHandler.getInstance().getVoicedCommandHandler(word);

			if(vch != null)
				vch.useVoicedCommand(word, player, args);
			else
			{
				player.sendMessage("Oops, cannot find command!");
				_log.warn("Unknow voiced command '" + word + "'");
			}

			return;
		}

		ShowBoard.separateAndSend(html, player);
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}
