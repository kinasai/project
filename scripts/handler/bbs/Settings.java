package handler.bbs;

import org.apache.commons.lang3.math.NumberUtils;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.actor.instances.player.config.PlayerConfig;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.dress.DressData;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.CharInfo;
import com.l2cccp.gameserver.network.l2.s2c.ClientSetTime;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Settings extends ScriptBbsHandler
{
	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbssetting" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.COMMUNITYBOARD_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			Util.communityNextPage(player, "_bbshome");
			return;
		}

		if(bypass.equals("_bbssetting"))
		{
			String html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/cabinet/settings.htm", player);
			PlayerConfig config = player.getConfig();
			final boolean RU = player.getLanguage() == Language.RUSSIAN;
			html = html.replace("<?lang_ru?>", RU ? "_Down" : "");
			html = html.replace("<?lang_en?>", !RU ? "_Down" : "");
			html = html.replace("<?img?>", config.showImages() ? "On" : "Off");
			html = html.replace("<?offline?>", config.isHideTraders() ? "On" : "Off");
			html = html.replace("<?loot?>", config.getAutoLoot() == 1 ? "On" : "Off");
			html = html.replace("<?hloot?>", config.isAutoLootHerb() ? "On" : "Off");
			html = html.replace("<?exp?>", config.isExpOff() ? "On" : "Off");
			html = html.replace("<?translit?>", config.getTranslit().equals("tl") ? "On" : "Off");
			html = html.replace("<?anim?>", String.valueOf(config.getBuffDistance() / 15));
			html = html.replace("<?nc?>", String.valueOf(config.getNoCarrier()));
			html = html.replace("<?rain?>", config.showRain() ? "Off" : "On");
			html = html.replace("<?visual?>", config.isHideVisual() ? "On" : "Off");

			ShowBoard.separateAndSend(html, player);
			return;
		}
		else if(bypass.startsWith("_bbssetting:set"))
		{
			final String[] data = bypass.split(":");
			final String key = data[2];

			final PlayerConfig config = player.getConfig();
			if(key.equals("translit"))
				config.setTranslit(config.getTranslit().equals("tl") ? "Off" : "tl");
			else if(key.equals("lang"))
			{
				final String lang = data[3];
				final GameClient client = player.getNetConnection();

				if(lang.equals("ru"))
					client.setLanguage(Language.RUSSIAN);
				else
					client.setLanguage(Language.ENGLISH);
			}
			else if(key.equals("exp"))
				config.setExpOff(config.isExpOff() ? false : true);
			else if(key.equals("loot"))
				config.setAutoLoot(config.getAutoLoot() == 1 ? 0 : 1);
			else if(key.equals("hloot"))
				config.setAutoLootHerb(config.isAutoLootHerb() ? false : true);
			else if(key.equals("offline"))
			{
				final boolean var = config.isHideTraders() ? false : true;
				config.setHideTraders(var);
				player.setNotShowTraders(var);
			}
			else if(key.equals("rain"))
			{
				final boolean var = config.showRain() ? false : true;
				player.setDisableFogAndRain(var);
				player.sendPacket(new ClientSetTime(player));
				config.setRain(var);
			}
			else if(key.equals("img"))
				config.setImages(config.showImages() ? false : true);
			else if(key.equals("buff"))
			{
				final int percent = NumberUtils.toInt(data[3], 0);
				int range = 15 * percent;

				if(range < 0)
					range = -1;
				else if(range > 1500)
					range = 1500;

				player.setBuffAnimRange(range);
				config.setBuffDistance(range);
			}
			else if(key.equals("nc"))
			{
				int sec = NumberUtils.toInt(data[3], Config.SERVICES_NO_CARRIER_MIN_TIME);
				if(sec > Config.SERVICES_NO_CARRIER_MAX_TIME)
					sec = Config.SERVICES_NO_CARRIER_MAX_TIME;
				config.setNoCarrier(sec);
			}
			else if(key.equals("visual"))
			{
				final int last = player.getSessionVarI("visualLast", 0);
				final int sec = (int) (last - (System.currentTimeMillis() / 1000L));
				if(sec > 0)
					player.sendMessage("Wait " + sec + " seconds for use this function.");
				else
				{
					player.addSessionVar("visualLast", System.currentTimeMillis() + 120000);
					final boolean force = player.isForceVisualFlag();
					player.setForceVisualFlag(!force);

					// Двойная отправка, из за затупа клиента!
					player.sendUserInfo(true);
					player.sendUserInfo(true);

					for(Player temp : World.getAroundPlayers(player))
					{
						if(temp.isInvisible() && !player.getPlayerAccess().IsGM)
							continue;

						player.sendPacket(new CharInfo(temp, DressData.check(player)));
					}
				}
			}

			onBypassCommand(player, "_bbssetting");
		}

	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}