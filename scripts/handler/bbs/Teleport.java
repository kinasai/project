package handler.bbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.TeleportPointHolder;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.bbs.teleport.Price;
import com.l2cccp.gameserver.model.bbs.teleport.PriceType;
import com.l2cccp.gameserver.model.bbs.teleport.TeleportPoint;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.utils.Util;

public class Teleport extends ScriptBbsHandler
{
	private static final Logger _log = LoggerFactory.getLogger(Teleport.class);

	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbsteleport" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.BBS_TELEPORT_ALLOW)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			Util.communityNextPage(player, "_bbshome");
			return;
		}
		String html = "";
		if(bypass.equals("_bbsteleport"))
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/teleport/index.htm", player);
		else if(bypass.startsWith("_bbsteleport:page"))
		{
			String[] path = bypass.split(":");
			if(path.length > 3)
				html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/teleport/" + path[2] + "/" + path[3] + ".htm", player);
			else
				html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/teleport/" + path[2] + ".htm", player);
		}
		else if(bypass.equals("_bbsteleport:save_page"))
		{
			showTeleportPoint(player);
			return;
		}
		else if(bypass.startsWith("_bbsteleport:delete"))
		{
			deleteTeleportPoint(player, Integer.parseInt(bypass.split(":")[2]));
			showTeleportPoint(player);
			return;
		}
		else if(bypass.startsWith("_bbsteleport:save"))
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/teleport/index.htm", player);
			String point = "";
			String[] next = bypass.split(" ");

			if(next.length > 1)
			{
				for(int i = 1; i < next.length; i++)
				{
					point += " " + next[i];
				}
			}

			if(point.length() > 0)
				addTeleportPoint(player, point);

			showTeleportPoint(player);
			return;
		}
		else if(bypass.startsWith("_bbsteleport:go"))
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/teleport/index.htm", player);
			String[] cord = bypass.split(":");
			int x = Integer.parseInt(cord[2]);
			int y = Integer.parseInt(cord[3]);
			int z = Integer.parseInt(cord[4]);
			goToTeleportPoint(player, x, y, z);
		}
		else if(bypass.startsWith("_bbsteleport:subsection"))
		{
			final int subsection = Integer.parseInt(bypass.split(":")[2]);
			String template = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/teleport/point_template.htm", player);
			String block = "";
			String points = "";
			int counter = 0;
			final boolean cond = checkFirstConditions(player);
			for(final TeleportPoint data : TeleportPointHolder.getInstance().getTeleports(subsection).values())
			{
				if(counter % 2 == 0)
				{
					counter = 0;
					points += "</td></tr><tr><td>";
				}
				else
					points += "</td><td>";

				final boolean use = cond && data.checkLevel(player.getLevel());
				block = template;
				int x = data.getX(), y = data.getY(), z = data.getZ();
				block = block.replace("{name}", data.getName(player.getLanguage()));
				block = block.replace("{id}", String.valueOf(data.getId()));
				block = block.replace("{color}", use ? "669900" : "FF3333");
				block = block.replace("{subsection}", String.valueOf(subsection));
				block = block.replace("{x}", String.valueOf(x));
				block = block.replace("{y}", String.valueOf(y));
				block = block.replace("{z}", String.valueOf(z));
				points += block;
				counter++;
			}

			String content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/teleport/subsection.htm", player);
			html = content.replace("{points}", points.equals("") ? "..." : points);
		}
		else if(bypass.startsWith("_bbsteleport:use"))
		{
			final int subsection = Integer.parseInt(bypass.split(":")[2]);
			final int id = Integer.parseInt(bypass.split(":")[3]);
			final TeleportPoint point = TeleportPointHolder.getInstance().getTeleport(subsection, id);
			if(point != null)
				teleport(player, point);
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/teleport/index.htm", player);
		}

		ShowBoard.separateAndSend(html, player);
	}

	private void teleport(final Player player, final TeleportPoint point)
	{
		if(!point.checkLevel(player.getLevel()))
		{
			player.sendMessage(new CustomMessage("teleport.point.level.min.max").addNumber(point.getMinLevel()).addNumber(point.getMaxLevel()));
			return;
		}
		else if(!point.canPkUse() && player.getKarma() > 0)
		{
			player.sendMessage(new CustomMessage("teleport.point.pk.denied"));
			return;
		}
		else if(point.isPremiumOnly() && !player.hasBonus())
		{
			player.sendMessage(new CustomMessage("teleport.point.only.premium"));
			return;
		}
		else if(point.isNoblesseOnly() && !player.isNoble())
		{
			player.sendMessage(new CustomMessage("teleport.point.only.premium"));
			return;
		}

		if(!checkFirstConditions(player))
			return;

		final PriceType type = player.hasBonus() ? PriceType.PREMIUM : player.isNoble() ? PriceType.NOBLESSE : PriceType.SIMPLE;
		final Price price = point.getPrice(type);

		if(player.getLevel() <= Config.BBS_TELEPORT_FREE_LEVEL || Util.getPay(player, price.getItem(), price.getCount(), true))
		{
			player.teleToLocation(point.getX(), point.getY(), point.getZ());
			player.sendMessage(new CustomMessage("teleport.point.success.location").addString(point.getName(player.getLanguage())));
		}
	}

	private void goToTeleportPoint(Player player, int x, int y, int z)
	{
		if(!checkFirstConditions(player))
			return;

		player.teleToLocation(x, y, z);
	}

	private void showTeleportPoint(Player player)
	{
		if(Config.BBS_TELEPORT_POINT_PA && !player.hasBonus())
		{
			player.sendMessage(new CustomMessage("teleport.personal.point.only.premium"));
			Util.communityNextPage(player, "_bbsteleport");
			return;
		}

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		String template = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/teleport/template.htm", player);
		String block = "";
		String points = "";
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM bbs_teleport WHERE player_id=?;");
			statement.setLong(1, player.getObjectId());
			rset = statement.executeQuery();

			int counter = 0;
			while(rset.next())
			{
				if(counter % 2 == 0)
				{
					counter = 0;
					points += "</td></tr><tr><td>";
				}
				else
					points += "</td><td>";

				block = template;
				int x = rset.getInt("x"), y = rset.getInt("y"), z = rset.getInt("z");
				block = block.replace("{name}", rset.getString("name"));
				block = block.replace("{id}", String.valueOf(rset.getInt("id")));
				block = block.replace("{x}", String.valueOf(x));
				block = block.replace("{y}", String.valueOf(y));
				block = block.replace("{z}", String.valueOf(z));
				points += block;
				counter++;
			}
		}
		catch(SQLException e)
		{
			_log.warn("SQL Error: " + e);
			_log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		String content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/teleport/save.htm", player);
		content = content.replace("{points}", points.equals("") ? new CustomMessage("teleport.personal.point.empty").toString(player) : points);
		content = content.replace("{all_price}", Util.formatAdena(Config.BBS_TELEPORT_SAVE_PRICE) + " " + Util.getItemName(Config.BBS_TELEPORT_SAVE_ITEM_ID));
		content = content.replace("{premium_price}", Util.formatAdena(Config.BBS_TELEPORT_PA_SAVE_PRICE) + " " + Util.getItemName(Config.BBS_TELEPORT_PA_SAVE_ITEM_ID));
		content = content.replace("{point_count}", String.valueOf(Config.BBS_TELEPORT_MAX_COUNT));
		ShowBoard.separateAndSend(content, player);
	}

	private void deleteTeleportPoint(Player player, int id)
	{
		Connection con = null;
		PreparedStatement statement = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("DELETE FROM bbs_teleport WHERE player_id=? AND id=?;");
			statement.setInt(1, player.getObjectId());
			statement.setInt(2, id);
			statement.execute();
		}
		catch(SQLException e)
		{
			_log.warn("SQL Error: " + e);
			_log.error("", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement);
		}
	}

	private void addTeleportPoint(Player player, String point)
	{
		if(!checkFirstConditions(player))
			return;

		if(Config.BBS_TELEPORT_POINT_PA && !player.hasBonus())
		{
			player.sendMessage(new CustomMessage("teleport.personal.point.only.premium"));
			return;
		}
		else if(player.isMovementDisabled() || player.isOutOfControl())
		{
			player.sendMessage(new CustomMessage("teleport.personal.point.outofcontrol"));
			return;
		}
		else if(player.isInZone(Zone.ZoneType.battle_zone) || player.isInZone(Zone.ZoneType.no_escape) || player.isInZone(Zone.ZoneType.epic) || player.isInZone(Zone.ZoneType.SIEGE) || player.isInZone(Zone.ZoneType.RESIDENCE) || player.getVar("jailed") != null)
		{
			player.sendMessage(new CustomMessage("teleport.personal.point.forbidden.zone"));
			return;
		}

		int item = Config.BBS_TELEPORT_SAVE_ITEM_ID, price = Config.BBS_TELEPORT_SAVE_PRICE;

		if(player.hasBonus())
		{
			item = Config.BBS_TELEPORT_PA_SAVE_ITEM_ID;
			price = Config.BBS_TELEPORT_PA_SAVE_PRICE;
		}

		if(Util.getPay(player, item, price, true))
		{
			Connection con = null;
			PreparedStatement statement = null;
			ResultSet rset = null;
			try
			{
				con = DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("SELECT COUNT(*) FROM bbs_teleport WHERE player_id=?;");
				statement.setLong(1, player.getObjectId());
				rset = statement.executeQuery();
				rset.next();

				if(rset.getInt(1) < Config.BBS_TELEPORT_MAX_COUNT)
				{
					statement = con.prepareStatement("SELECT COUNT(*) FROM bbs_teleport WHERE player_id=? AND name=?;");
					statement.setLong(1, player.getObjectId());
					statement.setString(2, point);
					ResultSet rset1 = statement.executeQuery();
					rset1.next();

					if(rset1.getInt(1) == 0)
					{
						statement = con.prepareStatement("INSERT INTO bbs_teleport (player_id, x, y, z, name) VALUES(?,?,?,?,?)");
						statement.setInt(1, player.getObjectId());
						statement.setInt(2, player.getX());
						statement.setInt(3, player.getY());
						statement.setInt(4, player.getZ());
						statement.setString(5, point);
						statement.execute();
					}
					else
					{
						statement = con.prepareStatement("UPDATE bbs_teleport SET x=?, y=?, z=? WHERE player_id=? AND name=?;");
						statement.setInt(1, player.getX());
						statement.setInt(2, player.getY());
						statement.setInt(3, player.getZ());
						statement.setInt(4, player.getObjectId());
						statement.setString(5, point);
						statement.execute();
					}
				}
				else
				{
					player.sendMessage(new CustomMessage("teleport.personal.point.max").addNumber(Config.BBS_TELEPORT_MAX_COUNT));
					player.getInventory().addItem(item, price);
				}

			}
			catch(SQLException e)
			{
				_log.warn("SQL Error: " + e);
				_log.error("", e);
			}
			finally
			{
				DbUtils.closeQuietly(con, statement, rset);
			}
		}
	}

	private static boolean checkFirstConditions(Player player)
	{
		if(player == null)
			return false;

		if(player.getActiveWeaponFlagAttachment() != null)
		{
			player.sendPacket(SystemMsg.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD);
			return false;
		}
		else if(player.isInOlympiadMode())
		{
			player.sendPacket(SystemMsg.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_PARTICIPATING_IN_AN_OLYMPIAD_MATCH);
			return false;
		}
		else if(!Config.BBS_TELEPORT_ALLOW_IN_INSTANCE && player.getReflection() != ReflectionManager.DEFAULT)
		{
			player.sendPacket(SystemMsg.YOU_CANNOT_USE_MY_TELEPORTS_IN_AN_INSTANT_ZONE);
			return false;
		}
		else if(player.isInDuel())
		{
			player.sendPacket(SystemMsg.YOU_CANNOT_USE_MY_TELEPORTS_DURING_A_DUEL);
			return false;
		}
		else if(!Config.BBS_TELEPORT_ALLOW_IN_COMBAT && player.isInCombat())
		{
			player.sendPacket(SystemMsg.YOU_CANNOT_USE_MY_TELEPORTS_DURING_A_BATTLE);
			return false;
		}
		else if(!Config.BBS_TELEPORT_ALLOW_IN_PVP_FLAG && player.getPvpFlag() != 0)
		{
			player.sendPacket(SystemMsg.YOU_CANNOT_USE_MY_TELEPORTS_DURING_A_BATTLE);
			return false;
		}
		else if(!Config.BBS_TELEPORT_ALLOW_ON_SIEGE && (player.isOnSiegeField() || player.isInZoneBattle()))
		{
			player.sendPacket(SystemMsg.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_PARTICIPATING_A_LARGESCALE_BATTLE_SUCH_AS_A_CASTLE_SIEGE_FORTRESS_SIEGE_OR_HIDEOUT_SIEGE);
			return false;
		}
		else if(player.isFlying())
		{
			player.sendPacket(SystemMsg.YOU_CANNOT_USE_MY_TELEPORTS_WHILE_FLYING);
			return false;
		}
		else if(!Config.BBS_TELEPORT_ALLOW_IN_UNDERWATHER && (player.isInWater() || player.isInBoat()))
		{
			player.sendPacket(SystemMsg.YOU_CANNOT_USE_MY_TELEPORTS_UNDERWATER);
			return false;
		}
		else
			return true;
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}