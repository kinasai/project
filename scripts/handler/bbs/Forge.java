package handler.bbs;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.FoundationHolder;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.Element;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.items.Inventory;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.items.PcInventory;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExShowVariationCancelWindow;
import com.l2cccp.gameserver.network.l2.s2c.ExShowVariationMakeWindow;
import com.l2cccp.gameserver.network.l2.s2c.InventoryUpdate;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.templates.item.WeaponTemplate.WeaponType;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Log;
import com.l2cccp.gameserver.utils.Util;

public class Forge extends ScriptBbsHandler
{
	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbsforge" };
	}

	@Override
	public void onBypassCommand(Player player, String command)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.BBS_FORGE_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			Util.communityNextPage(player, "_bbshome");
			return;
		}

		String content = "";
		if(command.equals("_bbsforge"))
			content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/index.htm", player);
		else if(command.startsWith("_bbsforge:augment"))
		{
			onBypassCommand(player, "_bbsforge");
			player.addSessionVar("augmentation", true);
			player.sendPacket(SystemMsg.SELECT_THE_ITEM_TO_BE_AUGMENTED, ExShowVariationMakeWindow.STATIC);
			return;
		}
		else if(command.startsWith("_bbsforge:remove:augment"))
		{
			onBypassCommand(player, "_bbsforge");
			player.addSessionVar("augmentation", true);
			player.sendPacket(SystemMsg.SELECT_THE_ITEM_FROM_WHICH_YOU_WISH_TO_REMOVE_AUGMENTATION, ExShowVariationCancelWindow.STATIC);
			return;
		}
		else if(command.equals("_bbsforge:enchant:list"))
		{
			content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/itemlist.htm", player);

			ItemInstance head = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_HEAD);
			ItemInstance chest = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_CHEST);
			ItemInstance legs = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LEGS);
			ItemInstance gloves = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_GLOVES);
			ItemInstance feet = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_FEET);

			ItemInstance lhand = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
			ItemInstance rhand = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);

			ItemInstance lfinger = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LFINGER);
			ItemInstance rfinger = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RFINGER);
			ItemInstance neck = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_NECK);
			ItemInstance lear = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LEAR);
			ItemInstance rear = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_REAR);

			Map<Integer, String[]> data = new HashMap<Integer, String[]>();

			// Armors
			data.put(Inventory.PAPERDOLL_HEAD, ForgeElement.generateEnchant(head, Config.BBS_FORGE_ENCHANT_MAX[1], Inventory.PAPERDOLL_HEAD, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_CHEST, ForgeElement.generateEnchant(chest, Config.BBS_FORGE_ENCHANT_MAX[1], Inventory.PAPERDOLL_CHEST, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_LEGS, ForgeElement.generateEnchant(legs, Config.BBS_FORGE_ENCHANT_MAX[1], Inventory.PAPERDOLL_LEGS, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_GLOVES, ForgeElement.generateEnchant(gloves, Config.BBS_FORGE_ENCHANT_MAX[1], Inventory.PAPERDOLL_GLOVES, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_FEET, ForgeElement.generateEnchant(feet, Config.BBS_FORGE_ENCHANT_MAX[1], Inventory.PAPERDOLL_FEET, player.getLanguage()));

			// Jewels
			data.put(Inventory.PAPERDOLL_LFINGER, ForgeElement.generateEnchant(lfinger, Config.BBS_FORGE_ENCHANT_MAX[2], Inventory.PAPERDOLL_LFINGER, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_RFINGER, ForgeElement.generateEnchant(rfinger, Config.BBS_FORGE_ENCHANT_MAX[2], Inventory.PAPERDOLL_RFINGER, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_NECK, ForgeElement.generateEnchant(neck, Config.BBS_FORGE_ENCHANT_MAX[2], Inventory.PAPERDOLL_NECK, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_LEAR, ForgeElement.generateEnchant(lear, Config.BBS_FORGE_ENCHANT_MAX[2], Inventory.PAPERDOLL_LEAR, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_REAR, ForgeElement.generateEnchant(rear, Config.BBS_FORGE_ENCHANT_MAX[2], Inventory.PAPERDOLL_REAR, player.getLanguage()));

			// Weapons
			data.put(Inventory.PAPERDOLL_RHAND, ForgeElement.generateEnchant(rhand, Config.BBS_FORGE_ENCHANT_MAX[0], Inventory.PAPERDOLL_RHAND, player.getLanguage()));
			if(rhand != null && (rhand.getTemplate().getItemType() == WeaponType.BIGBLUNT || rhand.getTemplate().getItemType() == WeaponType.BOW || rhand.getTemplate().getItemType() == WeaponType.DUALDAGGER || rhand.getTemplate().getItemType() == WeaponType.ANCIENTSWORD || rhand.getTemplate().getItemType() == WeaponType.CROSSBOW || rhand.getTemplate().getItemType() == WeaponType.BIGBLUNT || rhand.getTemplate().getItemType() == WeaponType.BIGSWORD || rhand.getTemplate().getItemType() == WeaponType.DUALFIST || rhand.getTemplate().getItemType() == WeaponType.DUAL || rhand.getTemplate().getItemType() == WeaponType.POLE || rhand.getTemplate().getItemType() == WeaponType.FIST))
			{
				data.put(Inventory.PAPERDOLL_LHAND, new String[] {
						rhand.getTemplate().getIcon(),
						(rhand.getName() + " " + (rhand.getEnchantLevel() > 0 ? "+" + rhand.getEnchantLevel() : "")),
						"<font color=\"FF0000\">...</font>",
						"L2UI_CT1.ItemWindow_DF_SlotBox_Disable" });
			}
			else
				data.put(Inventory.PAPERDOLL_LHAND, ForgeElement.generateEnchant(lhand, Config.BBS_FORGE_ENCHANT_MAX[0], Inventory.PAPERDOLL_LHAND, player.getLanguage()));

			content = content.replace("<?content?>", ForgeElement.page(player));

			for(Entry<Integer, String[]> info : data.entrySet())
			{
				int slot = info.getKey();
				String[] array = info.getValue();
				content = content.replace("<?" + slot + "_icon?>", array[0]);
				content = content.replace("<?" + slot + "_name?>", array[1]);
				content = content.replace("<?" + slot + "_button?>", array[2]);
				content = content.replace("<?" + slot + "_pic?>", array[3]);
			}
		}
		else if(command.startsWith("_bbsforge:enchant:item:"))
		{
			String[] array = command.split(":");
			int item = Integer.parseInt(array[3]);

			String name = ItemHolder.getInstance().getTemplate(Config.BBS_FORGE_ENCHANT_ITEM).getName();

			if(name.isEmpty())
				name = new CustomMessage("common.item.no.name").toString(player);

			if(item < 1 || item > 12)
				return;

			ItemInstance _item = player.getInventory().getPaperdollItem(item);
			if(_item == null || !_item.canBeEnchanted(true))
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.null"));
				Util.communityNextPage(player, "_bbsforge:enchant:list");
				return;
			}

			if(_item.getTemplate().isArrow())
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.arrow"));
				Util.communityNextPage(player, "_bbsforge:enchant:list");
				return;
			}

			content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/enchant.htm", player);

			String template = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/enchant_template.htm", player);

			template = template.replace("{icon}", _item.getTemplate().getIcon());
			String _name = _item.getName();
			_name = _name.replace(" {PvP}", "");

			if(_name.length() > 30)
				_name = _name.substring(0, 29) + "...";

			template = template.replace("{name}", _name);
			template = template.replace("{enchant}", _item.getEnchantLevel() <= 0 ? "" : "+" + _item.getEnchantLevel());
			template = template.replace("{msg}", new CustomMessage("communityboard.forge.enchant.select").toString(player));

			String button_tm = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/enchant_button_template.htm", player);
			String button = "";
			String block = null;

			int[] level = _item.getTemplate().isWeapon() ? Config.BBS_FORGE_WEAPON_ENCHANT_LVL : _item.getTemplate().isArmor() ? Config.BBS_FORGE_ARMOR_ENCHANT_LVL : Config.BBS_FORGE_JEWELS_ENCHANT_LVL;
			for(int i = 0; i < level.length; i++)
			{
				if(_item.getEnchantLevel() < level[i])
				{
					block = button_tm;
					block = block.replace("{link}", String.valueOf("bypass _bbsforge:enchant:" + (i * item) + ":" + item));
					block = block.replace("{value}", "+" + level[i] + " (" + (_item.getTemplate().isWeapon() ? Config.BBS_FORGE_ENCHANT_PRICE_WEAPON[i] : _item.getTemplate().isArmor() ? Config.BBS_FORGE_ENCHANT_PRICE_ARMOR[i] : Config.BBS_FORGE_ENCHANT_PRICE_JEWELS[i]) + " " + name + ")");
					button += block;
				}
			}

			template = template.replace("{button}", button == null ? "------" : button);

			content = content.replace("<?content?>", template);
		}
		else if(command.equals("_bbsforge:foundation:list"))
		{
			content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/foundationlist.htm", player);

			ItemInstance head = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_HEAD);
			ItemInstance chest = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_CHEST);
			ItemInstance legs = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LEGS);
			ItemInstance gloves = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_GLOVES);
			ItemInstance feet = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_FEET);

			ItemInstance lhand = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
			ItemInstance rhand = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);

			ItemInstance lfinger = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LFINGER);
			ItemInstance rfinger = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RFINGER);
			ItemInstance neck = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_NECK);
			ItemInstance lear = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LEAR);
			ItemInstance rear = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_REAR);

			Map<Integer, String[]> data = new HashMap<Integer, String[]>();

			// Armors
			data.put(Inventory.PAPERDOLL_HEAD, ForgeElement.generateFoundation(head, Inventory.PAPERDOLL_HEAD, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_CHEST, ForgeElement.generateFoundation(chest, Inventory.PAPERDOLL_CHEST, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_LEGS, ForgeElement.generateFoundation(legs, Inventory.PAPERDOLL_LEGS, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_GLOVES, ForgeElement.generateFoundation(gloves, Inventory.PAPERDOLL_GLOVES, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_FEET, ForgeElement.generateFoundation(feet, Inventory.PAPERDOLL_FEET, player.getLanguage()));

			// Jewels
			data.put(Inventory.PAPERDOLL_LFINGER, ForgeElement.generateFoundation(lfinger, Inventory.PAPERDOLL_LFINGER, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_RFINGER, ForgeElement.generateFoundation(rfinger, Inventory.PAPERDOLL_RFINGER, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_NECK, ForgeElement.generateFoundation(neck, Inventory.PAPERDOLL_NECK, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_LEAR, ForgeElement.generateFoundation(lear, Inventory.PAPERDOLL_LEAR, player.getLanguage()));
			data.put(Inventory.PAPERDOLL_REAR, ForgeElement.generateFoundation(rear, Inventory.PAPERDOLL_REAR, player.getLanguage()));

			// Weapons
			data.put(Inventory.PAPERDOLL_RHAND, ForgeElement.generateFoundation(rhand, Inventory.PAPERDOLL_RHAND, player.getLanguage()));
			if(rhand != null && (rhand.getTemplate().getItemType() == WeaponType.BIGBLUNT || rhand.getTemplate().getItemType() == WeaponType.BOW || rhand.getTemplate().getItemType() == WeaponType.DUALDAGGER || rhand.getTemplate().getItemType() == WeaponType.ANCIENTSWORD || rhand.getTemplate().getItemType() == WeaponType.CROSSBOW || rhand.getTemplate().getItemType() == WeaponType.BIGBLUNT || rhand.getTemplate().getItemType() == WeaponType.BIGSWORD || rhand.getTemplate().getItemType() == WeaponType.DUALFIST || rhand.getTemplate().getItemType() == WeaponType.DUAL || rhand.getTemplate().getItemType() == WeaponType.POLE || rhand.getTemplate().getItemType() == WeaponType.FIST))
			{
				data.put(Inventory.PAPERDOLL_LHAND, new String[] {
						rhand.getTemplate().getIcon(),
						(rhand.getName() + " " + (rhand.getEnchantLevel() > 0 ? "+" + rhand.getEnchantLevel() : "")),
						"<font color=\"FF0000\">...</font>",
						"L2UI_CT1.ItemWindow_DF_SlotBox_Disable" });
			}
			else
				data.put(Inventory.PAPERDOLL_LHAND, ForgeElement.generateFoundation(lhand, Inventory.PAPERDOLL_LHAND, player.getLanguage()));

			content = content.replace("<?content?>", ForgeElement.page(player));

			for(Entry<Integer, String[]> info : data.entrySet())
			{
				int slot = info.getKey();
				String[] array = info.getValue();
				content = content.replace("<?" + slot + "_icon?>", array[0]);
				content = content.replace("<?" + slot + "_name?>", array[1]);
				content = content.replace("<?" + slot + "_button?>", array[2]);
				content = content.replace("<?" + slot + "_pic?>", array[3]);
			}
		}
		else if(command.startsWith("_bbsforge:foundation:item:"))
		{
			String[] array = command.split(":");
			int item = Integer.parseInt(array[3]);

			if(item < 1 || item > 12)
				return;

			ItemInstance _item = player.getInventory().getPaperdollItem(item);
			if(_item == null)
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.null"));
				onBypassCommand(player, "_bbsforge:foundation:list");
				return;
			}

			int found = FoundationHolder.getInstance().getFoundation(_item.getItemId());
			if(found == -1)
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.null"));
				onBypassCommand(player, "_bbsforge:foundation:list");
				return;
			}

			PcInventory inv = player.getInventory();
			ItemInstance _found = ItemFunctions.createItem(found);

			final int id = (int) (_item.isWeapon() ? Config.BBS_FORGE_FOUNDATION_PRICE_WEAPON[0] : _item.isArmor() ? Config.BBS_FORGE_FOUNDATION_PRICE_ARMOR[0] : Config.BBS_FORGE_FOUNDATION_PRICE_ACCESORY[0]);
			final long count = _item.isWeapon() ? Config.BBS_FORGE_FOUNDATION_PRICE_WEAPON[1] : _item.isArmor() ? Config.BBS_FORGE_FOUNDATION_PRICE_ARMOR[1] : Config.BBS_FORGE_FOUNDATION_PRICE_ACCESORY[1];

			if(Util.getPay(player, id, count, true))
			{
				if(inv.destroyItemByObjectId(_item.getObjectId(), _item.getCount()))
				{
					_found.setEnchantLevel(_item.getEnchantLevel());
					_found.setAugmentation(_item.getAugmentationMineralId(), _item.getAugmentations());

					for(Element element : Element.VALUES)
					{
						int val = _item.getAttributes().getValue(element);
						if(val > 0)
							_found.setAttributeElement(element, val);
					}

					inv.addItem(_found);
					_found.setJdbcState(JdbcEntityState.UPDATED);
					_found.update();
					if(ItemFunctions.checkIfCanEquip(player, _found) == null)
						inv.equipItem(_found);
					player.sendMessage("You exchange item " + _item.getName() + " to Foundation " + _found.getName());
				}
				else
					_found.deleteMe();
			}

			onBypassCommand(player, "_bbsforge:foundation:list");
			return;
		}
		else if(command.startsWith("_bbsforge:enchant:"))
		{
			String[] array = command.split(":");

			int val = Integer.parseInt(array[2]);
			int item = Integer.parseInt(array[3]);

			int conversion = val / item;

			ItemInstance _item = player.getInventory().getPaperdollItem(item);

			int[] level = _item.getTemplate().isWeapon() ? Config.BBS_FORGE_WEAPON_ENCHANT_LVL : _item.getTemplate().isArmor() ? Config.BBS_FORGE_ARMOR_ENCHANT_LVL : Config.BBS_FORGE_JEWELS_ENCHANT_LVL;
			int Value = level[conversion];

			int max = _item.getTemplate().isWeapon() ? Config.BBS_FORGE_ENCHANT_MAX[0] : _item.getTemplate().isArmor() ? Config.BBS_FORGE_ENCHANT_MAX[1] : Config.BBS_FORGE_ENCHANT_MAX[2];
			if(Value > max)
				return;

			if(_item.getTemplate().isArrow())
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.arrow"));
				Util.communityNextPage(player, "_bbsforge:enchant:list");
				return;
			}

			int price = _item.isWeapon() ? Config.BBS_FORGE_ENCHANT_PRICE_WEAPON[conversion] : _item.getTemplate().isArmor() ? Config.BBS_FORGE_ENCHANT_PRICE_ARMOR[conversion] : Config.BBS_FORGE_ENCHANT_PRICE_JEWELS[conversion];

			if(_item != null)
			{
				if(Util.getPay(player, Config.BBS_FORGE_ENCHANT_ITEM, price, true))
				{
					player.getInventory().unEquipItem(_item);
					_item.setEnchantLevel(Value);
					player.getInventory().equipItem(_item);

					player.sendPacket(new InventoryUpdate().addModifiedItem(_item));
					player.broadcastUserInfo(true);

					player.sendMessage(new CustomMessage("communityboard.forge.enchant.success").addString(_item.getName()).addNumber(Value));
					Log.add("enchant item " + _item.getName() + " at +" + Value + "", "CommunityBoardForge", player);
				}
			}

			Util.communityNextPage(player, "_bbsforge:enchant:list");
			return;
		}
		else if(command.equals("_bbsforge:attribute:list"))
		{
			content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/attributelist.htm", player);

			ItemInstance head = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_HEAD);
			ItemInstance chest = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_CHEST);
			ItemInstance legs = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LEGS);
			ItemInstance gloves = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_GLOVES);
			ItemInstance feet = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_FEET);

			ItemInstance lhand = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LHAND);
			ItemInstance rhand = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);

			ItemInstance lfinger = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LFINGER);
			ItemInstance rfinger = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RFINGER);
			ItemInstance neck = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_NECK);
			ItemInstance lear = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LEAR);
			ItemInstance rear = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_REAR);

			Map<Integer, String[]> data = new HashMap<Integer, String[]>();
			// Armors
			data.put(Inventory.PAPERDOLL_HEAD, ForgeElement.generateAttribution(head, Inventory.PAPERDOLL_HEAD, player.getLanguage(), player.hasBonus()));
			data.put(Inventory.PAPERDOLL_CHEST, ForgeElement.generateAttribution(chest, Inventory.PAPERDOLL_CHEST, player.getLanguage(), player.hasBonus()));
			data.put(Inventory.PAPERDOLL_LEGS, ForgeElement.generateAttribution(legs, Inventory.PAPERDOLL_LEGS, player.getLanguage(), player.hasBonus()));
			data.put(Inventory.PAPERDOLL_GLOVES, ForgeElement.generateAttribution(gloves, Inventory.PAPERDOLL_GLOVES, player.getLanguage(), player.hasBonus()));
			data.put(Inventory.PAPERDOLL_FEET, ForgeElement.generateAttribution(feet, Inventory.PAPERDOLL_FEET, player.getLanguage(), player.hasBonus()));

			// Jewels
			data.put(Inventory.PAPERDOLL_LFINGER, ForgeElement.generateAttribution(lfinger, Inventory.PAPERDOLL_LFINGER, player.getLanguage(), player.hasBonus()));
			data.put(Inventory.PAPERDOLL_RFINGER, ForgeElement.generateAttribution(rfinger, Inventory.PAPERDOLL_RFINGER, player.getLanguage(), player.hasBonus()));
			data.put(Inventory.PAPERDOLL_NECK, ForgeElement.generateAttribution(neck, Inventory.PAPERDOLL_NECK, player.getLanguage(), player.hasBonus()));
			data.put(Inventory.PAPERDOLL_LEAR, ForgeElement.generateAttribution(lear, Inventory.PAPERDOLL_LEAR, player.getLanguage(), player.hasBonus()));
			data.put(Inventory.PAPERDOLL_REAR, ForgeElement.generateAttribution(rear, Inventory.PAPERDOLL_REAR, player.getLanguage(), player.hasBonus()));

			// Weapons
			data.put(Inventory.PAPERDOLL_RHAND, ForgeElement.generateAttribution(rhand, Inventory.PAPERDOLL_RHAND, player.getLanguage(), player.hasBonus()));
			if(rhand != null && (rhand.getTemplate().getItemType() == WeaponType.BIGBLUNT || rhand.getTemplate().getItemType() == WeaponType.BOW || rhand.getTemplate().getItemType() == WeaponType.DUALDAGGER || rhand.getTemplate().getItemType() == WeaponType.ANCIENTSWORD || rhand.getTemplate().getItemType() == WeaponType.CROSSBOW || rhand.getTemplate().getItemType() == WeaponType.BIGBLUNT || rhand.getTemplate().getItemType() == WeaponType.BIGSWORD || rhand.getTemplate().getItemType() == WeaponType.DUALFIST || rhand.getTemplate().getItemType() == WeaponType.DUAL || rhand.getTemplate().getItemType() == WeaponType.POLE || rhand.getTemplate().getItemType() == WeaponType.FIST))
			{
				data.put(Inventory.PAPERDOLL_LHAND, new String[] {
						rhand.getTemplate().getIcon(),
						(rhand.getName() + " " + (rhand.getEnchantLevel() > 0 ? "+" + rhand.getEnchantLevel() : "")),
						"<font color=\"FF0000\">...</font>",
						"L2UI_CT1.ItemWindow_DF_SlotBox_Disable" });
			}
			else
				data.put(Inventory.PAPERDOLL_LHAND, ForgeElement.generateAttribution(lhand, Inventory.PAPERDOLL_LHAND, player.getLanguage(), player.hasBonus()));

			content = content.replace("<?content?>", ForgeElement.page(player));

			for(Entry<Integer, String[]> info : data.entrySet())
			{
				int slot = info.getKey();
				String[] array = info.getValue();
				content = content.replace("<?" + slot + "_icon?>", array[0]);
				content = content.replace("<?" + slot + "_name?>", array[1]);
				content = content.replace("<?" + slot + "_button?>", array[2]);
				content = content.replace("<?" + slot + "_pic?>", array[3]);
			}
		}
		else if(command.startsWith("_bbsforge:attribute:item:"))
		{
			String[] array = command.split(":");
			int item = Integer.parseInt(array[3]);

			if(item < 1 || item > 12)
				return;

			ItemInstance _item = player.getInventory().getPaperdollItem(item);
			if(_item == null)
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.null").toString());
				Util.communityNextPage(player, "_bbsforge:attribute:list");
				return;
			}

			if(!ForgeElement.itemCheckGrade(player.hasBonus(), _item))
			{
				player.sendMessage(new CustomMessage("communityboard.forge.grade.incorrect").toString());
				Util.communityNextPage(player, "_bbsforge:attribute:list");
				return;
			}

			if(_item.isHeroWeapon())
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.hero").toString());
				Util.communityNextPage(player, "_bbsforge:attribute:list");
				return;
			}

			content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/attribute.htm", player);

			String slotclose = "<img src=\"L2UI_CT1.ItemWindow_DF_SlotBox_Disable\" width=\"32\" height=\"32\">";
			String buttonFire = "<button action=\"bypass _bbsforge:attribute:element:0:" + item + "\" width=34 height=34 back=\"L2UI_CT1.ItemWindow_DF_Frame_Down\" fore=\"L2UI_CT1.ItemWindow_DF_Frame\"/>";
			String buttonWater = "<button action=\"bypass _bbsforge:attribute:element:1:" + item + "\" width=34 height=34 back=\"L2UI_CT1.ItemWindow_DF_Frame_Down\" fore=\"L2UI_CT1.ItemWindow_DF_Frame\"/>";
			String buttonWind = "<button action=\"bypass _bbsforge:attribute:element:2:" + item + "\" width=34 height=34 back=\"L2UI_CT1.ItemWindow_DF_Frame_Down\" fore=\"L2UI_CT1.ItemWindow_DF_Frame\"/>";
			String buttonEarth = "<button action=\"bypass _bbsforge:attribute:element:3:" + item + "\" width=34 height=34 back=\"L2UI_CT1.ItemWindow_DF_Frame_Down\" fore=\"L2UI_CT1.ItemWindow_DF_Frame\"/>";
			String buttonHoly = "<button action=\"bypass _bbsforge:attribute:element:4:" + item + "\" width=34 height=34 back=\"L2UI_CT1.ItemWindow_DF_Frame_Down\" fore=\"L2UI_CT1.ItemWindow_DF_Frame\"/>";
			String buttonUnholy = "<button action=\"bypass _bbsforge:attribute:element:5:" + item + "\" width=34 height=34 back=\"L2UI_CT1.ItemWindow_DF_Frame_Down\" fore=\"L2UI_CT1.ItemWindow_DF_Frame\"/>";

			if(_item.isWeapon())
			{
				if(_item.getAttributes().getFire() > 0)
				{
					buttonWater = slotclose;
					buttonWind = slotclose;
					buttonEarth = slotclose;
					buttonHoly = slotclose;
					buttonUnholy = slotclose;
				}
				if(_item.getAttributes().getWater() > 0)
				{
					buttonFire = slotclose;
					buttonWind = slotclose;
					buttonEarth = slotclose;
					buttonHoly = slotclose;
					buttonUnholy = slotclose;
				}
				if(_item.getAttributes().getWind() > 0)
				{
					buttonWater = slotclose;
					buttonFire = slotclose;
					buttonEarth = slotclose;
					buttonHoly = slotclose;
					buttonUnholy = slotclose;
				}
				if(_item.getAttributes().getEarth() > 0)
				{
					buttonWater = slotclose;
					buttonWind = slotclose;
					buttonFire = slotclose;
					buttonHoly = slotclose;
					buttonUnholy = slotclose;
				}
				if(_item.getAttributes().getHoly() > 0)
				{
					buttonWater = slotclose;
					buttonWind = slotclose;
					buttonEarth = slotclose;
					buttonFire = slotclose;
					buttonUnholy = slotclose;
				}
				if(_item.getAttributes().getUnholy() > 0)
				{
					buttonWater = slotclose;
					buttonWind = slotclose;
					buttonEarth = slotclose;
					buttonHoly = slotclose;
					buttonFire = slotclose;
				}
			}

			if(_item.isArmor())
			{
				if(_item.getAttributes().getFire() > 0)
				{
					if(_item.getAttributes().getFire() >= Config.BBS_FORGE_ARMOR_ATTRIBUTE_MAX)
					{
						buttonFire = slotclose;
					}
					buttonWater = slotclose;

				}
				if(_item.getAttributes().getWater() > 0)
				{
					if(_item.getAttributes().getWater() >= Config.BBS_FORGE_ARMOR_ATTRIBUTE_MAX)
					{
						buttonWater = slotclose;
					}
					buttonFire = slotclose;
				}
				if(_item.getAttributes().getWind() > 0)
				{
					if(_item.getAttributes().getWind() >= Config.BBS_FORGE_ARMOR_ATTRIBUTE_MAX)
					{
						buttonWind = slotclose;
					}
					buttonEarth = slotclose;
				}
				if(_item.getAttributes().getEarth() > 0)
				{
					if(_item.getAttributes().getEarth() >= Config.BBS_FORGE_ARMOR_ATTRIBUTE_MAX)
					{
						buttonEarth = slotclose;
					}
					buttonWind = slotclose;
				}
				if(_item.getAttributes().getHoly() > 0)
				{
					if(_item.getAttributes().getHoly() >= Config.BBS_FORGE_ARMOR_ATTRIBUTE_MAX)
					{
						buttonHoly = slotclose;
					}
					buttonUnholy = slotclose;
				}
				if(_item.getAttributes().getUnholy() > 0)
				{
					if(_item.getAttributes().getUnholy() >= Config.BBS_FORGE_ARMOR_ATTRIBUTE_MAX)
					{
						buttonUnholy = slotclose;
					}
					buttonHoly = slotclose;
				}
			}

			String html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/attribute_choice_template.htm", player);

			html = html.replace("{icon}", _item.getTemplate().getIcon());
			String _name = _item.getName();
			_name = _name.replace(" {PvP}", "");

			if(_name.length() > 30)
				_name = _name.substring(0, 29) + "...";

			html = html.replace("{name}", _name);
			html = html.replace("{enchant}", _item.getEnchantLevel() <= 0 ? "" : " +" + _item.getEnchantLevel());
			html = html.replace("{msg}", new CustomMessage("communityboard.forge.attribute.select").toString(player));
			html = html.replace("{fire}", buttonFire);
			html = html.replace("{water}", buttonWater);
			html = html.replace("{earth}", buttonEarth);
			html = html.replace("{wind}", buttonWind);
			html = html.replace("{holy}", buttonHoly);
			html = html.replace("{unholy}", buttonUnholy);

			content = content.replace("<?content?>", html);
		}
		else if(command.startsWith("_bbsforge:attribute:element:"))
		{
			String[] array = command.split(":");
			int element = Integer.parseInt(array[3]);

			String elementName = "";
			if(element == 0)
				elementName = new CustomMessage("common.element.0").toString(player);
			else if(element == 1)
				elementName = new CustomMessage("common.element.1").toString(player);
			else if(element == 2)
				elementName = new CustomMessage("common.element.2").toString(player);
			else if(element == 3)
				elementName = new CustomMessage("common.element.3").toString(player);
			else if(element == 4)
				elementName = new CustomMessage("common.element.4").toString(player);
			else if(element == 5)
				elementName = new CustomMessage("common.element.5").toString(player);

			int item = Integer.parseInt(array[4]);

			String name = ItemHolder.getInstance().getTemplate(Config.BBS_FORGE_ENCHANT_ITEM).getName();

			if(name.isEmpty())
				name = new CustomMessage("common.item.no.name").toString(player);

			ItemInstance _item = player.getInventory().getPaperdollItem(item);

			if(_item == null)
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.null").toString());
				Util.communityNextPage(player, "_bbsforge:attribute:list");
				return;
			}

			if(!ForgeElement.itemCheckGrade(player.hasBonus(), _item))
			{
				player.sendMessage(new CustomMessage("communityboard.forge.grade.incorrect").toString());
				Util.communityNextPage(player, "_bbsforge:attribute:list");
				return;
			}

			if(_item.isHeroWeapon())
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.hero").toString());
				Util.communityNextPage(player, "_bbsforge:attribute:list");
				return;
			}

			content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/attribute.htm", player);
			String template = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/enchant_template.htm", player);

			template = template.replace("{icon}", _item.getTemplate().getIcon());
			String _name = _item.getName();
			_name = _name.replace(" {PvP}", "");

			if(_name.length() > 30)
				_name = _name.substring(0, 29) + "...";

			template = template.replace("{name}", _name);
			template = template.replace("{enchant}", _item.getEnchantLevel() <= 0 ? "" : "+" + _item.getEnchantLevel());
			template = template.replace("{msg}", new CustomMessage("communityboard.forge.attribute.selected").addString(elementName).toString(player));

			String button_tm = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/enchant_button_template.htm", player);
			String button = null;
			String block = null;

			int[] level = _item.getTemplate().isWeapon() ? Config.BBS_FORGE_ATRIBUTE_LVL_WEAPON : Config.BBS_FORGE_ATRIBUTE_LVL_ARMOR;
			for(int i = 0; i < level.length; i++)
			{
				if(_item.getAttributeElementValue(Element.getElementById(element), false) < (_item.isWeapon() ? Config.BBS_FORGE_ATRIBUTE_LVL_WEAPON[i] : Config.BBS_FORGE_ATRIBUTE_LVL_ARMOR[i]))
				{
					block = button_tm;
					block = block.replace("{link}", String.valueOf("bypass _bbsforge:attribute:" + (i * item) + ":" + item + ":" + element));
					block = block.replace("{value}", "+" + (_item.isWeapon() ? Config.BBS_FORGE_ATRIBUTE_LVL_WEAPON[i] : Config.BBS_FORGE_ATRIBUTE_LVL_ARMOR[i]) + " (" + (_item.isWeapon() ? Config.BBS_FORGE_ATRIBUTE_PRICE_WEAPON[i] : Config.BBS_FORGE_ATRIBUTE_PRICE_ARMOR[i]) + " " + name + ")");
					button += block;
				}
			}

			template = template.replace("{button}", button);

			content = content.replace("<?content?>", template);
		}
		else if(command.startsWith("_bbsforge:attribute:"))
		{
			String[] array = command.split(":");
			int val = Integer.parseInt(array[2]);
			int item = Integer.parseInt(array[3]);
			int att = Integer.parseInt(array[4]);

			ItemInstance _item = player.getInventory().getPaperdollItem(item);

			if(_item == null)
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.null").toString(player));
				Util.communityNextPage(player, "_bbsforge:attribute:list");
				return;
			}

			if(!ForgeElement.itemCheckGrade(player.hasBonus(), _item))
			{
				player.sendMessage(new CustomMessage("communityboard.forge.grade.incorrect").toString(player));
				Util.communityNextPage(player, "_bbsforge:attribute:list");
				return;
			}

			if(_item.isHeroWeapon())
			{
				player.sendMessage(new CustomMessage("communityboard.forge.item.hero").toString(player));
				Util.communityNextPage(player, "_bbsforge:attribute:list");
				return;
			}

			if(_item.isArmor() && !ForgeElement.canEnchantArmorAttribute(att, _item))
			{
				player.sendMessage(new CustomMessage("communityboard.forge.attribute.terms.incorrect").toString(player));
				Util.communityNextPage(player, "_bbsforge:attribute:list");
				return;
			}

			int conversion = val / item;

			int Value = _item.isWeapon() ? Config.BBS_FORGE_ATRIBUTE_LVL_WEAPON[conversion] : Config.BBS_FORGE_ATRIBUTE_LVL_ARMOR[conversion];

			if(Value > (_item.isWeapon() ? Config.BBS_FORGE_WEAPON_ATTRIBUTE_MAX : Config.BBS_FORGE_ARMOR_ATTRIBUTE_MAX))
				return;

			int price = _item.isWeapon() ? Config.BBS_FORGE_ATRIBUTE_PRICE_WEAPON[conversion] : Config.BBS_FORGE_ATRIBUTE_PRICE_ARMOR[conversion];

			if(Util.getPay(player, Config.BBS_FORGE_ENCHANT_ITEM, price, true))
			{
				player.getInventory().unEquipItem(_item);

				_item.setAttributeElement(Element.getElementById(att), Value);

				player.getInventory().equipItem(_item);

				player.sendPacket(new InventoryUpdate().addModifiedItem(_item));
				player.broadcastUserInfo(true);

				String elementName = "";
				if(att == 0)
					elementName = new CustomMessage("common.element.0").toString(player);
				else if(att == 1)
					elementName = new CustomMessage("common.element.1").toString(player);
				else if(att == 2)
					elementName = new CustomMessage("common.element.2").toString(player);
				else if(att == 3)
					elementName = new CustomMessage("common.element.3").toString(player);
				else if(att == 4)
					elementName = new CustomMessage("common.element.4").toString(player);
				else if(att == 5)
					elementName = new CustomMessage("common.element.5").toString(player);

				player.sendMessage(new CustomMessage("communityboard.forge.enchant.attribute.success").addString(_item.getName()).addString(elementName).addNumber(Value));
				Log.add("enchant item:" + _item.getName() + " val: " + Value + " AtributType:" + att, "CommunityBoardForge", player);
			}

			Util.communityNextPage(player, "_bbsforge:attribute:list");
			return;
		}

		ShowBoard.separateAndSend(content, player);
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}