package handler.bbs;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.FoundationHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.Element;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.templates.item.ItemTemplate.Grade;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.Util;

public class ForgeElement
{
	protected static String[] generateAttribution(ItemInstance item, int slot, Language lang, boolean hasBonus)
	{
		String[] data = new String[4];

		String noicon = "icon.NOIMAGE";
		String slotclose = "L2UI_CT1.ItemWindow_DF_SlotBox_Disable";
		String dot = "<font color=\"FF0000\">...</font>";
		String immposible = new CustomMessage("communityboard.forge.attribute.immposible").toString(lang);
		String maxenchant = new CustomMessage("communityboard.forge.attribute.maxenchant").toString(lang);
		String heronot = new CustomMessage("communityboard.forge.attribute.heronot").toString(lang);
		String picenchant = "l2ui_ch3.multisell_plusicon";
		String pvp = "icon.pvp_tab";

		if(item != null)
		{
			data[0] = item.getTemplate().getIcon();
			data[1] = item.getName() + " " + (item.getEnchantLevel() > 0 ? "+" + item.getEnchantLevel() : "");
			if((item.getTemplate().isAttributable() || (item.getTemplate().isPvP() && Config.BBS_FORGE_ATRIBUTE_PVP)) && itemCheckGrade(hasBonus, item))
			{
				if(item.isHeroWeapon())
				{
					data[2] = heronot;
					data[3] = slotclose;
				}
				else if((item.isArmor() && ((item.getAttributes().getFire() | item.getAttributes().getWater()) & (item.getAttributes().getWind() | item.getAttributes().getEarth()) & (item.getAttributes().getHoly() | item.getAttributes().getUnholy())) >= Config.BBS_FORGE_ARMOR_ATTRIBUTE_MAX) || item.isWeapon() && item.getAttributes().getValue() >= Config.BBS_FORGE_WEAPON_ATTRIBUTE_MAX)
				{
					data[2] = maxenchant;
					data[3] = slotclose;
				}
				else
				{
					data[2] = "<button action=\"bypass _bbsforge:attribute:item:" + slot + "\" value=\"" + new CustomMessage("common.enchant.attribute").toString(lang) + "\" width=120 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\">";
					if(item.getTemplate().isPvP())
						data[3] = pvp;
					else
						data[3] = picenchant;
				}
			}
			else
			{
				data[2] = immposible;
				data[3] = slotclose;
			}
		}
		else
		{
			data[0] = noicon;
			data[1] = new CustomMessage("common.item.not.clothed." + slot + "").toString(lang);
			data[2] = dot;
			data[3] = slotclose;
		}

		return data;
	}

	protected static String[] generateEnchant(ItemInstance item, int max, int slot, Language lang)
	{
		String[] data = new String[4];

		String noicon = "icon.NOIMAGE";
		String slotclose = "L2UI_CT1.ItemWindow_DF_SlotBox_Disable";
		String dot = "<font color=\"FF0000\">...</font>";
		String maxenchant = new CustomMessage("communityboard.forge.enchant.max").toString(lang);
		String picenchant = "l2ui_ch3.multisell_plusicon";
		String pvp = "icon.pvp_tab";

		if(item != null)
		{
			data[0] = item.getTemplate().getIcon();
			data[1] = item.getName() + " " + (item.getEnchantLevel() > 0 ? "+" + item.getEnchantLevel() : "");
			if(!item.getTemplate().isArrow())
			{
				if(item.getEnchantLevel() >= max || !item.canBeEnchanted(true))
				{
					data[2] = maxenchant;
					data[3] = slotclose;
				}
				else
				{
					data[2] = "<button action=\"bypass _bbsforge:enchant:item:" + slot + "\" value=\"" + new CustomMessage("common.enchant").toString(lang) + "\"width=120 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\">";
					if(item.getTemplate().isPvP())
						data[3] = pvp;
					else
						data[3] = picenchant;
				}
			}
			else
			{
				data[2] = dot;
				data[3] = slotclose;
			}
		}
		else
		{
			data[0] = noicon;
			data[1] = new CustomMessage("common.item.not.clothed." + slot + "").toString(lang);
			data[2] = dot;
			data[3] = slotclose;
		}

		return data;
	}

	protected static String[] generateFoundation(ItemInstance item, int slot, Language lang)
	{
		String[] data = new String[4];

		String noicon = "icon.NOIMAGE";
		String slotclose = "L2UI_CT1.ItemWindow_DF_SlotBox_Disable";
		String dot = "<font color=\"FF0000\">...</font>";
		String no = new CustomMessage("communityboard.forge.no.foundation").toString(lang);
		String picenchant = "l2ui_ch3.multisell_plusicon";
		String pvp = "icon.pvp_tab";

		if(item != null)
		{
			data[0] = item.getTemplate().getIcon();
			data[1] = item.getName() + " " + (item.getEnchantLevel() > 0 ? "+" + item.getEnchantLevel() : "");
			if(!item.getTemplate().isArrow())
			{
				int found = FoundationHolder.getInstance().getFoundation(item.getItemId());
				if(found == -1)
				{
					data[2] = no;
					data[3] = slotclose;
				}
				else
				{
					final int id = (int) (item.isWeapon() ? Config.BBS_FORGE_FOUNDATION_PRICE_WEAPON[0] : item.isArmor() ? Config.BBS_FORGE_FOUNDATION_PRICE_ARMOR[0] : Config.BBS_FORGE_FOUNDATION_PRICE_ACCESORY[0]);
					final long count = item.isWeapon() ? Config.BBS_FORGE_FOUNDATION_PRICE_WEAPON[1] : item.isArmor() ? Config.BBS_FORGE_FOUNDATION_PRICE_ARMOR[1] : Config.BBS_FORGE_FOUNDATION_PRICE_ACCESORY[1];
					data[2] = "<button action=\"bypass _bbsforge:foundation:item:" + slot + "\" value=\"" + new CustomMessage("common.exchange").toString(lang) + " " + Util.formatPay(lang, count, id) + "\"width=190 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\">";
					if(item.getTemplate().isPvP())
						data[3] = pvp;
					else
						data[3] = picenchant;
				}
			}
			else
			{
				data[2] = dot;
				data[3] = slotclose;
			}
		}
		else
		{
			data[0] = noicon;
			data[1] = new CustomMessage("common.item.not.clothed." + slot + "").toString(lang);
			data[2] = dot;
			data[3] = slotclose;
		}

		return data;
	}

	protected static String page(Player player)
	{
		return HtmCache.getInstance().getHtml(Config.BBS_PATH + "/forge/page_template.htm", player);
	}

	protected static boolean itemCheckGrade(boolean hasBonus, ItemInstance item)
	{
		Grade grade = item.getCrystalType();

		switch(grade)
		{
			case NONE:
				return Config.BBS_FORGE_GRADE_ATTRIBUTE[0].equals("NG:PA") ? (hasBonus ? true : false) : (Config.BBS_FORGE_GRADE_ATTRIBUTE[0].equals("NG:ON") ? true : Config.BBS_FORGE_GRADE_ATTRIBUTE[0].equals("NG:NO") ? false : true);
			case D:
				return Config.BBS_FORGE_GRADE_ATTRIBUTE[1].equals("D:PA") ? (hasBonus ? true : false) : (Config.BBS_FORGE_GRADE_ATTRIBUTE[1].equals("D:ON") ? true : Config.BBS_FORGE_GRADE_ATTRIBUTE[1].equals("D:NO") ? false : true);
			case C:
				return Config.BBS_FORGE_GRADE_ATTRIBUTE[2].equals("C:PA") ? (hasBonus ? true : false) : (Config.BBS_FORGE_GRADE_ATTRIBUTE[2].equals("C:ON") ? true : Config.BBS_FORGE_GRADE_ATTRIBUTE[2].equals("C:NO") ? false : true);
			case B:
				return Config.BBS_FORGE_GRADE_ATTRIBUTE[3].equals("B:PA") ? (hasBonus ? true : false) : (Config.BBS_FORGE_GRADE_ATTRIBUTE[3].equals("B:ON") ? true : Config.BBS_FORGE_GRADE_ATTRIBUTE[3].equals("B:NO") ? false : true);
			case A:
				return Config.BBS_FORGE_GRADE_ATTRIBUTE[4].equals("A:PA") ? (hasBonus ? true : false) : (Config.BBS_FORGE_GRADE_ATTRIBUTE[4].equals("A:ON") ? true : Config.BBS_FORGE_GRADE_ATTRIBUTE[4].equals("A:NO") ? false : true);
			case S:
				return Config.BBS_FORGE_GRADE_ATTRIBUTE[5].equals("S:PA") ? (hasBonus ? true : false) : (Config.BBS_FORGE_GRADE_ATTRIBUTE[5].equals("S:ON") ? true : Config.BBS_FORGE_GRADE_ATTRIBUTE[5].equals("S:NO") ? false : true);
			case S80:
				return Config.BBS_FORGE_GRADE_ATTRIBUTE[6].equals("S80:PA") ? (hasBonus ? true : false) : (Config.BBS_FORGE_GRADE_ATTRIBUTE[6].equals("S80:ON") ? true : Config.BBS_FORGE_GRADE_ATTRIBUTE[6].equals("S80:NO") ? false : true);
			case S84:
				return Config.BBS_FORGE_GRADE_ATTRIBUTE[7].equals("S84:PA") ? (hasBonus ? true : false) : (Config.BBS_FORGE_GRADE_ATTRIBUTE[7].equals("S84:ON") ? true : Config.BBS_FORGE_GRADE_ATTRIBUTE[7].equals("S84:NO") ? false : true);
			default:
				return false;
		}
	}

	protected static boolean canEnchantArmorAttribute(int attr, ItemInstance item)
	{
		switch(attr)
		{
			case 0:
				if(item.getAttributeElementValue(Element.getReverseElement(Element.getElementById(0)), false) != 0)
					return false;
				break;
			case 1:
				if(item.getAttributeElementValue(Element.getReverseElement(Element.getElementById(1)), false) != 0)
					return false;
				break;
			case 2:
				if(item.getAttributeElementValue(Element.getReverseElement(Element.getElementById(2)), false) != 0)
					return false;
				break;
			case 3:
				if(item.getAttributeElementValue(Element.getReverseElement(Element.getElementById(3)), false) != 0)
					return false;
				break;
			case 4:
				if(item.getAttributeElementValue(Element.getReverseElement(Element.getElementById(4)), false) != 0)
					return false;
				break;
			case 5:
				if(item.getAttributeElementValue(Element.getReverseElement(Element.getElementById(5)), false) != 0)
					return false;
				break;
		}
		return true;
	}
}