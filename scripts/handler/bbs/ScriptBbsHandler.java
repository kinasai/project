package handler.bbs;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.bbs.BbsHandlerHolder;
import com.l2cccp.gameserver.handler.bbs.IBbsHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;

/**
 * @author VISTALL
 * @date 2:17/19.08.2011
 */
public abstract class ScriptBbsHandler implements OnInitScriptListener, IBbsHandler
{
	@Override
	public void onInit()
	{
		if(Config.COMMUNITYBOARD_ENABLED)
			BbsHandlerHolder.getInstance().registerHandler(this);
	}
}
