package handler.bbs;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.NpcHolder;
import com.l2cccp.gameserver.instancemanager.RaidBossSpawnManager;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.RadarControl;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.templates.npc.MinionData;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Util;

import handler.onshiftaction.OnShiftAction_NpcInstance;

public class Bosses extends ScriptBbsHandler
{

	private static final int BOSSES_PER_PAGE = 10;

	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbsbosslist", "_bbsboss" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.BBS_BOSSES_ALLOW)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			Util.communityNextPage(player, "_bbshome");
			return;
		}

		StringTokenizer st = new StringTokenizer(bypass, "_");
		String cmd = st.nextToken();

		if("bbsbosslist".equals(cmd))//_bbsbosslist_sort_page_search
		{
			int sort = Integer.parseInt(st.hasMoreTokens() ? st.nextToken() : "1");
			int page = Integer.parseInt(st.hasMoreTokens() ? st.nextToken() : "0");
			String search = st.hasMoreTokens() ? st.nextToken().trim() : "";

			sendBossListPage(player, getSortByIndex(sort), page, search);
		}
		else if("bbsboss".equals(cmd))//_bbsboss_sort_page_search_rbId_btn
		{
			int bossId = Integer.parseInt(st.hasMoreTokens() ? st.nextToken() : "25044");
			int buttonClick = Integer.parseInt(st.hasMoreTokens() ? st.nextToken() : "0");

			if(buttonClick != 0)
				manageButtons(player, buttonClick, bossId);

			sendBossDetails(player, bossId);
		}
	}

	private static void sendBossListPage(Player player, SortType sort, int page, String search)
	{
		String html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/database/bosses/list.htm", player);

		Map<Integer, StatsSet> allBosses = getSearchedBosses(sort, search);
		Map<Integer, StatsSet> bossesToShow = getBossesToShow(allBosses, page);
		boolean isThereNextPage = allBosses.size() > bossesToShow.size();

		html = getBossListReplacements(html, page, bossesToShow, isThereNextPage);

		html = getNormalReplacements(html, page, sort, search, -1);
		ShowBoard.separateAndSend(html, player);
	}

	private static String getBossListReplacements(String html, int page, Map<Integer, StatsSet> allBosses, boolean nextPage)
	{
		String newHtml = html;

		int i = 0;

		for(Entry<Integer, StatsSet> entry : allBosses.entrySet())
		{
			StatsSet boss = entry.getValue();
			NpcTemplate temp = NpcHolder.getInstance().getTemplate(entry.getKey().intValue());

			boolean isAlive = isBossAlive(boss);

			newHtml = newHtml.replace("<?name_" + i + "?>", temp.getName());
			newHtml = newHtml.replace("<?level_" + i + "?>", String.valueOf(temp.level));
			newHtml = newHtml.replace("<?status_" + i + "?>", isAlive ? "Alive" : "Dead");
			newHtml = newHtml.replace("<?status_color_" + i + "?>", getTextColor(isAlive));
			newHtml = newHtml.replace("<?bp_" + i + "?>", "<button value=\"show\" action=\"bypass _bbsboss_" + entry.getKey() + "\" width=40 height=12 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\">");
			i++;
		}

		for(int j = i; j < BOSSES_PER_PAGE; j++)
		{
			newHtml = newHtml.replace("<?name_" + j + "?>", "...");
			newHtml = newHtml.replace("<?level_" + j + "?>", "...");
			newHtml = newHtml.replace("<?status_" + j + "?>", "...");

			newHtml = newHtml.replace("<?status_color_" + j + "?>", "FFFFFF");
			newHtml = newHtml.replace("<?bp_" + j + "?>", "...");
		}

		newHtml = newHtml.replace("<?previous?>", page > 0 ? "<button action=\"bypass _bbsbosslist_<?sort?>_" + (page - 1) + "_<?search?>\" width=16 height=16 back=\"L2UI_CH3.shortcut_prev_down\" fore=\"L2UI_CH3.shortcut_prev\">" : "<br>");
		newHtml = newHtml.replace("<?next?>", nextPage && i == BOSSES_PER_PAGE ? "<button action=\"bypass _bbsbosslist_<?sort?>_" + (page + 1) + "_<?search?>\" width=16 height=16 back=\"L2UI_CH3.shortcut_next_down\" fore=\"L2UI_CH3.shortcut_next\">" : "<br>");

		newHtml = newHtml.replace("<?pages?>", String.valueOf(page + 1));

		return newHtml;
	}

	private static Map<Integer, StatsSet> getBossesToShow(Map<Integer, StatsSet> allBosses, int page)
	{
		Map<Integer, StatsSet> bossesToShow = new LinkedHashMap<Integer, StatsSet>();
		int i = 0;
		for(Entry<Integer, StatsSet> entry : allBosses.entrySet())
		{
			if(i < page * BOSSES_PER_PAGE)
			{
				i++;
			}
			else
			{
				StatsSet boss = entry.getValue();
				NpcTemplate temp = NpcHolder.getInstance().getTemplate(entry.getKey().intValue());
				if(boss != null && temp != null)
				{
					i++;
					bossesToShow.put(entry.getKey(), entry.getValue());
					if(i > (page * BOSSES_PER_PAGE + BOSSES_PER_PAGE - 1)){ return bossesToShow; }
				}
			}
		}
		return bossesToShow;
	}

	private static void sendBossDetails(Player player, int bossId)
	{
		String html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/database/bosses/details.htm", player);
		StatsSet bossSet = RaidBossSpawnManager.getInstance().getAllBosses().get(bossId);

		if(bossSet == null)
		{
			ShowBoard.separateAndSend(html, player);
			return;
		}

		NpcTemplate bossTemplate = NpcHolder.getInstance().getTemplate(bossId);
		NpcInstance bossInstance = getAliveBoss(bossId);

		html = getDetailedBossReplacements(html, bossSet, bossTemplate, bossInstance);
		html = getNormalReplacements(html, 1, getSortByIndex(3), "", bossId);

		ShowBoard.separateAndSend(html, player);
	}

	private static void manageButtons(Player player, int buttonIndex, int bossId)
	{
		switch(buttonIndex)
		{
			case 1://Show Location
			{
				RaidBossSpawnManager.showBossLocation(player, bossId);
				break;
			}
			case 2://Show Drops
			{
				if(Config.ALT_GAME_SHOW_DROPLIST)
					OnShiftAction_NpcInstance.droplist(player, GameObjectsStorage.getByNpcId(bossId));
				break;
			}
			case 3://Go to Boss
			{
				if(!canTeleToMonster(player, bossId))
					break;

				NpcInstance aliveInstance = getAliveBoss(bossId);
				if(aliveInstance != null)
					player.teleToLocation(aliveInstance.getLoc());
				else
					player.sendMessage("Boss isn't alive!");
				break;
			}
			case 4://Show Location
			{
				player.sendPacket(new RadarControl(2, 2, 0, 0, 0));
				break;
			}
			case 5://Show Monster Stats
			{
				OnShiftAction_NpcInstance.stats(player, GameObjectsStorage.getByNpcId(bossId));
				break;
			}
			case 6://Show Monster Skills
			{
				OnShiftAction_NpcInstance.skills(player, GameObjectsStorage.getByNpcId(bossId));
				break;
			}
		}
	}

	private static boolean canTeleToMonster(Player player, int bossId)
	{
		if(Config.BBS_BOSSES_DISABLED_TELEPORT[0] == -1)
		{
			player.sendMessage("This function is disable!!");
			return false;
		}
		else if(!player.isInZonePeace())
		{
			player.sendMessage("You can do it only in safe zone!");
			return false;
		}
		else if(Olympiad.isRegistered(player) || player.isInOlympiadMode())
		{
			player.sendMessage("You cannot do it while being registered in Olympiad Battle!");
			return false;
		}

		for(int id : Config.BBS_BOSSES_DISABLED_TELEPORT)
		{
			if(id == bossId)
			{
				player.sendMessage("You cannot teleport to this Boss!");
				return false;
			}
		}
		return true;
	}

	private static String getDetailedBossReplacements(String html, StatsSet bossSet, NpcTemplate bossTemplate, NpcInstance bossInstance)
	{
		String newHtml = html;

		boolean isAlive = isBossAlive(bossSet);

		newHtml = newHtml.replace("<?name?>", bossTemplate.getName());
		newHtml = newHtml.replace("<?level?>", String.valueOf(bossTemplate.level));
		newHtml = newHtml.replace("<?status?>", isAlive ? "Alive" : "Dead");
		newHtml = newHtml.replace("<?status_color?>", getTextColor(isAlive));
		newHtml = newHtml.replace("<?minions?>", String.valueOf(getMinionsCount(bossTemplate)));

		newHtml = newHtml.replace("<?currentHp?>", Util.formatAdena((int) (bossInstance != null ? (int) bossInstance.getCurrentHp() : 0)));
		newHtml = newHtml.replace("<?maxHp?>", Util.formatAdena((int) bossTemplate.baseHpMax));
		newHtml = newHtml.replace("<?minions?>", String.valueOf(getMinionsCount(bossTemplate)));

		return newHtml;
	}

	private static String getNormalReplacements(String html, int page, SortType sort, CharSequence search, int bossId)
	{
		String newHtml = html;
		newHtml = newHtml.replace("<?page?>", String.valueOf(page));
		newHtml = newHtml.replace("<?sort?>", String.valueOf(sort.index));
		newHtml = newHtml.replace("<?bossId?>", String.valueOf(bossId));
		newHtml = newHtml.replace("<?search?>", search);

		for(int i = 1; i <= 6; i++)
		{
			if(Math.abs(sort.index) == i)
				newHtml = newHtml.replace("<?sort" + i + "?>", String.valueOf(-sort.index));
			else
				newHtml = newHtml.replace("<?sort" + i + "?>", String.valueOf(i));
		}

		return newHtml;
	}

	private static boolean isBossAlive(StatsSet set)
	{
		return (long) set.getInteger("respawn_delay", 0) < System.currentTimeMillis() / TimeUnit.SECONDS.toMillis(1L);
	}

	private static long getRespawn(StatsSet set)
	{
		return (long) set.getInteger("respawn_delay", 0) * TimeUnit.SECONDS.toMillis(1L);
	}

	private static NpcInstance getAliveBoss(int bossId)
	{
		List<NpcInstance> instances = GameObjectsStorage.getAllByNpcId(bossId, true);
		return instances.isEmpty() ? null : instances.get(0);
	}

	private static int getMinionsCount(NpcTemplate template)
	{
		int minionsCount = 0;
		for(MinionData minion : template.getMinionData())
			minionsCount += minion.getAmount();
		return minionsCount;
	}

	private static String getTextColor(boolean alive)
	{
		if(alive)
			return "259a30";//"327b39";
		else
			return "b02e31";//"8f3d3f";
	}

	private static Map<Integer, StatsSet> getSearchedBosses(SortType sort, String search)
	{
		Map<Integer, StatsSet> result = getBossesMapBySearch(search);

		for(int id : Config.BBS_BOSSES_HIDE)
			result.remove(id);

		result = sortResults(result, sort);

		return result;
	}

	private static Map<Integer, StatsSet> getBossesMapBySearch(String search)
	{
		Map<Integer, StatsSet> finalResult = new HashMap<Integer, StatsSet>();
		if(search.isEmpty())
		{
			finalResult = RaidBossSpawnManager.getInstance().getAllBosses();
		}
		else
		{
			for(Entry<Integer, StatsSet> entry : RaidBossSpawnManager.getInstance().getAllBosses().entrySet())
			{
				NpcTemplate temp = NpcHolder.getInstance().getTemplate(entry.getKey().intValue());
				if(StringUtils.containsIgnoreCase(temp.getName(), search))
					finalResult.put(entry.getKey(), entry.getValue());
			}
		}
		return finalResult;
	}

	private static Map<Integer, StatsSet> sortResults(Map<Integer, StatsSet> result, SortType sort)
	{
		ValueComparator bvc = new ValueComparator(result, sort);
		Map<Integer, StatsSet> sortedMap = new TreeMap<Integer, StatsSet>(bvc);
		sortedMap.putAll(result);
		return sortedMap;
	}

	private static class ValueComparator implements Comparator<Integer>, Serializable
	{
		private static final long serialVersionUID = 4782405190873267622L;
		private final Map<Integer, StatsSet> base;
		private final SortType sortType;

		private ValueComparator(Map<Integer, StatsSet> base, SortType sortType)
		{
			this.base = base;
			this.sortType = sortType;
		}

		@Override
		public int compare(Integer o1, Integer o2)
		{
			int sortResult = sortById(o1, o2, sortType);
			if(sortResult == 0 && !o1.equals(o2) && Math.abs(sortType.index) != 1)
				sortResult = sortById(o1, o2, SortType.NAME_ASC);
			return sortResult;
		}

		private int sortById(Integer a, Integer b, SortType sorting)
		{
			NpcTemplate temp1 = NpcHolder.getInstance().getTemplate(a.intValue());
			NpcTemplate temp2 = NpcHolder.getInstance().getTemplate(b.intValue());
			StatsSet set1 = base.get(a);
			StatsSet set2 = base.get(b);
			switch(sorting)
			{
				case NAME_ASC:
					return temp1.getName().compareTo(temp2.getName());
				case NAME_DESC:
					return temp2.getName().compareTo(temp1.getName());
				case LEVEL_ASC:
					return Integer.valueOf(temp1.level).compareTo(temp2.level);
				case LEVEL_DESC:
					return Integer.valueOf(temp2.level).compareTo(temp1.level);
				case STATUS_ASC:
					return Integer.valueOf(set1.getInteger("respawn_delay", 0)).compareTo(set2.getInteger("respawn_delay", 0));
				case STATUS_DESC:
					return Integer.valueOf(set2.getInteger("respawn_delay", 0)).compareTo(set1.getInteger("respawn_delay", 0));
			}
			return 0;
		}
	}

	private enum SortType
	{
		NAME_ASC(1),
		NAME_DESC(-1),
		LEVEL_ASC(2),
		LEVEL_DESC(-2),
		STATUS_ASC(3),
		STATUS_DESC(-3);

		public final int index;

		SortType(int index)
		{
			this.index = index;
		}
	}

	private static SortType getSortByIndex(int i)
	{
		for(SortType type : SortType.values())
			if(type.index == i)
				return type;
		return SortType.NAME_ASC;
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}