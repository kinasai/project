package handler.bbs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.tables.ClanTable;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

public class Statistics extends ScriptBbsHandler
{
	private static final Logger _log = LoggerFactory.getLogger(Statistics.class);

	@Override
	public void onInit()
	{
		super.onInit();
		if(Config.BBS_STATISTIC_ALLOW)
		{
			selectTopPK();
			selectTopPVP();
			selectTopOnline();
			selectTopRich();
			selectTopClan();
			selectTopRK();
			selectTopCIS();
			selectTopCIP();
			_log.info("Statistics in the commynity board has been updated.");
		}
	}

	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbsstatistic" };
	}

	public static class Manager
	{
		private String[] TopPvPName = new String[Config.BBS_STATISTIC_COUNT];
		private String[] TopPvPClan = new String[Config.BBS_STATISTIC_COUNT];
		private int[] TopPvPSex = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopPvPClass = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopPvPOn = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopPvPOnline = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopPvP = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopPvPPvP = new int[Config.BBS_STATISTIC_COUNT];

		private String[] TopPkName = new String[Config.BBS_STATISTIC_COUNT];
		private String[] TopPkClan = new String[Config.BBS_STATISTIC_COUNT];
		private int[] TopPkSex = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopPkClass = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopPkOn = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopPkOnline = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopPk = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopPkPvP = new int[Config.BBS_STATISTIC_COUNT];

		private String[] TopOnlineName = new String[Config.BBS_STATISTIC_COUNT];
		private String[] TopOnlineClan = new String[Config.BBS_STATISTIC_COUNT];
		private int[] TopOnlineSex = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopOnlineClass = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopOnlineOn = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopOnlineOnline = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopOnline = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopOnlinePvP = new int[Config.BBS_STATISTIC_COUNT];

		private String[] TopRichName = new String[Config.BBS_STATISTIC_COUNT];
		private String[] TopRichClan = new String[Config.BBS_STATISTIC_COUNT];
		private int[] TopRichSex = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopRichClass = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopRichOn = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopRichOnline = new int[Config.BBS_STATISTIC_COUNT];
		private long[] TopRich = new long[Config.BBS_STATISTIC_COUNT];

		public String[] TopClanName = new String[Config.BBS_STATISTIC_COUNT];
		public String[] TopClanAlly = new String[Config.BBS_STATISTIC_COUNT];
		public String[] TopClanLeader = new String[Config.BBS_STATISTIC_COUNT];
		public int[] TopClanLevel = new int[Config.BBS_STATISTIC_COUNT];
		public int[] TopClanPoint = new int[Config.BBS_STATISTIC_COUNT];
		public int[] TopClanMember = new int[Config.BBS_STATISTIC_COUNT];
		public int[] TopClanCastle = new int[Config.BBS_STATISTIC_COUNT];
		public int[] TopClanFort = new int[Config.BBS_STATISTIC_COUNT];

		private String[] TopRaidName = new String[Config.BBS_STATISTIC_COUNT];
		private String[] TopRaidClan = new String[Config.BBS_STATISTIC_COUNT];
		private int[] TopRaidSex = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopRaidClass = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopRaidOn = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopRaidOnline = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopRaid = new int[Config.BBS_STATISTIC_COUNT];

		private String[] TopInstanceSoloName = new String[Config.BBS_STATISTIC_COUNT];
		private String[] TopInstanceSoloClan = new String[Config.BBS_STATISTIC_COUNT];
		private int[] TopInstanceSoloSex = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopInstanceSoloClass = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopInstanceSoloOn = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopInstanceSoloOnline = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopInstanceSolo = new int[Config.BBS_STATISTIC_COUNT];

		private String[] TopInstancePartyName = new String[Config.BBS_STATISTIC_COUNT];
		private String[] TopInstancePartyClan = new String[Config.BBS_STATISTIC_COUNT];
		private int[] TopInstancePartySex = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopInstancePartyClass = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopInstancePartyOn = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopInstancePartyOnline = new int[Config.BBS_STATISTIC_COUNT];
		private int[] TopInstanceParty = new int[Config.BBS_STATISTIC_COUNT];
	}

	static Manager ManagerStats = new Manager();

	public long update = System.currentTimeMillis() / 1000;
	public int number = 0;
	public int time_update = Config.BBS_STATISTIC_UPDATE_TIME;

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.BBS_STATISTIC_ALLOW)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}
		else if(bypass.equals("_bbsstatistic:pk"))
			show(player, 2);
		else if(bypass.equals("_bbsstatistic:pvp"))
			show(player, 3);
		else if(bypass.equals("_bbsstatistic:online"))
			show(player, 4);
		else if(bypass.equals("_bbsstatistic:rich"))
			show(player, 5);
		else if(bypass.equals("_bbsstatistic:rk"))
			show(player, 6);
		else if(bypass.equals("_bbsstatistic:cis"))
			show(player, 7);
		else if(bypass.equals("_bbsstatistic:cip"))
			show(player, 8);
		else if(bypass.equals("_bbsstatistic:clan"))
			show(player, 9);
		else
			show(player, 2);

		if(update + time_update * 60 < System.currentTimeMillis() / 1000)
		{
			selectTopPK();
			selectTopPVP();
			selectTopOnline();
			selectTopRich();
			selectTopClan();
			selectTopRK();
			selectTopCIS();
			selectTopCIP();
			update = System.currentTimeMillis() / 1000;
			_log.info("Full statistics in the commynity board has been updated.");
		}
	}

	private void show(Player player, int page)
	{
		number = 0;
		String html = "";
		final String no = new CustomMessage("common.result.no").toString(player);
		final String yes = new CustomMessage("common.result.yes").toString(player);
		if(page == 2)
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/statistic/pk.htm", player);
			while(number < Config.BBS_STATISTIC_COUNT)
			{
				if(ManagerStats.TopPkName[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", ManagerStats.TopPkName[number]);
					html = html.replace("<?clan_" + number + "?>", ManagerStats.TopPkClan[number] == null ? "<font color=\"B59A75\">" + no + "</font>" : ManagerStats.TopPkClan[number]);
					html = html.replace("<?sex_" + number + "?>", ManagerStats.TopPkSex[number] == 0 ? "Male" : "Female");
					html = html.replace("<?class_" + number + "?>", String.valueOf(Util.className(player, ManagerStats.TopPkClass[number])));
					html = html.replace("<?on_" + number + "?>", ManagerStats.TopPkOn[number] == 1 ? "<font color=\"66FF33\">" + yes + "</font>" : "<font color=\"B59A75\">" + no + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(TimeUtils.formatTime(ManagerStats.TopPkOnline[number], player.getLanguage())));
					html = html.replace("<?pk_count_" + number + "?>", Integer.toString(ManagerStats.TopPk[number]));
					html = html.replace("<?pvp_count_" + number + "?>", Integer.toString(ManagerStats.TopPkPvP[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?pk_count_" + number + "?>", "...");
					html = html.replace("<?pvp_count_" + number + "?>", "...");
				}

				number++;
			}
		}
		else if(page == 3)
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/statistic/pvp.htm", player);
			while(number < Config.BBS_STATISTIC_COUNT)
			{
				if(ManagerStats.TopPvPName[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", ManagerStats.TopPvPName[number]);
					html = html.replace("<?clan_" + number + "?>", ManagerStats.TopPvPClan[number] == null ? "<font color=\"B59A75\">" + no + "</font>" : ManagerStats.TopPvPClan[number]);
					html = html.replace("<?sex_" + number + "?>", ManagerStats.TopPvPSex[number] == 0 ? "Male" : "Female");
					html = html.replace("<?class_" + number + "?>", String.valueOf(Util.className(player, ManagerStats.TopPvPClass[number])));
					html = html.replace("<?on_" + number + "?>", ManagerStats.TopPvPOn[number] == 1 ? "<font color=\"66FF33\">" + yes + "</font>" : "<font color=\"B59A75\">" + no + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(TimeUtils.formatTime(ManagerStats.TopPvPOnline[number], player.getLanguage())));
					html = html.replace("<?pk_count_" + number + "?>", Integer.toString(ManagerStats.TopPvP[number]));
					html = html.replace("<?pvp_count_" + number + "?>", Integer.toString(ManagerStats.TopPvPPvP[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?pk_count_" + number + "?>", "...");
					html = html.replace("<?pvp_count_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if(page == 4)
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/statistic/online.htm", player);
			while(number < Config.BBS_STATISTIC_COUNT)
			{
				if(ManagerStats.TopPvPName[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", ManagerStats.TopOnlineName[number]);
					html = html.replace("<?clan_" + number + "?>", ManagerStats.TopOnlineClan[number] == null ? "<font color=\"B59A75\">" + no + "</font>" : ManagerStats.TopOnlineClan[number]);
					html = html.replace("<?sex_" + number + "?>", ManagerStats.TopOnlineSex[number] == 0 ? "Male" : "Female");
					html = html.replace("<?class_" + number + "?>", String.valueOf(Util.className(player, ManagerStats.TopOnlineClass[number])));
					html = html.replace("<?on_" + number + "?>", ManagerStats.TopOnlineOn[number] == 1 ? "<font color=\"66FF33\">" + yes + "</font>" : "<font color=\"B59A75\">" + no + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(TimeUtils.formatTime(ManagerStats.TopOnlineOnline[number], player.getLanguage())));
					html = html.replace("<?pk_count_" + number + "?>", Integer.toString(ManagerStats.TopOnline[number]));
					html = html.replace("<?pvp_count_" + number + "?>", Integer.toString(ManagerStats.TopOnlinePvP[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?pk_count_" + number + "?>", "...");
					html = html.replace("<?pvp_count_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if(page == 5)
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/statistic/rich.htm", player);
			while(number < Config.BBS_STATISTIC_COUNT)
			{
				if(ManagerStats.TopRichName[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", ManagerStats.TopRichName[number]);
					html = html.replace("<?clan_" + number + "?>", ManagerStats.TopRichClan[number] == null ? "<font color=\"B59A75\">" + no + "</font>" : ManagerStats.TopRichClan[number]);
					html = html.replace("<?sex_" + number + "?>", ManagerStats.TopRichSex[number] == 0 ? "Male" : "Female");
					html = html.replace("<?class_" + number + "?>", String.valueOf(Util.className(player, ManagerStats.TopRichClass[number])));
					html = html.replace("<?on_" + number + "?>", ManagerStats.TopRichOn[number] == 1 ? "<font color=\"66FF33\">" + yes + "</font>" : "<font color=\"B59A75\">" + no + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(TimeUtils.formatTime(ManagerStats.TopRichOnline[number], player.getLanguage())));
					html = html.replace("<?count_" + number + "?>", Util.formatAdena(ManagerStats.TopRich[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?count_" + number + "?>", "...");
				}
				number++;
			}
			html = html.replace("<?item_name?>", Util.getItemName(Config.BBS_STATISTIC_ITEM));
		}
		else if(page == 6)
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/statistic/rk.htm", player);
			while(number < Config.BBS_STATISTIC_COUNT)
			{
				if(ManagerStats.TopRaidName[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", ManagerStats.TopRaidName[number]);
					html = html.replace("<?clan_" + number + "?>", ManagerStats.TopRaidClan[number] == null ? "<font color=\"B59A75\">" + no + "</font>" : ManagerStats.TopRaidClan[number]);
					html = html.replace("<?sex_" + number + "?>", ManagerStats.TopRaidSex[number] == 0 ? "Male" : "Female");
					html = html.replace("<?class_" + number + "?>", String.valueOf(Util.className(player, ManagerStats.TopRaidClass[number])));
					html = html.replace("<?on_" + number + "?>", ManagerStats.TopRaidOn[number] == 1 ? "<font color=\"66FF33\">" + yes + "</font>" : "<font color=\"B59A75\">" + no + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(TimeUtils.formatTime(ManagerStats.TopRaidOnline[number], player.getLanguage())));
					html = html.replace("<?count_" + number + "?>", Integer.toString(ManagerStats.TopRaid[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?count_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if(page == 7)
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/statistic/cis.htm", player);
			while(number < Config.BBS_STATISTIC_COUNT)
			{
				if(ManagerStats.TopInstanceSoloName[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", ManagerStats.TopInstanceSoloName[number]);
					html = html.replace("<?clan_" + number + "?>", ManagerStats.TopInstanceSoloClan[number] == null ? "<font color=\"B59A75\">" + no + "</font>" : ManagerStats.TopInstanceSoloClan[number]);
					html = html.replace("<?sex_" + number + "?>", ManagerStats.TopInstanceSoloSex[number] == 0 ? "Male" : "Female");
					html = html.replace("<?class_" + number + "?>", String.valueOf(Util.className(player, ManagerStats.TopInstanceSoloClass[number])));
					html = html.replace("<?on_" + number + "?>", ManagerStats.TopInstanceSoloOn[number] == 1 ? "<font color=\"66FF33\">" + yes + "</font>" : "<font color=\"B59A75\">" + no + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(TimeUtils.formatTime(ManagerStats.TopInstanceSoloOnline[number], player.getLanguage())));
					html = html.replace("<?count_" + number + "?>", Integer.toString(ManagerStats.TopInstanceSolo[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?count_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if(page == 8)
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/statistic/cip.htm", player);
			while(number < Config.BBS_STATISTIC_COUNT)
			{
				if(ManagerStats.TopInstancePartyName[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", ManagerStats.TopInstancePartyName[number]);
					html = html.replace("<?clan_" + number + "?>", ManagerStats.TopInstancePartyClan[number] == null ? "<font color=\"B59A75\">" + no + "</font>" : ManagerStats.TopInstancePartyClan[number]);
					html = html.replace("<?sex_" + number + "?>", ManagerStats.TopInstancePartySex[number] == 0 ? "Male" : "Female");
					html = html.replace("<?class_" + number + "?>", String.valueOf(Util.className(player, ManagerStats.TopInstancePartyClass[number])));
					html = html.replace("<?on_" + number + "?>", ManagerStats.TopInstancePartyOn[number] == 1 ? "<font color=\"66FF33\">" + yes + "</font>" : "<font color=\"B59A75\">" + no + "</font>");
					html = html.replace("<?online_" + number + "?>", String.valueOf(TimeUtils.formatTime(ManagerStats.TopInstancePartyOnline[number], player.getLanguage())));
					html = html.replace("<?count_" + number + "?>", Integer.toString(ManagerStats.TopInstanceParty[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?clan_" + number + "?>", "...");
					html = html.replace("<?sex_" + number + "?>", "...");
					html = html.replace("<?class_" + number + "?>", "...");
					html = html.replace("<?on_" + number + "?>", "...");
					html = html.replace("<?online_" + number + "?>", "...");
					html = html.replace("<?count_" + number + "?>", "...");
				}
				number++;
			}
		}
		else if(page == 9)
		{
			html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/statistic/clan.htm", player);
			while(number < Config.BBS_STATISTIC_COUNT)
			{
				if(ManagerStats.TopClanName[number] != null)
				{
					html = html.replace("<?name_" + number + "?>", ManagerStats.TopClanName[number]);
					html = html.replace("<?leader_" + number + "?>", ManagerStats.TopClanLeader[number]);
					html = html.replace("<?level_" + number + "?>", Integer.toString(ManagerStats.TopClanLevel[number]));
					html = html.replace("<?member_" + number + "?>", Integer.toString(ManagerStats.TopClanMember[number]));
				}
				else
				{
					html = html.replace("<?name_" + number + "?>", "...");
					html = html.replace("<?leader_" + number + "?>", "...");
					html = html.replace("<?level_" + number + "?>", "...");
					html = html.replace("<?member_" + number + "?>", "...");
				}
				number++;
			}
		}

		html = html.replace("<?update?>", String.valueOf(time_update));
		html = html.replace("<?last_update?>", String.valueOf(time(update)));
		html = html.replace("<?statistic_menu?>", HtmCache.getInstance().getHtml(Config.BBS_PATH + "/statistic/menu.htm", player));
		ShowBoard.separateAndSend(html, player);
	}

	private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

	public static String date(long time)
	{
		return DATE_FORMAT.format(new Date(time * 1000));
	}

	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");

	public static String time(long time)
	{
		return TIME_FORMAT.format(new Date(time * 1000));
	}

	public void selectTopPVP()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		number = 0;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT char_name, class_id, clanid, sex, online, onlinetime, pkkills, pvpkills FROM characters AS c LEFT JOIN character_subclasses AS cs ON (c.obj_Id=cs.char_obj_id) WHERE cs.isBase=1 ORDER BY pvpkills DESC LIMIT " + Config.BBS_STATISTIC_COUNT);
			rset = statement.executeQuery();

			while(rset.next())
			{
				if(!rset.getString("char_name").isEmpty())
				{
					ManagerStats.TopPvPName[number] = rset.getString("char_name");
					int clan_id = rset.getInt("clanid");
					Clan clan = clan_id == 0 ? null : ClanTable.getInstance().getClan(clan_id);
					ManagerStats.TopPvPClan[number] = clan == null ? null : clan.getName();
					ManagerStats.TopPvPSex[number] = rset.getInt("sex");
					ManagerStats.TopPvPClass[number] = rset.getInt("class_id");
					ManagerStats.TopPvPOn[number] = rset.getInt("online");
					ManagerStats.TopPvPOnline[number] = rset.getInt("onlinetime");
					ManagerStats.TopPvP[number] = rset.getInt("pkkills");
					ManagerStats.TopPvPPvP[number] = rset.getInt("pvpkills");
				}
				else
					ManagerStats.TopPvPName[number] = null;

				number++;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return;
	}

	public void selectTopPK()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		number = 0;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT char_name, class_id, clanid, sex, online, onlinetime, pkkills, pvpkills FROM characters AS c LEFT JOIN character_subclasses AS cs ON (c.obj_Id=cs.char_obj_id) WHERE cs.isBase=1 ORDER BY pkkills DESC LIMIT " + Config.BBS_STATISTIC_COUNT);
			rset = statement.executeQuery();
			while(rset.next())
			{
				if(!rset.getString("char_name").isEmpty())
				{
					ManagerStats.TopPkName[number] = rset.getString("char_name");
					int clan_id = rset.getInt("clanid");
					Clan clan = clan_id == 0 ? null : ClanTable.getInstance().getClan(clan_id);
					ManagerStats.TopPkClan[number] = clan == null ? null : clan.getName();
					ManagerStats.TopPkSex[number] = rset.getInt("sex");
					ManagerStats.TopPkClass[number] = rset.getInt("class_id");
					ManagerStats.TopPkOn[number] = rset.getInt("online");
					ManagerStats.TopPkOnline[number] = rset.getInt("onlinetime");
					ManagerStats.TopPk[number] = rset.getInt("pkkills");
					ManagerStats.TopPkPvP[number] = rset.getInt("pvpkills");
				}
				else
					ManagerStats.TopPkName[number] = null;

				number++;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void selectTopRich()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		number = 0;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT count, char_name, class_id, clanid, sex, online, onlinetime FROM items AS i JOIN characters AS c JOIN character_subclasses AS cs ON c.obj_Id = i.owner_id WHERE i.item_id='" + Config.BBS_STATISTIC_ITEM + "' AND c.obj_Id = cs.char_obj_id AND cs.isBase = '1' ORDER BY i.count DESC LIMIT 0," + Config.BBS_STATISTIC_COUNT);
			rset = statement.executeQuery();
			while(rset.next())
			{
				if(!rset.getString("char_name").isEmpty())
				{
					ManagerStats.TopRichName[number] = rset.getString("char_name");
					int clan_id = rset.getInt("clanid");
					Clan clan = clan_id == 0 ? null : ClanTable.getInstance().getClan(clan_id);
					ManagerStats.TopRichClan[number] = clan == null ? null : clan.getName();
					ManagerStats.TopRichSex[number] = rset.getInt("sex");
					ManagerStats.TopRichClass[number] = rset.getInt("class_id");
					ManagerStats.TopRichOn[number] = rset.getInt("online");
					ManagerStats.TopRichOnline[number] = rset.getInt("onlinetime");
					ManagerStats.TopRich[number] = rset.getLong("count");
				}
				else
					ManagerStats.TopRichName[number] = null;

				number++;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void selectTopClan()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		number = 0;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT clan_id FROM clan_data ORDER BY clan_level DESC, reputation_score DESC LIMIT 0," + Config.BBS_STATISTIC_COUNT + ";");
			rset = statement.executeQuery();

			while(rset.next())
			{
				int clan_id = rset.getInt("clan_id");
				Clan clan = clan_id == 0 ? null : ClanTable.getInstance().getClan(clan_id);

				if(clan != null)
				{
					ManagerStats.TopClanName[number] = clan.getName();
					ManagerStats.TopClanAlly[number] = clan.getAlliance() == null ? null : clan.getAlliance().getAllyName();
					ManagerStats.TopClanLeader[number] = clan.getLeaderName();
					ManagerStats.TopClanLevel[number] = clan.getLevel();
					ManagerStats.TopClanPoint[number] = clan.getReputationScore();
					ManagerStats.TopClanMember[number] = clan.getAllMembers().size();
					ManagerStats.TopClanCastle[number] = clan.getCastle();
					ManagerStats.TopClanFort[number] = clan.getHasFortress();
				}
				else
				{
					ManagerStats.TopClanName[number] = null;
					ManagerStats.TopClanAlly[number] = null;
					ManagerStats.TopClanLeader[number] = null;
					ManagerStats.TopClanLevel[number] = 0;
					ManagerStats.TopClanPoint[number] = 0;
					ManagerStats.TopClanMember[number] = 0;
					ManagerStats.TopClanCastle[number] = 0;
					ManagerStats.TopClanFort[number] = 0;
				}
				number++;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	public void selectTopOnline()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		number = 0;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT char_name, class_id, clanid, sex, online, onlinetime, pkkills, pvpkills FROM characters AS c LEFT JOIN character_subclasses AS cs ON (c.obj_Id=cs.char_obj_id) WHERE cs.isBase=1 ORDER BY onlinetime DESC LIMIT " + Config.BBS_STATISTIC_COUNT);
			rset = statement.executeQuery();
			while(rset.next())
			{
				if(!rset.getString("char_name").isEmpty())
				{
					ManagerStats.TopOnlineName[number] = rset.getString("char_name");
					int clan_id = rset.getInt("clanid");
					Clan clan = clan_id == 0 ? null : ClanTable.getInstance().getClan(clan_id);
					ManagerStats.TopOnlineClan[number] = clan == null ? null : clan.getName();
					ManagerStats.TopOnlineSex[number] = rset.getInt("sex");
					ManagerStats.TopOnlineClass[number] = rset.getInt("class_id");
					ManagerStats.TopOnlineOn[number] = rset.getInt("online");
					ManagerStats.TopOnlineOnline[number] = rset.getInt("onlinetime");
					ManagerStats.TopOnline[number] = rset.getInt("pkkills");
					ManagerStats.TopOnlinePvP[number] = rset.getInt("pvpkills");
				}
				else
					ManagerStats.TopOnlineName[number] = null;

				number++;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	private void selectTopRK()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		number = 0;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT char_name, class_id, clanid, sex, online, onlinetime, raidkills FROM characters AS c LEFT JOIN character_subclasses AS cs ON (c.obj_Id=cs.char_obj_id) WHERE cs.isBase=1 ORDER BY raidkills DESC LIMIT " + 10);
			rset = statement.executeQuery();
			while(rset.next())
			{
				if(!rset.getString("char_name").isEmpty())
				{
					ManagerStats.TopRaidName[number] = rset.getString("char_name");
					int clan_id = rset.getInt("clanid");
					Clan clan = clan_id == 0 ? null : ClanTable.getInstance().getClan(clan_id);
					ManagerStats.TopRaidClan[number] = clan == null ? null : clan.getName();
					ManagerStats.TopRaidSex[number] = rset.getInt("sex");
					ManagerStats.TopRaidClass[number] = rset.getInt("class_id");
					ManagerStats.TopRaidOn[number] = rset.getInt("online");
					ManagerStats.TopRaidOnline[number] = rset.getInt("onlinetime");
					ManagerStats.TopRaid[number] = rset.getInt("raidkills");
				}
				else
					ManagerStats.TopRaidName[number] = null;

				number++;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	private void selectTopCIS()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		number = 0;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT char_name, class_id, clanid, sex, online, onlinetime, soloinstance FROM characters AS c LEFT JOIN character_subclasses AS cs ON (c.obj_Id=cs.char_obj_id) WHERE cs.isBase=1 ORDER BY soloinstance DESC LIMIT " + 10);
			rset = statement.executeQuery();
			while(rset.next())
			{
				if(!rset.getString("char_name").isEmpty())
				{
					ManagerStats.TopInstanceSoloName[number] = rset.getString("char_name");
					int clan_id = rset.getInt("clanid");
					Clan clan = clan_id == 0 ? null : ClanTable.getInstance().getClan(clan_id);
					ManagerStats.TopInstanceSoloClan[number] = clan == null ? null : clan.getName();
					ManagerStats.TopInstanceSoloSex[number] = rset.getInt("sex");
					ManagerStats.TopInstanceSoloClass[number] = rset.getInt("class_id");
					ManagerStats.TopInstanceSoloOn[number] = rset.getInt("online");
					ManagerStats.TopInstanceSoloOnline[number] = rset.getInt("onlinetime");
					ManagerStats.TopInstanceSolo[number] = rset.getInt("soloinstance");
				}
				else
					ManagerStats.TopInstanceSoloName[number] = null;

				number++;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	private void selectTopCIP()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		number = 0;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT char_name, class_id, clanid,sex, online, onlinetime, partyinstance FROM characters AS c LEFT JOIN character_subclasses AS cs ON (c.obj_Id=cs.char_obj_id) WHERE cs.isBase=1 ORDER BY partyinstance DESC LIMIT " + 10);
			rset = statement.executeQuery();
			while(rset.next())
			{
				if(!rset.getString("char_name").isEmpty())
				{
					ManagerStats.TopInstancePartyName[number] = rset.getString("char_name");
					int clan_id = rset.getInt("clanid");
					Clan clan = clan_id == 0 ? null : ClanTable.getInstance().getClan(clan_id);
					ManagerStats.TopInstancePartyClan[number] = clan == null ? null : clan.getName();
					ManagerStats.TopInstancePartySex[number] = rset.getInt("sex");
					ManagerStats.TopInstancePartyClass[number] = rset.getInt("class_id");
					ManagerStats.TopInstancePartyOn[number] = rset.getInt("online");
					ManagerStats.TopInstancePartyOnline[number] = rset.getInt("onlinetime");
					ManagerStats.TopInstanceParty[number] = rset.getInt("partyinstance");
				}
				else
					ManagerStats.TopInstancePartyName[number] = null;

				number++;
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}