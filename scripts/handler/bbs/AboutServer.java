package handler.bbs;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.Rate;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class AboutServer extends ScriptBbsHandler
{
	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbsaboutserver" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(bypass.equals("_bbsaboutserver"))
		{
			aboutServer(player);
			return;
		}
	}

	private static void aboutServer(Player player)
	{
		String html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/bbs_about_server.htm", player);
		Rate rate = player.getRate();
		html = html.replace("<?rate_xp?>", String.valueOf(rate.getExp()));
		html = html.replace("<?rate_sp?>", String.valueOf(rate.getSp()));
		html = html.replace("<?rate_adena?>", String.valueOf(rate.getAdena()));
		html = html.replace("<?rate_items?>", String.valueOf(rate.getItems()));
		html = html.replace("<?rate_spoil?>", String.valueOf(rate.getSpoil()));
		html = html.replace("<?rate_raid?>", String.valueOf(rate.getRaid()));
		html = html.replace("<?rate_siege?>", String.valueOf(Config.RATE_DROP_SIEGE_GUARD));
		html = html.replace("<?rate_fish?>", String.valueOf(Config.RATE_FISH_DROP_COUNT));
		html = html.replace("<?rate_quest?>", "?");
		html = html.replace("<?rate_manor?>", String.valueOf(Config.RATE_MANOR));
		html = html.replace("<?rate_clanrep?>", String.valueOf(Config.RATE_CLAN_REP_SCORE));
		html = html.replace("<?rate_hellbound?>", String.valueOf(Config.RATE_HELLBOUND_CONFIDENCE));

		ShowBoard.separateAndSend(html, player);
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}