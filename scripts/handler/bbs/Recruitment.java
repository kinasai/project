package handler.bbs;

import handler.bbs.RecruitmentData;
import handler.bbs.ScriptBbsHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.dao.AcademiciansDAO;
import com.l2cccp.gameserver.dao.AcademyRequestDAO;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.EventHolder;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.handler.bbs.AcademicCheck;
import com.l2cccp.gameserver.handler.bbs.Academicians;
import com.l2cccp.gameserver.handler.bbs.AcademyRequest;
import com.l2cccp.gameserver.handler.bbs.AcademyStorage;
import com.l2cccp.gameserver.handler.bbs.ClanRequest;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerEnterListener;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.listener.CharListenerList;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.bbs.buffer.GenerateElement;
import com.l2cccp.gameserver.model.entity.events.impl.DominionSiegeEvent;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.model.pledge.SubUnit;
import com.l2cccp.gameserver.model.pledge.UnitMember;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.network.l2.s2c.JoinPledge;
import com.l2cccp.gameserver.network.l2.s2c.PledgeShowInfoUpdate;
import com.l2cccp.gameserver.network.l2.s2c.PledgeShowMemberListAdd;
import com.l2cccp.gameserver.network.l2.s2c.PledgeSkillList;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.network.l2.s2c.SkillList;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.ClanTable;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Recruitment extends ScriptBbsHandler
{
	public static final Logger _log = LoggerFactory.getLogger(Recruitment.class);
	private final Listener _listener = new Listener();
	public static Map<String, Integer> items;

	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbsrecruitment" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.BBS_RECRUITMENT_ALLOW)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}
		bypass = bypass.replace(" ", ":"); // для избежания создания лишнего массива!

		String content = "";
		if(bypass.equals("_bbsrecruitment:academy:add"))
			content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/academy-add.htm", player);
		else if(bypass.equals("_bbsrecruitment:invitelist"))
			content = inviteList(player);
		else if(bypass.startsWith("_bbsrecruitment:invite:sub"))
		{
			String[] data = bypass.split(":");
			int clanId = player.getClanId();
			int obj = Integer.parseInt(data[3]);
			int unity = Integer.parseInt(data[4]);
			if(RecruitmentData.getInstance().checkClanInvite(player, clanId, obj, unity))
				doInvite(clanId, obj, unity);

			onBypassCommand(player, "_bbsrecruitment:clan:id:" + player.getClanId());
			return;
		}
		else if(bypass.startsWith("_bbsrecruitment:academy:invite"))
		{
			String[] data = bypass.split(":");
			askBefor(player, data[3]);

			onBypassCommand(player, "_bbsrecruitment:list:academy:1");
			return;
		}
		else if(bypass.startsWith("_bbsrecruitment:invite:page"))
		{
			String[] data = bypass.split(":");
			content = invitePlayer(player, data[3]);
		}
		else if(bypass.startsWith("_bbsrecruitment:invite:remove"))
		{
			String[] data = bypass.split(":");
			RecruitmentData.getInstance().inviteRemove(player, data[3]);
			if(player.getClan().getInviteList().size() > 0)
				onBypassCommand(player, "_bbsrecruitment:invitelist");
			else
				onBypassCommand(player, "_bbsrecruitment:clan:id:" + player.getClanId());
			return;
		}
		else if(bypass.startsWith("_bbsrecruitment:list"))
		{
			String[] data = bypass.split(":");
			String page = data.length < 4 ? "1" : data[3];
			if(data[2].equals("clan"))
				content = buildClanList(page, player);
			else if(data[2].equals("academy"))
				content = buildAcademyList(page, player);
		}
		else if(bypass.startsWith("_bbsrecruitment:clan:id"))
			content = buildClanPage(bypass.split(":")[3], player);
		else if(bypass.startsWith("_bbsrecruitment:invite"))
		{
			String[] data = bypass.split(":");
			String clanId = data[2];
			String note = Util.ArrayToString(data, 3);
			RecruitmentData.getInstance().sendInviteTask(player, clanId, note, true);
			onBypassCommand(player, "_bbsrecruitment:clan:id:" + clanId);
			return;
		}
		else if(bypass.startsWith("_bbsrecruitment:warclan"))
		{
			String[] data = bypass.split(":");
			int war = Integer.parseInt(data[2]);
			RecruitmentData.getInstance().checkAndStartWar(player, war);
			onBypassCommand(player, "_bbsrecruitment:clan:id:" + war);
			return;
		}
		else if(bypass.startsWith("_bbsrecruitment:unwarclan"))
		{
			String[] data = bypass.split(":");
			int war = Integer.parseInt(data[2]);
			RecruitmentData.getInstance().checkAndStopWar(player, war);
			onBypassCommand(player, "_bbsrecruitment:clan:id:" + war);
			return;
		}
		else if(bypass.startsWith("_bbsrecruitment:removeinvite"))
		{
			String clanId = bypass.split(":")[2];
			RecruitmentData.getInstance().sendInviteTask(player, clanId, null, false);
			onBypassCommand(player, "_bbsrecruitment:clan:id:" + clanId);
			return;
		}

		ShowBoard.separateAndSend(content, player);
	}

	private String buildAcademyList(String page_num, Player player)
	{
		if(page_num == null)
			return null;

		int page = Integer.parseInt(page_num);
		String content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/academy-list.htm", player);
		int start = (page - 1) * 10;
		int end = Math.min(page * 10, AcademyStorage.getInstance().getAcademyList().size());

		String template = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/academy-list-template.htm", player);
		String body = "";
		for(int i = start; i < end; i++)
		{
			Clan clan = AcademyStorage.getInstance().getAcademyList().get(i);
			AcademyRequest request = AcademyStorage.getInstance().getReguest(clan.getClanId());
			String academytpl = template;
			academytpl = academytpl.replace("<?action?>", "bypass _bbsrecruitment:academy:invite:" + clan.getClanId());
			academytpl = academytpl.replace("<?color?>", i % 2 == 1 ? "333333" : "A7A19A");
			academytpl = academytpl.replace("<?position?>", String.valueOf(i + 1));
			academytpl = academytpl.replace("<?clan_name?>", clan.getName().length() > 12 ? clan.getName().substring(0, 11) + "..." : clan.getName());
			academytpl = academytpl.replace("<?clan_owner?>", clan.getLeaderName());
			academytpl = academytpl.replace("<?seats?>", String.valueOf(request.getSeats() + " мест"));
			academytpl = academytpl.replace("<?price?>", Util.formatPay(player, request.getPrice(), request.getItem()));
			academytpl = academytpl.replace("<?time?>", String.valueOf(request.getTime() + " часов"));
			body += academytpl;
		}

		content = content.replace("<?add?>", AddAcademy(player));
		content = content.replace("<?navigate?>", parseNavigate(page, false));
		content = content.replace("<?body?>", body);

		return content;
	}

	public String AddAcademy(Player player)
	{
		Clan clan = player.getClan();
		if(clan == null || clan.getLeader().getPlayer() != player)
			return "<font color=\"FF0000\">Добавить набор в Академию могут только Клан Лидеры!</font>";
		else if(clan.getSubUnit(Clan.SUBUNIT_ACADEMY) == null)
			return "<font color=\"FF0000\">Вы не можете добавить набор, так как у Вашего клана нет Академии!</font>";
		else if(AcademyStorage.getInstance().getReguest(clan.getClanId()) != null)
			return GenerateElement.button("Удалить набор в Академию", "_bbsbypass:services.RecruitmentPanel:removeAcademy", 180, 28, "L2UI_CT1.Button_DF_Down", "L2UI_CT1.Button_DF");
		else
			return GenerateElement.button("Добавить набор в Академию", "_bbsbypass:services.RecruitmentPanel:addAcademy", 180, 28, "L2UI_CT1.Button_DF_Down", "L2UI_CT1.Button_DF");
	}

	private String invitePlayer(Player player, String string)
	{
		String html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/invite-player-info.htm", player);

		Clan clan = player.getClan();
		if(clan == null)
			return null;

		int obj = Integer.parseInt(string);

		ClanRequest request = ClanRequest.getClanInvitePlayer(clan.getClanId(), obj);
		if(request == null)
			return null;

		String No = new CustomMessage("common.result.no").toString(player);
		String Yes = new CustomMessage("common.result.yes").toString(player);

		Player invited = request.getPlayer();

		if(!invited.isOnline())
		{
			Player restore = RecruitmentData.getInstance().restore(obj);
			if(restore != null)
				invited = restore;
		}

		html = html.replace("<?name?>", invited.getName());
		html = html.replace("<?online?>", String.valueOf(invited.isOnline() ? "<font color=\"18FF00\">" + Yes + "</font>" : "<font color=\"FF0000\">" + No + "</font>"));
		html = html.replace("<?noblesse?>", String.valueOf(invited.isNoble() ? "<font color=\"18FF00\">" + Yes + "</font>" : "<font color=\"FF0000\">" + No + "</font>"));
		html = html.replace("<?hero?>", String.valueOf(invited.isHero() ? "<font color=\"18FF00\">" + Yes + "</font>" : "<font color=\"FF0000\">" + No + "</font>"));
		html = html.replace("<?level?>", String.valueOf(invited.getLevel()));
		html = html.replace("<?time?>", TimeUtils.toSimpleFormat(request.getTime()));
		html = html.replace("<?class?>", Util.className(player, player.getBaseClassId()).toString());
		html = html.replace("<?remove?>", "bypass _bbsrecruitment:invite:remove:" + invited.getObjectId());
		html = html.replace("<?note?>", request.getNote());
		if(!invited.isOnline())
			html = html.replace("<?button?>", HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/invite-player-info-button-off.htm", player));
		else
		{
			String button = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/invite-player-info-button.htm", player);
			String block = "";
			String list = "";

			int i = 1;
			final int[] unity = new int[] { 0, 100, 200, 1001, 1002, 2001, 2002 };
			for(int id : unity)
			{
				SubUnit sub = clan.getSubUnit(id);
				if(sub != null && sub.size() < clan.getSubPledgeLimit(id))
				{
					block = button;
					block = block.replace("<?color?>", i % 2 == 1 ? "99CC33" : "669933");
					block = block.replace("<?action?>", "bypass _bbsrecruitment:invite:sub:" + invited.getObjectId() + ":" + id);
					block = block.replace("<?unity?>", takeFullSubName(id, sub.getName()));
					list += block;
					i++;
				}
			}

			if(list.isEmpty())
				list = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/invite-player-info-button-limit.htm", player);

			html = html.replace("<?button?>", list);
		}

		return html;
	}

	private String takeFullSubName(int id, String name)
	{
		String type = null;
		switch(id)
		{
			case 0:
				type = "Main Clan";
				break;
			case 100:
				type = "1st Royal Guard";
				break;
			case 200:
				type = "2nd Royal Guard";
				break;
			case 1001:
				type = "1st Order of Knights";
				break;
			case 1002:
				type = "2nd Order of Knights";
				break;
			case 2001:
				type = "3rd Order of Knights";
				break;
			case 2002:
				type = "4th Order of Knights";
				break;
		}

		type += ": " + name;

		return type;
	}

	private String inviteList(Player player)
	{
		Clan clan = player.getClan();

		if(clan == null)
			return null;

		String html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/invite-list.htm", player);
		String template = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/invite-list-template.htm", player);
		String block = "";
		String list = "";
		String No = new CustomMessage("common.result.no").toString(player);
		String Yes = new CustomMessage("common.result.yes").toString(player);
		int i = 1;
		for(ClanRequest request : clan.getInviteList())
		{
			Player invited = request.getPlayer();

			if(!invited.isOnline())
			{
				Player restore = RecruitmentData.getInstance().restore(invited.getObjectId());
				if(restore != null)
					invited = restore;
			}

			long time = request.getTime();

			block = template;
			block = block.replace("<?color?>", i % 2 == 1 ? "333333" : "A7A19A");
			block = block.replace("<?name?>", invited.getName());
			block = block.replace("<?online?>", String.valueOf(invited.isOnline() ? "<font color=\"18FF00\">" + Yes + "</font>" : "<font color=\"FF0000\">" + No + "</font>"));
			block = block.replace("<?noblesse?>", String.valueOf(invited.isNoble() ? "<font color=\"18FF00\">" + Yes + "</font>" : "<font color=\"FF0000\">" + No + "</font>"));
			block = block.replace("<?level?>", String.valueOf(invited.getLevel()));
			block = block.replace("<?time?>", TimeUtils.toSimpleFormat(time));
			block = block.replace("<?class?>", Util.className(invited, invited.getBaseClassId()).toString());
			block = block.replace("<?action?>", "bypass _bbsrecruitment:invite:page:" + invited.getObjectId());
			block = block.replace("<?remove?>", "bypass _bbsrecruitment:invite:remove:" + invited.getObjectId());
			list += block;
			i++;
		}
		html = html.replace("<?list?>", list);
		html = html.replace("<?action?>", "bypass _bbsrecruitment:clan:id:" + player.getClanId());
		return html;
	}

	private String buildClanList(String page_num, Player player)
	{
		if(page_num == null)
			return null;

		int page = Integer.parseInt(page_num);
		String content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-list.htm", player);
		int start = (page - 1) * 10;
		int end = Math.min(page * 10, ClanRequest.getClanList().size());

		String template = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-list-template.htm", player);
		String body = "";
		String No = new CustomMessage("common.result.no").toString(player);
		for(int i = start; i < end; i++)
		{
			Clan clan = ClanRequest.getClanList().get(i);
			String clantpl = template;
			clantpl = clantpl.replace("<?action?>", "bypass _bbsrecruitment:clan:id:" + clan.getClanId());
			clantpl = clantpl.replace("<?color?>", i % 2 == 1 ? "333333" : "A7A19A");
			clantpl = clantpl.replace("<?position?>", String.valueOf(i + 1));
			clantpl = clantpl.replace("<?ally_crest?>", clan.getAlliance() != null && clan.getAlliance().getAllyCrestId() > 0 ? "Crest.crest_" + Config.REQUEST_ID + "_" + clan.getAlliance().getAllyCrestId() : "L2UI_CH3.ssq_bar2back");
			clantpl = clantpl.replace("<?clan_name?>", clan.getName().length() > 12 ? clan.getName().substring(0, 11) + "..." : clan.getName());
			clantpl = clantpl.replace("<?clan_crest?>", clan.hasCrest() ? "Crest.crest_" + Config.REQUEST_ID + "_" + clan.getCrestId() : "L2UI_CH3.ssq_bar1back");
			clantpl = clantpl.replace("<?clan_owner?>", clan.getLeaderName());
			clantpl = clantpl.replace("<?level_color?>", clan.getLevel() == 11 ? "FFCC33" : (clan.getLevel() <= 10 && clan.getLevel() >= 8 ? "66CCCC" : "339933"));
			clantpl = clantpl.replace("<?clan_level?>", String.valueOf(clan.getLevel()));
			clantpl = clantpl.replace("<?clan_base?>", String.valueOf(clan.getCastle() != 0 ? RecruitmentData.getInstance().name(clan.getCastle()) : clan.getHasFortress() != 0 ? RecruitmentData.getInstance().name(clan.getHasFortress()) : No));
			clantpl = clantpl.replace("<?clan_hall?>", clan.getHasHideout() != 0 ? RecruitmentData.getInstance().name(clan.getHasHideout()) : No);
			clantpl = clantpl.replace("<?member_count?>", String.valueOf(clan.getAllSize()));
			body += clantpl;
		}

		Clan my_clan = player.getClan();
		if(my_clan != null && my_clan.getLevel() > 0)
		{
			String clantpl = template;
			clantpl = clantpl.replace("<?action?>", "bypass _bbsrecruitment:clan:id:" + my_clan.getClanId());
			clantpl = clantpl.replace("<?color?>", "669933");
			clantpl = clantpl.replace("<?position?>", "MY");
			clantpl = clantpl.replace("<?ally_crest?>", my_clan.getAlliance() != null && my_clan.getAlliance().getAllyCrestId() > 0 ? "Crest.crest_" + Config.REQUEST_ID + "_" + my_clan.getAlliance().getAllyCrestId() : "L2UI_CH3.ssq_bar2back");
			clantpl = clantpl.replace("<?clan_name?>", my_clan.getName().length() > 12 ? my_clan.getName().substring(0, 11) + "..." : my_clan.getName());
			clantpl = clantpl.replace("<?clan_crest?>", my_clan.hasCrest() ? "Crest.crest_" + Config.REQUEST_ID + "_" + my_clan.getCrestId() : "L2UI_CH3.ssq_bar1back");
			clantpl = clantpl.replace("<?clan_owner?>", my_clan.getLeaderName());
			clantpl = clantpl.replace("<?level_color?>", my_clan.getLevel() == 11 ? "FFCC33" : (my_clan.getLevel() <= 10 && my_clan.getLevel() >= 8 ? "66CCCC" : "339933"));
			clantpl = clantpl.replace("<?clan_level?>", String.valueOf(my_clan.getLevel()));
			clantpl = clantpl.replace("<?clan_base?>", String.valueOf(my_clan.getCastle() != 0 ? RecruitmentData.getInstance().name(my_clan.getCastle()) : my_clan.getHasFortress() != 0 ? RecruitmentData.getInstance().name(my_clan.getHasFortress()) : No));
			clantpl = clantpl.replace("<?clan_hall?>", my_clan.getHasHideout() != 0 ? RecruitmentData.getInstance().name(my_clan.getHasHideout()) : No);
			clantpl = clantpl.replace("<?member_count?>", String.valueOf(my_clan.getAllSize()));
			body += clantpl;
		}

		content = content.replace("<?navigate?>", parseNavigate(page, true));
		content = content.replace("<?body?>", body);

		return content;
	}

	private String parseNavigate(int page, boolean clan)
	{
		StringBuilder pg = new StringBuilder();

		double size = clan ? ClanRequest.getClanList().size() : AcademyStorage.getInstance().get().size();
		double inpage = 10;

		if(size > inpage)
		{
			double max = Math.ceil(size / inpage);

			pg.append("<center><table width=25 border=0><tr>");
			int line = 1;

			for(int current = 1; current <= max; current++)
			{
				if(page == current)
					pg.append("<td width=25 align=center><button value=\"[").append(current).append("]\" width=38 height=25 back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\"></td>");
				else
					pg.append("<td width=25 align=center><button value=\"").append(current).append("\" action=\"bypass _bbsrecruitment:list:" + (clan ? "clan" : "academy") + ":").append(current).append("\" width=28 height=25 back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\"></td>");

				if(line == 22)
				{
					pg.append("</tr><tr>");
					line = 0;
				}
				line++;
			}

			pg.append("</tr></table></center>");
		}

		return pg.toString();
	}

	private String buildClanPage(String string, Player player)
	{
		if(string == null)
			return null;

		int clanId = Integer.parseInt(string);

		Clan clan = ClanTable.getInstance().getClan(clanId);

		if(clan == null)
			return null;

		String No = new CustomMessage("common.result.no").toString(player);
		String content = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page.htm", player);
		content = content.replace("<?clan_name?>", clan.getName());
		content = content.replace("<?clan_ally?>", clan.getAlliance() != null ? clan.getAlliance().getAllyName() : No);
		content = content.replace("<?clan_owner?>", clan.getLeaderName());
		content = content.replace("<?clan_id?>", String.valueOf(clan.getClanId()));
		content = content.replace("<?clan_level?>", String.valueOf(clan.getLevel()));
		content = content.replace("<?clan_base?>", clan.getCastle() != 0 ? RecruitmentData.getInstance().name(clan.getCastle()) : clan.getHasFortress() != 0 ? RecruitmentData.getInstance().name(clan.getHasFortress()) : No);
		content = content.replace("<?clan_hall?>", clan.getHasHideout() != 0 ? RecruitmentData.getInstance().name(clan.getHasHideout()) : No);
		content = content.replace("<?clan_members?>", String.valueOf(clan.getAllSize()));
		content = content.replace("<?clan_point?>", Util.formatAdena(clan.getReputationScore()));
		content = content.replace("<?clan_avarage_level?>", Util.formatAdena(clan.getAverageLevel()));
		content = content.replace("<?clan_online?>", Util.formatAdena(clan.getOnlineMembers(0).size()));

		int pvp = 0;
		int pvp_total = 0;
		int pk = 0;
		String top_pk = "---";
		String top_pvp = "---";
		for(UnitMember pl : clan)
		{
			final int _pvp = pl.getPvp();
			final int _pk = pl.getPk();

			if(_pvp > pvp)
				top_pvp = pl.getName();
			if(_pk > pk)
				top_pk = pl.getName();

			pvp = _pvp;
			pk = _pk;
			pvp_total += _pvp;
		}

		content = content.replace("<?clan_pvp?>", Util.formatAdena(pvp_total));
		content = content.replace("<?clan_today_pvp?>", Util.formatAdena(clan.getPvP()));

		content = content.replace("<?clan_top_pvp?>", top_pvp);
		content = content.replace("<?clan_top_pk?>", top_pk);

		Clan myclan = player.getClan();
		if(myclan == null)
		{
			String page = null;
			if(clan.checkInviteList(player.getObjectId()))
			{
				page = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-remove_invite.htm", player);
				page = page.replace("<?bypass?>", "bypass _bbsrecruitment:removeinvite:" + clan.getClanId());
			}
			else
			{
				page = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-invite.htm", player);
				page = page.replace("<?bypass?>", "bypass _bbsbypass:services.RecruitmentPanel:addRequest " + clan.getClanId() + ";_bbsrecruitment:clan:id:" + clan.getClanId());
			}

			content = content.replace("<?container?>", page);
		}
		else if(myclan == clan && clan.getLeader().getPlayer() == player && clan.getInviteList().size() > 0)
		{
			String page = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-inviteinfo.htm", player);
			page = page.replace("<?bypass?>", "bypass _bbsrecruitment:invitelist");
			page = page.replace("<?invite_count?>", String.valueOf(clan.getInviteList().size()));
			content = content.replace("<?container?>", page);
		}
		else
			content = content.replace("<?container?>", "");

		if(RecruitmentData.getInstance().checkClanWar(clan, myclan, player, false))
		{
			if(myclan.isAtWarWith(clan.getClanId()))
			{
				String page = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-unwar.htm", player);
				page = page.replace("<?bypass?>", "bypass _bbsrecruitment:unwarclan:" + clan.getClanId());
				content = content.replace("<?war?>", page);
			}
			else
			{
				String page = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-war.htm", player);
				page = page.replace("<?bypass?>", "bypass _bbsrecruitment:warclan:" + clan.getClanId());
				content = content.replace("<?war?>", page);
			}
		}
		else
			content = content.replace("<?war?>", "");

		if(RecruitmentData.getInstance().haveWars(clan))
		{
			String war_body = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-war-list.htm", player);
			String war_temp = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-war-list-template.htm", player);
			String block = "";
			String list = "";
			int num = 0;
			for(Clan war : clan.getEnemyClans())
			{
				num++;
				if(num <= 6 && war.isAtWarWith(clan.getClanId()) && clan.isAtWarWith(war.getClanId()))
				{
					block = war_temp;
					block = block.replace("<?color?>", num % 2 == 1 ? "333333" : "A7A19A");
					block = block.replace("<?clan_name?>", war.getName());
					block = block.replace("<?clan_leader?>", war.getLeaderName());
					block = block.replace("<?clan_level?>", String.valueOf(war.getLevel()));
					block = block.replace("<?clan_member?>", String.valueOf(war.getAllSize()));
					block = block.replace("<?war_link?>", "bypass _bbsrecruitment:clan:id:" + war.getClanId());
					list += block;
				}
				else
					num--;
			}

			war_body = war_body.replace("<?war_list?>", list);
			war_body = war_body.replace("<?war_count?>", String.valueOf(num));
			content = content.replace("<?clan_warlist?>", war_body);
		}
		else
			content = content.replace("<?clan_warlist?>", HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-war-list-empty.htm", player));

		if(clan.getSkills().size() > 0)
		{
			String skill_body = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-skills.htm", player);
			String skill_temp = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-skills-template.htm", player);
			String block = "";
			String list = "";
			int count = 1;
			for(SkillEntry skill : clan.getSkills())
			{
				block = skill_temp;
				block = block.replace("<?skill_level?>", String.valueOf(skill.getLevel()));
				block = block.replace("<?skill_icon?>", skill.getTemplate().getIcon());
				list += block;

				if(count % 9 == 0)
					list += "</tr><tr>";

				count++;
			}

			final int[] unity = new int[] { 0, 100, 200, 1001, 1002, 2001, 2002 };
			ArrayList<SkillEntry> unity_list = new ArrayList<SkillEntry>();
			for(int id : unity)
			{
				SubUnit sub = clan.getSubUnit(id);
				if(sub != null)
				{
					for(SkillEntry skill : sub.getSkills())
					{
						unity_list.add(skill);
					}
				}
			}

			for(SkillEntry skill : unity_list)
			{
				block = skill_temp;
				block = block.replace("<?skill_level?>", String.valueOf(skill.getLevel()));
				block = block.replace("<?skill_icon?>", skill.getTemplate().getIcon());
				list += block;

				if(count % 9 == 0)
					list += "</tr><tr>";

				count++;
			}

			skill_body = skill_body.replace("<?skill_list?>", list);
			content = content.replace("<?clan_skill?>", skill_body);
		}
		else
			content = content.replace("<?clan_skill?>", HtmCache.getInstance().getHtml(Config.BBS_PATH + "/recruitment/clan-page-skills-empty.htm", player));

		String description = clan.getDescription();
		final boolean isnull = description == null || description.isEmpty();
		content = content.replace("<?clan_notice?>", isnull ? "<center><font color=\"FF0000\">Empty</font></center>" : description);
		content = content.replace("<?edit_notice?>", clan.getLeader().getPlayer() == player ? ("<button " + (isnull ? "action=\"bypass _bbsbypass:services.RecruitmentPanel:addDescription;_bbsrecruitment:clan:id:" + clan.getClanId() + "\" value=\"Add\"" : "action=\"bypass _bbsbypass:services.RecruitmentPanel:editDescription;_bbsrecruitment:clan:id:" + clan.getClanId() + "\" value=\"Edit\"") + " width=120 height=24 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"/>") : "");

		return content;
	}

	@Override
	public void onInit()
	{
		super.onInit();

		if(Config.BBS_RECRUITMENT_ALLOW)
		{
			CharListenerList.addGlobal(_listener);
			ClanRequest.updateList();
			ThreadPoolManager.getInstance().schedule(new AcademicCheck(), 30000L);
			AcademyRequestDAO.getInstance().load(); // Загружаем Академии!
			AcademiciansDAO.getInstance().load(); // Загружаем Академиков!
			items = new HashMap<String, Integer>();
			for(int item : Config.BBS_RECRUITMENT_ITEMS)
			{
				String name = ItemHolder.getInstance().getTemplate(item).getName();
				items.put(name, item);
			}
		}
	}

	private class Listener implements OnPlayerEnterListener
	{
		@Override
		public void onPlayerEnter(Player player)
		{
			Clan clan = player.getClan();
			if(clan == null || clan.getLevel() < 2)
				return;

			if(clan.getDescription() == null)
			{
				Connection con = null;
				PreparedStatement statement = null;
				ResultSet rset = null;
				String description = null;
				try
				{
					con = DatabaseFactory.getInstance().getConnection();
					statement = con.prepareStatement("SELECT description FROM `clan_description` WHERE `clan_id` = ?");
					statement.setInt(1, clan.getClanId());
					rset = statement.executeQuery();
					if(rset.next())
						description = rset.getString("description");
				}
				catch(Exception e)
				{
					_log.error("CommunityBoard -> Recruitment.onPlayerEnter(): " + e);
				}
				finally
				{
					DbUtils.closeQuietly(con, statement, rset);
				}

				if(description != null)
				{
					description = description.replace("\n", "<br1>\n");
					clan.setDescription(description);
				}
			}
		}
	}

	private void doInvite(int clanId, int obj, int unity)
	{
		Player player = GameObjectsStorage.getPlayer(obj);

		if(player == null || !player.isOnline())
			return;

		Clan clan = ClanTable.getInstance().getClan(clanId);

		player.sendPacket(new JoinPledge(clan.getClanId()));
		SubUnit subUnit = clan.getSubUnit(unity);
		if(subUnit == null)
			return;

		UnitMember member = new UnitMember(clan, player.getName(), player.getTitle(), player.getLevel(), player.getClassId().getId(), player.getObjectId(), unity, player.getPowerGrade(), player.getApprentice(), player.getSex(), player.getPvpKills(), player.getPkKills(), Clan.SUBUNIT_NONE);
		subUnit.addUnitMember(member);

		player.setPledgeType(unity);
		player.setClan(clan);

		member.setPlayerInstance(player, false);

		if(unity == Clan.SUBUNIT_ACADEMY)
			player.setLvlJoinedAcademy(player.getLevel());

		member.setPowerGrade(clan.getAffiliationRank(player.getPledgeType()));

		clan.broadcastToOtherOnlineMembers(new PledgeShowMemberListAdd(member), player);
		clan.broadcastToOnlineMembers(new SystemMessage(SystemMsg.S1_HAS_JOINED_THE_CLAN).addString(player.getName()), new PledgeShowInfoUpdate(clan));

		player.sendPacket(SystemMsg.ENTERED_THE_CLAN);
		player.sendPacket(player.getClan().listAll());
		player.setLeaveClanTime(0);
		player.updatePledgeClass();

		clan.addSkillsQuietly(player);
		player.sendPacket(new PledgeSkillList(clan));
		player.sendPacket(new SkillList(player));

		EventHolder.getInstance().findEvent(player);
		if(clan.getWarDominion() > 0) // баг оффа, после вступа в клан нужен релог для квестов
		{
			DominionSiegeEvent siegeEvent = player.getEvent(DominionSiegeEvent.class);

			siegeEvent.updatePlayer(player, true);
		}
		else
			player.broadcastCharInfo();

		player.store(false);

		if(unity != Clan.SUBUNIT_ACADEMY)
			ClanRequest.removeClanInvitePlayer(clan.getClanId(), player.getObjectId());

	}

	private void askBefor(Player player, String clanId)
	{
		int id = Integer.parseInt(clanId);
		AcademyRequest request;

		if((request = AcademyStorage.getInstance().getReguest(id)) == null)
			return;

		Clan clan;

		if((clan = ClanTable.getInstance().getClan(id)) == null)
			return;

		String msg = String.valueOf(new CustomMessage("academy.ask").addString(clan.getName()).addString(Util.formatPay(player, request.getPrice(), request.getItem())).addNumber(request.getTime()).toString(player));
		ConfirmDlg dlg = new ConfirmDlg(SystemMsg.S1, 30000).addString(msg);
		player.ask(dlg, new Ask(id));
	}

	private class Ask implements OnAnswerListener
	{
		private int clanId;

		protected Ask(int clanId)
		{
			this.clanId = clanId;
		}

		@Override
		public void sayYes(Player player)
		{
			if(player == null)
				return;

			if(RecruitmentData.getInstance().checkAcademyInvite(player))
			{
				AcademyRequest request = AcademyStorage.getInstance().getReguest(clanId);
				if(inviteAcademy(request, player))
				{
					if(!AcademicCheck.canRun())
						ThreadPoolManager.getInstance().schedule(new AcademicCheck(), 60000L);

					Academicians academic = new Academicians(System.currentTimeMillis() + (request.getTime() * 60 * 60 * 1000), player.getObjectId(), request.getClanId());
					AcademiciansDAO.getInstance().insert(academic);

					request.reduceSeats();
					doInvite(request.getClanId(), player.getObjectId(), Clan.SUBUNIT_ACADEMY);
					AcademyStorage.getInstance().updateList();
					onBypassCommand(player, "_bbsrecruitment:list:academy:1");
				}
			}
		}

		@Override
		public void sayNo(Player player)
		{}
	}

	private boolean inviteAcademy(AcademyRequest request, Player player)
	{
		Clan clan = ClanTable.getInstance().getClan(request.getClanId());

		if(clan == null)
			return false;
		else if(clan.getSubUnit(Clan.SUBUNIT_ACADEMY) == null)
			return false;
		else if(clan.getUnitMembersSize(Clan.SUBUNIT_ACADEMY) >= clan.getSubPledgeLimit(Clan.SUBUNIT_ACADEMY))
			return false;
		else if(request.getSeats() <= 0)
			return false;

		return true;
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}
