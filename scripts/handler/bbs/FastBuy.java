package handler.bbs;

import java.util.Map.Entry;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.xml.holder.FastBuyHolder;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.items.FastBuyData;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FastBuy extends ScriptBbsHandler
{
	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbsbuy" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.COMMUNITYBOARD_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			Util.communityNextPage(player, "_bbshome");
			return;
		}

		if(bypass.startsWith("_bbsbuy"))
		{
			//Example: _bbsbuy:1;_bbshome
			String[] bp = bypass.split(";");
			if(bp.length > 1)
				Util.communityNextPage(player, bp[1]);

			String id = bp[0].split(":")[1];
			if(Util.isNumber(id))
				ask(player, Integer.parseInt(id));
		}
	}

	public void ask(Player player, int id)
	{
		FastBuyData data = FastBuyHolder.getInstance().getProduct(id);
		if(data == null)
			return;

		String msg = new CustomMessage("item.buy.ask").addString(data.getName()).addString(Util.formatPay(player, data.getPriceCount(), data.getPriceId())).toString(player);
		ConfirmDlg ask = new ConfirmDlg(SystemMsg.S1, 60000);
		ask.addString(msg);

		player.ask(ask, new AnswerListener(data));
	}

	private class AnswerListener implements OnAnswerListener
	{
		private FastBuyData data;

		protected AnswerListener(FastBuyData data)
		{
			this.data = data;
		}

		@Override
		public void sayYes(Player player)
		{
			if(Util.getPay(player, data.getPriceId(), data.getPriceCount(), true))
			{
				for(Entry<Integer, Long> map : data.getList().entrySet())
					ItemFunctions.addItem(player, map.getKey(), map.getValue());
			}
		}

		@Override
		public void sayNo(Player player)
		{}
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}