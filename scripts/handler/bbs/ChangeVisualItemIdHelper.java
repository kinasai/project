package handler.bbs;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.utils.Util;

public class ChangeVisualItemIdHelper
{
	public static boolean changeVisualItemId(Player player, int objectId, int newVisualObjectId)
	{
		ItemInstance item = player.getInventory().getItemByObjectId(objectId);
		ItemInstance visual = player.getInventory().getItemByObjectId(newVisualObjectId);

		if(item == null || visual == null || item.getVisualItemId() != 0)
			return false;

		if(!ChangeVisualItemIdHelper.isValid(item.getTemplate(), visual.getTemplate()))
			return false;

		int id = item.isArmor() && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_COSTUME ? Config.BBS_BODY_CHANGE_ARMOR[1] : item.isDecoration() ? Config.BBS_BODY_CHANGE_ACCESSORIES[1] : item.isWeapon() ? Config.BBS_BODY_CHANGE_WEAPON[1] : item.isArmor() && item.getTemplate().getBodyPart() == ItemTemplate.SLOT_COSTUME ? Config.BBS_BODY_CHANGE_COSTUME[1] : 4037;
		int count = item.isArmor() && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_COSTUME ? Config.BBS_BODY_CHANGE_ARMOR[0] : item.isDecoration() ? Config.BBS_BODY_CHANGE_ACCESSORIES[0] : item.isWeapon() ? Config.BBS_BODY_CHANGE_WEAPON[0] : item.isArmor() && item.getTemplate().getBodyPart() == ItemTemplate.SLOT_COSTUME ? Config.BBS_BODY_CHANGE_COSTUME[0] : 1000000000;
		if(Util.getPay(player, id, count, true))
		{
			if(player.getInventory().destroyItem(visual))
			{
				player.sendPacket(SystemMessage.removeItems(visual));
				item.setVisualItemId(visual.getItemId());

				if(visual.getTemplate().getBodyPart() == ItemTemplate.SLOT_COSTUME)
					item.setCustomFlags(item.getCustomFlags() | ItemInstance.FLAG_COSTUME);

				item.setJdbcState(JdbcEntityState.UPDATED);
				item.update();
				player.sendUserInfo(true);
				player.broadcastUserInfo(true);
				return true;
			}
			else
			{
				player.getInventory().addItem(id, count);
				return false;
			}
		}
		else
			return false;
	}

	public static boolean isValid(ItemTemplate template, ItemTemplate template2)
	{
		return (template.getBodyPart() == ItemTemplate.SLOT_CHEST || template.getBodyPart() == ItemTemplate.SLOT_FULL_ARMOR) && template2.getBodyPart() == ItemTemplate.SLOT_COSTUME || template.getBodyPart() == template2.getBodyPart();
	}

	public static boolean dropVisualItemId(Player player, int objectId)
	{
		ItemInstance itemByObjectId = player.getInventory().getItemByObjectId(objectId);

		if(itemByObjectId == null || itemByObjectId.getVisualItemId() == 0)
			return false;

		itemByObjectId.setVisualItemId(0);
		itemByObjectId.setCustomFlags(itemByObjectId.getCustomFlags() & ~ItemInstance.FLAG_COSTUME);
		itemByObjectId.setJdbcState(JdbcEntityState.UPDATED);
		itemByObjectId.update();

		player.sendUserInfo(true);
		player.broadcastUserInfo(true);
		return true;
	}
}
