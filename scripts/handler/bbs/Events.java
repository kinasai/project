package handler.bbs;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.EventHolder;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.events.Event;
import com.l2cccp.gameserver.model.entity.events.EventType;
import com.l2cccp.gameserver.model.entity.events.impl.PartyVsPartyDuelEvent;
import com.l2cccp.gameserver.model.entity.events.impl.PlayerVsPlayerDuelEvent;
import com.l2cccp.gameserver.model.entity.events.impl.TeamEvent;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.ShowBoard;
import com.l2cccp.gameserver.utils.Util;

import events.CustomGvGBattleEvent;
import events.CustomPvPBattleEvent;

public class Events extends ScriptBbsHandler
{

	@Override
	public String[] getBypassCommands()
	{
		return new String[] { "_bbsevents" };
	}

	@Override
	public void onBypassCommand(Player player, String bypass)
	{
		if(player.getTeam() != TeamType.NONE && player.isInPvPEvent()) {
			return;
		}
		if(!Config.COMMUNITYBOARD_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			Util.communityNextPage(player, "_bbshome");
			return;
		}

		String html = "";
		if(bypass.equals("_bbsevents"))
		{
			onBypassCommand(player, "_bbsevents:page:index");
			return;
		}
		else if(bypass.startsWith("_bbsevents"))
		{
			String[] page = bypass.split(":");

			if(page.length < 3)
			{
				player.sendMessage("Error 404!");
				Util.communityNextPage(player, "_bbsevents:page:index");
				return;
			}

			String name = page[1];
			String param = page[2];
			if(name.equals("page"))
			{
				html = HtmCache.getInstance().getHtml(Config.BBS_PATH + "/events/" + param + ".htm", player);
				if(param.equals("index"))
				{
					int i = 1;
					for(Event event : EventHolder.getInstance().getEvents().values())
					{
						if(event != null)
						{
							if(event instanceof TeamEvent)
							{
								TeamEvent pvp = (TeamEvent) event;
								if(pvp.checkLevel(player))
								{
									html = html.replace("<?event_" + i + "?>", "<font color=\"99CC33\">" + pvp.getName() + ": " + pvp.getMinLevel() + "-" + pvp.getMaxLevel() + "</font>");
									if(pvp.isRegistrationOver())
										html = html.replace("<?bp_" + i + "?>", "<font color=\"CC3333\">Not Started</font>");
									else
										html = html.replace("<?bp_" + i + "?>", "<button value=\"Register\" action=\"bypass _bbsevents:reg:" + pvp.getId() + "\" width=50 height=16 back=\"L2UI_CT1.ListCTRL_DF_Title_Down\" fore=\"L2UI_CT1.ListCTRL_DF_Title\">");
									i++;
								}
							}
						}
					}

					// Подмена неактивных полей серверных ивентов
					for(int i1 = i; i1 <= 10; i1++)
					{
						html = html.replace("<?event_" + i1 + "?>", "...");
						html = html.replace("<?bp_" + i1 + "?>", "..");
					}

					int a = 1;

					for(Event fun : EventHolder.getInstance().getEvents().values())
					{
						if(fun.getType().equals(EventType.FUN_EVENT) && fun.isInProgress())
						{
							html = html.replace("<?my_event_" + a + "?>", "<font color=\"6699CC\">" + fun.getName() + "</font>");
							a++;
						}
					}

					for(Event my : player.getEvents())
					{
						if(my.getType().equals(EventType.PVP_EVENT))
						{
							// Сраный костыль, но ивенты 1х1 пвп и гвг не имеют своего имени!
							String my_name = "";
							if(my instanceof CustomPvPBattleEvent)
								my_name = "PvP Olympiad Stadiums Battle";
							else if(my instanceof CustomGvGBattleEvent)
								my_name = "GvG Coliseum Battle";
							else if(my instanceof PlayerVsPlayerDuelEvent)
								my_name = "Player vs Player Duel";
							else if(my instanceof PartyVsPartyDuelEvent)
								my_name = "Party vs Party Duel";
							else
								my_name = my.getName();

							boolean unk = false;
							if(my_name.isEmpty())
							{
								my_name = "UNKNOWN";
								unk = true;
							}

							html = html.replace("<?my_event_" + a + "?>", "<font color=\"" + (unk ? "FF0000" : "6699CC") + "\">" + my_name + "</font>");
							a++;
						}
					}

					// Подмена неактивных полей ивентов игрока
					for(int i2 = a; i2 <= 10; i2++)
					{
						html = html.replace("<?my_event_" + i2 + "?>", "...");
					}
				}

				ShowBoard.separateAndSend(html, player);
				return;
			}
			else if(name.equals("info"))
			{
				onBypassCommand(player, "_bbsevents:page:index");
				HtmlMessage window = new HtmlMessage(5).setFile("scripts/window/" + param + ".htm");
				player.sendPacket(window);
				return;
			}
			else
			{
				if(!Util.isNumber(param))
					return;

				int id = Integer.parseInt(param);

				Event event = EventHolder.getInstance().getEvent(EventType.PVP_EVENT, id);

				if(event != null && event instanceof TeamEvent)
				{
					TeamEvent pvp = (TeamEvent) event;

					if(name.equals("reg"))
						pvp.registerPlayer(player);
				}
				else
					player.sendMessage("Can't find event!");
			}

			Util.communityNextPage(player, "_bbsevents:page:index");
			return;
		}
	}

	@Override
	public void onWriteCommand(Player player, String bypass, String arg1, String arg2, String arg3, String arg4, String arg5)
	{}
}
