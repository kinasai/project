package handler.admincommands;

import com.l2cccp.gameserver.handler.admincommands.AdminCommandHandler;
import com.l2cccp.gameserver.handler.admincommands.IAdminCommandHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;

/**
 * @author VISTALL
 * @date 22:57/08.04.2011
 */
public abstract class ScriptAdminCommand implements IAdminCommandHandler, OnInitScriptListener
{
	@Override
	public void onInit()
	{
		AdminCommandHandler.getInstance().registerAdminCommandHandler(this);
	}
}
