package handler.admincommands;

import com.l2cccp.gameserver.data.xml.holder.EventHolder;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.Event;
import com.l2cccp.gameserver.model.entity.events.EventType;
import com.l2cccp.gameserver.model.entity.events.impl.FunEvent;
import com.l2cccp.gameserver.model.entity.events.impl.OfficialEvent;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExBR_BroadcastEventState;

/**
 * @author VISTALL
 * @date 18:45/07.06.2011
 */
public class AdminEvent extends ScriptAdminCommand
{
	enum Commands
	{
		admin_list_events,
		admin_start_event,
		admin_stop_event,
		admin_off
	}

	@Override
	public boolean useAdminCommand(Enum<?> comm, String[] wordList, String fullString, Player activeChar)
	{
		Commands c = (Commands) comm;
		switch(c)
		{
			case admin_start_event:
			case admin_stop_event:
				if(wordList.length < 2)
				{
					activeChar.sendMessage("USAGE://" + wordList[0] + " ID");
					return false;
				}
				try
				{
					int id = Integer.parseInt(wordList[1]);

					FunEvent event = EventHolder.getInstance().getEvent(EventType.FUN_EVENT, id);
					if(comm == Commands.admin_start_event)
						event.startEvent();
					else
						event.forceStopEvent();
				}
				catch(NumberFormatException e)
				{
					activeChar.sendMessage("USAGE://" + wordList[0] + " ID");
					return false;
				}
				break;
			case admin_off:
				if(wordList.length < 2)
				{
					activeChar.sendMessage("USAGE://" + wordList[0] + " ID");
					return false;
				}
				try
				{
					int id = Integer.parseInt(wordList[1]);

					OfficialEvent event = EventHolder.getInstance().getEvent(EventType.OFFICIAL_EVENT, id);
					if(event.isInProgress())
						event.stopEvent();
					else
						event.startEvent();
				}
				catch(NumberFormatException e)
				{
					activeChar.sendMessage("USAGE://" + wordList[0] + " ID");
					return false;
				}
				break;
			case admin_list_events:
				GameObject object = activeChar.getTarget();
				if(object == null)
					activeChar.sendPacket(SystemMsg.INVALID_TARGET);
				else
				{
					for(Event e : object.getEvents())
						activeChar.sendMessage("- " + e.toString());
				}
				break;
		}
		return false;
	}

	@Override
	public Enum<?>[] getAdminCommandEnum()
	{
		return Commands.values();
	}
}
