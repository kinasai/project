package handler.usercommands;

import com.l2cccp.gameserver.handler.usercommands.IUserCommandHandler;
import com.l2cccp.gameserver.handler.usercommands.UserCommandHandler;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;

/**
 * @author VISTALL
 * @date 16:53/24.06.2011
 */
public abstract class ScriptUserCommand implements IUserCommandHandler, OnInitScriptListener
{
	@Override
	public void onInit()
	{
		UserCommandHandler.getInstance().registerUserCommandHandler(this);
	}
}