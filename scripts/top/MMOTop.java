package top;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Calendar;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.top.Tops;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class MMOTop extends Top
{
	private static final Logger _log = LoggerFactory.getLogger(MMOTop.class);

	@Override
	public void onInit()
	{
		if(!Config.MMOTOPENABLE)
			return;

		super.onInit();
	}

	@Override
	protected void getPage()
	{
		int simple_count = 0;
		int sms_count = 0;

		try
		{
			URL url = new URL(getLink());
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "utf8"));
			for(String line = in.readLine(); line != null; line = in.readLine())
			{
				Calendar calendar = Calendar.getInstance();
				try
				{
					StringTokenizer array = new StringTokenizer(line, "\t");
					array.nextToken(); // Vote ID

					StringTokenizer date = new StringTokenizer(array.nextToken(), ". :");
					final int day = Integer.parseInt(date.nextToken());
					final int month = Integer.parseInt(date.nextToken());
					final int year = Integer.parseInt(date.nextToken());
					final int hour = Integer.parseInt(date.nextToken());
					final int minute = Integer.parseInt(date.nextToken());
					final int second = Integer.parseInt(date.nextToken());
					calendar.set(Calendar.YEAR, year);
					calendar.set(Calendar.MONTH, month);
					calendar.set(Calendar.DAY_OF_MONTH, day);
					calendar.set(Calendar.HOUR_OF_DAY, hour);
					calendar.set(Calendar.MINUTE, minute);
					calendar.set(Calendar.SECOND, second);
					calendar.set(Calendar.MILLISECOND, 0);

					array.nextToken(); // IP

					final String nick = array.nextToken();
					final int sms = Integer.parseInt(array.nextToken()) - 1;

					if(save(nick, calendar.getTimeInMillis(), sms, getName()) && setReward(nick, sms))
					{
						simple_count++;
						sms_count += sms;
					}
				}
				catch(NoSuchElementException nsee)
				{
					_log.error(getName().getLink() + ": Error -> " + nsee, nsee);
					continue;
				}
			}
		}
		catch(Exception e)
		{
			_log.error(getName().getLink() + ": Exception -> " + e);
		}

		if(simple_count > 0 || sms_count > 0)
			_log.info(getName().getLink() + ": Loaded " + simple_count + " simple and " + sms_count + " sms votes!");
	}

	@Override
	protected String[] getReward(boolean sms)
	{
		return sms ? Config.MMOTOP_REWARD_SMS_ARRAY : Config.MMOTOP_REWARD_WEB_ARRAY;
	}

	@Override
	protected String getLink()
	{
		return Config.MMOTOPURL;
	}

	@Override
	protected int getDelay()
	{
		return Config.MMOTOPREFRESHTIME * 60000;
	}

	@Override
	protected Tops getName()
	{
		return Tops.MMOTOPRU;
	}
}