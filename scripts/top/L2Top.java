package top;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Calendar;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.top.Tops;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class L2Top extends Top
{
	private static final Logger _log = LoggerFactory.getLogger(L2Top.class);

	@Override
	public void onInit()
	{
		if(!Config.L2TOPENABLE)
			return;

		super.onInit();
	}

	@Override
	protected void getPage()
	{
		int count = 0;

		try
		{
			URL url = new URL(getLink());
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "cp1251"));
			for(String line = in.readLine(); line != null; line = in.readLine())
			{
				Calendar calendar = Calendar.getInstance();

				int year = calendar.get(Calendar.YEAR);
				if(line.startsWith(String.valueOf(year)))
				{
					try
					{
						StringTokenizer st = new StringTokenizer(line, "\t ");
						String[] date = st.nextToken().split("-");
						String[] time = st.nextToken().split(":");
						String nick = st.nextToken();
						if(!Config.L2_TOP_SERVER_PREFIX.isEmpty())
							nick = nick.replace(Config.L2_TOP_SERVER_PREFIX + "-", "");

						int sms = 0;
						if(sms())
							sms = Integer.parseInt(st.nextToken().replace("x", ""));
						calendar.set(Calendar.YEAR, Integer.parseInt(date[0]));
						calendar.set(Calendar.MONTH, Integer.parseInt(date[1]));
						calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[2]));
						calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
						calendar.set(Calendar.MINUTE, Integer.parseInt(time[1]));
						calendar.set(Calendar.SECOND, Integer.parseInt(time[2]));
						calendar.set(Calendar.MILLISECOND, 0);
						if(save(nick, calendar.getTimeInMillis(), sms, getName()) && setReward(nick, sms))
							count++;
					}
					catch(NoSuchElementException nsee)
					{
						_log.error(getName().getLink() + ": Error -> " + nsee);
						continue;
					}
				}
			}
		}
		catch(Exception e)
		{
			_log.error(getName().getLink() + ": Exception -> " + e);
		}

		if(count > 0)
			_log.info(getName().getLink() + ": Loaded " + count + " " + (sms() ? "sms" : "simple") + " votes!");
	}

	@Override
	protected String[] getReward(boolean sms)
	{
		return sms ? Config.L2TOP_REWARD_SMS_ARRAY : Config.L2TOP_REWARD_WEB_ARRAY;
	}

	protected boolean sms()
	{
		return false;
	}

	@Override
	protected String getLink()
	{
		return Config.L2TOPWEB;
	}

	@Override
	protected int getDelay()
	{
		return Config.L2TOPREFRESHTIME * 60000;
	}

	@Override
	protected Tops getName()
	{
		return Tops.L2TOPRU;
	}
}