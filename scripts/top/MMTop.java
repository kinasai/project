package top;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.top.Tops;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class MMTop extends Top
{
	private static final Logger _log = LoggerFactory.getLogger(MMTop.class);

	@Override
	public void onInit()
	{
		if(!Config.MMTOPENABLE)
			return;

		super.onInit();
	}

	@Override
	protected void getPage()
	{
		int simple_count = 0;
		int sms_count = 0;

		try
		{
			URL url = new URL(getLink());
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.addRequestProperty("User-Agent", "Mozilla/4.76");

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf8"));
			for(String line = in.readLine(); line != null; line = in.readLine())
			{
				Calendar calendar = Calendar.getInstance();
				try
				{
					StringTokenizer votes = new StringTokenizer(line, "[]");
					final String vote = votes.nextToken();
					StringTokenizer data = new StringTokenizer(vote, "/ :");
					data.nextToken(); // Vote ID
					final int month = Integer.parseInt(data.nextToken());
					final int day = Integer.parseInt(data.nextToken());
					final int year = Integer.parseInt(data.nextToken());
					final int hour = Integer.parseInt(data.nextToken());
					final int minute = Integer.parseInt(data.nextToken());
					final int second = Integer.parseInt(data.nextToken());
					final String pm = data.nextToken(); // PM or AM
					calendar.set(Calendar.YEAR, year);
					calendar.set(Calendar.MONTH, month - 1);
					calendar.set(Calendar.DAY_OF_MONTH, day);
					calendar.set(Calendar.HOUR, hour);
					calendar.set(Calendar.MINUTE, minute);
					calendar.set(Calendar.SECOND, second);
					calendar.set(Calendar.MILLISECOND, 0);
					calendar.set(Calendar.AM_PM, pm.equalsIgnoreCase("PM") ? Calendar.PM : Calendar.AM);

					final String nick = data.nextToken();
					final int sms = 0; // TODO: Integer.parseInt(data.nextToken()) - 1; 1-Simple Vote, 2-Sms Vote

					if(save(nick, calendar.getTimeInMillis(), sms, getName()) && setReward(nick, sms))
					{
						simple_count++;
						sms_count += sms;
					}
				}
				catch(NoSuchElementException nsee)
				{
					_log.error(getName().getLink() + ": Error -> " + nsee, nsee);
					continue;
				}
			}
		}
		catch(Exception e)
		{
			_log.error(getName().getLink() + ": Exception -> " + e);
		}

		if(simple_count > 0 || sms_count > 0)
			_log.info(getName().getLink() + ": Loaded " + simple_count + " simple and " + sms_count + " sms votes!");
	}

	@Override
	protected String[] getReward(boolean sms)
	{
		return sms ? Config.MMTOP_REWARD_SMS_ARRAY : Config.MMTOP_REWARD_WEB_ARRAY;
	}

	@Override
	protected String getLink()
	{
		return Config.MMTOPURL;
	}

	@Override
	protected int getDelay()
	{
		return Config.MMTOPREFRESHTIME * 60000;
	}

	@Override
	protected Tops getName()
	{
		return Tops.MMTOPSU;
	}
}