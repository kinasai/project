package top;

import com.l2cccp.gameserver.Config;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class L2TopSms extends L2Top
{
	@Override
	protected String getLink()
	{
		return Config.L2TOPSMS;
	}

	@Override
	protected boolean sms()
	{
		return true;
	}
}