package top;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.top.Tops;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public abstract class Top implements OnInitScriptListener
{
	private static final Logger _log = LoggerFactory.getLogger(Top.class);
	private static final String SELECT_CHARACTER = "SELECT * FROM characters WHERE char_name=? LIMIT 1";
	private static final String SELECT_VOTES = "SELECT * FROM character_votes WHERE type=? AND char_name=? AND time=? LIMIT 1";
	private static final String INSERT_VOTES = "INSERT INTO character_votes (type, char_name, time, sms) VALUES (?, ?, ?, ?)";

	protected boolean save(String nick, long time, int sms, Tops top)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement(SELECT_CHARACTER);
			statement.setString(1, nick);
			rset = statement.executeQuery();
			if(rset.next())
			{
				DbUtils.closeQuietly(statement, rset);
				statement = con.prepareStatement(SELECT_VOTES);
				statement.setString(1, top.getLink());
				statement.setString(2, nick);
				statement.setLong(3, time);
				rset = statement.executeQuery();
				if(!rset.next())
				{
					DbUtils.closeQuietly(statement, rset);
					statement = con.prepareStatement(INSERT_VOTES);
					statement.setString(1, top.getLink());
					statement.setString(2, nick);
					statement.setLong(3, time);
					statement.setInt(4, sms > 1 ? 1 : 0);
					statement.execute();
					return true;
				}
			}
		}
		catch(SQLException e)
		{
			_log.error(getName().getLink() + ": Error saveDatabase() -> " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return false;
	}

	protected class Parse extends RunnableImpl
	{
		@Override
		public void runImpl() throws Exception
		{
			getPage();
		}
	}

	protected boolean setReward(String nick, int sms)
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT obj_Id FROM characters WHERE char_name=? LIMIT 1");
			statement.setString(1, nick);
			rset = statement.executeQuery();
			if(rset.next())
			{
				int obj_Id = rset.getInt("obj_Id");
				DbUtils.closeQuietly(statement, rset);
				String[] items = getReward(sms == 1);

				for(int i = 0; i < items.length; i++)
				{
					StringTokenizer st = new StringTokenizer(items[i], "{},()[]");
					int item_id = Integer.parseInt(st.nextToken());
					long item_count_min = Integer.parseInt(st.nextToken());
					long item_count_max = Integer.parseInt(st.nextToken());
					double item_chance = Double.parseDouble(st.nextToken());
					if(Rnd.chance(item_chance))
					{
						statement = con.prepareStatement("INSERT INTO items_delayed (`owner_id`, `item_id`, `count`, `payment_status`, `description`) VALUES (?, ?, ?, 0, ?)");
						statement.setInt(1, obj_Id);
						statement.setInt(2, item_id);
						statement.setLong(3, Rnd.get(item_count_min, item_count_max));
						statement.setString(4, getName().getLink());
						statement.execute();
						DbUtils.closeQuietly(statement);
					}
				}
				return true;
			}
			else
				_log.info(getName().getLink() + ": Unknown character -> " + nick);
		}
		catch(SQLException e)
		{
			_log.error(getName().getLink() + ": Error setReward() -> " + e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return false;
	}

	protected abstract void getPage();

	protected abstract String getLink();

	protected abstract int getDelay();

	protected abstract Tops getName();

	protected abstract String[] getReward(boolean sms);

	@Override
	public void onInit()
	{
		_log.info(getName().getLink() + ": Loaded services");
		ThreadPoolManager.getInstance().scheduleAtFixedRate(new Parse(), getDelay(), getDelay());
	}
}