package services;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.l2cccp.commons.lang.ArrayUtils;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.CharacterAccessDAO;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.CharSelectInfo;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.GameClient;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Util;

public class AuthPassword
{
	private static final Pattern digits = Pattern.compile("[0-9]{6,8}");

	@Bypass("auth.password.open")
	public void open(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SECOND_AUTH_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/PIN/set.htm");
		player.sendPacket(html);
	}

	@Bypass("auth.password.set")
	public void set(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SECOND_AUTH_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(arg.length < 2)
			return;

		GameClient client = player.getNetConnection();
		CharSelectInfo csi = ArrayUtils.valid(client.getCharacters(), client.getSelectedIndex());
		if(csi != null)
		{
			String _password = arg[0];
			String _password1 = arg[1];

			if(_password.isEmpty() || _password1.isEmpty())
			{
				player.sendMessage("Incorrect PIN!");
				return;
			}

			if(!_password.equals(_password1))
			{
				player.sendMessage("PIN must be equals!");
				return;
			}

			if(!validatePassword(_password))
			{
				player.sendMessage("PIN must be most strong!");
				return;
			}

			csi.setPasswordEnable(true);
			csi.setPassword(_password);
			CharacterAccessDAO.getInstance().update(csi.getObjectId(), _password);
			CharacterAccessDAO.getInstance().enable(csi.getObjectId(), 1);
			Util.communityNextPage(player, "_bbscabinet:show:security");
			player.sendMessage("For enter to game use PIN " + csi.getPassword() + "!");
		}
	}

	@Bypass("auth.password.enable")
	public void enable(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SECOND_AUTH_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(arg[0].isEmpty())
			return;

		boolean enable = Boolean.parseBoolean(arg[0]);
		GameClient client = player.getNetConnection();
		CharSelectInfo csi = ArrayUtils.valid(client.getCharacters(), client.getSelectedIndex());
		if(csi != null)
		{
			if(csi.getPassword() == null)
				return;

			CharacterAccessDAO.getInstance().enable(csi.getObjectId(), enable ? 1 : 0);

			player.sendMessage("PIN is turned " + (enable ? "on" : "off") + " from current character!");
			csi.setPasswordEnable(enable);
			Util.communityNextPage(player, "_bbscabinet:show:security");
		}
	}

	private boolean validatePassword(String password)
	{
		if(!Util.isNumber(password))
			return false;

		Matcher matcher = digits.matcher(password);
		if(!matcher.matches())
			return false;
		else
			return true;
	}
}