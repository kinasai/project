//package services;
//
//import handler.items.OpenBox;
//
//import com.l2cccp.gameserver.Config;
//import com.l2cccp.gameserver.handler.bypass.Bypass;
//import com.l2cccp.gameserver.model.Player;
//import com.l2cccp.gameserver.model.instances.NpcInstance;
//import com.l2cccp.gameserver.utils.Util;
//
///**
// * @author L2CCCP
// * @site http://l2cccp.com/
// */
//public class OpenBoxes
//{
//
//	@Bypass("box.key")
//	public void box(Player player, NpcInstance npc, String[] arg)
//	{
//		if(!arg[0].isEmpty() && Util.isNumber(arg[0]))
//		{
//			if(arg.length > 1 && !arg[1].isEmpty() && Util.isNumber(arg[1]))
//			{
//				final int box = Integer.parseInt(arg[0]);
//				if(isBox(box))
//				{
//					final int key = Integer.parseInt(arg[1]);
//					OpenBox.open(box, player, OpenBox.keys[key]);
//				}
//			}
//		}
//	}
//
//	private boolean isBox(int box)
//	{
//		for(int id : Config.BOXEX_LIST)
//		{
//			if(id == box)
//				return true;
//		}
//
//		return false;
//	}
//}