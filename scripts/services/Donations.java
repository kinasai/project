package services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.DonationHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.Element;
import com.l2cccp.gameserver.model.donatesystem.Attribution;
import com.l2cccp.gameserver.model.donatesystem.DonateItem;
import com.l2cccp.gameserver.model.donatesystem.Donation;
import com.l2cccp.gameserver.model.donatesystem.Enchant;
import com.l2cccp.gameserver.model.donatesystem.FoundList;
import com.l2cccp.gameserver.model.donatesystem.SimpleList;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.InventoryUpdate;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Util;

public class Donations
{
	private final static String[] _vars = new String[] { "FOUNDATION", "ENCHANT", "ATTRIBUTION" };

	@Bypass("donate.list")
	public void list(Player player, NpcInstance npc, String[] arg)
	{
		if(arg[0].isEmpty() || !Util.isNumber(arg[0]) || (arg.length > 1 && !arg[1].isEmpty() && !Util.isNumber(arg[1])))
			return;

		int id = Integer.parseInt(arg[0]);
		removeVars(player);

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Donate/index.htm");
		String template = HtmCache.getInstance().getHtml("scripts/services/Donate/template.htm", player);
		String block = "";
		String list = "";
		List<Donation> _donate = DonationHolder.getInstance().getGroup(id);

		final int perpage = 6;
		final int page = arg.length > 1 ? Integer.parseInt(arg[1]) : 1;
		int counter = 0;
		for(int i = (page - 1) * perpage; i < _donate.size(); i++)
		{
			Donation pack = _donate.get(i);
			block = template;
			block = block.replace("{bypass}", "bypass -h htmbypass_donate.open " + pack.getId());
			block = block.replace("{name}", pack.getName());
			block = block.replace("{icon}", pack.getIcon());

			SimpleList simple = pack.getSimple();
			block = block.replace("{cost}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, simple.getCount(), simple.getId())).toString(player));
			list += block;

			counter++;

			if(counter >= perpage)
				break;
		}

		double count = Math.ceil((double) _donate.size() / (double) perpage); // Используем округление для получения последней страницы с остатком!

		int inline = 1;
		String navigation = "";
		for(int i = 1; i <= count; i++)
		{
			if(i == page)
				navigation += "<td width=25 align=center valign=top><button value=\"[" + i + "]\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";
			else
				navigation += "<td width=25 align=center valign=top><button value=\"" + i + "\" action=\"bypass -h htmbypass_donate.list " + id + " " + i + "\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";

			if(inline % 7 == 0)
				navigation += "</tr><tr>";

			inline++;
		}

		if(inline == 2)
			navigation = "<td width=30 align=center valign=top>...</td>";

		html.replace("%list%", list);
		html.replace("%navigation%", navigation);

		player.sendPacket(html);
	}

	private void removeVars(Player player)
	{
		for(String var : _vars)
			player.deleteSessionVar(var);
		for(int i = 1; i <= 3; i++)
			player.deleteSessionVar("att_" + i);
	}

	@Bypass("donate.open")
	public void take(Player player, NpcInstance npc, String[] arg)
	{
		if(!Util.isNumber(arg[0]))
			return;

		int id = Integer.parseInt(arg[0]);
		Donation donate = DonationHolder.getInstance().getDonate(id);

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Donate/open.htm");
		String content = "";

		Map<Integer, Long> price = new HashMap<Integer, Long>();

		html.replace("%name%", donate.getName());
		html.replace("%icon%", donate.getIcon());
		html.replace("%id%", String.valueOf(donate.getId()));
		html.replace("%group%", String.valueOf(donate.getGroup()));

		SimpleList simple = donate.getSimple();
		html.replace("%cost%", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, simple.getCount(), simple.getId())).toString(player));
		price.put(simple.getId(), simple.getCount());

		if(donate.haveFound())
		{
			boolean is = isVar(player, _vars[0]);
			FoundList found = donate.getFound();
			String block = HtmCache.getInstance().getHtml("scripts/services/Donate/foundation.htm", player);;
			block = block.replace("{bypass}", "bypass -h htmbypass_donate.var " + _vars[0] + " " + (is ? 0 : 1) + " " + donate.getId());
			block = block.replace("{status}", is ? Active : NotActive);
			block = block.replace("{cost}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, found.getCount(), found.getId())).toString(player));
			block = block.replace("{action}", is ? "Cancel" : "Buy");

			if(is)
				updatePrice(price, found.getId(), found.getCount());

			content += block;
		}

		Enchant enchant = donate.getEnchant();
		if(enchant != null)
		{
			boolean is = isVar(player, _vars[1]);
			String block = HtmCache.getInstance().getHtml("scripts/services/Donate/enchant.htm", player);;
			block = block.replace("{bypass}", "bypass -h htmbypass_donate.var " + _vars[1] + " " + (is ? 0 : 1) + " " + donate.getId());
			block = block.replace("{status}", is ? Active : NotActive);
			block = block.replace("{ench}", "+" + enchant.getEnchant());
			block = block.replace("{cost}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, enchant.getCount(), enchant.getId())).toString(player));
			block = block.replace("{action}", is ? "Cancel" : "Buy");

			if(is)
				updatePrice(price, enchant.getId(), enchant.getCount());

			content += block;
		}

		Attribution att = donate.getAttribution();
		if(att != null && att.getSize() >= 1)
		{
			boolean is = isVar(player, _vars[2]);
			if(is && checkAttVars(player, att.getSize())) // Если был переход в раздел атт но не выбраны все атт то отменяем метки
			{
				is = false;
				player.deleteSessionVar(_vars[2]);
				var(player, npc, new String[] { _vars[2], "0" });
			}

			String block = HtmCache.getInstance().getHtml("scripts/services/Donate/attribute.htm", player);;
			block = block.replace("{bypass}", is ? ("bypass -h htmbypass_donate.var " + _vars[2] + " " + 0 + " " + donate.getId()) : ("bypass -h htmbypass_donate.attribute " + donate.getId()));
			block = block.replace("{status}", is ? Active : NotActive);
			block = block.replace("{cost}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, att.getCount(), att.getId())).toString(player));
			block = block.replace("{action}", is ? "Cancel" : "Buy");

			if(is)
				updatePrice(price, att.getId(), att.getCount());

			content += block;
		}

		String total = "";

		for(Entry<Integer, Long> map : price.entrySet())
			total += Util.formatPay(player, map.getValue(), map.getKey()) + "<br1>";

		html.replace("%content%", content);
		html.replace("%total%", total);

		player.sendPacket(html);
	}

	private boolean checkAttVars(Player player, int size)
	{
		int count = 0;
		for(int i = 1; i <= 3; i++)
		{
			int var = player.getSessionVarI("att_" + i, -1);
			if(var != -1)
				count++;
		}

		return count != size;
	}

	private void updatePrice(Map<Integer, Long> price, int id, long count)
	{
		if(price.containsKey(id))
			price.put(id, count + price.get(id));
		else
			price.put(id, count);
	}

	private static final String Active = "<font color=669900>You buy it!</font>";
	private static final String NotActive = "<font color=FF0000>Not buy!</font>";

	@Bypass("donate.var")
	public void var(Player player, NpcInstance npc, String[] arg)
	{
		if(arg.length < 3)
			return;

		if(!Util.isNumber(arg[1]) || !Util.isNumber(arg[2]))
			return;

		int action = Integer.parseInt(arg[1]);
		String var = arg[0];
		player.addSessionVar(var, action);

		if(action == 0)
		{
			player.deleteSessionVar(var);
			if(var.equals(_vars[2]))
			{
				for(int i = 1; i <= 3; i++)
					player.deleteSessionVar("att_" + i);
			}
		}

		take(player, npc, new String[] { arg[2] });
	}

	@Bypass("donate.attribute")
	public void attribute(Player player, NpcInstance npc, String[] arg)
	{
		if(arg.length < 1)
			return;

		if(!Util.isNumber(arg[0]))
			return;

		int id = Integer.parseInt(arg[0]);
		Donation donate = DonationHolder.getInstance().getDonate(id);
		if(donate == null)
			return;

		Attribution atribute = donate.getAttribution();

		if(atribute == null)
			return;

		if(atribute.getSize() < 1)
		{
			take(player, npc, arg);
			return;
		}

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Donate/attribute_choice.htm");
		html.replace("%name%", donate.getName());
		html.replace("%icon%", donate.getIcon());
		html.replace("%bypass%", "bypass -h htmbypass_donate.open " + donate.getId());
		html.replace("%value%", String.valueOf(atribute.getValue()));
		html.replace("%size%", String.valueOf(atribute.getSize()));
		html.replace("%id%", String.valueOf(donate.getId()));

		int att_1 = player.getSessionVarI("att_1", -1);
		int att_2 = player.getSessionVarI("att_2", -1);
		int att_3 = player.getSessionVarI("att_3", -1);
		html.replace("%att_1%", atribute.getSize() >= 1 ? (att_1 == -1 ? "..." : new CustomMessage("common.element." + att_1 + "").toString(player)) : "<font color=FF0000>Slot is block</font>");
		html.replace("%att_2%", atribute.getSize() >= 2 ? (att_2 == -1 ? "..." : new CustomMessage("common.element." + att_2 + "").toString(player)) : "<font color=FF0000>Slot is block</font>");
		html.replace("%att_3%", atribute.getSize() == 3 ? (att_3 == -1 ? "..." : new CustomMessage("common.element." + att_3 + "").toString(player)) : "<font color=FF0000>Slot is block</font>");

		build(player, html, donate, att_1, att_2, att_3);
		player.sendPacket(html);
	}

	private String button(int att, int id)
	{
		return "<button action=\"bypass -h htmbypass_donate.attribute.put " + id + " " + att + "\" width=34 height=34 back=\"L2UI_CT1.ItemWindow_DF_Frame_Down\" fore=\"L2UI_CT1.ItemWindow_DF_Frame\"/>";
	}

	@Bypass("donate.attribute.put")
	public void put_att(Player player, NpcInstance npc, String[] arg)
	{
		if(arg.length < 2)
			return;

		if(!Util.isNumber(arg[0]) || !Util.isNumber(arg[1]))
			return;

		int att = Integer.parseInt(arg[1]);
		if(player.getSessionVarI("att_1", -1) == -1)
			player.addSessionVar("att_1", att);
		else if(player.getSessionVarI("att_2", -1) == -1)
			player.addSessionVar("att_2", att);
		else if(player.getSessionVarI("att_3", -1) == -1)
			player.addSessionVar("att_3", att);

		player.addSessionVar(_vars[2], 1);

		attribute(player, npc, arg);
	}

	@Bypass("donate.attribute.clear")
	public void clear_att(Player player, NpcInstance npc, String[] arg)
	{
		if(arg.length < 1)
			return;

		if(!Util.isNumber(arg[0]))
			return;

		for(int i = 1; i <= 3; i++)
			player.deleteSessionVar("att_" + i);

		attribute(player, npc, arg);
	}

	private HtmlMessage build(Player player, HtmlMessage html, Donation donate, int att_1, int att_2, int att_3)
	{
		String slotclose = "<img src=\"L2UI_CT1.ItemWindow_DF_SlotBox_Disable\" width=\"32\" height=\"32\">";

		int id = donate.getId();
		int size = donate.getAttribution().getSize();

		boolean block = false;
		if(size == 1 && (att_1 != -1 || att_2 != -1 || att_3 != -1))
			block = true;
		else if(size == 2 && ((att_1 != -1 || att_2 != -1) && (att_1 != -1 || att_3 != -1) && (att_2 != -1 || att_3 != -1)))
			block = true;
		else if(size == 3 && (att_1 != -1 && att_2 != -1 && att_3 != -1))
			block = true;

		boolean one = block(player, 0, 1) || block;
		String fire = one ? slotclose : button(0, id);
		String water = one ? slotclose : button(1, id);

		boolean two = block(player, 2, 3) || block;
		String wind = two ? slotclose : button(2, id);
		String earth = two ? slotclose : button(3, id);

		boolean three = block(player, 4, 5) || block;
		String holy = three ? slotclose : button(4, id);
		String unholy = three ? slotclose : button(5, id);

		html.replace("%fire%", fire);
		html.replace("%water%", water);
		html.replace("%wind%", wind);
		html.replace("%earth%", earth);
		html.replace("%holy%", holy);
		html.replace("%unholy%", unholy);

		return html;
	}

	private boolean block(Player player, int id, int id2)
	{
		for(int i = 1; i <= 3; i++)
		{
			int var = player.getSessionVarI("att_" + i, -1);
			if(var == id || var == id2)
				return true;
		}

		return false;
	}

	private boolean isVar(Player player, String var)
	{
		return player.getSessionVarI(var, 0) != 0;
	}

	@Bypass("donate.buy")
	public void buy(Player player, NpcInstance npc, String[] arg)
	{
		if(arg.length < 1)
			return;

		if(!Util.isNumber(arg[0]))
			return;

		int id = Integer.parseInt(arg[0]);
		Donation donate = DonationHolder.getInstance().getDonate(id);
		if(donate == null)
			return;

		Map<Integer, Long> price = new HashMap<Integer, Long>();
		SimpleList simple = donate.getSimple();

		price.put(simple.getId(), simple.getCount());

		FoundList foundation = donate.getFound();
		boolean found_list = donate.haveFound() && foundation != null && player.getSessionVarI(_vars[0], -1) != -1;
		if(found_list)
			updatePrice(price, foundation.getId(), foundation.getCount());

		Enchant enchant = donate.getEnchant();
		boolean enchanted = enchant != null && player.getSessionVarI(_vars[1], -1) != -1;
		if(enchanted)
			updatePrice(price, enchant.getId(), enchant.getCount());

		Attribution att = donate.getAttribution();
		boolean attribution = att != null && player.getSessionVarI(_vars[2], -1) != -1;
		if(attribution)
			updatePrice(price, att.getId(), att.getCount());

		for(Entry<Integer, Long> map : price.entrySet())
		{
			int _id = map.getKey();
			long _count = map.getValue();

			if(ItemFunctions.getItemCount(player, map.getKey()) < map.getValue())
			{
				player.sendMessage(new CustomMessage("util.enoughItemCount").addString(Util.formatPay(player, _count, _id)));
				take(player, npc, arg);
				return;
			}
		}

		for(Entry<Integer, Long> map : price.entrySet())
		{
			int _id = map.getKey();
			long _count = map.getValue();
			player.getInventory().removeItemByItemId(_id, _count);
		}

		for(DonateItem _donate : (found_list ? foundation.getList() : simple.getList()))
		{
			ItemInstance item = player.getInventory().addItem(_donate.getId(), _donate.getCount());

			int enchant_level = 0;
			if(enchanted)
				enchant_level = enchant.getEnchant();
			else if(_donate.getEnchant() > 0)
				enchant_level = _donate.getEnchant();

			if(enchant_level > 0 && item.canBeEnchanted(false))
				item.setEnchantLevel(enchant_level);

			if((item.isArmor() || item.isWeapon()) && attribution) // Add all elements
			{
				for(int i = 1; i <= att.getSize(); i++)
				{
					int element_id = player.getSessionVarI("att_" + i, -1);
					if(element_id != -1)
					{
						Element element = Element.getElementById(element_id);

						if(item.isArmor()) // If is armor need reverse element Water to Fire and etc..
							element = Element.getReverseElement(element);
						item.setAttributeElement(element, att.getValue());
					}
				}
			}

			if(item.isEquipable() && ItemFunctions.checkIfCanEquip(player, item) == null)
				player.getInventory().equipItem(item);

			player.sendPacket(new InventoryUpdate().addModifiedItem(item));
			player.broadcastCharInfo();
		}

		removeVars(player);
		player.sendMessage("You buy: " + donate.getName());
	}
}