package services;

import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.data.xml.holder.MultiSellHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.residence.Castle;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.Inventory;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.templates.multisell.MultiSellEntry;
import com.l2cccp.gameserver.templates.multisell.MultiSellIngredient;
import com.l2cccp.gameserver.templates.multisell.MultiSellListContainer;

public class Pushkin
{
	@Bypass("services.Pushkin:doCrystallize")
	public void doCrystallize(Player player, NpcInstance npc, String[] arg)
	{
		NpcInstance merchant = npc;
		Castle castle = merchant != null ? merchant.getCastle(player) : null;

		MultiSellListContainer list = new MultiSellListContainer();
		list.setShowAll(false);
		list.setKeepEnchant(true);
		list.setNoTax(false);
		int entry = 0;
		final Inventory inv = player.getInventory();
		for(final ItemInstance itm : inv.getItems())
			if(itm.canBeCrystallized(player))
			{
				final ItemTemplate crystal = ItemHolder.getInstance().getTemplate(itm.getTemplate().getCrystalType().cry);
				final int crystalCount = itm.getTemplate().getCrystalCount(itm.getEnchantLevel(), false);
				MultiSellEntry possibleEntry = new MultiSellEntry(++entry, crystal.getItemId(), crystalCount, 0);
				possibleEntry.addIngredient(new MultiSellIngredient(itm.getItemId(), 1, itm.getEnchantLevel()));
				possibleEntry.addIngredient(new MultiSellIngredient(ItemTemplate.ITEM_ID_ADENA, Math.round(crystalCount * crystal.getReferencePrice() * 0.05), 0));
				list.addEntry(possibleEntry);
			}

		MultiSellHolder.getInstance().SeparateAndSend(list, player, merchant != null ? merchant.getObjectId() : -1, castle == null ? 0. : castle.getTaxRate());
	}
}