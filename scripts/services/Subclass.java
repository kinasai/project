package services;

import handler.bbs.Career;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.CharacterServitorDAO;
import com.l2cccp.gameserver.dao.SummonDAO;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.model.SubClass;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.base.ClassId;
import com.l2cccp.gameserver.model.base.ClassType;
import com.l2cccp.gameserver.model.base.PlayerClass;
import com.l2cccp.gameserver.model.base.Race;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.instances.PetInstance;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.utils.Util;

public class Subclass
{
	@Bypass("services.Subclass:page")
	public void page(Player player, NpcInstance npc, String[] arg)
	{
		String page = arg[0];
		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Subclass/" + page + ".htm");

		if(page.equals("choice_race"))
		{
			if(arg.length == 2)
			{
				html.replace("%type%", arg[1]);
				html.replace("%price%", Util.formatPay(player, Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[0], (int) Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[1]));
			}
		}
		else if(page.equals("choice"))
			html.replace("%price%", Util.formatPay(player, Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[0], (int) Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[1]));
		else if(page.equals("choice_cancel"))
		{
			if(arg.length == 2)
			{
				html.replace("%id%", arg[1]);
				html.replace("%price%", Util.formatPay(player, Config.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[0], (int) Config.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[1]));
			}
		}
		else if(page.equals("choice_cancel_race"))
		{
			if(arg.length == 3)
			{
				html.replace("%id%", arg[1]);
				html.replace("%type%", arg[2]);
				html.replace("%price%", Util.formatPay(player, Config.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[0], (int) Config.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[1]));
			}
		}

		player.sendPacket(html);
	}

	@Bypass("services.Subclass:add")
	public void add(Player player, NpcInstance npc, String[] arg)
	{
		if(arg.length < 2)
			return;

		if(!Config.BBS_CLASS_MASTER_ADD_SUB_CLASS)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(!checkCondition(player))
			return;

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Subclass/add.htm");

		Set<PlayerClass> sub = getAvailableSubClasses(player, true);
		String content = "";
		String type = arg[0];
		String race = arg[1];
		if(sub != null && !sub.isEmpty())
		{
			String template = HtmCache.getInstance().getHtml("scripts/services/Subclass/template_add.htm", player);
			String block = null;
			for(PlayerClass SUB : sub)
			{
				if(!check(type, SUB))
					continue;
				else if(race.equals("human") && !SUB.isOfRace(Race.human))
					continue;
				else if(race.equals("darkelf") && (!SUB.isOfRace(Race.darkelf) || player.getRace() == Race.elf))
					continue;
				else if(race.equals("dwarf") && !SUB.isOfRace(Race.dwarf))
					continue;
				else if(race.equals("elf") && (!SUB.isOfRace(Race.elf) || player.getRace() == Race.darkelf))
					continue;
				else if(race.equals("kamael") && (!SUB.isOfRace(Race.kamael) || player.getRace() != Race.kamael))
					continue;
				else if(race.equals("orc") && !SUB.isOfRace(Race.orc))
					continue;

				block = template;
				block = block.replace("{icon}", Util.getRaceIcon(SUB));
				block = block.replace("{name}", format(SUB));
				block = block.replace("{link}", "bypass -h htmbypass_services.Subclass:do_add " + SUB.ordinal());
				block = block.replace("{race}", Util.getRaceName(player, SUB));
				content += block;
			}
		}
		else
		{
			player.sendMessage(new CustomMessage("services.SubClass.add.empty"));
			return;
		}

		if(content.equals(""))
			html = new HtmlMessage(5).setFile("scripts/services/Subclass/empty.htm");
		else
			html.replace("%content%", content);

		player.sendPacket(html);
	}

	private boolean check(String type, PlayerClass sub)
	{
		if(type.equals("Fighter") && !sub.isOfType(ClassType.Fighter))
			return false;
		else if(type.equals("Mystic") && !sub.isOfType(ClassType.Mystic))
			return false;
		else if(type.equals("Priest") && !sub.isOfType(ClassType.Priest))
			return false;
		else
			return true;
	}

	@Bypass("services.Subclass:do_add")
	public void do_add(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.BBS_CLASS_MASTER_ADD_SUB_CLASS)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(!checkCondition(player))
			return;

		int id = Integer.parseInt(arg[0]);
		Map<Integer, SubClass> list = player.getSubClasses();

		if(player.getLevel() < Config.ALT_GAME_LEVEL_TO_GET_SUBCLASS)
		{
			player.sendMessage(new CustomMessage("l2p.gameserver.model.instances.L2VillageMasterInstance.NoSubBeforeLevel").addNumber(Config.ALT_GAME_LEVEL_TO_GET_SUBCLASS));
			return;
		}

		if(!list.isEmpty())
		{
			for(SubClass subClass : list.values())
			{
				if(subClass.getLevel() < Config.ALT_GAME_LEVEL_TO_GET_SUBCLASS)
				{
					player.sendMessage(new CustomMessage("l2p.gameserver.model.instances.L2VillageMasterInstance.NoSubBeforeLevel").addNumber(Config.ALT_GAME_LEVEL_TO_GET_SUBCLASS));
					return;
				}
			}
		}

		if(Util.getPay(player, (int) Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[1], Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[0], true))
		{
			if(!player.addSubClass(id, true, 0))
			{
				player.sendMessage(new CustomMessage("services.SubClass.add.haveall"));
				player.getInventory().addItem((int) Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[1], Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[0]);
			}
			else
			{
				player.sendMessage(new CustomMessage("services.SubClass.add.done").addString(Util.className(player, id)));
				Career.getInstance().showClassPage(player);
			}
		}
	}

	@Bypass("services.Subclass:change")
	public void change(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.BBS_CLASS_MASTER_CHANGE_SUB_CLASS)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(!checkCondition(player))
			return;

		HtmlMessage html;

		String content = "";

		final int base = player.getBaseClassId();
		Map<Integer, SubClass> list = player.getSubClasses();

		if(list.size() < 2)
		{
			html = new HtmlMessage(5).setFile("scripts/services/Subclass/change_empty.htm");
			html.replace("%price%", Util.formatPay(player, Config.BBS_CLASS_MASTER_SUB_CHANGE_PRICE_COUNT[0], (int) Config.BBS_CLASS_MASTER_SUB_CHANGE_PRICE_COUNT[1]));
		}
		else
		{
			html = new HtmlMessage(5).setFile("scripts/services/Subclass/change.htm");
			String template = HtmCache.getInstance().getHtml("scripts/services/Subclass/template_change.htm", player);
			String block = null;

			if(base == player.getActiveClassId())
			{
				block = template;
				block = block.replace("{icon}", "icon.etc_royal_membership_i00");
				block = block.replace("{name}", Util.className(player, base));
				block = block.replace("{link}", "");
				block = block.replace("{type}", new CustomMessage("services.SubClass.activebase").toString(player));
				content += block;
			}
			else
			{
				block = template;
				block = block.replace("{icon}", "icon.etc_quest_subclass_reward_i00");
				block = block.replace("{name}", Util.className(player, player.getActiveClassId()));
				block = block.replace("{link}", "");
				block = block.replace("{type}", new CustomMessage("services.SubClass.active").toString(player));
				content += block;

				block = template;
				block = block.replace("{icon}", "icon.etc_royal_membership_i00");
				block = block.replace("{name}", Util.className(player, base));
				block = block.replace("{link}", "bypass -h htmbypass_services.Subclass:do_change " + base);
				block = block.replace("{type}", new CustomMessage("services.SubClass.base").toString(player));
				content += block;
			}

			for(SubClass subClass : list.values())
			{
				int sub = subClass.getClassId();

				if(!subClass.isBase() && sub != player.getActiveClassId())
				{
					block = template;
					block = block.replace("{icon}", "icon.etc_quest_subclass_reward_i00");
					block = block.replace("{name}", Util.className(player, sub));
					block = block.replace("{link}", "bypass -h htmbypass_services.Subclass:do_change " + sub);
					block = block.replace("{type}", new CustomMessage("services.SubClass.normal").toString(player));
					content += block;
				}
			}
		}
		html.replace("%content%", content);
		player.sendPacket(html);
	}

	@Bypass("services.Subclass:do_change")
	public void do_change(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.BBS_CLASS_MASTER_CHANGE_SUB_CLASS)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(!checkCondition(player))
			return;

		int id = Integer.parseInt(arg[0]);
		if(Util.getPay(player, (int) Config.BBS_CLASS_MASTER_SUB_CHANGE_PRICE_COUNT[1], Config.BBS_CLASS_MASTER_SUB_CHANGE_PRICE_COUNT[0], true))
		{
			player.setActiveSubClass(id, true);
			Career.getInstance().showClassPage(player);
			player.sendMessage(new CustomMessage("services.SubClass.change.done").addString(Util.className(player, player.getActiveClassId())));
			return;
		}
	}

	@Bypass("services.Subclass:cancel")
	public void cancel(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.BBS_CLASS_MASTER_CANCEL_SUB_CLASS)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(!checkCondition(player))
			return;

		HtmlMessage html;

		String content = "";

		Map<Integer, SubClass> list = player.getSubClasses();

		if(list.size() < 2)
		{
			html = new HtmlMessage(5).setFile("scripts/services/Subclass/change_empty.htm");
			html.replace("%price%", Util.formatPay(player, Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[0], (int) Config.BBS_CLASS_MASTER_SUB_ADD_PRICE_COUNT[1]));
		}
		else
		{
			html = new HtmlMessage(5).setFile("scripts/services/Subclass/cancel.htm");
			String template = HtmCache.getInstance().getHtml("scripts/services/Subclass/template_cancel.htm", player);
			String block = null;
			for(SubClass sub : list.values())
			{
				if(!sub.isBase())
				{
					block = template;
					block = block.replace("{icon}", "icon.etc_quest_subclass_reward_i00");
					block = block.replace("{name}", Util.className(player, sub.getClassId()));
					block = block.replace("{link}", "bypass -h htmbypass_services.Subclass:page choice_cancel " + sub.getClassId());
					block = block.replace("{type}", new CustomMessage("services.SubClass.normal").toString(player));
					content += block;
				}
			}
		}

		html = html.replace("%content%", content);
		player.sendPacket(html);
	}

	@Bypass("services.Subclass:cancel_choice")
	public void cancel_choice(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.BBS_CLASS_MASTER_CANCEL_SUB_CLASS)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(!checkCondition(player))
			return;

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Subclass/cancel_choice.htm");

		String content = "";

		Set<PlayerClass> sub = getAvailableSubClasses(player, true);

		if(!sub.isEmpty())
		{
			String template = HtmCache.getInstance().getHtml("scripts/services/Subclass/template_cancel_choice.htm", player);
			String block = null;

			String type = arg[0];
			String race = arg[1];
			int id = Integer.parseInt(arg[2]);

			for(PlayerClass SUB : sub)
			{
				if(!check(type, SUB))
					continue;
				else if(race.equals("human") && !SUB.isOfRace(Race.human))
					continue;
				else if(race.equals("darkelf") && (!SUB.isOfRace(Race.darkelf) || player.getRace() == Race.elf))
					continue;
				else if(race.equals("dwarf") && !SUB.isOfRace(Race.dwarf))
					continue;
				else if(race.equals("elf") && (!SUB.isOfRace(Race.elf) || player.getRace() == Race.darkelf))
					continue;
				else if(race.equals("kamael") && (!SUB.isOfRace(Race.kamael) || player.getRace() != Race.kamael))
					continue;
				else if(race.equals("orc") && !SUB.isOfRace(Race.orc))
					continue;

				block = template;
				block = block.replace("{icon}", Util.getRaceIcon(SUB));
				block = block.replace("{name}", format(SUB));
				block = block.replace("{link}", "bypass -h htmbypass_services.Subclass:do_cancel " + id + " " + SUB.ordinal());
				block = block.replace("{race}", Util.getRaceName(player, SUB));
				content += block;
			}
		}

		if(content.isEmpty())
		{
			player.sendMessage(new CustomMessage("l2p.gameserver.model.instances.L2VillageMasterInstance.NoSubAtThisTime"));
			return;
		}

		html = html.replace("%content%", content);
		player.sendPacket(html);
	}

	@Bypass("services.Subclass:do_cancel")
	public void do_cancel(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.BBS_CLASS_MASTER_CANCEL_SUB_CLASS)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(!checkCondition(player))
			return;

		int oldid = Integer.parseInt(arg[0]);
		int newid = Integer.parseInt(arg[1]);

		if(Util.getPay(player, (int) Config.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[1], Config.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[0], true))
		{
			if(player.modifySubClass(oldid, newid))
			{
				if(newid > -1)
					player.sendMessage(new CustomMessage("services.SubClass.cancel.done").addString(Util.className(player, oldid)).addString(Util.className(player, newid)));
				else
					player.sendMessage(new CustomMessage("services.SubClass.cancel.delete").addString(Util.className(player, oldid)));
				Career.getInstance().showClassPage(player);
				return;
			}
			else
			{
				player.sendMessage(new CustomMessage("l2p.gameserver.model.instances.L2VillageMasterInstance.SubclassCouldNotBeAdded"));
				player.getInventory().addItem((int) Config.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[1], Config.BBS_CLASS_MASTER_SUB_CANCEL_PRICE_COUNT[0]);
				return;
			}
		}
	}

	@SuppressWarnings("incomplete-switch")
	private Set<PlayerClass> getAvailableSubClasses(Player player, boolean isNew)
	{
		final int charClassId = player.getBaseClassId();

		PlayerClass currClass = PlayerClass.values()[charClassId];

		Set<PlayerClass> availSubs = currClass.getAvailableSubclasses();

		if(availSubs == null)
			return Collections.emptySet();

		availSubs.remove(currClass);

		for(PlayerClass availSub : availSubs)
		{
			for(SubClass subClass : player.getSubClasses().values())
			{
				if(availSub.ordinal() == subClass.getClassId())
				{
					availSubs.remove(availSub);
					continue;
				}

				ClassId parent = ClassId.VALUES[availSub.ordinal()].getParent(player.getSex());
				if(parent != null && parent.getId() == subClass.getClassId())
				{
					availSubs.remove(availSub);
					continue;
				}

				ClassId subParent = ClassId.VALUES[subClass.getClassId()].getParent(player.getSex());
				if(subParent != null && subParent.getId() == availSub.ordinal())
					availSubs.remove(availSub);
			}

			if(availSub.isOfRace(Race.kamael))
			{
				switch(currClass)
				{
					case MaleSoulHound:
					case FemaleSoulHound:
					case FemaleSoulbreaker:
					case MaleSoulbreaker:
						if(availSub == PlayerClass.FemaleSoulbreaker || availSub == PlayerClass.MaleSoulbreaker)
							availSubs.remove(availSub);
						break;
					case Berserker:
					case Doombringer:
					case Arbalester:
					case Trickster:
						if(player.getSex() == 1 && availSub == PlayerClass.MaleSoulbreaker || player.getSex() == 0 && availSub == PlayerClass.FemaleSoulbreaker)
							availSubs.remove(availSub);
						break;
					case Inspector:
						if(player.getSubClasses().size() < (isNew ? 3 : 4))
							availSubs.remove(availSub);
				}
			}
		}
		return availSubs;
	}

	private String format(PlayerClass className)
	{
		String classNameStr = className.toString();
		char[] charArray = classNameStr.toCharArray();

		for(int i = 1; i < charArray.length; i++)
		{
			if(Character.isUpperCase(charArray[i]))
				classNameStr = classNameStr.substring(0, i) + " " + classNameStr.substring(i);
		}

		return classNameStr;
	}

	private boolean checkCondition(Player player)
	{
		if(player.isDead() || player.isAlikeDead() || player.isCastingNow() || player.isAttackingNow())
		{
			player.sendMessage(new CustomMessage("services.SubClass.condition.Dead"));
			return false;
		}
		else if(player.getServitor() != null)
		{
			player.sendPacket(SystemMsg.A_SUBCLASS_MAY_NOT_BE_CREATED_OR_CHANGED_WHILE_A_SERVITOR_OR_PET_IS_SUMMONED);
			return false;
		}
		else if(player.isActionsDisabled() || player.getTransformation() != 0)
		{
			player.sendPacket(SystemMsg.SUBCLASSES_MAY_NOT_BE_CREATED_OR_CHANGED_WHILE_A_SKILL_IS_IN_USE);
			return false;
		}
		else if(player.getWeightPenalty() >= 3)
		{
			player.sendPacket(SystemMsg.A_SUBCLASS_CANNOT_BE_CREATED_OR_CHANGED_WHILE_YOU_ARE_OVER_YOUR_WEIGHT_LIMIT);
			return false;
		}
		else if(player.getInventoryLimit() * 0.8 < player.getInventory().getSize())
		{
			player.sendPacket(SystemMsg.A_SUBCLASS_CANNOT_BE_CREATED_OR_CHANGED_BECAUSE_YOU_HAVE_EXCEEDED_YOUR_INVENTORY_LIMIT);
			return false;
		}
		else if(player.isInZone(Zone.ZoneType.battle_zone) || player.isInZone(Zone.ZoneType.no_escape) || player.isInZone(Zone.ZoneType.epic) || player.isInZone(Zone.ZoneType.SIEGE) || player.isInZone(Zone.ZoneType.RESIDENCE) || player.getVar("jailed") != null)
		{
			player.sendMessage(new CustomMessage("services.SubClass.condition.battle_zone"));
			return false;
		}
		else if(player.getActiveWeaponFlagAttachment() != null)
		{
			player.sendMessage(new CustomMessage("services.SubClass.condition.FlagAttachment"));
			return false;
		}
		else if(Config.ENABLE_OLYMPIAD && Olympiad.isRegisteredInComp(player) || player.isInOlympiadMode())
		{
			player.sendMessage(new CustomMessage("scripts.services.olympiad.enable"));
			return false;
		}
		else if(player.getReflection() != ReflectionManager.DEFAULT)
		{
			player.sendMessage(new CustomMessage("services.SubClass.condition.Reflection"));
			return false;
		}
		else if(player.isInDuel() || player.getTeam() != TeamType.NONE)
		{
			player.sendMessage(new CustomMessage("services.SubClass.condition.InDuel"));
			return false;
		}
		else if(player.isInCombat() || player.getPvpFlag() != 0)
		{
			player.sendMessage(new CustomMessage("services.SubClass.condition.InCombat"));
			return false;
		}
		else if(player.isOnSiegeField() || player.isInZoneBattle())
		{
			player.sendMessage(new CustomMessage("services.SubClass.condition.OnSiegeField"));
			return false;
		}
		else if(player.isFlying())
		{
			player.sendMessage(new CustomMessage("services.SubClass.condition.Flying"));
			return false;
		}
		else if(player.isInWater() || player.isInBoat())
		{
			player.sendMessage(new CustomMessage("services.SubClass.condition.InWater"));
			return false;
		}
		else
		{
			final Servitor servitor = player.getServitor();
			if(servitor != null)
			{
				if(servitor.isPet())
				{
					final ItemInstance controlItem = ((PetInstance) servitor).getControlItem();
					if(!controlItem.getTemplate().testCondition(player, controlItem, false))
						servitor.unSummon(false, false);
				}
				else
					servitor.unSummon(false, false);
			}

			List<int[]> summon = player.getSavedServitors();
			if(!summon.isEmpty()) // Чистим список рестор петов! Так как можно ресторить петов не от своего класса!
			{
				final int obj = player.getObjectId();
				SummonDAO.getInstance().delete(obj, summon.get(0)[1]);
				CharacterServitorDAO.getInstance().delete(obj);
			}
		}

		return true;
	}
}
