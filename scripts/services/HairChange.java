package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.utils.Util;

public class HairChange
{
	// Список одобренных причесек. (Сделано массивом для клиентов если нужно будет вносить правки)
	private static final int[] Male = new int[] { 1, 1, 1, 1, 1, 0, 0 };
	private static final int[] Female = new int[] { 1, 1, 1, 1, 1, 1, 1 };

	@Bypass("services.HairChange:show")
	public void show(Player player, NpcInstance npc, String[] arg)
	{
		if(Config.SERVICES_HAIR_CHANGE_ITEM_ID == -1)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/HairChange/index.htm");

		for(int i = 0; i < 7; i++)
		{
			String button = "<button action=\"bypass -h htmbypass_services.HairChange:ask " + i + "\" width=34 height=34 back=\"L2UI_CT1.ItemWindow_DF_Frame_Down\" fore=\"L2UI_CT1.ItemWindow_DF_Frame\"/>";
			String prohibited = "<img src=\"L2UI_CT1.ItemWindow_DF_SlotBox_Disable\" width=\"32\" height=\"32\">";
			final boolean result = player.getHairStyle() != i && (player.getSex() == 0 ? Male[i] == 0 ? false : true : Female[i] == 0 ? false : true);

			html.replace("%hair_" + (i + 1) + "%", result ? button : prohibited);
			html.replace("%color_" + (i + 1) + "%", result ? "99CC00" : "CC3333");
		}

		html.replace("%now%", HairTypeName(player.getHairStyle()));

		player.sendPacket(html);
	}

	@Bypass("services.HairChange:ask")
	public void ask(Player player, NpcInstance npc, String[] arg)
	{
		int id = Util.isNumber(arg[0]) ? Integer.parseInt(arg[0]) : 0;

		String msg = new CustomMessage("hair.ask").addString(HairTypeName(player.getHairStyle())).addString(HairTypeName(id)).addString(Util.formatPay(player, Config.SERVICES_HAIR_CHANGE_COUNT, Config.SERVICES_HAIR_CHANGE_ITEM_ID)).toString(player);
		ConfirmDlg ask = new ConfirmDlg(SystemMsg.S1, 60000);
		ask.addString(msg);

		player.ask(ask, new AnswerListener(id));
	}

	private class AnswerListener implements OnAnswerListener
	{
		private int id;

		protected AnswerListener(int id)
		{
			this.id = id;
		}

		@Override
		public void sayYes(Player player)
		{
			if(!player.isOnline() || !correct(id))
				return;

			changeHair(player, id);
			show(player, null, null);
		}

		@Override
		public void sayNo(Player player)
		{}
	}

	private final static boolean correct(int id)
	{
		return id >= 0 || id <= 6;
	}

	private final static String HairTypeName(int id)
	{
		switch(id)
		{
			case 0:
				return "A";
			case 1:
				return "B";
			case 2:
				return "C";
			case 3:
				return "D";
			case 4:
				return "E";
			case 5:
				return "F";
			case 6:
				return "G";
			default:
				return "?";
		}
	}

	private final static void changeHair(Player player, int id)
	{
		if(Util.getPay(player, Config.SERVICES_HAIR_CHANGE_ITEM_ID, Config.SERVICES_HAIR_CHANGE_COUNT, true))
		{
			player.setHairStyle(id);
			player.sendMessage(new CustomMessage("hair.change"));
			player.broadcastPacket(new MagicSkillUse(player, player, 6696, 1, 1000, 0));
			player.broadcastCharInfo();
		}
	}
}
