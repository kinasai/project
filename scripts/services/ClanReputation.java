package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.utils.DeclensionKey;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ClanReputation
{
	@Bypass("clanreputation:list")
	public void list(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SERVICES_CLAN_REPUTATION_SELL_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/ClanReputation/index.htm");
		String template = HtmCache.getInstance().getHtml("scripts/services/ClanReputation/template.htm", player);
		String block = "";
		String list = "";

		for(int i = 0; i < Config.SERVICES_CLAN_REPUTATION_SELL_DATA.length; i += 3)
		{
			block = template;
			block = block.replace("{id}", String.valueOf(i));
			final int count = Config.SERVICES_CLAN_REPUTATION_SELL_DATA[i];
			block = block.replace("{count}", count + " " + Util.declension(player.getLanguage(), (int) count, DeclensionKey.POINT));
			block = block.replace("{cost}", Util.formatPay(player, Config.SERVICES_CLAN_REPUTATION_SELL_DATA[i + 2], Config.SERVICES_CLAN_REPUTATION_SELL_DATA[i + 1]));
			list += block;
		}

		html.replace("%list%", list);

		player.sendPacket(html);
	}

	@Bypass("clanreputation")
	public void clanreputation(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SERVICES_CLAN_REPUTATION_SELL_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		Clan clan = player.getClan();
		if(clan == null)
		{
			player.sendMessage("You must be in clan!");
			return;
		}
		else if(clan.getLevel() < 5)
		{
			player.sendMessage("Clan reputation can buy only clan with level 5 or higher!");
			return;
		}
		else if(!Util.isNumber(arg[0]))
			return;

		final int id = Integer.parseInt(arg[0]);
		final int item = Config.SERVICES_CLAN_REPUTATION_SELL_DATA[id + 1];
		final long count = Config.SERVICES_CLAN_REPUTATION_SELL_DATA[id + 2];
		final int point = Config.SERVICES_CLAN_REPUTATION_SELL_DATA[id];

		if(Util.getPay(player, item, count, true))
		{
			player.sendMessage("You buy " + Util.formatAdena(point) + " Clan point!");
			clan.incReputation(point, false, "DonateFunction");
			player.broadcastPacket(new MagicSkillUse(player, player, 6696, 1, 1000, 0));
		}
	}
}