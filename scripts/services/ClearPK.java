package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.utils.DeclensionKey;
import com.l2cccp.gameserver.utils.Util;

public class ClearPK
{
	@Bypass("services.ClearPK:clear")
	public void clear(Player player, NpcInstance npc, String[] arg)
	{
		if(Config.SERVICES_CLEAR_PK_COUNT == 0)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(player.getPkKills() == 0)
		{
			player.sendMessage("You don't have PK points");
			return;
		}
		else if(player.isDead() || player.isAlikeDead() || player.isCastingNow() || player.isInCombat() || player.isAttackingNow() || player.isFlying())
		{
			player.sendMessage(new CustomMessage("common.Error"));
			return;
		}
		else
		{
			if(Util.getPay(player, Config.SERVICES_CLEAR_PK_PRICE_ITEM_ID, Config.SERVICES_CLEAR_PK_PRICE, true))
			{
				int pkCount = player.getPkKills() - Config.SERVICES_CLEAR_PK_COUNT;
				int msgpk = pkCount < 0 ? player.getPkKills() : Config.SERVICES_CLEAR_PK_COUNT;

				CustomMessage msg = new CustomMessage("clear.pk").addNumber(msgpk).addString(Util.declension(player.getLanguage(), pkCount, DeclensionKey.POINT));
				if(pkCount >= 0) // Если очков pk больше чем количество снимаего за раз то снимаем по конфигу, если меньше ставим 0.
					player.setPkKills(pkCount);
				else
					player.setPkKills(0);

				player.sendPacket(new ExShowScreenMessage(msg.toString(player), 3000, ScreenMessageAlign.TOP_CENTER, true));
				player.sendMessage(msg);
				player.sendChanges();
			}
		}
	}
}