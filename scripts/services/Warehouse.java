package services;

import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.utils.WarehouseFunctions;

public class Warehouse
{
	@Bypass("warehouse.private.deposit")
	public void private_deposit(Player player, NpcInstance npc, String[] arg)
	{
		if(!check(player))
			return;

		WarehouseFunctions.showDepositWindow(player);
	}

	@Bypass("warehouse.private.retrieve")
	public void private_retrieve(Player player, NpcInstance npc, String[] arg)
	{
		if(!check(player))
			return;

		WarehouseFunctions.showRetrieveWindow(player, 0);
	}

	@Bypass("warehouse.clan.deposit")
	public void clan_deposit(Player player, NpcInstance npc, String[] arg)
	{
		if(!check(player))
			return;

		WarehouseFunctions.showDepositWindowClan(player);
	}

	@Bypass("warehouse.clan.retrieve")
	public void clan_retrieve(Player player, NpcInstance npc, String[] arg)
	{
		if(!check(player))
			return;

		WarehouseFunctions.showWithdrawWindowClan(player, 0);
	}

	private boolean check(Player player)
	{
		if(!player.isInZonePeace())
		{
			player.sendMessage("It can be used only in Peaceful zone!");
			return false;
		}
		if(player.getVar("jailed") != null)
		{
			player.sendMessage("You cannot do it in Jail!");
			return false;
		}
		else
		{
			player.addSessionVar("warehouse", true);
			return true;
		}
	}
}