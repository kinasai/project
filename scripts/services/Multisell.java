package services;

import com.l2cccp.gameserver.data.xml.holder.MultiSellHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.utils.Util;

//bypass -h htmbypass_services.Multisell:open ID
public class Multisell
{
	@Bypass("Multisell:open")
	public void take(Player player, NpcInstance npc, String[] arg)
	{
		if(!Util.isNumber(arg[0]))
			return;

		int id = Integer.parseInt(arg[0]);
		MultiSellHolder.getInstance().SeparateAndSend(id, player, -1, 0);
	}
}