package services;

import java.util.List;

import com.l2cccp.gameserver.data.xml.holder.ResidenceHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.residence.Fortress;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.ClanTable;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.Util;

public class FortInfo //TODO: ПЕРЕПИСАТЬ
{
	@Bypass("services.FortInfo:list")
	public void list(Player player, NpcInstance npc, String[] arg)
	{
		if(!Util.isNumber(arg[0]))
			return;

		int page = Integer.parseInt(arg[0]);
		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/FortInfo/list.htm");
		String list = "";
		if(page == 1)
		{
			list += getButton(101, player);
			list += getButton(102, player);
			list += getButton(103, player);
			list += getButton(104, player);
			list += getButton(105, player);
			list += getButton(106, player);
		}
		else if(page == 2)
		{
			list += getButton(107, player);
			list += getButton(108, player);
			list += getButton(109, player);
			list += getButton(110, player);
			list += getButton(111, player);
			list += getButton(112, player);
		}
		else if(page == 3)
		{
			list += getButton(113, player);
			list += getButton(114, player);
			list += getButton(115, player);
			list += getButton(116, player);
			list += getButton(117, player);
			list += getButton(118, player);
		}
		else if(page == 4)
		{
			list += getButton(119, player);
			list += getButton(120, player);
			list += getButton(121, player);
		}

		html = html.replace("%list%", list);

		player.sendPacket(html);
	}

	private String getButton(int id, Player player)
	{
		return "<button action=\"bypass -h htmbypass_services.FortInfo:fort " + id + "\" value=\"" + new CustomMessage("common.fort." + id + "").toString(player) + "\" width=200 height=26 back=\"l2ui_ct1.button.button_df_small_down\" fore=\"l2ui_ct1.button.button_df_small\">";
	}

	@Bypass("services.FortInfo:fort")
	public void fort(Player player, NpcInstance npc, String[] arg)
	{
		if(!Util.isNumber(arg[0]))
			return;
		int id = Integer.parseInt(arg[0]);

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/FortInfo/fort.htm");
		html.replace("%name%", Util.getFortName(player, id).toString());
		html.replace("%info%", Fort(id));
		Fortress fortress = ResidenceHolder.getInstance().getResidence(Fortress.class, id);

		html.replace("%skill%", skills(fortress.getSkills()));
		player.sendPacket(html);
	}

	private String Fort(int id)
	{
		String Big = "Big.";
		String Small = "Small.";
		String Territorial = "Territorial.";
		String Border = "Border.";

		String type = "...", size = "...";

		switch(id)
		{
			case 101:
				size = Small;
				type = Territorial;
				break;
			case 102:
				size = Big;
				type = Territorial;
				break;
			case 103:
				size = Small;
				type = Territorial;
				break;
			case 104:
				size = Big;
				type = Territorial;
				break;
			case 105:
				size = Small;
				type = Territorial;
				break;
			case 106:
				size = Small;
				type = Territorial;
				break;
			case 107:
				size = Big;
				type = Territorial;
				break;
			case 108:
				size = Small;
				type = Territorial;
				break;
			case 109:
				size = Big;
				type = Territorial;
				break;
			case 110:
				size = Big;
				type = Territorial;
				break;
			case 111:
				size = Small;
				type = Territorial;
				break;
			case 112:
				size = Big;
				type = Border;
				break;
			case 113:
				size = Big;
				type = Border;
				break;
			case 114:
				size = Small;
				type = Border;
				break;
			case 115:
				size = Small;
				type = Border;
				break;
			case 116:
				size = Big;
				type = Border;
				break;
			case 117:
				size = Big;
				type = Border;
				break;
			case 118:
				size = Big;
				type = Border;
				break;
			case 119:
				size = Small;
				type = Border;
				break;
			case 120:
				size = Small;
				type = Border;
				break;
			case 121:
				size = Small;
				type = Border;
				break;
			default:
				break;
		}

		StringBuilder html = new StringBuilder();

		Fortress fortress = ResidenceHolder.getInstance().getResidence(Fortress.class, id);
		html.append("<table border=0 width=290>");
		html.append("<tr>");
		html.append("<td width=54 align=center valign=top height=20>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=30 height=20>");
		html.append("<tr>");
		html.append("<td width=32 height=45 align=center valign=top>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=32 height=32 background=\"icon.weapon_fort_flag_i00\">");
		html.append("<tr>");
		html.append("<td width=32 align=center valign=top>");
		html.append("<img src=\"icon.castle_tab\" width=32 height=32>");
		html.append("</td>");
		html.append("</tr>");
		html.append("</table>");
		html.append("</td>");
		html.append("</tr>");
		html.append("</table>");
		html.append("</td>");
		html.append("<td FIXWIDTH=230 align=left valign=top>");

		String fort_owner;
		Clan owner = fortress.getOwnerId() == 0 ? null : ClanTable.getInstance().getClan(fortress.getOwnerId());
		if(owner == null)
			fort_owner = "NPC";
		else
			fort_owner = owner.getName();

		html.append("Owner: <font color=\"FFFF00\">" + fort_owner + "</font>");
		html.append("<br1>Size: <font color=\"AAAAAA\">" + size + "</font>");
		html.append("<br1>Type: <font color=\"AAAAAA\">" + type + "</font>");
		html.append("</td>");
		html.append("</tr>");
		html.append("</table>");
		return html.toString();
	}

	private String skills(List<SkillEntry> list)
	{
		String html = "";

		for(SkillEntry skill : list)
		{
			html += skillHtml(skill.getId());
		}

		return html;
	}

	private String skillHtml(int id)
	{
		SkillEntry skill = SkillTable.getInstance().getSkillEntry(id, 1);
		return FortSkillsBlock(skill);
	}

	private String FortSkillsBlock(SkillEntry skill)
	{
		StringBuilder html = new StringBuilder();

		html.append("<br>");
		html.append("<table border=0 width=290 height=30>");
		html.append("<tr>");
		html.append("<td width=54 align=center valign=top height=20>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=30 height=20>");
		html.append("<tr>");
		html.append("<td width=32 height=45 align=center valign=top>");
		html.append("<table border=0 cellspacing=0 cellpadding=0 width=32 height=32 background=\"icon.skill" + skill.getTemplate().getIcon() + "\">");
		html.append("<tr>");
		html.append("<td width=32 align=center valign=top>");
		html.append("<img src=\"icon.panel_2\" width=32 height=32>");
		html.append("</td>");
		html.append("</tr>");
		html.append("</table>");
		html.append("</td>");
		html.append("</tr>");
		html.append("</table>");
		html.append("</td>");
		html.append("<td FIXWIDTH=230 align=left valign=top>");
		html.append("<font color=\"CCFF33\">" + skill.getName() + "</font>");
		html.append("<br1>Level: <font color=\"AAAAAA\">" + skill.getLevel() + "</font>");
		html.append("</td>");
		html.append("</tr>");
		html.append("</table>");

		return html.toString();
	}

}
