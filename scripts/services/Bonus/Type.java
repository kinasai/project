package services.Bonus;

public enum Type
{
	CLAN(0),
	PARTY(1);

	public int id;

	Type(int id)
	{
		this.id = id;
	}

	public int getid()
	{
		return id;
	}
}
