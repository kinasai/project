package services.Bonus;

import java.util.List;

import com.l2cccp.gameserver.model.Player;

public class Reward
{
	private String name;
	private List<Player> players;
	private Type type;

	public Reward(String name, List<Player> players, Type type)
	{
		this.name = name;
		this.players = players;
		this.type = type;
	}

	public String getName()
	{
		return name;
	}

	public Type getType()
	{
		return type;
	}

	public List<Player> getPlayers()
	{
		return players;
	}
}