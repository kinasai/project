package services.Bonus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.l2cccp.commons.dbutils.DbUtils;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.database.DatabaseFactory;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.templates.item.ItemTemplate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Bonus
{
	private static final Logger _log = LoggerFactory.getLogger(Bonus.class);
	private Map<Integer, Reward> toReward = new HashMap<Integer, Reward>();

	public static Bonus _instance;

	public static Bonus getInstance()
	{
		if(_instance == null)
			_instance = new Bonus();

		return _instance;
	}

	private Status giveReward(Reward reward)
	{
		if(!Config.BONUS_SERVICE_ENABLE)
			return Status.STATUS_ERROR;

		Status status = Status.STATUS_OK;
		Connection con = null;
		PreparedStatement statement = null;

		for(Player player : reward.getPlayers())
		{
			if(reward.getType().equals(Type.CLAN))
			{
				if(player.getLevel() < Config.BONUS_SERVICE_CLAN_MIN_LEVEL)
					continue;

				for(int i = 0; i < Config.BONUS_SERVICE_CLAN_REWARD.length; i += 2)
				{
					int id = (int) Config.BONUS_SERVICE_CLAN_REWARD[i];
					long count = Config.BONUS_SERVICE_CLAN_REWARD[i + 1];
					if(id == ItemTemplate.ITEM_ID_FAME)
						player.setFame(player.getFame() + (int) count, "BonusManager");
					else if(id == ItemTemplate.ITEM_ID_CLAN_REPUTATION_SCORE)
					{
						if(player.getClan() != null)
						{
							if(player.getClan().getLevel() < 5)
								player.getClan().setLevel(5);
							player.getClan().incReputation((int) count, false, "BonusManager");
						}
					}
					else
						player.getInventory().addItem(id, count);
				}
			}
			else if(reward.getType().equals(Type.PARTY))
			{
				if(player.getLevel() < Config.BONUS_SERVICE_PARTY_MIN_LEVEL)
					continue;

				for(int i = 0; i < Config.BONUS_SERVICE_PARTY_REWARD.length; i += 2)
				{
					int id = (int) Config.BONUS_SERVICE_PARTY_REWARD[i];
					long count = Config.BONUS_SERVICE_PARTY_REWARD[i + 1];
					if(id == ItemTemplate.ITEM_ID_FAME)
						player.setFame(player.getFame() + (int) count, "BonusManager");
					else
						player.getInventory().addItem(id, count);
				}
			}

			try
			{
				con = DatabaseFactory.getInstance().getConnection();
				statement = con.prepareStatement("INSERT INTO `bonus` VALUES (?,?,?,?,?)");
				statement.setString(1, player.getName());
				statement.setInt(2, player.getObjectId());
				statement.setString(3, player.getNetConnection().getHWID());
				statement.setString(4, player.getNetConnection().getIpAddr());
				statement.setString(5, reward.getType().toString());
				statement.execute();
			}
			catch(SQLException e)
			{
				_log.warn("", e);
				status = Status.STATUS_ERROR;
			}
			finally
			{
				DbUtils.closeQuietly(con, statement);
			}
		}

		return status;
	}

	public Status doReward(int hash)
	{
		if(toReward.get(hash) == null)
			return Status.STATUS_ERROR;

		return giveReward(toReward.get(hash));
	}

	public void sortPlayers(List<Player> players)
	{
		List<Player> remove = new ArrayList<Player>();
		for(Player player : players)
		{
			if(checkPlayerReward(player))
				remove.add(player);
		}
		players.removeAll(remove);
	}

	private boolean checkPlayerReward(Player player)
	{
		boolean result = false;
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM `bonus` WHERE `obj_id` = ? OR `hwid` = ? OR `ip` = ?");
			statement.setInt(1, player.getObjectId());
			statement.setString(2, player.getNetConnection().getHWID());
			statement.setString(3, player.getNetConnection().getIpAddr());
			rset = statement.executeQuery();
			result = rset.next();
		}
		catch(SQLException e)
		{
			_log.warn("", e);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}
		return result;
	}

	public void makeRewardList(Player player, List<Player> players, Type type)
	{
		if(toReward.containsKey(players.hashCode()))
			return;

		toReward.put(players.hashCode(), new Reward(player.getName(), players, type));
	}

	public boolean hasReward(String name, Type type)
	{
		boolean result = true;
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;

		try
		{
			con = DatabaseFactory.getInstance().getConnection();
			statement = con.prepareStatement("SELECT * FROM `bonus` WHERE `player`=?");
			statement.setString(1, name);
			rset = statement.executeQuery();
			result = rset.next();
		}
		catch(SQLException e)
		{
			_log.warn("Error while executing 'hasReward' for {}", name);
		}
		finally
		{
			DbUtils.closeQuietly(con, statement, rset);
		}

		return result;
	}
}