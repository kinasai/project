package services;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.ExchangeItemHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.Element;
import com.l2cccp.gameserver.model.exchange.Change;
import com.l2cccp.gameserver.model.exchange.Variant;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.ItemAttributes;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.items.PcInventory;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Util;

public class Exchanger
{

	@Bypass("change.page")
	public void change_page(Player player, NpcInstance npc, String[] arg)
	{
		removeVars(player, true);
		cleanAtt(player, -1);
		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Exchanger/page.htm");
		String template = HtmCache.getInstance().getHtml("scripts/services/Exchanger/template.htm", player);
		String block = "";
		String list = "";
		List<Change> _list = new ArrayList<Change>();
		for(ItemInstance item : player.getInventory().getPaperdollItems())
		{
			if(item != null)
			{
				Change change = ExchangeItemHolder.getInstance().getChanges(item.getItemId());
				if(change != null)
					_list.add(change);
			}
		}

		final int perpage = 6;
		final int page = !arg[0].isEmpty() && Util.isNumber(arg[0]) ? Integer.parseInt(arg[0]) : 1;
		int counter = 0;
		for(int i = (page - 1) * perpage; i < _list.size(); i++)
		{
			Change pack = _list.get(i);
			block = template;
			block = block.replace("{bypass}", "bypass -h htmbypass_change.list " + pack.getId());
			block = block.replace("{name}", pack.getName());
			block = block.replace("{icon}", pack.getIcon());
			block = block.replace("{cost}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, pack.getCostCount(), pack.getCostId())).toString(player));
			list += block;

			counter++;

			if(counter >= perpage)
				break;
		}

		double count = Math.ceil((double) _list.size() / (double) perpage); // Используем округление для получения последней страницы с остатком!

		int inline = 1;
		String navigation = "";
		for(int i = 1; i <= count; i++)
		{
			if(i == page)
				navigation += "<td width=25 align=center valign=top><button value=\"[" + i + "]\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";
			else
				navigation += "<td width=25 align=center valign=top><button value=\"" + i + "\" action=\"bypass -h htmbypass_change.page " + i + "\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";

			if(inline % 7 == 0)
				navigation += "</tr><tr>";

			inline++;
		}

		if(inline == 2)
			navigation = "<td width=30 align=center valign=top>...</td>";

		html.replace("%list%", list);
		html.replace("%navigation%", navigation);

		player.sendPacket(html);
	}

	@Bypass("change.list")
	public void change_list(Player player, NpcInstance npc, String[] arg)
	{
		cleanAtt(player, -1);
		removeVars(player, true);
		if(arg[0].isEmpty() || !Util.isNumber(arg[0]))
			return;

		int id = Integer.parseInt(arg[0]);

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Exchanger/list.htm");
		String template = HtmCache.getInstance().getHtml("scripts/services/Exchanger/template.htm", player);
		String block = "";
		String list = "";

		Change change = ExchangeItemHolder.getInstance().getChanges(id);
		if(change == null)
			return;

		player.addSessionVar("exchange", id);

		List<Variant> _list = change.getList();

		final int perpage = 6;
		final int page = arg.length > 1 && !arg[1].isEmpty() && Util.isNumber(arg[1]) ? Integer.parseInt(arg[1]) : 1;
		int counter = 0;
		for(int i = (page - 1) * perpage; i < _list.size(); i++)
		{
			Variant pack = _list.get(i);
			block = template;
			block = block.replace("{bypass}", "bypass -h htmbypass_change.open " + pack.getNumber());
			block = block.replace("{name}", pack.getName());
			block = block.replace("{icon}", pack.getIcon());
			block = block.replace("{cost}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, change.getCostCount(), change.getCostId())).toString(player));
			list += block;

			counter++;

			if(counter >= perpage)
				break;
		}

		double count = Math.ceil((double) _list.size() / (double) perpage); // Используем округление для получения последней страницы с остатком!

		int inline = 1;
		String navigation = "";
		for(int i = 1; i <= count; i++)
		{
			if(i == page)
				navigation += "<td width=25 align=center valign=top><button value=\"[" + i + "]\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";
			else
				navigation += "<td width=25 align=center valign=top><button value=\"" + i + "\" action=\"bypass -h htmbypass_change.list " + id + " " + i + "\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";

			if(inline % 7 == 0)
				navigation += "</tr><tr>";

			inline++;
		}

		if(inline == 2)
			navigation = "<td width=30 align=center valign=top>...</td>";

		html.replace("%list%", list);
		html.replace("%navigation%", navigation);
		html.replace("%choice%", change.getName());

		player.sendPacket(html);
	}

	@Bypass("change.att")
	public void change_att(Player player, NpcInstance npc, String[] arg)
	{
		if(arg[0].isEmpty() || !Util.isNumber(arg[0]))
			return;

		int obj_my = player.getSessionVarI("exchange_obj", -1);
		if(obj_my == -1)
			return;

		ItemInstance item = player.getInventory().getItemByObjectId(obj_my);
		if(item == null)
			return;

		int id_new = player.getSessionVarI("exchange_number", -1);
		if(id_new == -1)
			return;

		int att_id = Integer.parseInt(arg[0]);
		Element att = Element.getElementById(att_id); // Получаем нужный нам атт

		if(att != Element.NONE)
		{
			player.addSessionVar("ex_att_" + att_id, item.getAttributeElementValue());
			player.addSessionVar("ex_att", att_id);
			cleanAtt(player, att_id);
		}
		change_open(player, npc, new String[] { String.valueOf(id_new) });
	}

	private void cleanAtt(Player player, int exclude)
	{
		for(Element att : Element.VALUES)
		{
			if(att.getId() != exclude)
				player.deleteSessionVar("ex_att_" + att.getId());
		}

		if(exclude == -1)
			player.deleteSessionVar("ex_att");
	}

	@Bypass("change.open")
	public void change_open(Player player, NpcInstance npc, String[] arg)
	{
		int id = player.getSessionVarI("exchange", -1);
		if(id == -1 || arg[0].isEmpty() || !Util.isNumber(arg[0]))
			return;

		int new_id = Integer.parseInt(arg[0]);
		ItemInstance item = null;
		Change change = null;
		for(ItemInstance inv : player.getInventory().getPaperdollItems()) // Ищем предмет в инвентаре в одетых слотах!
		{
			if(inv != null)
			{
				change = ExchangeItemHolder.getInstance().getChanges(inv.getItemId());
				if(change != null && change.getId() == id)
				{
					item = inv;
					break;
				}
			}
		}

		if(item == null)
			return;

		Variant variant = change.getVariant(new_id);
		if(variant == null)
			return;

		removeVars(player, false);
		player.addSessionVar("exchange_obj", item.getObjectId());
		player.addSessionVar("exchange_new", variant.getId());
		player.addSessionVar("exchange_attribute", change.attChange());
		if(change.attChange())
			player.addSessionVar("exchange_number", variant.getNumber());

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Exchanger/general.htm");
		html.replace("%my_name%", item.getName());
		html.replace("%my_ench%", "+" + item.getEnchantLevel());
		html.replace("%my_icon%", item.getTemplate().getIcon());
		ItemAttributes att = item.getAttributes();

		if(!change.attChange() || item.getAttributeElementValue() == 0)
		{
			String att_info = HtmCache.getInstance().getHtml("scripts/services/Exchanger/att_info.htm", player);
			att_info = att_info.replace("%Earth%", String.valueOf(att.getEarth()));
			att_info = att_info.replace("%Fire%", String.valueOf(att.getFire()));
			att_info = att_info.replace("%Holy%", String.valueOf(att.getHoly()));
			att_info = att_info.replace("%Unholy%", String.valueOf(att.getUnholy()));
			att_info = att_info.replace("%Water%", String.valueOf(att.getWater()));
			att_info = att_info.replace("%Wind%", String.valueOf(att.getWind()));
			html.replace("%att_info%", att_info);
		}
		else
		{
			String att_info = HtmCache.getInstance().getHtml("scripts/services/Exchanger/att_change.htm", player);
			if(player.getSessionVarI("ex_att", -1) == -1)
			{
				att_info = att_info.replace("%Earth%", String.valueOf(att.getEarth()));
				att_info = att_info.replace("%Fire%", String.valueOf(att.getFire()));
				att_info = att_info.replace("%Holy%", String.valueOf(att.getHoly()));
				att_info = att_info.replace("%Unholy%", String.valueOf(att.getUnholy()));
				att_info = att_info.replace("%Water%", String.valueOf(att.getWater()));
				att_info = att_info.replace("%Wind%", String.valueOf(att.getWind()));
			}
			else
			{
				att_info = att_info.replace("%Fire%", String.valueOf(player.getSessionVarI("ex_att_0", 0)));
				att_info = att_info.replace("%Water%", String.valueOf(player.getSessionVarI("ex_att_1", 0)));
				att_info = att_info.replace("%Wind%", String.valueOf(player.getSessionVarI("ex_att_2", 0)));
				att_info = att_info.replace("%Earth%", String.valueOf(player.getSessionVarI("ex_att_3", 0)));
				att_info = att_info.replace("%Holy%", String.valueOf(player.getSessionVarI("ex_att_4", 0)));
				att_info = att_info.replace("%Unholy%", String.valueOf(player.getSessionVarI("ex_att_5", 0)));
			}
			html.replace("%att_info%", att_info);
		}

		html.replace("%cost%", Util.formatPay(player, change.getCostCount(), change.getCostId()));
		html.replace("%new_name%", variant.getName());
		html.replace("%new_icon%", variant.getIcon());
		html.replace("%new_id%", String.valueOf(id));

		player.sendPacket(html);
	}

	@Bypass("exchange")
	public void exchange(Player player, NpcInstance npc, String[] arg)
	{
		int obj_my = player.getSessionVarI("exchange_obj", -1);
		if(obj_my == -1)
			return;

		int id_new = player.getSessionVarI("exchange_new", -1);
		if(id_new == -1)
			return;

		boolean att_change = player.getSessionVarB("exchange_attribute", false);

		PcInventory inv = player.getInventory();
		ItemInstance item_my = inv.getItemByObjectId(obj_my);
		if(item_my == null)
			return;

		ItemInstance item = ItemFunctions.createItem(id_new);
		item.setEnchantLevel(item_my.getEnchantLevel());
		item.setAugmentation(item_my.getAugmentationMineralId(), item_my.getAugmentations());

		int new_att = player.getSessionVarI("ex_att", -1);
		if(att_change && new_att != -1)
		{
			Element element = Element.getElementById(new_att);
			int val = item_my.getAttributeElementValue();
			if(val > 0)
				item.setAttributeElement(element, val);
		}
		else
		{
			for(Element element : Element.VALUES)
			{
				int val = item_my.getAttributes().getValue(element);
				if(val > 0)
					item.setAttributeElement(element, val);
			}
		}

		String msg = "You exchange item " + item_my.getName() + " to " + item.getName();
		if(inv.destroyItemByObjectId(item_my.getObjectId(), item_my.getCount()))
		{
			inv.addItem(item);
			item.setJdbcState(JdbcEntityState.UPDATED);
			item.update();
			if(ItemFunctions.checkIfCanEquip(player, item) == null)
				inv.equipItem(item);
			player.sendMessage(msg);
		}
		else
			item.deleteMe();

		removeVars(player, true);
		cleanAtt(player, -1);
	}

	private void removeVars(Player player, boolean exchange)
	{
		if(exchange)
			player.deleteSessionVar("exchange");
		player.deleteSessionVar("exchange_obj");
		player.deleteSessionVar("exchange_new");
		player.deleteSessionVar("exchange_attribute");
	}
}
