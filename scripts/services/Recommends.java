package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Recommends
{
	@Bypass("recommends.show")
	public void show(Player player, NpcInstance npc, String[] arg)
	{
		if(Config.SERVICES_RECOMMENDS_SELL_ENABLED)
		{
			HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Recommends/index.htm");
			html.replace("%price%", Util.formatPay(player, Config.SERVICES_RECOMMENDS_SELL_PRICE, Config.SERVICES_RECOMMENDS_SELL_ITEM));
			player.sendPacket(html);
		}
	}

	@Bypass("recommends")
	public void recommends(Player player, NpcInstance npc, String[] arg)
	{
		if(Config.SERVICES_RECOMMENDS_SELL_ENABLED)
		{
			final int max = 255;

			if(player.getRecommendSystem().getRecommendsHave() >= max)
			{
				player.sendMessage("You have already " + max + " Reccomends!");
				return;
			}

			if(Util.getPay(player, Config.SERVICES_RECOMMENDS_SELL_ITEM, Config.SERVICES_RECOMMENDS_SELL_PRICE, true))
			{
				player.sendMessage("You buy " + Util.formatAdena(Config.SERVICES_RECOMMENDS_SELL_COUNT) + " Recommends!");
				player.getRecommendSystem().addRecommendsHave(Config.SERVICES_RECOMMENDS_SELL_COUNT);
				player.broadcastPacket(new MagicSkillUse(player, player, 6696, 1, 1000, 0));
			}
		}
	}
}