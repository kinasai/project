package services;

import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.botreport.ReportPunishment;
import com.l2cccp.gameserver.model.botreport.ReportTemplate;
import com.l2cccp.gameserver.model.botreport.ReportVariant;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.utils.Log;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class BotReport
{
	@Bypass("report")
	public void answer(Player player, NpcInstance npc, String[] arg)
	{
		final String param = arg[0];
		if(param.isEmpty() || !Util.isNumber(param))
			return;

		final int id = Integer.parseInt(param);
		final ReportTemplate report = player.getReport();

		if(report == null)
		{
			player.unblock();
			player.sendMessage(new CustomMessage("common.Error"));
			return;
		}
		else if(player.getReportTime() > 0)
		{
			for(ReportVariant variants : report.getVariants())
			{
				if(variants.getId() == id)
				{
					player.stopReport();
					if(variants.isCorrect())
					{
						player.unblock();
						player.sendMessage(new CustomMessage("report.bot.passed"));
						Log.report("Player " + player.getName() + " report answer is done successful!", "BotReport");
					}
					else
					{
						player.sendMessage(new CustomMessage("report.bot.wrong"));
						ReportPunishment.getInstance().jail(player);
						Log.report("Player " + player.getName() + " report answer is wrong!", "BotReport");
					}

					return;
				}
			}
		}
	}
}
