package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.ExperienceHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.utils.Util;

public class LevelPanel
{
	@Bypass("services.LevelPanel:show")
	public void show(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SERVICES_LEVEL_UP_ENABLE && !Config.SERVICES_DELEVEL_ENABLE)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/LevelPanel/index.htm");

		String up = "", lower = "";

		// Код высерный но сделано для возможности полной манипуляции оформлением по условиям!
		if(!Config.SERVICES_LEVEL_UP_ENABLE)
		{
			up = HtmCache.getInstance().getHtml("scripts/services/LevelPanel/up_off.htm", player);
			up = up.replace("{cost}", "<font color=\"CC3333\">" + new CustomMessage("scripts.services.off").toString(player) + "</font>");
			html.replace("%up%", up);
		}
		else
		{
			up = HtmCache.getInstance().getHtml("scripts/services/LevelPanel/up.htm", player);
			up = up.replace("{cost}", Util.formatPay(player, Config.SERVICES_LEVEL_UP[1], Config.SERVICES_LEVEL_UP[0]));
			html.replace("%up%", up);
		}

		if(!Config.SERVICES_DELEVEL_ENABLE)
		{
			lower = HtmCache.getInstance().getHtml("scripts/services/LevelPanel/lower_off.htm", player);
			lower = lower.replace("{cost}", "<font color=\"CC3333\">" + new CustomMessage("scripts.services.off").toString(player) + "</font>");
			html.replace("%lower%", lower);
		}
		else
		{
			lower = HtmCache.getInstance().getHtml("scripts/services/LevelPanel/lower.htm", player);
			lower = lower.replace("{cost}", Util.formatPay(player, Config.SERVICES_DELEVEL[1], Config.SERVICES_DELEVEL[0]));
			html.replace("%lower%", lower);
		}

		html.replace("%up_info%", up);
		html.replace("%lower_info%", lower);

		player.sendPacket(html);
	}

	@Bypass("services.LevelPanel:calc")
	public void calc(Player player, NpcInstance npc, String[] arg)
	{
		int level = Util.isNumber(arg[0]) ? Integer.parseInt(arg[0]) : player.getLevel();

		if(level == player.getLevel())
		{
			player.sendMessage("Levels is equals!");
			return;
		}

		int item = level < player.getLevel() ? Config.SERVICES_DELEVEL[0] : Config.SERVICES_LEVEL_UP[0];
		long count = level < player.getLevel() ? ((player.getLevel() - level) * Config.SERVICES_DELEVEL[1]) : ((level - player.getLevel()) * Config.SERVICES_LEVEL_UP[1]);

		String msg = new CustomMessage("level.ask").addNumber(player.getLevel()).addNumber(level).addString(Util.formatPay(player, count, item)).toString(player);
		ConfirmDlg ask = new ConfirmDlg(SystemMsg.S1, 60000);
		ask.addString(msg);

		player.ask(ask, new AnswerListener(level));
	}

	private class AnswerListener implements OnAnswerListener
	{
		private int level;

		protected AnswerListener(int level)
		{
			this.level = level;
		}

		@Override
		public void sayYes(Player player)
		{
			if(!player.isOnline())
				return;

			if(!correct(level, player.getActiveClass().isBase()))
			{
				player.sendMessage("Incorrect level!");
				return;
			}

			boolean delevel = level < player.getLevel();
			if(delevel && !Config.SERVICES_DELEVEL_ENABLE)
			{
				player.sendPacket(new CustomMessage("scripts.services.off"));
				return;
			}
			else if(!delevel && !Config.SERVICES_LEVEL_UP_ENABLE)
			{
				player.sendPacket(new CustomMessage("scripts.services.off"));
				return;
			}

			int item = delevel ? Config.SERVICES_DELEVEL[0] : Config.SERVICES_LEVEL_UP[0];
			long count = delevel ? ((player.getLevel() - level) * Config.SERVICES_DELEVEL[1]) : ((level - player.getLevel()) * Config.SERVICES_LEVEL_UP[1]);

			if(Util.getPay(player, item, count, true))
			{
				player.sendMessage(new CustomMessage("level.change").addNumber(player.getLevel()).addNumber(level));
				Long exp = ExperienceHolder.getInstance().getExpForLevel(level) - player.getExp();
				player.addExpAndSp(exp, 0);
			}
		}

		@Override
		public void sayNo(Player player)
		{}
	}

	private final static boolean correct(int level, boolean base)
	{
		return level >= 1 && level <= (base ? ExperienceHolder.getInstance().getMaxLevel() : ExperienceHolder.getInstance().getMaxSubLevel());
	}
}
