package services;

import java.util.Collection;

import org.napile.primitive.maps.IntObjectMap;

import com.l2cccp.commons.dao.JdbcEntityState;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.OptionDataHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.ShortCut;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.Inventory;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.InventoryUpdate;
import com.l2cccp.gameserver.network.l2.s2c.ShortCutRegister;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.templates.OptionDataTemplate;
import com.l2cccp.gameserver.templates.augmentation.AugmentationFilter;
import com.l2cccp.gameserver.templates.augmentation.AugmentationInfo;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Augmentation
{
	@Bypass("augment.task")
	public void task(Player player, NpcInstance npc, String[] arg)
	{
		if(arg.length < 1)
		{
			showMainMenu(player, 0, 0);
			return;
		}

		String command = arg[0];
		int _page = 0;
		int _filter = 0;
		if(command.equals("menu"))
		{
			_page = 1;
			_filter = Integer.parseInt(arg[1]);
		}
		else if(command.equals("page"))
		{
			_page = Integer.parseInt(arg[1]);
			_filter = Integer.parseInt(arg[2]);
		}
		else if(command.equals("put"))
		{
			try
			{
				if(player.isInStoreMode() || player.isProcessingRequest() || player.isInTrade())
				{
					player.sendMessage("You cannot edit augmentation because you are on store mode");
					return;
				}

				final ItemInstance targetItem = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_RHAND);
				if(targetItem == null)
				{
					player.sendMessage("You doesn't have any weapon equipped");
					return;
				}

				if(!targetItem.isAugmented() && !targetItem.getTemplate().getAugmentationInfos().isEmpty())
				{
					final int augId = Integer.parseInt(arg[1]);
					OptionDataTemplate augment = OptionDataHolder.getInstance().getTemplate(augId);
					if(augment != null && Util.getPay(player, (int) Config.BBS_FORGE_AUGMENT_COST_DATA[0], Config.BBS_FORGE_AUGMENT_COST_DATA[1], true))
					{
						IntObjectMap<AugmentationInfo> augmentationInfos = targetItem.getTemplate().getAugmentationInfos();
						AugmentationInfo augmentationInfo = augmentationInfos.get(16167);
						int[] options = new int[2];
						options[0] = augmentationInfo.randomOption(targetItem.getTemplate())[0];
						options[1] = augment.getId();

						player.getInventory().unEquipItem(targetItem);
						targetItem.setAugmentation(augId, options);
						targetItem.setJdbcState(JdbcEntityState.UPDATED);
						targetItem.update();

						player.getInventory().equipItem(targetItem);

						player.sendPacket(new InventoryUpdate().addModifiedItem(targetItem));

						for(ShortCut sc : player.getAllShortCuts())
						{
							if(sc.getId() == targetItem.getObjectId() && sc.getType() == ShortCut.TYPE_ITEM)
								player.sendPacket(new ShortCutRegister(player, sc));
						}
						player.sendChanges();
					}
				}
				else
					player.sendMessage("Error: Augmentation for this item is empty!");
			}
			catch(Exception e)
			{
				e.printStackTrace();
				player.sendMessage("Error.");
			}
		}

		showMainMenu(player, _page, _filter);
	}

	private AugmentationFilter getFilter(int id)
	{
		switch(id)
		{
			case 2:
				return AugmentationFilter.ACTIVE_SKILL;
			case 3:
				return AugmentationFilter.PASSIVE_SKILL;
			case 4:
				return AugmentationFilter.CHANCE_SKILL;
			case 5:
				return AugmentationFilter.STATS;
			default:
				return AugmentationFilter.NONE;
		}
	}

	private void showMainMenu(Player player, int _page, int _filter)
	{
		if(_page < 1)
		{
			HtmlMessage html = new HtmlMessage(5);
			html.setFile("scripts/services/Augmentations/index.htm");
			player.sendPacket(html);
			return;
		}

		final Collection<OptionDataTemplate> augmentations = OptionDataHolder.getInstance().getUniqueOptions(getFilter(_filter));
		if(augmentations.isEmpty())
		{
			showMainMenu(player, _page, _filter);
			player.sendMessage("Augmentation list is empty. Try with another filter");
			return;
		}

		final HtmlMessage html = new HtmlMessage(5);
		html.setFile("scripts/services/Augmentations/list.htm");

		String template = HtmCache.getInstance().getHtml("scripts/services/Augmentations/template.htm", player);
		String block = "";
		String list = "";

		final StringBuilder pagesHtm = new StringBuilder();
		final int maxPage = (int) Math.ceil(augmentations.size() / (double) 7);
		_page = Math.min(_page, maxPage);

		int page = 1;
		int count = 0;
		boolean lastColor = true;
		SkillEntry skill;

		for(int i = Math.max((maxPage - _page < 6 / 2 ? maxPage - 6 : _page - 6 / 2), 1); i <= maxPage; i++)
		{
			if(i == _page)
				pagesHtm.append("<td background=L2UI_ct1.button_df><button action=\"\" value=\"" + i + "\" width=38 height=20 back=\"\" fore=\"\"></td>");
			else
				pagesHtm.append("<td><button action=\"bypass -h htmbypass_augment.task page " + i + " " + _filter + "\" value=\"" + i + "\" width=34 height=20 back=L2UI_ct1.button_df fore=L2UI_ct1.button_df></td>");

			count++;
			if(count >= 6)
				break;
		}

		count = 0;
		for(OptionDataTemplate augm : augmentations)
		{
			count++;
			if(count >= 7)
			{
				count = 0;
				page++;
				continue;
			}

			if(page > _page)
				break;

			if(page != _page)
				continue;

			skill = (!augm.getTriggerList().isEmpty() ? augm.getTriggerList().get(0).getSkill() : (!augm.getSkills().isEmpty() ? augm.getSkills().get(0) : null));

			block = template;
			block = block.replace("{bypass}", "bypass -h htmbypass_augment.task put " + augm.getId() + " " + _filter);

			String name = "";
			if(skill != null)
				name = (skill.getName().length() > 28 ? skill.getName().substring(0, 28) : skill.getName());
			else
			{
				name = "+1 ";
				switch(augm.getId())
				{
					case 16341: // +1 STR
						name += "STR";
						break;
					case 16342: // +1 CON
						name += "CON";
						break;
					case 16343: // +1 INT
						name += "INT";
						break;
					case 16344: // +1 MEN
						name += "MEN";
						break;
					default:
						name += "(Id:" + augm.getId() + ")";
						break;
				}
			}

			block = block.replace("{name}", name);
			block = block.replace("{icon}", skill != null ? skill.getTemplate().getIcon() : "icon.skill5041");
			block = block.replace("{color}", lastColor ? "222222" : "333333");
			block = block.replace("{price}", Util.formatPay(player, Config.BBS_FORGE_AUGMENT_COST_DATA[1], (int) Config.BBS_FORGE_AUGMENT_COST_DATA[0]));
			list += block;

			lastColor = !lastColor;
		}

		html.replace("%pages%", pagesHtm.toString());
		html.replace("%augs%", list);
		player.sendPacket(html);

	}
}
