package services;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.l2cccp.commons.lang.reference.HardReference;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.PremiumDAO;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.PremiumHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.actor.instances.player.Rate;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.premium.PremiumAccount;
import com.l2cccp.gameserver.model.premium.PremiumStart;
import com.l2cccp.gameserver.model.premium.PremiumVariant;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.network.l2.s2c.MagicSkillUse;
import com.l2cccp.gameserver.utils.HtmlUtils;
import com.l2cccp.gameserver.utils.Language;
import com.l2cccp.gameserver.utils.Log;
import com.l2cccp.gameserver.utils.TimeUtils;
import com.l2cccp.gameserver.utils.Util;

public class Premium
{
	@Bypass("services.Premium:see")
	public void see(Player player, NpcInstance npc, String[] arg)
	{
		String html = HtmCache.getInstance().getHtml("scripts/services/Premium/index.htm", player);
		String button = null;
		String template = HtmCache.getInstance().getHtml("scripts/services/Premium/button_index.htm", player);
		String block = null; // Для избежания повторного подгруза шаблона делаем отдельную переменную для присвоения.
		for(PremiumAccount premium : PremiumHolder.getInstance().getAllPremiums())
		{
			block = template;
			block = block.replace("{name}", premium.getName());
			block = block.replace("{icon}", premium.getIcon());
			block = block.replace("{variants}", String.valueOf(premium.getVariants().size()));
			block = block.replace("{link}", "bypass -h htmbypass_services.Premium:list " + premium.getId());
			button += block;
		}

		html = html.replace("{body}", button);

		HtmlMessage parse = new HtmlMessage(5).setHtml(HtmlUtils.bbParse(html));
		player.sendPacket(parse);
	}

	@Bypass("services.Premium:list")
	public void list(Player player, NpcInstance npc, String[] arg)
	{
		int val = Util.isNumber(arg[0]) ? Integer.parseInt(arg[0]) : 1;
		String html = HtmCache.getInstance().getHtml("scripts/services/Premium/index.htm", player);
		String button = null;
		String template = HtmCache.getInstance().getHtml("scripts/services/Premium/button.htm", player);
		String block = null; // Для избежания повторного подгруза шаблона делаем отдельную переменную для присвоения.
		final PremiumAccount premium = PremiumHolder.getInstance().getPremium(val);
		for(final PremiumVariant variant : premium.getVariants().values())
		{
			block = template;
			block = block.replace("{name}", premium.getName());
			block = block.replace("{icon}", premium.getIcon());
			block = block.replace("{time}", TimeUtils.formatTime((int) (variant.getTime() / 1000L), player.getLanguage()));
			block = block.replace("{price}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, variant.getPriceCount(), variant.getPriceId())).toString(player));
			block = block.replace("{link}", "bypass -h htmbypass_services.Premium:info " + premium.getId() + " " + variant.getId());
			button += block;
		}

		html = html.replace("{body}", button);

		HtmlMessage parse = new HtmlMessage(5).setHtml(HtmlUtils.bbParse(html));
		player.sendPacket(parse);
	}

	@Bypass("services.Premium:info")
	public void info(Player player, NpcInstance npc, String[] arg)
	{
		String html = HtmCache.getInstance().getHtml("scripts/services/Premium/info.htm", player);

		final int val = Util.isNumber(arg[0]) ? Integer.parseInt(arg[0]) : 1;
		final int var = Util.isNumber(arg[1]) ? Integer.parseInt(arg[1]) : 1;
		final PremiumAccount premium = PremiumHolder.getInstance().getPremium(val);
		final PremiumVariant variant = premium.getVariant(var);
		html = html.replace("{id}", String.valueOf(premium.getId()));
		html = html.replace("{variant}", String.valueOf(variant.getId()));
		html = html.replace("{name}", premium.getName());
		html = html.replace("{icon}", premium.getIcon());
		html = html.replace("{price}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, variant.getPriceCount(), variant.getPriceId())).toString(player));

		html = html.replace("{xp}", "+" + Util.cutOff(premium.getExp() * 100, 0) + "%");
		html = html.replace("{sp}", "+" + Util.cutOff(premium.getSp() * 100, 0) + "%");
		html = html.replace("{adena}", "+" + Util.cutOff(premium.getAdena() * 100, 0) + "%");
		html = html.replace("{items}", "+" + Util.cutOff(premium.getItems() * 100, 0) + "%");
		html = html.replace("{spoil}", "+" + Util.cutOff(premium.getSpoil() * 100, 0) + "%");
		html = html.replace("{epaulette}", "+" + Util.cutOff(premium.getEpaulette() * 100, 0) + "%");
		html = html.replace("{weight}", "+" + Util.cutOff(premium.getWeight() * 100, 0) + "%");
		html = html.replace("{masterwork}", "+" + premium.getMasterWorkChance() + "%");
		html = html.replace("{craft}", "+" + premium.getCraftChance() + "%");

		Rate rate = player.getRate();
		html = html.replace("{xp_f}", String.valueOf(Util.cutOff(rate.getExp() * premium.getExp(), 2)));
		html = html.replace("{sp_f}", String.valueOf(Util.cutOff(rate.getSp() * premium.getSp(), 2)));
		html = html.replace("{adena_f}", String.valueOf(Util.cutOff(rate.getAdena() * premium.getAdena(), 2)));
		html = html.replace("{items_f}", String.valueOf(Util.cutOff(rate.getItems() * premium.getItems(), 2)));
		html = html.replace("{spoil_f}", String.valueOf(Util.cutOff(rate.getSpoil() * premium.getSpoil(), 2)));
		html = html.replace("{epaulette_f}", String.valueOf(Util.cutOff(Config.RATE_DROP_SIEGE_GUARD * premium.getEpaulette(), 2)));
		html = html.replace("{weight_f}", String.valueOf(Util.cutOff(player.getMaxLoad() * premium.getWeight(), 2)));
		html = html.replace("{masterwork_f}", String.valueOf(Util.cutOff(Config.CRAFT_MASTERWORK_CHANCE + premium.getMasterWorkChance(), 2)));

		html = html.replace("{time}", TimeUtils.formatTime((int) (variant.getTime() / 1000L), player.getLanguage()));

		HtmlMessage parse = new HtmlMessage(5).setHtml(HtmlUtils.bbParse(html));
		player.sendPacket(parse);
	}

	private static final DateFormat TIME_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");

	@Bypass("services.Premium:choice")
	public void choice(Player player, NpcInstance npc, String[] arg)
	{
		if(arg[0].isEmpty())
			return;

		final int val = Util.isNumber(arg[0]) ? Integer.parseInt(arg[0]) : 1;
		final int var = Util.isNumber(arg[1]) ? Integer.parseInt(arg[1]) : 1;
		final PremiumAccount premium = PremiumHolder.getInstance().getPremium(val);
		final PremiumVariant variant = premium.getVariant(var);

		if(Util.getPay(player, variant.getPriceId(), variant.getPriceCount(), true))
		{
			final long expire = player.getBonus().getExpire(premium) + variant.getTime();
			PremiumStart.getInstance().start(player, premium.getId(), expire);
			PremiumDAO.getInstance().insert(player, premium.getId(), expire);

			String html = HtmCache.getInstance().getHtml("scripts/services/Premium/buy.htm", player);
			final String end = TIME_FORMAT.format(new Date(expire));
			html = html.replace("{time}", TimeUtils.formatTime((int) (variant.getTime() / 1000L), player.getLanguage()));
			html = html.replace("{end}", end);

			HtmlMessage parse = new HtmlMessage(5).setHtml(HtmlUtils.bbParse(html));
			player.sendPacket(parse);

			player.broadcastPacket(new MagicSkillUse(player, player, 6463, 1, 0, 0));
			Log.service("Player " + player + " buy Premium-Account (id:" + premium.getId() + ", name: " + premium.getName() + ") at " + TimeUtils.formatTime((int) (variant.getTime() / 1000L), Language.ENGLISH) + ", Price: " + Util.formatAdena(variant.getPriceCount()) + " " + Util.getItemName(variant.getPriceId()) + " End: " + end + ".", this.getClass().getName());
		}
	}

	@Bypass("services.Premium:giftAsk")
	public void giftAsk(Player player, NpcInstance npc, String[] arg)
	{
		final Player target = Util.isNumber(arg[0]) ? GameObjectsStorage.getPlayer(Integer.parseInt(arg[0])) : null;

		if(target == null)
		{
			player.sendMessage(new CustomMessage("common.Error"));
			return;
		}

		final PremiumAccount premium = PremiumHolder.getInstance().getPremium(Config.PREMIUM_ACCOUNT_PARTY_GIFT_DATA[0]);
		final PremiumVariant variant = premium.getVariant(Config.PREMIUM_ACCOUNT_PARTY_GIFT_DATA[1]);
		final String msg = new CustomMessage("premium.partyGift").addString(premium.getName()).addString(TimeUtils.formatTime((int) (variant.getTime() / 1000L), player.getLanguage())).addString(target.getName()).addString(Util.formatPay(player, variant.getPriceCount(), variant.getPriceId())).toString(player);
		ConfirmDlg dlg = new ConfirmDlg(SystemMsg.S1, 60000).addString(msg);
		player.ask(dlg, new Ask(target));
	}

	private class Ask implements OnAnswerListener
	{
		private final HardReference<Player> target;

		protected Ask(final Player target)
		{
			this.target = target.getRef();
		}

		@Override
		public void sayYes(final Player player)
		{
			Player target = this.target.get();
			if(target == null || !player.isOnline() || target == null || !target.isOnline())
				return;

			gift(target, player);
		}

		@Override
		public void sayNo(final Player player)
		{}
	}

	public void gift(final Player player, final Player buyer)
	{
		final PremiumAccount premium = PremiumHolder.getInstance().getPremium(Config.PREMIUM_ACCOUNT_PARTY_GIFT_DATA[0]);
		final PremiumVariant variant = premium.getVariant(Config.PREMIUM_ACCOUNT_PARTY_GIFT_DATA[1]);

		if(Util.getPay(buyer, variant.getPriceId(), variant.getPriceCount(), true))
		{

			final long expire = player.getBonus().getExpire(premium) + variant.getTime();
			PremiumStart.getInstance().start(player, premium.getId(), expire);
			PremiumDAO.getInstance().insert(player, premium.getId(), expire);

			player.broadcastPacket(new MagicSkillUse(player, player, 6463, 1, 0, 0));
			Log.service("Player " + player + " buy gift Premium-Account id:" + premium.getId() + " at " + TimeUtils.formatTime((int) variant.getTime(), Language.ENGLISH) + ".", this.getClass().getName());
		}
	}
}