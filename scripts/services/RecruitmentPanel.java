package services;

import handler.bbs.Recruitment;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.AcademyRequestDAO;
import com.l2cccp.gameserver.dao.ClanDataDAO;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.handler.bbs.AcademiciansStorage;
import com.l2cccp.gameserver.handler.bbs.AcademyRequest;
import com.l2cccp.gameserver.handler.bbs.AcademyStorage;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.ClassId;
import com.l2cccp.gameserver.model.base.ClassType2;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.tables.ClanTable;
import com.l2cccp.gameserver.utils.Util;

public class RecruitmentPanel
{
	@Bypass("services.RecruitmentPanel:editDescription")
	public void editDescription(Player player, NpcInstance npc, String[] arg)
	{
		Clan clan = player.getClan();

		if(clan == null || clan.getLeader().getPlayer() != player)
			return;

		player.sendPacket(new HtmlMessage(5).setFile("scripts/services/RecruitmentPanel/edit.htm"));
	}

	@Bypass("services.RecruitmentPanel:doEdit")
	public void doEdit(Player player, NpcInstance npc, String[] arg)
	{
		Clan clan = player.getClan();
		String description = Util.ArrayToString(arg, 0);

		if(checkDescription(player, clan, description))
		{
			ClanDataDAO.getInstance().updateDescription(clan.getClanId(), description);
			clan.setDescription(description);
			Util.communityNextPage(player, "_bbsrecruitment:clan:id:" + clan.getClanId());
		}
	}

	@Bypass("services.RecruitmentPanel:addDescription")
	public void addDescription(Player player, NpcInstance npc, String[] arg)
	{
		Clan clan = player.getClan();

		if(clan == null || clan.getLeader().getPlayer() != player)
			return;

		player.sendPacket(new HtmlMessage(5).setFile("scripts/services/RecruitmentPanel/add.htm"));
	}

	@Bypass("services.RecruitmentPanel:doAdd")
	public void doAdd(Player player, NpcInstance npc, String[] arg)
	{
		Clan clan = player.getClan();
		String description = Util.ArrayToString(arg, 0);

		if(checkDescription(player, clan, description))
		{
			ClanDataDAO.getInstance().insertDescription(clan.getClanId(), description);
			clan.setDescription(description);
			Util.communityNextPage(player, "_bbsrecruitment:clan:id:" + clan.getClanId());
		}
	}

	private boolean checkDescription(Player player, Clan clan, String description)
	{
		if(player == null || clan == null || clan.getLeader().getPlayer() != player)
			return false;

		int min = Config.BBS_RECRUITMENT_CLAN_DESCRIPTION_MIN;
		if(description.length() < min)
		{
			player.sendMessage("Descriptions size must be minimum " + min + " symbols!");
			return false;
		}

		return true;
	}

	@Bypass("services.RecruitmentPanel:addRequest")
	public void addRequest(Player player, NpcInstance npc, String[] arg)
	{
		if(player.getClan() != null)
			return;
		if(arg[0].isEmpty() || !Util.isNumber(arg[0]))
			return;

		int clan = Integer.parseInt(arg[0]);

		if(clan == -1)
			return;

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/RecruitmentPanel/request.htm");
		html.replace("%clan%", String.valueOf(clan));

		player.sendPacket(html);
	}

	@Bypass("services.RecruitmentPanel:addAcademy")
	public void addAcademy(Player player, NpcInstance npc, String[] arg)
	{
		if(arg.length < 4)
		{
			if(!checkAcademy(player))
				return;

			HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/RecruitmentPanel/academy.htm");
			html.replace("%time%", parseTime());
			html.replace("%items%", parseItems());
			html.replace("%size%", parseSize(player));
			player.sendPacket(html);
		}
		else
		{
			if(!checkAcademy(player))
				return;

			int seats = Integer.parseInt(arg[0]);
			int time = Integer.parseInt(arg[1]);
			long price = Long.parseLong(arg[2].replace(",", ""));
			String item_name = Util.ArrayToString(arg, 3);
			int item = Recruitment.items.get(item_name);
			if(Util.getClanPay(player, item, (price * seats), true))
			{
				int id = player.getClan().getClanId();
				AcademyRequest request = new AcademyRequest(time, id, seats, price, item); // Создаем набор
				AcademyRequestDAO.getInstance().insert(request);
			}

			Util.communityNextPage(player, "_bbsrecruitment:list:academy:1");
		}
	}

	@Bypass("services.RecruitmentPanel:online")
	public void online(Player player, NpcInstance npc, String[] arg)
	{
		if(arg[0].isEmpty())
			return;

		int clan_id = Integer.parseInt(arg[0]);

		int page = 1;

		if(arg.length > 1)
			page = Integer.parseInt(arg[1]);

		Clan clan = ClanTable.getInstance().getClan(clan_id);

		if(clan != null)
		{
			HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/RecruitmentPanel/clan_online.htm");
			String template = HtmCache.getInstance().getHtml("scripts/services/RecruitmentPanel/clan_online_template.htm", player);
			String list = "";
			String data = "";

			int current = 1;
			int start = (page - 1) * 10;
			int end = Math.min(page * 10, clan.getOnlineMembers(0).size());
			for(int i = start; i < end; i++)
			{
				Player member = clan.getOnlineMembers(0).get(i);
				list = template;
				list = list.replace("<?name?>", member.getName());
				list = list.replace("<?level?>", String.valueOf(member.getLevel()));
				list = list.replace("<?color?>", (current % 2 == 0 ? "666666" : "999999"));
				ClassId classId = member.getClassId();
				list = list.replace("<?icon?>", getClanClassIcon(classId, classId.getType2()));
				String unity = member.getClan().getUnitName(member.getPledgeType());
				list = list.replace("<?unity?>", (unity.length() > 10 ? (unity.substring(0, 8) + "...") : unity));
				data += list;
				current++;
			}

			html.replace("%navigate%", parseNavigate(clan, page));
			html.replace("%data%", data);
			html.replace("%name%", clan.getName());
			html.replace("%count%", String.valueOf(clan.getOnlineMembers(0).size()));
			player.sendPacket(html);
		}
	}

	private String parseNavigate(Clan clan, int page)
	{
		StringBuilder pg = new StringBuilder();

		double size = clan.getOnlineMembers(0).size();
		double inpage = 10;

		if(size > inpage)
		{
			double max = Math.ceil(size / inpage);

			pg.append("<center><table width=25 border=0><tr>");
			int line = 1;

			for(int current = 1; current <= max; current++)
			{
				if(page == current)
					pg.append("<td width=25 align=center><button value=\"[").append(current).append("]\" width=38 height=25 back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\"></td>");
				else
					pg.append("<td width=25 align=center><button value=\"").append(current).append("\" action=\"bypass -h htmbypass_services.RecruitmentPanel:online " + clan.getClanId() + " ").append(current).append("\" width=28 height=25 back=\"L2UI_ct1.button_df_down\" fore=\"L2UI_ct1.button_df\"></td>");

				if(line == 22)
				{
					pg.append("</tr><tr>");
					line = 0;
				}
				line++;
			}

			pg.append("</tr></table></center>");
		}

		return pg.toString();
	}

	private String getClanClassIcon(ClassId classId, ClassType2 type)
	{
		final String deff = "L2UI_CH3.party_styleicon1_" + (classId.isMage() ? "2" : "1");
		if(type == null)
			return deff;

		String icon = "";
		switch(type)
		{
			case Enchanter:
				icon = "L2UI_CH3.party_styleicon5";
				break;
			case Healer:
				icon = "L2UI_CH3.party_styleicon6";
				break;
			case Knight:
				icon = "L2UI_CH3.party_styleicon3";
				break;
			case Rogue:
				icon = "L2UI_CH3.party_styleicon2";
				break;
			case Summoner:
				icon = "L2UI_CH3.party_styleicon7";
				break;
			case Warrior:
				icon = "L2UI_CH3.party_styleicon1";
				break;
			case Wizard:
				icon = "L2UI_CH3.party_styleicon5";
				break;
			default:
				icon = deff;
				break;
		}

		if(classId.getLevel() == 4)
			icon += "_3";

		return icon;
	}

	private boolean checkAcademy(Player player)
	{
		Clan clan;
		if((clan = player.getClan()) == null)
			return false;
		else if(clan.getSubUnit(Clan.SUBUNIT_ACADEMY) == null)
			return false;
		else if(clan.getUnitMembersSize(Clan.SUBUNIT_ACADEMY) >= 20)
			return false;
		else if(AcademyStorage.getInstance().getReguest(clan.getClanId()) != null)
			return false;

		return true;
	}

	@Bypass("services.RecruitmentPanel:sendRequest")
	public void sendRequest(Player player, NpcInstance npc, String[] arg)
	{
		if(player.getClan() != null)
			return;

		int clan = Integer.parseInt(arg[0]);
		String note = arg.length > 1 ? Util.ArrayToString(arg, 1) : "...";
		Util.communityNextPage(player, "_bbsrecruitment:invite:" + clan + " " + note);
	}

	@Bypass("services.RecruitmentPanel:removeAcademy")
	public void removeAcademy(Player player, NpcInstance npc, String[] arg)
	{
		Clan clan;

		if((clan = player.getClan()) == null)
			return;

		AcademyRequest request = AcademyStorage.getInstance().getReguest(clan.getClanId());

		if(request == null)
		{
			player.sendMessage("Request not found!");
			return;
		}

		if(!AcademiciansStorage.getInstance().clanCheck(clan.getClanId()))
		{
			clan.getWarehouse().addItem(request.getItem(), (request.getPrice() * request.getSeats()));
			AcademyRequestDAO.getInstance().delete(request.getClanId());
			AcademyStorage.getInstance().get().remove(request);
			AcademyStorage.getInstance().updateList();
			player.sendMessage("Your acceptance to the Academy was successfully deleted! Payment is sent to the warehouse!");
			Util.communityNextPage(player, "_bbsrecruitment:list:academy:1");
		}
		else
			player.sendMessage("You can not remove the acceptance to the Academy until all Academics do not finish passage!");
	}

	/**
	 * @return "20;19;18;17;16";
	 */
	private String parseSize(Player player)
	{
		String list = "";
		int count = 1;
		Clan clan = player.getClan();
		int max = clan.getSubPledgeLimit(Clan.SUBUNIT_ACADEMY);
		int size = player.getClan().getUnitMembersSize(Clan.SUBUNIT_ACADEMY);
		if(size < max)
		{
			for(int i = max; i > size; i--)
			{
				if(count > 1 && count <= max)
					list += ";";
				list += ((max + 1) - i);
				count++;
			}
		}
		else
			list = "0";

		return list;
	}

	/**
	 * @return "Adena;Coin of Luck";
	 */
	private String parseItems()
	{
		String items = "";
		int count = 1;
		int size = Recruitment.items.size();
		for(String item : Recruitment.items.keySet())
		{
			if(count > 1 && count <= size)
				items += ";";
			items += item;
			count++;
		}
		return items;
	}

	/**
	 * @return "1;2;3;4;5;6;7";
	 */
	private String parseTime()
	{
		String time = "";
		int count = 1;
		int size = Config.BBS_RECRUITMENT_TIME.length;
		for(int hour : Config.BBS_RECRUITMENT_TIME)
		{
			if(count > 1 && count <= size)
				time += ";";
			time += hour;
			count++;
		}
		return time;
	}

}
