package services;

import java.util.ArrayList;
import java.util.List;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Announcements;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.data.xml.holder.FortuneBoxHolder;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.model.items.ItemInstance.ItemLocation;
import com.l2cccp.gameserver.model.minigame.FortuneBox;
import com.l2cccp.gameserver.model.minigame.FortuneBoxReward;
import com.l2cccp.gameserver.model.minigame.Reward;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Log;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class FortuneGame
{
	@Bypass("fortune.index")
	public void index(Player player, NpcInstance npc, String[] arg)
	{
		if((arg[0].isEmpty() || !Util.isNumber(arg[0])))
			return;

		int page = Integer.parseInt(arg[0]);

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/FortuneGame/index.htm");
		String template = HtmCache.getInstance().getHtml("scripts/services/FortuneGame/template-box.htm", player);
		String block = "";
		String list = "";

		final int perpage = 6;
		int counter = 0;
		List<FortuneBox> _reward = FortuneBoxHolder.getInstance().getAllBox();
		for(int i = (page - 1) * perpage; i < _reward.size(); i++)
		{
			FortuneBox box = _reward.get(i);
			if(box != null)
			{
				block = template;

				block = block.replace("{bypass}", "bypass -h htmbypass_fortune.box.page " + box.getId() + " 1");
				block = block.replace("{name}", box.getName());
				block = block.replace("{price}", Util.formatPay(player, box.getPriceCount(), box.getPriceId()));
				list += block;
			}

			counter++;

			if(counter >= perpage)
				break;
		}

		double count = Math.ceil((double) _reward.size() / (double) perpage);
		int inline = 1;
		String navigation = "";

		if(count > 1)
		{
			for(int i = 1; i <= count; i++)
			{
				if(i == page)
					navigation += "<td width=25 align=center valign=top><button value=\"[" + i + "]\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";
				else
					navigation += "<td width=25 align=center valign=top><button value=\"" + i + "\" action=\"bypass -h htmbypass_fortune.index " + i + "\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";

				if(inline % 7 == 0)
					navigation += "</tr><tr>";

				inline++;
			}
		}

		if(navigation.equals(""))
			navigation = "<td width=30 align=center valign=top>...</td>";

		html = html.replace("%list%", list);
		html = html.replace("%navigation%", navigation);

		player.sendPacket(html);
	}

	@Bypass("fortune.play")
	public void play(Player player, NpcInstance npc, String[] arg)
	{
		FortuneBoxHolder holder = FortuneBoxHolder.getInstance();
		if(!holder.isEnable())
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(!holder.canPlay(player.getLevel()))
		{
			player.sendMessage("Level should be between " + holder.getMin() + " and " + holder.getMax());
			return;
		}

		if(!Util.isNumber(arg[0]))
			return;

		int id = Integer.parseInt(arg[0]);

		FortuneBox box = holder.getBox(id);

		if(box != null)
		{
			if(Util.getPay(player, box.getPriceId(), box.getPriceCount(), true))
			{
				int take = 0;
				int max = box.getMaxRewards();
				List<Reward> list = new ArrayList<Reward>();
				for(FortuneBoxReward box_reward : box.getRewards())
				{
					if(take >= max)
						break;

					if(Rnd.chance(box_reward.getChance()))
					{
						take++;
						long count = 1;
						int enchant = 0;
						ItemInstance item = ItemFunctions.createItem(box_reward.getId());

						count = Rnd.get(box_reward.getCount()[0], box_reward.getCount()[1]);
						if(!item.isStackable())
						{
							if(box_reward.getEnchantData() != null)
							{
								if(Rnd.chance(box_reward.getEnchantData()[2]))
								{
									enchant = Rnd.get((int) box_reward.getEnchantData()[0], (int) box_reward.getEnchantData()[1]);
									item.setEnchantLevel(enchant);
								}
							}

							if(count > 1)
								count = 1;
						}

						item.setCount(count);
						item.setLocation(ItemLocation.INVENTORY);
						if(count > 0)
						{
							Reward reward = new Reward();
							reward.setId(item.getItemId());
							reward.setName(item.getName());
							reward.setIcon(item.getTemplate().getIcon());
							reward.setCount(count);
							reward.setEnchantLevel(enchant);

							list.add(reward);

							player.getInventory().addItem(item);
							player.sendPacket(SystemMessage.obtainItems(item.getItemId(), item.isStackable() ? count : 1, enchant));

							if(box_reward.doAnnounce())
								Announcements.getInstance().announceToAll("Player " + player.getName() + " win " + reward.toString() + " in " + box.getName(), "Fortune box");
						}
						else
							item.deleteMe();
					}
				}

				if(take > 0)
					sendPage(player, id, list);
				else
				{
					HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/FortuneGame/lose.htm");
					html = html.replace("%id%", String.valueOf(id));
					player.sendPacket(html);
				}

				if(holder.doLog())
					doLog(box, player, list);
			}
		}
		else
			System.out.println("Can't find game " + id);
	}

	private void doLog(FortuneBox box, Player player, List<Reward> list)
	{
		String rewards = " ";

		for(Reward reward : list)
		{
			if(reward.getEnchantLevel() > 0)
				rewards += "+" + reward.getEnchantLevel() + " ";
			rewards += reward.getName() + " [id:" + reward.getId() + "]";
			rewards += " (" + Util.formatAdena(reward.getCount()) + ") ";
		}

		Log.service("Player " + player + " played the game Fortune Box (id:" + box.getId() + ", name: " + box.getName() + ", cost: " + Util.formatPay(player, box.getPriceCount(), box.getPriceId()) + ") Result: " + (list.size() > 0 ? "WIN, Rewards -> [" + rewards + "]" : "LOSE") + ".", this.getClass().getName());
	}

	private void sendPage(Player player, int id, List<Reward> _list)
	{
		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/FortuneGame/win.htm");
		String template = HtmCache.getInstance().getHtml("scripts/services/FortuneGame/template-win.htm", player);
		String block = "";
		String list = "";
		for(Reward item : _list)
		{
			if(item != null)
			{
				block = template;

				String name = "";

				if(item.getEnchantLevel() > 0)
					name = "+" + item.getEnchantLevel() + " ";

				name += item.getName();

				if(name.length() > 29)
					name = name.substring(0, 29) + "...";

				block = block.replace("{icon}", item.getIcon());
				block = block.replace("{name}", name);
				block = block.replace("{count}", Util.formatAdena(item.getCount()));
				list += block;
			}
		}

		html = html.replace("%list%", list);
		html = html.replace("%id%", String.valueOf(id));
		player.sendPacket(html);
	}

	@Bypass("fortune.box.page")
	public void page(Player player, NpcInstance npc, String[] arg)
	{
		if((arg[0].isEmpty() || !Util.isNumber(arg[0])) || (arg[1].isEmpty() || !Util.isNumber(arg[1])))
			return;

		int id = Integer.parseInt(arg[0]);
		int page = Integer.parseInt(arg[1]);

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/FortuneGame/box.htm");
		String block = "";
		String list = "";
		FortuneBox box = FortuneBoxHolder.getInstance().getBox(id);
		final int perpage = 5;
		int counter = 0;
		List<FortuneBoxReward> _reward = box.getRewards();
		for(int i = (page - 1) * perpage; i < _reward.size(); i++)
		{
			FortuneBoxReward reward = _reward.get(i);
			if(reward != null)
			{
				String name = Util.getItemName(reward.getId());

				if(name.length() > 37)
					name = name.substring(0, 37) + "...";

				ItemTemplate it = ItemHolder.getInstance().getTemplate(reward.getId());

				if(!it.isStackable())
				{
					if(reward.getEnchantData() != null)
					{
						block = HtmCache.getInstance().getHtml("scripts/services/FortuneGame/template-reward-enchant.htm", player);
						block = block.replace("{emin}", String.valueOf((int) reward.getEnchantData()[0]));
						block = block.replace("{emax}", String.valueOf((int) reward.getEnchantData()[1]));
					}
					else
					{
						block = HtmCache.getInstance().getHtml("scripts/services/FortuneGame/template-reward.htm", player);
						block = block.replace("{count}", "1");
					}
				}
				else
				{
					long min = reward.getCount()[0];
					long max = reward.getCount()[1];
					if(min != max)
					{
						block = HtmCache.getInstance().getHtml("scripts/services/FortuneGame/template-reward-random.htm", player);
						block = block.replace("{min}", Util.formatAdena(min));
						block = block.replace("{max}", Util.formatAdena(max));
					}
					else
					{
						block = HtmCache.getInstance().getHtml("scripts/services/FortuneGame/template-reward.htm", player);
						block = block.replace("{count}", Util.formatAdena(reward.getCount()[0]));
					}
				}

				block = block.replace("{name}", name);
				block = block.replace("{icon}", it.getIcon());
				list += block;
			}

			counter++;

			if(counter >= perpage)
				break;
		}

		double count = Math.ceil((double) _reward.size() / (double) perpage);
		int inline = 1;
		String navigation = "";
		if(count > 1)
		{
			for(int i = 1; i <= count; i++)
			{
				if(i == page)
					navigation += "<td width=30 align=center valign=center><button value=\"[" + i + "]\" width=32 height=23 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";
				else
					navigation += "<td width=30 align=center valign=top><button value=\"" + i + "\" action=\"bypass -h htmbypass_fortune.box.page " + id + " " + i + "\" width=30 height=23 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";

				if(inline % 8 == 0)
					navigation += "</tr><tr>";

				inline++;
			}
		}

		if(navigation.equals(""))
			navigation = "<td width=30 align=center valign=top>...</td>";

		html = html.replace("%price%", Util.formatPay(player, box.getPriceCount(), box.getPriceId()));
		html = html.replace("%name%", box.getName());
		html = html.replace("%id%", String.valueOf(id));
		html = html.replace("%list%", list);
		html = html.replace("%navigation%", navigation);

		player.sendPacket(html);
	}
}
