package services.Body;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.templates.item.ItemTemplate;
import com.l2cccp.gameserver.utils.HtmlUtils;
import com.l2cccp.gameserver.utils.Util;

import handler.bbs.Cabinet;
import handler.bbs.ChangeVisualItemIdHelper;

/**
 * @author L2CCCP
 */
public class Peace
{
	@Bypass("services.Body.Peace:page")
	public void page(Player player, NpcInstance npc, String[] arg)
	{
		String page = arg[0];
		String html = HtmCache.getInstance().getHtml("scripts/services/Body/" + page + ".htm", player);

		if(page.equals("choice_my"))
		{
			html = html.replace("{armor}", Util.formatAdena(Config.BBS_BODY_CHANGE_ARMOR[0]) + " " + Util.getItemName(Config.BBS_BODY_CHANGE_ARMOR[1]));
			html = html.replace("{weapon}", Util.formatAdena(Config.BBS_BODY_CHANGE_WEAPON[0]) + " " + Util.getItemName(Config.BBS_BODY_CHANGE_WEAPON[1]));
			html = html.replace("{accessories}", Util.formatAdena(Config.BBS_BODY_CHANGE_ACCESSORIES[0]) + " " + Util.getItemName(Config.BBS_BODY_CHANGE_ACCESSORIES[1]));
		}
		else if(page.equals("choice_my_armor"))
			html = html.replace("{armor}", Util.formatAdena(Config.BBS_BODY_CHANGE_ARMOR[0]) + " " + Util.getItemName(Config.BBS_BODY_CHANGE_ARMOR[1]));
		else if(page.equals("accessories"))
			html = html.replace("{accessories}", Util.formatAdena(Config.BBS_BODY_CHANGE_ACCESSORIES[0]) + " " + Util.getItemName(Config.BBS_BODY_CHANGE_ACCESSORIES[1]));
		else if(page.startsWith("choice_remove"))
			html = html.replace("{remove}", Util.formatAdena(Config.BBS_BODY_REMOVE_ITEM[0]) + " " + Util.getItemName(Config.BBS_BODY_REMOVE_ITEM[1]));

		HtmlMessage parse = new HtmlMessage(5).setHtml(HtmlUtils.bbParse(html));
		player.sendPacket(parse);
	}

	@Bypass("services.Body.Peace:mylist")
	public void mylist(Player player, NpcInstance npc, String[] arg)
	{
		String html = HtmCache.getInstance().getHtml("scripts/services/Body/index.htm", player);
		String button = null;
		String template = HtmCache.getInstance().getHtml("scripts/services/Body/list.htm", player);
		String block = null;
		String type = arg[0];
		for(ItemInstance item : player.getInventory().getItems())
		{
			if(type.equals("weapon") && !item.getTemplate().isWeapon())
				continue;
			else if(type.equals("shield") && (item.getTemplate().getBodyPart() != ItemTemplate.SLOT_L_HAND || item.getTemplate().isArrow()))
				continue;
			else if(type.equals("cloak") && !item.getTemplate().isCloak())
				continue;
			else if(type.equals("helmet") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_HEAD)
				continue;
			else if(type.equals("chest") && (item.getTemplate().getBodyPart() != ItemTemplate.SLOT_CHEST && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_FULL_ARMOR))
				continue;
			else if(type.equals("legs") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_LEGS)
				continue;
			else if(type.equals("gloves") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_GLOVES)
				continue;
			else if(type.equals("feet") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_FEET)
				continue;
			else if(type.equals("hair") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_HAIR)
				continue;
			else if(type.equals("dhair") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_DHAIR)
				continue;
			else if(type.equals("hairall") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_HAIRALL)
				continue;
			else if(item.isTemporalItem())
				continue;
			else if(item.isHeroWeapon() && !Config.BBS_BODY_VISUAL_HERO)
				continue;

			if(Cabinet.getInstance().check(player, item.getObjectId(), false) && item.getVisualItemId() == 0)
			{
				block = template;
				block = block.replace("{icon}", item.getTemplate().getIcon());
				String name = item.getName() + (item.getEnchantLevel() > 0 ? " +" + item.getEnchantLevel() : "") + (item.getTemplate().getAdditionalName().length() > 0 ? " - " + item.getTemplate().getAdditionalName() : "");
				block = block.replace("{name}", name.length() >= 30 ? name.substring(0, 29) : name);
				block = block.replace("{link}", "bypass -h htmbypass_services.Body.Peace:my " + item.getObjectId());
				block = block.replace("{type}", Cabinet.getInstance().getBodyPart(player, item));
				button += block;
			}
		}

		if(button == null)
			html = HtmCache.getInstance().getHtml("scripts/services/Body/empty.htm", player);
		else
			html = html.replace("{body}", button);

		HtmlMessage parse = new HtmlMessage(5).setHtml(HtmlUtils.bbParse(html));
		player.sendPacket(parse);
	}

	@Bypass("services.Body.Peace:visuallist")
	public void visuallist(Player player, NpcInstance npc, String[] arg)
	{
		if(player.getMyPeace() == 0)
		{
			String html = HtmCache.getInstance().getHtml("scripts/services/Body/error.htm", player);
			HtmlMessage parse = new HtmlMessage(5).setHtml(HtmlUtils.bbParse(html));
			player.sendPacket(parse);
			return;
		}

		String html = HtmCache.getInstance().getHtml("scripts/services/Body/index.htm", player);
		String button = null;
		String template = HtmCache.getInstance().getHtml("scripts/services/Body/list.htm", player);
		String block = null;
		ItemInstance my = player.getInventory().getItemByObjectId(player.getMyPeace());

		if(my == null)
			return;

		for(ItemInstance item : player.getInventory().getItems())
		{
			if(item.isTemporalItem())
				continue;
			else if(item.isHeroWeapon() && !Config.BBS_BODY_VISUAL_TO_HERO)
				continue;

			if(Cabinet.getInstance().check(player, item.getObjectId(), false) && item.getVisualItemId() == 0 && ChangeVisualItemIdHelper.isValid(my.getTemplate(), item.getTemplate()) && item.getItemId() != my.getItemId() && (item.getItemType() == my.getItemType() || item.getTemplate().getBodyPart() == ItemTemplate.SLOT_COSTUME))
			{
				block = template;
				block = block.replace("{icon}", item.getTemplate().getIcon());
				String name = item.getName() + (item.getEnchantLevel() > 0 ? " +" + item.getEnchantLevel() : "") + (item.getTemplate().getAdditionalName().length() > 0 ? " - " + item.getTemplate().getAdditionalName() : "");
				block = block.replace("{name}", name.length() >= 30 ? name.substring(0, 29) : name);
				block = block.replace("{link}", "bypass -h htmbypass_services.Body.Peace:visual " + item.getObjectId());
				block = block.replace("{type}", Cabinet.getInstance().getBodyPart(player, item));
				button += block;
			}
		}

		if(button == null)
			html = HtmCache.getInstance().getHtml("scripts/services/Body/empty.htm", player);
		else
			html = html.replace("{body}", button);

		HtmlMessage parse = new HtmlMessage(5).setHtml(HtmlUtils.bbParse(html));
		player.sendPacket(parse);
	}

	@Bypass("services.Body.Peace:removelist")
	public void removelist(Player player, NpcInstance npc, String[] arg)
	{
		String html = HtmCache.getInstance().getHtml("scripts/services/Body/index.htm", player);
		String button = null;
		String template = HtmCache.getInstance().getHtml("scripts/services/Body/list.htm", player);
		String block = null;
		String type = arg[0];
		for(ItemInstance item : player.getInventory().getItems())
		{
			if(type.equals("weapon") && !item.getTemplate().isWeapon())
				continue;
			else if(type.equals("shield") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_L_HAND)
				continue;
			else if(type.equals("cloak") && !item.getTemplate().isCloak())
				continue;
			else if(type.equals("helmet") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_HEAD)
				continue;
			else if(type.equals("chest") && (item.getTemplate().getBodyPart() != ItemTemplate.SLOT_CHEST && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_FULL_ARMOR))
				continue;
			else if(type.equals("legs") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_LEGS)
				continue;
			else if(type.equals("gloves") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_GLOVES)
				continue;
			else if(type.equals("feet") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_FEET)
				continue;
			else if(type.equals("hair") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_HAIR)
				continue;
			else if(type.equals("dhair") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_DHAIR)
				continue;
			else if(type.equals("hairall") && item.getTemplate().getBodyPart() != ItemTemplate.SLOT_HAIRALL)
				continue;

			if(Cabinet.getInstance().check(player, item.getObjectId(), true) && item.getVisualItemId() != 0)
			{
				block = template;
				block = block.replace("{icon}", item.getTemplate().getIcon());
				String name = item.getName() + (item.getEnchantLevel() > 0 ? " +" + item.getEnchantLevel() : "") + (item.getTemplate().getAdditionalName().length() > 0 ? " - " + item.getTemplate().getAdditionalName() : "");
				block = block.replace("{name}", name.length() >= 30 ? name.substring(0, 29) : name);
				block = block.replace("{link}", "bypass -h htmbypass_services.Body.Peace:remove " + item.getObjectId());
				block = block.replace("{type}", Cabinet.getInstance().getBodyPart(player, item));
				button += block;
			}
		}

		if(button == null)
			html = HtmCache.getInstance().getHtml("scripts/services/Body/empty.htm", player);
		else
			html = html.replace("{body}", button);

		HtmlMessage page = new HtmlMessage(5).setHtml(HtmlUtils.bbParse(html));
		player.sendPacket(page);
	}

	@Bypass("services.Body.Peace:my")
	public void my(Player player, NpcInstance npc, String[] arg)
	{
		if(arg[0].isEmpty())
			return;

		Util.communityNextPage(player, "_bbscabinet:body:set:my:" + arg[0] + ":peace");
	}

	@Bypass("services.Body.Peace:removemy")
	public void removemy(Player player, NpcInstance npc, String[] arg)
	{
		player.setMyPeace(0);
		player.setNewPeace(0);
		Util.communityNextPage(player, "_bbscabinet:body:show:peace");
	}

	@Bypass("services.Body.Peace:visual")
	public void visual(Player player, NpcInstance npc, String[] arg)
	{
		if(arg[0].isEmpty())
			return;

		Util.communityNextPage(player, "_bbscabinet:body:set:new:" + arg[0] + ":peace");
	}

	@Bypass("services.Body.Peace:removevisual")
	public void removevisual(Player player, NpcInstance npc, String[] arg)
	{
		player.setNewPeace(0);
		Util.communityNextPage(player, "_bbscabinet:body:show:peace");
	}

	@Bypass("services.Body.Peace:remove")
	public void remove(Player player, NpcInstance npc, String[] arg)
	{
		if(arg[0].isEmpty())
			return;

		int id = Integer.parseInt(arg[0]);
		ItemInstance my = player.getInventory().getItemByObjectId(id);

		if(my != null && my.getVisualItemId() != 0)
		{
			if(Util.getPay(player, Config.BBS_BODY_REMOVE_ITEM[1], Config.BBS_BODY_REMOVE_ITEM[0], true))
				ChangeVisualItemIdHelper.dropVisualItemId(player, my.getObjectId());
		}

		page(player, null, new String[] { "choice_remove" });
	}
}
