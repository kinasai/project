package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.dao.CharacterDAO;
import com.l2cccp.gameserver.data.client.holder.NpcNameLineHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.impl.SiegeEvent;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.utils.Log;
import com.l2cccp.gameserver.utils.Util;

public class NameChange
{
	@Bypass("services.NameChange:page")
	public void rename_page(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SERVICES_CHANGE_NICK_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		player.sendPacket(new HtmlMessage(5).setFile("scripts/services/NameChange/index.htm"));
	}

	@Bypass("services.NameChange:ask")
	public void ask(Player player, NpcInstance npc, String[] arg)
	{
		if(arg[0].isEmpty())
		{
			player.sendMessage(new CustomMessage("common.Error"));
			return;
		}
		String name = arg[0];

		String msg = new CustomMessage("services.NameChange.ask").addString(player.getName()).addString(name).addString(Util.formatPay(player, Config.SERVICES_CHANGE_NICK_PRICE, Config.SERVICES_CHANGE_NICK_ITEM)).toString(player);
		ConfirmDlg ask = new ConfirmDlg(SystemMsg.S1, 60000);
		ask.addString(msg);

		player.ask(ask, new AnswerListener(name));
	}

	private void rename(Player player, String name)
	{
		if(!Config.SERVICES_CHANGE_NICK_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}
		else if(name == null || name.isEmpty())
		{
			player.sendMessage(new CustomMessage("services.NameChange.empty"));
			return;
		}
		else if(player.isHero())
		{
			player.sendMessage(new CustomMessage("services.NameChange.hero"));
			return;
		}
		else if(player.isClanLeader())
		{
			player.sendMessage(new CustomMessage("services.NameChange.clanleader"));
			return;
		}
		else if(player.getEvent(SiegeEvent.class) != null)
		{
			player.sendMessage(new CustomMessage("services.NameChange.event"));
			return;
		}

		if(!Util.isMatchingRegexp(name, Config.SERVICES_CHANGE_NICK_TEMPLATE) || NpcNameLineHolder.getInstance().isBlackListContainsName(name))
		{
			player.sendMessage(new CustomMessage("services.NameChange.incorrect"));
			return;
		}
		else if(CharacterDAO.getInstance().getObjectIdByName(name) > 0)
		{
			player.sendMessage(new CustomMessage("services.NameChange.used"));
			return;
		}
		else if(Util.getPay(player, Config.SERVICES_CHANGE_NICK_ITEM, Config.SERVICES_CHANGE_NICK_PRICE, true))
		{
			String oldName = player.getName();
			player.reName(name, true);

			Log.add("Character " + oldName + " renamed to " + name, "NameChange");
			player.sendMessage(new CustomMessage("services.NameChange.done").addString(oldName).addString(name));
		}
	}

	private class AnswerListener implements OnAnswerListener
	{
		private String name;

		protected AnswerListener(String name)
		{
			this.name = name;
		}

		@Override
		public void sayYes(Player player)
		{
			if(!player.isOnline())
				return;

			rename(player, name);
		}

		@Override
		public void sayNo(Player player)
		{}
	}
}
