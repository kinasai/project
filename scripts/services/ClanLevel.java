package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.model.pledge.UnitMember;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.PledgeShowInfoUpdate;
import com.l2cccp.gameserver.network.l2.s2c.PledgeStatusChanged;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.SiegeUtils;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ClanLevel
{
	@Bypass("clanlevel:list")
	public void list(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SERVICES_CLAN_LEVEL_SELL_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		Clan clan = player.getClan();
		if(clan == null)
		{
			player.sendMessage("You must be in clan!");
			return;
		}
		else if(clan.getLevel() >= 11)
		{
			player.sendMessage("Your clan have max level!");
			return;
		}

		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/ClanLevel/index.htm");
		String template = HtmCache.getInstance().getHtml("scripts/services/ClanLevel/template.htm", player);
		String block = "";
		String list = "";

		for(int i = 0; i < Config.SERVICES_CLAN_LEVEL_SELL_DATA.length; i++)
		{
			final int lvl = i + 1;
			if(clan.getLevel() >= lvl)
				continue;

			block = template;
			block = block.replace("{level}", String.valueOf(lvl));
			final int item = (int) Config.SERVICES_CLAN_LEVEL_SELL_DATA[i][0];
			final long count = Config.SERVICES_CLAN_LEVEL_SELL_DATA[i][1];
			block = block.replace("{cost}", Util.formatPay(player, count, item));
			list += block;
		}

		html.replace("%list%", list);

		player.sendPacket(html);
	}

	@Bypass("clanlevel")
	public void clanlevel(Player player, NpcInstance npc, String[] arg)
	{
		if(!arg[0].isEmpty() && Util.isNumber(arg[0]) && check(player))
		{
			final int level = Integer.parseInt(arg[0]);
			levelUpClan(player, level);
		}
	}

	private boolean check(Player player)
	{
		if(!Config.SERVICES_CLAN_LEVEL_SELL_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return false;
		}
		else if(player.getClan() == null)
		{
			player.sendMessage("You must be in clan!");
			return false;
		}
		else if(!player.isClanLeader())
		{
			player.sendMessage("You must be clan Leader!");
			return false;
		}
		else
			return true;
	}

	private void levelUpClan(Player player, int level)
	{
		Clan clan = player.getClan();
		if(clan.isPlacedForDisband())
		{
			player.sendPacket(SystemMsg.AS_YOU_ARE_CURRENTLY_SCHEDULE_FOR_CLAN_DISSOLUTION_YOUR_CLAN_LEVEL_CANNOT_BE_INCREASED);
			return;
		}
		else if(clan.getLevel() >= level)
			return;

		final int num = level - 1;
		final int item = (int) Config.SERVICES_CLAN_LEVEL_SELL_DATA[num][0];
		final long cost = Config.SERVICES_CLAN_LEVEL_SELL_DATA[num][1];
		if(Util.getPay(player, item, cost, true))
			increaseClanLevel(clan, level, player);
	}

	private void increaseClanLevel(Clan clan, int level, Player player)
	{
		clan.setLevel(level);
		clan.updateClanInDB();

		player.broadcastCharInfo();

		player.doCast(SkillTable.getInstance().getSkillEntry(5103, 1), player, true);

		if(clan.getLevel() >= SiegeUtils.MIN_CLAN_SIEGE_LEVEL)
			SiegeUtils.addSiegeSkills(player);

		if(clan.getLevel() == 5)
			player.sendPacket(SystemMsg.NOW_THAT_YOUR_CLAN_LEVEL_IS_ABOVE_LEVEL_5_IT_CAN_ACCUMULATE_CLAN_REPUTATION_POINTS);

		PledgeShowInfoUpdate pu = new PledgeShowInfoUpdate(clan);
		PledgeStatusChanged ps = new PledgeStatusChanged(clan);
		for(UnitMember mbr : clan)
		{
			if(mbr.isOnline())
			{
				mbr.getPlayer().updatePledgeClass();
				mbr.getPlayer().sendPacket(SystemMsg.YOUR_CLANS_LEVEL_HAS_INCREASED, pu, ps);
				mbr.getPlayer().broadcastCharInfo();
			}
		}
	}
}