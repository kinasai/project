package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;

public class TeleToCatacomb
{
	@Bypass("services.TeleToCatacomb:list")
	public void list(Player player, NpcInstance npc, String[] var)
	{
		if(!Config.ALT_TELE_TO_CATACOMBS)
			return;

		HtmlMessage message = new HtmlMessage(5);
		message.setFile(player.getLevel() <= Config.GATEKEEPER_FREE ? "scripts/services/tele_catacomb_free.htm" : "scripts/services/tele_catacomb.htm");
		player.sendPacket(message);
	}
}