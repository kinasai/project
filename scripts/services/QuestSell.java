package services;

import gnu.trove.list.array.TIntArrayList;
import gnu.trove.list.array.TLongArrayList;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.instancemanager.QuestManager;
import com.l2cccp.gameserver.listener.actor.player.OnAnswerListener;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.quest.Quest;
import com.l2cccp.gameserver.model.quest.QuestState;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ConfirmDlg;
import com.l2cccp.gameserver.utils.Util;

public class QuestSell implements OnInitScriptListener
{
	public static final Logger _log = LoggerFactory.getLogger(QuestSell.class);
	private static int[] quest_id = {};
	private static int[] quest_item = {};
	private static long[] quest_price = {};

	@Bypass("services.QuestSell:ask")
	public void ask(Player player, NpcInstance npc, String[] arg)
	{
		int id = 0;
		if(arg[0].isEmpty() || player == null)
		{
			player.sendMessage(new CustomMessage("common.Error"));
			return;
		}

		try
		{
			id = Integer.parseInt(arg[0]);
		}
		catch(Exception e)
		{
			_log.error("QuestSell Player: " + player.getName() + " param: " + arg[0], e);
			return;
		}

		int item = Config.QUEST_SELL_PRICE_FOR_ALL[0];
		long count = Config.QUEST_SELL_PRICE_FOR_ALL[1];

		for(int i = 0; i < quest_id.length; i++)
		{
			if(quest_id[i] == id)
			{
				item = quest_item[i];
				count = quest_price[i];
			}
		}

		Quest q = QuestManager.getQuest(id);
		if(q != null)
		{
			String quest = q.getName(player.getLanguage());
			ConfirmDlg dlg = new ConfirmDlg(SystemMsg.S1, 60000).addString(new CustomMessage("quest.sell.ask").addString(quest).addString(Util.formatPay(player, count, item)).toString(player));
			player.ask(dlg, new Ask(id));
		}
		else
			player.sendMessage(new CustomMessage("quest.sell.no.id"));
	}

	public void sell(Player player, int id)
	{
		if(player == null)
			return;

		if(!Config.QUEST_SELL_ENABLE)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}
		else if(!ArrayUtils.contains(Config.QUEST_SELL_ID_ENABLE, id))
		{
			player.sendMessage(new CustomMessage("quest.sell.prohibited"));
			return;
		}

		int item = Config.QUEST_SELL_PRICE_FOR_ALL[0];
		long count = Config.QUEST_SELL_PRICE_FOR_ALL[1];

		for(int i = 0; i < quest_id.length; i++)
		{
			if(quest_id[i] == id)
			{
				item = quest_item[i];
				count = quest_price[i];
			}
		}

		Quest q = QuestManager.getQuest(id);

		if(q != null)
		{
			QuestState qs = player.getQuestState(id);

			boolean notnull = qs != null;

			if(notnull && qs.getState() == Quest.COMPLETED)
			{
				player.sendMessage(new CustomMessage("quest.sell.completed").addString(q.getName(player.getLanguage())));
				return;
			}
			else if(!Util.getPay(player, item, count, true))
				return;

			if(notnull)
				qs.exitCurrentQuest(true);

			q.newQuestState(player, Quest.COMPLETED);
			player.sendMessage(new CustomMessage("quest.sell.done").addString(q.getName(player.getLanguage())).addString(Util.formatPay(player, count, item)));
		}
		else
			player.sendMessage(new CustomMessage("quest.sell.no.id"));
	}

	private class Ask implements OnAnswerListener
	{
		private int id;

		protected Ask(int id)
		{
			this.id = id;
		}

		@Override
		public void sayYes(Player player)
		{
			if(player == null || !player.isOnline())
				return;

			sell(player, id);
		}

		@Override
		public void sayNo(Player player)
		{}
	}

	@Override
	public void onInit()
	{
		_log.info("Loaded Service: Quest Sell");

		TIntArrayList _quest = new TIntArrayList(), _quest_item = new TIntArrayList();
		TLongArrayList _quest_price = new TLongArrayList();

		for(String quest : Config.QUEST_SELL_PRICE_UNIQUE)
		{
			_quest.add(Integer.parseInt(quest.split(";")[0].split(":")[0]));
			_quest_item.add(Integer.parseInt(quest.split(";")[0].split(":")[1].split("-")[0]));
			_quest_price.add(Long.parseLong(quest.split(";")[0].split(":")[1].split("-")[1]));
		}

		quest_id = _quest.toArray();
		quest_item = _quest_item.toArray();
		quest_price = _quest_price.toArray();
	}
}