package services;

import com.l2cccp.gameserver.data.xml.holder.MultiSellHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Util;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Window
{
	@Bypass("window")
	public void window(Player player, NpcInstance npc, String[] arg)
	{
		if(arg.length > 1)
			Util.communityNextPage(player, arg[1]);

		HtmlMessage window = new HtmlMessage(5).setFile("scripts/window/" + arg[0] + ".htm");
		player.sendPacket(window);
		return;
	}

	@Bypass("shop")
	public void shop(Player player, NpcInstance npc, String[] arg)
	{
		final int id = Integer.parseInt(arg[0]);

		if(arg.length > 2)
			Util.communityNextPage(player, arg[2]);

		HtmlMessage window = new HtmlMessage(5).setFile("scripts/window/" + arg[1] + ".htm");
		player.sendPacket(window);
		MultiSellHolder.getInstance().SeparateAndSend(id, player, -1, 0);
		return;
	}
}