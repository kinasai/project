package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.utils.Util;

public class WashSins
{
	@Bypass("services.WashSins:clear")
	public void wash(Player player, NpcInstance npc, String[] arg)
	{
		if(Config.SERVICES_WASH_SINS_PRICE_ITEM_ID == -1)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(player.getKarma() == 0)
		{
			player.sendMessage("You don't have karma");
			return;
		}
		else if(player.isDead() || player.isAlikeDead() || player.isCastingNow() || player.isInCombat() || player.isAttackingNow() || player.isFlying())
		{
			player.sendMessage(new CustomMessage("common.Error"));
			return;
		}
		else
		{
			if(Util.getPay(player, Config.SERVICES_WASH_SINS_PRICE_ITEM_ID, Config.SERVICES_WASH_SINS_PRICE, true))
			{
				player.setKarma(0, true);

				CustomMessage msg = new CustomMessage("wash.sins");
				player.sendPacket(new ExShowScreenMessage(msg.toString(player), 3000, ScreenMessageAlign.TOP_CENTER, true));
				player.sendMessage(msg);
			}
		}
	}
}
