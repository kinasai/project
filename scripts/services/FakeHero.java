package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.Hero;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.network.l2.s2c.SocialAction;
import com.l2cccp.gameserver.utils.DeclensionKey;
import com.l2cccp.gameserver.utils.Util;

public class FakeHero
{
	private final int[] ITEM = Config.SERVICES_HERO_SELL_ITEM;
	private final int[] PRICE = Config.SERVICES_HERO_SELL_PRICE;
	private final int[] DAY = Config.SERVICES_HERO_SELL_DAY;

	@Bypass("services.FakeHero:list")
	public void list(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SERVICES_HERO_SELL_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		HtmlMessage html = null;
		if(!player.isHero() && !player.isFakeHero())
		{
			html = new HtmlMessage(5).setFile("scripts/services/FakeHero/index.htm");
			String template = HtmCache.getInstance().getHtml("scripts/services/FakeHero/template.htm", player);
			String block = "";
			String list = "";

			final int perpage = 6;
			final int page = arg[0].length() > 0 ? Integer.parseInt(arg[0]) : 1;
			int counter = 0;
			for(int i = (page - 1) * perpage; i < DAY.length; i++)
			{
				block = template;
				block = block.replace("{bypass}", "bypass -h htmbypass_services.FakeHero:buy " + i);
				block = block.replace("{info}", DAY[i] + " " + Util.declension(player.getLanguage(), (int) DAY[i], DeclensionKey.DAYS));
				block = block.replace("{cost}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, PRICE[i], ITEM[i])).toString(player));
				list += block;

				counter++;

				if(counter >= perpage)
					break;
			}

			double count = Math.ceil((double) DAY.length / (double) perpage); // Используем округление для получения последней страницы с остатком!
			int inline = 1;
			String navigation = "";
			for(int i = 1; i <= count; i++)
			{
				if(i == page)
					navigation += "<td width=25 align=center valign=top><button value=\"[" + i + "]\" action=\"bypass -h htmbypass_services.FakeHero:list " + i + "\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";
				else
					navigation += "<td width=25 align=center valign=top><button value=\"" + i + "\" action=\"bypass -h htmbypass_services.FakeHero:list " + i + "\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";

				if(inline % 7 == 0)
					navigation += "</tr><tr>";

				inline++;
			}

			if(inline == 2)
				navigation = "<td width=30 align=center valign=top>...</td>";

			html.replace("%list%", list);
			html.replace("%navigation%", navigation);
		}
		else
		{
			html = new HtmlMessage(5).setFile("scripts/services/FakeHero/already.htm");
			player.sendMessage(new CustomMessage("scripts.services.FakeHero.ishero"));
		}

		player.sendPacket(html);
	}

	@Bypass("services.FakeHero:buy")
	public void buy(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SERVICES_HERO_SELL_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(arg[0].isEmpty() || player == null)
			return;

		int i = Integer.parseInt(arg[0]);

		if(i > Config.SERVICES_HERO_SELL_DAY.length)
		{
			player.sendMessage(new CustomMessage("common.Error"));
			return;
		}

		if(player.isHero() || player.isFakeHero())
		{
			player.sendMessage(new CustomMessage("scripts.services.FakeHero.ishero"));
			return;
		}

		if(Util.getPay(player, ITEM[i], PRICE[i], true))
		{
			long day = Util.addDay(DAY[i]);
			long time = System.currentTimeMillis() + day;
			try
			{
				player.setVar("hasFakeHero", 1, time);
				player.sendChanges();
				player.broadcastCharInfo();

				if(Config.SERVICES_HERO_SELL_SKILL)
					Hero.addSkills(player);

				player.getPlayer().broadcastPacket(new SocialAction(player.getPlayer().getObjectId(), SocialAction.GIVE_HERO));
				player.sendMessage(new CustomMessage("scripts.services.FakeHero.congratulations").addNumber(DAY[i]).addString(Util.declension(player.getLanguage(), DAY[i], DeclensionKey.DAYS)));
			}
			catch(Exception e)
			{
				player.sendMessage(new CustomMessage("common.Error"));
			}
		}
	}
}