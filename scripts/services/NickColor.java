package services;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Util;

public class NickColor
{
	@Bypass("services.NickColor:list")
	public void list(Player player, NpcInstance npc, String[] arg)
	{
		if(!Config.SERVICES_CHANGE_NICK_COLOR_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		HtmlMessage html = null;

		html = new HtmlMessage(5).setFile("scripts/services/NickColor/index.htm");
		String template = HtmCache.getInstance().getHtml("scripts/services/NickColor/template.htm", player);
		String block = "";
		String list = "";

		final int perpage = 6;
		final int page = arg[0].length() > 0 ? Integer.parseInt(arg[0]) : 1;
		int counter = 0;
		for(int i = (page - 1) * perpage; i < Config.SERVICES_CHANGE_NICK_COLOR_LIST.length; i++)
		{
			String color = Config.SERVICES_CHANGE_NICK_COLOR_LIST[i];
			block = template;
			block = block.replace("{bypass}", "bypass -h htmbypass_services.NickColor:change " + color);
			block = block.replace("{name}", player.getName());
			block = block.replace("{color}", (color.substring(4, 6) + color.substring(2, 4) + color.substring(0, 2)));
			block = block.replace("{cost}", new CustomMessage("scripts.services.cost").addString(Util.formatPay(player, Config.SERVICES_CHANGE_NICK_COLOR_PRICE, Config.SERVICES_CHANGE_NICK_COLOR_ITEM)).toString(player));
			list += block;

			counter++;

			if(counter >= perpage)
				break;
		}

		double count = Math.ceil((double) Config.SERVICES_CHANGE_NICK_COLOR_LIST.length / (double) perpage); // Используем округление для получения последней страницы с остатком!
		int inline = 1;
		String navigation = "";
		for(int i = 1; i <= count; i++)
		{
			if(i == page)
				navigation += "<td width=25 align=center valign=top><button value=\"[" + i + "]\" action=\"bypass -h htmbypass_services.NickColor:list " + i + "\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";
			else
				navigation += "<td width=25 align=center valign=top><button value=\"" + i + "\" action=\"bypass -h htmbypass_services.NickColor:list " + i + "\" width=32 height=25 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>";

			if(inline % 7 == 0)
				navigation += "</tr><tr>";
			
			inline++;
		}

		if(inline == 2)
			navigation = "<td width=30 align=center valign=top>...</td>";

		html.replace("%list%", list);
		html.replace("%navigation%", navigation);

		player.sendPacket(html);
	}

	@Bypass("services.NickColor:change")
	public void change(Player player, NpcInstance npc, String[] arg)
	{
		if(arg == null || arg.length < 1)
			return;

		if(!Config.SERVICES_CHANGE_NICK_COLOR_ENABLED)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(arg[0].equalsIgnoreCase("FFFFFF"))
		{
			player.setNameColor(Integer.decode("0xFFFFFF"));
			player.broadcastUserInfo(true);
			return;
		}

		if(Util.getPay(player, Config.SERVICES_CHANGE_NICK_COLOR_ITEM, Config.SERVICES_CHANGE_NICK_COLOR_PRICE, true))
		{
			player.setNameColor(Integer.decode("0x" + arg[0]));
			player.broadcastUserInfo(true);
		}
	}
}