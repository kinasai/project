package services;

import handler.bbs.Career;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.data.htm.HtmCache;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.HtmlMessage;
import com.l2cccp.gameserver.utils.Util;

public class Class
{
	@Bypass("services.Class:choice")
	public void choice(Player player, NpcInstance npc, String[] arg)
	{
		if(!Util.isNumber(arg[0]) || !Util.isNumber(arg[1]))
			return;

		int class_id = Integer.parseInt(arg[0]);
		int id = Integer.parseInt(arg[1]);
		HtmlMessage html = new HtmlMessage(5).setFile("scripts/services/Class/index.htm");
		String content = "";
		String template = HtmCache.getInstance().getHtml("scripts/services/Class/template.htm", player);
		String block = null;

		int first = Config.BBS_CLASS_MASTER_PRICE_ITEM[id];
		int second = Config.BBS_CLASS_MASTER_PRICE_SECOND_ITEM[id];
		long first_count = Config.BBS_CLASS_MASTER_PRICE_COUNT[id];
		long second_count = Config.BBS_CLASS_MASTER_PRICE_SECOND_COUNT[id];

		block = template;
		block = block.replace("{icon}", Util.getItemIcon(first));
		block = block.replace("{name}", Util.getItemName(first));
		block = block.replace("{link}", "bypass -h htmbypass_services.Class:take " + class_id + " " + id + " " + 0);
		block = block.replace("{price}", Util.formatPay(player, first_count, first));
		content += block;

		block = template;
		block = block.replace("{icon}", Util.getItemIcon(second));
		block = block.replace("{name}", Util.getItemName(second));
		block = block.replace("{link}", "bypass -h htmbypass_services.Class:take " + class_id + " " + id + " " + 1);
		block = block.replace("{price}", Util.formatPay(player, second_count, second));
		content += block;

		html.replace("%content%", content);

		player.sendPacket(html);
	}

	@Bypass("services.Class:take")
	public void take(Player player, NpcInstance npc, String[] arg)
	{
		if(!Util.isNumber(arg[0]) || !Util.isNumber(arg[1]) || !Util.isNumber(arg[2]))
			return;

		int class_id = Integer.parseInt(arg[0]);
		int id = Integer.parseInt(arg[1]);
		int pay = Integer.parseInt(arg[2]);
		Career.getInstance().changeClass(player, class_id, id, pay);
	}
}
