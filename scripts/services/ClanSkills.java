package services;

import java.util.Collection;

import com.l2cccp.gameserver.data.xml.holder.SkillAcquireHolder;
import com.l2cccp.gameserver.handler.bypass.Bypass;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.SkillLearn;
import com.l2cccp.gameserver.model.base.AcquireType;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.AcquireSkillDone;
import com.l2cccp.gameserver.network.l2.s2c.AcquireSkillList;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ClanSkills
{
	@Bypass("clanskills")
	public void clanskills(Player player, NpcInstance npc, String[] arg)
	{
		if(check(player))
			showAcquireList(AcquireType.CLAN, player);
	}

	private void showAcquireList(AcquireType t, Player player)
	{
		final Collection<SkillLearn> skills = SkillAcquireHolder.getInstance().getAvailableSkills(player, t);

		final AcquireSkillList asl = new AcquireSkillList(t, skills.size());

		for(SkillLearn s : skills)
			asl.addSkill(s.getId(), s.getLevel(), s.getLevel(), s.getCost(), 0);

		if(skills.size() == 0)
		{
			player.sendPacket(AcquireSkillDone.STATIC);
			player.sendPacket(SystemMsg.THERE_ARE_NO_OTHER_SKILLS_TO_LEARN);
		}
		else
		{
			player.sendPacket(asl);
			player.addSessionVar("clanskill", true);
		}

		player.sendActionFailed();
	}

	private boolean check(Player player)
	{
		if(player.getClan() == null)
		{
			player.sendMessage("You must be in clan!");
			return false;
		}
		else if(!player.isClanLeader())
		{
			player.sendMessage("You must be clan Leader!");
			return false;
		}
		else
			return true;
	}
}