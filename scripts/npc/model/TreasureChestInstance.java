package npc.model;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.instances.ChestInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

public class TreasureChestInstance extends ChestInstance
{
	private static final int TREASURE_BOMB_ID = 4143;

	public TreasureChestInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void tryOpen(Player opener, Skill skill)
	{
		double chance = calcChance(opener, skill);
		if(Rnd.chance(chance))
		{
			getAggroList().addDamageHate(opener, 10000, 0);
			doDie(opener);
		}
		else
			fakeOpen(opener);
	}

	public double calcChance(Player opener, Skill skill)
	{
		double chance = skill.getActivateRate();

		int npcLvl = getLevel();
		if(!isCommonTreasureChest())
		{
			double levelmod = (double) skill.getMagicLevel() - npcLvl;
			chance += levelmod * skill.getLevelModifier();
		}
		else
		{
			int level = opener.getLevel();
			int different = Math.abs(level - npcLvl);
			boolean fake = (level <= 77 && different >= 6) || (level >= 78 && different >= 5);
			chance = !fake ? chance : 0;
		}

		return chance;
	}

	private void fakeOpen(Creature opener)
	{
		SkillEntry bomb = SkillTable.getInstance().getSkillEntry(TREASURE_BOMB_ID, getBombLvl());
		if(bomb != null)
			doCast(bomb, opener, false);
		onDecay();
	}

	private int getBombLvl()
	{
		int npcLvl = getLevel();
		int lvl = 1;
		if(npcLvl >= 78)
			lvl = 10;
		else if(npcLvl >= 72)
			lvl = 9;
		else if(npcLvl >= 66)
			lvl = 8;
		else if(npcLvl >= 60)
			lvl = 7;
		else if(npcLvl >= 54)
			lvl = 6;
		else if(npcLvl >= 48)
			lvl = 5;
		else if(npcLvl >= 42)
			lvl = 4;
		else if(npcLvl >= 36)
			lvl = 3;
		else if(npcLvl >= 30)
			lvl = 2;
		return lvl;
	}

	private boolean isCommonTreasureChest()
	{
		int npcId = getNpcId();
		return npcId >= 18265 && npcId <= 18286;
	}

	@Override
	public void onReduceCurrentHp(double damage, final Creature attacker, SkillEntry skill, final boolean awake, final boolean standUp, boolean directHp)
	{
		if(!isCommonTreasureChest())
			fakeOpen(attacker);

		damage = getCurrentHp() - damage > 1 ? damage : 0;
		super.onReduceCurrentHp(damage, attacker, skill, awake, standUp, directHp);
	}

	@Override
	public boolean isImmobilized()
	{
		return true;
	}
}