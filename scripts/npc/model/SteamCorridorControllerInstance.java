package npc.model;

import java.util.StringTokenizer;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.ReflectionUtils;

import instances.CrystalCaverns;

/**
 * @author pchayka
 */
public class SteamCorridorControllerInstance extends NpcInstance
{
	public SteamCorridorControllerInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equalsIgnoreCase("move_next"))
		{
			if(getReflection().getInstancedZoneId() == 10)
				((CrystalCaverns) getReflection()).notifyNextLevel(this);
		}
		else
			super.onBypassFeedback(player, command);
	}
}
