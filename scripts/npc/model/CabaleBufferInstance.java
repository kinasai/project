package npc.model;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

public final class CabaleBufferInstance extends NpcInstance
{
	public CabaleBufferInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void showChatWindow(Player player, int val, Object... arg)
	{}

	@Override
	public void showChatWindow(Player player, String filename, Object... ar)
	{}

	@Override
	public void onBypassFeedback(Player player, String command)
	{}
}