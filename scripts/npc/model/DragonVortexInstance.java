package npc.model;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;

/**
 * @author pchayka
 */

public final class DragonVortexInstance extends NpcInstance
{
	private NpcInstance boss;
	private long _timer = 0L;

	public DragonVortexInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.startsWith("request_boss"))
		{
			if(Config.DRAGON_VORTEX_RAID_LIKE_OFF && boss != null && !boss.isDeleted() || _timer > System.currentTimeMillis())
			{
				showChatWindow(player, "default/32871-3.htm");
				return;
			}

			if(ItemFunctions.deleteItem(player, 17248, 1))
			{
				int chance = Rnd.get(100);
				int bossToSpawn;
				int emerald_horn = 25718;
				int dust_rider = 25719;
				int bleeding_fly = 25720;
				int blackdagger_wing = 25721;
				int shadow_summoner = 25722;
				int spike_slasher = 25723;
				int muscle_bomber = 25724;

				if(chance <= Config.DRAGON_VORTEX_CHANCE_MUSCLE_BOMBER)
					bossToSpawn = muscle_bomber;
				else if(chance <= Config.DRAGON_VORTEX_CHANCE_SHADOW_SUMMONER)
					bossToSpawn = shadow_summoner;
				else if(chance <= Config.DRAGON_VORTEX_CHANCE_SPIKE_SLASHER)
					bossToSpawn = spike_slasher;
				else if(chance <= Config.DRAGON_VORTEX_CHANCE_BLACKDAGGER_WING)
					bossToSpawn = blackdagger_wing;
				else if(chance <= Config.DRAGON_VORTEX_CHANCE_BLEEDING_FLY)
					bossToSpawn = bleeding_fly;
				else if(chance <= Config.DRAGON_VORTEX_CHANCE_DUST_RIDER)
					bossToSpawn = dust_rider;
				else if(chance <= Config.DRAGON_VORTEX_CHANCE_EMERALD_HORN)
					bossToSpawn = emerald_horn;
				else
					bossToSpawn = Rnd.get(25718, 25724);

				boss = NpcUtils.spawnSingle(bossToSpawn, Location.coordsRandomize(getLoc(), 300, 600), getReflection());
				_timer = System.currentTimeMillis() + (Config.DRAGON_VORTEX_RAID_TIME * 1000);
			}
			else
				showChatWindow(player, "default/32871-2.htm");
		}
		else
			super.onBypassFeedback(player, command);
	}
}