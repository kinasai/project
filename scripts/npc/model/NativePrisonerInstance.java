package npc.model;

import java.util.StringTokenizer;

import com.l2cccp.gameserver.instancemanager.HellboundManager;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.AbnormalEffect;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.ChatUtils;

/**
 * Данный инстанс используется в городе-инстансе на Hellbound
 * @author SYS
 */
public final class NativePrisonerInstance extends NpcInstance
{
	public NativePrisonerInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	protected void onSpawn()
	{
		startAbnormalEffect(AbnormalEffect.HOLD_2);
		super.onSpawn();
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this) || isBusy())
			return;

		StringTokenizer st = new StringTokenizer(command);
		if(st.nextToken().equals("rescue"))
		{
			stopAbnormalEffect(AbnormalEffect.HOLD_2);
			ChatUtils.say(this, "Thank you for saving me! Guards are coming, run!");
			HellboundManager.addConfidence(15);
			deleteMe();
		}
		else
			super.onBypassFeedback(player, command);
	}
}