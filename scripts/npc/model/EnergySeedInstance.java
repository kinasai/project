package npc.model;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

/**
 * @author VISTALL
 * @date 20:32/16.05.2011
 */
public class EnergySeedInstance extends NpcInstance
{
	public EnergySeedInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public double getRewardRate(Player player)
	{
		return Config.RATE_DROP_ENERGY_SEED;
	}
}
