package npc.model;

import java.util.StringTokenizer;

import services.Bonus.Bonus;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.s2c.InventoryUpdate;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class NewbieBonusInstance extends NpcInstance
{
	//private static final int[][] fighter = new int[][] { { 57, 5000000 }, { 4037, 500 } };
	//private static final int[][] mage = new int[][] { { 57, 2500000 }, { 4037, 25 } };
	private static final int[][] list = new int[][] { //
	{ 5802, 1, 20 }, //For NPC(Dawn) 	5802  +20
			{ 13979, 1, 44 }, //Dragon sword 13979 +44
			{ 13980, 1, 0 }, //Dragon shield 13980
			{ 6718, 1, 52 }, //Einhasad Shaman 6718 +52
			{ 5128, 1, 44 }, //Crokian Blade 5128 +44
			{ 8220, 1, 52 }, //Grail Apostle Bow 8220 +52
			{ 13497, 1, 0 }, //Refined Chick Hat 13497
			{ 861, 1, 0 }, //Paradia Earring 861
			{ 861, 1, 0 }, //Paradia Earring 861
			{ 892, 1, 0 }, //Paradia Ring 892
			{ 892, 1, 0 }, //Paradia Ring 892
			{ 923, 1, 0 }, //Paradia Necklace 923
			{ 7850, 1, 22 },
			{ 7851, 1, 22 },
			{ 7852, 1, 22 },
			{ 7853, 1, 22 },
			{ 7854, 1, 22 },
			{ 7855, 1, 22 },
			{ 7856, 1, 22 },
			{ 7857, 1, 22 },
			{ 7858, 1, 22 },
			{ 7859, 1, 22 },
			{ 17, 1000, 0 }, //Wooden Arrow
			//Соски
			{ 1835, 10, 0 },
			{ 1463, 10, 0 },
			{ 1464, 10, 0 },
			{ 1465, 10, 0 },
			{ 1466, 10, 0 },
			{ 1467, 10, 0 },
			{ 3947, 10, 0 },
			{ 22072, 10, 0 },
			{ 22073, 10, 0 },
			{ 22074, 10, 0 },
			{ 22075, 10, 0 },
			{ 22076, 10, 0 },
			//Адена
			{ 57, 10000000, 0 } };

	public NewbieBonusInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
		Bonus.getInstance();
	}

	@Override
	public void showChatWindow(Player player, int val, Object... args)
	{
		showChatWindow(player, "scripts/services/NewbieBonus/index.htm");
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.startsWith("take"))
		{
			if(player.getVarB("NewbieBonus"))
			{
				player.sendMessage("Вы уже получили бонус новичка!");
				return;
			}

			StringTokenizer st = new StringTokenizer(command, " ");
			st.nextToken();
			giveReward(player);
		}
	}

	private void giveReward(Player player)
	{
		player.setVar("NewbieBonus", 1, -1);
		//for(int[] data : player.isMageClass() ? mage : fighter)

		for(int[] data : list)
		{
			ItemInstance item = player.getInventory().addItem(data[0], data[1]);

			final int ench = data[2];
			if(ench > 0 && item.canBeEnchanted(false))
				item.setEnchantLevel(ench);

			player.sendPacket(new InventoryUpdate().addModifiedItem(item));
			player.broadcastCharInfo();
		}

		player.sendMessage("Вы получили бонус новичка! Желаю приятной игры!");
	}
}
