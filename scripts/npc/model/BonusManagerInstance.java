package npc.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

import services.Bonus.Bonus;
import services.Bonus.Status;
import services.Bonus.Type;

public class BonusManagerInstance extends NpcInstance
{
	public BonusManagerInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
		Bonus.getInstance();
	}

	@Override
	public void showChatWindow(Player player, int val, Object... args)
	{
		showChatWindow(player, "scripts/services/Bonus/index.htm");
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(!Config.BONUS_SERVICE_ENABLE)
		{
			player.sendMessage(new CustomMessage("scripts.services.off"));
			return;
		}

		if(command.equalsIgnoreCase("index"))
			showChatWindow(player, "scripts/services/Bonus/index.htm");
		else if(command.equalsIgnoreCase("info"))
			showChatWindow(player, "scripts/services/Bonus/info.htm");
		else if(command.equalsIgnoreCase("clan"))
			getClanBonus(player);
		else if(command.equalsIgnoreCase("party"))
			getPartyBonus(player);
		else if(command.startsWith("take"))
		{
			StringTokenizer st = new StringTokenizer(command, " ");
			st.nextToken();
			giveReward(player, Integer.parseInt(st.nextToken()));
		}
	}

	private void giveReward(Player player, int hash)
	{
		Status result = Bonus.getInstance().doReward(hash);

		switch(result)
		{
			case STATUS_OK:
				sendResult(player, "success");
				break;
			case STATUS_NO:
				sendResult(player, "error_not_in_list");
				break;
			case STATUS_ERROR:
				sendResult(player, "error_system");
				break;
		}
	}

	private void getClanBonus(Player player)
	{
		if(player.getClan() == null)
		{
			sendResult(player, "error_no_clan");
			return;
		}
		else if(!player.isClanLeader())
		{
			sendResult(player, "error_not_clan_leader");
			return;
		}
		else if(Bonus.getInstance().hasReward(player.getName(), Type.CLAN))
		{
			sendResult(player, "error_clan_done");
			return;
		}

		List<String> list = Arrays.asList(Config.BONUS_SERVICE_CLAN_LIST);
		if(!list.contains(player.getClan().getName()))
		{
			sendResult(player, "error_clan_register");
			return;
		}

		List<Player> players = new ArrayList<Player>();
		HashMap<String, Player> hwid = new HashMap<String, Player>();

		for(Player cp : World.getAroundPlayers(this, 300, 300))
		{
			if(cp.isOnline() && cp.getClan() != null && cp.getClan().getLeaderId() == player.getObjectId())
			{
				if(!hwid.containsKey(cp.getNetConnection().getHWID()))
				{
					hwid.put(cp.getNetConnection().getHWID(), cp);
					players.add(cp);
				}
			}
		}
		Bonus.getInstance().sortPlayers(players);
		makeRewardHtml(player, players, player.getClan().getName(), Type.CLAN);
	}

	private void getPartyBonus(Player player)
	{
		if(player.getParty() == null)
		{
			sendResult(player, "error_no_party");
			return;
		}
		else if(player.getParty().getPartyLeader() != player)
		{
			sendResult(player, "error_not_party_leader");
			return;
		}
		else if(Bonus.getInstance().hasReward(player.getName(), Type.PARTY))
		{
			sendResult(player, "error_party_done");
			return;
		}

		List<String> list = Arrays.asList(Config.BONUS_SERVICE_PARTY_LIST);
		if(!list.contains(player.getName()))
		{
			sendResult(player, "error_party_register");
			return;
		}

		List<Player> players = new ArrayList<Player>();
		HashMap<String, Player> hwid = new HashMap<String, Player>();

		for(Player cp : World.getAroundPlayers(this, 300, 300))
		{
			if(cp.isOnline() && cp.isInParty() && cp.getParty().getPartyLeader().equals(player))
			{
				if(!hwid.containsKey(cp.getNetConnection().getHWID()))
				{
					hwid.put(cp.getNetConnection().getHWID(), cp);
					players.add(cp);
				}
			}
		}

		Bonus.getInstance().sortPlayers(players);

		if(players.size() < 9)
		{
			sendResult(player, "error_party_not_full");
			return;
		}

		makeRewardHtml(player, players, player.getName(), Type.PARTY);
	}

	private void makeRewardHtml(Player player, List<Player> players, String name, Type type)
	{

		String list = "";

		int count = 0;
		for(int i = 0; i < players.size(); i++)
		{
			Player p = players.get(i);
			if(count != 0)
				list += ", ";

			if(count % 2 == 0 || count == 0)
				list += "<font color=LEVEL>" + p.getName() + "</font>";
			else
				list += p.getName();

			count++;

			if(i + 1 == players.size())
				list += ".";
		}

		Bonus.getInstance().makeRewardList(player, players, type);

		showChatWindow(player, "scripts/services/Bonus/reward.htm", "%list%", list, "%hash%", players.hashCode());
	}

	public void sendResult(Player player, String page)
	{
		showChatWindow(player, "scripts/services/Bonus/" + page + ".htm");

	}
}
