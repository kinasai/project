package npc.model;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.RaidBossInstance;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

import bosses.BelethManager;

public class BelethInstance extends RaidBossInstance
{
	public BelethInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	protected int getMinChannelSizeForLock()
	{
		return -1;
	}

	@Override
	protected void onDeath(Creature killer)
	{
		super.onDeath(killer);
		BelethManager.setBelethDead();
	}
}