package npc.model;

import instances.Frintezza;

import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.ReflectionUtils;

/**
 * @author pchayka
 */

public final class FrintezzaGatekeeperInstance extends NpcInstance
{
	private static final int frintezzaIzId = 136;

	public FrintezzaGatekeeperInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.equalsIgnoreCase("request_frintezza"))
		{
			if (player.getActiveReflection() != null)
				ReflectionUtils.simpleEnterInstancedZone(player, Frintezza.class, frintezzaIzId);
			else if (ItemFunctions.getItemCount(player, 8073) > 0)
			{
				if (ReflectionUtils.simpleEnterInstancedZone(player, Frintezza.class, frintezzaIzId) != null)
					ItemFunctions.deleteItem(player, 8073, 1);
			}
			else
				player.sendPacket(new SystemMessage(SystemMsg.C1S_ITEM_REQUIREMENT_IS_NOT_SUFFICIENT_AND_CANNOT_BE_ENTERED).addName(player));
		}
		else
			super.onBypassFeedback(player, command);
	}
}