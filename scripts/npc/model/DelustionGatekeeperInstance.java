package npc.model;

import java.util.Map;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.data.xml.holder.RiftsHolder;
import com.l2cccp.gameserver.model.Party;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.entity.rifts.DelusionChamber;
import com.l2cccp.gameserver.model.entity.rifts.DimensionalRiftRoom;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.ReflectionUtils;

/**
 * @author pchayka
 */

public final class DelustionGatekeeperInstance extends NpcInstance
{
	public DelustionGatekeeperInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	public void onBypassFeedback(Player player, String command)
	{
		if(!canBypassCheck(player, this))
			return;

		if(command.startsWith("enterDC"))
		{
			int izId = Integer.parseInt(command.substring(8));
			int type = izId - 120;
			Map<Integer, DimensionalRiftRoom> rooms = RiftsHolder.getInstance().getRooms(type);
			if(rooms == null)
			{
				player.sendPacket(SystemMsg.SYSTEM_ERROR);
				return;
			}
			// TODO: DS: move to simpleEnterInstance
			Reflection r = player.getActiveReflection();
			if(r != null)
			{
				if(ReflectionUtils.canReenterInstance(player, izId))
					player.teleToLocation(r.getTeleportLoc(), r);
			}
			else if(ReflectionUtils.canEnterInstance(player, izId))
			{
				Party party = player.getParty();
				if(party != null)
					new DelusionChamber(party, type, Rnd.get(1, rooms.size()));
			}
		}
		else if(command.equals("exitDC"))
		{
			final String var = player.getVar("DCBackCoords");
			if(var == null || var.isEmpty())
			{
				player.teleToLocation(new Location(43768, -48232, -800), 0);
				return;
			}

			player.teleToLocation(Location.parseLoc(var), 0);
			player.unsetVar("DCBackCoords");
		}
		else
			super.onBypassFeedback(player, command);
	}
}