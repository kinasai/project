package npc.model.residences.castle;

import com.l2cccp.gameserver.model.instances.MerchantInstance;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

/**
 * @author VISTALL
 * @date 17:35/13.07.2011
 */
public class MercenaryManagerInstance extends MerchantInstance
{
	public MercenaryManagerInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}
}
