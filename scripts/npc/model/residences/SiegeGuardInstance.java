package npc.model.residences;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.impl.SiegeEvent;
import com.l2cccp.gameserver.model.instances.MonsterInstance;
import com.l2cccp.gameserver.model.pledge.Clan;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;

public class SiegeGuardInstance extends MonsterInstance
{
	public SiegeGuardInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
		setHasChatWindow(false);
	}

	@Override
	public boolean isSiegeGuard()
	{
		return true;
	}

	@Override
	public int getAggroRange()
	{
		return 1200;
	}

	@Override
	public double getRewardRate(Player player)
	{
		return player.getRate().getSiege() * player.getRateSiege();
	}

	@Override
	public boolean isAutoAttackable(Creature attacker)
	{
		final Player player = attacker.getPlayer();
		if(player == null)
			return false;
		final SiegeEvent<?, ?> siegeEvent = getEvent(SiegeEvent.class);
		if(siegeEvent == null)
			return false;
		final Clan clan = player.getClan();
		if(clan == null)
			return false;
		final SiegeEvent<?, ?> siegeEvent2 = attacker.getEvent(SiegeEvent.class);
		if(siegeEvent == siegeEvent2 && siegeEvent.getSiegeClan(SiegeEvent.DEFENDERS, clan) != null)
			return false;
		return true;
	}

	@Override
	public boolean hasRandomAnimation()
	{
		return false;
	}

	@Override
	public boolean isFearImmune()
	{
		return true;
	}

	@Override
	public boolean isParalyzeImmune()
	{
		return true;
	}

	@Override
	public boolean isMonster()
	{
		return false;
	}

	@Override
	protected void onDeath(Creature killer)
	{
		if(killer != null)
		{
			Player player = killer.getPlayer();
			SiegeEvent<?, ?> event = getEvent(SiegeEvent.class);
			if(event != null && player != null)
			{
				Clan clan = player.getClan();
				if(clan != null)
				{
					SiegeEvent<?, ?> siege = player.getEvent(SiegeEvent.class);
					if(siege != null && event.getId() == siege.getId() && event.getSiegeClan(SiegeEvent.DEFENDERS, clan) == null)
					{
						super.onDeath(killer);
						return;
					}
				}
			}
		}

		super.onDeathNoRewards(killer);
	}
}