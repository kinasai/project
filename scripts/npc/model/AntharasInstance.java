package npc.model;

import com.l2cccp.gameserver.model.instances.BossInstance;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.templates.npc.NpcTemplate;
import com.l2cccp.gameserver.utils.Location;

public class AntharasInstance extends BossInstance
{
	public AntharasInstance(int objectId, NpcTemplate template)
	{
		super(objectId, template);
	}

	@Override
	protected int getMinChannelSizeForLock()
	{
		return 0;
	}

	@Override
	public void notifyMinionDied(NpcInstance minion)
	{
		// spawned from ai
	}

	@Override
	public Location getRndMinionPosition()
	{
		return Location.findPointToStay(this, 400, 700);
	}
}