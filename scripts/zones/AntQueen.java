package zones;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.listener.zone.OnZoneEnterLeaveListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.ReflectionUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class AntQueen implements OnInitScriptListener
{
	private ZoneListener _zoneListener;
	private int limit;
	private Location teleport;

	@Override
	public void onInit()
	{
		Zone zone = ReflectionUtils.getZone("[queen_ant_epic]");
		MultiValueSet<String> param = zone.getParams();
		if(param != null)
		{
			limit = param.getInteger("levelLimit");
			teleport = Location.parseLoc(param.getString("teleport"));
			_zoneListener = new ZoneListener();
			zone.addListener(_zoneListener);
		}
	}

	public class ZoneListener implements OnZoneEnterLeaveListener
	{
		@Override
		public void onZoneEnter(Zone zone, Creature cha)
		{
			if(!cha.isPlayable())
				return;

			if(cha.getLevel() > limit)
			{
				Player player = cha.getPlayer();
				if(player != null)
				{
					if(!player.isGM())
					{
						cha.getPlayer().sendMessage(new CustomMessage("scripts.zones.epic.banishMsg"));
						cha.teleToLocation(teleport);
					}
					else
						player.sendMessage("You not teleported because you are GM!");
				}
			}
		}

		@Override
		public void onZoneLeave(Zone zone, Creature cha)
		{}
	}
}