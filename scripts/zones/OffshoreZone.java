package zones;

import java.util.List;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.listener.zone.OnZoneEnterLeaveListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage.ScreenMessageAlign;
import com.l2cccp.gameserver.utils.ReflectionUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class OffshoreZone implements OnInitScriptListener
{
	private static ZoneListener _zoneListener;

	@Override
	public void onInit()
	{
		_zoneListener = new ZoneListener();
		List<Zone> zones = ReflectionUtils.getZonesByType(Zone.ZoneType.offshore);
		for(Zone zone : zones)
			zone.addListener(_zoneListener);
	}

	public class ZoneListener implements OnZoneEnterLeaveListener
	{
		@Override
		public void onZoneEnter(Zone zone, Creature cha)
		{
			if(!cha.isPlayer())
				return;

			Player player = cha.getPlayer();
			final String msg = new CustomMessage("zone.enter.offshore").addNumber((int) Config.SERVICES_OFFSHORE_TRADE_TAX).toString(player);
			player.sendPacket(new ExShowScreenMessage(msg, 900000, ScreenMessageAlign.MIDDLE_CENTER, false));
		}

		@Override
		public void onZoneLeave(Zone zone, Creature cha)
		{
			if(!cha.isPlayer())
				return;

			Player player = cha.getPlayer();
			final String msg = new CustomMessage("zone.leave.offshore").toString(player);
			player.sendPacket(new ExShowScreenMessage(msg, 3000, ScreenMessageAlign.MIDDLE_CENTER, true));
		}
	}
}