package zones;

import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.listener.zone.OnZoneEnterLeaveListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.ReflectionUtils;

public class BaiumSilence implements OnInitScriptListener
{
	private static ZoneListener _zoneListener;

	@Override
	public void onInit()
	{
		_zoneListener = new ZoneListener();
		Zone zone = ReflectionUtils.getZone("[baium_silence]");
		zone.addListener(_zoneListener);
	}

	public class ZoneListener implements OnZoneEnterLeaveListener
	{
		@Override
		public void onZoneEnter(Zone zone, Creature cha)
		{
			if(!cha.isPlayable())
				return;

			Player player = cha.getPlayer();
			if(player != null)
			{
				int id = zone.getParams().getInteger("id");
				int level = zone.getParams().getInteger("level");
				int time = zone.getParams().getInteger("time");
				SkillEntry skill = SkillTable.getInstance().getSkillEntry(id, level);
				skill.getEffects(player, player, false, false, time * 60000, 0, 5);
				player.sendMessage("Baium: You moved to restricted area, you feel negative magic. Leave this zone!");
			}
		}

		@Override
		public void onZoneLeave(Zone zone, Creature cha)
		{
			Player player = cha.getPlayer();
			if(player != null)
			{
				for(Effect effect : player.getEffectList().getAllEffects())
				{
					SkillEntry skill = effect.getSkill();
					int id = zone.getParams().getInteger("id");
					int level = zone.getParams().getInteger("level");
					if(skill.getId() == id && skill.getLevel() == level)
						effect.exit();
				}
				player.sendMessage("Baium: You leave the restricted area, negative magic is gone.");
			}
		}
	}
}
