package zones;

import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.listener.zone.OnZoneEnterLeaveListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.utils.AdminFunctions;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.ReflectionUtils;

public class BelethForbidden implements OnInitScriptListener
{
	private static ZoneListener _zone_20_24;
	private static ZoneListener _zone_18_25;

	@Override
	public void onInit()
	{
		_zone_18_25 = new ZoneListener();
		_zone_20_24 = new ZoneListener();
		Zone zone = ReflectionUtils.getZone("[20_24_water1]");
		zone.addListener(_zone_20_24);
		Zone zone2 = ReflectionUtils.getZone("[18_25_water1]");
		zone2.addListener(_zone_18_25);
	}

	public class ZoneListener implements OnZoneEnterLeaveListener
	{
		@Override
		public void onZoneEnter(Zone zone, Creature cha)
		{
			if(!cha.isPlayable())
				return;

			Player player = cha.getPlayer();
			if(player != null)
			{
				if(zone.getParams().getBool("jail", false))
				{
					final int period = zone.getParams().getInteger("jail_time", 25000);
					player.setVar("jailedFrom", player.getX() + ";" + player.getY() + ";" + player.getZ() + ";" + player.getReflectionId(), -1);
					player.setVar("jailed", period, -1);
					player.startUnjailTask(player, period);
					player.teleToLocation(Location.findPointToStay(player, AdminFunctions.JAIL_SPAWN, 50, 200), ReflectionManager.JAIL);
					player.sitDown(null);
					player.block();
					player.sendMessage("You moved to jail, time to escape - " + period + " minutes, reason - try use beleth zone bug.");
				}
				else
				{
					player.sendMessage("You swam too deep!");
					cha.teleToLocation(Location.parseLoc(zone.getParams().getString("teleport")));
				}
			}
		}

		@Override
		public void onZoneLeave(Zone zone, Creature cha)
		{}
	}
}
