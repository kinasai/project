package zones;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.listener.zone.OnZoneEnterLeaveListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.utils.ReflectionUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class PvPZone implements OnInitScriptListener
{
	private static ZoneListener _zoneListener;

	@Override
	public void onInit()
	{
		_zoneListener = new ZoneListener();
		for(final String name : Config.PVP_ZONE_LIST)
		{
			Zone zone = ReflectionUtils.getZone(name);
			zone.addListener(_zoneListener);
		}
	}

	public class ZoneListener implements OnZoneEnterLeaveListener
	{
		@Override
		public void onZoneEnter(Zone zone, Creature cha)
		{
			if(!cha.isPlayable())
				return;

			Player player = cha.getPlayer();
			player.startPvPFlag(null);
		}

		@Override
		public void onZoneLeave(Zone zone, Creature cha)
		{
			if(!cha.isPlayable())
				return;

			Player player = cha.getPlayer();
			player.stopPvPFlag();
		}
	}
}