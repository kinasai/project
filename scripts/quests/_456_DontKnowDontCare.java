package quests;

import org.apache.commons.lang3.ArrayUtils;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.quest.Quest;
import com.l2cccp.gameserver.model.quest.QuestState;

/**
 * @author pchayka
 *         Daily quest
 *         ВНИМАНИЕ! Данный квест можно выполнять не только группой, но и командным каналом, все персонажи в командном канале имеют шанс получить квестовые предметы. После убийства боссов будут появляться специальные НПЦ - мертвые тела боссов, для получения квестовых предметов необходимо будет "поговорить" с этим НПЦ.
 */
public class _456_DontKnowDontCare extends Quest
{
	private static final int[] SEPARATED_SOULS = { 32864, 32865, 32866, 32867, 32868, 32869, 32870 };
	private static final int DRAKE_LORD_ESSENCE = 17251;
	private static final int BEHEMOTH_LEADER_ESSENCE = 17252;
	private static final int DRAGON_BEAST_ESSENCE = 17253;

		private static final int DRAKE_LORD_CORPSE = 32884;
	private static final int BEHEMOTH_LEADER_CORPSE = 32885;
	private static final int DRAGON_BEAST_CORPSE = 32886;

	//Reward set
	private static final int[] weapons = { 15558, 15559, 15560, 15561, 15562, 15563, 15564, 15565, 15566, 15567, 15568, 15569, 15570, 15571 };
	private static final int[] armors = {
			15743,
			15744,
			15745,
			15746,
			15747,
			15748,
			15749,
			15750,
			15751,
			15752,
			15753,
			15754,
			15755,
			15756,
			15757,
			15759,
			15758 };
	private static final int[] accessory = { 15763, 15764, 15765 };
	private static final int[] scrolls = { 6577, 6578, 959 };
	private static final int[] reward_attr_crystal = { 4342, 4343, 4344, 4345, 4346, 4347 };
	private static final int gemstone_s = 2134;

	public _456_DontKnowDontCare()
	{
		super(PARTY_ALL);
	}

	@Override
	public String onEvent(String event, QuestState st, NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("sepsoul_q456_05.htm"))
		{
			st.setState(STARTED);
			st.setCond(1);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("take_essense"))
		{
			if(st.getCond() == 1 && npc != null && st.getInt("RaidKilled") == npc.getObjectId())
			{
				switch(npc.getNpcId())
				{
					case DRAKE_LORD_CORPSE:
						if(st.getQuestItemsCount(DRAKE_LORD_ESSENCE) == 0)
						{
							st.giveItems(DRAKE_LORD_ESSENCE, 1);
							htmltext = "corpse_drake_lord_q0456_02.htm";
						}
						break;
					case BEHEMOTH_LEADER_CORPSE:
						if(st.getQuestItemsCount(BEHEMOTH_LEADER_ESSENCE) == 0)
						{
							st.giveItems(BEHEMOTH_LEADER_ESSENCE, 1);
							htmltext = "corpse_behemoth_leader_q0456_02.htm";
						}
						break;
					case DRAGON_BEAST_CORPSE:
						if(st.getQuestItemsCount(DRAGON_BEAST_ESSENCE) == 0)
						{
							st.giveItems(DRAGON_BEAST_ESSENCE, 1);
							htmltext = "corpse_dragon_beast_q0456_02.htm";
						}
						break;
				}
				if(st.getQuestItemsCount(DRAKE_LORD_ESSENCE) > 0 && st.getQuestItemsCount(BEHEMOTH_LEADER_ESSENCE) > 0 && st.getQuestItemsCount(DRAGON_BEAST_ESSENCE) > 0)
					st.setCond(2);
			}
		}
		else if(event.equalsIgnoreCase("sepsoul_q456_08.htm"))
		{
			st.takeAllItems(DRAKE_LORD_ESSENCE);
			st.takeAllItems(BEHEMOTH_LEADER_ESSENCE);
			st.takeAllItems(DRAGON_BEAST_ESSENCE);

			if(Rnd.chance(30))
				st.giveItems(weapons[Rnd.get(weapons.length)], 1);
			else if(Rnd.chance(50))
				st.giveItems(armors[Rnd.get(armors.length)], 1);
			else
				st.giveItems(accessory[Rnd.get(accessory.length)], 1);

			if(Rnd.chance(30))
				st.giveItems(scrolls[Rnd.get(scrolls.length)], 1);
			if(Rnd.chance(70))
				st.giveItems(reward_attr_crystal[Rnd.get(reward_attr_crystal.length)], 1);
			st.giveItems(gemstone_s, 15);

			st.setState(COMPLETED);
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(this);
		}

		return htmltext;
	}

	@Override
	public String onTalk(NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getCond();
		if(ArrayUtils.contains(SEPARATED_SOULS, npc.getNpcId()))
		{
			switch(st.getState())
			{
				case CREATED:
					if(st.isNowAvailable())
					{
						if(st.getPlayer().getLevel() >= 80)
							htmltext = "sepsoul_q456_01.htm";
						else
						{
							htmltext = "sepsoul_q456_00.htm";
							st.exitCurrentQuest(true);
						}
					}
					else
						htmltext = "sepsoul_q456_00a.htm";
					break;
				case STARTED:
					if(cond == 1)
						htmltext = "sepsoul_q456_06.htm";
					else if(cond == 2)
						htmltext = "sepsoul_q456_07.htm";
					break;
			}
		}

		return htmltext;
	}

	@Override
	public String onFirstTalk(NpcInstance npc, Player player)
	{
		final QuestState st = player.getQuestState(this);
		final boolean result = (st != null && st.getInt("RaidKilled") == npc.getObjectId());
		switch(npc.getNpcId())
		{
			case DRAKE_LORD_CORPSE:
				return result ? st.getQuestItemsCount(DRAKE_LORD_ESSENCE) == 0 ? "corpse_drake_lord_q0456_01.htm" : "corpse_drake_lord001.htm" : "corpse_drake_lord_q0456_03.htm";
			case BEHEMOTH_LEADER_CORPSE:
				return result ? st.getQuestItemsCount(BEHEMOTH_LEADER_ESSENCE) == 0 ? "corpse_behemoth_leader_q0456_01.htm" : "corpse_behemoth_leader001.htm" : "corpse_behemoth_leader_q0456_03.htm";
			case DRAGON_BEAST_CORPSE:
				return result ? st.getQuestItemsCount(DRAGON_BEAST_ESSENCE) == 0 ? "corpse_dragon_beast_q0456_01.htm" : "corpse_dragon_beast001.htm" : "corpse_dragon_beast_q0456_03.htm";
		}
		return "noquest";
	}
}