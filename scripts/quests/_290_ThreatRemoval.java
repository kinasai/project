package quests;

import org.apache.commons.lang3.ArrayUtils;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.quest.Quest;
import com.l2cccp.gameserver.model.quest.QuestState;

/**
 * @author pchayka
 */
public class _290_ThreatRemoval extends Quest
{
	private static final int GuardPinaps = 30201;
	private static final int[] SelMahumTrainers = {
			22775,
			22776,
			22777,
			22778
	};
	private static final int[] SelMahumRecruits = {
			22780,
			22781,
			22782,
			22783,
			22784,
			22785
	};
	private static final int SelMahumIDTag = 15714;

	public _290_ThreatRemoval()
	{
		super(false);
	}

	@Override
	public String onEvent(String event, QuestState st, NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("pinaps_q290_02.htm"))
		{
			st.setState(STARTED);
			st.setCond(1);
			st.playSound(SOUND_ACCEPT);
		}
		else if(event.equalsIgnoreCase("pinaps_q290_05.htm"))
		{
			if(st.getQuestItemsCount(SelMahumIDTag) < 400)
				return "pinaps_q290_03.htm";

			st.takeItems(SelMahumIDTag, 400);
			switch(Rnd.get(1, 6))
			{
				case 1:
					st.giveItems(getParam().getInteger("exchange1Id"), getParam().getInteger("exchange1Count"));
					break;
				case 2:
					st.giveItems(getParam().getInteger("exchange2Id"), getParam().getInteger("exchange2Count"));
					break;
				case 3:
					st.giveItems(getParam().getInteger("exchange3Id"), getParam().getInteger("exchange3Count"));
					break;
				case 4:
					st.giveItems(getParam().getInteger("exchange4Id"), getParam().getInteger("exchange4Count"));
					break;
				case 5:
					st.giveItems(getParam().getInteger("exchange5Id"), getParam().getInteger("exchange5Count"));
					break;
				case 6:
					st.giveItems(getParam().getInteger("exchange6Id"), getParam().getInteger("exchange6Count"));
					break;
			}
			st.playSound(SOUND_MIDDLE);
		}
		else if(event.equalsIgnoreCase("continue"))
		{
			htmltext = "pinaps_q290_06.htm";
		}
		else if(event.equalsIgnoreCase("quit"))
		{
			htmltext = "pinaps_q290_07.htm";
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int cond = st.getCond();
		if(npc.getNpcId() == GuardPinaps)
		{
			if(cond == 0)
			{
				QuestState qs = st.getPlayer().getQuestState(251);
				if(st.getPlayer().getLevel() >= 82 && qs != null && qs.isCompleted())
					htmltext = "pinaps_q290_01.htm";
				else
				{
					htmltext = "pinaps_q290_00.htm";
					st.exitCurrentQuest(true);
				}
			}
			else if(cond == 1 && st.getQuestItemsCount(SelMahumIDTag) < 400)
				htmltext = "pinaps_q290_03.htm";
			else if(cond == 1 && st.getQuestItemsCount(SelMahumIDTag) >= 400)
				htmltext = "pinaps_q290_04.htm";
		}

		return htmltext;
	}

	@Override
	public String onKill(NpcInstance npc, QuestState st)
	{
		int cond = st.getCond();
		if(cond == 1)
		{
			if(ArrayUtils.contains(SelMahumTrainers, npc.getNpcId()))
				st.rollAndGive(SelMahumIDTag, getParam().getInteger("killTrainersReward"), getParam().getDouble("killTrainersChance"));
			else if(ArrayUtils.contains(SelMahumRecruits, npc.getNpcId()))
				st.rollAndGive(SelMahumIDTag, getParam().getInteger("killRecruitsReward"), getParam().getDouble("killRecruitsChance"));
		}
		return null;
	}
}