package quests;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.data.xml.holder.ExperienceHolder;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.quest.Quest;
import com.l2cccp.gameserver.model.quest.QuestState;

public class _634_InSearchofDimensionalFragments extends Quest
{
	private static final int DIMENSION_FRAGMENT_ID = 7079;

	public _634_InSearchofDimensionalFragments()
	{
		super(true);
	}

	@Override
	public String onEvent(String event, QuestState st, NpcInstance npc)
	{
		String htmltext = event;
		if(event.equalsIgnoreCase("quest_accept"))
		{
			htmltext = "dimension_keeper_1_q0634_03.htm";
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
			st.setCond(1);
		}
		else if(event.equalsIgnoreCase("634_2"))
		{
			htmltext = "dimension_keeper_1_q0634_06.htm";
			st.playSound(SOUND_FINISH);
			st.exitCurrentQuest(true);
		}
		return htmltext;
	}

	@Override
	public String onTalk(NpcInstance npc, QuestState st)
	{
		String htmltext = "noquest";
		int id = st.getState();
		if(id == CREATED)
		{
			if(st.getPlayer().getLevel() > 20)
				htmltext = "dimension_keeper_1_q0634_01.htm";
			else
			{
				htmltext = "dimension_keeper_1_q0634_02.htm";
				st.exitCurrentQuest(true);
			}
		}
		else if(id == STARTED)
			htmltext = "dimension_keeper_1_q0634_04.htm";
		return htmltext;
	}

	@Override
	public String onKill(NpcInstance npc, QuestState st)
	{
		if(!getParam().getBool("useSimpleChance"))
			st.rollAndGive(DIMENSION_FRAGMENT_ID, Rnd.get(getParam().getInteger("minCount"), getParam().getInteger("maxCount")), 60 * ExperienceHolder.getInstance().penaltyModifier(st.calculateLevelDiffForDrop(npc.getLevel(), st.getPlayer().getLevel()), 9) * npc.getTemplate().rateHp / 4);
		else
			st.rollAndGive(DIMENSION_FRAGMENT_ID, Rnd.get(getParam().getInteger("minCount"), getParam().getInteger("maxCount")), getParam().getDouble("chance"));

		return null;
	}
}