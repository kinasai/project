package quests;

import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.quest.Quest;
import com.l2cccp.gameserver.model.quest.QuestState;

public class _122_OminousNews extends Quest
{
	private static final int MOIRA = 31979;
	private static final int KARUDA = 32017;

	public _122_OminousNews()
	{
		super(false);
	}

	@Override
	public String onEvent(String event, QuestState st, NpcInstance npc)
	{
		String htmltext = event;
		int cond = st.getCond();
		htmltext = event;
		if(htmltext.equalsIgnoreCase("seer_moirase_q0122_0104.htm") && cond == 0)
		{
			st.setCond(1);
			st.setState(STARTED);
			st.playSound(SOUND_ACCEPT);
		}
		else if(htmltext.equalsIgnoreCase("karuda_q0122_0201.htm"))
			if(cond == 1)
			{
				st.giveItems(getParam().getInteger("rewardId"), getParam().getInteger("rewardCount"));
				st.addExpAndSp(getParam().getInteger("exp"), getParam().getInteger("sp")); // награда соответствует Т2
				st.playSound(SOUND_FINISH);
				st.exitCurrentQuest(false);
			}
			else
				htmltext = "noquest";
		return htmltext;
	}

	@Override
	public String onTalk(NpcInstance npc, QuestState st)
	{
		int npcId = npc.getNpcId();
		String htmltext = "noquest";
		int cond = st.getCond();
		if(npcId == MOIRA)
		{
			if(cond == 0)
			{
				if(st.getPlayer().getLevel() >= 20)
					htmltext = "seer_moirase_q0122_0101.htm";
				else
				{
					htmltext = "seer_moirase_q0122_0103.htm";
					st.exitCurrentQuest(true);
				}
			}
			else
				htmltext = "seer_moirase_q0122_0104.htm";
		}
		else if(npcId == KARUDA && cond == 1)
			htmltext = "karuda_q0122_0101.htm";
		return htmltext;
	}
}