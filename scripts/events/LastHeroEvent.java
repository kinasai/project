package events;

import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.ScheduledFuture;

import org.napile.primitive.maps.IntObjectMap;
import org.napile.primitive.maps.impl.CHashIntObjectMap;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Announcements;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.listener.actor.OnDeathListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.Hero;
import com.l2cccp.gameserver.model.entity.events.objects.DuelSnapshotObject;
import com.l2cccp.gameserver.model.entity.events.util.EventState;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SocialAction;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.TimeUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class LastHeroEvent extends CustomInstantTeamEvent
{
	private final boolean _hero;
	private boolean hero_items;
	private boolean hero_skills;
	private boolean hero_chat;
	private long hero_time;
	private Player last;
	private Player winner;

	private OnDeathListener _deathListener = new OnDeathListenerImpl();
	private IntObjectMap<ScheduledFuture<?>> _deadList = new CHashIntObjectMap<ScheduledFuture<?>>();

	public LastHeroEvent(MultiValueSet<String> set)
	{
		super(set);
		_hero = set.getBool("hero", false);

		if(_hero)
		{
			String[] setting = set.getString("hero_setting").split(",");
			hero_items = Boolean.parseBoolean(setting[0]);
			hero_skills = Boolean.parseBoolean(setting[1]);
			hero_chat = Boolean.parseBoolean(setting[2]);
			hero_time = TimeUtils.addHours(Integer.parseInt(setting[3]));
		}
	}

	@Override
	public synchronized void checkForWinner()
	{
		if(state != EventState.FIRST_PHASE)
			return;

		final int size = getObjects(TeamType.BLUE).size();

		if(last != null)
		{
			giveReward(size);

			if(winner != null)
			{
				if(winner != null)
				{
					last = winner;
					giveReward(size - 1);
				}

				if(_hero)
				{
					long time = System.currentTimeMillis() + hero_time;
					winner.setVar("hasFakeHero", 1, time);
					if(hero_skills)
					{
						winner.setVar("hasFakeHeroSkills", 1, time);
						Hero.addSkills(winner);
					}

					if(hero_items)
						winner.setVar("hasFakeHeroItems", 1, time);

					if(hero_chat)
						winner.setVar("hasFakeHeroChat", 1, time);

					winner.sendChanges();
					winner.broadcastCharInfo();
					winner.broadcastPacket(new SocialAction(winner.getObjectId(), SocialAction.GIVE_HERO));
				}
			}

			if(size <= 1)
			{
				winner = null;
				last = null;
				setState(EventState.FINISH);
				super.stopEvent();
			}
		}
	}

	private void giveReward(int size)
	{
		if(rewardItems != null && size <= 1)
		{
			for(int i = 0; i < rewardItems.length; i++)
			{
				ItemFunctions.addItem(last, rewardItems[i], levelMul ? rewardCounts[i] * last.getLevel() : rewardCounts[i]);
			}
		}

		if(rewardPosition != null)
		{
			final int positions = rewardPosition.length;
			if(positions > 1)
			{
				if(size < positions)
				{
					Announcements.getInstance().announceToAll(new CustomMessage("event.win.position").addString(getName()).addString(last.getName()).addNumber(size + 1), ChatType.CRITICAL_ANNOUNCE);
					StringTokenizer st = new StringTokenizer(rewardPosition[size], "{},()[]");
					int item_id = Integer.parseInt(st.nextToken());
					long item_count_min = Integer.parseInt(st.nextToken());
					long item_count_max = Integer.parseInt(st.nextToken());
					double item_chance = Double.parseDouble(st.nextToken());
					if(Rnd.chance(item_chance))
						ItemFunctions.addItem(last, item_id, Rnd.get(item_count_min, item_count_max));
				}
			}
		}
	}

	@Override
	public boolean registerPlayer(Player player)
	{
		if(super.registerPlayer(player))
		{
			addObject(TeamType.BLUE, new DuelSnapshotObject(player, TeamType.BLUE, true));

			player.addEvent(this);
			player.addListener(_playerListeners);
			player.sendMessage(new CustomMessage("event.registration").addString(getName()));
			return true;
		}
		else
			return false;
	}

	@Override
	public SystemMsg checkForAttack(Creature target, Creature attacker, Skill skill, boolean force)
	{
		if(!canAttack(target, attacker, skill, force, false))
			return SystemMsg.INVALID_TARGET;

		return null;
	}

	@Override
	public boolean canAttack(Creature target, Creature attacker, Skill skill, boolean force, boolean nextAttackCheck)
	{
		if(state != EventState.FIRST_PHASE || target.getTeam() == TeamType.NONE || attacker.getTeam() == TeamType.NONE)
			return false;

		return true;
	}

	@Override
	public void onAddEvent(GameObject o)
	{
		super.onAddEvent(o);
		if(o.isPlayer())
			o.getPlayer().addListener(_deathListener);
	}

	@Override
	public void onRemoveEvent(GameObject o)
	{
		super.onRemoveEvent(o);
		if(o.isPlayer())
		{
			_deadList.remove(o.getObjectId());
			o.getPlayer().removeListener(_deathListener);
		}
	}

	private class OnDeathListenerImpl implements OnDeathListener
	{
		@Override
		public void onDeath(Creature actor, Creature killer)
		{
			_deadList.put(actor.getObjectId(), ThreadPoolManager.getInstance().schedule(new DieAndAnnounceTask(actor.getPlayer(), killer.getPlayer()), 150L));
		}
	}

	protected Location getTeleportLoc(TeamType team)
	{
		Location[] loc = map.getPlayerSpawns();
		return loc[Rnd.get(0, loc.length - 1)];
	}

	@Override
	protected boolean checkTeamSize()
	{
		List<Object> blue = getObjects(TeamType.BLUE);
		if(blue != null && blue.size() < map.getMin())
			return false;
		else
			return true;
	}

	private class DieAndAnnounceTask extends RunnableImpl
	{
		private Player _player;
		private Player _killer;

		public DieAndAnnounceTask(Player player, Player killer)
		{
			_player = player;
			_killer = killer;
		}

		@Override
		public void runImpl()
		{
			if(state == EventState.FIRST_PHASE)
			{
				if(_player != null && _player.getTeam() == TeamType.BLUE)
				{
					if(_killer != null && _killer.getTeam() == TeamType.BLUE)
					{
						List<DuelSnapshotObject> objects = getObjects(TeamType.BLUE);
						announceKill(_killer, _player, objects);
					}

					_scores.remove(_player.getName());

					List<DuelSnapshotObject> objs = getObjects(TeamType.BLUE);
					for(DuelSnapshotObject obj : objs)
					{
						if(obj.getPlayer() == _player)
						{
							last = _player;
							objs.remove(obj);
							quit(obj);
							if(objs.size() <= 1)
								winner = _killer;
						}
					}
				}

				checkForWinner();
			}
		}
	}

	@Override
	public Iterator<DuelSnapshotObject> iterator()
	{
		List<DuelSnapshotObject> blue = getObjects(TeamType.BLUE);
		return blue.iterator();
	}
}