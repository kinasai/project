package events;

import java.util.List;
import java.util.concurrent.ScheduledFuture;

import org.napile.pair.primitive.IntObjectPair;
import org.napile.primitive.maps.IntObjectMap;
import org.napile.primitive.maps.impl.CHashIntObjectMap;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.listener.actor.OnDeathListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.events.objects.DuelSnapshotObject;
import com.l2cccp.gameserver.model.entity.events.util.EventState;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.skillclasses.Resurrect;
import com.l2cccp.gameserver.utils.Location;

import events.impl.ctf.CtfBaseObject;
import events.impl.ctf.CtfFlagObject;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CaptureTeamFlagEvent extends CustomInstantTeamEvent
{
	private class RessurectTask extends RunnableImpl
	{
		private Player _player;
		private int _seconds = 10;

		public RessurectTask(Player player)
		{
			_player = player;
		}

		@Override
		public void runImpl()
		{
			_seconds -= 1;
			if(_seconds == 0)
			{
				_deadList.remove(_player.getObjectId());

				if(_player.getTeam() == TeamType.NONE) // Он уже не на эвенте.
					return;

				_player.teleToLocation(getTeleportLoc(_player.getTeam()));
				_player.doRevive();
			}
			else
			{
				_player.sendPacket(new SystemMessage(SystemMsg.RESURRECTION_WILL_TAKE_PLACE_IN_THE_WAITING_ROOM_AFTER_S1_SECONDS).addNumber(_seconds));
				ScheduledFuture<?> f = ThreadPoolManager.getInstance().schedule(this, 1000L);

				_deadList.put(_player.getObjectId(), f);
			}
		}
	}

	private class OnDeathListenerImpl implements OnDeathListener
	{
		@Override
		public void onDeath(Creature actor, Creature killer)
		{
			if(killer.isPlayer() && actor.isPlayer())
			{
				List<DuelSnapshotObject> objects = getObjects(killer.getTeam());
				announceKill(killer.getPlayer(), actor.getPlayer(), objects);
			}

			_deadList.put(actor.getObjectId(), ThreadPoolManager.getInstance().schedule(new RessurectTask(actor.getPlayer()), 1000L));
		}
	}

	public static final String FLAGS = "flags";
	public static final String BASES = "bases";
	public static final String UPDATE_ARROW = "update_arrow";
	private OnDeathListener _deathListener = new OnDeathListenerImpl();
	private IntObjectMap<ScheduledFuture<?>> _deadList = new CHashIntObjectMap<ScheduledFuture<?>>();

	public CaptureTeamFlagEvent(MultiValueSet<String> set)
	{
		super(set);
		Resurrect.GLOBAL.add(this);
	}

	private void updateArrowInPlayers()
	{
		List<CtfFlagObject> flagObjects = getObjects(FLAGS);

		if(flagObjects != null)
		{
			for(DuelSnapshotObject object : this)
			{
				final TeamType team = object.getTeam();
				CtfFlagObject selfFlag = flagObjects.get(team.ordinalWithoutNone());
				CtfFlagObject enemyFlag = flagObjects.get(team.revert().ordinalWithoutNone());

				Player player = object.getPlayer();
				if(player == null)
					continue;

				Location location = null;
				if(enemyFlag.getOwner() == player) // у тя чужой флаг в руках, посылаем к базе
				{
					List<CtfBaseObject> bases = getObjects(BASES);
					location = bases.get(team.ordinalWithoutNone()).getLoc();
				}
				else if(selfFlag.getOwner() != null)
					location = selfFlag.getOwner().getLoc(); // свой флаг потерян, посылаем к овнеру
				else
					location = enemyFlag.getLocation(); // иначе посылаем к чужом флагу

				player.addRadar(location.getX(), location.getY(), location.getZ());
			}
		}

	}

	public void setWinner(TeamType teamType)
	{
		if(state != EventState.FIRST_PHASE)
			return;

		winner = teamType;
		setState(EventState.FINISH);
		stopEvent();
	}

	@Override
	public void startEvent()
	{
		spawnAction(BASES, true);
		spawnAction(FLAGS, true);
		super.startEvent();
	}

	//region Implementation & Override
	@Override
	public void stopEvent()
	{
		spawnAction(BASES, false);
		spawnAction(FLAGS, false);

		for(IntObjectPair<ScheduledFuture<?>> pair : _deadList.entrySet())
			pair.getValue().cancel(true);

		_deadList.clear();

		super.stopEvent();
	}

	@Override
	protected void actionUpdate(boolean start, Player player)
	{
		if(!start)
			player.removeRadar();
	}

	@Override
	public void action(String name, boolean start)
	{
		if(name.equals(UPDATE_ARROW))
			updateArrowInPlayers();
		else
			super.action(name, start);
	}

	@Override
	protected Location getTeleportLoc(TeamType team)
	{
		return Location.findAroundPosition(map.getTeamSpawns().get(team.ordinal())[0], 100, 200, getReflection().getGeoIndex());
	}

	@Override
	public synchronized void checkForWinner()
	{
		if(state == EventState.NONE)
			return;

		TeamType winnerTeam = null;
		for(TeamType team : TeamType.VALUES)
		{
			List<DuelSnapshotObject> objects = getObjects(team);

			if(objects.isEmpty())
			{
				winnerTeam = team.revert();
				break;
			}
		}

		if(winnerTeam != null)
		{
			winner = winnerTeam;
			setState(EventState.FINISH);
			stopEvent();
		}
	}

	@Override
	protected boolean canWalkInWaitTime()
	{
		return false;
	}

	@Override
	public void setRegistrationOver(boolean registrationOver)
	{
		if(!registrationOver)
		{
			setState(EventState.REGISTRATION);
			addObject(BASES, new CtfBaseObject(35426, map.getKeyLocations()[0], TeamType.BLUE));
			addObject(BASES, new CtfBaseObject(35423, map.getKeyLocations()[1], TeamType.RED));
			addObject(FLAGS, new CtfFlagObject(map.getKeyLocations()[2], TeamType.BLUE));
			addObject(FLAGS, new CtfFlagObject(map.getKeyLocations()[3], TeamType.RED));
			announceToValidPlayer("event.registration.open");
		}
		else
			announceToValidPlayer("event.registration.close");
	}

	@Override
	public boolean registerPlayer(Player player)
	{
		if(super.registerPlayer(player))
		{
			addPlayer(player);
			return true;
		}
		else
			return false;
	}

	@Override
	public boolean canResurrect(Creature active, Creature target, boolean force, boolean quiet)
	{
		CaptureTeamFlagEvent cubeEvent = target.getEvent(CaptureTeamFlagEvent.class);
		if(cubeEvent == this)
		{
			if(!quiet)
				active.sendPacket(SystemMsg.INVALID_TARGET);
			return false;
		}
		else
			return true;
	}

	@Override
	public void onAddEvent(GameObject o)
	{
		super.onAddEvent(o);
		if(o.isPlayer())
			o.getPlayer().addListener(_deathListener);
	}

	@Override
	public void onRemoveEvent(GameObject o)
	{
		super.onRemoveEvent(o);
		if(o.isPlayer())
			o.getPlayer().removeListener(_deathListener);
	}
	//endregion
}
