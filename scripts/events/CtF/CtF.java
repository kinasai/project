package events.CtF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.l2cccp.gameserver.Announcements;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.data.xml.holder.InstantZoneHolder;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.listener.actor.OnDeathListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerExitListener;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.actor.listener.CharListenerList;
import com.l2cccp.gameserver.model.base.ClassId;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.entity.events.GameEvent;
import com.l2cccp.gameserver.model.entity.events.GameEventManager;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.ChangeWaitType;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.Revive;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.skills.effects.EffectTemplate;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.templates.InstantZone;
import com.l2cccp.gameserver.utils.GArray;
import com.l2cccp.gameserver.utils.ItemFunctions;
import com.l2cccp.gameserver.utils.Location;

public class CtF extends GameEvent implements OnInitScriptListener {
	private static final Logger _log = LoggerFactory.getLogger(CtF.class);
	private final OnDeathListenerImpl deathListener = new OnDeathListenerImpl();
	private static int _state = 0;
	private static CtF _instance;
	private Map<Player, Integer> _participants = new HashMap();
	private Map<Player, String> _dualBoxes = new HashMap();
	private Map<Player, List<Effect>> returnBuffs = new HashMap();
	private List<Effect> _effects;
	private int[] _score;
	private Reflection _ref;
	private InstantZone _instantZone;
	private NpcInstance redFlag = null;
	private NpcInstance blueFlag = null;

	public CtF() {
		_instance = this;
	}

	public static CtF getInstance() {
		if(_instance == null) {
			_instance = new CtF();
		}
		return _instance;
	}

	@Override
	public int getState() {
		return _state;
	}

	@Override
	public String getName() {
		return "Capture The Flag";
	}

	@Override
	public long getNextTime() {
		long next_time = getConfigs().START_TIME;
		while(next_time <= System.currentTimeMillis() / 1000) {
			getConfigs().START_TIME += 86400;
			setNextEvent();
			next_time = getConfigs().START_TIME;
		}
		return next_time;
	}
	
	public static boolean isRunned() {
		return _state > 0;
	}

	public void setNextEvent() {
		if (CTFConfig._configs != null && CTFConfig._configs.size() > 1) {
			CTFConfig._configs.sort();
		}
	}

	public Configs getConfigs() {
		return CTFConfig._configs != null ? CTFConfig._configs.get(0) : null;
	}

	@Override
	public boolean canUseItem(Player actor, ItemInstance item) {
		if(_state == 2) {
			if((item.isHeroWeapon() && !getConfigs().ALLOW_HERO_WEAPONS) || ArrayUtils.contains(getConfigs().getRestictId(), item.getItemId())) {
				actor.sendMessage(actor.isLangRus() ? "Запрещено использовать во время ивентов." : "You may not use during the events.");
				return false;
			}
		}
		return true;
	}

	@Override
	public void onInit() {
        CharListenerList.addGlobal(deathListener);
        CharListenerList.addGlobal(new OnPlayerExitListenerImpl());
		CTFConfig.load();
		GameEventManager.getInstance().registerEvent(getInstance());
		_log.info("Loaded Event: CTF");
		_state = 0;
	}

	@Override
	public boolean register(Player player) {
		if(!canRegister(player, true)) {
			return false;
		}
		player.setPvPTeam(TeamWithMinPlayers());
		_participants.put(player, Integer.valueOf(0));
		if(getConfigs().CHECK_DUALBOX) {
			if(getConfigs().DUALBOX_MODE.equalsIgnoreCase("HWID")) {
				_dualBoxes.put(player, player.getNetConnection().getHWID());				
			} else if(getConfigs().DUALBOX_MODE.equalsIgnoreCase("IP")) {
				_dualBoxes.put(player, player.getNetConnection().getIpAddr());
			}
		} else {
			_dualBoxes.put(player, "0");
		}
		player.setIsInCtF(true);
		player.sendMessage(new CustomMessage("scripts.events.CtF.YouRegistred"));
		player._event = this;
		return true;
	}

	public void addPlayer() {
		registerPlayer();
	}
	
	public void removePlayer() {
		unregisterPlayer();
	}

	public void registerPlayer() {
		Player player = getSelf();
		GameEvent event = GameEventManager.getInstance().findEvent("Capture The Flag");
		event.register(player);
	}

	public void unregisterPlayer() {
		Player player = getSelf();
		GameEvent event = GameEventManager.getInstance().findEvent("Capture The Flag");
		event.unreg(player);
	}
	
	@Override
	public void unreg(Player player) {
		if(player == null) {
			return;
		}
		if(_state == 2 || !isParticipant(player)) {
			player.sendMessage(new CustomMessage("scripts.events.CtF.YouCancelRegistration"));
			player.setPvPTeam(0);
			player.allowPvPTeam();
			player.setIsInCtF(false);
			player._event = null;
			return;
		}
		_participants.remove(player);
		_dualBoxes.remove(player);
		player.setPvPTeam(0);
		player.setIsInCtF(false);
		player.allowPvPTeam();
		player._event = null;
		player.sendMessage(new CustomMessage("scripts.events.CtF.YouRegistrationCanceled"));
	}

	@Override
	public void remove(Player player) {
		if(player == null) {
			return;
		}
		if(_participants.containsKey(player)) {
			_participants.remove(player);
			_dualBoxes.remove(player);
		}
		player.setPvPTeam(0);
		player.allowPvPTeam();
		player._event = null;
		player.sendMessage(new CustomMessage("scripts.events.CtF.YouDisqualified"));
	}

	@Override
	public boolean canRegister(Player player, boolean first) {
		if(getConfigs().ALLOW_TAKE_ITEM) {
			long take_item_count = getItemCount(player, getConfigs().TAKE_ITEM_ID);
			String name_take_items = ItemHolder.getInstance().getTemplate(getConfigs().TAKE_ITEM_ID).getName();
			if(take_item_count > 0) {
				if((int)take_item_count < getConfigs().TAKE_COUNT) {
					player.sendMessage("Недостаточно" + name_take_items + "для участия.");
					return false;
				}
			} else {
				player.sendMessage("У Вас нет " + name_take_items + ", требуется для участия.");
				return false;
			}
		}
		if(first && _state != 1) {
			player.sendMessage("Процесс регистрации не активен.");
			return false;
		}
		if(first && isParticipant(player)) {
			player.sendMessage("Вы уже являетесь участником этого эвента.");
			return false;
		}
		if(first && !checkDualBox(player)) {
			player.sendMessage("Нельзя участвовать в ивенте в несколько окон!");
			return false;
		}
		if(player.isMounted()) {
			player.sendMessage("Отзовите питомца.");
			return false;
		}
		if(player.isInDuel()) {
			player.sendMessage("Вы должны завершить дуель.");
			return false;
		}
		if(player.getLevel() < getConfigs().MIN_LEVEL || player.getLevel() > getConfigs().MAX_LEVEL) {
			player.sendMessage("Вы не подходите для участия в эвенте с таким уровнем.");
			return false;
		}
		if(player.getTeam() != TeamType.NONE) {
			player.sendMessage("Вы уже зарегестрированы на другом эвенте или принимаете участие в дуэли.");
			return false;
		}
		if(first && player.isInPvPEvent()) {
			player.sendMessage("Вы уже зарегестрированы на другом эвенте или принимаете участие в дуэли.");
			return false;
		}
		if(first &&(player.isInOlympiadMode() || Olympiad.isRegistered(player))) {
			player.sendMessage("Вы уже зарегестрированы на Олимпиаде.");
			return false;
		}
		if(player.isInParty() && player.getParty().isInDimensionalRift()) {
			player.sendMessage("Вы уже зарегестрированы на другом эвенте.");
			return false;
		}
		if(player.isTeleporting()) {
			player.sendMessage("Вы находитесь в процессе телепортации.");
			return false;
		}
		if(first && _participants.size() >= getConfigs().MAX_PARTICIPANTS) {
			player.sendMessage("Достигнуто максимальное кол-во участников.");
			return false;
		}
		if(player.isCursedWeaponEquipped()) {
			player.sendMessage("С проклятым оружием на эвент нельзя.");
			return false;
		}
		if(player.getKarma() > 0) {
			player.sendMessage("PK не может учавствовать в эвенте.");
			return false;
		}
		if(player.getTransformation() > 0 || player.getMountNpcId() > 0) {
			player.sendMessage("В режиме трансформации на эвент нельзя.");
			return false;
		}
		if(player.isInOfflineMode()) {
			return false;
		}
		if(player.isInStoreBuff()) {
			return false;
		}
		if(player.isInStoreMode()) {
			return false;
		}
		return true;
	}
	
	public boolean checkDualBox(Player player) {
		if(getConfigs().CHECK_DUALBOX) {
			if(getConfigs().DUALBOX_MODE.equalsIgnoreCase("IP") && _dualBoxes.containsValue(player.getNetConnection().getIpAddr())) {
				return false;
			}
			if(getConfigs().DUALBOX_MODE.equalsIgnoreCase("HWID") && _dualBoxes.containsValue(player.getNetConnection().getHWID())) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int getCountPlayers() {
		return _participants.size();
	}
	
	public int getTeamCount(TeamType type) {
		int count = 0;
		if(_participants != null) {
			for(Player player : _participants.keySet()) {
				if(player.getTeam() == type) {
					count++;
				}
			}
		}
		return count;
	}

	public void canRegisters() {
		if(_participants != null) {
			for(Player player : _participants.keySet()) {
				if(!canRegister(player, false)) {
					player.sendMessage("Если все условия не будут соблюдены - вы будите дисквалифицированы");
				}
			}
		}
	}	

	@Override
	public boolean isParticipant(Player player) {
		return _participants.containsKey(player);
	}

	public int TeamWithMinPlayers() {
		int[] count = new int[getConfigs().TEAM_COUNTS + 1];
		for(Player player : _participants.keySet()) {
			count[player.getPvPTeam()] += 1;
		}
		int min = count[1];
		for(int i = 1; i < count.length; i++) {
			min = Math.min(min, count[i]);
		}
		for(int i = 1; i < count.length; i++) {
			if(count[i] != min) {
				continue;
			}
			min = i;
		}
		return min;
	}

	public void sayToAll(String adress, String[] replacements, boolean all) {
		if (all) {
			Announcements.getInstance().announceByCustomMessage(adress, replacements, ChatType.CRITICAL_ANNOUNCE);
		} else {
			for(Player player : _participants.keySet()) {
				Announcements.getInstance().announceToPlayerByCustomMessage(player, adress, replacements, ChatType.CRITICAL_ANNOUNCE);
			}
		}
	}

	public void question() {
		for(Player player : GameObjectsStorage.getPlayers()) {
			if(canQuestion(player)) {
				player.scriptRequest(new CustomMessage("scripts.events.CtF.AskPlayer").toString(player), "events.CtF.CtF:registerPlayer", new Object[0]);
			}
		}
	}
	
	public boolean canQuestion(Player player) {
		if(player == null) {
			return false;
		}
		if(player.getLevel() < getConfigs().MIN_LEVEL) {
			return false;
		}
		if(player.getLevel() > getConfigs().MAX_LEVEL) {
			return false;
		}
		if(player.getReflection().getId() > 0) {
			return false;
		}
		if(player.isInOlympiadMode()) {
			return false;
		}
		if(Olympiad.isRegistered(player)) {
			return false;
		}
		if(player.isInOfflineMode()) {
			return false;
		}
		if(player.isInStoreBuff()) {
			return false;
		}
		if(player.isInStoreMode()) {
			return false;
		}
		if(!checkDualBox(player)) {
			return false;
		}
		return true;
	}
	
	public void startRegistration() {
		_state = 1;
		sayToAll("scripts.events.CtF.AnnounceRegistrationStarted", new String[] { getName() , "" + getConfigs().MIN_LEVEL, "" + getConfigs().MAX_LEVEL}, true);
		question();		
		_score = new int[getConfigs().TEAM_COUNTS];
		if(getConfigs().TIME_TO_START_BATTLE >= 30) {
			ThreadPoolManager.getInstance().schedule(new StartMessages("scripts.events.CtF.EventStartOver", new String[] { "30" }), (getConfigs().TIME_TO_START_BATTLE - 30) * 1000);
		}
		if(getConfigs().TIME_TO_START_BATTLE >= 10) {
			ThreadPoolManager.getInstance().schedule(new StartMessages("scripts.events.CtF.EventStartOver", new String[] { "10" }), (getConfigs().TIME_TO_START_BATTLE - 10) * 1000);
		}
		for(int i = 5; i >= 1; i--) {
			if(getConfigs().TIME_TO_START_BATTLE - i >= i) {
				ThreadPoolManager.getInstance().schedule(new StartMessages("scripts.events.CtF.EventStartOver", new String[] { Integer.toString(i) }), (getConfigs().TIME_TO_START_BATTLE - i) * 1000);
			}
		}
		ThreadPoolManager.getInstance().schedule(new TaskVoid("canRegisters", null), (getConfigs().TIME_TO_START_BATTLE - 10) * 1000);
		ThreadPoolManager.getInstance().schedule(new TaskVoid("start", null), getConfigs().TIME_TO_START_BATTLE * 1000);
	}

	private void initRef() {
		_ref = new Reflection();
		_instantZone = InstantZoneHolder.getInstance().getInstantZone(602);
		_ref.init(_instantZone);
	}

	@Override
	public void start() {
		initRef();
		if(_state == 0) {
			startRegistration();
		} else if(_state == 1) {
			if(getCountPlayers() >= getConfigs().MIN_PARTICIPANTS) {
				_ref.getDoor(24190002).closeMe();
				_ref.getDoor(24190003).closeMe();
				ThreadPoolManager.getInstance().schedule(new go(), getConfigs().PAUSE_TIME * 1000);
				sayToAll("scripts.events.CtF.AnnounceTeleportToColiseum", new String[0], true);
				_state = 2;
				teleportPlayersToColiseum();
				if(redFlag != null) {
					redFlag.deleteMe();
				}
				if(blueFlag != null) {
					blueFlag.deleteMe();
				}
				blueFlag = spawn(getConfigs().FLAG_COORDS.get(0), 35426, _ref);
				redFlag = spawn(getConfigs().FLAG_COORDS.get(1), 35423, _ref);
			} else {
				sayToAll("scripts.events.CtF.AnnounceEventCanceled", new String[] { getName() }, true);
				for(Player player : _participants.keySet()) {
					player.setPvPTeam(0);
					player.allowPvPTeam();
					player._event = null;
				}
				_participants.clear();
				_dualBoxes.clear();
				_state = 0;
				abort();
			}
		} else {
			sayToAll("scripts.events.CtF.AnnounceStartError", new String[0], true);
		}
	}

	@Override
	public void finish() {
		sayToAll("scripts.events.CtF.AnnounceEventEnd", new String[0], false);
		if(_state == 2) {
			int WinTeam = -1;
			int max = 0;
			int count = 0;
			for(int i = 0; i < _score.length - 1; i++) {
				max = Math.max(_score[i], _score[(i + 1)]);
			}
			for(int i = 0; i < _score.length; i++) {
				if(_score[i] != max) {
					continue;
				}
				WinTeam = i;
				count++;
			}

			if(count != 1 || WinTeam == -1 || _score[WinTeam] == 0) {
				sayToAll("scripts.events.CtF.EventDraw", new String[0], false);
			} else {
				rewardToWinTeam(WinTeam);
			}
			ThreadPoolManager.getInstance().schedule(new TaskVoid("restorePlayers", null), 1000);
			ThreadPoolManager.getInstance().schedule(new TaskVoid("teleportPlayersToSavedCoords", null), 3000);
		}
		_ref.getDoor(24190002).openMe();
		_ref.getDoor(24190003).openMe();
		ThreadPoolManager.getInstance().schedule(new TaskVoid("clearAll", null), 3500);
		GameEventManager.getInstance().nextEvent();
		_state = 0;
	}

	@Override
	public void abort() {
		finish();
		if(_state > 0) {
			sayToAll("scripts.events.CtF.EventCompletedManually", new String[] { getName() }, true);
		}
	}

	public void teleportPlayersToSavedCoords() {
		for(Player player : _participants.keySet()) {
			teleportPlayerToSavedCoords(player);
		}
	}

	public void teleportPlayerToSavedCoords(Player player) {
		try {
			if(player == null) {
				return;
			}
			if(player._stablePoint == null) {// игрока не портнуло на стадион
				return;
			}
			removeFlag(player);
			player.getEffectList().stopAllEffects();
			player.unsetVar("CtF_Flag");
			player.setPvPTeam(0);
			player.allowPvPTeam();
			player.setIsInCtF(false);
			player._event = null;
			if(player.getServitor() != null) {
				Servitor summon = player.getServitor();
				summon.unSummon(false, false);
			}
			if(getConfigs().STOP_ALL_EFFECTS) {
				ThreadPoolManager.getInstance().schedule(new TaskVoid("returnBuffsToPlayers", player), 500L);
			}
			player.removeInstanceReuse(_instantZone.getId());
			player.teleToLocation(player._stablePoint, ReflectionManager.DEFAULT);
			player._stablePoint = null;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private class OnDeathListenerImpl implements OnDeathListener {
		@Override
		public void onDeath(Creature actor, Creature killer) {
			if(actor == null || killer == null) {
				return;
			}
			if(actor.isPlayer() && killer.isPlayer()) {
				Player player = (Player)actor;
				Player kill = (Player)killer;
				if(_state == 2 && _participants.containsKey(player)) {
					_participants.put(player, _participants.get(player) + 1);
				}
				if(_state == 2 && getConfigs().ALLOW_KILL_BONUS && _participants.containsKey(kill)) {
					addItem(kill, getConfigs().KILL_BONUS_ID, getConfigs().KILL_BONUS_COUNT);
				}
				if(_state == 2 && player.getPvPTeam() > 0 && kill.getPvPTeam() > 0 && _participants.containsKey(player) && _participants.containsKey(kill)) {
					player.setFakeDeath(1);
					player.getAI().notifyEvent(CtrlEvent.EVT_FAKE_DEATH, null, null);
					player.broadcastPacket(new ChangeWaitType(player, ChangeWaitType.WT_START_FAKEDEATH));
					player.broadcastCharInfo();
					player.abortCast(true, false);
					player.abortAttack(true, false);
					player.sendMessage(new CustomMessage("scripts.events.CtF.YouDead").addString(String.valueOf(getConfigs().RESURRECTION_TIME)));
					ThreadPoolManager.getInstance().schedule(new TaskVoid("ResurrectionPlayer", player), getConfigs().RESURRECTION_TIME * 1000);
				}
				player.unsetVar("CtF_Flag");
				dropFlag(player, true);
			}
		}		
	}

	public void teleportPlayersToColiseum() {
		for(Player player : _participants.keySet()) {
			if(!canRegister(player, false)) {
				remove(player);
				continue;
			}
			if(getConfigs().ALLOW_TAKE_ITEM) {
				removeItem(player, getConfigs().TAKE_ITEM_ID, (long)getConfigs().TAKE_COUNT);
			}
			ItemInstance wpn = player.getActiveWeaponInstance();
			if(wpn != null && wpn.isHeroWeapon() && !getConfigs().ALLOW_HERO_WEAPONS) {
				player.getInventory().unEquipItem(wpn);
				player.abortAttack(true, true);
			}
			unRide(player);
			if(getConfigs().STOP_ALL_EFFECTS) {
				removeBuff(player);
			}
			if(player.getParty() != null) {
				player.leaveParty();
			}
			player.allowPvPTeam();
			player._stablePoint = player._stablePoint == null ? player.getReflection().getReturnLoc() == null ? player.getLoc() : player.getReflection().getReturnLoc() : player._stablePoint;
			Reflection ref = _ref;
			InstantZone instantZone = ref.getInstancedZone();
			Location tele = Location.findPointToStay(instantZone.getTeleportCoords().get(player.getPvPTeam() - 1), 150, 200, ref.getGeoIndex());
			player.teleToLocation(tele, ref);
			restorePlayer(player);
			player.sendPacket(new ExShowScreenMessage("Через несколько секунд бой начнется!", getConfigs().PAUSE_TIME * 700, ExShowScreenMessage.ScreenMessageAlign.MIDDLE_CENTER, true));
		}
		paralyzePlayers();
	}

	public void removeBuff(Player player) {
		if(player != null) {
			List<Effect> effectList = player.getEffectList().getAllEffects();
			_effects = new ArrayList(effectList.size());
			if(player.isCastingNow()) {
				player.abortCast(true, true);
			}
			for(Effect $effect : effectList) {
				Effect effect = $effect.getTemplate().getEffect(new Env($effect.getEffector(), $effect.getEffected(), $effect.getSkill()));
				effect.setCount($effect.getCount());
				effect.setPeriod($effect.getCount() == 1 ? $effect.getPeriod() - $effect.getTime() : $effect.getPeriod());
				_effects.add(effect);
			}
			if(player.getServitor() != null) {
				Servitor summon = player.getServitor();
				summon.unSummon(false, false);
			}
			returnBuffs.put(player, _effects);
			player.getEffectList().stopAllEffects();
		}
	}

	public void returnBuffsToPlayers(Player player) {
		for(Effect e : returnBuffs.get(player)) {
			player.getEffectList().addEffect(e);
		}
	}

	public void paralyzePlayers() {
		SkillEntry revengeSkill = SkillTable.getInstance().getSkillEntry(4515, 1);
		for(Player player : _participants.keySet()) {
			player.getEffectList().stopEffect(1411);
			revengeSkill.getEffects(player, player, false, false);
			if(player.getServitor() != null) {
				revengeSkill.getEffects(player, player.getServitor(), false, false);
			}
		}
	}

	public void unParalyzePlayers() {
		for(Player player : _participants.keySet()) {
			player.getEffectList().stopEffect(4515);
			if(player.getServitor() != null) {
				player.getServitor().getEffectList().stopEffect(4515);
			}
			if(player.isInParty()) {
				player.leaveParty();
			}
		}
	}

	public void restorePlayer(Player player) {
		ClassId nclassId = ClassId.VALUES[player.getClassId().getId()];
		if(player.isFakeDeath() || player.isDead()) {
			player.setFakeDeath(0);
			player.broadcastPacket(new ChangeWaitType(player, ChangeWaitType.WT_STOP_FAKEDEATH));
			player.broadcastPacket(new Revive(player));
			player.broadcastCharInfo();
		}
		if(nclassId.isMage()) {
			playerBuff(player, getConfigs().LIST_MAGE_MAG_SUPPORT);
		} else {
			playerBuff(player, getConfigs().LIST_MAGE_FAITER_SUPPORT);
		}
		player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp(), true);
		player.setCurrentCp(player.getMaxCp());
	}

	public void restorePlayers() {
		for(Player player : _participants.keySet()) {
			if(player.isFakeDeath()) {
				player.setFakeDeath(0);
				player.broadcastPacket(new ChangeWaitType(player, ChangeWaitType.WT_STOP_FAKEDEATH));
				player.broadcastPacket(new Revive(player));
				player.broadcastCharInfo();
			}
			player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp(), true);
			player.setCurrentCp(player.getMaxCp());
		}
	}

	public void ResurrectionPlayer(Player player) {
		if((!player.isInCtF()) || _state != 2 || !_participants.containsKey(player)) {
			return;
		}
		Reflection ref = _ref;
		InstantZone instantZone = ref.getInstancedZone();
		Location tele = Location.findPointToStay(instantZone.getTeleportCoords().get(player.getPvPTeam() - 1), 150, 200, ref.getGeoIndex());
		player.teleToLocation(tele, ref);
		restorePlayer(player);
	}

	private void clearAll() {
		for(Player player : _participants.keySet()) {
			player.setPvPTeam(0);
			player.allowPvPTeam();
			player.setIsInCtF(false);
			player._event = null;
		}
		_participants.clear();
		_dualBoxes.clear();
		if(redFlag != null) {
			redFlag.deleteMe();
		}
		if(blueFlag != null) {
			blueFlag.deleteMe();
		}
	}

	public void rewardToWinTeam(int WinTeam) {
		WinTeam++;
		for(Player player : _participants.keySet()) {
			if((player != null) && (player.getPvPTeam() == WinTeam)) {
				for(int i = 0; i < getConfigs().getRewardId().length; i++) {
					addItem(player, getConfigs().getRewardId()[i], getConfigs().getRewardCount()[i]);
				}
			}
		}
		sayToAll("scripts.events.CtF.EventWin", new String[] { getConfigs().TEAM_NAME.get(WinTeam - 1), "Флагов", Integer.toString(_score[(WinTeam - 1)]) }, false);
	}

	private void addFlag(Player player) {
		int flagId = player.getPvPTeam() == 1 ? 13560 : 13561;
		ItemInstance item = ItemFunctions.createItem(flagId);
		item.setCustomType1(77);
		item.setCustomFlags(354);
		player.getInventory().addItem(item);
		player.getInventory().equipItem(item);
		player.sendChanges();
		player.setVar("CtF_Flag", "1", -1);
		if (flagId == 13561) {
			blueFlag.decayMe();
		} else {
			redFlag.decayMe();
		}
	}

	private void removeFlag(Player player) {
		if(player != null) {
			ItemInstance flag = player.getInventory().getItemByItemId(13560);
			if(flag == null) {
				flag = player.getInventory().getItemByItemId(13561);
			}
			if(flag != null && flag.getCustomType1() == 77) {
				player.unsetVar("CtF_Flag");
				flag.setCustomFlags(0);
				player.getInventory().destroyItem(flag, 1);
				player.broadcastUserInfo(true);
			}
		}
	}

	private void dropFlag(Player player, boolean onBase) {
		if(player != null) {
			ItemInstance flag = player.getActiveWeaponInstance();
			if(flag != null && flag.getCustomType1() == 77 && (flag.getItemId() == 13560 || flag.getItemId() == 13561)) {
				removeFlag(player);
				if(flag.getItemId() == 13561) {
					blueFlag.setXYZInvisible(onBase ? getConfigs().FLAG_COORDS.get(0) : player.getLoc());
					blueFlag.spawnMe();
				} else if(flag.getItemId() == 13560) {
					redFlag.setXYZInvisible(onBase ? getConfigs().FLAG_COORDS.get(1) : player.getLoc());
					redFlag.spawnMe();
				}
			}
		}
	}

	@Override
	public boolean talkWithNpc(Player player, NpcInstance npc) {
		if(_state > 0 && player != null && _participants.containsKey(player)) {
			int weaponId = getActiveWeaponId(player);
			if(npc.getNpcId() == 35426) {
				if(weaponId == 13560 && player.getPvPTeam() == 1) {
					flagToBase(player);
					player.unsetVar("CtF_Flag");
				} else if(weaponId != 13560 && weaponId != 13561 && player.getPvPTeam() == 2 && npc.isVisible()) {
					addFlag(player);
				}
				return true;
			}
			if(npc.getNpcId() == 35423) {
				if(weaponId == 13561 && player.getPvPTeam() == 2) {
					flagToBase(player);
					player.unsetVar("CtF_Flag");
				} else if(weaponId != 13560 && weaponId != 13561 && player.getPvPTeam() == 1 && npc.isVisible()) {
					addFlag(player);
				}
				return true;
			}
		}
		return false;
	}
	
	public int getActiveWeaponId(Player player) {
		if(player.getActiveWeaponInstance() != null) {
			return player.getActiveWeaponInstance().getItemId();
		}
		return 0;
	}

	public void flagToBase(Player player) {
		dropFlag(player, true);
		player.unsetVar("CtF_Flag");
		_score[(player.getPvPTeam() - 1)] += 1;
		sayToAll("scripts.events.CtF.FlagToBase", new String[] { player.getName(), getConfigs().TEAM_NAME.get(player.getPvPTeam() - 1) }, false);
	}

	class TaskVoid implements Runnable {
		String _name;
		Player _player;
		TaskVoid(String name, Player player) {
			_name = name;
			_player = player;
		}

		@Override
		public void run() {
			if(_name.equals("canRegisters")) {
				canRegisters();
			} else if(_name.equals("start")) {
				start();
			} else if(_name.equals("restorePlayers")) {
				restorePlayers();
			} else if(_name.equals("returnBuffsToPlayers")) {
				returnBuffsToPlayers(_player);
			} else if(_name.equals("teleportPlayersToSavedCoords")) {
				teleportPlayersToSavedCoords();
			} else if(_name.equals("clearAll")) {
				clearAll();
			} else if(_name.equals("ResurrectionPlayer")) {
				ResurrectionPlayer(_player);
			}
		}
	}

	class StartMessages	implements Runnable {
		String _adress;
		String[] _replacing;
		StartMessages(String adress, String[] replacing) {
			_adress = adress;
			_replacing = replacing;
		}
		@Override
		public void run() {
			if (_state == 1) {
				sayToAll(_adress, _replacing, true);
			}
		}
	}

	public class go implements Runnable {
		public go() {			
		}
		@Override
		public void run() {
			unParalyzePlayers();
			int time = getConfigs().TIME_TO_END_BATTLE;
			sayToAll("scripts.events.CtF.RoundStarted", new String[0], false);
			while(time >= 0 && _state == 2) {
				int sec = time - time / 60 * 60;
				for(Player player : _participants.keySet()) {
					String message = "Очков: " + _score[(player.getPvPTeam() - 1)] + " из " + getConfigs().NEED_SCORE;
					message = message + "\nКоманда: " + getConfigs().TEAM_NAME.get(player.getPvPTeam() - 1);
					if(sec < 10) {
						message = message + "\nОсталось: " + time / 60 + ":0" + sec;
					} else {
						message = message + "\nОсталось: " + time / 60 + ":" + sec;
					}
					player.sendPacket(new ExShowScreenMessage(message, 2000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT, false));
				}
				if(getCountPlayers() <= 1 || getTeamCount(TeamType.BLUE) <= 0 || getTeamCount(TeamType.RED) <= 0 || _score[0] >= getConfigs().NEED_SCORE || _score[1] >= getConfigs().NEED_SCORE) {
					finish();
					break;
				}
				try {
					Thread.sleep(1000);
				} catch(Exception e) {
					e.printStackTrace();
				}
				time--;
				if(time <= 0) {
					finish();
					break;
				}
			}
    	}
	}

	private void playerBuff(Player player, GArray<Integer> list) {
		int time = getConfigs().TIME_MAGE_SUPPORT;
		Servitor pet = player.getServitor();
		SkillEntry skill = null;
		for(int i : list) {
			int lvl = SkillTable.getInstance().getBaseLevel(i);			
			skill = SkillTable.getInstance().getSkillEntry(i, lvl);
			if(pet != null) {
				for(EffectTemplate et : skill.getTemplate().getEffectTemplates())
				{	
					Env env = new Env(pet, pet, skill);
					Effect effect = et.getEffect(env);
					effect.setPeriod(time * 60000);
					pet.getEffectList().addEffect(effect);
					pet.updateEffectIcons();
				}
			} else {
				for(EffectTemplate et : skill.getTemplate().getEffectTemplates()) {	
					Env env = new Env(player, player, skill);
					Effect effect = et.getEffect(env);
					effect.setPeriod(time * 60000);
					player.getEffectList().addEffect(effect);
					player.sendChanges();
					player.updateEffectIcons();
				}
			}
		}
	}

	@Override
	public boolean canAttack(Creature attacker, Creature target) {
		if(_state == 2) {
			if(attacker.getPlayer().getTeam() == target.getPlayer().getTeam()) {
				return false;
			}
			if(target.getPlayer().isFakeDeath() || target.getPlayer().isDead()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean canUseSkill(Creature caster, Creature target, Skill skill) {
		if(_state == 2) {
			if(skill.isHeroic() && !getConfigs().ALLOW_HERO_WEAPONS) {
				caster.sendMessage(caster.getPlayer().isLangRus() ? "Запрещено использовать во время ивентов." : "You may not use during the events.");
				return false;
			}
			if(skill.isBaseTransformation() || skill.isSummonerTransformation()) {
				return false;
			}
			if(target.getPlayer().isFakeDeath()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String minLvl() {
		return "" + getConfigs().MIN_LEVEL;
	}

	@Override
	public String maxLvl() {
		return "" + getConfigs().MAX_LEVEL;
	}

	private class OnPlayerExitListenerImpl implements OnPlayerExitListener {
		@Override
		public void onPlayerExit(Player player) {
			if(player == null || player.getPvPTeam() < 1) {
				return;
			}
			if(_state == 1 && _participants.containsKey(player)) {
				unreg(player);
				player.setPvPTeam(0);
				player.allowPvPTeam();
				player.setIsInCtF(false);
				player._event = null;
				return;
			}
			if(_state == 2 && _participants.containsKey(player)) {
				try {
					player.getEffectList().stopAllEffects();
					player.unsetVar("CtF_Flag");
					dropFlag(player, true);
					if(player != null) {
						player.teleToLocation(player._stablePoint, ReflectionManager.DEFAULT);
					}
					if (getConfigs().STOP_ALL_EFFECTS) {
						returnBuffsToPlayers(player);
					}
					remove(player);
					player.setPvPTeam(0);
					player.allowPvPTeam();
					player.setIsInCtF(false);
					player._event = null;
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}