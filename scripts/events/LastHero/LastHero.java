package events.LastHero;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2cccp.gameserver.Announcements;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.data.xml.holder.InstantZoneHolder;
import com.l2cccp.gameserver.data.xml.holder.ItemHolder;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.listener.actor.OnDeathListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerExitListener;
import com.l2cccp.gameserver.listener.actor.player.OnTeleportListener;
import com.l2cccp.gameserver.listener.script.OnInitScriptListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Effect;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Servitor;
import com.l2cccp.gameserver.model.Skill;
import com.l2cccp.gameserver.model.actor.listener.CharListenerList;
import com.l2cccp.gameserver.model.base.ClassId;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.Hero;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.entity.events.GameEvent;
import com.l2cccp.gameserver.model.entity.events.GameEventManager;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.model.items.ItemInstance;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.s2c.ChangeWaitType;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.network.l2.s2c.Revive;
import com.l2cccp.gameserver.network.l2.s2c.SkillList;
import com.l2cccp.gameserver.network.l2.s2c.SocialAction;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.skills.effects.EffectTemplate;
import com.l2cccp.gameserver.stats.Env;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.templates.InstantZone;
import com.l2cccp.gameserver.utils.GArray;
import com.l2cccp.gameserver.utils.Location;

public class LastHero extends GameEvent implements OnInitScriptListener {
	private static int _state = 0;
	private static LastHero _instance;
	private static final Logger _log = LoggerFactory.getLogger(LastHero.class);
	private final OnDeathListenerImpl deathListener = new OnDeathListenerImpl();

	private Map<Player, String> _participants = new HashMap();
	private Map<Player, List<Effect>> returnBuffs = new HashMap();
	private List<Effect> _effects;

	private Reflection _ref;
	private InstantZone _instantZone;
	private static final Location _enter = new Location(149505, 46719, -3417);

	public LastHero() {
		_instance = this;
	}

	public static LastHero getInstance() {
		if(_instance == null) {
			_instance = new LastHero();
		}
		return _instance;
	}

	@Override
	public int getState() {
		return _state;
	}

	@Override
	public String getName() {
		return "LastHero";
	}

	@Override
	public long getNextTime() {
		long next_time = getConfigs().START_TIME;
		while(next_time <= System.currentTimeMillis() / 1000) {
			getConfigs().START_TIME += 86400;
			setNextEvent();
			next_time = getConfigs().START_TIME;
		}
		return next_time;
	}

	public void setNextEvent() {
		if(LHConfig._configs != null && LHConfig._configs.size() > 1) {
			LHConfig._configs.sort();
		}
	}

	public Configs getConfigs() {
		return LHConfig._configs == null ? null : LHConfig._configs.get(0);
	}

	@Override
	public boolean canUseItem(Player actor, ItemInstance item) {
		if(_state == 2) {
			if((item.isHeroWeapon() && !getConfigs().ALLOW_HERO_WEAPONS) || ArrayUtils.contains(getConfigs().getRestictId(), item.getItemId())) {
				actor.sendMessage(actor.isLangRus() ? "Запрещено использовать во время ивентов." : "You may not use during the events.");
				return false;
			}
		}
		return true;
	}

	@Override
	public void onInit() {
        CharListenerList.addGlobal(deathListener);
        CharListenerList.addGlobal(new OnPlayerExitListenerImpl());
        CharListenerList.addGlobal(new OnTeleportListenerImpl());
        LHConfig.load();
		GameEventManager.getInstance().registerEvent(getInstance());
		_log.info("Loaded Event: LastHero");
		_state = 0;
	}

	@Override
	public boolean register(Player player) {
		if(!canRegister(player, true)) {
			return false;
		}
		player.setPvPTeam(2);
		if(getConfigs().CHECK_DUALBOX) {
			if(getConfigs().DUALBOX_MODE.equalsIgnoreCase("HWID")) {
				_participants.put(player, player.getNetConnection().getHWID());				
			} else if(getConfigs().DUALBOX_MODE.equalsIgnoreCase("IP")) {
				_participants.put(player, player.getNetConnection().getIpAddr());
			}
		} else {
			_participants.put(player, "0");
		}
		player.sendMessage(new CustomMessage("scripts.events.CustomLastHero.YouRegistred"));
		player.setIsInLastHero(true);
		player._event = this;
		return true;
	}

	public void addPlayer() {
		registerPlayer();
	}
	
	public void removePlayer() {
		unregisterPlayer();
	}

	public void registerPlayer() {
		Player player = getSelf();
		GameEvent event = GameEventManager.getInstance().findEvent("LastHero");
		event.register(player);
	}

	public void unregisterPlayer() {
		Player player = getSelf();
		GameEvent event = GameEventManager.getInstance().findEvent("LastHero");
		event.unreg(player);
	}

	@Override
	public void unreg(Player player) {
		if(player == null) {
			return;
		}
		if(_state == 2 || !isParticipant(player)) {
			player.sendMessage(new CustomMessage("scripts.events.CustomLastHero.YouCancelRegistration"));
			return;
		}
		_participants.remove(player);
		player.setIsInLastHero(false);
		player.setPvPTeam(0);
		player.allowPvPTeam();
		player._event = null;
		player.sendMessage(new CustomMessage("scripts.events.CustomLastHero.YouRegistrationCanceled"));
	}

	@Override
	public void remove(Player player) {
		if(player == null) {
			return;
		}
		if(_participants.containsKey(player)) {
			_participants.remove(player);
		}
		player.setPvPTeam(0);
		player.allowPvPTeam();
		player.setIsInLastHero(false);
		player._event = null;
		player.sendMessage(new CustomMessage("scripts.events.CustomLastHero.YouDisqualified"));
	}

	@Override
	public boolean canRegister(Player player, boolean first) {
		if(getConfigs().ALLOW_TAKE_ITEM) {
			long take_item_count = getItemCount(player, getConfigs().TAKE_ITEM_ID);
			String name_take_items = ItemHolder.getInstance().getTemplate(getConfigs().TAKE_ITEM_ID).getName();
			if(take_item_count > 0) {
				if((int)take_item_count < getConfigs().TAKE_COUNT) {
					player.sendMessage("Недостаточно" + name_take_items + "для участия.");
					return false;
				}
			} else {
				player.sendMessage("У Вас нет " + name_take_items + ", требуется для участия.");
				return false;
			}
		}
		if(first && _state != 1) {
			player.sendMessage("Процесс регистрации не активен.");
			return false;
		}
		if(first && isParticipant(player)) {
			player.sendMessage("Вы уже являетесь участником этого эвента.");
			return false;
		}
		if(first && !checkDualBox(player)) {
			player.sendMessage("Нельзя участвовать в ивенте в несколько окон!");
			return false;
		}
		if(player.isMounted()) {
			player.sendMessage("Отзовите питомца.");
			return false;
		}
		if(player.isInDuel()) {
			player.sendMessage("Вы должны завершить дуель.");
			return false;
		}
		if(player.getLevel() < getConfigs().MIN_LEVEL || player.getLevel() > getConfigs().MAX_LEVEL) {
			player.sendMessage("Вы не подходите для участия в эвенте с таким уровнем.");
			return false;
		}
		if(player.isInOlympiadMode() || Olympiad.isRegistered(player)) {
			player.sendMessage("Вы уже зарегестрированы на Олимпиаде.");
			return false;
		}
		if(player.isInParty() && player.getParty().isInDimensionalRift()) {
			player.sendMessage("Вы уже зарегестрированы на другом эвенте.");
			return false;
		}
		if(player.getTeam() != TeamType.NONE) {
			player.sendMessage("Вы уже зарегестрированы на другом эвенте или принимаете участие в дуэли.");
			return false;
		}
		if(first && player.isInPvPEvent()) {
			player.sendMessage("Вы уже зарегестрированы на другом эвенте или принимаете участие в дуэли.");
			return false;
		}
		if(player.isTeleporting()) {
			player.sendMessage("Вы находитесь в процессе телепортации.");
			return false;
		}
		if(first && _participants.size() >= getConfigs().MAX_PARTICIPANTS) {
			player.sendMessage("Достигнуто максимальное кол-во участников.");
			return false;
		}
		if(player.isCursedWeaponEquipped()) {
			player.sendMessage("С проклятым оружием на эвент нельзя.");
			return false;
		}
		if(player.getKarma() > 0) {
			player.sendMessage("PK не может учавствовать в эвенте.");
			return false;
		}
		if(player.getTransformation() > 0 || player.getMountNpcId() > 0) {
			player.sendMessage("В режиме трансформации на эвент нельзя.");
			return false;
		}
		if(player.isHero() || player.isFakeHero() || player.isLastHeroWinner()) {
			player.sendMessage("Героям на эвент нельзя.");
			return false;
		}
		if(player.isInOfflineMode()) {
			return false;
		}
		if(player.isInStoreBuff()) {
			return false;
		}
		if(player.isInStoreMode()) {
			return false;
		}
		return true;
	}
	
	public boolean checkDualBox(Player player) {
		if(getConfigs().CHECK_DUALBOX) {
			if(getConfigs().DUALBOX_MODE.equalsIgnoreCase("IP") && _participants.containsValue(player.getNetConnection().getIpAddr())) {
				return false;
			}
			if(getConfigs().DUALBOX_MODE.equalsIgnoreCase("HWID") && _participants.containsValue(player.getNetConnection().getHWID())) {
				return false;
			}
		}
		return true;
	}

	public void question() {
		for(Player player : GameObjectsStorage.getPlayers()) {
			if(canQuestion(player)) {
				player.scriptRequest(new CustomMessage("scripts.events.CustomLastHero.AskPlayer").toString(player), "events.LastHero.LastHero:registerPlayer", new Object[0]);
			}
		}
	}
	
	public boolean canQuestion(Player player) {
		if(player == null) {
			return false;
		}
		if(player.getLevel() < getConfigs().MIN_LEVEL) {
			return false;
		}
		if(player.getLevel() > getConfigs().MAX_LEVEL) {
			return false;
		}
		if(player.getReflection().getId() > 0) {
			return false;
		}
		if(player.isInOlympiadMode()) {
			return false;
		}
		if(Olympiad.isRegistered(player)) {
			return false;
		}
		if(player.isInOfflineMode()) {
			return false;
		}
		if(player.isInStoreBuff()) {
			return false;
		}
		if(player.isInStoreMode()) {
			return false;
		}
		if(!checkDualBox(player)) {
			return false;
		}
		return true;
	}
	
	@Override
	public int getCountPlayers() {
		return _participants.size();
	}

	public void canRegisters() {
		if(_participants != null) {
			for(Player player : _participants.keySet()) {
				if(!canRegister(player, false)) {
					player.sendMessage("Если все условия не будут соблюдены - вы будите дисквалифицированы");
				}
			}
		}
	}

	@Override
	public boolean isParticipant(Player player) {
		return _participants.containsKey(player);
	}

	public void sayToAll(String adress, String[] replacements, boolean all) {
		if (all) {
			Announcements.getInstance().announceByCustomMessage(adress, replacements, ChatType.CRITICAL_ANNOUNCE);
		} else {
			for(Player player : _participants.keySet()) {
				Announcements.getInstance().announceToPlayerByCustomMessage(player, adress, replacements, ChatType.CRITICAL_ANNOUNCE);
			}
		}
	}

	public void startRegistration() {
		_state = 1;
		sayToAll("scripts.events.CustomLastHero.AnnounceRegistrationStarted", new String[] { getName(),  "" + getConfigs().MIN_LEVEL, "" + getConfigs().MAX_LEVEL }, true);
		question();
		if(getConfigs().TIME_TO_START_BATTLE >= 30) {
			ThreadPoolManager.getInstance().schedule(new StartMessages("scripts.events.CustomLastHero.EventStartOver", new String[] { "30" }), (getConfigs().TIME_TO_START_BATTLE - 30) * 1000);
		}
		if(getConfigs().TIME_TO_START_BATTLE >= 10) {
			ThreadPoolManager.getInstance().schedule(new StartMessages("scripts.events.CustomLastHero.EventStartOver", new String[] { "10" }), (getConfigs().TIME_TO_START_BATTLE - 10) * 1000);
		}
		for(int i = 5; i >= 1; i--) {
			if(getConfigs().TIME_TO_START_BATTLE - i >= i) {
				ThreadPoolManager.getInstance().schedule(new StartMessages("scripts.events.CustomLastHero.EventStartOver", new String[] { Integer.toString(i) }), (getConfigs().TIME_TO_START_BATTLE - i) * 1000);
			}
		}
		ThreadPoolManager.getInstance().schedule(new TaskVoid("canRegisters", null), (getConfigs().TIME_TO_START_BATTLE - 10) * 1000);
		ThreadPoolManager.getInstance().schedule(new TaskVoid("start", null), getConfigs().TIME_TO_START_BATTLE * 1000);
	}

	private void initRef() {
		_ref = new Reflection();
		_instantZone = InstantZoneHolder.getInstance().getInstantZone(602);
		_ref.init(_instantZone);
	}

	@Override
	public void start() {
		initRef();
		if(_state == 0) {
			startRegistration();
		} else if(_state == 1) {
			if(getCountPlayers() >= getConfigs().MIN_PARTICIPANTS) {
				teleportPlayersToColiseum();
				ThreadPoolManager.getInstance().schedule(new go(), getConfigs().PAUSE_TIME * 1000);
				sayToAll("scripts.events.CustomLastHero.AnnounceTeleportToColiseum", null, true);
				_state = 2;
			} else {
				sayToAll("scripts.events.CustomLastHero.AnnounceEventCanceled", new String[] { getName() }, true);
				for(Player player : _participants.keySet()) {
					player.setPvPTeam(0);
					player.allowPvPTeam();
					player.setIsInLastHero(false);
					player._event = null;
				}
				_participants.clear();
				_state = 0;
				abort();
			}
		} else {
			sayToAll("scripts.events.CustomLastHero.AnnounceStartError", null, true);
		}
	}

	@Override
	public void finish() {
		sayToAll("scripts.events.CustomLastHero.AnnounceEventEnd", null, false);
		if(_state == 2) {
			if(getLivePlayersCount() != 1) {
				sayToAll("scripts.events.CustomLastHero.EventDraw", null, false);
			} else {
				rewardToWinPlayer();
			}
			ThreadPoolManager.getInstance().schedule(new TaskVoid("restorePlayers", null), 1000L);
			ThreadPoolManager.getInstance().schedule(new TaskVoid("teleportPlayersToSavedCoords", null), 2000L);
		}
		_ref.getDoor(24190002).openMe();
		_ref.getDoor(24190003).openMe();
		ThreadPoolManager.getInstance().schedule(new TaskVoid("clearAll", null), 3500L);
		GameEventManager.getInstance().nextEvent();
		_state = 0;
	}
	
	public int getLivePlayersCount() {
		int liveCount = 0;
		for(Player player : _participants.keySet()) {
			if(!player.isDead() && !player.isFakeDeath()) {
				liveCount++;
			}
		}
		return liveCount;
	}

	@Override
	public void abort() {
		finish();
		if(_state > 0) {
			sayToAll("scripts.events.CustomLastHero.EventCompletedManually", new String[] { getName() }, true);
		}
	}

	public void teleportPlayersToSavedCoords() {
		for(Player player : _participants.keySet()) {
			teleportPlayerToSavedCoords(player);
		}
	}

	public void teleportPlayerToSavedCoords(Player player) {
		try {
			if(player == null) {
				return;
			}
			if(player._stablePoint == null) {// игрока не портнуло на стадион
				return;
			}
			player.getEffectList().stopAllEffects();
			player.setPvPTeam(0);
			player.allowPvPTeam();
			player.setIsInLastHero(false);
			player._event = null;
			if(player.getServitor() != null) {
				Servitor summon = player.getServitor();
				summon.unSummon(false, false);
			}
			if(getConfigs().STOP_ALL_EFFECTS) {
				ThreadPoolManager.getInstance().schedule(new TaskVoid("returnBuffsToPlayers", player), 500L);
			}
			player.removeInstanceReuse(_instantZone.getId());
			player.teleToLocation(player._stablePoint, ReflectionManager.DEFAULT);
			player._stablePoint = null;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	private class OnDeathListenerImpl implements OnDeathListener {
		@Override
		public void onDeath(Creature actor, Creature killer) {
			if(actor == null || killer == null) {
				return;
			}
			if((actor.isPlayer()) && (killer.isPlayer())) {
				Player player = (Player)actor;
				Player kill = (Player)killer;
				if(_state == 2 && getConfigs().ALLOW_KILL_BONUS && _participants.containsKey(kill)) {
					addItem(kill, getConfigs().KILL_BONUS_ID, getConfigs().KILL_BONUS_COUNT);
				}
				if(_state == 2 && player.getPvPTeam() > 0 && kill.getPvPTeam() > 0 && _participants.containsKey(player) && _participants.containsKey(kill)) {
					if(player != null) {
						player.setFakeDeath(1);
						player.getAI().notifyEvent(CtrlEvent.EVT_FAKE_DEATH, null, null);
						player.broadcastPacket(new ChangeWaitType(player, ChangeWaitType.WT_START_FAKEDEATH));
						player.broadcastCharInfo();
						player.abortCast(true, false);
						player.abortAttack(true, false);
						show(new CustomMessage("scripts.events.CustomLastHero.YouLose"), player);
					}
				}
			}
		}
	}

	public void teleportPlayersToColiseum() {
		for(Player player : _participants.keySet()) {
			if(!canRegister(player, false)) {
				remove(player);
				continue;
			}
			if(getConfigs().ALLOW_TAKE_ITEM) {
				removeItem(player, getConfigs().TAKE_ITEM_ID, (long)getConfigs().TAKE_COUNT);
			}
			ItemInstance wpn = player.getActiveWeaponInstance();
			if(wpn != null && wpn.isHeroWeapon() && !getConfigs().ALLOW_HERO_WEAPONS) {
				player.getInventory().unEquipItem(wpn);
				player.abortAttack(true, true);
			}
			unRide(player);
			if(getConfigs().STOP_ALL_EFFECTS) {
				removeBuff(player);
			}
			if(player.getParty() != null) {
				player.leaveParty();
			}
			player.allowPvPTeam();
			player._stablePoint = player._stablePoint == null ? player.getReflection().getReturnLoc() == null ? player.getLoc() : player.getReflection().getReturnLoc() : player._stablePoint;
			Reflection ref = _ref;
			Location tele = Location.findPointToStay(_enter, 150, 500, ref.getGeoIndex());
			player.teleToLocation(tele, ref);
			restorePlayer(player);
			player.sendPacket(new ExShowScreenMessage(new CustomMessage("scripts.events.CustomLastHero.StartBattle").toString(player), getConfigs().PAUSE_TIME * 700, ExShowScreenMessage.ScreenMessageAlign.MIDDLE_CENTER, true));
		}
		paralyzePlayers();
	}

	public void removeBuff(Player player) {
		if(player != null) {
			List<Effect> effectList = player.getEffectList().getAllEffects();
			_effects = new ArrayList(effectList.size());
			if(player.isCastingNow()) {
				player.abortCast(true, true);
			}
			for(Effect $effect : effectList) {
				Effect effect = $effect.getTemplate().getEffect(new Env($effect.getEffector(), $effect.getEffected(), $effect.getSkill()));
				effect.setCount($effect.getCount());
				effect.setPeriod($effect.getCount() == 1 ? $effect.getPeriod() - $effect.getTime() : $effect.getPeriod());
				_effects.add(effect);
			}
			if(player.getServitor() != null) {
				Servitor summon = player.getServitor();
				summon.unSummon(false, false);
			}
			returnBuffs.put(player, _effects);
			player.getEffectList().stopAllEffects();
		}
	}

	public void returnBuffsToPlayers(Player player) {
		for(Effect e : returnBuffs.get(player)) {
			player.getEffectList().addEffect(e);
		}
	}

	public void paralyzePlayers() {
		SkillEntry revengeSkill = SkillTable.getInstance().getSkillEntry(4515, 1);
		for(Player player : _participants.keySet()) {
			player.getEffectList().stopEffect(1411);
			revengeSkill.getEffects(player, player, false, false);
			if (player.getServitor() != null) {
				revengeSkill.getEffects(player, player.getServitor(), false, false);
			}
		}
	}

	public void unParalyzePlayers() {
		for(Player player : _participants.keySet()) {
			player.getEffectList().stopEffect(4515);
			if(player.getServitor() != null) {
				player.getServitor().getEffectList().stopEffect(4515);
			}
			if(player.isInParty()) {
				player.leaveParty();
			}
		}
	}

	public void restorePlayer(Player player) {
		ClassId nclassId = ClassId.VALUES[player.getClassId().getId()];
		if(player.isFakeDeath() || player.isDead()) {
			player.setFakeDeath(0);
			player.broadcastPacket(new ChangeWaitType(player, ChangeWaitType.WT_STOP_FAKEDEATH));
			player.broadcastPacket(new Revive(player));
			player.broadcastCharInfo();
		}
		if(nclassId.isMage()) {
			playerBuff(player, getConfigs().LIST_MAGE_MAG_SUPPORT);
		} else {
			playerBuff(player, getConfigs().LIST_MAGE_FAITER_SUPPORT);
		}
		player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp(), true);
		player.setCurrentCp(player.getMaxCp());
	}

	public void restorePlayers() {
		for(Player player : _participants.keySet()) {
			if(player.isFakeDeath()) {
				player.setFakeDeath(0);
				player.broadcastPacket(new ChangeWaitType(player, ChangeWaitType.WT_STOP_FAKEDEATH));
				player.broadcastPacket(new Revive(player));
				player.broadcastCharInfo();
			}
			//player.doRevive(100.);
			player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp(), true);
			player.setCurrentCp(player.getMaxCp());
		}
	}

	private void clearAll() {
		for(Player player : _participants.keySet()) {
			player.setPvPTeam(0);
			player.allowPvPTeam();
			player.setIsInLastHero(false);
			player._event = null;
		}
		_participants.clear();
	}

	public void rewardToWinPlayer() {
		for(Player player : _participants.keySet()) {
			if(player != null && !player.isDead() && !player.isFakeDeath()) {
				for(int i = 0; i < getConfigs().getRewardId().length; i++) {
	                player.setHero(true);
	                Hero.addSkills(player);
	                player.updatePledgeClass();
	                player.sendPacket(new SkillList(player));
	                player.broadcastUserInfo(true);
	                player.setVar("HeroPeriod", "true", -1);
	                player.setHeroAura(true);
	                player.broadcastCharInfo();
	                player.broadcastPacket(new SocialAction(player.getObjectId(), 16));
					addItem(player, getConfigs().getRewardId()[i], getConfigs().getRewardCount()[i]);
					sayToAll("scripts.events.CustomLastHero.EventWin", new String[]{player.getName()}, false);
				}
			}
		}
	}

	class TaskVoid implements Runnable {
		String _name;
		Player _player;
		TaskVoid(String name, Player player) {
			_name = name;
			_player = player;
		}
		@Override
		public void run() {
			if(_name.equals("canRegisters")) {
				canRegisters();
			} else if(_name.equals("start")) {
				start();
			} else if(_name.equals("restorePlayers")) {
				restorePlayers();
			} else if(_name.equals("returnBuffsToPlayers")) {
				returnBuffsToPlayers(_player);
			} else if(_name.equals("teleportPlayersToSavedCoords")) {
				teleportPlayersToSavedCoords();
			} else if(_name.equals("clearAll")) {
				clearAll();
			}
		}
	}
	
	public static boolean isRunned() {
		return _state > 0;
	}

	class StartMessages implements Runnable {
		String _adress;
		String[] _replacing;
		StartMessages(String adress, String[] replacing) {
			_adress = adress;
			_replacing = replacing;
		}
		@Override
		public void run() {
			if(_state == 1) {
				sayToAll(_adress, _replacing, true);
			}
		}
	}

	public class go implements Runnable {
		public go() {
		}
		@Override
		public void run() {
			_ref.getDoor(24190002).closeMe();
			_ref.getDoor(24190003).closeMe();
			unParalyzePlayers();
			int time = getConfigs().TIME_TO_END_BATTLE;
			sayToAll("scripts.events.CustomLastHero.RoundStarted", null, false);
			while(time >= 0 && _state == 2) {
				int sec = time - time / 60 * 60;
				for(Player player : _participants.keySet()) {
					String message = "";
					if(sec < 10) {
						message = "\nОсталось: " + time / 60 + ":0" + sec;
					} else {
						message = "\nОсталось: " + time / 60 + ":" + sec;
					}
					player.sendPacket(new ExShowScreenMessage(message, 2000, ExShowScreenMessage.ScreenMessageAlign.BOTTOM_RIGHT, false));
				}
				if(getCountPlayers() <= 1) {
					finish();
					break;
				}
				if(getLivePlayersCount() <= 1) {
					finish();
					break;
				}
				try {
					Thread.sleep(1000);
				} catch(Exception e) {
					e.printStackTrace();
				}
				time--;
				if(time <= 0) {
					finish();
				}
			}
		}
	}

	private void playerBuff(Player player, GArray<Integer> list) {
		int time = getConfigs().TIME_MAGE_SUPPORT;
		Servitor pet = player.getServitor();
		SkillEntry skill = null;
		for(int i : list) {
			int lvl = SkillTable.getInstance().getBaseLevel(i);			
			skill = SkillTable.getInstance().getSkillEntry(i, lvl);
			if(pet != null) {
				for(EffectTemplate et : skill.getTemplate().getEffectTemplates()) {	
					Env env = new Env(pet, pet, skill);
					Effect effect = et.getEffect(env);
					effect.setPeriod(time * 60000);
					pet.getEffectList().addEffect(effect);
					pet.updateEffectIcons();
				}
			} else {
				for(EffectTemplate et : skill.getTemplate().getEffectTemplates()) {	
					Env env = new Env(player, player, skill);
					Effect effect = et.getEffect(env);
					effect.setPeriod(time * 60000);
					player.getEffectList().addEffect(effect);
					player.sendChanges();
					player.updateEffectIcons();
				}
			}
		}
	}

	@Override
	public boolean canAttack(Creature attacker, Creature target) {
		if(_state == 2) {
			if(target.getPlayer().isFakeDeath() || target.getPlayer().isDead()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean canUseSkill(Creature caster, Creature target, Skill skill) {
		if(_state == 2) {
			if(skill.isHeroic() && !getConfigs().ALLOW_HERO_WEAPONS) {
				caster.sendMessage(caster.getPlayer().isLangRus() ? "Запрещено использовать во время ивентов." : "You may not use during the events.");
				return false;
			}
			if(skill.isBaseTransformation() || skill.isSummonerTransformation()) {
				return false;
			}
			if(target.getPlayer().isFakeDeath()) {
				return false;
			}
		}
		return true;
	}

	@Override
	public String minLvl() {
		return "" + getConfigs().MIN_LEVEL;
	}

	@Override
	public String maxLvl() {
		return "" + getConfigs().MAX_LEVEL;
	}

	private class OnTeleportListenerImpl implements OnTeleportListener {
		@Override
		public void onTeleport(Player player, int x, int y, int z, Reflection reflection) {
			if(_state == 2 && _participants.containsKey(player)) {
				try {
					player.getEffectList().stopAllEffects();
					if(player != null) {
						player.teleToLocation(player._stablePoint, ReflectionManager.DEFAULT);
					}
					if (getConfigs().STOP_ALL_EFFECTS) {
						returnBuffsToPlayers(player);
					}
					remove(player);
					player.setPvPTeam(0);
					player.allowPvPTeam();
					player.setIsInLastHero(false);
					player._event = null;
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	private class OnPlayerExitListenerImpl implements OnPlayerExitListener {
		@Override
		public void onPlayerExit(Player player) {
			if(player == null || player.getPvPTeam() < 1) {
				return;
			}
			if(_state == 1 && _participants.containsKey(player)) {
				unreg(player);
				player.setPvPTeam(0);
				player.allowPvPTeam();
				player.setIsInLastHero(false);
				player._event = null;
				return;
			}
			if(_state == 2 && _participants.containsKey(player)) {
				try {
					player.getEffectList().stopAllEffects();
					if(player != null) {
						player.teleToLocation(player._stablePoint, ReflectionManager.DEFAULT);
					}
					if (getConfigs().STOP_ALL_EFFECTS) {
						returnBuffsToPlayers(player);
					}
					remove(player);
					player.setPvPTeam(0);
					player.allowPvPTeam();
					player.setIsInLastHero(false);
					player._event = null;
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}