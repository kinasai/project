package events;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

import org.napile.pair.primitive.IntObjectPair;
import org.napile.primitive.maps.IntObjectMap;
import org.napile.primitive.maps.impl.CHashIntObjectMap;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.listener.actor.OnDeathListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.events.objects.DuelSnapshotObject;
import com.l2cccp.gameserver.model.entity.events.util.EventState;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.ExPVPMatchCCRecord;
import com.l2cccp.gameserver.network.l2.s2c.ExPVPMatchCCRetire;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.skills.skillclasses.Resurrect;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class TeamVsTeamEvent extends CustomInstantTeamEvent
{
	private class RessurectTask extends RunnableImpl
	{
		private Player _player;
		private int _seconds = 10;

		public RessurectTask(Player player)
		{
			_player = player;
		}

		@Override
		public void runImpl()
		{
			_seconds -= 1;
			if(_seconds == 0)
			{
				_deadList.remove(_player.getObjectId());

				if(_player.getTeam() == TeamType.NONE) // Он уже не на эвенте.
					return;

				_player.teleToLocation(getTeleportLoc(_player.getTeam()));
				_player.doRevive();
			}
			else
			{
				_player.sendPacket(new SystemMessage(SystemMsg.RESURRECTION_WILL_TAKE_PLACE_IN_THE_WAITING_ROOM_AFTER_S1_SECONDS).addNumber(_seconds));
				ScheduledFuture<?> f = ThreadPoolManager.getInstance().schedule(this, 1000L);

				_deadList.put(_player.getObjectId(), f);
			}
		}
	}

	private void showScores()
	{
		Map<String, Integer> scores = getBestScores();
		if(scores != null)
		{
			final ExPVPMatchCCRecord packet = new ExPVPMatchCCRecord(scores);
			sendPacket(packet);
		}
	}

	protected void hideScores(Creature c)
	{
		c.sendPacket(ExPVPMatchCCRetire.STATIC);
	}

	private Map<String, Integer> getBestScores()
	{
		List<Integer> points = new ArrayList<Integer>(_scores.values());
		final int size = points.size();
		if(size < 1)
			return null;

		Collections.sort(points);
		Collections.reverse(points);

		int cap;
		if(size <= 26)
			cap = points.get(size - 1).intValue();
		else
			cap = points.get(25).intValue();
		Map<String, Integer> finalResult = new LinkedHashMap<String, Integer>();

		List<Entry<String, Integer>> toAdd = new ArrayList<Entry<String, Integer>>();
		for(Entry<String, Integer> i : _scores.entrySet())
			if(i.getValue().intValue() > cap && finalResult.size() < 25)
				toAdd.add(i);

		if(finalResult.size() < 25)
		{
			for(Entry<String, Integer> i : _scores.entrySet())
			{
				if(i.getValue().intValue() == cap)
				{
					toAdd.add(i);
					if(finalResult.size() == 25)
						break;
				}
			}
		}

		for(int i = 0; i < toAdd.size(); i++)
		{
			Entry<String, Integer> biggestEntry = null;
			for(Entry<String, Integer> entry : toAdd)
			{
				if(!finalResult.containsKey(entry.getKey()) && (biggestEntry == null || entry.getValue().intValue() > biggestEntry.getValue().intValue()))
					biggestEntry = entry;
			}
			if(biggestEntry != null)
				finalResult.put(biggestEntry.getKey(), biggestEntry.getValue());
		}

		_bestScores = finalResult;

		return finalResult;
	}

	private void updateEveryScore()
	{
		for(DuelSnapshotObject obj : this)
			_scores.put(obj.getPlayer().getName(), obj.getKills());
	}

	private class OnDeathListenerImpl implements OnDeathListener
	{
		@Override
		public void onDeath(Creature actor, Creature killer)
		{
			if(killer.isPlayer() && actor.isPlayer())
			{
				Player kill = killer.getPlayer();
				List<DuelSnapshotObject> objects = getObjects(killer.getTeam());
				announceKill(kill, actor.getPlayer(), objects);
				for(DuelSnapshotObject obj : objects)
				{
					if(obj != null && obj.getPlayer() == kill)
					{
						obj.incraseKills();

						if(obj.getTeam() == TeamType.RED)
							red++;
						else
							blue++;

						_scores.put(kill.getName(), obj.getKills());
					}
				}
			}

			_deadList.put(actor.getObjectId(), ThreadPoolManager.getInstance().schedule(new RessurectTask(actor.getPlayer()), 1000L));
		}
	}

	private OnDeathListener _deathListener = new OnDeathListenerImpl();
	private IntObjectMap<ScheduledFuture<?>> _deadList = new CHashIntObjectMap<ScheduledFuture<?>>();
	private int red;
	private int blue;

	private Map<String, Integer> _bestScores = new ConcurrentHashMap<String, Integer>();

	public TeamVsTeamEvent(MultiValueSet<String> set)
	{
		super(set);

		Resurrect.GLOBAL.add(this);
	}

	@Override
	public void startEvent()
	{
		red = 0;
		blue = 0;
		updateEveryScore();
		super.startEvent();
	}

	@Override
	public void stopEvent()
	{
		showScores();

		if(red > blue)
			winner = TeamType.RED;
		else if(red < blue)
			winner = TeamType.BLUE;
		else
			winner = TeamType.NONE;

		for(IntObjectPair<ScheduledFuture<?>> pair : _deadList.entrySet())
			pair.getValue().cancel(true);

		_bestScores.clear();
		_deadList.clear();

		setState(EventState.FINISH);
		super.stopEvent();
	}

	@Override
	protected void actionUpdate(boolean start, Player player)
	{
		if(!start)
			player.removeRadar();
	}

	@Override
	protected Location getTeleportLoc(TeamType team)
	{
		return Location.findAroundPosition(map.getTeamSpawns().get(team.ordinal())[0], 100, 200, getReflection().getGeoIndex());
	}

	@Override
	public synchronized void checkForWinner() // Check if one team is leave event
	{
		if(state == EventState.NONE)
			return;

		TeamType winnerTeam = null;
		for(TeamType team : TeamType.VALUES)
		{
			List<DuelSnapshotObject> objects = getObjects(team);

			if(objects.isEmpty())
			{
				winnerTeam = team.revert();
				break;
			}
		}

		if(winnerTeam != null)
		{
			winner = winnerTeam;
			setState(EventState.FINISH);
			stopEvent();
		}
	}

	@Override
	protected boolean canWalkInWaitTime()
	{
		return false;
	}

	@Override
	public boolean registerPlayer(Player player)
	{
		if(super.registerPlayer(player))
		{
			addPlayer(player);
			return true;
		}
		else
			return false;
	}

	@Override
	public boolean canResurrect(Creature active, Creature target, boolean force, boolean quiet)
	{
		TeamVsTeamEvent event = target.getEvent(TeamVsTeamEvent.class);
		if(event == this)
		{
			if(!quiet)
				active.sendPacket(SystemMsg.INVALID_TARGET);
			return false;
		}
		else
			return true;
	}

	@Override
	public void onAddEvent(GameObject o)
	{
		super.onAddEvent(o);
		if(o.isPlayer())
			o.getPlayer().addListener(_deathListener);
	}

	@Override
	public void onRemoveEvent(GameObject o)
	{
		super.onRemoveEvent(o);
		if(o.isPlayer())
		{
			Player player = o.getPlayer();
			player.removeListener(_deathListener);
			hideScores(player);
		}
	}
}
