package events.impl.ctf;

import org.apache.commons.lang3.StringUtils;

import com.l2cccp.commons.geometry.Circle;
import com.l2cccp.gameserver.listener.zone.OnZoneEnterLeaveListener;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.Territory;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.Zone;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.events.Event;
import com.l2cccp.gameserver.model.entity.events.objects.SpawnSimpleObject;
import com.l2cccp.gameserver.model.items.attachment.FlagItemAttachment;
import com.l2cccp.gameserver.templates.StatsSet;
import com.l2cccp.gameserver.templates.ZoneTemplate;
import com.l2cccp.gameserver.utils.Location;

import events.CaptureTeamFlagEvent;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class CtfBaseObject extends SpawnSimpleObject
{
	private class OnZoneEnterLeaveListenerImpl implements OnZoneEnterLeaveListener
	{
		@Override
		public void onZoneEnter(Zone zone, Creature actor)
		{
			if(!actor.isPlayer() || actor.getTeam() == TeamType.NONE || _teamType != actor.getTeam())
				return;

			Player player = actor.getPlayer();

			FlagItemAttachment flag = player.getActiveWeaponFlagAttachment();

			if(flag instanceof CtfFlagObject)
			{
				CaptureTeamFlagEvent event = actor.getEvent(CaptureTeamFlagEvent.class);
				event.setWinner(actor.getTeam());
			}
		}

		@Override
		public void onZoneLeave(Zone zone, Creature actor)
		{}
	}

	private Zone _zone = null;
	private TeamType _teamType;

	public CtfBaseObject(int npcId, Location loc, TeamType teamType)
	{
		super(npcId, loc);
		_teamType = teamType;
	}

	@Override
	public void spawnObject(Event event)
	{
		super.spawnObject(event);

		Circle c = new Circle(getLoc(), 300);
		c.setZmax(World.MAP_MAX_Z);
		c.setZmin(World.MAP_MIN_Z);

		StatsSet set = new StatsSet();
		set.set("name", StringUtils.EMPTY);
		set.set("type", Zone.ZoneType.dummy);
		set.set("territory", new Territory().add(c));

		_zone = new Zone(new ZoneTemplate(set));
		_zone.setReflection(event.getReflection());
		_zone.addListener(new OnZoneEnterLeaveListenerImpl());
		_zone.setType(Zone.ZoneType.dummy);
		_zone.setActive(true);

		_npc.setShowName(false);
		_npc.setTargetable(false, true);
		_npc.setHasChatWindow(false);
	}

	@Override
	public void despawnObject(Event event)
	{
		super.despawnObject(event);

		if(_zone == null)
			return;

		_zone.setActive(false);
		_zone = null;
	}
}