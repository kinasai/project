package events;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.commons.time.cron.SchedulingPattern;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.model.entity.events.actions.StartStopAction;
import com.l2cccp.gameserver.model.entity.events.impl.FunEvent;

public class ValleyRunnersEvent extends FunEvent
{
	private final SchedulingPattern _repeatPattern;
	private final int _random;

	public ValleyRunnersEvent(MultiValueSet<String> set)
	{
		super(set);

		final String repeat = set.getString("repeat-pattern", null);
		_repeatPattern = repeat != null ? new SchedulingPattern(repeat) : null;
		_random = set.getInteger("random", 0);
	}

	@Override
	public void reCalcNextTime(boolean onInit)
	{
		final long stopTime = _stopPattern.getTimeInMillis();
		final long currentTime = System.currentTimeMillis();
		if(currentTime >= stopTime)
			return;

		long startTime = _startPattern.getTimeInMillis();
		if(startTime < currentTime && _repeatPattern != null)
			startTime = _repeatPattern.next(currentTime + 60000L);

		if(_random > 0)
			startTime += Rnd.get(-_random, _random) * 1000L;
		if(startTime <= currentTime)
			startTime = currentTime + 60000L;

		final long dff = stopTime - startTime;
		if(dff <= 0)
			return;

		addOnTimeAction(0, new StartStopAction(StartStopAction.EVENT, true));
		addOnTimeAction((int) (dff / 1000L), new StartStopAction(StartStopAction.EVENT, false));

		_startTime = startTime;
		registerActions();
	}
}