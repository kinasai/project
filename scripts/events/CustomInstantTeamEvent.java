package events;

import java.util.List;

import com.l2cccp.commons.collections.MultiValueSet;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.instancemanager.ReflectionManager;
import com.l2cccp.gameserver.listener.PlayerListener;
import com.l2cccp.gameserver.listener.actor.player.OnPlayerExitListener;
import com.l2cccp.gameserver.listener.actor.player.OnTeleportListener;
import com.l2cccp.gameserver.model.GameObject;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.entity.events.impl.TeamEvent;
import com.l2cccp.gameserver.model.entity.events.objects.DuelSnapshotObject;
import com.l2cccp.gameserver.model.entity.events.util.EventState;
import com.l2cccp.gameserver.network.l2.components.ChatType;
import com.l2cccp.gameserver.network.l2.components.CustomMessage;
import com.l2cccp.gameserver.network.l2.components.SystemMsg;
import com.l2cccp.gameserver.network.l2.s2c.Say2;
import com.l2cccp.gameserver.network.l2.s2c.SystemMessage;
import com.l2cccp.gameserver.utils.ItemFunctions;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public abstract class CustomInstantTeamEvent extends TeamEvent
{
	/**
	 * Прослушка игроков на случай вылета или тп с ивент!
	 **/
	private class PlayerListeners implements OnPlayerExitListener, OnTeleportListener
	{
		@Override
		public void onPlayerExit(Player player)
		{
			player.setReflection(ReflectionManager.DEFAULT);
			exitPlayer(player, true);
		}

		@Override
		public void onTeleport(Player player, int x, int y, int z, Reflection r)
		{
			if(state != EventState.NONE && r != getReflection())
				exitPlayer(player, false);
		}

		private void exitPlayer(Player player, boolean exit)
		{
			for(DuelSnapshotObject d : CustomInstantTeamEvent.this)
			{
				if(d.getPlayer() == player)
				{
					quit(d);
					d.getPlayer()._stablePoint = d.getReturnLoc();
					removeObject(d.getTeam(), d);
					checkForWinner();
					break;
				}
			}
		}
	}

	public static final String REGISTRATION = "registration";
	protected PlayerListener _playerListeners = new PlayerListeners();

	protected CustomInstantTeamEvent(MultiValueSet<String> set)
	{
		super(set);
	}

	//region Abstracts
	protected abstract void checkForWinner();

	//endregion

	//region Start&Stop and player actions
	@Override
	public void teleportPlayers(String name)
	{
		setState(EventState.TELEPORT_PLAYERS);
		teamCheck(false);

		if(!checkTeamSize())
		{
			setState(EventState.NO_PLAYERS);
			announceToValidPlayer("event.сancelled");
			stopEvent();
			return;
		}

		setRegistrationOver(true); // посылаем мессагу

		final boolean dispel = getReflection().getInstancedZone().isDispelBuffs();
		for(DuelSnapshotObject object : this)
		{
			Player player = object.getPlayer();

			if(!canWalkInWaitTime())
				paralyze(player);

			object.store();

			if(dispel)
				dispelBuffs(player);

			player._stablePoint = object.getReturnLoc();
			player.teleToLocation(getTeleportLoc(object.getTeam()), getReflection());
		}

	}

	@Override
	public void stopEvent()
	{
		clearActions();

		updatePlayers(false, false);

		if(winner != null && rewardItems != null && state == EventState.FINISH)
		{
			switch(winner)
			{
				case NONE:
					sendPacket(SystemMsg.THE_DUEL_HAS_ENDED_IN_A_TIE);
					break;
				case RED:
				case BLUE:
					List<DuelSnapshotObject> winners = getObjects(winner);

					sendPacket(new SystemMessage(winner == TeamType.RED ? SystemMsg.THE_RED_TEAM_IS_VICTORIOUS : SystemMsg.THE_BLUE_TEAM_IS_VICTORIOUS));

					for(DuelSnapshotObject d : winners)
					{
						for(int i = 0; i < rewardItems.length; i++)
						{
							if(d.getPlayer() != null)
								ItemFunctions.addItem(d.getPlayer(), rewardItems[i], levelMul ? rewardCounts[i] * d.getPlayer().getLevel() : rewardCounts[i]);
						}
					}
					break;
			}
		}

		setState(EventState.NONE);

		updatePlayers(false, true);
		removeObjects(TeamType.RED);
		removeObjects(TeamType.BLUE);

		reCalcNextTime(false);

		super.stopEvent();
	}

	protected boolean addPlayer(Player player)
	{
		List<DuelSnapshotObject> blue = getObjects(TeamType.BLUE);
		List<DuelSnapshotObject> red = getObjects(TeamType.RED);
		TeamType team;
		if(blue.size() == red.size())
			team = Rnd.get(TeamType.VALUES);
		else if(blue.size() > red.size())
			team = TeamType.RED;
		else
			team = TeamType.BLUE;

		addObject(team, new DuelSnapshotObject(player, team, true));

		player.addEvent(this);
		player.addListener(_playerListeners);
		player.sendMessage(new CustomMessage("event.registration").addString(getName()));
		return true;
	}

	@Override
	public boolean isInProgress()
	{
		return state == EventState.FIRST_PHASE;
	}

	@Override
	public void action(String name, boolean start)
	{
		if(name.equalsIgnoreCase(REGISTRATION))
			setRegistrationOver(!start);
		else
			super.action(name, start);
	}

	public boolean isRegistrationOver()
	{
		return state != EventState.REGISTRATION;
	}

	protected void setRegistrationOver(boolean registrationOver)
	{
		if(!registrationOver)
		{
			setState(EventState.REGISTRATION);
			announceToValidPlayer("event.registration.open");
		}
		else
			announceToValidPlayer("event.registration.close");
	}

	@Override
	public void announce(int i)
	{
		switch(i)
		{
			case -10:
			case -5:
			case -4:
			case -3:
			case -2:
			case -1:
			{
				for(DuelSnapshotObject d : this)
				{
					Player player = d.getPlayer();
					if(player != null)
						player.sendPacket(new Say2(0, ChatType.COMMANDCHANNEL_ALL, getName(), new CustomMessage("event.start.countdown").addNumber(-i).toString(player), null));
				}
				break;
			}
		}
	}

	@Override
	public void onRemoveEvent(GameObject o)
	{
		if(o.isPlayer())
			o.getPlayer().removeListener(_playerListeners);
	}
}