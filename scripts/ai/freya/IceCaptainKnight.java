package ai.freya;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.NpcString;
import com.l2cccp.gameserver.network.l2.s2c.ExShowScreenMessage;
import com.l2cccp.gameserver.utils.ChatUtils;
import com.l2cccp.gameserver.utils.Location;

/**
 * @author pchayka
 */

public class IceCaptainKnight extends Fighter
{
	private long _destroyTimer = 0;
	private boolean _destroyUsed = false;
	private boolean _isHard = false;
	Reflection r = _actor.getReflection();

	public IceCaptainKnight(NpcInstance actor)
	{
		super(actor);
		actor.setItemDropEnabled(false);
	}

	@Override
	protected void onEvtSpawn()
	{
		super.onEvtSpawn();
		_destroyTimer = System.currentTimeMillis();
		_isHard = r.getInstancedZoneId() == 144;
		for(Player p : r.getPlayers())
			this.notifyEvent(CtrlEvent.EVT_AGGRESSION, p, 5);
	}

	@Override
	protected void thinkAttack()
	{
		if(!_destroyUsed && _destroyTimer + 60 * 1000L < System.currentTimeMillis())
		{
			_destroyUsed = true;
			int mode = Rnd.get(3);
			if(!r.isDefault())
				for(Player p : r.getPlayers())
					p.sendPacket(new ExShowScreenMessage(NpcString.THE_SPACE_FEELS_LIKE_ITS_GRADUALLY_STARTING_TO_SHAKE, 5000, ExShowScreenMessage.ScreenMessageAlign.MIDDLE_CENTER, true));
			switch(mode)
			{
				case 0:
					ChatUtils.shout(getActor(), NpcString.ARCHER);
					break;
				case 1:
					ChatUtils.shout(getActor(), NpcString.MY_KNIGHTS);
					break;
				case 2:
					ChatUtils.shout(getActor(), NpcString.I_CAN_TAKE_IT_NO_LONGER);
					break;
				case 3:
					ChatUtils.shout(getActor(), NpcString.ARCHER_);
					break;
			}
			int count = _isHard ? 7 : 5;
			for(int i = 0; i < count; i++)
				r.addSpawnWithoutRespawn(_isHard ? 18856 : 18855, Location.findAroundPosition(_actor, 350, 370), _actor.getGeoIndex());
		}

		super.thinkAttack();
	}

	@Override
	protected void teleportHome()
	{
	}
}