package ai.freya;

import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.instances.NpcInstance;

/**
 * @author pchayka
 */

public class IceCastleBreath extends Fighter
{
	public IceCastleBreath(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtSpawn()
	{
		super.onEvtSpawn();
		Reflection r = getActor().getReflection();
		if(r != null && r.getPlayers() != null)
			for(Player p : r.getPlayers())
				this.notifyEvent(CtrlEvent.EVT_AGGRESSION, p, 300);
	}

	@Override
	protected void teleportHome()
	{
	}
}