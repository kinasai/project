package ai.freya;

import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.tables.SkillTable;

import bosses.AntharasManager;

/**
 * @author pchayka
 */

public class AntharasMinion extends Fighter
{
	public AntharasMinion(NpcInstance actor)
	{
		super(actor);
		actor.startDebuffImmunity();
	}

	@Override
	protected void onEvtSpawn()
	{
		super.onEvtSpawn();
		for(Player p : AntharasManager.getZone().getInsidePlayers())
			notifyEvent(CtrlEvent.EVT_AGGRESSION, p, 5000);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		getActor().doCast(SkillTable.getInstance().getSkillEntry(5097, 1), getActor(), true);
		super.onEvtDead(killer);
	}

	@Override
	protected void returnHome(boolean clearAggro, boolean teleport)
	{
		return;
	}
}