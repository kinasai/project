package ai.dragonvalley;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class ai_drakos_warrior extends Fighter
{
	private static SkillEntry summon = SkillTable.getInstance().getSkillEntry(6858, 1);

	public ai_drakos_warrior(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(Creature attacker, SkillEntry skill, int damage)
	{
		super.onEvtAttacked(attacker, skill, damage);

		if(Rnd.chance(Config.DRAKOS_REPRODUCTION_CHANCE))
		{
			for(int i = 0; i < Rnd.get(Config.DRAKOS_REPRODUCTION[0], Config.DRAKOS_REPRODUCTION[1]); i++)
			{
				NpcInstance n = NpcUtils.spawnSingle(22823, Location.coordsRandomize(getActor().getLoc(), 50, 200));
				n.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, 1000);
				addTaskCast(getActor(), summon);
			}
		}
	}
}