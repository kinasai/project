package ai.dragonvalley;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;

/**
 * @author pchayka
 */
public class BloodyMonster extends Fighter
{
	public BloodyMonster(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(Rnd.chance(Config.KARIK_REPRODUCTION_CHANCE))
		{
			for(int i = 0; i < Rnd.get(Config.KARIK_REPRODUCTION[0], Config.KARIK_REPRODUCTION[1]); i++)
			{
				NpcInstance n = NpcUtils.spawnSingle(getActor().getNpcId(), Location.coordsRandomize(getActor().getLoc(), 50, 100));
				n.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, killer, 1000);
			}
		}
	}
}