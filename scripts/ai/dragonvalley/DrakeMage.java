package ai.dragonvalley;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.Mystic;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;

public class DrakeMage extends Mystic
{
	public DrakeMage(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);

		final Player player = killer.getPlayer();
		if (player != null && player.isMageClass())
			if (Rnd.chance(70))
				getActor().dropItem(player, 8603, 1, false);
			else
				getActor().dropItem(player, 8604, 1, false);
	}
}