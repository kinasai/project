package ai.dragonvalley;

import java.util.Map;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Playable;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.PlayerGroup;
import com.l2cccp.gameserver.model.AggroList.HateInfo;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.quest.QuestState;
import com.l2cccp.gameserver.utils.NpcUtils;

/**
 * @author pchayka
 */
public class DrakeBosses extends Fighter
{
	public DrakeBosses(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		NpcInstance corpse = null;
		switch(getActor().getNpcId())
		{
			case 25725:
				corpse = NpcUtils.spawnSingle(32884, getActor().getLoc(), 300000);
				break;
			case 25726:
				corpse = NpcUtils.spawnSingle(32885, getActor().getLoc(), 300000);
				break;
			case 25727:
				corpse = NpcUtils.spawnSingle(32886, getActor().getLoc(), 300000);
				break;
		}

		if (killer != null && corpse != null)
		{
			final Player player = killer.getPlayer();
			if (player != null)
			{
				PlayerGroup pg = player.getPlayerGroup();
				if (pg != null)
				{
					QuestState qs;
					Map<Playable, HateInfo> aggro = getActor().getAggroList().getPlayableMap();
					for (Player pl : pg)
					{
						if (pl != null && !pl.isDead() && aggro.containsKey(pl) && (getActor().isInRangeZ(pl, Config.ALT_PARTY_DISTRIBUTION_RANGE) || getActor().isInRangeZ(killer, Config.ALT_PARTY_DISTRIBUTION_RANGE)))
						{
							qs = pl.getQuestState(456);
							if (qs != null && qs.getCond() == 1)
								qs.set("RaidKilled", corpse.getObjectId());
						}
					}
				}
			}
		}

		super.onEvtDead(killer);
		getActor().endDecayTask();
	}
}