package ai.dragonvalley;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.ai.Mystic;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.utils.NpcUtils;

/**
 * @author pchayka
 *         После смерти призывает одного из двух монстров
 */
public class Necromancer extends Mystic
{
	public Necromancer(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(Rnd.chance(30))
		{
			NpcInstance n = NpcUtils.spawnSingle(Rnd.chance(50) ? 22818 : 22819, getActor().getLoc());
			n.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, killer, 2);
		}
	}
}