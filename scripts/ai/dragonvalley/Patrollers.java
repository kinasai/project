package ai.dragonvalley;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ai.CtrlIntention;
import com.l2cccp.gameserver.model.AggroList;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.templates.moveroute.MoveNode;
import org.apache.commons.lang3.ArrayUtils;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.Location;

import java.util.List;

/**
 * @author pchayka
 */

public class Patrollers extends Fighter
{
	protected Location[] _points = null;
	protected MoveNode _destination = null;
	protected int _currentNodeIndex = 0;
	protected boolean _incPoint = true;

	private int[] _teleporters = {
			22857,
			22833,
			22834
	};

	private int _lastPoint = 0;
	private boolean _firstThought = true;
	private volatile boolean _moving = false;

	public Patrollers(NpcInstance actor)
	{
		super(actor);
	}


	@Override
	public boolean isGlobalAI()
	{
		return true;
	}

	@Override
	protected void thinkAttack()
	{
		NpcInstance actor = getActor();
		if(actor.isDead())
			return;

		if(doTask() && !actor.isAttackingNow() && !actor.isCastingNow())
		{
			if(!createNewTask())
			{
				if(System.currentTimeMillis() > getAttackTimeout())
					returnHome();
			}
		}
	}

	@Override
	protected boolean thinkActive()
	{
		if(super.thinkActive())
			return true;

		if(!getActor().isMoving)
			startMoveTask();

		NpcInstance actor = getActor();
		if(actor.isActionsDisabled())
			return true;

		if(_randomAnimationEnd > System.currentTimeMillis())
			return true;

		if(_def_think)
		{
			if(doTask())
				clearTasks();
			return true;
		}

		long now = System.currentTimeMillis();
		if(now - _checkAggroTimestamp > Config.AGGRO_CHECK_INTERVAL)
		{
			_checkAggroTimestamp = now;

			boolean aggressive = Rnd.chance(actor.getParameter("SelfAggressive", isAggressive() ? 100 : 0));
			if(!actor.getAggroList().isEmpty() || aggressive)
			{
				List<Creature> targets = World.getAroundCharacters(actor);
				while(!targets.isEmpty())
				{
					Creature target = getNearestTarget(targets);
					if(target == null)
						break;

					if(aggressive || actor.getAggroList().get(target) != null)
						if(checkAggression(target))
						{
							actor.getAggroList().addDamageHate(target, 0, 2);

							if(target.isServitor())
								actor.getAggroList().addDamageHate(target.getPlayer(), 0, 1);

							startRunningTask(AI_TASK_ATTACK_DELAY);
							setIntention(CtrlIntention.AI_INTENTION_ATTACK, target);

							return true;
						}

					targets.remove(target);
				}
			}
		}

		return true;
	}


	private void startMoveTask()
	{
		if(_moving)
			return;

		try
		{
			_moving = true;

			NpcInstance npc = getActor();
			if(_firstThought)
			{
				_lastPoint = getIndex(Location.findNearest(npc, _points));
				_firstThought = false;
			}
			else
				_lastPoint++;

			if(_lastPoint >= _points.length)
			{
				_lastPoint = 0;
				if(ArrayUtils.contains(_teleporters, npc.getNpcId()))
					npc.teleToLocation(_points[_lastPoint]);
			}

			npc.setRunning();
			if(Rnd.chance(30))
				npc.altOnMagicUseTimer(npc, SkillTable.getInstance().getSkillEntry(6757, 1));
			addTaskMove(Location.findPointToStay(_points[_lastPoint], 250, npc.getGeoIndex()), true);
			doTask();
		}
		finally
		{
			_moving = false;
		}
	}

	private int getIndex(Location loc)
	{
		for(int i = 0; i < _points.length; i++)
			if(_points[i] == loc)
				return i;
		return 0;
	}


	protected boolean isInAggroRange(Creature target)
	{
		if(_points == null)
			return super.isInAggroRange(target);

		final NpcInstance actor = getActor();
		final AggroList.AggroInfo ai = actor.getAggroList().get(target);
		if(ai != null && ai.hate > 0)
		{
			final Location loc = _destination != null ? _destination : actor.getSpawnedLoc();
			if(!target.isInRangeZ(loc, MAX_PURSUE_RANGE))
				return false;
		}
		else if(!target.isInRangeZ(actor.getLoc(), actor.getAggroRange()))
			return false;



		return true;
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

	@Override
	protected boolean maybeMoveToHome()
	{
		return false;
	}

	@Override
	protected void teleportHome()
	{
	}

	@Override
	protected void returnHome(boolean clearAggro, boolean teleport)
	{
		super.returnHome(clearAggro, teleport);
		clearTasks();
		_firstThought = true;
		startMoveTask();
	}
}
