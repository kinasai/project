package ai.moveroute;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;

/**
 * @author VISTALL
 * @date 22:38/24.10.2011
 */
public class NotAggressiveNpc extends MoveRoutDefaultAI
{
	public NotAggressiveNpc(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(Creature attacker, SkillEntry skill, int damage)
	{
		//
	}

	@Override
	protected void onEvtAggression(Creature target, int aggro)
	{
		//
	}
}
