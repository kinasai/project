package ai.hellbound;

import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.World;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.utils.ChatUtils;

public class OutpostCaptain extends Fighter
{
	private boolean _attacked = false;

	public OutpostCaptain(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(Creature attacker, SkillEntry skill, int damage)
	{
		if(attacker == null || attacker.getPlayer() == null)
			return;

		for(NpcInstance minion : World.getAroundNpc(getActor(), 3000, 2000))
			if(minion.getNpcId() == 22358 || minion.getNpcId() == 22357)
				minion.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, 5000);

		if(!_attacked)
		{
			ChatUtils.say(getActor(), "Fool, you and your friends will die! Attack!");
			_attacked = true;
		}
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}

}