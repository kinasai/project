package ai.hellbound;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.utils.ChatUtils;

/**
 * AI Tortured Native в городе-инстанте на Hellbound<br>
 * - периодически кричат
 *
 * @author SYS
 */
public class TorturedNative extends Fighter
{
	public TorturedNative(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected boolean thinkActive()
	{
		NpcInstance actor = getActor();
		if(actor.isDead())
			return true;

		if(Rnd.chance(1))
			if(Rnd.chance(10))
				ChatUtils.say(actor, "Eeeek... I feel sick... yow...!");
			else
				ChatUtils.say(actor, "It... will... kill... everyone...!");

		return super.thinkActive();
	}
}