package ai.chest;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;

import npc.model.TreasureChestInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Treasure45 extends CharacterAI
{
	private final static int chance = 10000;

	public Treasure45(Creature actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer == null || !killer.isPlayer())
			return;

		final Player player = killer.getPlayer();
		TreasureChestInstance chest = (TreasureChestInstance) _actor;

		int random = Rnd.get(chance);
		if(random < 7662)
			chest.dropItem(player, 736, 5, false);
		random = Rnd.get(chance);
		if(random < 4789)
			chest.dropItem(player, 1061, 4, false);
		random = Rnd.get(chance);
		if(random < 2554)
			chest.dropItem(player, 737, 3, false);
		random = Rnd.get(chance);
		if(random < 7662)
			chest.dropItem(player, 1539, 5, false);
		random = Rnd.get(chance);
		if(random < 3831)
			chest.dropItem(player, 8625, 2, false);
		random = Rnd.get(chance);
		if(random < 3303)
			chest.dropItem(player, 8631, 2, false);
		random = Rnd.get(chance);
		if(random < 6385)
			chest.dropItem(player, 8637, 3, false);
		random = Rnd.get(chance);
		if(random < 6385)
			chest.dropItem(player, 8636, 4, false);
		random = Rnd.get(chance);
		if(random < 4404)
			chest.dropItem(player, 8630, 2, false);
		random = Rnd.get(chance);
		if(random < 5321)
			chest.dropItem(player, 8624, 2, false);
		random = Rnd.get(chance);
		if(random < 5894)
			chest.dropItem(player, 10260, 1, false);
		random = Rnd.get(chance);
		if(random < 5894)
			chest.dropItem(player, 10261, 1, false);
		random = Rnd.get(chance);
		if(random < 5894)
			chest.dropItem(player, 10262, 1, false);
		random = Rnd.get(chance);
		if(random < 5894)
			chest.dropItem(player, 10263, 1, false);
		random = Rnd.get(chance);
		if(random < 5894)
			chest.dropItem(player, 10264, 1, false);
		random = Rnd.get(chance);
		if(random < 5894)
			chest.dropItem(player, 10265, 1, false);
		random = Rnd.get(chance);
		if(random < 5894)
			chest.dropItem(player, 10266, 1, false);
		random = Rnd.get(chance);
		if(random < 5894)
			chest.dropItem(player, 10267, 1, false);
		random = Rnd.get(chance);
		if(random < 5894)
			chest.dropItem(player, 10268, 1, false);
		random = Rnd.get(chance);
		if(random < 8186)
			chest.dropItem(player, 5593, 9, false);
		random = Rnd.get(chance);
		if(random < 7367)
			chest.dropItem(player, 5594, 2, false);
		random = Rnd.get(chance);
		if(random < 737)
			chest.dropItem(player, 5595, 1, false);
		random = Rnd.get(chance);
		if(random < 5894)
			chest.dropItem(player, 10269, 1, false);
		random = Rnd.get(chance);
		if(random < 6226)
			chest.dropItem(player, 10137, 1, false);
		random = Rnd.get(chance);
		if(random < 6226)
			chest.dropItem(player, 10138, 1, false);
		random = Rnd.get(chance);
		if(random < 8301)
			chest.dropItem(player, 1538, 1, false);
		random = Rnd.get(chance);
		if(random < 3113)
			chest.dropItem(player, 3936, 1, false);
		random = Rnd.get(chance);
		if(random < 2491)
			chest.dropItem(player, 5577, 1, false);
		random = Rnd.get(chance);
		if(random < 2491)
			chest.dropItem(player, 5578, 1, false);
		random = Rnd.get(chance);
		if(random < 2491)
			chest.dropItem(player, 5579, 1, false);
		random = Rnd.get(chance);
		if(random < 1437)
			chest.dropItem(player, 70, 1, false);
		random = Rnd.get(chance);
		if(random < 1080)
			chest.dropItem(player, 21747, 1, false);
	}
}