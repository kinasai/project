package ai.chest;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;

import npc.model.TreasureChestInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Treasure24 extends CharacterAI
{
	private final static int chance = 10000;

	public Treasure24(Creature actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer == null || !killer.isPlayer())
			return;

		final Player player = killer.getPlayer();
		TreasureChestInstance chest = (TreasureChestInstance) _actor;

		int random = Rnd.get(chance);
		if(random < 3159)
			chest.dropItem(player, 736, 7, false);
		random = Rnd.get(chance);
		if(random < 2764)
			chest.dropItem(player, 1061, 4, false);
		random = Rnd.get(chance);
		if(random < 4422)
			chest.dropItem(player, 737, 4, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 10260, 1, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 10261, 1, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 10262, 1, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 10263, 1, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 10264, 1, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 10265, 1, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 10266, 1, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 10267, 1, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 10268, 1, false);
		random = Rnd.get(chance);
		if(random < 2764)
			chest.dropItem(player, 5593, 6, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 5594, 1, false);
		random = Rnd.get(chance);
		if(random < 1327)
			chest.dropItem(player, 10269, 1, false);
		random = Rnd.get(chance);
		if(random < 5749)
			chest.dropItem(player, 10131, 1, false);
		random = Rnd.get(chance);
		if(random < 5749)
			chest.dropItem(player, 10132, 1, false);
		random = Rnd.get(chance);
		if(random < 5749)
			chest.dropItem(player, 10133, 1, false);
		random = Rnd.get(chance);
		if(random < 3833)
			chest.dropItem(player, 1538, 1, false);
		random = Rnd.get(chance);
		if(random < 1438)
			chest.dropItem(player, 3936, 1, false);
		random = Rnd.get(chance);
		if(random < 3058)
			chest.dropItem(player, 68, 1, false);
		random = Rnd.get(chance);
		if(random < 374)
			chest.dropItem(player, 21747, 1, false);
	}
}