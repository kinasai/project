package ai.chest;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;

import npc.model.TreasureChestInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Treasure81 extends CharacterAI
{
	private final static int chance = 10000;

	public Treasure81(Creature actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer == null || !killer.isPlayer())
			return;

		final Player player = killer.getPlayer();
		TreasureChestInstance chest = (TreasureChestInstance) _actor;

		int random = Rnd.get(chance);
		if(random < 7420)
			chest.dropItem(player, 8627, 2, false);
		random = Rnd.get(chance);
		if(random < 6625)
			chest.dropItem(player, 8633, 2, false);
		random = Rnd.get(chance);
		if(random < chance)
			chest.dropItem(player, 8639, 4, false);
		random = Rnd.get(chance);
		if(random < 891)
			chest.dropItem(player, 9546, 1, false);
		random = Rnd.get(chance);
		if(random < 891)
			chest.dropItem(player, 9547, 1, false);
		random = Rnd.get(chance);
		if(random < 891)
			chest.dropItem(player, 9548, 1, false);
		random = Rnd.get(chance);
		if(random < 891)
			chest.dropItem(player, 9549, 1, false);
		random = Rnd.get(chance);
		if(random < 891)
			chest.dropItem(player, 9550, 1, false);
		random = Rnd.get(chance);
		if(random < 891)
			chest.dropItem(player, 9551, 1, false);
		random = Rnd.get(chance);
		if(random < 45)
			chest.dropItem(player, 959, 1, false);
		random = Rnd.get(chance);
		if(random < 446)
			chest.dropItem(player, 960, 1, false);
		random = Rnd.get(chance);
		if(random < 2226)
			chest.dropItem(player, 14701, 2, false);
		random = Rnd.get(chance);
		if(random < 3339)
			chest.dropItem(player, 10260, 3, false);
		random = Rnd.get(chance);
		if(random < 3339)
			chest.dropItem(player, 10261, 3, false);
		random = Rnd.get(chance);
		if(random < 3339)
			chest.dropItem(player, 10262, 3, false);
		random = Rnd.get(chance);
		if(random < 3339)
			chest.dropItem(player, 10263, 3, false);
		random = Rnd.get(chance);
		if(random < 3339)
			chest.dropItem(player, 10264, 3, false);
		random = Rnd.get(chance);
		if(random < 3339)
			chest.dropItem(player, 10265, 3, false);
		random = Rnd.get(chance);
		if(random < 3339)
			chest.dropItem(player, 10266, 3, false);
		random = Rnd.get(chance);
		if(random < 3339)
			chest.dropItem(player, 10267, 3, false);
		random = Rnd.get(chance);
		if(random < 3339)
			chest.dropItem(player, 10268, 3, false);
		random = Rnd.get(chance);
		if(random < 627)
			chest.dropItem(player, 5595, 2, false);
		random = Rnd.get(chance);
		if(random < 251)
			chest.dropItem(player, 9898, 1, false);
		random = Rnd.get(chance);
		if(random < 126)
			chest.dropItem(player, 17185, 1, false);
		random = Rnd.get(chance);
		if(random < 3339)
			chest.dropItem(player, 10269, 3, false);
		random = Rnd.get(chance);
		if(random < 4348)
			chest.dropItem(player, 9574, 1, false);
		random = Rnd.get(chance);
		if(random < 3623)
			chest.dropItem(player, 10484, 1, false);
		random = Rnd.get(chance);
		if(random < 3021)
			chest.dropItem(player, 14167, 1, false);
		random = Rnd.get(chance);
		if(random < 2756)
			chest.dropItem(player, 21185, 1, false);
		random = Rnd.get(chance);
		if(random < 1654)
			chest.dropItem(player, 21186, 1, false);
		random = Rnd.get(chance);
		if(random < 2363)
			chest.dropItem(player, 21187, 1, false);
		random = Rnd.get(chance);
		if(random < 2363)
			chest.dropItem(player, 21188, 1, false);
		random = Rnd.get(chance);
		if(random < 2363)
			chest.dropItem(player, 21189, 1, false);
		random = Rnd.get(chance);
		if(random < 2363)
			chest.dropItem(player, 21190, 1, false);
		random = Rnd.get(chance);
		if(random < 2363)
			chest.dropItem(player, 21191, 1, false);
		random = Rnd.get(chance);
		if(random < 207)
			chest.dropItem(player, 9552, 1, false);
		random = Rnd.get(chance);
		if(random < 207)
			chest.dropItem(player, 9553, 1, false);
		random = Rnd.get(chance);
		if(random < 207)
			chest.dropItem(player, 9554, 1, false);
		random = Rnd.get(chance);
		if(random < 207)
			chest.dropItem(player, 9555, 1, false);
		random = Rnd.get(chance);
		if(random < 207)
			chest.dropItem(player, 9556, 1, false);
		random = Rnd.get(chance);
		if(random < 207)
			chest.dropItem(player, 9557, 1, false);
		random = Rnd.get(chance);
		if(random < 3308)
			chest.dropItem(player, 6622, 1, false);
		random = Rnd.get(chance);
		if(random < 207)
			chest.dropItem(player, 9627, 1, false);
		random = Rnd.get(chance);
		if(random < 5512)
			chest.dropItem(player, 1538, 2, false);
		random = Rnd.get(chance);
		if(random < 4134)
			chest.dropItem(player, 3936, 1, false);
		random = Rnd.get(chance);
		if(random < 21)
			chest.dropItem(player, 10480, 1, false);
		random = Rnd.get(chance);
		if(random < 21)
			chest.dropItem(player, 10482, 1, false);
		random = Rnd.get(chance);
		if(random < 21)
			chest.dropItem(player, 10481, 1, false);
		random = Rnd.get(chance);
		if(random < 16)
			chest.dropItem(player, 10215, 1, false);
		random = Rnd.get(chance);
		if(random < 27)
			chest.dropItem(player, 21749, 1, false);
	}
}