package ai.chest;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;

import npc.model.TreasureChestInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Treasure57 extends CharacterAI
{
	private final static int chance = 10000;

	public Treasure57(Creature actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer == null || !killer.isPlayer())
			return;

		final Player player = killer.getPlayer();
		TreasureChestInstance chest = (TreasureChestInstance) _actor;

		int random = Rnd.get(chance);
		if(random < 8657)
			chest.dropItem(player, 736, 8, false);
		random = Rnd.get(chance);
		if(random < 8657)
			chest.dropItem(player, 1061, 4, false);
		random = Rnd.get(chance);
		if(random < 4617)
			chest.dropItem(player, 737, 3, false);
		random = Rnd.get(chance);
		if(random < 6926)
			chest.dropItem(player, 8625, 2, false);
		random = Rnd.get(chance);
		if(random < 5971)
			chest.dropItem(player, 8631, 2, false);
		random = Rnd.get(chance);
		if(random < 8657)
			chest.dropItem(player, 8637, 4, false);
		random = Rnd.get(chance);
		if(random < 9234)
			chest.dropItem(player, 8638, 3, false);
		random = Rnd.get(chance);
		if(random < 4810)
			chest.dropItem(player, 8632, 2, false);
		random = Rnd.get(chance);
		if(random < 5541)
			chest.dropItem(player, 8626, 2, false);
		random = Rnd.get(chance);
		if(random < 4987)
			chest.dropItem(player, 10260, 1, false);
		random = Rnd.get(chance);
		if(random < 4987)
			chest.dropItem(player, 10261, 1, false);
		random = Rnd.get(chance);
		if(random < 4987)
			chest.dropItem(player, 10262, 1, false);
		random = Rnd.get(chance);
		if(random < 4987)
			chest.dropItem(player, 10263, 1, false);
		random = Rnd.get(chance);
		if(random < 4987)
			chest.dropItem(player, 10264, 1, false);
		random = Rnd.get(chance);
		if(random < 4987)
			chest.dropItem(player, 10265, 1, false);
		random = Rnd.get(chance);
		if(random < 4987)
			chest.dropItem(player, 10266, 1, false);
		random = Rnd.get(chance);
		if(random < 4987)
			chest.dropItem(player, 10267, 1, false);
		random = Rnd.get(chance);
		if(random < 4987)
			chest.dropItem(player, 10268, 1, false);
		random = Rnd.get(chance);
		if(random < 6233)
			chest.dropItem(player, 5594, 2, false);
		random = Rnd.get(chance);
		if(random < 624)
			chest.dropItem(player, 5595, 1, false);
		random = Rnd.get(chance);
		if(random < 4987)
			chest.dropItem(player, 10269, 1, false);
		random = Rnd.get(chance);
		if(random < 7214)
			chest.dropItem(player, 8736, 1, false);
		random = Rnd.get(chance);
		if(random < 6233)
			chest.dropItem(player, 8737, 1, false);
		random = Rnd.get(chance);
		if(random < 5195)
			chest.dropItem(player, 8738, 1, false);
		random = Rnd.get(chance);
		if(random < 5402)
			chest.dropItem(player, 21183, 1, false);
		random = Rnd.get(chance);
		if(random < 5402)
			chest.dropItem(player, 21184, 1, false);
		random = Rnd.get(chance);
		if(random < 5402)
			chest.dropItem(player, 1538, 2, false);
		random = Rnd.get(chance);
		if(random < 4052)
			chest.dropItem(player, 3936, 1, false);
		random = Rnd.get(chance);
		if(random < 751)
			chest.dropItem(player, 9648, 1, false);
		random = Rnd.get(chance);
		if(random < 901)
			chest.dropItem(player, 9649, 1, false);
		random = Rnd.get(chance);
		if(random < 163)
			chest.dropItem(player, 5580, 1, false);
		random = Rnd.get(chance);
		if(random < 163)
			chest.dropItem(player, 5581, 1, false);
		random = Rnd.get(chance);
		if(random < 163)
			chest.dropItem(player, 5582, 1, false);
		random = Rnd.get(chance);
		if(random < 161)
			chest.dropItem(player, 79, 1, false);
		random = Rnd.get(chance);
		if(random < 103)
			chest.dropItem(player, 21748, 1, false);
	}
}