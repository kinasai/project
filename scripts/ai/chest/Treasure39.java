package ai.chest;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;

import npc.model.TreasureChestInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Treasure39 extends CharacterAI
{
	private final static int chance = 10000;

	public Treasure39(Creature actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer == null || !killer.isPlayer())
			return;

		final Player player = killer.getPlayer();
		TreasureChestInstance chest = (TreasureChestInstance) _actor;

		int random = Rnd.get(chance);
		if(random < 6879)
			chest.dropItem(player, 736, 7, false);
		random = Rnd.get(chance);
		if(random < 6019)
			chest.dropItem(player, 1061, 4, false);
		random = Rnd.get(chance);
		if(random < 9630)
			chest.dropItem(player, 737, 4, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 10260, 1, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 10261, 1, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 10262, 1, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 10263, 1, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 10264, 1, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 10265, 1, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 10266, 1, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 10267, 1, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 10268, 1, false);
		random = Rnd.get(chance);
		if(random < 6019)
			chest.dropItem(player, 5593, 6, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 5594, 1, false);
		random = Rnd.get(chance);
		if(random < 2889)
			chest.dropItem(player, 10269, 1, false);
		random = Rnd.get(chance);
		if(random < 8346)
			chest.dropItem(player, 10134, 1, false);
		random = Rnd.get(chance);
		if(random < 8346)
			chest.dropItem(player, 10135, 1, false);
		random = Rnd.get(chance);
		if(random < 8346)
			chest.dropItem(player, 10136, 1, false);
		random = Rnd.get(chance);
		if(random < 8346)
			chest.dropItem(player, 1538, 1, false);
		random = Rnd.get(chance);
		if(random < 3130)
			chest.dropItem(player, 3936, 1, false);
		random = Rnd.get(chance);
		if(random < 2527)
			chest.dropItem(player, 69, 1, false);
		random = Rnd.get(chance);
		if(random < 815)
			chest.dropItem(player, 21747, 1, false);
	}
}