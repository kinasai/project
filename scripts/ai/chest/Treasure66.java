package ai.chest;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;

import npc.model.TreasureChestInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Treasure66 extends CharacterAI
{
	private final static int chance = 10000;

	public Treasure66(Creature actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer == null || !killer.isPlayer())
			return;

		final Player player = killer.getPlayer();
		TreasureChestInstance chest = (TreasureChestInstance) _actor;

		int random = Rnd.get(chance);
		if(random < 6323)
			chest.dropItem(player, 8627, 2, false);
		random = Rnd.get(chance);
		if(random < 5646)
			chest.dropItem(player, 8633, 2, false);
		random = Rnd.get(chance);
		if(random < 6323)
			chest.dropItem(player, 8639, 5, false);
		random = Rnd.get(chance);
		if(random < 6323)
			chest.dropItem(player, 8638, 6, false);
		random = Rnd.get(chance);
		if(random < 6587)
			chest.dropItem(player, 8632, 2, false);
		random = Rnd.get(chance);
		if(random < 5059)
			chest.dropItem(player, 8626, 3, false);
		random = Rnd.get(chance);
		if(random < 106)
			chest.dropItem(player, 729, 1, false);
		random = Rnd.get(chance);
		if(random < 791)
			chest.dropItem(player, 730, 1, false);
		random = Rnd.get(chance);
		if(random < 4742)
			chest.dropItem(player, 1540, 4, false);
		random = Rnd.get(chance);
		if(random < 2134)
			chest.dropItem(player, 10260, 3, false);
		random = Rnd.get(chance);
		if(random < 2134)
			chest.dropItem(player, 10261, 3, false);
		random = Rnd.get(chance);
		if(random < 2134)
			chest.dropItem(player, 10262, 3, false);
		random = Rnd.get(chance);
		if(random < 2134)
			chest.dropItem(player, 10263, 3, false);
		random = Rnd.get(chance);
		if(random < 2134)
			chest.dropItem(player, 10264, 3, false);
		random = Rnd.get(chance);
		if(random < 2134)
			chest.dropItem(player, 10265, 3, false);
		random = Rnd.get(chance);
		if(random < 2134)
			chest.dropItem(player, 10266, 3, false);
		random = Rnd.get(chance);
		if(random < 2134)
			chest.dropItem(player, 10267, 3, false);
		random = Rnd.get(chance);
		if(random < 2134)
			chest.dropItem(player, 10268, 3, false);
		random = Rnd.get(chance);
		if(random < 801)
			chest.dropItem(player, 5595, 1, false);
		random = Rnd.get(chance);
		if(random < 801)
			chest.dropItem(player, 9898, 1, false);
		random = Rnd.get(chance);
		if(random < 2134)
			chest.dropItem(player, 10269, 3, false);
		random = Rnd.get(chance);
		if(random < 5335)
			chest.dropItem(player, 8739, 1, false);
		random = Rnd.get(chance);
		if(random < 4446)
			chest.dropItem(player, 8740, 1, false);
		random = Rnd.get(chance);
		if(random < 3705)
			chest.dropItem(player, 8741, 1, false);
		random = Rnd.get(chance);
		if(random < 3335)
			chest.dropItem(player, 8742, 1, false);
		random = Rnd.get(chance);
		if(random < 10088)
			chest.dropItem(player, 21180, 1, false);
		random = Rnd.get(chance);
		if(random < 8070)
			chest.dropItem(player, 21181, 1, false);
		random = Rnd.get(chance);
		if(random < 8070)
			chest.dropItem(player, 21182, 1, false);
		random = Rnd.get(chance);
		if(random < 6725)
			chest.dropItem(player, 1538, 2, false);
		random = Rnd.get(chance);
		if(random < 5044)
			chest.dropItem(player, 3936, 1, false);
		random = Rnd.get(chance);
		if(random < 935)
			chest.dropItem(player, 9654, 1, false);
		random = Rnd.get(chance);
		if(random < 935)
			chest.dropItem(player, 9655, 1, false);
		random = Rnd.get(chance);
		if(random < 202)
			chest.dropItem(player, 5580, 1, false);
		random = Rnd.get(chance);
		if(random < 202)
			chest.dropItem(player, 5581, 1, false);
		random = Rnd.get(chance);
		if(random < 202)
			chest.dropItem(player, 5582, 1, false);
		random = Rnd.get(chance);
		if(random < 144)
			chest.dropItem(player, 80, 1, false);
		random = Rnd.get(chance);
		if(random < 141)
			chest.dropItem(player, 21748, 1, false);
	}
}