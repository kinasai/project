package ai.chest;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;

import npc.model.TreasureChestInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Treasure33 extends CharacterAI
{
	private final static int chance = 10000;

	public Treasure33(Creature actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer == null || !killer.isPlayer())
			return;

		final Player player = killer.getPlayer();
		TreasureChestInstance chest = (TreasureChestInstance) _actor;

		int random = Rnd.get(chance);
		if(random < 5010)
			chest.dropItem(player, 736, 7, false);
		random = Rnd.get(chance);
		if(random < 4383)
			chest.dropItem(player, 1061, 4, false);
		random = Rnd.get(chance);
		if(random < 7013)
			chest.dropItem(player, 737, 4, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 10260, 1, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 10261, 1, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 10262, 1, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 10263, 1, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 10264, 1, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 10265, 1, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 10266, 1, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 10267, 1, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 10268, 1, false);
		random = Rnd.get(chance);
		if(random < 4383)
			chest.dropItem(player, 5593, 6, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 5594, 1, false);
		random = Rnd.get(chance);
		if(random < 2104)
			chest.dropItem(player, 10269, 1, false);
		random = Rnd.get(chance);
		if(random < 6078)
			chest.dropItem(player, 10134, 1, false);
		random = Rnd.get(chance);
		if(random < 6078)
			chest.dropItem(player, 10135, 1, false);
		random = Rnd.get(chance);
		if(random < 6078)
			chest.dropItem(player, 10136, 1, false);
		random = Rnd.get(chance);
		if(random < 6078)
			chest.dropItem(player, 1538, 1, false);
		random = Rnd.get(chance);
		if(random < 2280)
			chest.dropItem(player, 3936, 1, false);
		random = Rnd.get(chance);
		if(random < 1840)
			chest.dropItem(player, 69, 1, false);
		random = Rnd.get(chance);
		if(random < 593)
			chest.dropItem(player, 21747, 1, false);
	}
}