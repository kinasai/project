package ai.chest;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;

import npc.model.TreasureChestInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Treasure48 extends CharacterAI
{
	private final static int chance = 10000;

	public Treasure48(Creature actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer == null || !killer.isPlayer())
			return;

		final Player player = killer.getPlayer();
		TreasureChestInstance chest = (TreasureChestInstance) _actor;

		int random = Rnd.get(chance);
		if(random < 8719)
			chest.dropItem(player, 736, 5, false);
		random = Rnd.get(chance);
		if(random < 5450)
			chest.dropItem(player, 1061, 4, false);
		random = Rnd.get(chance);
		if(random < 2907)
			chest.dropItem(player, 737, 3, false);
		random = Rnd.get(chance);
		if(random < 8719)
			chest.dropItem(player, 1539, 5, false);
		random = Rnd.get(chance);
		if(random < 4360)
			chest.dropItem(player, 8625, 2, false);
		random = Rnd.get(chance);
		if(random < 3759)
			chest.dropItem(player, 8631, 2, false);
		random = Rnd.get(chance);
		if(random < 7266)
			chest.dropItem(player, 8637, 3, false);
		random = Rnd.get(chance);
		if(random < 7266)
			chest.dropItem(player, 8636, 4, false);
		random = Rnd.get(chance);
		if(random < 5011)
			chest.dropItem(player, 8630, 2, false);
		random = Rnd.get(chance);
		if(random < 6055)
			chest.dropItem(player, 8624, 2, false);
		random = Rnd.get(chance);
		if(random < 6707)
			chest.dropItem(player, 10260, 1, false);
		random = Rnd.get(chance);
		if(random < 6707)
			chest.dropItem(player, 10261, 1, false);
		random = Rnd.get(chance);
		if(random < 6707)
			chest.dropItem(player, 10262, 1, false);
		random = Rnd.get(chance);
		if(random < 6707)
			chest.dropItem(player, 10263, 1, false);
		random = Rnd.get(chance);
		if(random < 6707)
			chest.dropItem(player, 10264, 1, false);
		random = Rnd.get(chance);
		if(random < 6707)
			chest.dropItem(player, 10265, 1, false);
		random = Rnd.get(chance);
		if(random < 6707)
			chest.dropItem(player, 10266, 1, false);
		random = Rnd.get(chance);
		if(random < 6707)
			chest.dropItem(player, 10267, 1, false);
		random = Rnd.get(chance);
		if(random < 6707)
			chest.dropItem(player, 10268, 1, false);
		random = Rnd.get(chance);
		if(random < 9315)
			chest.dropItem(player, 5593, 9, false);
		random = Rnd.get(chance);
		if(random < 8384)
			chest.dropItem(player, 5594, 2, false);
		random = Rnd.get(chance);
		if(random < 839)
			chest.dropItem(player, 5595, 1, false);
		random = Rnd.get(chance);
		if(random < 6707)
			chest.dropItem(player, 10269, 1, false);
		random = Rnd.get(chance);
		if(random < 7084)
			chest.dropItem(player, 21180, 1, false);
		random = Rnd.get(chance);
		if(random < 5668)
			chest.dropItem(player, 21181, 1, false);
		random = Rnd.get(chance);
		if(random < 9446)
			chest.dropItem(player, 1538, 1, false);
		random = Rnd.get(chance);
		if(random < 3542)
			chest.dropItem(player, 3936, 1, false);
		random = Rnd.get(chance);
		if(random < 2834)
			chest.dropItem(player, 5577, 1, false);
		random = Rnd.get(chance);
		if(random < 2834)
			chest.dropItem(player, 5578, 1, false);
		random = Rnd.get(chance);
		if(random < 2834)
			chest.dropItem(player, 5579, 1, false);
		random = Rnd.get(chance);
		if(random < 481)
			chest.dropItem(player, 135, 1, false);
		random = Rnd.get(chance);
		if(random < 1229)
			chest.dropItem(player, 21747, 1, false);
	}
}