package ai.chest;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;

import npc.model.TreasureChestInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Treasure60 extends CharacterAI
{
	private final static int chance = 10000;

	public Treasure60(Creature actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer == null || !killer.isPlayer())
			return;

		final Player player = killer.getPlayer();
		TreasureChestInstance chest = (TreasureChestInstance) _actor;

		int random = Rnd.get(chance);
		if(random < 9646)
			chest.dropItem(player, 736, 8, false);
		random = Rnd.get(chance);
		if(random < 9646)
			chest.dropItem(player, 1061, 4, false);
		random = Rnd.get(chance);
		if(random < 5145)
			chest.dropItem(player, 737, 3, false);
		random = Rnd.get(chance);
		if(random < 7717)
			chest.dropItem(player, 8625, 2, false);
		random = Rnd.get(chance);
		if(random < 6652)
			chest.dropItem(player, 8631, 2, false);
		random = Rnd.get(chance);
		if(random < 9646)
			chest.dropItem(player, 8637, 4, false);
		random = Rnd.get(chance);
		if(random < 10289)
			chest.dropItem(player, 8638, 3, false);
		random = Rnd.get(chance);
		if(random < 5359)
			chest.dropItem(player, 8632, 2, false);
		random = Rnd.get(chance);
		if(random < 6173)
			chest.dropItem(player, 8626, 2, false);
		random = Rnd.get(chance);
		if(random < 5556)
			chest.dropItem(player, 10260, 1, false);
		random = Rnd.get(chance);
		if(random < 5556)
			chest.dropItem(player, 10261, 1, false);
		random = Rnd.get(chance);
		if(random < 5556)
			chest.dropItem(player, 10262, 1, false);
		random = Rnd.get(chance);
		if(random < 5556)
			chest.dropItem(player, 10263, 1, false);
		random = Rnd.get(chance);
		if(random < 5556)
			chest.dropItem(player, 10264, 1, false);
		random = Rnd.get(chance);
		if(random < 5556)
			chest.dropItem(player, 10265, 1, false);
		random = Rnd.get(chance);
		if(random < 5556)
			chest.dropItem(player, 10266, 1, false);
		random = Rnd.get(chance);
		if(random < 5556)
			chest.dropItem(player, 10267, 1, false);
		random = Rnd.get(chance);
		if(random < 5556)
			chest.dropItem(player, 10268, 1, false);
		random = Rnd.get(chance);
		if(random < 6945)
			chest.dropItem(player, 5594, 2, false);
		random = Rnd.get(chance);
		if(random < 695)
			chest.dropItem(player, 5595, 1, false);
		random = Rnd.get(chance);
		if(random < 5556)
			chest.dropItem(player, 10269, 1, false);
		random = Rnd.get(chance);
		if(random < 8038)
			chest.dropItem(player, 8736, 1, false);
		random = Rnd.get(chance);
		if(random < 6945)
			chest.dropItem(player, 8737, 1, false);
		random = Rnd.get(chance);
		if(random < 5788)
			chest.dropItem(player, 8738, 1, false);
		random = Rnd.get(chance);
		if(random < 6019)
			chest.dropItem(player, 21183, 1, false);
		random = Rnd.get(chance);
		if(random < 6019)
			chest.dropItem(player, 21184, 1, false);
		random = Rnd.get(chance);
		if(random < 6019)
			chest.dropItem(player, 1538, 2, false);
		random = Rnd.get(chance);
		if(random < 4514)
			chest.dropItem(player, 3936, 1, false);
		random = Rnd.get(chance);
		if(random < 836)
			chest.dropItem(player, 9648, 1, false);
		random = Rnd.get(chance);
		if(random < 1004)
			chest.dropItem(player, 9649, 1, false);
		random = Rnd.get(chance);
		if(random < 181)
			chest.dropItem(player, 5580, 1, false);
		random = Rnd.get(chance);
		if(random < 181)
			chest.dropItem(player, 5581, 1, false);
		random = Rnd.get(chance);
		if(random < 181)
			chest.dropItem(player, 5582, 1, false);
		random = Rnd.get(chance);
		if(random < 179)
			chest.dropItem(player, 79, 1, false);
		random = Rnd.get(chance);
		if(random < 115)
			chest.dropItem(player, 21748, 1, false);
	}
}