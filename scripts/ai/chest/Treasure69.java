package ai.chest;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CharacterAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;

import npc.model.TreasureChestInstance;

/**
 * @author L2CCCP
 * @site http://l2cccp.com/
 */
public class Treasure69 extends CharacterAI
{
	private final static int chance = 10000;

	public Treasure69(Creature actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer == null || !killer.isPlayer())
			return;

		final Player player = killer.getPlayer();
		TreasureChestInstance chest = (TreasureChestInstance) _actor;

		int random = Rnd.get(chance);
		if(random < 6967)
			chest.dropItem(player, 8627, 2, false);
		random = Rnd.get(chance);
		if(random < 6220)
			chest.dropItem(player, 8633, 2, false);
		random = Rnd.get(chance);
		if(random < 6967)
			chest.dropItem(player, 8639, 5, false);
		random = Rnd.get(chance);
		if(random < 6967)
			chest.dropItem(player, 8638, 6, false);
		random = Rnd.get(chance);
		if(random < 7257)
			chest.dropItem(player, 8632, 2, false);
		random = Rnd.get(chance);
		if(random < 5573)
			chest.dropItem(player, 8626, 3, false);
		random = Rnd.get(chance);
		if(random < 117)
			chest.dropItem(player, 729, 1, false);
		random = Rnd.get(chance);
		if(random < 871)
			chest.dropItem(player, 730, 1, false);
		random = Rnd.get(chance);
		if(random < 5225)
			chest.dropItem(player, 1540, 4, false);
		random = Rnd.get(chance);
		if(random < 2352)
			chest.dropItem(player, 10260, 3, false);
		random = Rnd.get(chance);
		if(random < 2352)
			chest.dropItem(player, 10261, 3, false);
		random = Rnd.get(chance);
		if(random < 2352)
			chest.dropItem(player, 10262, 3, false);
		random = Rnd.get(chance);
		if(random < 2352)
			chest.dropItem(player, 10263, 3, false);
		random = Rnd.get(chance);
		if(random < 2352)
			chest.dropItem(player, 10264, 3, false);
		random = Rnd.get(chance);
		if(random < 2352)
			chest.dropItem(player, 10265, 3, false);
		random = Rnd.get(chance);
		if(random < 2352)
			chest.dropItem(player, 10266, 3, false);
		random = Rnd.get(chance);
		if(random < 2352)
			chest.dropItem(player, 10267, 3, false);
		random = Rnd.get(chance);
		if(random < 2352)
			chest.dropItem(player, 10268, 3, false);
		random = Rnd.get(chance);
		if(random < 882)
			chest.dropItem(player, 5595, 1, false);
		random = Rnd.get(chance);
		if(random < 882)
			chest.dropItem(player, 9898, 1, false);
		random = Rnd.get(chance);
		if(random < 2352)
			chest.dropItem(player, 10269, 3, false);
		random = Rnd.get(chance);
		if(random < 5878)
			chest.dropItem(player, 8739, 1, false);
		random = Rnd.get(chance);
		if(random < 4899)
			chest.dropItem(player, 8740, 1, false);
		random = Rnd.get(chance);
		if(random < 4082)
			chest.dropItem(player, 8741, 1, false);
		random = Rnd.get(chance);
		if(random < 3674)
			chest.dropItem(player, 8742, 1, false);
		random = Rnd.get(chance);
		if(random < 7410)
			chest.dropItem(player, 21183, 1, false);
		random = Rnd.get(chance);
		if(random < 7410)
			chest.dropItem(player, 21184, 1, false);
		random = Rnd.get(chance);
		if(random < 3705)
			chest.dropItem(player, 21185, 1, false);
		random = Rnd.get(chance);
		if(random < 7410)
			chest.dropItem(player, 1538, 2, false);
		random = Rnd.get(chance);
		if(random < 5558)
			chest.dropItem(player, 3936, 1, false);
		random = Rnd.get(chance);
		if(random < 1030)
			chest.dropItem(player, 9654, 1, false);
		random = Rnd.get(chance);
		if(random < 1030)
			chest.dropItem(player, 9655, 1, false);
		random = Rnd.get(chance);
		if(random < 112)
			chest.dropItem(player, 5908, 1, false);
		random = Rnd.get(chance);
		if(random < 112)
			chest.dropItem(player, 5911, 1, false);
		random = Rnd.get(chance);
		if(random < 112)
			chest.dropItem(player, 5914, 1, false);
		random = Rnd.get(chance);
		if(random < 52)
			chest.dropItem(player, 6364, 1, false);
		random = Rnd.get(chance);
		if(random < 156)
			chest.dropItem(player, 21748, 1, false);
	}
}