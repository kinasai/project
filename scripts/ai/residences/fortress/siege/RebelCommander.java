package ai.residences.fortress.siege;

import ai.residences.SiegeGuardFighter;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.NpcString;
import com.l2cccp.gameserver.utils.ChatUtils;

/**
 * @author VISTALL
 * @date 20:10/19.04.2011
 */
public class RebelCommander extends SiegeGuardFighter
{
	public RebelCommander(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);

		ChatUtils.say(getActor(), NpcString.DONT_THINK_THAT_ITS_GONNA_END_LIKE_THIS);
	}
}