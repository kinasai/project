package ai;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.DefaultAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.ChatUtils;

public class EvilNpc extends DefaultAI
{
	private long _lastAction;
	private static final String[] _txt = {
			"отстань!",
			"уймись!",
			"я тебе отомщу, потом будешь прощения просить!",
			"у тебя будут неприятности!",
			"я на тебя пожалуюсь, тебя арестуют!" };

	public EvilNpc(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(Creature attacker, SkillEntry skill, int damage)
	{
		NpcInstance actor = getActor();
		if(attacker == null || attacker.getPlayer() == null)
			return;

		// Ругаемся и кастуем скилл не чаще, чем раз в 3 секунды
		if(System.currentTimeMillis() - _lastAction > 3000)
		{
			int chance = Rnd.get(0, 100);
			if(chance < 2)
				attacker.getPlayer().setKarma(attacker.getPlayer().getKarma() + 5, true);
			else if(chance < 4)
				actor.doCast(SkillTable.getInstance().getSkillEntry(4578, 1), attacker, true); // Petrification
			else
				actor.doCast(SkillTable.getInstance().getSkillEntry(4185, 7), attacker, true); // Sleep

			ChatUtils.say(actor, attacker.getName() + ", " + _txt[Rnd.get(_txt.length)]);
			_lastAction = System.currentTimeMillis();
		}
	}
}