package ai.seedofinfinity;

import com.l2cccp.gameserver.ai.DefaultAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;

/**
 * @author pchayka
 */

public class SymbolofCohemenes extends DefaultAI
{

	public SymbolofCohemenes(NpcInstance actor)
	{
		super(actor);
		actor.startImmobilized();
	}

	@Override
	protected void onEvtAttacked(Creature attacker, SkillEntry skill, int damage)
	{}

	@Override
	protected void onEvtAggression(Creature target, int aggro)
	{}
}