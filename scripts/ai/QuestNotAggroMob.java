package ai;

import java.util.List;

import com.l2cccp.gameserver.ai.DefaultAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.quest.QuestEventType;
import com.l2cccp.gameserver.model.quest.QuestState;
import com.l2cccp.gameserver.skills.SkillEntry;

/**
 * @author VISTALL
 * @date 8:44/10.06.2011
 */
public class QuestNotAggroMob extends DefaultAI
{
	public QuestNotAggroMob(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	public boolean thinkActive()
	{
		return false;
	}

	@Override
	public  void onEvtAttacked(Creature attacker, SkillEntry skill, int dam)
	{
		NpcInstance actor = getActor();
		Player player = attacker.getPlayer();

		if(player != null)
		{
			List<QuestState> quests = player.getQuestsForEvent(actor, QuestEventType.ATTACKED_WITH_QUEST);
			if(quests != null)
				for(QuestState qs : quests)
					qs.getQuest().notifyAttack(actor, qs);
		}
	}

	@Override
	public  void onEvtAggression(Creature attacker, int d)
	{
		//
	}
}
