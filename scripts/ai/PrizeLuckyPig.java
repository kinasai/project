package ai;

import com.l2cccp.gameserver.ai.DefaultAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;

/**
 * @author VISTALL
 * @date 23:59/28.04.2012
 */
public class PrizeLuckyPig extends DefaultAI
{
	public PrizeLuckyPig(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(Creature attacker, SkillEntry skill, int damage)
	{
		getActor().doDie(attacker);
	}
}
