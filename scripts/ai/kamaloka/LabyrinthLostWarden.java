package ai.kamaloka;

import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.entity.Reflection;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.instances.ReflectionBossInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;

/**
 * @author pchayka
 */
public class LabyrinthLostWarden extends Fighter
{
	NpcInstance actor = getActor();
	Reflection r = actor.getReflection();
	SkillEntry pa_down = SkillTable.getInstance().getSkillEntry(5701, 7);

	public LabyrinthLostWarden(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		NpcInstance captain = findLostCaptain();
		if(!r.isDefault() && captain != null)
			captain.altOnMagicUseTimer(captain, pa_down);
		super.onEvtDead(killer);
	}

	private NpcInstance findLostCaptain()
	{
		for(NpcInstance n : r.getNpcs())
			if(n instanceof ReflectionBossInstance)
				return n;
		return null;
	}
}