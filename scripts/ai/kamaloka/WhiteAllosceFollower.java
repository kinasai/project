package ai.kamaloka;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CtrlIntention;
import com.l2cccp.gameserver.ai.Mystic;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.base.SpecialEffectState;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.Location;

/**
 * Минион босса 73й камалоки
 *
 * @author pchayka
 */
public class WhiteAllosceFollower extends Mystic
{
	private NpcInstance actor = getActor();
	private long _skillTimer = 0L;
	private final static long _skillInterval = 15000L;
	final SkillEntry s_soul_bind = SkillTable.getInstance().getSkillEntry(5624, 1);  // Soul Confinement

	public WhiteAllosceFollower(NpcInstance actor)
	{
		super(actor);
		actor.setInvul(SpecialEffectState.TRUE);
	}

	@Override
	protected boolean thinkActive()
	{
		if(_skillTimer + _skillInterval < System.currentTimeMillis())
		{
			_skillTimer = System.currentTimeMillis() + Rnd.get(1L, 5000L);
			for(Creature p : actor.getAroundCharacters(1000, 200))
				if(p.isPlayer() && !p.isDead() && !p.isInvisible())
					actor.getAggroList().addDamageHate(p, 0, 10);

			Creature target = actor.getAggroList().getRandomHated();
			if(target != null)
				actor.doCast(s_soul_bind, target, false);
			setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
			addTaskMove(Location.findAroundPosition(actor, 400), false);
		}
		return super.thinkActive();
	}

	@Override
	protected void thinkAttack()
	{
		// do not attack
		setIntention(CtrlIntention.AI_INTENTION_ACTIVE);
	}
}