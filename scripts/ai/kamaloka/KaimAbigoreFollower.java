package ai.kamaloka;

import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;

/**
 * Минион босса 63й камалоки
 *
 * @author pchayka
 */
public class KaimAbigoreFollower extends Fighter
{
	private NpcInstance actor = getActor();
	final SkillEntry s_self_explosion = SkillTable.getInstance().getSkillEntry(4614, 6);  // Explosion

	public KaimAbigoreFollower(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void thinkAttack()
	{
		Creature target = getAttackTarget();
		if(actor.getDistance(target) < 50)
		{
			actor.doCast(s_self_explosion, target, true);
			actor.doDie(null);
		}
		super.thinkAttack();
	}
}