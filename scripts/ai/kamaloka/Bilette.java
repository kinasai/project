package ai.kamaloka;

import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;

/**
 * Босс 66й камалоки
 *
 * @author pchayka
 */
public class Bilette extends Fighter
{
	private static final int _followerId = 18574;  // Follower of Bilette
	private NpcInstance actor = getActor();

	private long _spawnTimer = 0L;
	private int _spawnCounter = 0;
	private final static long _spawnInterval = 60000L;
	private final static int _spawnLimit = 15;

	public Bilette(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void thinkAttack()
	{
		if(_spawnTimer == 0)
			_spawnTimer = System.currentTimeMillis();
		if(_spawnCounter < _spawnLimit && _spawnTimer + _spawnInterval < System.currentTimeMillis())
		{
			NpcInstance follower = NpcUtils.spawnSingle(_followerId, Location.findPointToStay(actor.getLoc(), 200, actor.getGeoIndex()), actor.getReflection());
			follower.getAI().notifyEvent(CtrlEvent.EVT_AGGRESSION, getAttackTarget(), 1000);
			_spawnTimer = System.currentTimeMillis();
			_spawnCounter++;
		}
		super.thinkAttack();
	}
}