package ai.territorywars;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.events.impl.TerritoryWarsEvent;
import com.l2cccp.gameserver.model.entity.events.objects.DuelSnapshotObject;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.tables.SkillTable;
import com.l2cccp.gameserver.utils.Location;
import com.l2cccp.gameserver.utils.NpcUtils;

/**
 * @author L2CCCP & Java-man
 */
public class TWRaidbossIT extends Fighter
{
	private int rbType;
	private int variable;

	public TWRaidbossIT(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtSpawn()
	{
		rbType = Rnd.get(0, 2);
		variable = 0;
	}

	@Override
	protected void onEvtAttacked(Creature attacker, SkillEntry skill, int damage)
	{
		if(rbType == 0) // summoner
		{
			if(_actor.getCurrentHpPercents() <= 10 && variable == 20)
			{
				spawnNpcs(25451, 5, _actor.getLoc());
				variable = 10;
			}
			else if(_actor.getCurrentHpPercents() <= 20 && variable == 30)
			{
				spawnNpcs(25221, 5, _actor.getLoc());
				variable = 20;
			}
			else if(_actor.getCurrentHpPercents() <= 30 && variable == 40)
			{
				spawnNpcs(25145, 5, _actor.getLoc());
				variable = 30;
			}
			else if(_actor.getCurrentHpPercents() <= 40 && variable == 50)
			{
				spawnNpcs(25056, 5, _actor.getLoc());
				variable = 40;
			}
			else if(_actor.getCurrentHpPercents() <= 50 && variable == 0)
			{
				spawnNpcs(25094, 5, _actor.getLoc());
				variable = 50;
			}
		}
		else if(rbType == 1) // debuffer
		{
			if(_actor.getCurrentHpPercents() <= 10 && variable == 20)
			{
				castSkill(attacker, 1366, 1);
				variable = 10;
			}
			else if(_actor.getCurrentHpPercents() <= 20 && variable == 30)
			{
				castSkill(attacker, 1248, 12);
				variable = 20;
			}
			else if(_actor.getCurrentHpPercents() <= 30 && variable == 40)
			{
				castSkill(attacker, 1104, 14);
				variable = 30;
			}
			else if(_actor.getCurrentHpPercents() <= 40 && variable == 50)
			{
				castSkill(attacker, 883, 7);
				variable = 40;
			}
			else if(_actor.getCurrentHpPercents() <= 50 && variable == 0)
			{
				castSkill(attacker, 1096, 16);
				variable = 50;
			}
		}
		else if(rbType == 2) // killer
		{
			if(_actor.getCurrentHpPercents() <= 50 && variable == 0)
			{
				variable = 1;
				ThreadPoolManager.getInstance().scheduleAtFixedRate(new Runnable(){
					@Override
					public void run()
					{
						Creature target = _actor.getAggressionTarget();
						if(target != null)
							castSkill(target, 4730, 12);
					}
				}, 40000L, 40000L);
			}
		}

		super.onEvtAttacked(attacker, skill, damage);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		final TeamType team = killer.getTeam();

		if(team != TeamType.NONE)
		{
			final TerritoryWarsEvent event = getActor().getEvent(TerritoryWarsEvent.class);

			if(event != null)
			{
				for(final DuelSnapshotObject obj : event.getPlayers(team))
				{
					Player player = obj.getPlayer();
					if(player != null)
						player.getInventory().addItem(Config.IVORY_REWARD_ID[0], Rnd.get(Config.IVORY_REWARD_ID[1], Config.IVORY_REWARD_ID[2]));
				}

				event.stopEvent();
			}
			else
				System.out.println("Raid None team for player -> " + killer.getName());
		}

		super.onEvtDead(killer);
	}

	private void spawnNpcs(int npcId, int count, Location loc)
	{
		for(int i = 0; i < count; i++)
		{
			NpcUtils.spawnSingle(npcId, loc);
		}
	}

	private void castSkill(Creature target, int skillId, int skillLvl)
	{
		SkillEntry skill = SkillTable.getInstance().getSkillEntry(skillId, skillLvl);
		_actor.doCast(skill, target, true);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}
}