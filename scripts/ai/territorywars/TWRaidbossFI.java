package ai.territorywars;

import java.util.concurrent.atomic.AtomicBoolean;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.GameObjectsStorage;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.TeamType;
import com.l2cccp.gameserver.model.entity.events.impl.TerritoryWarsEvent;
import com.l2cccp.gameserver.model.entity.events.objects.DuelSnapshotObject;
import com.l2cccp.gameserver.model.instances.NpcInstance;

/**
 * @author L2CCCP & Java-man
 */
public class TWRaidbossFI extends Fighter
{
	private AtomicBoolean dead = new AtomicBoolean(false);

	public TWRaidbossFI(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtSpawn()
	{
		dead = new AtomicBoolean(false);
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		if(!dead.compareAndSet(false, true))
			return;

		final TerritoryWarsEvent event = getActor().getEvent(TerritoryWarsEvent.class);

		for(NpcInstance npc : GameObjectsStorage.getAllByNpcId(_actor.getNpcId(), true))
		{
			if(npc.getReflection() == event.getReflection())
			{
				npc.removeEvent(event);
				npc.deleteMe();
			}
		}

		if(Rnd.nextBoolean())
		{
			final TeamType team = killer.getTeam();
			if(team != TeamType.NONE)
			{
				if(event != null)
				{
					for(final DuelSnapshotObject obj : event.getPlayers(team))
					{
						Player player = obj.getPlayer();
						if(player != null)
							player.getInventory().addItem(22352, 1);
					}

					event.stopEvent();
				}
			}
			else
				System.out.println("Raid None team for player -> " + killer.getName());
		}

		super.onEvtDead(killer);
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}
}