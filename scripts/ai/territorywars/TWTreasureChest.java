package ai.territorywars;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.entity.events.impl.TerritoryWarsEvent;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;

/**
 * @author Java-man & L2CCCP
 */
public class TWTreasureChest extends Fighter
{
	private final int maxhit;
	private int hit;

	public TWTreasureChest(NpcInstance actor)
	{
		super(actor);
		maxhit = actor.getParameter("killHit", 15);
	}

	@Override
	protected void onEvtSpawn()
	{
		hit = 0;
	}

	@Override
	protected void onEvtAttacked(Creature attacker, SkillEntry skill, int damage)
	{
		if(hit >= maxhit)
			_actor.doDie(attacker);
		else
			hit++;
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		Player player = killer.getPlayer();
		if(player != null)
			player.getInventory().addItem(Config.CHEST_REWARD_ID[0], Rnd.get(Config.CHEST_REWARD_ID[1], Config.CHEST_REWARD_ID[2]));

		if(getActor().getParameter("stopEvent", false))
		{
			TerritoryWarsEvent event = (TerritoryWarsEvent) _actor.getEvent(TerritoryWarsEvent.class);
			if(event != null)
				event.stopEvent();
		}

		super.onEvtDead(killer);
	}

	@Override
	protected void onEvtDeSpawn()
	{
		if(getActor().getParameter("stopEvent", false))
		{
			TerritoryWarsEvent event = (TerritoryWarsEvent) _actor.getEvent(TerritoryWarsEvent.class);
			if(event != null)
				event.stopEvent();
		}

		super.onEvtDeSpawn();
	}
}