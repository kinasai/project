package ai.plainsoflizardman;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;

/**
 * @author VISTALL
 * @date 11:42/01.05.2012
 */
public class Mystic extends com.l2cccp.gameserver.ai.Mystic
{
	public Mystic(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	public void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer != null && killer.isPlayable())
			HerbHelper.give(getActor(), killer.getPlayer());
	}
}
