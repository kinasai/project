package ai.plainsoflizardman;

import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;

/**
 * @author VISTALL
 * @date 11:33/01.05.2012
 */
public class Fighter extends com.l2cccp.gameserver.ai.Fighter
{
	public Fighter(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	public void onEvtDead(Creature killer)
	{
		super.onEvtDead(killer);
		if(killer != null && killer.isPlayable())
			HerbHelper.give(getActor(), killer.getPlayer());
	}
}
