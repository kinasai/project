package ai;

import com.l2cccp.gameserver.Config;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.instancemanager.QuestManager;
import com.l2cccp.gameserver.model.CommandChannel;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Party;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.base.Race;
import com.l2cccp.gameserver.model.entity.olympiad.Olympiad;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.model.quest.Quest;
import com.l2cccp.gameserver.model.quest.QuestState;
import com.l2cccp.gameserver.network.l2.s2c.SkillList;
import com.l2cccp.gameserver.utils.ItemFunctions;

/**
 * AI рейд босса Barakiel, выдает нубл за свою смерть
 * @author L2CCCP
 */
public class Barakiel extends Fighter
{
	private static final int BARAKIEL_ID = 25325;
	private static final int NOBLESS_TIARA = 7694;
	private static final int[] QUESTS = { 241, 242, 246, 247 };

	public Barakiel(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtDead(final Creature killer)
	{
		if(Config.BARAKIEL_KILL_NOBLE && getActor().getNpcId() == BARAKIEL_ID && killer.isPlayer())
		{
			final Player _killer = killer.getPlayer();
			CommandChannel channel = null;
			final Party party = _killer.getParty();

			if(party != null)
				channel = party.getCommandChannel();

			if(channel != null)
			{
				for(Party partys : channel.getParties())
				{
					for(Player member : partys)
					{
						finishQuest(member);
						addNoble(member);
					}
				}
			}
			else if(party != null)
			{
				for(Player member : party.getPartyMembers())
				{
					finishQuest(member);
					addNoble(member);
				}
			}
			else
			{
				finishQuest(_killer);
				addNoble(_killer);
			}
		}

		super.onEvtDead(killer);
	}

	private void addNoble(final Player member)
	{
		if(!member.isNoble())
		{
			Olympiad.addNoble(member);
			member.setNoble(true);
			member.updatePledgeClass();
			member.updateNobleSkills();
			member.sendPacket(new SkillList(member));
			member.broadcastUserInfo(true);
			ItemFunctions.addItem(member, NOBLESS_TIARA, 1);
			member.sendChanges();
		}
	}

	private void finishQuest(final Player member)
	{
		for(final int id : QUESTS)
		{
			Quest quest = QuestManager.getQuest(id);
			final QuestState state = member.getQuestState(quest);
			if(state != null)
				state.exitCurrentQuest(false);
			quest.newQuestState(member, Quest.COMPLETED);
		}

		finishSubQuest(member);
	}

	private void finishSubQuest(Player member)
	{
		Quest quest = QuestManager.getQuest(234);
		QuestState state = member.getQuestState(234);
		if(state != null)
			state.exitCurrentQuest(true);
		quest.newQuestState(member, Quest.COMPLETED);

		if(member.getRace() == Race.kamael)
		{
			quest = QuestManager.getQuest(236);
			state = member.getQuestState(236);
			if(state != null)
				state.exitCurrentQuest(true);
			quest.newQuestState(member, Quest.COMPLETED);
		}
		else
		{
			quest = QuestManager.getQuest(235);
			state = member.getQuestState(235);
			if(state != null)
				state.exitCurrentQuest(true);
			quest.newQuestState(member, Quest.COMPLETED);
		}
	}
}