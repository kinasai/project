package ai;

import com.l2cccp.commons.lang.reference.HardReference;
import com.l2cccp.commons.lang.reference.HardReferences;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.CtrlEvent;
import com.l2cccp.gameserver.ai.Fighter;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.utils.ChatUtils;

/**
 * AI для ищущих помощи при HP < 50%
 *
 * @author Diamond
 */
public class WatchmanMonster extends Fighter
{
	private long _lastSearch = 0;
	private boolean isSearching = false;
	private HardReference<? extends Creature> _attackerRef = HardReferences.emptyRef();
	static final String[] flood = { "I'll be back", "You are stronger than expected" };
	static final String[] flood2 = { "Help me!", "Alarm! We are under attack!" };

	public WatchmanMonster(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtAttacked(final Creature attacker, SkillEntry skill, int damage)
	{
		final NpcInstance actor = getActor();
		if(attacker != null && !actor.getFaction().isNone() && actor.getCurrentHpPercents() < 50 && _lastSearch < System.currentTimeMillis() - 15000)
		{
			_lastSearch = System.currentTimeMillis();
			_attackerRef = attacker.getRef();

			if(findHelp())
				return;
			ChatUtils.say(actor, "Anyone, help me!");
		}
		super.onEvtAttacked(attacker, skill, damage);
	}

	private boolean findHelp()
	{
		isSearching = false;
		final NpcInstance actor = getActor();
		Creature attacker = _attackerRef.get();
		if(attacker == null)
			return false;

		for(final NpcInstance npc : actor.getAroundNpc(1000, 150))
			if(!actor.isDead() && npc.isInFaction(actor) && !npc.isInCombat())
			{
				clearTasks();
				isSearching = true;
				addTaskMove(npc.getLoc(), true);
				ChatUtils.say(actor, flood[Rnd.get(flood.length)]);
				return true;
			}
		return false;
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		_lastSearch = 0;
		_attackerRef = HardReferences.emptyRef();
		isSearching = false;
		super.onEvtDead(killer);
	}

	@Override
	protected void onEvtArrived()
	{
		NpcInstance actor = getActor();
		if(isSearching)
		{
			Creature attacker = _attackerRef.get();
			if(attacker != null)
			{
				ChatUtils.say(actor, flood2[Rnd.get(flood2.length)]);
				notifyFriends(attacker, null, 100);
			}
			isSearching = false;
			notifyEvent(CtrlEvent.EVT_AGGRESSION, attacker, 100);
		}
		else
			super.onEvtArrived();
	}

	@Override
	protected void onEvtAggression(Creature target, int aggro)
	{
		if(!isSearching)
			super.onEvtAggression(target, aggro);
	}
}