package ai.isle_of_prayer;

import instances.CrystalCaverns;

import com.l2cccp.gameserver.ai.DefaultAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.Player;
import com.l2cccp.gameserver.model.instances.DoorInstance;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.utils.ItemFunctions;

public class EmeraldDoorController extends DefaultAI
{
	private boolean openedDoor = false;
	private Player opener = null;

	public EmeraldDoorController(NpcInstance actor)
	{
		super(actor);
		actor.setHasChatWindow(false);
	}

	@Override
	protected boolean thinkActive()
	{
		DoorInstance door = getClosestDoor();
		boolean active = false;
		if(getActor().getReflection().getInstancedZoneId() == 10)
			active = ((CrystalCaverns) getActor().getReflection()).areDoorsActivated();
		if(door != null && active)
		{
			for(Creature c : getActor().getAroundCharacters(250, 150))
				if(!openedDoor && c.isPlayer() && ItemFunctions.deleteItem(c.getPlayer(), 9694, 1)) // Secret Key
				{
					openedDoor = true;
					door.openMe();
					opener = c.getPlayer();
				}

			boolean found = false;
			if(opener != null)
				for(Creature c : getActor().getAroundCharacters(250, 150))
					if(openedDoor && c.isPlayer() && c.getPlayer() == opener)
						found = true;

			if(!found)
				door.closeMe();
		}
		return super.thinkActive();
	}

	private DoorInstance getClosestDoor()
	{
		NpcInstance actor = getActor();
		for(Creature c : actor.getAroundCharacters(200, 200))
			if(c.isDoor())
				return (DoorInstance) c;
		return null;
	}

	@Override
	protected boolean randomWalk()
	{
		return false;
	}
}