package ai;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.DefaultAI;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.utils.ChatUtils;

public class GuardianAngel extends DefaultAI
{
	static final String[] flood = {
			"Waaaah! Step back from the confounded box! I will take it myself!",
			"Grr! Who are you and why have you stopped me?",
			"Grr. I've been hit..." };

	public GuardianAngel(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected boolean thinkActive()
	{
		NpcInstance actor = getActor();
		ChatUtils.say(actor, flood[Rnd.get(2)]);

		return super.thinkActive();
	}

	@Override
	protected void onEvtDead(Creature killer)
	{
		NpcInstance actor = getActor();
		if(actor != null)
			ChatUtils.say(actor, flood[2]);
		super.onEvtDead(killer);
	}
}