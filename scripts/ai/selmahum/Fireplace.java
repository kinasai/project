package ai.selmahum;

import java.util.concurrent.Future;

import com.l2cccp.commons.threading.RunnableImpl;
import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ThreadPoolManager;
import com.l2cccp.gameserver.ai.DefaultAI;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.utils.NpcUtils;

public class Fireplace extends DefaultAI
{
	private static final long delay = 5 * 60 * 1000L;
	private Future<?> respawn = null;

	public Fireplace(NpcInstance actor)
	{
		super(actor);
		actor.startImmobilized();
	}

	@Override
	protected void onEvtSpawn()
	{
		super.onEvtSpawn();
		if(Rnd.chance(60))
			getActor().setNpcState(1);
		respawn = ThreadPoolManager.getInstance().scheduleAtFixedRate(new Switch(), 10000L, delay);
	}

	@Override
	protected void onEvtDeSpawn()
	{
		super.onEvtDeSpawn();
		if(respawn != null)
		{
			respawn.cancel(false);
			respawn = null;
		}
	}

	public class Switch extends RunnableImpl
	{
		@Override
		public void runImpl()
		{
			NpcInstance actor = getActor();
			if(actor.getNpcState() == 1)
				actor.setNpcState(0);
			else
			{
				actor.setNpcState(1);
				if(Rnd.chance(70))
					NpcUtils.spawnSingle(18933, actor.getLoc(), delay / 2);
			}
		}
	}

	@Override
	public boolean isGlobalAI()
	{
		return true;
	}
}