package ai;

import com.l2cccp.commons.util.Rnd;
import com.l2cccp.gameserver.ai.Ranger;
import com.l2cccp.gameserver.model.Creature;
import com.l2cccp.gameserver.model.instances.NpcInstance;
import com.l2cccp.gameserver.network.l2.components.NpcString;
import com.l2cccp.gameserver.skills.SkillEntry;
import com.l2cccp.gameserver.utils.ChatUtils;

/**
 * AI для Karul Bugbear ID: 20600
 *
 * @author Diamond
 */
public class KarulBugbear extends Ranger
{
	private boolean _firstTimeAttacked = true;

	public KarulBugbear(NpcInstance actor)
	{
		super(actor);
	}

	@Override
	protected void onEvtSpawn()
	{
		_firstTimeAttacked = true;
		super.onEvtSpawn();
	}

	@Override
	protected void onEvtAttacked(Creature attacker, SkillEntry skill, int damage)
	{
		NpcInstance actor = getActor();
		if(_firstTimeAttacked)
		{
			_firstTimeAttacked = false;
			if(Rnd.chance(25))
				ChatUtils.say(actor, NpcString.YOUR_REAR_IS_PRACTICALLY_UNGUARDED);
		}
		else if(Rnd.chance(10))
			ChatUtils.say(actor, NpcString.S1_WATCH_YOUR_BACK, attacker.getPlayer().getName());
		super.onEvtAttacked(attacker, skill, damage);
	}
}